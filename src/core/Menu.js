import React from 'react'
//import AppBar from '@material-ui/AppBar'
// import AppBar from '@material-ui/core/AppBar';
//import Toolbar from '@material-ui/Toolbar'
// import Toolbar from '@material-ui/core/Toolbar';
//import Typography from '@material-ui/Typography'
// import Typography from '@material-ui/core/Typography';
//import IconButton from '@material-ui/IconButton'
// import IconButton from '@material-ui/core/IconButton';
//import HomeIcon from '@material-ui/icons/Home'
// import HomeIcon from '@material-ui/core/Icon';
//import Button from '@material-ui/Button'
import Button from '@material-ui/core/Button';
import auth from '../auth/auth-helper'
import {withRouter} from 'react-router-dom'



// const isActive = (history, path) => {
//   if (history.location.pathname === path)
//     return {color: '#ffa726'}
//   else
//     return {color: '#ffffff'}
// }
const Menu = withRouter(({history}) => (
<div className="fh-logout">
      {
        auth.isAuthenticated() && (
          <Button onClick={() => {
              auth.signout(() => history.push('/'))
            }}>Sign out</Button>
  )
      }</div>
))

export default Menu
