import React, {Component} from 'react'
import PropTypes from 'prop-types'
//import {withStyles} from 'material-ui/styles'
import { withStyles } from "@material-ui/core/styles";
//import Card, {CardContent, CardMedia} from 'material-ui/Card'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
//import Typography from 'material-ui/Typography'
import Typography from '@material-ui/core/Typography';
import seashellImg from './../assets/images/seashell.jpg'
//import Grid from 'material-ui/Grid'
import Grid from '@material-ui/core/Grid';
import auth from './../auth/auth-helper'

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 5,
  },
  card: {
    maxWidth: 1500,
    margin: 'auto',
    marginTop: theme.spacing.unit * 0
  },
  title: {
    padding:`${theme.spacing.unit * 3}px ${theme.spacing.unit * 2.5}px ${theme.spacing.unit * 2}px`,
    color: theme.palette.text.secondary
  },
  media: {
    minHeight: 500
  }
})

class Home extends Component {
  state = {
    defaultPage: true
  }
  init = () => {
    if(auth.isAuthenticated()){
      this.setState({defaultPage: false})
    }else{
      this.setState({defaultPage: true})
    }
  }
  componentWillReceiveProps = () => {
    this.init()
  }
  componentDidMount = () => {
    this.init()
  }
  render() {
    const {classes} = this.props
    return (
      <div className={classes.root}>
        {this.state.defaultPage &&
          <Grid container spacing={24}>
            <Grid item xs={12}>
              <Card className={classes.card}>
                {/*<Typography type="headline" component="h2" className={classes.title}>
                  Home Page
                </Typography>*/}
                <CardMedia className={classes.media} image={seashellImg} title="Unicorn Shells"/>
                <CardContent>
                  <Typography type="body1" component="p">
                    Welcome to the Familhey. 
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        }
        {/* {!this.state.defaultPage &&
          <Grid container spacing={24}>
            <Grid item xs={8} sm={7}>
              <Newsfeed/>
            </Grid>
            <Grid item xs={6} sm={5}>
              <FindPeople/>
            </Grid>
          </Grid>
        } */}
      </div>
    )
  }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Home)
