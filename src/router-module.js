import React, { useState, useMemo, useEffect } from 'react'
import { Route, Redirect, Switch, BrowserRouter as Router } from 'react-router-dom'
// import Signup from './auth/Signup';
import Signin from './auth/Signin';
import forgotPasswordClass from './auth/forgotpassword';
import creation from './modules/createFamily/FamilyCreation';
import AdvanceSettings from './modules/advanceSettings/advanceSettings';
import FamilyView from './modules/family/familyView/Familyview';
import FamilyList from './modules/family/familyList/FamilyList'
import MemberListing from './modules/memberListing/memberListing';
import defaultHome from './modules/default-home';
import CreatePost from './modules/Post/CreatePost/iindex';
// import PostList from './modules/Post/PostListing/PostList';
import eventListing from './modules/events/eventListing';
import EventDetails from './modules/events/eventDetails';
import UserDetails from './modules/users/userDetails';
import Calender from './modules/calender/Calender';
import topbar from './components/TopBar';
import Callto from './callto';
import listLanding from './modules/Post/listLanding';
import myFamily from './modules/family/myFamily';
import resultPage from './components/SearchResultPage/resultPage'
import albumDetails from './modules/AlbumModal/albumDetails';
import CreateMyFamily from './modules/family/createFamily';
import EditMyFamily from './modules/family/editFamily';
import listAnnouncement from './modules/Post/listAnnouncement';
import listAnnouncementPage from './modules/Post/listAnnouncement/list';
import policy from './modules/policy';
import termsofservice from './modules/termsofservice';
import { UserContext } from './components/TopBar/GlobalContext';
import PostDetails from './modules/Post/PostDetails';
import FooterBar from './components/FooterBar';
import support from './modules/support';
import NoPage from './modules/NoPage';
import RequestForHelp from './modules/RequestForHelp';
import RequestDetails from './modules/RequestDetails';
import ContributorsList from './modules/ContributorsList';
import listMessages from './modules/messageRequest/list';
import messageDetails from './modules/messageRequest/chat';
import RequestList from './modules/RequestList';
import CreateTopic from './modules/messageRequest/create';
import HomePage from './modules/home-page';
import { useSelector,useDispatch } from 'react-redux'
import { decryption } from './shared/encryptDecrypt'
import StripeRedirect from "./modules/stripe";
import ExternalPayment from './components/ExternalPayment'
import createExcel from './modules/createExcel';
import OfflinePay from './modules/RequestDetails/offline'
import Collash from './modules/collash';
import SettingsPage from './components/TopBar/Notifications/settings'
import IndividualStatus from './components/Announcements/individualStatus'
const Routing = () => {
    const dispatch = useDispatch()
    const [user, setUser] = useState({});
    const value = useMemo(() => ({ user, setUser }), [user, setUser]);
    let user_logged_ = useSelector(stateX => stateX.user_details && stateX.user_details.userId && stateX.user_details.logged)
    const [state, setState] = useState({
        user_id: useSelector(stateY => stateY.user_details && stateY.user_details.userId && stateY.user_details.userId),
        user_logged: undefined,
        landing_pages: ['/signup', '/signin', '/forgot', '/policy', ''],
    })
    let { user_id, user_logged } = state
    //get Details from Local when reload or tab reopen
    const getStateFromLocal = () => {
        let user_state = localStorage.getItem('hash_key')
        if (user_state) {
            let state_n = decryption(user_state)
            let state_splited = state_n.split('#@#');
            let _user_id = state_splited[0]
            let accessToken = state_splited[1]
            let token = state_splited[2]
            let phone = state_splited[3]
            if (_user_id) {
                dispatch({ type: 'USER_UPDATE', payload: { user_id: _user_id, accessToken: accessToken, token: token, phone: phone } })
            }
            return user_id
        }
    }
    useEffect(() => {
        setState(prev => ({ ...prev, user_logged: user_logged_ ? user_logged_ : false }))
    }, [user_logged_])

    useEffect(() => {
        if (user_id) {
            setState(prev => ({ ...prev, user_logged: true }))
        }
        else {
            if (localStorage.getItem('hash_key')) {
                setState(prev => ({ ...prev, user_logged: true, user_id: getStateFromLocal() }))
            }
            else {
                localStorage.clear()
                setState(prev => ({ ...prev, user_logged: false }))
            }
        }
    }, [user_id])

    const maintainRoute = () => {
        switch (user_logged) {
            case true:
                return (
                    <React.Fragment>
                        {window.location.pathname !== '/payment/client_id' && <Route exact component={topbar} />}
                        <Switch>
                            <Route exact path="/home" render={() => (<Redirect to="/post" />)} />
                            <Route exact path="/" render={() => (<Redirect to="/post" />)} />
                            <Route exact path="/familyview/:familyId" component={FamilyView} />
                            <Route exact path="/familyview/:familyId/:albumId" component={FamilyView} />
                            <Route exact path="/familyview/:familyId/:section/:subSection" component={FamilyView} />
                            <Route exact path="/advsetting" component={AdvanceSettings} />
                            <Route exact path="/listmembers" component={MemberListing} />
                            <Route exact path="/creation" component={creation} />
                            <Route exact path="/FamilyList" component={FamilyList} />
                            <Route exact path="/default" component={defaultHome} />
                            <Route exact path="/events" component={eventListing} />
                            <Route exact path="/event-details/:eventId" component={EventDetails} />
                            <Route exact path="/event-details/:eventId/:sectionID" component={EventDetails} />
                            <Route exact path="/user-details/:profileId" component={UserDetails} />
                            <Route exact path="/user-details/:profileId/:sectionID" component={UserDetails} />
                            <Route exact path="/post/create" component={CreatePost} />
                            <Route exact path="/post/create/:post_id" component={CreatePost} />
                            <Route exact path="/announcement/create" component={CreatePost} />
                            <Route exact path="/announcement/create_from_family/:grp_id" component={CreatePost} />
                            <Route exact path="/announcement/create/:post_id" component={CreatePost} />
                            <Route exact path="/calender" component={Calender} />
                            <Route exact path="/post" component={listLanding} />
                            <Route exact path="/post_details/:id" component={PostDetails} />
                            <Route exact path="/announcement/story" component={listAnnouncement} />
                            <Route exact path="/announcement" component={listAnnouncementPage} />
                            <Route exact path="/topics" component={listMessages} />
                            <Route exact path="/topics/create" component={CreateTopic} />
                            <Route exact path="/topics/edit" component={CreateTopic} />
                            <Route exact path="/topics/message" component={messageDetails} />
                            <Route exact path="/request-help" component={RequestForHelp} />
                            <Route exact path="/request-details" component={RequestDetails} />
                            <Route exact path="/contributors" component={ContributorsList} />
                            <Route exact path="/requests" component={RequestList} />
                            <Route exact path="/signup" render={() => (<Redirect to="/post" />)} />
                            <Route exact path="/signin" render={() => (<Redirect to="/post" />)} />
                            <Route exact path="/my-family" component={myFamily} />
                            <Route exact path="/searchResult" component={resultPage} />
                            <Route exact path="/album-details/:folderId" component={albumDetails} />
                            <Route exact path="/album-details/:folderId/admin" component={albumDetails} />
                            <Route exact path="/create-family" component={CreateMyFamily} />
                            <Route exact path="/create-family-link/createAndLink/:familyID" component={CreateMyFamily} />
                            <Route exact path="/create-family/:familyId" component={CreateMyFamily} />
                            <Route exact path="/edit-family/:familyId" component={EditMyFamily} />
                            <Route exact path="/create-family/:familyId/tabId=:tabId" component={CreateMyFamily} />
                            <Route exact path="/events/:eventTag" component={EventDetails} />
                            <Route exact path="/payment/:auth_details" component={ExternalPayment} />
                            <Route exact path="/page/:type/:id" component={Callto} />
                            <Route exact path="/stripe_oauth" component={StripeRedirect} />
                            <Route exact path="/policy" component={policy} />
                            <Route exact path="/termsofservice" component={termsofservice} />
                            <Route exact path="/support" component={support} />
                            <Route exact path="/createExcel" component={createExcel} />
                            <Route exact path="/offlinePayment" component={OfflinePay} />
                            <Route exact path="/collash" component={Collash} />
                            <Route exact path="/individualStatus" component={IndividualStatus} />
                            <Route exact path="/notificationSettings" component={SettingsPage} />
                            <Route exact path="*" component={NoPage} />

                        </Switch>
                    </React.Fragment>
                );
                break;
            case false:
                return (
                    <Switch>
                        <Route exact path="/signin" component={Signin} />
                        <Route exact path="/forgot" component={forgotPasswordClass} />
                        <Route exact path="/policy" component={policy} />
                        <Route exact path="/termsofservice" component={termsofservice} />
                        <Route exact path="/support" component={support} />
                        <Route exact path="/home" component={HomePage} />
                        <Route exact path="/" exact path="/" render={() => (<Redirect to="/home" />)} />
                        <Route exact path="/post" render={() => (<Redirect to="/home" />)} />
                        <Route exact path="/stripe_oauth" component={StripeRedirect} />
                        <Route exact path="/payment/:auth_details" component={ExternalPayment} />
                        <Route exact path="/page/:type/:id" component={Callto} />
                        <Route exact path="*" component={HomePage} />
                    </Switch>
                )
                break;
        }
    }
    return (
        <Router>
            <div className="fh-wrap" id="f-wrap">
                <UserContext.Provider value={value}>
                    <Switch>
                        {maintainRoute()}
                    </Switch>
                    {window.location.pathname !== '/payment/client_id' && <Route exact component={FooterBar} />}
                </UserContext.Provider>
            </div>
        </Router>
    )
}

export default Routing 