import {decryption} from './encryptDecrypt'

export default function getStateFromLocal(){
    let user_state = localStorage.getItem('hash_key')
    if(user_state){
      let state = decryption(user_state)
      let state_splited = state.split('#@#');
      let user_id = state_splited[0]
      let accessToken = state_splited[1]
      let token = state_splited[2]
      return [user_id,accessToken,token]
    }
  }

export function setNewDataInLocal(){

}