import {notification } from "antd";

const notifications = (type,message,description) => {
    notification[type]({
        message: message ? message : '',
        description: description ?description : '' ,
      });
}

export {
    notifications
}