import React from 'react';
import { useAppConfig } from './sharedFunction';

const withHooksHOC = (Component) => {
  return (props) => {
    const configValue = useAppConfig();
    return <Component configObj={configValue} {...props} />;
  };
};

export {
  withHooksHOC
}