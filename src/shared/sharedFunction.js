import { useEffect, useState } from 'react';
import config from '../config/config';
function useAppConfig() {
  const [configObj, setconfigObj] = useState({});

  useEffect(() => {
    setconfigObj(config);
    return () => {
      setconfigObj(config);
    };
  }, []);
  return configObj;
}


export  {
  useAppConfig
}