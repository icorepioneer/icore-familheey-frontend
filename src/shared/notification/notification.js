import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import React from 'react';
import { encryption,decryption } from '../encryptDecrypt';

export default class firebaseConfig extends React.Component {
	app: any;
	apiKey: any;
	db: any;
	constructor(props: any) {
		super(props);
		this.state = {
			aa: '',
		};
    const firebase_Config = {
			apiKey: decryption('U2FsdGVkX1842hxRacu%2B5Cy3P0XUj75umbKeTVI%2F0OtR6f0%2BKQJiVOlUmm7du8IsrhBM1T6guHrXTvPDlMMTyg%3D%3D'),
			authDomain: "familheey-255407.firebaseapp.com",
			databaseURL: "https://familheey-255407.firebaseio.com",
			projectId: decryption('U2FsdGVkX1%2BdMqGEnKZAtca1XZvQ3WL5lxTBFsHkRp4FnH%2F2vfaKKlOS6lBrc%2B%2BA'),
			storageBucket: "familheey-255407.appspot.com",
			messagingSenderId: decryption('U2FsdGVkX19whUVsOjqqvVTbtUNKUF0h6GkOkm061EY%3D'),
			appId: decryption('U2FsdGVkX19QU9MINEa4vLHX1QvKTTOEkSda54h1pND8xydbWS5jdnbzPXMZSSYMbeB%2FKoj81cwKIYKUdC7wbA%3D%3D'),
			measurementId: decryption('U2FsdGVkX1%2BZ0%2BvVt%2F%2Fr1aq3an7GhvAyPMvNgPAAMlg%3D')
		};
		if (!firebase.apps.length) {
			this.app = firebase.initializeApp(firebase_Config);
			this.db = this.app.database();
		} else {
			this.app = firebase.apps[0];
			this.db = this.app.database();
		}
	}
	config = () => {
		return this.db;
	};
	firebaseTime = () => {
		return firebase.database.ServerValue.TIMESTAMP;
	};
}