const CryptoJS = require("crypto-js");
const key = process.env.REACT_APP_SECRET_KEY;

const decryption = (data) => {
    var cipher = decodeURIComponent(data);
    var decipher = CryptoJS.AES.decrypt(cipher, key);
    return decipher.toString(CryptoJS.enc.Utf8);
}
const encryption = (data) => {
    var eventId = CryptoJS.AES.encrypt(data.toString(), key);
    return encodeURIComponent(eventId)
}

export {
    decryption,
    encryption
}