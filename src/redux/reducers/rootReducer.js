import { combineReducers } from 'redux'
import userReducer from './userReducer'
import SideBarReducer from './sideBarReducer'
const rootReducer = {
    user_details: userReducer,
    side_bar_data:SideBarReducer
}

export default combineReducers(rootReducer);