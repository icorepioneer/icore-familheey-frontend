const initialState = {
    sideBarCount_loaded: false,
    sideBarCount: undefined,
    announcement_data_loaded: false,
    announcement: undefined,
    active_fam_loaded: false,
    active_fam: undefined,
    events_up: undefined,
    events_rem: undefined,
    events_loaded: false
}

const SideBarReducer = (state, action) => {
    if (typeof state === 'undefined') {
        return initialState
    }
    let { data } = action.payload
    switch (action.type) {
        case 'COUNT_ADD':
            return {
                ...state,
                sideBarCount: data,
                sideBarCount_loaded: true
            }
        case 'ANNOUNCEMENT_ADD':
            return {
                ...state,
                announcement: data,
                announcement_data_loaded: true
            }
        case 'FAMILY_ADD':
            return {
                ...state,
                active_fam: data,
                active_fam_loaded: true
            }
        case 'EVENT_ADD':
            return {
                ...state,
                events_up: data.upcoming,
                events_rem: data.remaining,
                events_loaded: true
            }
        default:
            return state;
    }
}
export default SideBarReducer