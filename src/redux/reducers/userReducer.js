import { encryption } from '../../shared/encryptDecrypt';
const initialState = {
    userId: undefined,
    accessToken: undefined,
    user_details: undefined,
    token: undefined,
    phone:undefined,
    logged:undefined,
    discover:undefined,
    search_term:undefined,
    user_details_loaded:false
}

const userReducer = (state, action) => {
    if (typeof state === 'undefined') {
        return initialState
    }
    if(action.type === 'USER_ADD'){
        let {user_id,accessToken,token,phone} = action.payload;
        //accessToken - AuthToken
        //token - refresh token
        return { ...state, 
            userId:`${user_id}`,
            accessToken:accessToken,
            token:token,
            phone:phone,
            logged:true
         }
    }
    if(action.type === 'USER_UPDATE'){
        let {user_id,accessToken,token,phone} = action.payload;
        return { ...state, 
            userId:`${user_id}`,
            accessToken:accessToken,
            token:token,
            phone:phone
         }
    }
    if(action.type === 'USER_DETAILS_UPDATE'){
        let {data} = action.payload;
        return { ...state, 
            user_details:data,
            phone:data.phone,
            user_details_loaded:true
         }
    }
    if(action.type === 'AUTH_TOKEN_UPDATE'){
        let {userId,token,phone} = state;
        let {data} = action.payload;
        let to_encrypt = `${userId}#@#${data}#@#${token}#@#${phone}`
        localStorage.setItem('hash_key',encryption(to_encrypt))
        return { ...state, 
            accessToken:data
        }
    }
    if(action.type === 'USER_LOGGED_OUT'){
        return { ...state, 
            logged:false
        }
    }
    if(action.type === 'DISCOVER_UNMOUNT'){
        return {
            ...state,
            ...action.payload
        }
    }
    if(action.type === 'SEARCH_TERM'){
        return {
            ...state,
            ...action.payload
        }
    }
    
    return state;
}
export default userReducer