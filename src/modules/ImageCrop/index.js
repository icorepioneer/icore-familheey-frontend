import React, { PureComponent } from 'react';
import ReactCrop from 'react-image-crop';
import './style.css';
import { updateFamilyCover } from './../createFamily/api-family';
import { Button, Icon } from "antd";
import { makeAlbumCoverPIc } from '../AlbumModal/album-api'
import { updateEventDetails } from '../events/eventDetails/api-eventDetails'
import {updateUserDetails} from '../../modules/users/userProfile/api-profile'
import { connect } from 'react-redux'
import { notifications } from '../../shared/toasterMessage';
class CustomImageCrop extends PureComponent {
    state;
    imageRef;
    constructor(props) {
        super(props);
        let {userID} = props
        this.state = {
            crop: {
                unit: "%",
                aspect: 261 / 100,
                width: 30,
            },
            imgFile: '',
            croppedImageUrl: '',
            src: '',
            user_id: userID,
            _loading: false,
            button_disabled:true
        }
    }
    componentDidMount() {
        this.setState({ crop: this.props.params.crop });
    }
    setCrop(e) {
        this.setState({ crop: e });
    }
    blobToFile = (theBlob, fileName) => {
        var b = theBlob;
        //A Blob() is almost a File() - it's just missing the two properties below which we will add
        b.lastModifiedDate = new Date();
        b.name = fileName;
        return theBlob;
    }
    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            this.state.croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({ croppedImageUrl: this.state.croppedImageUrl });
        }
    }
    enableButton=()=>{
        this.setState({
            button_disabled:false
        })
    }
    saveImg = () => {
        let { croppedImageUrl, src, user_id, _loading } = this.state;
        let { params } = this.props;
        croppedImageUrl = croppedImageUrl ? croppedImageUrl :src
        if(croppedImageUrl == ""){
            notifications('error', "* required fields are empty", '')
            return;
        }
        this.setState({ _loading: !_loading })
        fetch(croppedImageUrl)
            .then(res => res.blob())
            .then(blob => {
                let file = new File([blob], "file_name.png", {type: blob.type});
                var formData = new FormData();
                formData.append('id', params.group_id)
                formData.append('user_id', user_id)
                switch (params.editType) {
                    case 'coverpic':
                        this.enableButton()
                        formData.append("cover_pic", file);
                        formData.append("group_original_image", this.state.imgFile)
                        updateFamilyCover(formData).then((response) => {
                            this.setImage(response, this.props.params.editType)
                        }).catch((error) => {
                            console.log("error happened")
                        })
                        break;
                    case 'logo':
                        this.enableButton()
                        formData.append("logo", file);
                        updateFamilyCover(formData).then((response) => {
                            this.setImage(response, this.props.params.editType)
                        }).catch((error) => {
                            console.log("error happened")
                        })
                        break;
                    case 'folder_cover':
                        let _params = {
                            album_id: this.props.folderId,
                            cover_pic: "https://familheey.s3.amazonaws.com/Documents/Documents-1576229121035.png"
                        }
                        this.enableButton()
                        makeAlbumCoverPIc(_params).then((response) => {
                            this.setImage(response, this.props.params.editType)
                        }).catch((err) => {
                            console.log("error happened")
                        })
                        break;
                    case 'event_cover_pic':
                        this.enableButton()
                        fetch(src).then(res => res.blob()).then(img_blob => {
                            let file_original = new File([img_blob], "file_name.jpeg", { type: img_blob.type });
                            let _formData = new FormData();
                            _formData.append('event_image', file)
                            _formData.append('event_original_image', file_original)
                            _formData.append('id', params.event_id)
                            _formData.append('user_id', params.user_id)
                            updateEventDetails(_formData).then(res => {
                                setTimeout((() => this.setImage('coverpic', res)), 3000)
                                this.setState({ _loading: !this.state.loading })
                                // this.setImage('coverpic', res)
                            })
                        })
                        break;
                    case 'user_cover_pic':
                        this.enableButton()
                        fetch(src).then(res=>res.blob()).then(file_blob=>{
                            let user_cover_pic_ = new File([file_blob],'user_cover.jpg',{type:file_blob.type});
                            let user_formData = new FormData();
                            user_formData.append('user_original_image',user_cover_pic_);
                            user_formData.append('cover_pic',file);
                            user_formData.append('id',params.user_id)
                            console.log(user_formData)
                            updateUserDetails(user_formData).then(res=>{
                                setTimeout(()=>{
                                    this.setImage('user_cover',res.data)
                                },3000)
                            })
                        })
                        break;
                    case 'user_profile_pic':
                        this.enableButton()
                        fetch(src).then(res=>res.blob()).then(()=>{
                            let user_formData = new FormData();
                            user_formData.append('propic',file);
                            user_formData.append('id',params.user_id)
                            console.log(user_formData)
                            updateUserDetails(user_formData).then(res=>{
                                setTimeout(()=>{
                                    this.setImage('user_cover',res.data)
                                },3000)
                            })
                        })
                        break;
                    default:
                        formData.append("logo", file);
                        break;
                }
            })
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );
        return new Promise((resolve, reject) => {
            const base64Image = canvas.toDataURL('image/jpeg');
            resolve(base64Image);
        });
    }
    onImageLoaded = (e) => {
        this.imageRef = e;
        this.enableButton()
    }
    onCropComplete = (crop) => {
        this.makeClientCrop(crop);
    }
    onSelectFile = e => {
        this.setState({ imgFile: e.target.files[0] })
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
        e.target.value = null;
    };
    setImage = (response, type) => {
        setTimeout(() => {
            this.setState({ src: '', _loading: false })
        }, (3000))
        this.props.callBackimgFromParent(response, type);
    }
    cancelImgEdit = () => {
        this.setState({ src: '', croppedImageUrl: '' })
        this.props.callBackimgFromParent('');
    }
    render() {
        let { _loading, crop,button_disabled } = this.state;
        return (
            <div style={{ display: 'inline-block', width: '100%' }}>
                <div className="f-modal-wiz-body">
                    <div className="f-form-control">
                        <label>Select Image*</label>
                        <input type="file" accept="image/*" onChange={this.onSelectFile} />
                    </div>
                    <ReactCrop
                        crop={crop}
                        crossorigin="anonymous"
                        src={this.state.src} crop={this.state.crop}
                        onImageLoaded={this.onImageLoaded}
                        onComplete={this.onCropComplete}
                        onChange={newCrop => this.setCrop(newCrop)} />
                </div>
                
                {
                    <div className="f-modal-wiz-footer">
                        <Button 
                        loading={_loading} 
                        style={{ minWidth: 150 }}
                        onClick={() => this.saveImg()}
                        disabled={button_disabled}
                        >
                            Upload 
                            <Icon
                             type="arrow-right"
                              />
                              </Button>
                    
                    <Button
                         onClick={() => this.cancelImgEdit()} 
                         disabled={_loading==true?true:false}
                         type="link"
                          >Cancel</Button>
                    </div>
                }
                
            </div>
        );//change on line no 246 added conditional statement Anand
    }
}

const mapStateToProps=(state)=>{
    let {userId} = state.user_details;
    return {
        userID:userId
    }
}
export default connect(mapStateToProps,null)(CustomImageCrop);