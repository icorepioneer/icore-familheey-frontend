import React, { useState, useEffect } from 'react';
import './style.scss'
import { Card, Button, Avatar,Spin,notification } from "antd";
import MessageSidebar from '../../../components/MessageSidebar';
import io from 'socket.io-client';
import { getCommentsByPost, addTopicComment, deleteConversation, getTopicById, addusersToTopic, getTopicUsersList, acceptUserTopic, rejectUserTopic } from '../../../modules/Post/post_service'
import ChatSection from '../../../components/ChatSection'
import ChatUSerModal from './Components/index'
import * as moment from 'moment'
import {useSelector} from 'react-redux';


const MessageDetails = ({ history, location }) => {
    const [messageState, setChatState] = useState({
        topicId: undefined,
        tag: undefined,
        user_id: useSelector(state=>state.user_details.userId),
        chatHistory: undefined,
        commentIdSet: new Set(),
        newMessage: undefined,
        remove_chat_id: undefined,
        trigger: false,
        topicDetails: undefined,
        created_at: undefined,
        created_user: '',
        description: '',
        propic: undefined,
        title: '',
        to_users: [],
        topic_attachment: [],
        connections: [],
        _loading: false,
        modal_visible: false,
        channel_name: '',
        is_accept: false,
        created_by: undefined,
        topic_owner: false,
        one_Accepted: false,
        accept_tag: undefined,
        chatloader: true
    })
    let { topicId, trigger, tag, user_id, chatHistory, newMessage, remove_chat_id,
        created_at, created_user, description, propic, title, to_users, connections,
        _loading, modal_visible, is_accept, created_by, topic_owner, one_Accepted,
        chatloader
    } = messageState

    let socket = io(process.env.REACT_APP_SOCKET_URL)


    useEffect(() => {
        let { state } = location
        if (state.messageId) {
            let { messageId,created_user:c_user,title:T1,description:descp } = state
            setChatState(prev => ({
                ...prev, topicId: messageId,
                tag: `topic_channel_${messageId}`,
                accept_tag: `topic_accept_${messageId}`,
                created_user: c_user,
                title: T1,
                description: descp
            }))
        }
    }, [location])
    useEffect(() => {
        if (user_id && topicId) {
            setChatState(prev => ({ ...prev, channel_name: `topic_channel_${topicId}` }))
            let params = {
                user_id: `${user_id}`,
                topic_id: `${topicId}`,
                limit: 5,
                offset: 0
            }
            let params_ = {
                user_id: `${user_id}`,
                topic_id: `${topicId}`
            }
            getCommentsByPost(params).then(res => {
                let { data } = res.data
                setChatState(prev => ({ ...prev, chatHistory: data }))
            })
            getTopicById(params_).then(res => {
                let data = res.data.data[0]
                let oneofThemAccepted = false;
                let { created_at:createAt, created_user:cuser, description:descrp, propic:p_pic, title:T2, to_users:to_usr, is_accept:isAcpt, created_by:createdBy } = data
                to_usr && to_usr.map(item => {
                    if (item.is_accept) {
                        oneofThemAccepted = true
                    }
                })
                setChatState(prev => ({
                    ...prev, topicDetails: data,
                    created_at: createAt,
                    created_user: cuser,
                    description: descrp,
                    propic: p_pic,
                    title: T2,
                    to_users: to_usr,
                    created_by: createdBy,
                    is_accept: isAcpt,
                    one_Accepted: oneofThemAccepted,
                    chatloader: false
                }))
            })
            getAllList()
        }
    }, [user_id, topicId])

    useEffect(() => {
        created_by && setChatState(prev => ({ ...prev, topic_owner: user_id == created_by ? true : false }))
    }, [created_by])

    const modChat = (data) => {
        let { type, delete_id,user_name,propic:Ppic,user_id:Usr_ID } = data[0]
        switch (type) {
            case "delete_comment":
                setChatState(prev => ({ ...prev, remove_chat_id: delete_id[0] }))
                break;
            case "topic_accept":
                openNotification(user_name);
                let newUser={
                    full_name:user_name,
                    is_accept:true,
                    propic:Ppic,
                    user_id:parseInt(Usr_ID)
                }
                let _to_users = to_users
                let new_to_users = [..._to_users,newUser]
                setChatState(prev=>({...prev,to_users:new_to_users,one_Accepted:true}))
                break;
            default:
                setChatState(prev => ({ ...prev, newMessage: data }))
                break;
        }
    }

    useEffect(() => {
        if (remove_chat_id) {
            // console.log(remove_chat_id,chatHistory)
            let current = [...chatHistory]
            current = current.filter(data => data.comment_id != remove_chat_id)
            // console.log(current)
            setChatState(prev => ({ ...prev, chatHistory: current }))
            return (() => {
                setChatState(prev => ({ ...prev, remove_chat_id: undefined }))
            })
        }
    }, [remove_chat_id])
    useEffect(() => {
        if (newMessage) {
            let current = [...chatHistory, newMessage[0]]
            // current.push(newMessage[0])
            setChatState(prev => ({ ...prev, chatHistory: current }))
            return (() => {
                setChatState(prev => ({ ...prev, newMessage: undefined }))
            })
        }
    }, [newMessage])
    //socket
    useEffect(() => {
        socket.on(tag, (data) => {
            modChat(data)
        })
    }, [tag])

    const addUsersToTopic_ = (params) => {
        setChatState(prev => ({ ...prev, _loading: true }))
        addusersToTopic(params).then(res => {
            getAllList()
            toggleModal()
            setChatState(prev => ({ ...prev, _loading: false }))
        })
    }
    const getAllList = () => {
        let params = {
            topic_id: `${topicId}`,
            user_id: `${user_id}`
        }
        getTopicUsersList(params).then(res => {
            let { topicUsers, connectionUsers, nonConnectionUsers } = res.data
            let new_array = [...connectionUsers, ...nonConnectionUsers]
            let idArray = []
            topicUsers.map(item => {
                idArray.push(item.user_id)
            })
            new_array = new_array.filter(data => !idArray.includes(data.user_id))
            // new_array = new_array
            setChatState(prev => ({
                ...prev, to_users: topicUsers
                , connections: new_array
            }))
        })
    }
    const toggleModal = () => {
        setChatState(prev => ({ ...prev, modal_visible: !modal_visible }))
    }

    const acceptOrRejectUserTopic = (_tag) => {
        let params = {
            user_id: `${user_id}`,
            topic_id: `${topicId}`
        }
        if (_tag === "accept") {
            acceptUserTopic(params).then(res => {
                setChatState(prev => ({ ...prev, is_accept: true }))
                getAllList()
            })
        }
        else if (_tag === "reject") {
            rejectUserTopic(params).then(res => {
                setChatState(prev => ({ ...prev, is_accept: false }))
                history.push('/post')
            })
        }
    }

    const openNotification = (user) => {
        notification.info({
          message: `${user} has accepted your request`,
        });
      };
    return (
        <React.Fragment>
            <section className="listAnnouncement messageDetails f-page">
                <ChatUSerModal
                    connections={connections}
                    visible={modal_visible}
                    addUsersToTopic={addUsersToTopic_}
                    user_id={user_id}
                    topic_id={topicId}
                    _loading={_loading}
                    toggleModal={toggleModal}
                />
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <div className="f-post-listing">
                            <MessageSidebar history={history} userList={to_users} />
                            <div className="f-posts-main">
                                <Card className="postsCard f-post-card">
                                    <header className="f-head" >
                                        <div className="f-west" onClick={() => this.redirectStory()}>
                                            <Avatar src={propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + propic : require('../../../assets/images/default_propic.png')} />
                                            <span>
                                                <h5>{created_user}</h5>
                                                <h6>{` Created on ${created_at && moment(created_at).format("MMM d ,YYYY hh:mm A")}`}</h6>
                                                {/* <h6>Created on Apr 17 ,2020 12:37 PM</h6> */}
                                            </span>
                                        </div>
                                        <div className="f-east">
                                            <Button
                                                onClick={() => toggleModal()}
                                                className="f-add-user f-button">
                                                <img
                                                    src={require('../../../assets/images/icon/plus_user.svg')}
                                                /> Add Users</Button>
                                        </div>
                                    </header>
                                    <section className="f-sect">
                                        <div className="f-post-description">
                                            <h4>{title}</h4>
                                            <p>{description}</p>
                                        </div>
                                    </section>
                                </Card>
                                {/* Communications */}
                                {chatHistory && <div className="f-story-communications f-have-drop-menu">
                                    <h4>Conversations</h4>
                                    <div className="f-story-communication" >

                                        {chatloader ?
                                            <div style={{
                                                height: '500px'
                                            }}>
                                                <Spin
                                                    style={{
                                                        marginTop: '40%',
                                                        marginLeft: '48%'
                                                    }}
                                                    size="large" />
                                            </div> :
                                            <ChatSection
                                                one_Accepted={one_Accepted}
                                                is_accept={is_accept}
                                                data={chatHistory}
                                                userId={user_id}
                                                comment={addTopicComment}
                                                topicId={topicId}
                                                trigger_={trigger}
                                                deleteConversation={deleteConversation}
                                                history={history}
                                                created_user={created_user}
                                                acceptOrRejectUserTopic={acceptOrRejectUserTopic}
                                                topicOwner={topic_owner}
                                                chatloader={chatloader}
                                            />}
                                </div>
                                </div>}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default MessageDetails