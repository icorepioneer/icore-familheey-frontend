import React, { useState, useEffect } from 'react'
import { Modal, Card, Checkbox, Button,Input } from 'antd'
import lowerCase from 'lower-case'

const { Search } = Input

const ChatUSerModal = ({ visible,toggleModal,connections, status = "Add",addUsersToTopic,user_id,topic_id,_loading }) => {
    const [state,setState] = useState({
        selectedUserIDs:[],
        displayContent_:[],
        searchTag:null
    })
    let { selectedUserIDs,displayContent_ ,searchTag} = state

    const addIds=(id)=>{
        let copy_array = selectedUserIDs
        if(copy_array.includes(id)){
            copy_array = copy_array.filter(data=>data!=id)
        }
        else{
            copy_array.push(id)
        }
        setState(prev=>({...prev,selectedUserIDs:copy_array}))
    }

    useEffect(()=>{
        setState(prev => ({ ...prev, displayContent_: connections }))
    },[connections])
    useEffect(() => {
        let Filtered = connections && Array.isArray(connections) && connections.filter(item => lowerCase(item.full_name || item.location).indexOf(lowerCase(searchTag)) !== -1)
        // let _data = searchTag&&searchTag.length > 0 ? Filtered : connections
        setState(prev => ({ ...prev, displayContent_: Filtered }))
    }, [searchTag])

    const setSearchTag=(event)=>{
        let searchKey = event.target.value
        setState(prev=>({...prev,searchTag:searchKey}))
    }
//use updateTopic in case we need to use remove people
    const onSubmitClicked=()=>{
        let params={
            created_by:`${user_id}`,
            to_user_id:selectedUserIDs,
            topic_id:`${topic_id}`
        }
        addUsersToTopic(params);
    }

    const resetSelected=()=>{
        setState(prev=>({...prev,selectedUserIDs:[]}))
    }

    return (
        <React.Fragment>
            <Modal
                onCancel ={toggleModal}
                closable={true}
                title="Select members"
                visible={visible}
                destroyOnClose={true}
                className="f-modal f-modal-altx f-modal-wizard f-modal-altxz"
                footer={
                    [<div key="submit" className="f-modal-wiz-footer f-flx-start">
                        <Button
                            onClick={()=>onSubmitClicked()} 
                            loading={_loading} 
                            className="f-fill">{status}</Button>
                        <Button
                            onClick={()=>resetSelected()} 
                            style={{ marginLeft: '15px' }}>Reset</Button>
                    </div>]
                }
            >   <Search
                    style={{width:200,float:'right'}}
                    placeholder=" Search "
                    onChange={(event)=>setSearchTag(event)}
                />
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-member-listing">
                        <div className="f-member-listing-cards f-share-scroll">
                            {
                                displayContent_.map(item => {
                                    return <Card key={item.user_id} className="f-member-cardx">
                                        <div className="f-start">
                                            <div className="f-left">
                                                <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../../assets/images/default_propic.png')} />
                                            </div>
                                            <div className="f-right">
                                                <h4>{item.full_name}</h4>
                                                <h6>{item.location}</h6>
                                                <div className="f-right-combo">
                                                    <Checkbox
                                                        onClick={() => {
                                                            let id = item.user_id;
                                                            addIds(id)
                                                        }} >
                                                        {(selectedUserIDs.includes(item.user_id) ? "Un-Select" : "Select")}
                                                    </Checkbox>
                                                </div>
                                            </div>
                                        </div>
                                    </Card>
                                }
                                )
                            }
                        </div>
                    </div>
                </div>
            </Modal>
        </React.Fragment>
    )
}

export default ChatUSerModal