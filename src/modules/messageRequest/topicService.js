import axios from '../../services/apiCall';

const getTopics = (data) => {
    return axios.post(`/api/v1/familheey/topic_list`, data)
}

const CreateMessageRequest = (data) => {
    return axios.post(`/api/v1/familheey/topic_create`, data)
}

const updateMessageRequest = (data) => {
    return axios.post(`/api/v1/familheey/update_topic`, data)
}

const fetchMessageData = (data) => {
    return axios.post(`/api/v1/familheey/topic_detail`, data)
}

const addUserToMessage = (data) => {
    return axios.post(`/api/v1/familheey/topic_users_list`, data)
}

const fetchHistory = (data) => {
    return axios.post(`/api/v1/familheey/commonTopicListByUsers`, data)
}

export {
    getTopics,
    CreateMessageRequest,
    fetchMessageData,
    addUserToMessage,
    updateMessageRequest,
    fetchHistory
} 