import React, { useState, useEffect } from "react";
import "./style.scss";
import { Button, Input, Divider, Form, Spin, Modal, Card, Checkbox } from "antd";
import CommonSideBar from "../../../components/commonSideBar";
import { CreateMessageRequest, fetchMessageData, addUserToMessage, updateMessageRequest, fetchHistory } from "../topicService";
import { notifications } from "../../../shared/toasterMessage";
import { useSelector } from 'react-redux';
import moment from "moment";
// const { TextArea } = Input;
const { Search } = Input;
const CreateTopic = ({ history, form }) => {
    let { state } = history.location;
    let { getFieldDecorator, validateFields } = form;
    const [messageState, setMessageState] = useState({
        loading: false,
        visible: false,
        user_id: useSelector(stateX => stateX.user_details.userId),
        topicsData: {
            title: '',
            description: '',
            userToadd: state && state.uid ? state.uid : [],
            Topic_history: []
        },
        loader: true,
        hist_status: state && state.status ? state.status : 'create',
        topic_id: state && state.topic_id ? state.topic_id : null,
        usersList: []
    });
    let { visible, user_id, loader, hist_status, topic_id, topicsData, usersList } = messageState;
    let { title, userToadd, Topic_history } = topicsData;

    useEffect(() => {
        if (hist_status == 'edit') {
            getTopicDetails();
        } else {
            setMessageState((prev) => ({
                ...prev,
                loader: false
            }))
        }
        fetchHistoryFn();
    }, []);

    // Close Sidebar
    const closepostsidebar = () => {
        document.getElementById("f-wrap").classList.remove("f-show-post-sider");
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        validateFields((err, values) => {
            let { title:T1, description } = values;
            if (err) return;
            if (userToadd.length == 0) {
                notifications("error", "error", "Select atleast one user");
                return;
            }

            let input = {
                to_user_id: userToadd,
                title: T1,
                description: description ? description : ""
            }

            setMessageState((prev) => ({
                ...prev,
                loader: true
            }));

            switch (hist_status) {
                case 'create':
                    input = { ...input, created_by: user_id }
                    CreateMessageRequest(input).then((response) => {
                        let { data } = response.data;
                        history.push({
                            pathname: '/topics/message',
                            state: { messageId: data.topic_id }
                        })
                    }).catch((err_sub) => {
                        console.log(err_sub)
                        notifications("error", "error", "Something went wrong")
                    })
                    break;
                case 'edit':
                    input = { ...input, updated_by: user_id, topic_id: topicsData.topic_id.toString() }
                    updateMessageRequest(input).then((response) => {
                        // let { data } = response.data;
                        history.push({
                            pathname: '/topics/message',
                            state: { messageId: topic_id }
                        })
                    }).catch((er_r) => {
                        console.log(er_r)
                        notifications("error", "error", "Something went wrong")
                    })

                    break;

                default:
                    break;
            }
        });
    }

    // const onhandlechange = (e) => {
    //     let _value = e.target.value;
    //     topicsData = { ...topicsData, [e.target.name]: _value }
    //     setMessageState((prev) => ({
    //         ...prev,
    //         topicsData: topicsData
    //     }))
    // }

    /**
     * if edit , fetch the tpic details
     */
    const getTopicDetails = () => {
        let input = {
            user_id: user_id.toString(),
            topic_id: topic_id.toString()
        }

        fetchMessageData(input).then((response) => {
            let { data } = response.data;
            let alreadyExist = data[0].to_users.map((r) => r.user_id);
            topicsData.userToadd = [...topicsData.userToadd, ...alreadyExist];
            topicsData = { ...topicsData, ...data[0] }
            setMessageState((prev) => ({
                ...prev,
                topicsData: topicsData,
                loader: false
            }))
        }).catch((err) => {
            notifications("error", "error", "Something went wrong.")
        })
    }

    /**
     * button click fetch users to add to list
     */
    const addUser = (txt = "") => {
        setMessageState((prev) => ({
            ...prev,
            loader: true
        }))
        let input = { topic_id: topic_id, user_id: user_id, txt: txt }
        addUserToMessage(input).then(doc => {
            let { topicUsers, connectionUsers, nonConnectionUsers } = doc.data;
            let TotalUsers = [...topicUsers, ...connectionUsers, ...nonConnectionUsers]
            setMessageState((prev) => ({
                ...prev,
                usersList: TotalUsers,
                visible: true,
                loader: false
            }));
        }).catch(err => {
            notifications("error", "error", "Something went wrong.")
        })
    }

    /**
     * close modal
     */
    const onClose = () => {
        setMessageState((prev) => ({
            ...prev,
            visible: false
        }))
    }

    /**
     * add or remove users to a list
     */
    const addOrRemovetoList = (e, _user) => {
        let IndexIs = topicsData.userToadd.indexOf(parseInt(_user.user_id));
        if (IndexIs == -1) topicsData.userToadd.push(parseInt(_user.user_id));
        else topicsData.userToadd.splice(IndexIs, 1);
        setMessageState((prev) => ({ ...prev, ...topicsData }));
        fetchHistoryFn();
    }
    /**
     * search user to add in list
     */
    const searchUser = (e) => {
        let searchTxt = e.target.value;
        addUser(searchTxt)
    }

    /**
     * fetch history between two users
     */
    const fetchHistoryFn = () => {
        let { userToadd:uta } = topicsData;
        topicsData = { ...topicsData, Topic_history:[] }
        setMessageState((prev) => ({ ...prev, topicsData:topicsData }));
        if (uta.length == 1) {
            let input = {
                login_user: user_id.toString(),
                second_user: uta[0].toString()
            }
            fetchHistory(input).then((doc) => {
                let { data } = doc;
                topicsData = { ...topicsData, Topic_history: data.data }
                setMessageState((prev) => ({ ...prev, topicsData: topicsData }));
            }).catch((err) => {
                console.log(err)
                topicsData = { ...topicsData, Topic_history: [] }
                setMessageState((prev) => ({ ...prev, topicsData: topicsData }));
            })
        }
    }

    const redirectStory = (mid) => {
        history.push({
            pathname: '/topics/message',
            state: { messageId: mid }
        })
    }

    return (
        <section className="listLanding listMessages f-page">
            {loader &&
                <div className="spin-loader">
                    <Spin tip="Loading..." size="large"></Spin>
                </div>
            }
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-post-listing">
                        <CommonSideBar history={history} default="10" />
                        {/* Close Sidebar Mobile */}
                        <span
                            className="f-close-post-sidebar"
                            onClick={() => closepostsidebar()}
                            style={{ display: "none" }}
                        />
                        <div className="f-posts-main">
                            <div className="f-req-details-header">
                                <div className="f-left">
                                    <h4>Messages</h4>
                                </div>
                            </div>
                            <div className="f-post-feed">
                                <Form className="f-form-layout" onSubmit={(e) => handleSubmit(e)}>
                                    <div className="f-clearfix f-listland-card">
                                        <div className="f-modal-wiz-body">
                                            <div className="f-form-control">
                                                <label>
                                                    Topic <i>*</i>
                                                </label>
                                                <Form.Item>
                                                    {
                                                        getFieldDecorator('title', {
                                                            initialValue: title,
                                                            rules: [{ required: true, message: 'Title is mandatory.' }],
                                                        })(<Input placeholder="Topic Title" />)
                                                    }
                                                </Form.Item>
                                                {/* <Input type="text" name="title" value={title} required  onChange={ (e)=> onhandlechange(e)}/> */}
                                            </div>
                                            {/* <div className="f-form-control">
                                                <label>Description</label>
                                                <Form.Item>
                                                {
                                                getFieldDecorator('description',{ initialValue:description })(<TextArea rows={6}  placeholder="Description" />)
                                                }
                                                </Form.Item>
                                                <TextArea rows={3} name="description" value={description}  onChange={ (e)=> onhandlechange(e)}/>
                                            </div> */}

                                            <div className="f-form-control">
                                                <Button className="f-button f-line" onClick={() => { addUser() }}>Add People</Button>
                                                <b> {userToadd.length} </b> Users in list
                                            </div>

                                        </div>
                                        <Divider className="f-clearfix" style={{ margin: '20px 0' }} />
                                        <div className="f-modal-wiz-footer" key="1">
                                            <Button htmlType="submit" className="f-button">
                                                {hist_status == 'edit' ? 'Update' : 'Send'} Invite
                                            </Button>
                                        </div>
                                    </div>
                                </Form>
                            </div>

                            { Topic_history.length > 0 && 
                            <div className="f-post-feed" style={{marginTop:'30px'}}>
                                <div className="f-clearfix f-listland-card">

                                    <div className="f-req-details-header">
                                        <div className="f-left">
                                            <h4>Topic History</h4>
                                        </div>
                                    </div>

                                    {/* Cards */}
                                    {Topic_history.map((item, index) => (
                                        <Card key={index} className="postsCard f-post-card" onClick={() => redirectStory(item.topic_id)}>
                                            <header className="f-head">
                                                <div className="f-west" >
                                                    {/* <Avatar
                                                        src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/default_propic.png')}
                                                    /> */}
                                                    <span>
                                                        <h5>{item.title}</h5>
                                                        <h6>Posted {moment(item.created_at).fromNow()}</h6>
                                                    </span>
                                                </div>
                                            </header>
                                            {item.description && <section className="f-sect">
                                                <div className="f-post-description">
                                                    <p>
                                                        {item.description}
                                                    </p>
                                                </div>
                                            </section>}
                                            <footer className="f-foot">
                                                <div className="f-west">
                                                    <ul>
                                                        <li>
                                                            <span className="f-comment">
                                                                {item.conversation_count}
                                                                {item.conversation_count_new && parseInt(item.conversation_count_new) > 0 && <i />}
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </footer>
                                        </Card>
                                    ))
                                    }
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>

            <Modal
                visible={visible}
                destroyOnClose={true}
                title="Add New Users"
                okText="Add"
                onCancel={onClose}
                onOk={onClose}
                className="f-modal f-modal-wizard f-modal-wfooter"
                footer={[
                    <div className="f-modal-wiz-footer" style={{ marginTop: '0' }}>
                        <Button key="submit" type="primary" onClick={onClose}>Done</Button>
                    </div>
                ]}
            >
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-member-listing">
                        <div className="f-content-searchable">
                            <h4><strong>{userToadd.length}</strong> Users selected</h4>
                            <Search placeholder="Search" onChange={(e) => searchUser(e)} />
                        </div>
                        <div className="f-member-listing-cards f-share-scroll">
                            {
                                usersList.length > 0 && usersList.map((item, index) => (
                                    item.full_name && <Card className="f-member-cardx">
                                        <div className="f-start">
                                            <div className="f-left">
                                                <img onError={(e) => { e.target.src = require('../../../assets/images/profile-pic.png') }} src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic} />
                                            </div>
                                            <div className="f-right">
                                                <h4>{item.full_name}</h4>
                                                <h6>{item.location}</h6>
                                                <div className="f-right-combo">
                                                    <Checkbox onChange={(e) => addOrRemovetoList(e, item)} checked={(userToadd.indexOf(item.user_id) == -1 ? false : true)}>
                                                        {userToadd.indexOf(item.user_id) == -1 ? 'Select' : 'Selected'}
                                                    </Checkbox>
                                                    {/* <Checkbox onChange={(e) => this.handleChooseUserList(e, user)} checked={(this.state.selectedUsers.indexOf(user.user_id) == -1 ? false : true)}>
                                                                                    {this.state.selectedUsers.indexOf(user.user_id) == -1 ? 'Select' : 'Selected'}</Checkbox> */}
                                                    {/* { !item.is_accept &&  <button key={index} onClick={ (e)=>addOrRemovetoList(e)} value={item.user_id}> Select </button>} */}
                                                </div>
                                            </div>
                                        </div>
                                    </Card>))
                            }
                        </div>
                    </div>
                </div>
            </Modal>

        </section>
    );
};

export default Form.create()(CreateTopic);