import React, { useState, useEffect, useRef } from "react";
import "./style.scss";
import { Button, Icon, Card, Dropdown, Menu, Modal, Input, Divider, Spin } from "antd";
import CommonSideBar from "../../../components/commonSideBar";
import { getTopics } from "../topicService";
import { notifications } from "../../../shared/toasterMessage";
import moment from "moment";
import {useSelector} from 'react-redux';

const { Search }  = Input;

const ListMessages = ({ history }) => {

    const [state, setMessageState] = useState({
        loading: false,
        visible: false,
        user_id: useSelector(stateX=>stateX.user_details.userId),
        topicsData: [],
        loader:true,
        search_query:'',
        offset:0,
        limit:10
    });
    let { visible, user_id, topicsData, loader, search_query, offset, limit } = state;

    useEffect(() => {
        getTopic();
    }, [])


    const getTopic = () => {
        setMessageState((prev) => ({ ...prev, loader:true }))
        let input = {
            user_id: user_id,
            txt:search_query,
            limit:limit,
            offset:offset
        }
        getTopics(input).then((response) => {
            let { data } = response.data;
            let new_data = [...topicsData, ...data];
            let _offset = offset + data.length;
            setMessageState((prev)=>({
                ...prev,
                topicsData:new_data,
                offset:_offset,
                loader:false
            }));
        }).catch((err) => {
            notifications('error', 'error', 'Something went Wrong.')
        })
    }

    // const showModal = () => {
    //     setMessageState((prev) => ({ ...prev, visible: !visible }));
    // };

    const handleCancel = () => {
        setMessageState((prev) => ({ ...prev, visible: !visible }));
    };

    const { TextArea } = Input;
    // Dropdown
    const menu = (item)=> (
        <Menu style={{ minWidth: "100px" }}>
            <Menu.Item key="0" onClick={()=>editTopic(item.topic_id)}>
                Edit
            </Menu.Item>
        </Menu>
    );

    // Toggle Sidebar
    const togglepostsidebar = () => {
        document.getElementById("f-wrap").classList.toggle("f-show-post-sider");
    };

    // Close Sidebar
    const closepostsidebar = () => {
        document.getElementById("f-wrap").classList.remove("f-show-post-sider");
    };

    const redirectStory = (item) => {
        history.push({
            pathname:'/topics/message',
            state: { messageId: item.topic_id,
                created_user:item.created_user,
                title:item.title,
                description:item.description }
        })
    }

        /**
     * search text set to variable
     */
    const handleonChange = (e) => {
        search_query = e.target.value;
        offset = 0;
        setMessageState((prev)=>({
            ...prev,
            search_query:search_query, 
            topicsData:[], 
            offset:offset,
            limit:limit
        }));
        getTopic();
    }

    // implementing lazyload data
    const [element,setElement] = useState(null);
    const Loader = useRef(getTopic)
    const observer = useRef(new IntersectionObserver((entries)=>{
        let last = entries[0];
        last.isIntersecting && Loader.current();
    }, { threshold:1 } ) );
    useEffect(()=>{
        const currentElement = element;
        const currentObserver = observer.current;
        if(currentElement){
            currentObserver.observe(currentElement)
        }
        return() =>{
            if(currentElement) currentObserver.unobserve(currentElement);
        }
    },[element]);
    useEffect(()=>{
        Loader.current = getTopic;
    },[getTopic]);


    const editTopic = (id) => {
        history.push({
            pathname:'/topics/edit',
            state:{ status:'edit', topic_id:id }
        })
    }
    
    return (
        <section className="listLanding listMessages f-page">
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-post-listing">
                        <CommonSideBar history={history} default="10" />
                        {/* Close Sidebar Mobile */}
                        <span
                            className="f-close-post-sidebar"
                            onClick={() => closepostsidebar()}
                            style={{ display: "none" }}
                        />
                        <div className="f-posts-main">
                            <div className="f-posts-create f-force-hide-tab">
                                <Button onClick={() => history.push("/topics/create")}>
                                    Create New Message{" "}
                                    <img src={require("../../../assets/images/icon/plus.svg")} />
                                </Button>
                                <Modal
                                    visible={visible}
                                    title="Message Request"
                                    className="f-modal f-modal-wizard"
                                    onCancel={() => handleCancel()}
                                    footer={[]}>
                                    <div className="f-modal-wiz-body">
                                        <div className="f-form-control">
                                            <label>
                                                Topic <i>*</i>
                                            </label>
                                            <Input />
                                        </div>
                                        <div className="f-form-control">
                                            <label>Description</label>
                                            <TextArea rows={3} />
                                        </div>
                                        <div className="f-form-control">
                                            <Button className="f-button">Add People</Button>
                                        </div>
                                    </div>
                                    <Divider />
                                    <div className="f-modal-wiz-footer" key="1">
                                        <Button key="submit" onClick={() => handleCancel()}>
                                            Send Invite <Icon type="arrow-right" />
                                        </Button>
                                    </div>
                                </Modal>
                            </div>
                            <div className="f-posts-filters f-mg-btm0 f-force-hide-tab">
                                <Button
                                    onClick={() => togglepostsidebar()}
                                    className="f-postside-unfold"
                                    style={{ display: "none" }}>
                                    <Icon type="menu-unfold" />
                                </Button>
                            </div>

                            {/* Empty List */}
                            {/* <div className="f-event-contents">
                            <div className="f-switch-content">
                            <div className="f-content">
                            <div className="f-empty-content">
                            <img src={require('../../../assets/images/create_post.png')} />
                            <Card className="eventCard f-event-card" loading={true}></Card>
                            <div>
                            <h4>New messages will show up here.</h4>
                            <p>If you can't find a message, create one.</p>
                            <div className="f-clearfix">
                            <Button onClick={this.showModal}>Create New Message</Button>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div> */}
                            {/* Empty List - END */}

                            {/* Post Cards */}


                            <div className="f-req-details-header">
                            <div className="f-left">
                                <h4>Messages</h4>
                                {/* <h5><em>124</em> Needs</h5> */}
                            </div>
                            <Search placeholder="Search" value={search_query} onChange={(e)=>handleonChange(e)}/>
                        </div>


                            <div className="f-post-feed">
                                <div className="f-clearfix f-listland-card">
                                    {/* Cards */}
                                    {topicsData.length > 0 && topicsData.map((item, index) => (
                                        <Card key={index} className="postsCard f-post-card" onClick={() => redirectStory(item)}>
                                            <header className="f-head">
                                                <div className="f-west" >
                                                    {/* <Avatar
                                                        src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/default_propic.png')}
                                                    /> */}
                                                    <span>
                                                    <h5>{ (item.to_users.length == 1) && item.to_users.join(", ") }</h5>
                                                    <h5>{ (item.to_users.length == 2) && item.to_users.join(", ") }</h5>
                                                    <h5>{ (item.to_users.length > 2) && `${item.to_users[0]}, ${item.to_users[1]} and ${item.to_users.length-2} others` }</h5>
                                                    
                                                        {/* <h5> {item.created_user} </h5> */}
                                                        <h6>Posted {moment(item.created_at).fromNow()}</h6>
                                                    </span>
                                                </div>
                                            </header>
                                            <section className="f-sect">
                                                <div className="f-post-description">
                                                    <h4>{item.title}</h4>
                                                    <p>
                                                        {item.description}
                                                    </p>
                                                </div>
                                            </section>
                                            <footer className="f-foot">
                                                <div className="f-west">
                                                    <ul>
                                                        <li>
                                                            <span className="f-comment">
                                                                { item.conversation_count} 
                                                                { item.conversation_count_new && parseInt(item.conversation_count_new)>0 &&  <i />}
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="f-east" onClick={ (e)=>e.stopPropagation() }>
                                                    { user_id == item.created_by &&
                                                    <Dropdown overlay={()=>menu(item)} trigger={['click']} placement="bottomRight">
                                                        <Button />
                                                    </Dropdown>
                                                    }
                                                </div>
                                            </footer>
                                        </Card>
                                    ))
                                    }

                                    { topicsData.length == 0 && !loader &&
                                    <div>No Topic Found</div>
                                    }
                                </div>
                                { loader && <Spin tip="Loading..." style={{ 'marginLeft': '50%' }}></Spin> }
                            </div>
                        </div>
                    </div>
                </div>
                <div ref={setElement}></div>
            </div>
            
        </section>
    );
};

export default ListMessages;