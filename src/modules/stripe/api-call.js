
import axios from '../../services/beforeLogin'

const stripeOauth = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/payment/stripe_oauth`,data)
}

export {
    stripeOauth
}