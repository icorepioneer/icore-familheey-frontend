import React, { useState, useEffect } from 'react';
import { stripeOauth } from './api-call';
// import { notifications } from '../../shared/toasterMessage';
const StripeRedirect = ({ history }) => {
    const [state, setState] = useState({
        message:`Thank you for connecting your account with Familheey.<br/>
        Note : If your account is not fully verified, it will be connected, but you wont be able to request for funds until it is fully verified.<br/>
        You can check your account status in the following link : <a href='https://dashboard.stripe.com/'>https://dashboard.stripe.com/</a>`,
        loader:true,
        isSuccess:false,
        isFailure:false
    });
    let { message } = state;
    useEffect(() => {
        let QueryParams = history.location.search.split("?")[1];
            QueryParams = JSON.parse('{"' + decodeURI(QueryParams.replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}')
        stripeOauth(QueryParams)
        .then((doc)=>{
            setState((prev) => ({ ...prev, message:message, loader:false, isSuccess:true }));
        }).catch((err)=>{
            console.log(err.response)
            // let { data } = err.response;
            setState((prev) => ({ ...prev, message:message , isFailure:true, loader:false}));
            //notifications("error","error","Something went wrong!")
        })
    }, [])
    return (
        <section className="f-clearfix f-static-page f-four-not-four">
            <section className="f-page">
                <section className="f-clearfix f-body">
                    <section className="f-boxed">
                        <section className="f-fornotfor">
                            {/* { !loader && isSuccess && <span dangerouslySetInnerHTML={{ __html: message }}></span> }  */}
                            {/* { !loader && isFailure && <span dangerouslySetInnerHTML={{ __html: message }}></span> } */}
                            <span dangerouslySetInnerHTML={{ __html: message }}></span>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    )
}
export default StripeRedirect;