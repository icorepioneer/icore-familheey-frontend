import React from 'react';
class NoPage extends React.Component {
    render() {
        return (
            <section className="f-clearfix f-static-page f-four-not-four">
                {/* <header className="f-head-skelton">
                    <div className="f-boxed">
                        <div className="f-head-dock">
                            <a href="javascript:void(0);" onClick={() => this.props.history.push(`/post`)} className="f-brand"><img src={require('../../assets/images/logo-m.png')} alt="Familheey" /></a>
                        </div>
                    </div>
                </header> */}
                <section className="f-page">
                    <section className="f-clearfix f-body">
                        <section className="f-boxed">
                            <section className="f-fornotfor">
                                <div>
                                    <h3>404 <em>ERROR</em></h3>
                                    <h4>The page you are looking for is not found!</h4>
                                    <p>Goto <a onClick={() => this.props.history.push(`/`)}>Home</a></p>
                                </div>
                                <img src={require('../../assets/images/404-bg.png')} alt="404" />
                            </section>
                        </section>
                    </section>
                </section>

            </section>
        )
    }
}

export default NoPage;
