import React, { useState, useEffect } from 'react';
import './style.scss'
import { Button, Modal, Icon, Input, Select, Switch, Checkbox, Spin, Card } from 'antd';
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import ReactGoogleMapLoader from "react-google-maps-loader"

import { updateFamily, getAllLinkedFamily, requestLinkFamily } from '../../createFamily/api-family'
import CustomImageCrop from '../../ImageCrop/index';
import { encryption, decryption } from '../../../shared/encryptDecrypt';
import { getFamilDetails } from "../familyView/api-familyview";
import { useSelector } from 'react-redux'

const MY_API_KEY = decryption(process.env.REACT_APP_PLACE_ENCRYPT)
const { Option } = Select;
const { TextArea } = Input;
const EditMyFamily = ({ history }) => {
  const [createFamilyState, setcreateFamilyState] = useState({
    user_id: useSelector(state=>state.user_details.userId),
    familyName: '',
    location: '',
    current: 0,
    limitIndex: 10,
    familyDetails: {},
    disableNext: false,
    joinSuggesion: false,
    groupList: [],
    group_id: '',
    nextStage: false,
    familyIntro: '',
    visibility: true,
    searchable: true,
    is_linkable: false,
    showAdvanced: false,
    member_joining: 2,
    member_approval: 4,
    post_create: 6,
    post_visibilty: 8,
    post_approval: 10,
    link_family: 13,
    announcement_create:17,
    announcement_visibility:21,
    link_approval: 16,
    imageModal: false,
    imageType: '',
    params: {
      crop: {
        unit: "%",
        aspect: 261 / 100,
        // height : 100,
        width: 50
      },
      group_id: '',
      editType: ''
    },
    cover_pic: undefined,
    logo: '',
    linkModal: false,
    fetchFamilyList: [],
    selectedGroups: [],
    selectedGrpsID: [],
    group_category: '',
    spinLoader: false,
    search: "",
    values: "",
    linkParam: true

  })
  const {  current, familyName, location, groupList, user_id, group_id, nextStage, familyIntro,
    visibility, searchable, is_linkable, member_joining, member_approval, post_create, post_visibilty, post_approval,
    link_family, link_approval, imageModal, params, cover_pic, logo, linkModal, fetchFamilyList, selectedGroups, selectedGrpsID,
    group_category, spinLoader, search, values, linkParam,announcement_create,announcement_visibility } = createFamilyState;

  useEffect(() => {
    let cipher = window.location.pathname.split('edit-family/').pop();
    let groupId = decryption(cipher);
    let _params = params;
    if (groupId) {
      _params.group_id = groupId
    }
    if (groupId && !nextStage) {
      setcreateFamilyState((prev) => ({ ...prev, group_id: groupId, params: _params, spinLoader: true }));
      getFamilyDetails(groupId)
    }
  }, [current, groupList]);
  const getFamilyDetails = (groupId) => {
    getFamilDetails({ user_id: user_id, group_id: groupId }).then((res) => {
      let data = res.data.data[0]
      setcreateFamilyState((prev) => ({
        ...prev, values: data.base_region, location: data.base_region, group_category: data.group_category, familyName: data.group_name, familyIntro: data.intro,
        visibility: (data.visibility || false), searchable: data.searchable, is_linkable: data.is_linkable, member_joining: data.member_joining, member_approval: data.member_approval,
        post_create: data.post_create, post_visibilty: data.post_visibilty, post_approval: data.post_approval, link_family: data.link_family, link_approval: data.link_approval, cover_pic: data.cover_pic, logo: data.logo,
        spinLoader: false,announcement_create:data.announcement_create,announcement_visibility:data.announcement_visibility
      }));
    }).catch((err) => {
      setcreateFamilyState((prev) => ({ ...prev, spinLoader: false }))
    });
  }
  const handleChange = (name, e) => {
    e.persist()
    setcreateFamilyState(prev => ({ ...prev, [name]: e.target.value }))
  }
  const handleSelect = (name, event) => {
    setcreateFamilyState(prev => ({ ...prev, [name]: event }))
  }
  const handleAdvSelect = (name, event) => {
    if(name =='visibility') {
      let newVisibility = {
        member_joining: 3,
        post_visibilty:9,
        announcement_visibility:22,
        [name]:event
      }
      setcreateFamilyState(prev => ({ ...prev, ...newVisibility }));
    } else {
      setcreateFamilyState(prev => ({ ...prev, [name]: event }))
    }
  }
  const backToStage = () => {
    history.goBack();
  }
  const updateCreatedFamily = () => {
    const formData = new FormData();
    formData.append('id', group_id.toString());
    formData.append('group_name', familyName);
    formData.append('base_region', location);
    formData.append('user_id', user_id);
    formData.append('intro', familyIntro);
    formData.append('is_active', true);
    formData.append('searchable', searchable);
    formData.append('group_category', group_category);
    formData.append('visibility', visibility);
    formData.append('member_joining', member_joining);
    formData.append('member_approval', member_approval);
    formData.append('post_create', post_create);
    formData.append('post_approval', post_approval);
    formData.append('post_visibilty', post_visibilty);
    formData.append('link_family', link_family);
    formData.append('link_approval', link_approval);
    formData.append('is_linkable', is_linkable);
    formData.append('announcement_create',announcement_create)
    formData.append('announcement_visibility',announcement_visibility)
    updateFamily(formData).then((response) => {
      console.log(response)
      if (response && response.data && response.data.data && response.data.data[0]) {
        console.log(response, "rtrtrtrtr", linkParam)
        linkFamilyCreated(response)
        let groupId = encryption(response.data.data[0].id);
        history.push(`/familyview/${groupId}`)
      }
    }).catch((error) => {
      console.log(error)
    })
  }
  const linkFamilyCreated = (response) => {
    let _from = window.location.pathname.split('create-family-link/createAndLink/').pop()
    let u_id = user_id
    let _to = response.data.data[0].id
    let params_is = {
      from_group: _from,
      requested_by: u_id,
      to_group: [_to]
    }
    console.log(params_is)
    requestLinkFamily(params_is).then(res => {
      console.log(res)
    })
  }
  const openImageModal = (type) => {
    let paramValue = params;
    paramValue.editType = type
    setcreateFamilyState(prev => ({ ...prev, params: paramValue, imageModal: true, imageType: type }));
  }
  const handleOk = () => {
    setcreateFamilyState(prev => ({ ...prev, imageModal: false }));
  }
  const handleCancel = () => {
    setcreateFamilyState(prev => ({ ...prev, imageModal: false }));
  }
  const dataCallback = (response, type) => {
    if (response && response !== '') {
      setTimeout(() => {
        setcreateFamilyState((prev) => ({ ...prev, cover_pic: response.data.data[0].cover_pic, logo: response.data.data[0].logo, imageModal: false }))
      }, (3000))
    } else {
      setcreateFamilyState((prev) => ({ ...prev, imageModal: false }))
    }

  }
  const openShareFamilyList = () => {
    setcreateFamilyState((prev) => ({ ...prev, linkModal: true }));
    getAllLinkedFamily({ user_id: `${user_id}`, group_id: `${params.group_id}` }).then((response) => {
      let linkArray = response.data.data;
      setcreateFamilyState((prev) => ({ ...prev, fetchFamilyList: linkArray }));
    }).catch((err) => {
      console.log("error happened")
    })
  }
  const handleChooseFamilyList = (e, v) => {
    var selectedArray = selectedGroups;
    var selectedIds = selectedGrpsID;
    if (e.target.checked) {
      selectedArray.push({ id: v.id });
      selectedIds.push(v.id);
    } else {
      let index = selectedIds.indexOf(v.id);
      selectedArray.splice(index, 1);
      selectedIds.splice(index, 1);
    }
    setcreateFamilyState((prev) => ({ ...prev, selectedGroups: selectedArray, selectedGrpsID: selectedIds }));
  }
  const onClose = () => {
    setcreateFamilyState((prev) => ({ ...prev, linkModal: false }));
  }
  const sednRequestToFamily = () => {
    let _params = {
      to_group: selectedGrpsID,
      from_group: params.group_id,
      requested_by: user_id
    }
    requestLinkFamily(_params).then((res) => {
      setcreateFamilyState((prev) => ({ ...prev, linkModal: false }));
    }).then((err) => {

    })
  }
  const navigateToFamily = () => {
    if (group_id && group_id != '') {
      let groupId = encryption(group_id);
      history.push(`/familyview/${groupId}`)
    }
  }
  const handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
    setcreateFamilyState((prev) => ({ ...prev, search: "", values: geocodedPrediction.formatted_address }))
    setcreateFamilyState((prev) => ({ ...prev, location: geocodedPrediction.formatted_address }))
  }
  const handleSearchChange = (e) => {
    e.persist()
    setcreateFamilyState((prev) => ({ ...prev, search: e.target.value, values: e.target.value, location: e.target.value }))
  }
  // console.log(familyDetails,"###############")
  // console.log(process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + cover_pic)
  // console.log(process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + logo)
  return (
    <section className="CreateFamilheey f-page">
      <div className="f-clearfix f-body">
        {spinLoader &&
          <Spin style={{ 'padding': '25% 0 25% 50%' }}></Spin>}
        {!spinLoader && <div className="f-boxed">
          <Button className="f-create-form-back" onClick={() => navigateToFamily()} />
          <div className="f-clearfix f-create-form-control">
            <div className="f-create-form-head">
              <h3>Edit Family</h3>
            </div>
            <div className="f-create-form-body" >
              {/* Steps */}
              {/* Body */}
              <div className="f-modal-wiz-body">

                {/* Step 1 */}
                  <div className="f-modal-wiz-step">
                    <h4>Basic Settings</h4>
                    <div className="f-form-control">
                      <label>Family Name</label>
                      <Input id='familyName' placeholder="Eg: Nigeria womens group" value={familyName} onChange={(e) => handleChange('familyName', e)} />
                    </div>
                    <div className="f-form-control">
                      <label>Family Category</label>
                      <Select placeholder="Eg: Company" value={group_category} onChange={(e) => handleSelect('group_category', e)}>
                        <Option value="Regular">Regular</Option>
                        <Option value="Company">Company</Option>
                        <Option value="Individual firm">Individual firm</Option>
                        <Option value="Non Profit Organisation">Non Profit Organisation</Option>
                        <Option value="Institute">Institute</Option>
                        <Option value="Religious/Spiritual">Religious/Spiritual</Option>
                        <Option value="Community">Community</Option>
                        <Option value="Association">Association</Option>
                        <Option value="Team">Team</Option>
                        <Option value="Project">Project</Option>
                        <Option value="Branch or division">Branch or division</Option>
                      </Select>
                    </div>
                    <div className="f-form-control">
                      <label>Base Region or Location</label>
                      <ReactGoogleMapLoader
                        params={{
                          key: MY_API_KEY,
                          libraries: "places,geocode",
                        }}
                        render={googleMaps =>
                          googleMaps && (
                            <ReactGooglePlacesSuggest
                              googleMaps={googleMaps}
                              autocompletionRequest={{
                                input: search,
                              }}
                              onSelectSuggest={handleSelectSuggest}
                              textNoResults="My custom no results text" // null or "" if you want to disable the no results item
                              customRender={prediction => (
                                <div className="customWrapper">
                                  {prediction
                                    ? prediction.description
                                    : "My custom no results text"}
                                </div>
                              )}
                            >
                              <Input className="art-mat-style art-suffix art-sfx-location" placeholder="Location"
                                type="text"
                                value={values}
                                onChange={(e) => handleSearchChange(e)}
                              />
                            </ReactGooglePlacesSuggest>
                          )
                        }
                      />
                    </div>
                  </div>
                  <div className="f-modal-wiz-step">
                    {/* <h4>Family Introduction</h4> */}
                    <div className="f-form-control">
                      <label>A little bit about your family</label>
                      <TextArea placeholder="Eg: Nigeria womens group " rows={6} value={familyIntro} onChange={(e) => handleChange('familyIntro', e)} />
                      <span className="f-tip">250 Characters max</span>
                    </div>
                  </div>
                  <div className="f-modal-wiz-step" >
                    {/* <h4>Upload Photo</h4> */}
                    <div className="f-form-control">
                      <label>Please take a moment to upload your family cover pic as well as logo/ profile pic.</label>
                      <div className="f-clearfix">
                        <header className="f-upload-poster">
                          {<div className="f-poster">
                            {/* Edit Poster */}
                            <Button className="f-edit-poster-pic" onClick={() => openImageModal('coverpic')}><Icon type="camera" /></Button>
                            {/* <img src={require('../../../assets/images/demo/demo_poster.png')} /> */}
                            <div className="f-poster-default ">
                              <img className={cover_pic ? "full-width-img" : "no-class"} src={cover_pic ? (process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + cover_pic) : require('../../../assets/images/empty_img.png')} />
                              {!cover_pic ? <h4>Upload your family cover pic !</h4> : ''}
                              {!cover_pic ? <p>Size recommendation: 1044 X 400 px</p> : ''}
                            </div>
                            {/* Shadow */}
                            <span className="f-overlay" />
                          </div>}
                          {/* {cover_pic && <div className="f-poster">
                          <Button className="f-edit-poster-pic" onClick={() => openImageModal('coverpic')}><Icon type="camera" /></Button>
                            <img src={cover_pic ?  process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + cover_pic : require('../../../assets/images/empty_img.png')} />
                          </div>} */}
                          <div className="f-page-overview">
                            {!logo && <span className="f-display-pic">
                              {/* <img src={require('../../../assets/images/demo/demo_poster.png')} /> */}
                              <div className="f-logo-default">
                                <img src={logo ? process.env.REACT_APP_S3_BASE_URL + 'logo/' + logo : require('../../../assets/images/default.png')} />
                                {!cover_pic ? <h4>Logo/ Profile</h4> : ''}
                                {!logo ? <p>120 x 120 px</p> : ''}
                              </div>
                            </span>}
                            {logo &&
                              <span className="f-display-pic">
                                <img src={logo ? process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + logo : require('../../../assets/images/default.png')} />
                              </span>
                            }
                            <Button className="f-edit-family" onClick={() => openImageModal('logo')}><Icon type="camera" /></Button>
                            {/* Error Message */}
                            {/* <p className="f-error">Error message *</p> */}
                          </div>
                        </header>
                      </div>
                    </div>
                  </div>
                  <div className="f-modal-wiz-step" >
                    <div className="f-advanced">
                      {/* <h4>Family Settings</h4> */}
                      <div className="f-form-control f-wiz-switch">
                        <ul>
                          <li>
                            <h6>Make this family private</h6>
                            <Switch checked={visibility} onChange={(e) => handleAdvSelect('visibility', e)} />
                          </li>
                          <li className="f-inactive">
                            <h6>Make this group searchable</h6>
                            <Switch defaultChecked checked={searchable} onChange={(e) => handleAdvSelect('searchable', e)} />
                          </li>
                          <li>
                            <h6>Link with other families as well</h6>
                            <Switch checked={is_linkable} onClick={(e) => handleAdvSelect('is_linkable', e)} /> {is_linkable && <Button type="link" onClick={() => openShareFamilyList()}>Select</Button>}
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
              </div>

              {/* Footer */}
              <div className="f-modal-wiz-footer">
                {/* Back Button */}
                  <Button type="link" onClick={() => backToStage()}><Icon type="arrow-left" /> Back</Button>
                  <Button className="f-styled" style={{ minWidth: 150 }} onClick={() => updateCreatedFamily()}>Done</Button>

                {/* Step 2 Suggestion Buttons */}

                {/* Show if no family selected */}
                {/* <div className="f-clearfix f-not-selected">
                    <Button className="f-styled">Continue <Icon type="arrow-right" /></Button>
                  </div> */}

                {/* Show if any family selected */}
                {/* <div className="f-clearfix f-if-selected">
                    <Button className="f-lined">Reset</Button>
                    <Button className="f-styled">Confirm</Button>
                  </div> */}

                {/* Step 2 Suggestion Buttons - END */}

              </div>

            </div>
          </div>
        </div>}
      </div>
      <Modal
        title="Add/Edit Image"
        visible={imageModal}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        className="f-modal f-modal-wizard"
      >
        <CustomImageCrop params={params} callBackimgFromParent={dataCallback}></CustomImageCrop>
      </Modal>
      <Modal
        title="Link Other Families"
        visible={linkModal}
        // onOk={handleOk}
        onCancel={() => onClose()}
        // footer={null}
        className="f-modal f-modal-wizard"
      >
        <div className="f-modal-wiz-body" >
          <div className="f-member-listing-cards f-share-scroll">
            {fetchFamilyList && fetchFamilyList.length > 0 &&
              fetchFamilyList.map((item, index) => {
                return <Card key={index} className="f-member-cardx">
                  <div className="f-start">
                    <div className="f-left">
                      <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/demo/demo-fam1.png')} />
                    </div>
                    <div className="f-right">
                      <h4>{item.group_name}</h4>
                      <h6>By <strong>{item.created_by}</strong></h6>
                      <h6>Member since {item.member_since ? item.member_since : ''}</h6>
                      <h6 className="f-pin">{item.base_region}</h6>
                    </div>
                  </div>
                  <div className="f-stop">
                    <h5>
                    </h5>
                    <div className="f-right-combo">
                      {(item.is_linked === 'pending' || item.is_linked === 'accepted') &&
                        <Checkbox>
                          {item.is_linked}
                        </Checkbox>
                      }
                      {(item.is_linked !== 'pending' && item.is_linked !== 'accepted') &&
                        <Checkbox onChange={(e) => handleChooseFamilyList(e, item)}>
                          {selectedGrpsID.indexOf(item.id) == -1 ? 'Select' : 'Selected'}
                        </Checkbox>
                      }
                    </div>
                  </div>
                </Card>
              })
            }
          </div>
        </div>
        <div className="f-modal-wizard f-modal-wfooter">
          <Button className="f-fill" onClick={() => sednRequestToFamily()}>Done</Button>
          <Button style={{ marginLeft: '15px' }} onClick={() => onClose()}>Cancel</Button>
        </div>
      </Modal>
    </section>
  )
}
export default EditMyFamily;