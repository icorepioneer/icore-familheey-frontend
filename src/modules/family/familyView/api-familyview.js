import axios from '../../../services/apiCall';
const getFamilDetails = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getFamilyById`, data)

}
const fetchFamilyDetails = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/fetch_link_family`, data)
}
const requestLinkFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/requestToLinkFamily`, data)
}
const listLinkedFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/list_linked_family`, data)
}
const inviteOthers = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/event_shareor_invite`, data)
}
const listGroupFolders = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listGroupFolders`, data)
}
const adminRequestAction = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/admin_request_action`, data)
}
const familyLinkExist = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/family_link_exist`, data)
}
const addRelation = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/addRelation`, data)
}
const updateRelation = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/updateRelation`, data)
}
const updateGroupMaps = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_groupmaps`, data)
}
const pendingRequest = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/pending_requests`, data)
}
const deleteRequest = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/delete_pending_requests`, data)
}
const listAllMemberShipTypes = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/membership_dashboard`, data)
}

const saveMemberShipType = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/add_membership_lookup`, data)
}

const updateMemberShipType = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_membership_lookup`, data)
}

const getMembershiptypeById = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getMembershiptypeById`, data)
}

const GetListLookup = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/list_membership_lookup`, data)
}

const AddUserToType = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/user_membership_update`, data)
}

const sendReminder = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/Membership_reminder`, data)
}

const showMshipPeriod = (data)=> {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_membership_lookup_periods`, data)
}

const stripeAuthLinkGenerate = (data)=> {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/stripe_oauth_link_generation`, data)
}

const getInvitedUsersList = (data)=> {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_other_invited_users_by_group`, data)
}

export {
  getFamilDetails,
  fetchFamilyDetails,
  requestLinkFamily,
  listLinkedFamily,
  listGroupFolders,
  adminRequestAction,
  familyLinkExist,
  addRelation,
  updateGroupMaps,
  pendingRequest,
  deleteRequest,
  updateRelation,
  listAllMemberShipTypes,
  saveMemberShipType,
  getMembershiptypeById,
  updateMemberShipType,
  GetListLookup,
  AddUserToType,
  sendReminder,
  showMshipPeriod,
  stripeAuthLinkGenerate,
  getInvitedUsersList
} 