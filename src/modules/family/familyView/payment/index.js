import React, { useState } from 'react'
import { Modal, Button } from 'antd'
const CreatePaymentInstantWindow = () => {
    const [state, setState] = useState({
        loading: false,
        marginLeft:`26%`
    })
    let { loading,marginLeft } = state
    const generateButtonClick=()=>{
        setState(prev=>({...prev,loading:true,marginLeft:`33%`}))
    }
    return (
        <div >
            <Button
                onClick={generateButtonClick}
                loading={loading}
                style={{ marginLeft: `${marginLeft}` }}
                >{loading?`Generating....`:`Generate new payment tag`}</Button>
        </div>
    )
}

const PaymentComp = ({ visiblepaymentmodal }) => {
    // const genrateUrl=()=>{
    //     let params = {
    //         group_id:'3'
    //     }
    // }
    return (
        <React.Fragment>
            <Modal
                title="Create payment mod"
                visible={visiblepaymentmodal}
                footer={[]}
            >
                <CreatePaymentInstantWindow />
            </Modal>
        </React.Fragment>
    )
}

export default PaymentComp