import React from 'react'
import { Form,Input,Select,Button,DatePicker } from 'antd'
import * as moment from 'moment'
const { TextArea } = Input;
const { Option } = Select;
const EditmemberShip = ({
    setValmfrom,
    AddUserToPlan,
    getFieldDecorator,
    membershipData,
    changeDrop,
    memberShipTypes,
    setValduration,
    periodSubLookup
}) => {
    let { 
        membership_id,
        membership_duration,
        membership_payment_status,
        membership_fees,
        membership_from,
        membership_to,
        membership_paid_on,
        membership_payment_notes,
        membership_period_type,
        membership_total_payed_amount
     } = membershipData;
     const dueAmount = parseInt(membership_fees) - parseInt(membership_total_payed_amount ? membership_total_payed_amount : 0)
     return (
        <div className="f-switch-content" >
            <Form className="f-form-layout" onSubmit={(e) => AddUserToPlan(e)}>
                <div className="f-form-control">
                    <label>Membership Type*</label>
                    <Form.Item>
                        {getFieldDecorator('membership_id', {
                            initialValue: membership_id ? membership_id : '',
                            rules: [{
                                required: true,
                                message: "Membership Type is mandatory."
                            }]
                        })(
                            <Select 
                                style={{ width: '100%' }}
                                placeholder="Select" 
                                onChange={(val) => changeDrop(val)}
                            >
                                {memberShipTypes['ListLookup'] && memberShipTypes['ListLookup'].map((item, index) => {
                                    return (<Option key={index} value={item.id}>{item.membership_name}</Option>)
                                })
                                }
                            </Select>
                        )}
                    </Form.Item>
                </div>
                <div className="f-form-control">
                    <label>Duration*</label>
                    <Form.Item>
                        {getFieldDecorator('membership_duration', {
                            initialValue: membership_duration ? membership_duration : '',
                            rules: [{
                                required: true,
                                message: "Duration is mandatory."
                            }]
                        })(
                            <Select style={{ width: '100%' }} 
                            placeholder="Select" 
                            onChange={(val) => setValduration(val)}>
                                {
                                    periodSubLookup&&periodSubLookup.map((v, i) => {
                                        return (<Option key={i} value={v.type_value}>{v.type_name}</Option>)
                                    })
                                }
                            </Select>
                        )}
                    </Form.Item>
                </div>
                <div className="f-form-control">
                    <label>Payment Status*</label>
                    <Form.Item>
                        {getFieldDecorator('membership_payment_status', {
                            initialValue: membership_payment_status==='success'?'Completed':membership_payment_status,
                            rules: [{
                                required: true,
                                message: "Payment Status is mandatory."
                            }]
                        })(
                            <Select >
                                <Option value="Completed">Completed</Option>
                                <Option value="Partial">Partial</Option>
                                <Option value="Pending">Pending</Option>
                            </Select>
                        )}
                    </Form.Item>
                </div>
                <div className="f-form-control">
                    <label>Membership Fees*</label>
                    <Form.Item>
                        {getFieldDecorator("membership_fees", {
                            initialValue: membership_fees ? membership_fees : "",
                            rules: [{
                                required: true,
                                message: "Membership Fees is mandatory."
                            }]
                        })(<Input type="number" placeholder="Eg: 1000" />)}
                    </Form.Item>
                    <label>Due Amount : { dueAmount > 0 ? dueAmount : 0 } </label>
                </div>
                <div className="f-form-control">
                    <label>Membership From*</label>
                    <Form.Item>
                        {getFieldDecorator("membership_from", {
                            initialValue: membership_from ? moment(moment.unix(membership_from).format('MMM/DD/YYYY')) : moment(),
                            rules: [{
                                required: true,
                                message: "Membership From is mandatory."
                            }]
                        })(<DatePicker format={'MMM/DD/YYYY'} onChange={(val) => setValmfrom(val)} />)}
                    </Form.Item>
                </div>
                <div className="f-form-control">
                    <label>Membership To</label>
                    <Form.Item>
                        {getFieldDecorator("membership_to", {
                            initialValue: (membership_period_type==='Life time') ?"Life time":(membership_to?moment.unix(membership_to).format('MMM/DD/YYYY'):null)})(<Input type="text" readOnly disabled />)}
                    </Form.Item>
                </div>
                {/* <div className="f-form-control">
                    <label>Paid On*</label>
                    <Form.Item>
                        {getFieldDecorator("membership_paid_on", {
                            initialValue: membership_paid_on ? moment(moment.unix( membership_paid_on).format('MMM/DD/YYYY')) : moment(),
                            rules: [{
                                required: true,
                                message: "Paid On date is mandatory."
                            }]
                        })(<DatePicker format={'MMM/DD/YYYY'} />)}
                    </Form.Item>
                </div> */}
                <div className="f-form-control">
                    <label>Notes</label>
                    <Form.Item>
                        {getFieldDecorator("membership_payment_notes", {
                            initialValue:  membership_payment_notes ?  membership_payment_notes : "",
                        })(<TextArea rows={5} placeholder="Notes" />)}
                    </Form.Item>
                </div>
                <div className="f-form-control">
                    <Button htmlType="submit" className="f-button">Save Changes</Button>
                </div>
            </Form>
        </div>
    )
}

export default EditmemberShip