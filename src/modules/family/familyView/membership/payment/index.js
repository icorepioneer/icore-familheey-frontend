import React, { useState, useEffect } from 'react'
import { Modal, Button, Table, Input, Form } from 'antd'
import * as moment from 'moment'
import { chekActiveORNot, memberOfflinePayment } from '../../../../RequestForHelp/api'
import StripeComp from '../../../../../components/Stripe/stripe'
import { getClientSecret } from '../../../../../components/Stripe/stripe-api'
import ThankYOU from '../../../../../components/ThankYouModal'

const CreateForm = Form.create({ name: 'due_form' })(
    class extends React.Component {
        render() {
            let { form, defaultPaymentAmount } = this.props
            const { getFieldDecorator } = form
            const validateNumber = (rule, value, callback) => {
                if (parseFloat(value) <= 0) {
                    return callback('Please provide a value which is greater than zero')
                }
                else if (!Number.isInteger(parseFloat(value))) {
                    return callback('Please provide a whole number value which is not zero')
                }
                else if (parseFloat(value) > defaultPaymentAmount) {
                    return callback('value should be less than due amount')
                }
                callback()
            }
            return (
                <div style={{ paddingBottom: '0px' }}>
                    <Form >
                        <Form.Item>
                            {getFieldDecorator('due_amount', { rules: [{ validator: validateNumber }],initialValue: defaultPaymentAmount })(
                                <Input
                                    type="number"
                                    addonBefore="Due amount"
                                    placeholder="Due amount" />
                            )}
                        </Form.Item>
                    </Form>
                </div>
            )
        }
    }
)

const PaymentStatusModal = ({ pMVisible, toggle, familyDetails, user_id, callback }) => {
    let formReff
    const [state, setState] = useState({
        modalVisible: false,
        membership_type: undefined,
        membership_payment_status: undefined,
        membership_period_type: undefined,
        membership_from: undefined,
        membership_to: undefined,
        membership_fees: 0,
        stripe_account_id: undefined,
        id: undefined,
        payment_button_disabled: false,
        stripe_valid: undefined,
        stripeVisible: undefined,
        MembPayLaterObj: undefined,
        group_map_id: undefined,
        thankyou: false,
        membership_total_payed_amount: undefined,
        defaultPaymentAmount: undefined,
        membership_payment_notes:undefined
    })
    let { modalVisible,
        membership_type,
        id,
        //payment_button_disabled,
        membership_payment_status,
        membership_period_type,
        membership_from,
        membership_to,
        membership_fees,
        //stripe_account_id,
        stripe_valid,
        stripeVisible,
        MembPayLaterObj,
        group_map_id,
        thankyou,
        membership_total_payed_amount,
        defaultPaymentAmount,
        membership_payment_notes
    } = state

    useEffect(() => {
        setState(prev => ({ ...prev, modalVisible: pMVisible }))
    }, [pMVisible])

    const handleOk = () => {
        setState(prev => ({ ...prev, pMVisible: !pMVisible }))
    }

    // const handleCancel = () => {
    //     setState(prev => ({ ...prev, pMVisible: !pMVisible }))
    // }
    useEffect(() => {
        let { membership_payment_status:mps,
            stripe_account_id:sai,
            id:id_,
            membership_type:mty,
            membership_period_type:mpt,
            membership_from:mf,
            membership_to:mt,
            membership_fees:mfe,
            group_map_id:gm_id,
            membership_total_payed_amount:memb_t_amnt,membership_payment_notes:memb_pay_note } = familyDetails
        setState(prev => ({
            ...prev,
            membership_payment_status: mps,
            membership_type: mty,
            membership_period_type: mpt,
            membership_from: mf,
            membership_to: mt,
            membership_fees: mfe,
            stripe_account_id: sai,
            id: id_,
            membership_payment_notes:memb_pay_note,
            group_map_id: gm_id,
            membership_total_payed_amount: memb_t_amnt,
            defaultPaymentAmount: mfe - memb_t_amnt
        }))
    }, [familyDetails])


    const columns = [
        {
            title: '',
            dataIndex: 'name',
        },
        {
            title: '',
            dataIndex: 'value',
        },
    ];

    const data = [
        {
            key: '1',
            name: 'Membership Type',
            value: `${membership_type}`,
        },
        {
            key: '2',
            name: 'Membership Duration',
            value: `${membership_period_type}`,
        },
        {
            key: '3',
            name: 'Starts From',
            value: `${moment.unix(membership_from).format('MMM DD YYYY')}`,
        },
        {
            key: '4',
            name: 'Valid Till',
            value: `${moment.unix(membership_to).format('MMM DD YYYY')}`,
        }, {
            key: '5',
            name: 'Membership Fees',
            value: `${membership_fees}`,
        },
        {
            key: '6',
            name: 'Paid Amount',
            value: `${membership_total_payed_amount}`,
        },
        {
            key: '6',
            name: 'Payment Note',
            value: `${membership_payment_notes}`,
        },
        
    ];
    const validateStripAccount = () => {
        let params = {
            group_id: `${id}`
        }
        const { form } = formReff.props
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            let { due_amount } = values
            setState(prev => ({ ...prev, defaultPaymentAmount: due_amount }))
            chekActiveORNot(params).then(res => {
                setState(prev => ({ ...prev, stripe_valid: true }))
            }).catch(err1 => {
                setState(prev => ({ ...prev, stripe_valid: false }))
            })
            let new_obj = {
                ...MembPayLaterObj,
                membership_fees: due_amount,
                contributionId: group_map_id,
                stripe_valid: stripe_valid
            }
            toggle()
            form.resetFields()
            setState(prev => ({ ...prev, stripeVisible: true, MembPayLaterObj: new_obj }))
        })
    }

    useEffect(() => {
        if (stripe_valid) {
            if (stripe_valid) {
                let params = {
                    user_id: `${user_id}`,
                    to_type: "membership",
                    to_type_id: `${group_map_id}`,
                    to_subtype: "",
                    to_subtype_id: "",
                    group_id: `${id}`,
                    contributionId: `${group_map_id}`,
                    amount: defaultPaymentAmount * 100
                }
                getClientSecret(params).then(res => {
                    let new_obj_ = {
                        ...MembPayLaterObj,
                        ...res.data,
                        membership_fees: defaultPaymentAmount,
                        contributionId: group_map_id,
                        stripe_valid: stripe_valid
                    }
                    setState(prev => ({ ...prev, stripeVisible: true, MembPayLaterObj: new_obj_ }))
                })

            }
            else {
                let new_obj = {
                    ...MembPayLaterObj,
                    membership_fees: defaultPaymentAmount,
                    contributionId: group_map_id,
                    stripe_valid: stripe_valid
                }
                setState(prev => ({ ...prev, stripeVisible: true, MembPayLaterObj: new_obj }))
            }
        }
    }, [stripe_valid])
    const renderButtonGroup = () => {
        switch (membership_payment_status) {
            case 'Pending':
            case 'Partial':
                return (
                    <div key="pay" style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Button onClick={() => validateStripAccount()}>PAY NOW</Button>
                        <Button onClick={() => toggle()}>CLOSE</Button>
                    </div>
                )
            default:
                return (
                    <div key="pay" style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Button onClick={() => toggle()}>CLOSE</Button>
                    </div>
                )

        }
    }

    const toggleStripeModal = () => {
        setState(prev => ({ ...prev, stripeVisible: false }))
    }

    const ty_handleCancel = () => {
        setState((prev) => ({
            ...prev,
            thankyou: false
        }))
    };
    const ty_handleOk = () => {
        setState((prev) => ({
            ...prev,
            thankyou: false
        }))
    };

    const showThanYou = () => {
        setState(prev => ({ ...prev, thankyou: true,stripe_valid:undefined,MembPayLaterObj:undefined,defaultPaymentAmount:undefined }))
        setTimeout(() => { callback() }, 3000)
    }

    const saveFormRef = formRef => {
        formReff = formRef;
    };
    return (
        <React.Fragment>
            <Modal
                // title="Basic Modal"
                visible={modalVisible}
                onOk={handleOk}
                // onCancel={toggle}
                closable={false}
                footer={[renderButtonGroup()]}
                style={{ textAlign: 'center' }}
            >
                <Table
                    style={{ marginTop: '15px' }}
                    columns={columns}
                    dataSource={data}
                    bordered
                    pagination={false}
                    footer={() =>
                        <CreateForm
                            wrappedComponentRef={saveFormRef}
                            defaultPaymentAmount={defaultPaymentAmount}
                        />
                    }
                />
            </Modal>
            <StripeComp
                modalVisible={stripeVisible}
                toggleStripeModal={toggleStripeModal}
                post_request_id={null}
                reqDetails={undefined}
                current_selected_item_id={null}
                handleChange={() => { }}
                showThanYou={() => { showThanYou() }}
                MembPayLaterObj={MembPayLaterObj}
                memberOfflinePayment={memberOfflinePayment}
                callback={showThanYou}
                showThanYou={showThanYou}
            />
            <ThankYOU
                member={true}
                thankyou={thankyou}
                ty_handleCancel={ty_handleCancel}
                ty_handleOk={ty_handleOk}
                callNow={ty_handleOk}
            />
        </React.Fragment>
    )
}

export default PaymentStatusModal