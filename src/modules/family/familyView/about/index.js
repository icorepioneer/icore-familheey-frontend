import React, { useState, useEffect } from 'react'
import { Button, Input, Carousel, Upload, Icon, Dropdown, Menu } from 'antd';
import Lightbox from 'react-image-lightbox';
import { updateFamilyJSON } from '../../../createFamily/api-family'
import { notifications } from '../../../../shared/toasterMessage'
import SocialMediaShare from '../../../../components/PostsCard/SocialMedia';
import './style.scss'
import { useSelector } from 'react-redux';
const { TextArea } = Input

const About = ({
    history,
    _familyDetails,
    familyLinkExist,
    handleChange,
    setfamilyViewState,
    url,
    famLinkValue,
    user_id,
    getFamilyDetails
}) => {
    const [state, setState] = useState({
        familyDetails: undefined,
        _images: [],
        _fileList: [],
        _intro_edit_flag: false,
        _history_edit_flag: false,
        _link_edit_flag: false,
        _image_view: false,
        image_upload_status: false,
        photoIndex: 0,
        famLinkValue: '',
        modalVisible: false,
        modalShareVisible: false,
        Authtoken: useSelector(stateA => stateA.user_details.token),
        logined_user_id: useSelector(stateX => stateX.user_details.userId),
    })
    let { _images, _fileList, _intro_edit_flag, _history_edit_flag, _image_view, _link_edit_flag, image_upload_status, familyDetails, photoIndex, modalVisible, modalShareVisible, Authtoken, logined_user_id } = state;
    const store = useSelector(stateY => stateY.user_details)
    useEffect(() => {
        setState(prev => ({ ...prev, Authtoken: store.accessToken }))
    }, [store.token])
    const changeEdit = (flag,value_flag) => {
        setState(prev => ({ ...prev, [flag]: !state[flag] }))
        handleChange(value_flag, { target: { value: '' } })
    }
    const changeImagePreview = (current, index) => {
        setState(prev => ({ ...prev, _image_view: !_image_view, photoIndex: index }))
    }
    useEffect(() => {
        let { history_images } = _familyDetails;
        let images = [], fileList = [];
        history_images = history_images ? history_images : [];
        history_images.forEach((item, i) => {
            images.push(process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'history_images/' + item.filename)
            fileList.push({
                uid: i,
                filename: item.filename,
                type: item.type,
                url: process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'history_images/' + item.filename,
            })
        })
        setState(prev => ({ ...prev, _images: images, _fileList: fileList, familyDetails: _familyDetails }))
        return () => { _familyDetails = null }
    }, [_familyDetails])
    const handleRemoveChange = (file) => {
        let fileList = _fileList.filter((item => {
            return item.filename !== file.filename
        }));
        setState((prev) => ({ ...prev, _fileList: fileList }));
    }
    const saveIntro = () => {
        setState((prev) => ({ ...prev, _intro_edit_flag: false, _history_edit_flag: false, _link_edit_flag: false }));
        let { id, intro, history_text } = familyDetails;
        let params = {
            intro: intro,
            id: id.toString(),
            user_id: user_id,
            history_text: history_text,
            history_images: _fileList,
            f_text: famLinkValue
        }
        let { user_status } = familyDetails;
        updateFamilyJSON(params).then(res => {
            getFamilyDetails({ group_id: id, user_id: user_id })
            setState(prev => ({ ...prev, familyDetails: { ...res.data.data[0], user_status: user_status } }))
        }).catch(err => {
            notifications('error', 'Error', 'Error happened,please try again...')
        })
    }
    const responsefromdb = (res) => {
        let fileList = _fileList
        fileList.push({
            uid: _fileList.length,
            filename: res.data[0].filename,
            type: res.data[0].type,
            url: process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'history_images/' + res.data[0].filename
        })
        setState((prev) => ({ ...prev, _fileList: fileList, image_upload_status: false }));
    }

    const checkLink = () => {
        familyLinkExist({ f_text: famLinkValue }).then((res) => {
            if (res && res.data && res.data.count === 0) {
                let _family_details = familyDetails;
                _family_details.f_link = familyDetails.f_link.split('/').splice(0, familyDetails.f_link.split('/').length - 1).toString().replace(/,/g, '/') + `/${famLinkValue}`
                setfamilyViewState((prev) => ({ ...prev, familyDetails: _family_details }));
                saveIntro()
            } else {
                notifications('error', 'Error', 'link already exist')
            }
        })
    }
    const beforeUpload = () => {
        setState(prev => ({ ...prev, image_upload_status: true }))
    }
    const menu = () => {
        return <Menu onClick={e => handleMenuClick(e)}>
            <Menu.Item key="1">
                Share in Familheey
            </Menu.Item>
            <Menu.Item key="2">
                Share in Social Media
            </Menu.Item>
        </Menu>
    }

    const handleMenuClick = (e) => {
        switch (e.key) {
            case "1":
                history.push('/post/create', { path_: window.location.pathname, type_: 'post', description: familyDetails.f_link });
                break;
            case "2":
                setState((prev) => ({ ...prev, modalVisible: true, modalShareVisible: true }));
                break;
        }
    }
    const showSocialModal = () => {
        setState(prev => ({ ...prev, modalShareVisible: !modalShareVisible, modalVisible: !modalVisible }));
    }
    return (
        <React.Fragment>
            <div className="f-event-contents main-div">
                <div className="f-switch-content" >
                    <div className="f-title">
                        <h3>Introduction
                            {familyDetails && familyDetails.user_status === 'admin'
                                && !_intro_edit_flag &&
                                <Button
                                    onClick={() => changeEdit('_intro_edit_flag')}
                                    className="f-title-edit"
                                />}</h3>
                    </div>
                    {!_intro_edit_flag && <div className="f-content">
                        <p>{familyDetails && familyDetails.intro}</p>
                    </div>}
                    {_intro_edit_flag && <div className="f-content">
                        <div className="f-form-control">
                            <TextArea rows={4} value={familyDetails && familyDetails.intro} onChange={(e) => handleChange('intro', e)}></TextArea>
                        </div>
                        <div className="f-form-control">
                            <Button className="f-button" onClick={() => saveIntro()}>Save</Button>
                            <Button className="f-button f-line" onClick={() => changeEdit('_intro_edit_flag','intro')}>Cancel</Button>
                        </div>
                    </div>}
                    <div className="f-title">
                        <h3>History {familyDetails && familyDetails.user_status === 'admin' && !_history_edit_flag &&
                            <Button
                                onClick={() => changeEdit('_history_edit_flag')}
                                className="f-title-edit"
                            />}</h3>
                    </div>
                    <div className="f-content">
                        {!_history_edit_flag &&
                            <div className="f-about-carousel">
                                <Carousel>
                                    {_images.length > 0 &&
                                        _images.map((item, index) => {
                                            return <div key={index}>
                                                <img onClick={() => changeImagePreview(item, index)} src={item} />
                                            </div>
                                        })
                                    }

                                </Carousel>
                                {_image_view && _images.length > 1 && (
                                    <Lightbox
                                        mainSrc={_images[photoIndex]}
                                        nextSrc={_images[(photoIndex + 1) % _images.length]}
                                        prevSrc={_images[(photoIndex + _images.length - 1) % _images.length]}
                                        onCloseRequest={() => changeImagePreview()}
                                        onMovePrevRequest={() =>
                                            setState((prev) => ({
                                                ...prev, photoIndex: (photoIndex + _images.length - 1) % _images.length,
                                            }))
                                        }
                                        onMoveNextRequest={() =>
                                            setState((prev) => ({
                                                ...prev, photoIndex: (photoIndex + 1) % _images.length,
                                            }))
                                        }
                                    />
                                )}
                                {_image_view && _images.length === 1 && (
                                    <Lightbox
                                        mainSrc={_images[photoIndex]}
                                    />
                                )}
                            </div>}
                        {!_history_edit_flag &&
                            <p>{familyDetails && familyDetails.history_text}</p>}
                        {_history_edit_flag &&
                            <div className="f-clearfix">
                                <div className="f-form-control">
                                    <Upload
                                        beforeUpload={beforeUpload}
                                        headers={{
                                            Authorization: `Bearer ${Authtoken}`,
                                            logined_user_id: `${logined_user_id}`
                                        }}
                                        action={url}
                                        listType='picture'
                                        defaultFileList={[..._fileList]}
                                        onSuccess={responsefromdb}
                                        onRemove={(file) => handleRemoveChange(file)}
                                    >
                                        <Icon type="upload" /> Upload
                                                                </Upload>
                                </div>
                                <div className="f-form-control">
                                    <TextArea
                                        rows={4}
                                        value={familyDetails && familyDetails.history_text}
                                        onChange={(e) => handleChange('history_text', e)}
                                    />
                                </div>
                                <div className="f-form-control">
                                    <Button
                                        loading={image_upload_status}
                                        className="f-button"
                                        onClick={() => saveIntro()}>
                                        {image_upload_status ? 'uploading' : 'Save'}
                                    </Button>
                                    <Button
                                        className="f-button f-line"
                                        onClick={() => changeEdit('_history_edit_flag','history_text')}>
                                        Cancel
                                        </Button>
                                </div>
                            </div>}
                    </div>
                    <div className="f-title">
                        <h3>Family Url {familyDetails && familyDetails.user_status === 'admin' && !_link_edit_flag && <Button onClick={() => changeEdit('_link_edit_flag')} className="f-title-edit" />}</h3>
                    </div>
                    {!_link_edit_flag &&
                        <div>{familyDetails && familyDetails.f_link}
                            <Dropdown overlay={() => menu()}>
                                <Button className="f-btn-share" />
                            </Dropdown>
                        </div>
                    }
                    {_link_edit_flag &&
                        <div className="f-clearfix">
                            <div className="f-form-control">
                                <label>{familyDetails && familyDetails.f_link.split('/').splice(0, familyDetails && familyDetails.f_link.split('/').length - 1).toString().replace(/,/g, '/')}</label>
                                <Input value={famLinkValue} onChange={(e) => handleChange('famLinkValue', e)} />
                            </div>
                            <div className="f-form-control">
                                <Button className="f-button" onClick={() => checkLink()}>Save</Button>
                                <Button className="f-button f-line" onClick={() => changeEdit('_link_edit_flag')}>Cancel</Button>
                            </div>
                        </div>
                    }
                </div>
                <SocialMediaShare
                    modalVisible={modalVisible}
                    post_id={{ url: _familyDetails.f_link }}
                    showSocialModal={showSocialModal} />


            </div>
        </React.Fragment>)
}
export default About;