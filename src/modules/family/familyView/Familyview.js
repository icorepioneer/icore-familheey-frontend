import React, { useState, useEffect, useRef } from 'react';
import { Modal, Button, Card, Popover, message, Input, Radio, Dropdown, Menu, Upload, Popconfirm, Select, Checkbox, notification, Spin, Result, Form, Switch, Tag } from "antd";
import { Icon } from 'antd'
import {
    getFamilDetails,
    fetchFamilyDetails,
    listLinkedFamily,
    //inviteOthers,
    listGroupFolders,
    adminRequestAction,
    familyLinkExist,
    addRelation,
    updateGroupMaps,
    pendingRequest,
    deleteRequest,
    updateRelation,
    listAllMemberShipTypes,
    saveMemberShipType,
    updateMemberShipType,
    getMembershiptypeById,
    GetListLookup,
    AddUserToType,
    sendReminder,
    showMshipPeriod,
    getInvitedUsersList
} from './api-familyview.js'
import './style.scss';
// import MemberListing from '../../memberListing/memberListing.js';
import CustomImageCrop from '../../ImageCrop/index.js';
import { withHooksHOC } from '../../../shared/propsfunctoin';
import FamilyDetailsSidebar from '../../../components/FamilyDetailsSidebar/index.js';
import { groupEvents } from '../../events/eventListing/api-eventList';
import { decryption, encryption } from '../../../shared/encryptDecrypt'
import { postList } from '../../Post/post_service';
import PostsCard from '../../../components/PostsCard';
import {
    listAllMembers,
    listAllGroupMember,
    listAllGroupRequests,
    getAllBlockedUsers,
    addMemberToGroup,
    memberUpdateGroupmaps,
    getallRelations,
    leaveFamily
} from '../../memberListing/api-memberlist';
import EventCard from '../../../components/EventCard'
import Lightbox from 'react-image-lightbox';
import { feedBackSubmit } from '../../Post/post_service';
import countries from '../../../common/countrylist.json';
import {
    inviteViaSms,
    requestLinkFamily,
    actionToLinkedFamily,
    updateFamily,
    // updateFamilyJSON,
    unfollow,
    follow,
    joinToExistingFamily,
    bulkUpload,
    linkRequestStatus
} from '../../createFamily/api-family';
import Calender from '../../calender/Calender';
import { notifications } from '../../../shared/toasterMessage.js';
import { deleteAlbum, createAlbum } from '../../AlbumModal/album-api'
import AlbumCard from '../../../components/AlbumCard/index';
import CreateAlbum from '../../AlbumModal/createAlbum.js';
import { _listEventFolders, removeFolder } from './../../events/eventDetails/api-eventDetails';
import Documents from '../../events/components/Documents/index.js';
import { globalSearchResult } from '../../../components/SearchResultPage/api-searchResult';
import About from './about/index'
import EventCreate from '../../events/components/CreateEvent/index'
import RequestList from '../../RequestList/index'
import currency from '../../../assets/uploads/currency.json';
import { useSelector } from 'react-redux'
import PaymentModal from './payment/index'
import { isNull } from 'util';
import { generateLink } from '../../../modules/RequestForHelp/api/index'
import EditmemberShip from './membership/edit'
import PaymentStatusModal from './membership/payment/index'
import { paymentHistoryByUserid, paymentHistoryByGroupid } from '../../users/userProfile/api-profile.js';
import PaymentHistory from '../../../components/PaymentHistory/index'
import Sticky from '../../Sticky-component/index.js';
import TableInvited from '../../../components/invitedUsersList'
import { chekActiveORNot } from '../../RequestForHelp/api'
const { Search, TextArea } = Input;
const { confirm } = Modal;
const { Option } = Select;
const props = {
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    listType: 'picture',
}

var moment = require('moment');
// interface IHooksHOCProps {
//     configObj: Array;
// }

const FamilyView = ({ history, form }) => {
    const { getFieldDecorator, validateFields } = form;
    let redux_state = useSelector(state => state.user_details)
    const [familyViewState, setfamilyViewState] = useState({
        user_id: useSelector(red_state => red_state.user_details.userId),
        sub_item_id: '10',
        familyDetails: '',
        loader: true,
        postLists: [],
        fetchlinkfamily: '',
        showmodal: false,
        addMemberList: [],
        cover_edit: true,
        fetchFamilyInfo: [],
        groupsEvents: [],
        params: {
            crop: {
                unit: "%",
                aspect: 261 / 100,
                // height : 100,
                width: 50
            },
            group_id: '',
            editType: ''
        },
        enableEdit: false,
        familyPropValue: {
            familyDetails: '',
            count: '',
            group_id: '',
            callbackFn: {}
        },
        tabloader: false,
        groupMemberList: '',
        groupRequestList: [],
        blockedList: [],
        memberListDetails: '',
        selectedItem: '',
        invite: 'people',
        linkedFamilyList: [],
        isOpen: false,
        photoIndex: 0,
        images: [],
        feedbackModel: false,
        description: '',
        title: '',
        image: '',
        msgTitle: '',
        countryList: countries.countries,
        // selectValue: '+91',
        email: '',
        phone: '',
        name: '',
        selectedGroups: [],
        selectedGrpsID: [],
        selectFamilyModal: false,
        count: '',
        al_visible: false,
        dc_visible: false,
        dcup_visible: false,
        bulk_visible: false,
        introEdit: false,
        historyEdit: false,
        fileList: [],
        previewVisible: false,
        previewImage: '',
        roleRelationArray: [],
        roleRelation: false,
        selectRelationRole: '',
        linkRequestArray: [],
        LinkEdit: false,
        folderObj: {},
        famLinkValue: '', listEnable: false, listInputValue: '', selectedId: '', groupAlbums: [], groupDocuments: [], initial: '10',
        openCoverPic: false,
        openProfilePic: false,
        item_to_edit: undefined,
        searchTag: '',
        _fileList: [],
        visibleleaveModal: true,
        isGroupAdmin: false,
        pendingReq: [],
        button_loading: false,
        relationExist: false,
        existingRelationId: '',
        modalvisible: false,
        selectValue: 'United States/+1',
        csvUploadingStatus: false,
        csvUploadResponse: '',
        csvUploadResponseMessage: '',
        ToastModal: false,
        ToastModal_status: 'success',
        ToastModal_message: 'Successfully invited all the members!',
        _loader_spin: false,
        query: '',
        create_event_flag: false,
        user_status: undefined,
        request_tab_visible: false,
        memberShipTypes: {},
        showMemberships: [],
        membershipData: {},
        radioVal: 1,
        showMemberToAdd: [],
        _Duration: [],
        periodLookup: [],
        periodSubLookup: [],
        visiblepaymentmodal: false,
        stripe_account_id: undefined,
        link_to_generate_stripe: undefined,
        membershipSaveButtonLoader: false,
        membership_item_to_edit: undefined,
        pMVisible: false,
        payment_history_view_enabled: false,
        payment_history_view_user_id: undefined,
        invited_members: [],
        read_status: false,
        offset: 0,
        limit: 50,
        user_details: undefined,
        group_id: undefined,
        error_code_stripe_message: undefined,
        error_code_stripe: false
    })
    let { user_id, sub_item_id, familyDetails, loader, postLists, fetchFamilyInfo, groupsEvents, params, enableEdit, selectValue,
        familyPropValue, tabloader, groupMemberList, groupRequestList, blockedList, memberListDetails, selectedItem, countryList,
        invite, linkedFamilyList, description, title, image, msgTitle, selectedGroups, name, email, phone,
        selectedGrpsID, selectFamilyModal, count, dc_visible, al_visible, dcup_visible, bulk_visible,
        fileList, roleRelationArray, roleRelation, linkRequestArray, famLinkValue, listInputValue, selectedId, csvUploadingStatus,
        groupAlbums, folderObj, initial, openCoverPic, openProfilePic, item_to_edit, searchTag, ToastModal, visibleleaveModal, isGroupAdmin, pendingReq, button_loading,
        relationExist, existingRelationId, modalvisible, ToastModal_status, ToastModal_message, _loader_spin, create_event_flag, _fileList,
        memberShipTypes, showMemberships, membershipData, radioVal, showMemberToAdd, periodLookup, periodSubLookup, visiblepaymentmodal,
        stripe_account_id, link_to_generate_stripe, membershipSaveButtonLoader, membership_item_to_edit, pMVisible, payment_history_view_enabled, payment_history_view_user_id,
        invited_members, read_status, offset, limit, user_details, group_id, error_code_stripe, error_code_stripe_message
    } = familyViewState


    useEffect(() => {
        let { user_details: ud } = redux_state
        ud && setfamilyViewState(prev => ({ ...prev, user_details: ud }))
    }, [redux_state])
    useEffect(() => {
        let urlParams = window.location.pathname.split('/');
        let cipher;
        let _params = params
        cipher = window.location.pathname.split('/')[2];
        let group_id = decryption(cipher);
        switch (true) {
            case urlParams.length === 4:
                var sideTab = window.location.pathname.split('/')[3];
                sideTab && callback(sideTab, group_id)
                setfamilyViewState((prev) => ({ ...prev, initial: `${sideTab}`, sub_item_id: `${window.location.pathname.split('/')[3]}` }))
                break;
            case urlParams.length === 5:
                _params.group_id = group_id;
                setfamilyViewState(prev => ({ ...prev, sub_item_id: window.location.pathname.split('/')[3] }))
                callback(window.location.pathname.split('/')[4])
                break;
            default:
                cipher = window.location.pathname.split('/').pop();
                console.log(cipher)
                break;
        }
        localStorage.setItem('currentGrpID', group_id);
        _params.group_id = group_id
        familyPropValue.group_id = group_id;
        setfamilyViewState((prev) => ({ ...prev, loader: true, group_id: group_id, params: _params, familyPropValue: familyPropValue }));
        getFamilyDetails(_params);

    }, [window.location.pathname]);

    useEffect(() => {
        if (group_id) {
            let params = {
                group_id: `${group_id}`
            }
            chekActiveORNot(params).then(({ data }) => {
                let { id } = data
            }).catch(({ response }) => {
                let { data } = response
                setfamilyViewState(prev => ({
                    ...prev,
                    error_code_stripe_message: data.message,
                    error_code_stripe: true
                }))
            })
        }
    }, [group_id])
    const error_content = () => {
        notification.open({
            message: 'Error happened',
            description: `${error_code_stripe_message}`,
        });
    };
    // useEffect(() => {  alert(membershipData.isloaded)
    //     if(membershipData && membershipData.membershipid && !membershipData.isloaded) showMemberEdit(membershipData)
    // }, [membershipData['membershipid']]);

    const _handleChange = (e) => {
        setfamilyViewState(prev => ({ ...prev, invite: e.target.value, selectValue: 'United States/+1', email: '', phone: '', name: '' }))
    }
    // const onChange = (value) => {
    //     var str = value.split("/");
    //     setfamilyViewState(prev => ({ ...prev, selectValue: str[1] }))
    // }
    const callback = (key, val = group_id) => {
        if (key === '3') {
            setfamilyViewState(prev => ({ ...prev, payment_history_view_enabled: false }))
            listGroupMember()
        } else if (key === '11') {
            listGroupRequests()
        } else if (key === '13') {
            listBlockedMembers()
        } else if (key === '12') {
            listAllMembersToInvite()
        } else if (key === '1') {
            getFeeds('post', val);
        } else if (key === '2') {
            let ofset = 0; let pstLists = [];
            setfamilyViewState((prev) => ({ ...prev, postLists: pstLists, offset: ofset, _loader_spin: true }));
            getFeedsAnnouncement();
            //getFeeds('announcement');
        } else if (key === '4') {
            listGroupEvents()
        } else if (key === '5') {
            getAlbums()
        } else if (key == '6') {
            getDocuments()
        } else if (key === '7') {
            listAllLinkedFamily()
        } else if (key === '8') {
            history.push({
                pathname: '/calender',
                state: { group_id: params.group_id.toString(), group_name: familyDetails.group_name }
            })
            return;
        } else if (key == 'edit_family') {
            navigateToFamilyView();
            return;
        } else if (key == 'delete_family') {
            blockOrRemoveConfirmation('deletefamily', 'Are you sure you want to delete this family', '');
            return;
        } else if (key === 'leave_family') {
            setfamilyViewState(prev => ({ ...prev, visibleleaveModal: true }));
        } else if (key === '15') {
            pendingRequest({
                type: "invite",
                admin_id: user_id.toString(),
                group_id: params.group_id.toString()
            })
                .then((doc) => {
                    let { data } = doc.data;
                    setfamilyViewState((prev) => ({ ...prev, pendingReq: data, tabloader: false }))
                })
                .catch(e => e)
        }
        else if (key === '14') {
            listGroupRequests()
        }
        else if (key === '16') {
            setfamilyViewState(prev => ({ ...prev, sub_item_id: 16 }))
        }
        else if (key === '17') { //membership
            setfamilyViewState(prev => ({ ...prev, sub_item_id: 17, membershipData: {} }))
            let _filter = (radioVal != 1) ? { filter: radioVal } : {}
            listAllMemberShip(_filter)
        }
        else if (key == 'showAdd') {
            setfamilyViewState(prev => ({ ...prev, sub_item_id: 'showAdd' }))
        }
        else if (key == 'showMemberships') {
            setfamilyViewState(prev => ({ ...prev, sub_item_id: 'showMemberships' }))
        }
        else if (key == 'showMemberEdit') {
            setfamilyViewState(prev => ({ ...prev, sub_item_id: 'showMemberEdit', tabloader: false }))
        } else if (key == 'showMemberToAdd') {
            setfamilyViewState(prev => ({ ...prev, sub_item_id: 'showMemberToAdd', tabloader: false }))
        }
        else if (key == 'payment') {
            setfamilyViewState(prev => ({ ...prev, sub_item_id: 'payment', visiblepaymentmodal: !visiblepaymentmodal, tabloader: false }))
        }
        else if (key === '18') {
            setfamilyViewState(prev => ({ ...prev, sub_item_id: key }))
        }
        else if (key === '20') {
            displayPaymentTab()
            setfamilyViewState(prev => ({ ...prev, sub_item_id: key }))
        }
        else if (key === '21') {
            getInvitedMembersList()
            setfamilyViewState(prev => ({ ...prev, sub_item_id: key }))
        }
        setfamilyViewState(prev => ({ ...prev, sub_item_id: key }));
    }
    const getInvitedMembersList = () => {
        let { id: grp_id } = familyDetails
        let params_ = {
            group_id: `${grp_id}`
        }
        getInvitedUsersList(params_).then(res => {
            setfamilyViewState(prev => ({ ...prev, invited_members: res.data }))
        }).catch(err => {
            console.log(err)
        })
    }
    const listGroupEvents = (_query = '') => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        let _params = {
            group_id: params.group_id.toString(),
            user_id: user_id,
            query: _query
        }
        groupEvents(_params).then(r_res => {
            let res = r_res.data;
            let groupsEvents_ = []
            if (res.data.event_invitation.length === 0 && res.data.event_share.length === 0) {
                // groupsEvents.push({ no_data: true })
            }
            else {
                groupsEvents_ = res.data.event_invitation.concat(res.data.event_share)
            }

            setfamilyViewState((prev) => ({ ...prev, groupsEvents: groupsEvents_, tabloader: false }))

        }).catch((error) => {
            setfamilyViewState((prev) => ({ ...prev, tabloader: false }))
        });
    }
    // showmodal() {
    //     setfamilyViewState({ showmodal: !showmodal })
    //     this.fetchlinkfamily();
    // }

    const getFamilyDetails = (fun_data) => {
        if (!fun_data.group_id) {
            message.error('Access denied');
            // let self = this;
            setTimeout(function () {
                history.push('/my-family');
            }, 100)
            return;
        }
        let _params = {
            group_id: fun_data.group_id.toString(),
            user_id: user_id
        }
        getFamilDetails(_params).then((response) => {
            let { data } = response.data;
            let { stripe_account_id: stripe_acc_id } = data[0]
            if ((data.length && data[0] && !data[0].is_active) || data.length == 0) {
                setfamilyViewState((prev) => ({ ...prev, modalvisible: true }));
            }
            if (data.length > 0 && data[0].created_by == user_id) {
                setfamilyViewState((prev) => ({ ...prev, isGroupAdmin: true }));
            }
            let familyPropsValue = {
                familyDetails: response.data.data[0],
                count: response.data.count,
                group_id: fun_data.group_id.toString(),
            }
            // if (response.data.data[0].user_status !== 'member' && response.data.data[0].user_status !== 'admin') {
            //     var id = '10'
            // } else {
            //     var id = '1'
            // }
            var images = [];
            if (response && response.data && response.data.data[0] && response.data.data[0].history_images && response.data.data[0].history_images.length > 0) {
                response.data.data[0].history_images.map((item, index) => {
                    images.push(process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'history_images/' + item.filename);
                    fileList.push({
                        uid: index,
                        filename: item.filename,
                        type: item.type,
                        // status: 'done',
                        url: process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'history_images/' + item.filename,
                    })
                })
            }
            else {
                // images.push(require('../../../assets/images/demo/album1.png'))
            }
            setfamilyViewState((prev) => ({
                ...prev,
                famLinkValue: familyPropsValue.familyDetails.f_text,
                images: images,
                familyDetails: response.data.data[0],
                count: response.data.count,
                loader: false,
                familyPropValue: familyPropsValue,
                stripe_account_id: stripe_acc_id
            }));

        }).catch((error) => {
            console.log(error)
            setfamilyViewState((prev) => ({ ...prev, loader: false }));
        })
    }

    useEffect(() => {
        if (isNull(stripe_account_id)) {
            let { id: grp_id } = familyDetails
            let params_ = {
                group_id: `${grp_id}`
            }
            generateLink(params_).then(res => {
                let { data } = res
                let { link } = data
                setfamilyViewState(prev => ({ ...prev, link_to_generate_stripe: link }))
            })
        }
    }, [stripe_account_id]);

    /**
     * load the read/unread announcement
     */
    const getFeedsAnnouncement = () => {
        setfamilyViewState((prev) => ({ ...prev, _loader_spin: true }));
        postList({ user_id: user_id, group_id: params.group_id.toString(), type: 'announcement', offset: offset, limit: limit, read_status: read_status })
            .then(res => {
                let { data } = res;
                let new_data = [...postLists, ...data.data];
                let _offset = offset + limit;
                setfamilyViewState((prev) => ({ ...prev, postLists: new_data, offset: _offset, _loader_spin: false }));
            });
    }

    const [element, setElement] = useState(null);
    const Loader = useRef(getFeedsAnnouncement)
    const observer = useRef(new IntersectionObserver((entries) => {
        let last = entries[0];
        last.isIntersecting && Loader.current();
    }, { threshold: 1 }));

    useEffect(() => {
        const currentElement = element;
        const currentObserver = observer.current;
        if (currentElement) {
            currentObserver.observe(currentElement)
        }
        return () => {
            if (currentElement) currentObserver.unobserve(currentElement);
        }
    }, [element]);

    useEffect(() => {
        Loader.current = getFeedsAnnouncement;
    }, [getFeedsAnnouncement]);

    const fetchlinkfamilies = () => {
        setfamilyViewState((prev) => ({ ...prev, selectFamilyModal: true, tabloader: true }));
        var _params = {
            group_id: params.group_id.toString(),
            user_id: user_id
        }
        fetchFamilyDetails(_params).then((response) => {
            let data = response.data.data;
            setfamilyViewState((prev) => ({ ...prev, fetchFamilyInfo: data, tabloader: false }));
        }).catch((error) => {
            setfamilyViewState((prev) => ({ ...prev, tabloader: false }));
        })

    }
    const enableEditMode = (type) => event => {
        if (type === 'logo' && familyDetails.user_status !== 'admin') {
            return;
        }
        let _params = params
        _params.editType = type
        setfamilyViewState((prev) => ({ ...prev, enableEdit: true, params: _params }))
    }
    const showCropModel = () => {
        setfamilyViewState((prev) => ({ ...prev, enableEdit: false }))
    }
    const dataCallback = (response, type) => {
        if (response && response !== '') {
            var _familyDetails = familyDetails;
            if (type === 'coverpic') {
                _familyDetails.cover_pic = response.data.data[0].cover_pic;
                _familyDetails.group_original_image = response.data.data[0].group_original_image;
            } else {
                _familyDetails.logo = response.data.data[0].logo
            }
            setTimeout(() => {
                setfamilyViewState((prev) => ({ ...prev, familyDetails: _familyDetails, enableEdit: false }))
            }, (3000))
        } else {
            setfamilyViewState((prev) => ({ ...prev, enableEdit: false }))
        }

    }
    const listGroupMember = async (e) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }));
        let searchvalue
        if (e && e.target && e.target.value && e.target.value.length > 0) {
            searchvalue = e.target.value
        } else {
            searchvalue = ''
        }
        let _params = {
            crnt_user_id: user_id,
            group_id: params.group_id.toString(),
            query: e ? searchvalue : ''
        }
        await listAllGroupMember(_params).then((response) => {
            response.data.data.filter((item, index) => {
                if (item.is_blocked === true || item.is_removed === true) {
                    response.data.data.splice(index, 1);
                }
                else if ((user_id.toString() === item.user_id.toString()) && item.user_type === 'admin') {
                    response.data.admin = true;
                    setfamilyViewState((prev) => ({ ...prev, groupMemberList: response.data }));
                }
            });
            setfamilyViewState((prev) => ({ ...prev, groupMemberList: response.data, tabloader: false }))
        }).catch((error) => {

        })
    }
    const listGroupRequests = () => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        let _params = {
            group_id: params.group_id.toString(),
        }
        listAllGroupRequests(_params).then((response) => {
            setfamilyViewState((prev) => ({ ...prev, groupRequestList: response.data.data, linkRequestArray: response.data.linkRequest, tabloader: false }))
        }).catch((error) => {

        })
    }
    const listBlockedMembers = () => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        let _params = {
            user_id: user_id,
            type: "groups",
            group_id: params.group_id.toString()
        }
        getAllBlockedUsers(_params).then((response) => {
            setfamilyViewState((prev) => ({ ...prev, blockedList: response.data.data, tabloader: false }))
        }).catch((error) => {
            console.log('error=>', error);
            setfamilyViewState((prev) => ({ ...prev, tabloader: false }))
        })
    }
    const getFeeds = (type, id) => {
        console.log(id)
        setfamilyViewState((prev) => ({ ...prev, postLists: [], _loader_spin: true }));
        postList({ user_id: user_id, group_id: `${id}`, type: type, offset: 0, limit: 1000, read_status: read_status })
            .then(res => {
                setfamilyViewState((prev) => ({ ...prev, postLists: res.data.data, _loader_spin: false }));
            });
    }
    const listAllMembersToInvite = () => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }));
        let _params = {
            client_id: user_id,
            group_id: params.group_id.toString()
        }
        listAllMembers(_params).then((response) => {
            setfamilyViewState((prev) => ({ ...prev, memberListDetails: response.data, tabloader: false }))
        }).catch((error) => {

        })
    }
    const searchAllMembersToInvite = (e) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }));
        if (e.target.value.length > 0) {
            var _params = {
                searchtxt: e.target.value,
                userid: user_id,
                type: "users",
                group_id: params.group_id.toString()
            };
            globalSearchResult(_params).then((response) => {
                let resultArray = { rows: response.data.users };
                setfamilyViewState((prev) => ({ ...prev, memberListDetails: resultArray, tabloader: false }))
            })
        } else {
            listAllMembersToInvite();
        }
    }
    const navigateToUserPage = (item) => {
        var userId = encryption(item.user_id)
        history.push(`/user-details/${userId}`, { prev: window.location.pathname });
    }
    const itemIngroupMemberList = (item) => {
        if (groupMemberList && groupMemberList.data) {
            for (let i = 0; i < groupMemberList.data.length; i++) {
                if (groupMemberList.data[i].user_id == item.id)
                    return true;
            }
            return false;
        } else {
            return false;
        }
    }
    const addMemberToFamily = (item) => {
        let item_id = item.id
        let _params = {
            userId: item.id.toString(),
            groupId: params.group_id.toString(),
            from_id: user_id
        }

        addMemberToGroup(_params).then((response) => {
            if (response && response.data) {
                memberListDetails.rows.filter((each, index) => {
                    if (each.id === item_id) {
                        // if (response.data.status === 'pending') {
                        memberListDetails.rows[index].invitation_status = true
                        // } else if (response.data.status === 'accepted') {
                        //     memberListDetails.rows[index].EXISTS = true
                        // }
                        listGroupMember();
                        setfamilyViewState((prev) => ({ ...prev, memberListDetails: memberListDetails }))
                    }
                });
            }
        }).catch((error) => {

        })
    }
    const propagation = (e, value) => {
        e.stopPropagation();
        setfamilyViewState((prev) => ({ ...prev, selectedItem: value }))
    }

    const blockOrRemoveUser = (type) => {
        let param_data = {
            id: selectedItem.id,
        }
        if (type === 'block') {
            param_data.is_blocked = true;
        } else if (type === 'remove') {
            param_data.is_removed = true;
        } else if (type === 'unblock') {
            param_data.is_blocked = false;
        }
        memberUpdateGroupmaps(param_data).then((response) => {
            if (response.data && response.data.data && response.data.data.length > 0) {
                if (type === 'block' || type === 'remove') {
                    groupMemberList.data.filter((item, index) => {
                        if (item.user_id === response.data.data[0].user_id) {
                            groupMemberList.data.splice(index, 1);
                            setfamilyViewState((prev) => ({ ...prev, groupMemberList: groupMemberList }))
                        }
                    });

                } else if (type === 'unblock') {
                    blockedList.filter((item, index) => {
                        if (item.user_id === response.data.data[0].user_id) {
                            blockedList.splice(index, 1);
                            setfamilyViewState((prev) => ({ ...prev, blockedList: blockedList }))
                        }
                    });
                }
                displaySuccess()
            }
        }).catch((error) => {
            console.log('error=>', error)
        })
    }
    const handleChange = (tag, e) => {
        if (tag === 'phone') {
            var re = /^[0-9\b]+$/;
            if (e.target.value === '' || re.test(e.target.value)) {
                setfamilyViewState((prev) => ({ ...prev, [tag]: e.target.value, error: '' }))
            }
        } else if (tag === 'intro' || tag === 'history_text') {
            let famDetails = familyDetails;
            famDetails[tag] = e.target.value;
            setfamilyViewState((prev) => ({ ...prev, familyDetails: famDetails, error: '' }))
        } else {
            if (tag == 'famLinkValue') { e.target.value = e.target.value.replace(/ /g, "_") }
            setfamilyViewState((prev) => ({ ...prev, [tag]: e.target.value }))
        }
    }

    const listAllLinkedFamily = (queryParam) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true, selectedGrpsID: [] }))
        let _params = {
            group_id: params.group_id.toString(),
            query: queryParam ? queryParam.target.value : ''
        }
        listLinkedFamily(_params).then((response) => {
            setfamilyViewState((prev) => ({ ...prev, linkedFamilyList: response.data.data, tabloader: false }));
        }).catch((error) => {
            setfamilyViewState((prev) => ({ ...prev, tabloader: false }));
            console.log('errror=>', error);
        })
    }
    const handleOk = () => {
        let errcount = 0;
        if (!description) errcount++;
        if (!title) errcount++;

        if (errcount > 0) {
            openNotificationWithIcon('error', '* required fields are empty')
            return;
        }
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }));
        const formData = new FormData();
        formData.append('os', 'web');
        formData.append('description', description);
        formData.append('title', title);
        formData.append('user', user_id);
        formData.append('device', navigator.platform);
        formData.append('image', image);
        feedBackSubmit(formData).then((response) => {
            openNotificationWithIcon('success', 'Feedback Successfully sent.')
            setfamilyViewState((prev) => ({ ...prev, description: '', title: '', image: '', tabloader: false }));

        }).catch((error) => {

        })
    };
    const uploadPhoto = info => {
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, imageUrl =>
                setfamilyViewState((prev) => ({ ...prev, imageUrl, loading: false })),
            );
        }
    }
    const getBase64 = (img, callback_fn) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback_fn(reader.result));
        reader.readAsDataURL(img);
        setfamilyViewState((prev) => ({ ...prev, image: img }));
    }
    const blockOrRemoveConfirmation = (type, note, u_id) => {
        var msg = note ? note : `Are you sure you want to ${type} this person`
        confirm({
            title: msg,
            okText: 'Yes',
            cancelText: 'No',
            onOk() {
                if (type === 'deletefamily') {
                    deleteFamily();
                } else if (type === 'albumDelete') {
                    deleteAlbumByID(u_id);
                } else if (type === 'leavefamily') {
                    handleOkLeave();
                } else {
                    blockOrRemoveUser(type);
                }
            },
            onCancel() {
            },
        });
    }
    const unblockUser = (value) => {
        value.id = value.groupmap_id;
        setfamilyViewState((prev) => ({ ...prev, selectedItem: value }))
        blockOrRemoveConfirmation('unblock', '', '')
    }
    const onPhoneCodeChange = (value) => {
        setfamilyViewState((prev) => ({ ...prev, selectValue: value }))
    }
    const inviteMember = () => {
        var re = /^[0-9\b]+$/;
        var email_res = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var flag = 0;
        if (!name || name.trim().length <= 0) {
            notifications('error', 'Error', 'Invalid User name');
            flag = 1;
        }
        if (!phone || phone.length < 10 || !re.test(phone)) {
            notifications('error', 'Error', 'Invalid Phone Number');
            flag = 1;
        }
        if (email && !email_res.test(email)) {
            notifications('error', 'Error', 'Invalid Email');
            flag = 1;
        }
        if (flag == 0) {
            setfamilyViewState((prev) => ({
                ...prev,
                tabloader: true,
                invite: 'others'
            }))
            let _params = {
                user_id: user_id,
                phone: selectValue.split('/')[1] + phone,
                email: email,
                group_id: params.group_id,
                name: name,
                from_name: (user_details && user_details.full_name) ? user_details.full_name : ''
            };
            inviteViaSms(_params).then((response) => {
                if (response && response.data && response.data.message) {
                    if (response.data.message == 'success') {
                        setfamilyViewState((prev) => ({ ...prev, tabloader: false, selectValue: 'United States/+1', email: '', phone: '', name: '' }));
                        notifications('success', 'Success', 'Invited user successfully');
                    } else {
                        notifications('error', 'Error', response.data.message);
                    }
                } else {
                    notifications('error', 'Error', 'Could not invite user');
                }
                setfamilyViewState((prev) => ({ ...prev, tabloader: false }))
            }).catch((error) => {
                let { message: eer_message } = error.response.data;
                setfamilyViewState((prev) => ({ ...prev, tabloader: false }))
                notifications('error', 'Error', eer_message);
            })
        }
    }
    const onChangeFunction = (e, type) => {
        let value = e.target.value;
        setfamilyViewState((prev) => ({ ...prev, [type]: value }))
    }
    const handleChooseFamilyList = (e, v) => {
        if (e.target.checked) {
            selectedGroups.push({ id: v.id });
            selectedGrpsID.push(v.id);
        } else {
            let index = selectedGrpsID.indexOf(v.id);
            selectedGroups.splice(index, 1);
            selectedGrpsID.splice(index, 1);
        }
        setfamilyViewState((prev) => ({ ...prev, selectedGroups: selectedGroups }));
    }
    const onClose = () => {
        setfamilyViewState((prev) => ({ ...prev, selectFamilyModal: false, selectedGroups: [], selectedGrpsID: [] }));
    }
    const linkToSelectedGroups = () => {
        setfamilyViewState(prev => ({ ...prev, button_loading: true }))
        let _params = {
            to_group: selectedGrpsID,
            from_group: params.group_id,
            requested_by: user_id,
        }
        requestLinkFamily(_params).then((response) => {
            setfamilyViewState((prev) => ({ ...prev, selectFamilyModal: false, button_loading: false }))
            listAllLinkedFamily()
        }).catch((error) => {
            setfamilyViewState((prev) => ({ ...prev, selectFamilyModal: false }));
        })
    }
    const unlinkOrLinkFamily = (type, value, e) => {
        e.stopPropagation()
        let params_ = {
            id: value.id.toString(),
            status: type
        }
        actionToLinkedFamily(params_).then((response) => {
            if (response && response.data && response.data.data) {
                var idIndex = linkedFamilyList.findIndex(i => i.id === response.data.data.id);
                linkedFamilyList[idIndex].status = type;
                listAllLinkedFamily()
                setfamilyViewState((prev) => ({ ...prev, linkedFamilyList: linkedFamilyList }))
                message.success(`Successfully ${type} `)
            }
        }).catch((error) => {
            console.log('error', error)
        })

    }
    // const listGroupAlbums = () => {
    //     let _params = {
    //         group_id: params.group_id.toString(),
    //         user_id: user_id,
    //         folder_for: "groups",
    //         folder_type: "albums"
    //     }
    //     listGroupFolders(_params).then((response) => {
    //     }).catch((error) => {
    //         console.log('error', error)
    //     })
    // }
    // Modal - Album
    // const al_showModal = () => {
    //     setfamilyViewState((prev) => ({ ...prev, al_visible: true }));
    // };
    const al_handleCancel = e => {
        setfamilyViewState((prev) => ({ ...prev, al_visible: false }));
    };
    // Modal - Share
    const sh_showModal = () => {
        setfamilyViewState(prev => ({
            ...prev,
            sh_visible: true,
        }))
    };
    // const sh_handleCancel = e => {
    //     setfamilyViewState(prev => ({
    //         ...prev,
    //         sh_visible: false,
    //     }))
    // };
    // const dcup_showModal = () => {
    //     setfamilyViewState((prev) => ({ ...prev, dcup_visible: true }));
    // };
    const dcup_handleCancel = e => {
        setfamilyViewState((prev) => ({ ...prev, dcup_visible: false, roleRelation: false }));
    };
    const bulk_showModal = () => {
        setfamilyViewState((prev) => ({ ...prev, bulk_visible: true }));
    };
    const bulk_handleCancel = e => {
        setfamilyViewState((prev) => ({ ...prev, bulk_visible: false, roleRelation: false, csvUploadResponse: '', ToastModal: false }));
    };
    // const enableIntroEdit = (name) => {
    //     setfamilyViewState((prev) => ({ ...prev, [name]: true }));
    // }
    // const updateTheFamilyJSON = (formData) => {
    //     updateFamilyJSON(formData).then((resp) => {
    //         let imageArray = [];
    //         fileList && fileList.length > 0 && fileList.map((item, i) => {
    //             imageArray.push(process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'history_images/' + item.filename)
    //         })
    //         setfamilyViewState((prev) => ({ ...prev, images: imageArray }))
    //     }).catch((err) => {
    //     })
    // }
    const updateTheFamily = (formData) => {
        updateFamily(formData).then((resp) => {
        }).catch((err) => {
        })
    }
    const listRoleOrRelation = (type, query, item, existFlag) => {
        setfamilyViewState((prev) => ({ ...prev, roleRelation: true, tabloader: true, selectedItem: item ? item.user_id : selectedItem, existingRelationId: (item ? item.relation_id : '') }))
        let params_ = {
            type: type,
            query: query ? query : ''
        }
        getallRelations(params_).then((res) => {
            if (res && res.data.data && res.data.data) {
                setfamilyViewState((prev) => ({ ...prev, roleRelationArray: res.data.data, tabloader: false, relationExist: existFlag }))
            }
        }).catch((err) => {

        })
    }
    const acceptOrReject = (resp, type, value) => {
        let params_ = {
            id: value.id.toString(),
            type: type,
            status: resp,
            from_user: value.user_id,
            to_user: familyDetails.created_by,
            responded_by: user_id
        }
        adminRequestAction(params_).then((res) => {
            listGroupRequests();
        }).catch((err) => {
            console.log(err)
        })
    }
    const navigateToFamily = (value) => {
        window.location.reload()
        var familyId = encryption(value.to_group)
        history.push(`/familyview/${familyId}`);
    }
    // const responsefromdb = (res) => {
    //     console.log(res)
    //     let _fileList = fileList
    //     _fileList.push({
    //         filename: res.data[0].filename,
    //         type: res.data[0].type
    //     })
    //     setfamilyViewState((prev) => ({ ...prev, fileList: _fileList }));
    // }
    // const handleRemoveChange = (file) => {
    //     let _fileList = fileList.filter((item => {
    //         return item.filename !== file.name
    //     }));
    //     setfamilyViewState((prev) => ({ ...prev, fileList: _fileList }));
    // }
    const deleteFamily = () => {
        const formData = new FormData();
        formData.append('intro', familyDetails.intro);
        formData.append('id', familyDetails.id);
        formData.append('user_id', user_id);
        formData.append('is_active', false);
        updateTheFamily(formData);
        history.push('/my-family')
    }
    // const checkLink = () => {
    //     familyLinkExist({ f_text: famLinkValue }).then((res) => {
    //         if (res && res.data && res.data.count === 0) {
    //             let _familyDetails = familyDetails;
    //             _familyDetails.f_link = familyDetails.f_link.split('/').splice(0, familyDetails.f_link.split('/').length - 1).toString().replace(/,/g, '/') + `/${famLinkValue}`
    //             setfamilyViewState((prev) => ({ ...prev, familyDetails: _familyDetails }));
    //             saveIntro()
    //         } else {
    //             notifications('error', 'Error', 'link already exist')
    //         }
    //     })
    // }
    const selectedList = (item) => {
        setfamilyViewState(prev => ({ ...prev, listEnable: false, listInputValue: item.relationship, selectedId: item.id }));

    }
    const handleRoleList = (e) => {
        setfamilyViewState(prev => ({ ...prev, listEnable: false, listInputValue: e.target.value }));
        if (e.target.value.length > 0) {
            listRoleOrRelation('role', e.target.value, '')
        } else {
            listRoleOrRelation('role', '', '')
        }
        // setTimeout(() => {

        // }, (1000))
    }
    const addRoleOrRelation = (type) => {
        let params_ = {
            primary_user: user_id,
            secondary_user: selectedItem.toString(),
            group_id: familyPropValue.group_id.toString(),
            // id: existingRelationId ? existingRelationId.toString() : '',
            type: familyDetails.group_category === 'Regular' ? 'relation' : 'role',
            relation_id: selectedId ? selectedId : 'others',
            // value: listInputValue
        }
        if (type == true) { //updateRelation
            updateRelation({ ...params_, id: `${existingRelationId}` }).then((res) => {
                if (res && res.data && res.data.data) {
                    listGroupMember();
                    setfamilyViewState(prev => ({
                        ...prev,
                        roleRelation: false
                    }));
                }
            }).catch((err) => {
                console.log("error happened")
            })
        } else {
            addRelation(params_).then((res) => {
                if (res && res.data && res.data.data) {
                    listGroupMember();
                    setfamilyViewState(prev => ({
                        ...prev,
                        roleRelation: false
                    }));
                }
            }).catch((err) => {

            })
        }

    }
    const getAlbums = () => {
        let _params = {
            user_id: user_id,
            group_id: params.group_id ? params.group_id.toString() : localStorage.getItem('currentGrpID'),
            folder_for: "groups",
            folder_type: "albums"
        }
        listGroupFolders(_params).then((resp) => {
            setfamilyViewState(prev => ({ ...prev, groupAlbums: resp.data.data }))
            countRefresh(_params.group_id);
        })
    }
    const displaySuccess = () => {
        notification.open({
            message: "Success",
        });
    };
    const createFolderAction = () => {
        folderObj['created_by'] = user_id;
        folderObj['folder_for'] = 'groups';
        folderObj['folder_type'] = 'documents';

        createAlbum(folderObj)
            .then(response => {
                console.log(response);
            });
    }
    const onFolderObjChange = (value, key) => {
        folderObj[key] = value.target.value;
        setfamilyViewState(prev => ({ ...prev, folderObj: folderObj }))
    }

    const getDocuments = () => {
        let _params = {
            user_id: user_id,
            group_id: params.group_id.toString(),
            folder_for: "groups",
            folder_type: "documents"
        }
        listGroupFolders(_params).then((resp) => {
            setfamilyViewState(prev => ({ ...prev, groupDocuments: resp.data.data }))
        })
    }

    const countRefresh = (groupid) => {
        getFamilDetails({ group_id: groupid, user_id: user_id }).then((response) => {
            setfamilyViewState(prev => ({ ...prev, count: response.data.count }))
        }).catch(err => {

        })
    }
    const deleteAlbumByID = (album_id) => {
        let params_ = {
            folder_id: album_id,
            user_id: user_id
        }
        deleteAlbum(params_).then((res) => {
            if (res && res.data && res.data.code === 200) {
                let alvumArray = groupAlbums.filter(item => item.id !== id);
                setfamilyViewState(prev => ({ ...prev, groupAlbums: alvumArray }))
            }
        }).then((err) => {
        })
    }
    const navigateToFamilyView = () => {
        var familyId = encryption(params.group_id);
        console.log(familyId)
        history.push(`/edit-family/${familyId}`)
    }
    const navigateToFamilyViewAdvancedSettings = () => {
        var familyId = encryption(params.group_id);
        console.log(familyId)
        history.push(`/create-family/${familyId}/tabId=3`)
    }
    const followUnfollow = (type) => {
        var _params = {
            user_id: user_id,
            group_id: params.group_id
        }
        if (type) {
            unfollow(_params).then((res) => {
                let data = familyDetails;
                data.following = res.data.following;
                setfamilyViewState(prev => ({ ...prev, familyDetails: data }))
            })
        } else {
            follow(_params).then((res) => {
                let data = familyDetails;
                data.following = res.data.following;
                setfamilyViewState(prev => ({ ...prev, familyDetails: data }))
            })
        }
    }
    const openProfilePhoto = () => {
        let _openProfilePic = openProfilePic
        setfamilyViewState(prev => ({ ...prev, openProfilePic: !_openProfilePic }))
    }
    const openCoverPhoto = () => {
        let _openCoverPic = openCoverPic
        setfamilyViewState(prev => ({ ...prev, openCoverPic: !_openCoverPic }))
    }
    const joinToFamily = () => {
        let param = {
            user_id: user_id,
            group_id: familyDetails.id.toString()
        }
        joinToExistingFamily(param).then((resp) => {
            if (resp && resp.data && resp.data.data && resp.data.error != 1) {
                let _familyDetails = familyDetails
                _familyDetails.joined_status = resp.data.data.status;
                _familyDetails.user_status = resp.data.data.user_status;
                setfamilyViewState((prev) => ({ ...prev, familyDetails: _familyDetails }));
            } else if (resp && resp.data && resp.data.error == 1) {
                notifications('error', 'Error', resp.data.message);
            }
        }).catch((err) => {
            console.log(err);
            notifications('error', 'Error', err.message)
        })
    }
    // render() {
    const menu = (current) => {
        let { user_id: userId } = current
        return <Menu >
            <Menu.Item key="1" onClick={() => blockOrRemoveConfirmation('block', '', '')}>
                <Icon type="user" /> Block User
            </Menu.Item>
            <Menu.Item key="2" onClick={() => blockOrRemoveConfirmation('remove', '', '')}>
                <Icon type="info-circle" /> Remove User
            </Menu.Item>
            {current.is_primar_admin === "admin" && <Menu.Item key="3" onClick={() => changeUserType(current)}>
                <Icon type="info-circle" /> {current.user_type === "member" ? "Make admin" : "Remove admin role"}
            </Menu.Item>}
            <Menu.Item key="4" onClick={() => displayPaymentTab(userId)}>
                <Icon type="calculator" /> View payment history
            </Menu.Item>
        </Menu>
    }
    // const membership = (current) => {
    //     return <Menu >
    //         <Menu.Item key="1">Chat</Menu.Item>
    //         <Menu.Item key="2" onClick={() => callback('showMemberEdit')}>Edit</Menu.Item>
    //         <Menu.Item key="3">Remove</Menu.Item>
    //     </Menu>
    // }
    const navigateToAboutUs = () => {
        setfamilyViewState(prev => ({ ...prev, sub_item_id: 10 }))
        callback(10)
    }
    const setting = (
        <Menu >
            {familyDetails && familyDetails.user_status === 'admin' &&
                <Menu.Item key="1" onClick={() => navigateToFamilyView()}>
                    <Icon type="edit" /> Basic Settings
                </Menu.Item>
            }
            {familyDetails && familyDetails.user_status === 'admin' &&
                <Menu.Item key="2" onClick={() => navigateToFamilyViewAdvancedSettings()}>
                    <Icon type="edit" /> Advanced Settings
                </Menu.Item>
            }
            {familyDetails && stripe_account_id === null ?
                <Menu.Item key="7" onClick={() => { window.open(`${link_to_generate_stripe}`) }}>
                    <Icon type="info-circle" /> Add Bank Account
                </Menu.Item> :
                (error_code_stripe ?
                    <Menu.Item>
                        <div onClick={error_content}>
                            <Icon type="exclamation" />
                            <span style={{ marginRight: '30px', color: 'red' }}>
                                {familyDetails.stripe_account_id}
                            </span>
                            <span>Error</span>
                            <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
                        </div>
                    </Menu.Item> :
                    <Menu.Item key="7">
                        <Icon type="info-circle" />
                        <span style={{ marginRight: '30px' }}>
                            {familyDetails.stripe_account_id}
                        </span>
                        <span>Active</span>
                        <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
                    </Menu.Item>)
            }
            {familyDetails &&
                <Menu.Item key="3" onClick={() => followUnfollow(familyDetails.following)}>
                    <Icon type="info-circle" /> {familyDetails && familyDetails.following ? 'Unfollow Family' : 'Follow Family'}
                </Menu.Item>
            }
            {familyDetails &&
                <Menu.Item key="4" onClick={() => blockOrRemoveConfirmation('leavefamily', 'Are you sure you want to leave this family?', '')}>
                    <Icon type="info-circle" /> Leave Family
                </Menu.Item>
            }
            {familyDetails && familyDetails.user_status === 'admin' &&
                <Menu.Item key="5" onClick={() => blockOrRemoveConfirmation('deletefamily', 'Are you sure you want to delete this family', '')}>
                    <Icon type="info-circle" /> Delete Family
                </Menu.Item>
            }
            {familyDetails && sub_item_id != 10 &&
                <Menu.Item key="6" onClick={() => navigateToAboutUs()}>
                    <Icon type="edit" /> Family Url
                </Menu.Item>
            }
        </Menu>
    );
    const changeUserType = (user) => {
        var _params = {
            id: user.id,
            type: user.user_type === "admin" ? "member" : "admin"
        }
        updateGroupMaps(_params).then(async res => {
            if (_params.type == 'admin') {
                notifications('success', 'success', 'User added as admin');
            } else {
                notifications('success', 'success', 'User removed from admin');
            }
            var list_params = {
                crnt_user_id: user_id,
                group_id: params.group_id.toString(),
            };

            await listAllGroupMember(list_params).then((response) => {
                response.data.data.filter((item, index) => {
                    if (item.is_blocked === true || item.is_removed === true) {
                        response.data.data.splice(index, 1);
                    }
                    else if ((user_id.toString() === item.user_id.toString()) && item.user_type === 'admin') {
                        response.data.admin = true;
                        setfamilyViewState((prev) => ({ ...prev, groupMemberList: response.data }));
                    }
                });
                setfamilyViewState((prev) => ({ ...prev, groupMemberList: response.data, tabloader: false }))
            }).catch((error) => {

            })
        })
    }
    // const uploadButton = (
    //     <div>
    //         <Icon type="plus" />
    //         <div className="ant-upload-text">Upload</div>
    //     </div>
    // );
    // Modal - Document
    const dc_showModal = () => {
        setfamilyViewState(prev => ({
            ...prev,
            dc_visible: true,
        }))
    };
    const dc_handleCancel = e => {
        setfamilyViewState(prev => ({
            ...prev,
            roleRelation: false,
        }))
    };
    const setAlbumToEdit = (item) => {
        setfamilyViewState(prev => ({ ...prev, item_to_edit: item }))
    }
    const url = `${process.env.REACT_APP_API_URL}/api/v1/familheey/upload_file_tos3?name=history_images`
    const onAlbumSearch = (event) => {
        setfamilyViewState(prev => ({ ...prev, searchTag: event.target.value }))
    }
    const CSVupload = (file) => {
        setfamilyViewState((prev) => ({ ...prev, csvUploadingStatus: true }));
        let formData = new FormData();
        formData.append('from_user', user_id);
        formData.append('file', file);
        formData.append('type', 'add_memmer');
        formData.append('group_id', params.group_id.toString())
        bulkUpload(formData).then(res => {
            setfamilyViewState((prev) => ({ ...prev, csvUploadingStatus: false, csvUploadResponse: res.data, csvUploadResponseMessage: file.name, ToastModal: true, _fileList: [] }));
            // openNotificationWithIcon('success', 'Successfully uploaded');
        }).catch(err => {
            // openNotificationWithIcon('error', 'error happened');
            setfamilyViewState((prev) => ({ ...prev, csvUploadingStatus: false, ToastModal: true, ToastModal_message: "Something happened please try after sometime", ToastModal_status: 'error' }));
            setTimeout(() => {
                setfamilyViewState(prev => ({ ...prev, bulk_visible: false }))
            }, 3000)
        })
    }
    const openNotificationWithIcon = (type, txt) => {
        notification[type]({
            message: `${txt}`,
        });
    };
    // Toggle Sidebar
    const togglefamilhsidebar = () => {
        document.getElementById('f-wrap').classList.toggle("f-show-familhy-sider");
    }
    // Close Sidebar
    const closefamilhsidebar = () => {
        document.getElementById('f-wrap').classList.remove("f-show-familhy-sider");
    }
    //naviagatetofamilylist
    const navigateToMyFamilyView = () => {
        history.push(`/my-family/`)
    }
    //leaveFamily
    const handleOkLeave = () => {
        var i_params = {
            group_id: params && params.group_id ? params.group_id : '',
            //user_id: user_id ? user_id : user_id,
            user_id: user_id,
        }
        leaveFamily(i_params).then((response) => {
            setfamilyViewState(prev => ({
                ...prev,
                visibleleaveModal: false,
            }))
            message.success("You have left this family")
            navigateToMyFamilyView()
        })

    }
    const handleCancelLeave = () => {
        setfamilyViewState(prev => ({
            ...prev,
            visibleleaveModal: false,
        }))

    }

    /**
    *
    * @param {*} id
    * delete pending request
    */
    const _deleteRequest = (req_id) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        deleteRequest({ id: `${req_id}` })
            .then((doc) => {
                callback('15')
            })
            .catch(e => e)
    }
    let { id } = familyDetails
    const approveORrejectLinkRequest = (link_id, status) => {
        let params_data = {
            id: `${link_id}`,
            type: 'fetch_link',
            status: status,
            responded_by: user_id
        }
        linkRequestStatus(params_data).then(res => {
            listGroupRequests()
        }).catch(err => {
            console.log(err)
        })
    }
    let buttonFlag = selectedGroups.length > 0
    const handleModalOk = () => {
        history.push('/');
        setfamilyViewState((prev) => ({ ...prev, modalvisible: false }))
    }

    const ModalToast = () => <Result
        status={`${ToastModal_status}`}
        title={`${ToastModal_message}`}
        subTitle=""
    />
    const searchEvent = (param) => {
        // setfamilyViewState((prev) => ({ ...prev,query: param.target.value }))
        listGroupEvents(param.target.value);
    }
    const create = () => {
        history.push('/post/create',
            {
                path_: `${window.location.pathname}`,
                type_: 'announcement',
                groupId: familyDetails.id,
                group_name: familyDetails.group_name
            })
        // var grouptId = encryption(params.group_id);
        // history.push('/announcement/create_from_family/' + grouptId,{path_:`/familyview/${grouptId}`,type_:'announcement'});
    }
    const handleButtonClick = () => {
        setfamilyViewState(prev => ({ ...prev, create_event_flag: !create_event_flag }))
    }
    const beforeUpload = file => {
        setfamilyViewState(prev => ({ ...prev, _fileList: [file] }));
        return false;
    }
    const removeFile = file => {
        setfamilyViewState(prev => ({ ...prev, _fileList: [] }));
        return true;
    }
    const showButtons = (tag) => {
        let post_create = familyDetails.post_create
        let user = familyDetails.user_status
        let announcement_create = familyDetails.announcement_create
        let post_button_visible, ann_button_visible
        switch (tag) {
            case 'post':
                switch (user) {
                    case 'admin':
                        post_button_visible = true
                        break;
                    case 'member':
                        post_create == 6 ? (post_button_visible = true) : (post_button_visible = false)
                        break;
                    default:
                        post_button_visible = false
                        break;
                }
                return post_button_visible;
                break;
            case 'ann':
                switch (user) {
                    case 'admin':
                        ann_button_visible = true
                        break;
                    case 'member':
                        announcement_create == 18 ? (ann_button_visible = true) : (ann_button_visible = false)
                        break;
                    default:
                        ann_button_visible = false
                        break;
                }
                return ann_button_visible;
                break;
        }
    }

    /**
    * 
    * @param {all,overdue,current} filters 
    */
    const listAllMemberShip = (filters) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        let input = { user_id: user_id, group_id: params.group_id, ...filters }
        listAllMemberShipTypes(input).then((doc) => {
            let { data } = doc;
            setfamilyViewState((prev) => ({
                ...prev,
                tabloader: false,
                memberShipTypes: data
            }))
        }).catch((err) => {
            setfamilyViewState((prev) => ({
                ...prev,
                tabloader: false,
                memberShipTypes: {}
            }))
        })
    }

    const getm_to = () => {
        let { membership_duration, membership_from } = membershipData;
        let _membership_duration = membership_duration ? membership_duration : 0;
        let _m_to = membership_from ? moment.unix(membership_from).add(_membership_duration, "days").unix() : moment().add(_membership_duration, "days").unix();
        return parseInt(_m_to);
    }

    /**
    * form to create membership types
    */
    const handleSubmit = (event) => {
        event.preventDefault();
        validateFields((err, values) => {
            if (err) return;

            let periodslookup = periodLookup.filter(v => v.id == values.membership_period)
            let input = {
                ...values,
                group_id: params.group_id,
                created_by: user_id,
                membership_period_type_id: periodslookup[0].id,
                membership_period: periodslookup[0].membership_lookup_period,
                membership_to: getm_to()
            }
            setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
            if (membershipData.type) {//update
                input = { ...input, id: membershipData.id.toString() }
                updateMemberShipType(input).then((doc) => {
                    callback("17");
                    notifications("success", "success", "Membership updated");
                }).catch((error) => {
                    callback("17");
                })
            } else {//create
                saveMemberShipType(input).then((doc) => {
                    callback("17");
                    notifications("success", "success", 'Membership created');
                }).catch((error) => {
                    callback("17");
                })
            }

        });
    }

    const AddUserToPlan = (event) => {
        event.preventDefault();
        validateFields((err, values) => {
            let selectedMembershipDurationObject = periodSubLookup.find(item => item.type_value === values.membership_duration)
            values = {
                ...values,
                // membership_paid_on: values.membership_paid_on.utc().unix(),
                group_id: params.group_id,
                user_id: membership_item_to_edit.user_id ? membership_item_to_edit.user_id.toString() : memberShipTypes.uid.toString(),
                membership_id: values.membership_id.toString(),
                membership_from: values.membership_from.utc().unix(),
                membership_to: moment(values.membership_to).utc().unix(),
                membership_period_type: selectedMembershipDurationObject.type_name,
                edited_user_id: user_id
            }
            if (membership_item_to_edit.membership_from && membership_item_to_edit.membership_to) {
                values = {
                    ...values,
                    membership_from: membership_item_to_edit.membership_from,
                    membership_to: membership_item_to_edit.membership_to
                }
            }
            if (err) return
            setfamilyViewState((prev) => ({ ...prev, tabloader: true }));
            AddUserToType(values).then((doc) => {
                membership_item_to_edit.isloaded = true;
                setfamilyViewState((prev) => ({ ...prev, tabloader: false }));
                // fetch_mDetails(membership_item_to_edit)
                callback('17')
                let param = {
                    group_id: `${params.group_id}`,
                    user_id: `${user_id}`
                }
                getFamilyDetails(param)
            }).catch((error) => {
                membership_item_to_edit.isloaded = true;
                setfamilyViewState((prev) => ({ ...prev, tabloader: false }))
                fetch_mDetails(membership_item_to_edit)
            })
        });
    }

    /**
    * 
    * radio button change events
    * All active inactive
    */
    const radioSelect = (e) => {
        let val = e.target.value;
        let _filter = (val != 1) ? { filter: val } : {}
        setfamilyViewState((prev) => ({ ...prev, radioVal: val }))
        listAllMemberShip(_filter)
    }

    /**
    * get membership by id to edit
    */
    const getMembershipByid = (mid) => {
        Add_mShip();
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        let input = { membership_id: mid.toString(), user_id: user_id.toString() }
        getMembershiptypeById(input).then((doc) => {
            let { data } = doc;
            data.doc = { ...data.doc, type: 'Edit' }
            setfamilyViewState((prev) => ({ ...prev, membershipData: data.doc, tabloader: false }))
            callback('showAdd')
        }).catch((err) => {

        })
    }

    /**
    * fetch the membership members to show in view
    */
    const fetch_mDetails = (item) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        let input = {
            crnt_user_id: user_id,
            group_id: params.group_id,
            membership_id: item.membershipid || item.membership_id
        }
        listAllGroupMember(input).then((doc) => {
            let { data } = doc;
            setfamilyViewState((prev) => ({ ...prev, membershipData: item, showMemberships: data.data, tabloader: false }))
            callback('showMemberships');
        }).catch((err) => {
            setfamilyViewState((prev) => ({ ...prev, membershipData: item, showMemberships: [], tabloader: false }))
        })
    }

    const showFormMship = () => {
        setfamilyViewState((prev) => ({ ...prev, membershipData: {} }))
        Add_mShip("showAdd");
    }

    const Add_mShip = (redirectTo = null) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        showMshipPeriod({}).then((doc) => {
            let { data } = doc;
            setfamilyViewState((prev) => ({ ...prev, tabloader: false, periodLookup: data }))
            if (redirectTo) callback(redirectTo)
        }).catch((err) => {
            setfamilyViewState((prev) => ({ ...prev, tabloader: false }))
            if (redirectTo) callback(redirectTo)
        })
    }

    const ListMemberToAdd = () => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }))
        let input = {
            crnt_user_id: user_id,
            group_id: params.group_id
        }
        listAllGroupMember(input).then((doc) => {
            let { data } = doc;
            setfamilyViewState((prev) => ({ ...prev, showMemberToAdd: data.data, tabloader: false }))
            callback('showMemberToAdd')
        }).catch((err) => {
            setfamilyViewState((prev) => ({ ...prev, showMemberToAdd: [], tabloader: false }))
            callback('showMemberToAdd')
        })
    }

    const EditForm = (e, item) => {
        // console.log(item)
        e.stopPropagation()
        let { membership_from } = item
        let newDateFrom = membership_from ? membership_from : moment().unix()
        let edited_item = { ...item, membership_from: newDateFrom }
        setfamilyViewState((prev) => ({ ...prev, membership_item_to_edit: edited_item }));
        showMemberEdit(item)
    }

    // const AddForm = (item) => {
    //     membershipData = {
    //         membershipid: membershipData.membershipid,
    //         membership_name: membershipData.membership_name,
    //         member_count: membershipData.member_count,
    //         is_active: membershipData.is_active
    //     }
    //     membershipData = { ...membershipData, membership_id: item.membership_id, user_id: item.user_id }
    //     setfamilyViewState((prev) => ({ ...prev, membershipData: membershipData }));
    //     showMemberEdit(item)
    // }

    const showMemberEdit = (item) => {
        setfamilyViewState((prev) => ({ ...prev, tabloader: true }));
        let input = {
            user_id: user_id,
            group_id: params.group_id
        }
        GetListLookup(input).then((doc) => {
            let { data } = doc;
            // console.log(data)
            // console.log(item)
            memberShipTypes = { ...memberShipTypes, ListLookup: data, uid: item.user_id }
            if (item.membership_id) {
                let selected_membership_obj = data.find(it => it.id === item.membership_id)
                let { types } = selected_membership_obj
                setfamilyViewState((prev) => ({
                    ...prev,
                    tabloader: true,
                    memberShipTypes: memberShipTypes,
                    periodSubLookup: types
                }))
            }
            else {
                setfamilyViewState((prev) => ({
                    ...prev,
                    tabloader: true,
                    memberShipTypes: memberShipTypes,
                    periodSubLookup: []
                }))
            }
            // setfamilyViewState((prev) => ({
            //     ...prev,
            //     tabloader: true,
            //     memberShipTypes: memberShipTypes,
            //     periodSubLookup: types?types:[]
            // }));
            callback("showMemberEdit");
        }).catch((err) => {
            console.log(err)
            callback("showMemberEdit")
            setfamilyViewState((prev) => ({
                ...prev,
                tabloader: true,
                memberShipTypes: memberShipTypes,
            }));
        });
    }

    const changeDrop = (val) => {
        let { ListLookup } = memberShipTypes
        let selected_membership_obj = ListLookup.find(item => item.id === val)
        console.log(selected_membership_obj)
        let { id: membershipId, membership_period_type_id, types, membership_fees } = selected_membership_obj
        let { membership_id, membership_from } = membership_item_to_edit
        let new_obj = {
            ...membership_item_to_edit,
            membership_id: membershipId,
            periodId: membership_period_type_id,
            isloaded: true,
            membership_payment_status: 'Pending',
            membership_duration: membership_id ? types[0].type_value : null,
            membership_fees: membership_fees,
            membership_to: moment(moment.unix(membership_from).add(types[0].type_value, 'days')).unix(),
            membership_period_type: types[0].type_name
        }
        // console.log(types)
        setfamilyViewState((prev) => ({
            ...prev,
            membership_item_to_edit: new_obj,
            periodSubLookup: types
        }));
    }

    const sendRemind = (e, item) => {
        e.stopPropagation();
        let input = {
            loggedin_user_id: user_id.toString(),
            to_userid: item.user_id.toString(),
            group_id: params.group_id.toString()
        }
        sendReminder(input).then((doc) => {
            notifications("success", "success", "Reminder send successfully")
        }).catch((err) => {
            notifications("success", "success", "Something went wrong.")
        })
    }

    /**
     * User add : set the membership to date 
     */
    const setToDate = (ev = null) => {
        let { membership_duration, membership_from } = membership_item_to_edit;
        let _membership_duration = membership_duration ? membership_duration : 0;
        let _m_to = membership_from ? moment.unix(membership_from).add(_membership_duration, "days").unix() : moment().add(_membership_duration, "days").unix();
        if (membership_item_to_edit.periodId == 4) { _m_to = 'Life time' }
        membership_item_to_edit.membership_to = _m_to;
        setfamilyViewState((prev) => ({ ...prev, membershipData: membershipData }));
    }

    /**
     * user add : duration dropdown changes
     */
    const setValduration = (val) => {
        console.log(membership_item_to_edit, val)
        let { membership_id, membership_from } = membership_item_to_edit;
        let DropArr = memberShipTypes['ListLookup'].find(item => item.id == membership_id);
        let { membership_fees, membership_period } = DropArr
        let new_membership_to = moment(moment.unix(membership_from).add(val, 'days')).unix()
        let totalAmount = (val / membership_period) * membership_fees
        let new_obj = { ...membership_item_to_edit, membership_duration: val, membership_fees: totalAmount, membership_to: new_membership_to }
        setfamilyViewState((prev) => ({ ...prev, membership_item_to_edit: new_obj }));
        setToDate();
    }
    /**
     * user add : Membership from dropdown changes
     */
    const setValmfrom = (val) => {
        let { membership_id, membership_duration } = membership_item_to_edit
        let { ListLookup } = memberShipTypes
        let selected_membership_obj = ListLookup.find(item => item.id === membership_id)
        let { types } = selected_membership_obj
        let selectedTypes = types.find(item => item.type_value == membership_duration)
        let new_obj = { ...membership_item_to_edit, membership_from: isNull(val) ? null : moment(val).unix(), membership_to: isNull(val) ? null : moment(moment(val).add(selectedTypes.type_value, 'days')).unix() }
        setfamilyViewState((prev) => ({ ...prev, membership_item_to_edit: new_obj }));
        setToDate();
    }

    const showMembership = () => {
        setfamilyViewState((prev) => ({ ...prev, membershipData: membershipData }));
        callback("showMemberships")
    }
    const toggledisplayPaymentMembershipStatusModal = () => {
        setfamilyViewState(prev => ({ ...prev, pMVisible: !pMVisible }))
    }
    const redirectToMessage = (e, msg_id) => {
        e.stopPropagation();
        history.push({
            pathname: "/topics/create",
            state: { uid: [parseInt(msg_id)], status: 'create' }
        })
    }

    const getFamDetails = () => {
        let param = {
            group_id: `${params.group_id}`,
            user_id: `${user_id}`
        }
        getFamilyDetails(param)
    }
    const displayPaymentTab = (crnt_user_id = null) => {
        setfamilyViewState(prev => ({ ...prev, payment_history_view_enabled: !payment_history_view_enabled, payment_history_view_user_id: crnt_user_id }))
    }
    // const carouselSettings = {
    //     dots: true,
    //   infinite: true,
    //   speed: 500,
    //   slidesToShow: 1,
    //   swipeToSlide: true,
    //   slidesToScroll: 1,
    //   nextArrow: <Button>Next</Button>,
    //   prevArrow: <Button>Prev</Button>
    // };
    // const { Meta } = Card;

    /**
     * callback function from child
     * postCard, Sticky
     */
    const postCardAction = (_params) => {
        console.log("== _params= = == ", _params)
        setfamilyViewState(prev => ({ ...prev, _loader_spin: true }));
        let { type } = _params;
        let { callbackFn } = familyPropValue;

        switch (type) {
            case 'sticky':
                callbackFn = { ...callbackFn, sticky: true }
                familyPropValue = { ...familyPropValue, callbackFn: callbackFn }
                setfamilyViewState(prev => ({ ...prev, familyPropValue: familyPropValue }));
                //getFeeds('post');
                break;
            case 'unsticky':
                callbackFn = { ...callbackFn, sticky: false }
                familyPropValue = { ...familyPropValue, callbackFn: callbackFn }
                setfamilyViewState(prev => ({ ...prev, familyPropValue: familyPropValue }));
                //getFeeds('post');
                break;

            default:
                break;
        }
        setfamilyViewState(prev => ({ ...prev, _loader_spin: false }));
    }


    /**
    * Tab change announcement call
    */
    const handleAnnChange = (e) => {
        let { value } = e.target;
        offset = 0; postLists = [];
        switch (value) {
            case '1':
                read_status = false;
                setfamilyViewState(prev => ({ ...prev, postLists: postLists, read_status: read_status, offset: offset }));
                break;
            case '2':
                read_status = true;
                setfamilyViewState(prev => ({ ...prev, postLists: postLists, read_status: read_status, offset: offset }));
                break;

            default:
                break;
        }
        getFeedsAnnouncement();//call announcmemnt list function
    }

    const AfterRead = () => {
        offset = 0; postLists = [];
        if (read_status) {
            setfamilyViewState(prev => ({ ...prev, offset: offset, postLists: postLists }));
        } else {
            setfamilyViewState(prev => ({ ...prev, offset: offset, postLists: postLists }));
        }
        getFeedsAnnouncement();
    }

    return (
        <section className="familyDetails f-page">
            {loader &&
                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '200px' }}>
                </Spin>
            }
            {<PaymentStatusModal
                pMVisible={pMVisible}
                toggle={toggledisplayPaymentMembershipStatusModal}
                familyDetails={familyDetails}
                user_id={user_id}
                callback={getFamDetails}
            />}
            <Popconfirm
                title={msgTitle}
                // onConfirm={confirm}
                // onCancel={cancel}
                okText="Yes"
                cancelText="No"
            >
            </Popconfirm>
            <Modal
                cancelButtonProps={{ style: { display: 'none' } }}
                title="Basic Modal"
                visible={modalvisible}
                onOk={handleModalOk}
            >
                <p>Oops! This family is no longer available.</p>
            </Modal>
            {loader === false && familyDetails && familyDetails.is_active &&

                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <div className="f-event-details">
                            <header className="f-head">
                                <div className="f-poster">
                                    {/* Edit Poster */}
                                    {familyDetails.user_status === 'admin' &&
                                        <Button className="f-edit-poster-pic" onClick={enableEditMode('coverpic')}><Icon type="camera" /></Button>}
                                    {familyDetails.cover_pic &&
                                        <img src={process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + familyDetails.cover_pic} />
                                    }
                                    {!familyDetails.cover_pic &&
                                        <div className="f-poster-default">
                                            <img src={require('../../../assets/images/default.png')} />
                                            <h4>Upload a photo to be displayed here !</h4>
                                            <p>Size recommendation: 1044 X 400 px</p>
                                        </div>
                                    }
                                    {/* Shadow */}
                                    <span className="f-overlay" onClick={() => openCoverPhoto()} />
                                </div>
                                <div className="f-page-overview">
                                    <span className="f-display-pic">
                                        {familyDetails.logo &&
                                            <img onClick={() => openProfilePhoto()} src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + familyDetails.logo} />
                                        }
                                        {!familyDetails.logo &&
                                            <div className="f-logo-default" >
                                                <img src={require('../../../assets/images/family_logo.png')} />
                                                {/* <h4>Profile Photo</h4> */}
                                                {/* <p>120 x 120 px</p> */}
                                            </div>
                                        }
                                        {/* Edit Profile Pic Button */}
                                        {familyDetails.user_status === 'admin' &&
                                            <Button className="f-edit-pro-pic f-yfix" onClick={enableEditMode('logo')}><Icon type="camera" /></Button>
                                        }
                                    </span>
                                    <div className="f-head">
                                        <h3 className="f-head-title">{familyDetails.group_name}</h3>
                                        <h5>{familyDetails.group_category} <em>by {familyDetails.created_by_name}</em></h5>
                                        <h5 className="f-location"><em>{familyDetails.base_region}</em></h5>
                                        {familyDetails.is_membership && familyDetails.membership_type &&
                                            <Tag
                                                className="f-membership-tag"
                                                onClick={() => toggledisplayPaymentMembershipStatusModal()}
                                            >
                                                {(familyDetails.membership_payment_status === 'Pending' || familyDetails.membership_payment_status === 'Partial') &&
                                                    <Icon
                                                        type="exclamation-circle"
                                                        theme="twoTone"
                                                        twoToneColor="#ff1100"
                                                        style={{ marginRight: '5px' }}
                                                    />}
                                                {familyDetails.membership_type}, till {familyDetails.membership_period_type_id == 4 ? familyDetails.membership_period_type : moment.unix(familyDetails.membership_to).format(" MMM DD YYYY")}
                                            </Tag>}
                                        <span>
                                            <strong>{count && count.familyMembers ? count.familyMembers : 0}</strong> Members
                                            {familyDetails.member_joining !== 1 && familyDetails.user_status !== 'admin' && familyDetails.user_status !== 'member' &&
                                                <strong>{count && count.knownConnections ? count.knownConnections : 0}</strong>
                                            }
                                            {familyDetails.member_joining !== 1 && familyDetails.user_status !== 'admin' && familyDetails.user_status !== 'member' &&
                                                "Known Members"
                                            }
                                            <strong>{count && count.post_count ? count.post_count : 0}</strong>  Posts
                                        </span>
                                    </div>
                                    {familyDetails.member_joining !== 1 && familyDetails.user_status !== 'admin' && familyDetails.user_status !== 'member' &&
                                        <Button className="f-edit-family f-alt-ef1" onClick={() => !(familyDetails.joined_status && familyDetails.joined_status === 'pending') && joinToFamily()}>{familyDetails.joined_status && familyDetails.joined_status === 'pending' ? 'Pending' : 'Join'}</Button>}
                                    {familyDetails.user_status === 'admin' &&
                                        <Dropdown overlay={setting} trigger={['click']} placement="bottomLeft">
                                            <Button className="f-edit-family"><Icon type="setting" /></Button>
                                        </Dropdown>
                                    }
                                </div>
                            </header>
                            <section className="f-sect">
                                <FamilyDetailsSidebar
                                    count={count}
                                    user_status={familyDetails && familyDetails.user_status}
                                    familyPropValue={familyPropValue}
                                    callbackFromParent={callback}
                                    initial={initial}
                                    user_status={familyDetails.user_status}
                                />
                                <span onClick={() => closefamilhsidebar()} className="f-close-familhy-sidebar" style={{ display: 'none' }} />
                                <div className="f-events-main">
                                    <Button onClick={() => togglefamilhsidebar()} className="f-familhy-unfold" style={{ display: 'none' }}><Icon type="menu-unfold" /></Button>
                                    {/* FEED */}
                                    {
                                        sub_item_id == 1 &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <div className="f-title f-randm-ttl" style={{ marginBottom: '10px' }}>
                                                    <h3>Feed</h3>
                                                    {showButtons('post') && <Button
                                                        onClick={() => history.push('/post/create',
                                                            {
                                                                path_: `${window.location.pathname}`,
                                                                type_: 'post',
                                                                groupId: familyDetails.id,
                                                                group_name: familyDetails.group_name
                                                            })
                                                        }
                                                        type="link"
                                                        icon="plus-circle"
                                                        className="f-bttn" />}
                                                    <Button type="link" className="f-btn-back" onClick={() => history.push('/post')}>Public Post</Button>
                                                </div>
                                                <Sticky data={familyPropValue} history={history} callbackParent={postCardAction}></Sticky>
                                                <div className="f-content">
                                                    {_loader_spin &&
                                                        <Spin tip="Loading..." style={{ 'paddingLeft': '45%', 'paddingTop': '35%', 'paddingBottom': '55%' }}>
                                                        </Spin>
                                                    }
                                                    {
                                                        !_loader_spin && postLists && postLists.length == 0 ? 'NO FEED AVAILABLE' : postLists && postLists.map((v, index) => {
                                                            v.isGroupAdmin = isGroupAdmin
                                                            return v.shared_user_names ? <PostsCard
                                                                history={history}
                                                                callbackParent={postCardAction}
                                                                type="2"
                                                                data={v}
                                                                key={index}
                                                                chatVisible={false}
                                                            /> :
                                                                <PostsCard
                                                                    history={history}
                                                                    callbackParent={postCardAction}
                                                                    type="1"
                                                                    data={v}
                                                                    key={index}
                                                                    chatVisible={false}
                                                                />;
                                                        })
                                                    }


                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* ANNOUNCEMENTS */}
                                    {
                                        sub_item_id == 2 &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <div className="f-title f-randm-ttl" style={{ marginBottom: '10px' }}>
                                                    <h3>Announcements</h3>
                                                    {showButtons('ann') && <Button
                                                        type="link"
                                                        icon="plus-circle"
                                                        onClick={create}
                                                        className="f-bttn" />}

                                                </div>
                                                <div className="f-posts-filters">
                                                    <div className="f-tab-ui">
                                                        <Radio.Group defaultValue="1" onChange={(e) => handleAnnChange(e)}>
                                                            <Radio value="1">Announcements</Radio>
                                                            <Radio value="2">Read Announcements</Radio>
                                                        </Radio.Group>
                                                    </div>
                                                </div>
                                                <div className="f-content">
                                                    {_loader_spin &&
                                                        <Spin tip="Loading..." style={{ 'paddingLeft': '45%' }}>
                                                        </Spin>
                                                    }
                                                    {
                                                        !_loader_spin && postLists && postLists.length == 0 ? 'NO ANNOUNCEMENTS AVAILABLE' : postLists && postLists.map((v, index) => {
                                                            return v.shared_user_names ?
                                                                <PostsCard
                                                                    type="3"
                                                                    data={v}
                                                                    key={index}
                                                                    history={history}
                                                                    chatVisible={false}
                                                                    callParentAfterDelete={AfterRead}
                                                                /> :
                                                                <PostsCard
                                                                    history={history}
                                                                    type="3"
                                                                    data={v}
                                                                    chatVisible={false}
                                                                    key={index}
                                                                    callParentAfterDelete={AfterRead} />;
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {
                                        sub_item_id == 20 && payment_history_view_enabled &&
                                        <PaymentHistory
                                            paymentHistoryByUserid={paymentHistoryByGroupid}
                                            userId={payment_history_view_user_id}
                                            groupId={familyDetails.id}
                                            forUsers={false}
                                        />
                                    }
                                    {/* MEMBER LISTING */}
                                    {
                                        sub_item_id == 3 &&
                                        <div className="f-event-contents f-blanco">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    {payment_history_view_enabled ?
                                                        <PaymentHistory
                                                            paymentHistoryByUserid={paymentHistoryByUserid}
                                                            userId={payment_history_view_user_id}
                                                            groupId={familyDetails.id}
                                                            forUsers={true}
                                                        />
                                                        : <div className="f-member-listing">
                                                            <div className="f-content-searchable">
                                                                <h4><strong>{groupMemberList && groupMemberList.data && groupMemberList.data.length}</strong> {groupMemberList && groupMemberList.data && groupMemberList.data.length > 1 ? 'Members' : 'Member'} <Button type="link" icon="plus-circle" onClick={() => callback('12')} /></h4>
                                                                <Search
                                                                    placeholder="Search members" onChange={(e) => listGroupMember(e)}
                                                                />
                                                            </div>
                                                            <div className="f-member-listing-cards">
                                                                {(!tabloader && groupMemberList && groupMemberList.data && groupMemberList.data.length) ?
                                                                    groupMemberList.data.map((item, i) => {
                                                                        return <Card key={i} className="f-member-cardx">
                                                                            <div className="f-start">
                                                                                <div className="f-left" onClick={() => navigateToUserPage(item)}>
                                                                                    <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                                                    {item && item.user_type && item.user_type === 'admin' &&
                                                                                        <i>{item.user_type}</i>
                                                                                    }
                                                                                </div>
                                                                                <div className="f-right">
                                                                                    <h4>{item.full_name}</h4>
                                                                                    <h5><strong>{item.relation_ship}</strong></h5>
                                                                                    <h6>Member since {moment(item.member_since).format('MMM YYYY')}</h6>
                                                                                    <div className="f-right-combo" style={{ marginTop: '10px' }}>
                                                                                        {familyDetails.group_category === 'Regular' && !item.relation_ship && <Button onClick={() => listRoleOrRelation('regular', '', item)}>Add Relation</Button>}
                                                                                        {familyDetails.group_category != 'Regular' && !item.relation_ship && <Button onClick={() => listRoleOrRelation('org', '', item)}>Add Role</Button>}
                                                                                        {item.relation_ship && <Button className="f-role-limit" onClick={() => listRoleOrRelation((familyDetails.group_category === 'Regular' ? 'regular' : 'org'), '', item, true)} title={item.relation_ship}>{item.relation_ship}</Button>}
                                                                                    </div>
                                                                                </div>
                                                                                {groupMemberList && groupMemberList.admin && groupMemberList.admin === true && item.user_id != user_id &&
                                                                                    <Dropdown overlay={() => menu(item)} trigger={['click']} onClick={(e) => propagation(e, item)} placement="bottomLeft">
                                                                                        <div className="f-edge" style={{ 'marginBottom': '50px' }}><Button /></div>
                                                                                    </Dropdown>
                                                                                }

                                                                            </div>
                                                                        </Card>
                                                                    }) : !tabloader && 'No results to show'
                                                                }
                                                            </div>
                                                        </div>}
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* MEMBER REQUESTS */}
                                    {
                                        sub_item_id == 11 &&
                                        <div className="f-event-contents f-blanco">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    {/* <MemberListing group_id={params.group_id} /> */}
                                                    <div className="f-member-listing">
                                                        <div className="f-content-searchable">
                                                            <h4><strong>{groupRequestList && groupRequestList.length}</strong>{groupRequestList && groupRequestList.length > 1 ? 'Members Requests' : 'Members Request'} </h4>
                                                            {/* <Search
                                                            placeholder="Search friends in the family"
                                                            /> */}
                                                        </div>
                                                        <div className="f-member-listing-cards">
                                                            {groupRequestList && groupRequestList.length > 0 && !tabloader &&
                                                                groupRequestList.map((item, index) => {
                                                                    return <Card key={index} className="f-member-cardx">
                                                                        <div className="f-start">
                                                                            <div className="f-left">
                                                                                <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                                            </div>
                                                                            <div className="f-right">
                                                                                <h4>{item.full_name}</h4>
                                                                                <h5>Wants to join this family.</h5>
                                                                                {((familyDetails.member_approval === 4 && familyDetails.user_status === 'admin') || (familyDetails.member_approval === 5 && (familyDetails.user_status === 'member' || familyDetails.user_status === 'admin'))) &&
                                                                                    <div className="f-right-combo">
                                                                                        <Button onClick={() => acceptOrReject('accepted', 'request', item)}>Accept</Button>
                                                                                        <Button onClick={() => acceptOrReject('rejected', 'request', item)} className="f-line">Reject</Button>
                                                                                    </div>
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    </Card>
                                                                })
                                                            }
                                                        </div>
                                                        {/* <div className="f-member-listing-cards">
                                                        {linkRequestArray && linkRequestArray.length > 0 && !tabloader &&
                                                        linkRequestArray.map((item, index) => {
                                                        return <Card className="f-member-cardx">
                                                        <div className="f-start">
                                                        <div className="f-left">
                                                        <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                        </div>
                                                        <div className="f-right">
                                                        <h4>{item.full_name}</h4>
                                                        <h5>Wants to join this family.</h5>
                                                        {((familyDetails.member_approval === 4 && familyDetails.user_status === 'admin') || (familyDetails.member_approval === 5 && (familyDetails.user_status === 'member' || familyDetails.user_status === 'admin'))) &&
                                                        <div className="f-right-combo">
                                                        <Button onClick={() => acceptOrReject('accepted', 'fetch_link', item)}>Accept</Button>
                                                        <Button onClick={() => acceptOrReject('rejected', 'fetch_link', item)} className="f-line">Reject</Button>
                                                        </div>
                                                        }
                                                        </div>
                                                        </div>
                                                        </Card>
                                                        })
                                                        }
                                                        {linkRequestArray && linkRequestArray.length === 0 && groupRequestList && groupRequestList.length === 0 && !tabloader &&
                                                        <div>NO DATA</div>
                                                        }
                                                        </div> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* MEMBER INVITE */}
                                    {
                                        sub_item_id == 12 &&
                                        <div className="f-event-contents f-blanco">
                                            <div className="f-posts-filters">
                                                <Button className="f-postside-unfold" style={{ display: 'none' }}><Icon type="menu-unfold" /></Button>
                                                <div className="f-tab-ui">
                                                    <Radio.Group defaultValue="people" onChange={(e) => _handleChange(e)}>
                                                        <Radio value="people">PEOPLE</Radio>
                                                        <Radio value="others">Others</Radio>
                                                    </Radio.Group>
                                                </div>
                                            </div>
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    {/* <MemberListing group_id={params.group_id} /> */}
                                                    <div className="f-member-listing">
                                                        {invite === 'people' &&
                                                            <div className="f-content-searchable">
                                                                <h4><strong>{memberListDetails && memberListDetails.rows && memberListDetails.rows.length ? memberListDetails.rows.length : 0}</strong> Connections</h4>
                                                                <Search
                                                                    placeholder="Search members" onChange={(e) => searchAllMembersToInvite(e)}
                                                                />
                                                            </div>}
                                                        {invite === 'people' &&
                                                            <div className="f-member-listing-cards">
                                                                {(memberListDetails && memberListDetails.rows && memberListDetails.rows.length > 0) ?
                                                                    memberListDetails.rows.map((item, index) => {
                                                                        return <Card key={index} className="f-member-cardx">
                                                                            <div className="f-start">
                                                                                <div className="f-left">
                                                                                    <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                                                </div>
                                                                                <div className="f-right">
                                                                                    <h4>{item.full_name}</h4>
                                                                                    <h5>{item.location}</h5>
                                                                                    {/* <div className="f-right-combo">
                                                                                    <Button className="f-line">Cancel Invitation</Button>
                                                                                    </div> */}
                                                                                    {item && itemIngroupMemberList(item) &&
                                                                                        <div className="f-right-combo">
                                                                                            <Button >Member</Button>
                                                                                        </div>
                                                                                    }
                                                                                    {item && !itemIngroupMemberList(item) && !item.exists && !item.invitation_status &&
                                                                                        <div className="f-right-combo">
                                                                                            <Button onClick={() => { addMemberToFamily(item) }}>Add To Family</Button>
                                                                                        </div>
                                                                                    }
                                                                                    {item && !itemIngroupMemberList(item) && !item.exists && item.invitation_status &&
                                                                                        <div className="f-right-combo">
                                                                                            <Button>Pending</Button>
                                                                                        </div>
                                                                                    }
                                                                                    {item && !itemIngroupMemberList(item) && item.exists &&
                                                                                        <div className="f-right-combo">
                                                                                            <Button>Member</Button>
                                                                                        </div>
                                                                                    }

                                                                                </div>
                                                                            </div>
                                                                        </Card>
                                                                    }) : !tabloader && 'No results to show'

                                                                }
                                                            </div>
                                                        }
                                                        {invite === 'others' &&
                                                            <div className="f-member-listing-cards">
                                                                <div className="f-invite-other">
                                                                    <Button className="f-fill" onClick={() => bulk_showModal()} style={{ fontSize: '12px', float: 'right', marginBottom: '15px' }}>Bulk Upload</Button>
                                                                    <div className="f-inv-title">
                                                                        <img src={require('../../../assets/images/invite.png')} />
                                                                        <h4>Let’s invite your friends</h4>
                                                                        <p>Inviting your lovedones is just a touch away!</p>
                                                                    </div>
                                                                    <div className="f-inv-left">
                                                                        <div className="f-form-control">
                                                                            <label>Name*</label>
                                                                            <Input onChange={(e) => onChangeFunction(e, 'name')} value={name} />
                                                                        </div>
                                                                        <div className="f-form-control">
                                                                            <label>Phone*</label>
                                                                            <div className="f-form-phone">
                                                                                <Select
                                                                                    value={selectValue}
                                                                                    showSearch
                                                                                    onChange={(e) => onPhoneCodeChange(e)}
                                                                                    style={{ 'width': '250px' }}
                                                                                    defaultValue="1"
                                                                                >
                                                                                    {countryList.map((value, index) => {
                                                                                        return <Option key={index} value={value.name + '/' + value.code}>{`${value.name} (${value.code})`}</Option>
                                                                                    })}
                                                                                </Select>
                                                                                <Input onChange={(e) => onChangeFunction(e, 'phone')} value={phone} namemaxLength={10} />
                                                                            </div>
                                                                        </div>
                                                                        <div className="f-form-control">
                                                                            <label>Email</label>
                                                                            <Input onChange={(e) => onChangeFunction(e, 'email')} value={email} />
                                                                        </div>
                                                                        <div className="f-form-control">
                                                                            <Button onClick={(e) => inviteMember()} className="f-button">Invite</Button>
                                                                        </div>
                                                                    </div>
                                                                    {/* Modal - Bulk Upload */}
                                                                    <Modal title="Bulk Upload"
                                                                        visible={bulk_visible}
                                                                        onCancel={() => bulk_handleCancel()}
                                                                        onOk={() => bulk_handleCancel()}
                                                                        destroyOnClose={true}
                                                                        className="f-modal f-modal-wizard">
                                                                        <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                                                                            {!ToastModal && <p>Invite multiple people by uploading a CSV file. <a href={process.env.REACT_APP_S3_BASE_URL + 'bulkupload.csv'} download="bulkupload.csv">Click to download</a> CSV format.</p>}
                                                                            <div className="f-form-control">
                                                                                {!csvUploadingStatus ?
                                                                                    (ToastModal ? <ModalToast /> : <Upload
                                                                                        fileList={_fileList}
                                                                                        showUploadList={true}
                                                                                        multiple={false}
                                                                                        accept=".csv"
                                                                                        beforeUpload={(file) => beforeUpload(file)}
                                                                                        onRemove={(e) => removeFile(e)}
                                                                                    >
                                                                                        {/* <Upload
                                                                                    onChange={(e) => CSVupload(e)}
                                                                                    multiple={false}
                                                                                    accept=".csv"
                                                                                    action=''
                                                                                    onRemove={false}
                                                                                    //fileList={_fileList}
                                                                                    > */}
                                                                                        <span style={{ margin: '10px 0' }}><Icon type="upload" /> &nbsp; Upload CSV</span>
                                                                                    </Upload>) : <Spin tip="Uploading CSV" style={{ 'paddingLeft': '40%' }}></Spin>
                                                                                }
                                                                            </div>
                                                                            {/* {!csvUploadingStatus && csvUploadResponse == 'success' &&
                                                                            <p>Successfully Uploaded File {csvUploadResponseMessage}</p>
                                                                            } */}
                                                                        </div>
                                                                        {_fileList && _fileList[0] && _fileList[0].type && !csvUploadingStatus && !ToastModal &&
                                                                            <div className="f-modal-wiz-footer">
                                                                                <Button className="f-fill" onClick={(e) => CSVupload(_fileList[0])}>Invite</Button>
                                                                            </div>
                                                                        }
                                                                    </Modal>

                                                                </div>
                                                            </div>
                                                        }
                                                        <div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* MEMBER BLOCKED */}
                                    {
                                        sub_item_id == 13 &&
                                        <div className="f-event-contents f-blanco">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    {/* <MemberListing group_id={params.group_id} /> */}
                                                    <div className="f-member-listing">
                                                        <div className="f-content-searchable">
                                                            <h4><strong>{blockedList.length}</strong>{blockedList.length.length > 0 ? 'Blocked Members' : 'Blocked Member'} </h4>
                                                            {/* <Search
                                                            placeholder="Search"
                                                            /> */}
                                                        </div>
                                                        <div className="f-member-listing-cards">
                                                            {blockedList && blockedList.length > 0 && !tabloader &&
                                                                blockedList.map((item, index) => {
                                                                    return <Card key={index} className="f-member-cardx">
                                                                        <div className="f-start">
                                                                            <div className="f-left">
                                                                                <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                                            </div>
                                                                            <div className="f-right">
                                                                                <h4>{item.full_name}</h4>
                                                                                <h5>Houston, TX</h5>
                                                                                {familyDetails.user_status === 'admin' &&
                                                                                    <div className="f-right-combo">
                                                                                        <Button onClick={() => unblockUser(item)}>Unblock</Button>
                                                                                    </div>
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    </Card>
                                                                })
                                                            }
                                                            {blockedList && blockedList.length === 0 && !tabloader &&
                                                                <div> NO DATA</div>
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {sub_item_id == 21 && <TableInvited
                                        invited_members={invited_members}
                                    />}
                                    {/* PENDING REQUEST */}
                                    {
                                        sub_item_id == 15 &&
                                        <div className="f-event-contents f-blanco">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    {/* <MemberListing group_id={params.group_id} /> */}
                                                    <div className="f-member-listing">
                                                        <div className="f-content-searchable">
                                                            <h4><strong>{pendingReq.length}</strong>{pendingReq.length > 0 ? 'Pending Requests' : 'Pending Request'} </h4>
                                                            {/* <Search
                                                            placeholder="Search"
                                                            /> */}
                                                        </div>
                                                        <div className="f-member-listing-cards">
                                                            {pendingReq && pendingReq.length > 0 && !tabloader &&
                                                                pendingReq.map((item, index) => {
                                                                    return <Card key={index} className="f-member-cardx">


                                                                        <div className="f-start">
                                                                            <div className="f-left">
                                                                                <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                                            </div>
                                                                            <div className="f-right">
                                                                                <h4>{item.full_name}</h4>

                                                                                <div className="f-right-combo">
                                                                                    <Button onClick={() => _deleteRequest(item.id)} className="f-line">Cancel</Button>
                                                                                </div>

                                                                            </div>
                                                                        </div>


                                                                    </Card>
                                                                })
                                                            }
                                                            {pendingReq && pendingReq.length === 0 && !tabloader &&
                                                                <div> NO DATA</div>
                                                            }

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }


                                    {/* EVENTS */}
                                    {
                                        sub_item_id == 4 &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <div className="f-title">
                                                    <h3>Events</h3>
                                                </div>
                                                <div className="f-content">
                                                    {groupsEvents &&
                                                        <EventCard
                                                            from_family={true}
                                                            handleButtonClick={handleButtonClick}
                                                            type={'familyEventDisplay'}
                                                            exploreEventArray={groupsEvents}
                                                            history={history}
                                                            eventimageUri={require('../../../assets/images/default_event_image.png')}
                                                            searchEvent={searchEvent}
                                                            handleButtonClick={handleButtonClick}
                                                        />
                                                    }
                                                    <EventCreate
                                                        callback={listGroupEvents}
                                                        group_id={params.group_id}
                                                        _modal_visible={create_event_flag}
                                                        handleButtonClick={handleButtonClick}
                                                        history={history}
                                                    />
                                                    {/* {groupsEvents && groupsEvents.length == 0 && !tabloader &&
                                                    <div> No events till now
                                                    <br />
                                                    <button onClick={() => history.push('/events')}>Create new</button>
                                                    </div>
                                                    } */}
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* ALBUM */}
                                    {
                                        sub_item_id == 5 &&

                                        <div className="f-event-contents f-blanco">
                                            {/* Album List */}
                                            <div className="f-switch-content">
                                                <div className="f-content">
                                                    <div className="f-clearfix">
                                                        <div className="f-member-listing">
                                                            <div className="f-content-searchable">
                                                                {/* {user_id && <Button onClick={() => al_showModal()} className="f-btn">Create Album</Button>} */}
                                                                {(isGroupAdmin ? true : (familyDetails.post_create == 6 ? true : false)) && <CreateAlbum
                                                                    getAlbums={getAlbums}
                                                                    type={'groups'}
                                                                    groupId={params.group_id}
                                                                    item_to_edit={item_to_edit}
                                                                    setAlbumToEdit={setAlbumToEdit}
                                                                    familyDetails={familyDetails}
                                                                />}
                                                                <Search onChange={onAlbumSearch} placeholder="Discover" />
                                                            </div>
                                                            <div className="f-content-searchable">
                                                                <h4><strong>{groupAlbums && groupAlbums.length ? groupAlbums.length : 0}</strong> Albums</h4>
                                                            </div>
                                                            <AlbumCard
                                                                searchTag={searchTag}
                                                                setAlbumToEdit={setAlbumToEdit}
                                                                displayContent={groupAlbums}
                                                                groupId={params.group_id}
                                                                history={history}
                                                                is_admin={familyDetails.user_status === "admin" ? true : false}
                                                                _getAlbums={getAlbums}
                                                            ></AlbumCard>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* Album List End */}
                                        </div>
                                    }

                                    {/* DOCUMENTS */}
                                    {
                                        sub_item_id == 6 &&
                                        <div className="f-clearfix">
                                            <Documents
                                                familyDetails={familyDetails}
                                                dc_showModal={dc_showModal}
                                                sh_showModal={sh_showModal}
                                                _listEventFolders={listGroupFolders}
                                                folder_for='groups'
                                                user_id={user_id}
                                                event_id={params.group_id}
                                                dc_visible={dc_visible}
                                                dc_handleCancel={dc_handleCancel}
                                                removeFolder={removeFolder}
                                                groupId={familyDetails.id}
                                            />
                                            <div className="f-clearfix">
                                                <div className="f-clearfix">
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* LINKED FAMILIES */}
                                    {
                                        sub_item_id == 7 &&
                                        <div className="f-event-contents f-blanco">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    <div className="f-member-listing">
                                                        <div className="f-content-searchable">
                                                            <h4><strong>{linkedFamilyList ? linkedFamilyList.length : 0}</strong> Linked Families <Button type="link" icon="plus-circle" onClick={() => fetchlinkfamilies()} /></h4>
                                                            <Search placeholder="Search families" onChange={(value) => listAllLinkedFamily(value)} />
                                                        </div>
                                                        <div className="f-member-listing-cards">
                                                            {linkedFamilyList && linkedFamilyList.length > 0 && !tabloader &&
                                                                linkedFamilyList.map((item, index) => {
                                                                    return (
                                                                        <React.Fragment>
                                                                            {item.status != 'unlinked' &&
                                                                                <Card key={index} className="f-member-cardx" onClick={() => navigateToFamily(item)}>
                                                                                    <div className="f-start">
                                                                                        <div className="f-left">
                                                                                            <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/def_x.png')} />
                                                                                        </div>
                                                                                        <div className="f-right">
                                                                                            <h4>{item.group_name}</h4>
                                                                                            <h5>{item.base_region}</h5>
                                                                                            <div className="f-right-combo">
                                                                                                {item.status === 'accepted' &&
                                                                                                    <Button onClick={(e) => unlinkOrLinkFamily('unlinked', item, e)}>Unlink</Button>
                                                                                                }
                                                                                                {item.status === 'unlinked' &&
                                                                                                    <Button onClick={(e) => unlinkOrLinkFamily('accepted', item, e)}>Link</Button>
                                                                                                }
                                                                                                {item.status === 'pending' &&
                                                                                                    <Button >Pending</Button>
                                                                                                }
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </Card>
                                                                            }
                                                                        </React.Fragment>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                        {linkedFamilyList && linkedFamilyList.length === 0 && !tabloader &&
                                                            <div className="f-event-contents">
                                                                <div className="f-switch-content" >
                                                                    <div className="f-content">
                                                                        <div className="f-empty-content">
                                                                            <img src={require('../../../assets/images/link-fam.png')} style={{ borderRadius: '0', maxWidth: '250px' }} />
                                                                            {/* <h4>You’ve not linked families yet!</h4> */}
                                                                            <h5>Search for other families here...</h5>
                                                                            <div className="f-clearfix">
                                                                                <Button onClick={() => fetchlinkfamilies()}>Link Families</Button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* LINKED FAMILIES - REQUESTS */}
                                    {
                                        sub_item_id == 14 &&
                                        <div className="f-event-contents f-blanco">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    <div className="f-member-listing">
                                                        <div className="f-content-searchable">
                                                            <h4><strong>{linkRequestArray ? linkRequestArray.length : 0}</strong>Requests</h4>
                                                            {/* <Search placeholder="Search families" /> */}
                                                        </div>
                                                        <div className="f-member-listing-cards">
                                                            {linkRequestArray && linkRequestArray.map((item) => {
                                                                return <Card key={item.id} className="f-member-cardx">
                                                                    <div className="f-start">
                                                                        <div className="f-left">
                                                                            <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/family_logo.png')} />
                                                                        </div>
                                                                        <div className="f-right">
                                                                            <h4>{item.group_name}</h4>
                                                                            <h6>By {item.full_name}</h6>
                                                                            <h5>{item.base_region}</h5>
                                                                            <div className="f-right-combo">
                                                                                <Button onClick={() => approveORrejectLinkRequest(item.id, "accepted")}>Accept</Button>
                                                                                <Button onClick={() => approveORrejectLinkRequest(item.id, "rejected")} className="f-line">Reject</Button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </Card>
                                                            })}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* CALENDAR */}
                                    {
                                        sub_item_id == 8 &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <div className="f-title">
                                                    <h3>Calendar</h3>
                                                </div>
                                                <div className="f-famil-calendar">
                                                    <Calender
                                                        passed_id={familyDetails.id}
                                                    ></Calender>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* MY FEEDBACK */}

                                    {
                                        sub_item_id == 9 &&
                                        <div className="f-event-contents">
                                            {tabloader &&
                                                <Spin style={{ 'paddingLeft': '20%', 'paddingTop': '20%', 'position': 'absolute', 'z-index': '999999' }}>
                                                </Spin>
                                            }
                                            <div className="f-switch-content" >
                                                <div className="f-title">
                                                    <h3>MY FEEDBACK</h3>
                                                </div>
                                                {/* <div className="f-content">
                                                FEEDBACK NOT AVAILABLE
                                                </div> */}
                                                <div className="f-modal-wiz-body">
                                                    <div className="f-form-control">
                                                        <label>Title*</label>
                                                        <Input value={title} placeholder="Enter Title" onChange={(e) => handleChange('title', e)}></Input>
                                                    </div>
                                                    <div className="f-form-control">
                                                        <label>Image</label>
                                                        <Upload {...props} onChange={(event) => uploadPhoto(event)}>
                                                            <Icon type="upload" /> Click to Upload
                                                        </Upload>
                                                    </div>
                                                    <div className="f-form-control">
                                                        <label>Description*</label>
                                                        <TextArea rows={5} placeholder="Description" onChange={(e) => handleChange('description', e)} value={description} />
                                                    </div>
                                                    <div className="f-form-control" key="1" >
                                                        <Button className="f-button" key="submit" onClick={() => handleOk()}>Submit <Icon type="arrow-right" /></Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* ABOUT US */}
                                    {
                                        sub_item_id == 10 &&
                                        <About
                                            history={history}
                                            user_id={user_id}
                                            famLinkValue={famLinkValue}
                                            handleChange={handleChange}
                                            setfamilyViewState={setfamilyViewState}
                                            url={url}
                                            _familyDetails={familyDetails}
                                            familyLinkExist={familyLinkExist}
                                            getFamilyDetails={getFamilyDetails}
                                        />
                                    }
                                    {
                                        sub_item_id == 16 && <RequestList
                                            from_family={familyDetails}
                                            history={history}
                                            groupId={familyPropValue.group_id}
                                            admin_status={familyDetails.user_status === 'admin'}
                                            sideBarVisible={false}
                                            style_passed={{ paddingTop: '0px' }}
                                        />
                                    }

                                    {/* LEAVE FAMILY */}
                                    {sub_item_id === 'leave_family' &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <Modal
                                                    title="LEAVE FAMILY"
                                                    visible={visibleleaveModal}
                                                    onOk={() => handleOkLeave()}
                                                    okText={'Yes'}
                                                    onCancel={() => handleCancelLeave()}
                                                    cancelText={"No"}
                                                // okButtonProps={{ disabled: true }}
                                                // cancelButtonProps={{ disabled: true }}
                                                >
                                                    <p>Do you want to leave this family?</p>
                                                </Modal>
                                            </div>
                                        </div>}
                                    {sub_item_id === 'payment' &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <PaymentModal
                                                    visiblepaymentmodal={visiblepaymentmodal}
                                                />
                                            </div>
                                        </div>}
                                    {
                                        !tabloader && sub_item_id == 17 &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <div className="f-title f-randm-ttl">
                                                    <h3>Memberships</h3>
                                                    <Button type="link" className="f-bttn" icon="plus-circle" onClick={() => showFormMship()} />
                                                </div>
                                                <div className="f-filter-ui f-filter-membership">
                                                    <Radio.Group defaultValue={radioVal} onChange={(e) => radioSelect(e)}>
                                                        <Radio value={1}>All</Radio>
                                                        <Radio value={'current'}>Current ({memberShipTypes.membership_counts ? memberShipTypes.membership_counts[0].active_count : 0})</Radio>
                                                        <Radio value={'overdue'}>Expired ({memberShipTypes.membership_counts ? memberShipTypes.membership_counts[0].expaired_count : 0})</Radio>
                                                    </Radio.Group>
                                                </div>
                                                {memberShipTypes.membership_counts && memberShipTypes.membership_counts.total_due_amount &&
                                                    <div className="f-membership-due">
                                                        <p>Total Due Amount:</p>
                                                        <strong>{memberShipTypes.membership_counts.total_due_amount}</strong>
                                                    </div>}
                                                <div className="f-membership-dash">
                                                    {(memberShipTypes && memberShipTypes.membership_data && memberShipTypes.membership_data.length > 0) ?
                                                        memberShipTypes.membership_data.map((item) => {
                                                            return <div key={item.membershipid} className="f-single-membership" onClick={() => fetch_mDetails(item)}>
                                                                <strong>{item.member_count}</strong>
                                                                <p>{item.membership_name}</p>
                                                            </div>
                                                        }) : <div className="f-membershi-dash-empty"><p>No Memberships!</p><Button className="f-btn" onClick={() => showFormMship()}>Add Membership</Button></div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {
                                        !tabloader && sub_item_id == "showAdd" &&
                                        <div className="f-event-contents f-add-membership">
                                            <div className="f-switch-content" >
                                                <div className="f-title f-randm-ttl">
                                                    <Button className="f-create-form-back" onClick={() => callback('17')} />
                                                    <h3>{membershipData.type ? membershipData.type : 'Add'} Membership Type</h3>
                                                </div>
                                                <div className="f-switch-content" >
                                                    <Form className="f-form-layout" onSubmit={(e) => handleSubmit(e)}>
                                                        <div className="f-form-control">
                                                            <label>Membership type*</label>
                                                            <Form.Item>
                                                                {getFieldDecorator("membership_name", {
                                                                    initialValue: membershipData.membership_name ? membershipData.membership_name : "",
                                                                    rules: [{
                                                                        required: true,
                                                                        message: "Membership type is mandatory."
                                                                    }]
                                                                })(<Input placeholder="Eg: Premium" />)}
                                                            </Form.Item>
                                                        </div>
                                                        <div className="f-form-control">
                                                            <label>Period Type*</label>
                                                            {getFieldDecorator('membership_period', {
                                                                initialValue: membershipData.membership_period_type_id ? membershipData.membership_period_type_id : 1,
                                                                rules: [{
                                                                    required: true,
                                                                    message: "Period Type is mandatory."
                                                                }]
                                                            })(
                                                                <Select style={{ width: '100%' }} placeholder="Period Type" >
                                                                    {
                                                                        periodLookup.length > 0 && periodLookup.map((item) => {
                                                                            return (
                                                                                <Option value={item.id}>
                                                                                    {item.membership_lookup_period_type}
                                                                                </Option>)
                                                                        })
                                                                    }
                                                                </Select>
                                                            )}
                                                        </div>
                                                        <div className="f-form-control">
                                                            <label>Fees*</label>
                                                            <Form.Item style={{ "display": "none" }} className="f-membership-fix">
                                                                {getFieldDecorator('membership_currency', {
                                                                    initialValue: membershipData.membership_currency ? membershipData.membership_currency : "USD",
                                                                })(
                                                                    <Select showSearch>
                                                                        {
                                                                            currency && currency.map((item, index) => {
                                                                                return (<Option key={index} value={item.code}>{item.name}</Option>)
                                                                            })
                                                                        }
                                                                    </Select>
                                                                )}
                                                            </Form.Item>
                                                            <Form.Item>
                                                                {getFieldDecorator("membership_fees", {
                                                                    initialValue: membershipData.membership_fees ? membershipData.membership_fees : "",
                                                                    rules: [{
                                                                        required: true,
                                                                        message: "Fees is mandatory."
                                                                    }]
                                                                })(<Input type="number" placeholder="Eg: 1000" />)}
                                                            </Form.Item>
                                                        </div>
                                                        <div className="f-form-control">
                                                            <label>Active</label>
                                                            <Form.Item>
                                                                {getFieldDecorator('is_active', {
                                                                    initialValue: membershipData.is_active ? membershipData.is_active : true,
                                                                })(
                                                                    <Switch defaultChecked ></Switch>
                                                                )}
                                                            </Form.Item>
                                                        </div>
                                                        <div className="f-form-control">
                                                            <Button htmlType="submit" className="f-button">{membershipData.type ? 'Update' : 'Save'}</Button>
                                                        </div>
                                                    </Form>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {
                                        !tabloader && sub_item_id == "showMemberships" &&
                                        <div className="f-event-contents f-blanco f-membership-members">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    <div className="f-member-listing">
                                                        <div className="f-content-searchable">
                                                            <Button className="f-create-form-back" onClick={() => callback('17')} />
                                                            <h4>
                                                                <strong>{membershipData.membership_name}</strong>
                                                                <em>({membershipData.member_count})</em>
                                                                {membershipData.membership_name !== 'Unassigned' && <em>
                                                                    <img style={{ "cursor": "pointer", marginLeft: '10px' }} src={require('../../../assets/images/icon/edit_g.png')} onClick={() => getMembershipByid(membershipData.membershipid)} />
                                                                </em>}
                                                            </h4>
                                                            {/* <Search placeholder="Search members" /> */}
                                                        </div>
                                                        <div className="f-switch-membership">
                                                            <div className="f-form-control">
                                                                Add Member to plan <Button type="link" className="f-bttn" icon="plus-circle" onClick={() => ListMemberToAdd()} />
                                                            </div>
                                                        </div>
                                                        <div className="f-member-listing-cards">
                                                            {
                                                                showMemberships.length > 0 ? showMemberships.map((item, index) => {
                                                                    let dueAmount = parseInt(item.membership_fees) - parseInt(item.membership_total_payed_amount ? item.membership_total_payed_amount : 0)
                                                                    return (<Card key={index} className="f-member-cardx f-card-badge-inc">
                                                                        {item.membership_payment_status !== 'Pending' && <span className="f-card-badge-colored" style={{ backgroundColor: "#62fa2e" }} />}
                                                                        {(item.membership_payment_status === 'Pending' || item.membership_payment_status === 'Partial') && <span className="f-card-badge-colored" style={{ backgroundColor: "#f8ed11" }} />}
                                                                        <div className="f-start">
                                                                            <div className="f-left">
                                                                                <img
                                                                                    onError={(e) => { e.target.onerror = null; e.target.src = require('../../../assets/images/def_x.png') }}
                                                                                    src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                                            </div>
                                                                            <div className="f-right">
                                                                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                                                    <h4> {item.full_name} &nbsp;</h4>
                                                                                    <em style={{ margin: '0 8px' }}><img style={{ "cursor": "pointer" }} src={require('../../../assets/images/icon/edit_g.png')}
                                                                                        onClick={(e) => EditForm(e, item)} /></em>

                                                                                </div>
                                                                                <h5><strong>{item.membership_type}</strong></h5>
                                                                                {!item.is_expaired && <h6>Valid Till: {item.membership_period_type_id == 4 ? item.membership_period_type : moment.unix(item.membership_to).format(" MMM DD YYYY")}</h6>}
                                                                                {(item.membership_payment_status === 'Pending' || item.membership_payment_status === 'Partial') && <h6><i>Payment Pending</i></h6>}
                                                                                {item.is_expaired && <h6><i>Expired</i></h6>}
                                                                                <h6>Due amount: {dueAmount > 0 ? dueAmount : 0}  </h6>
                                                                                {item.crnt_user_id !== `${item.user_id}` && <div className="f-right-combo">
                                                                                    <Button onClick={(e) => { redirectToMessage(e, item.user_id) }}>Message</Button>
                                                                                    {(item.membership_payment_status === 'Pending' || item.membership_payment_status === 'Partial') &&
                                                                                        <Button onClick={(e) => sendRemind(e, item)}>Send Reminder</Button>}
                                                                                </div>}
                                                                            </div>
                                                                        </div>
                                                                    </Card>)
                                                                }) : <div className="f-event-contents">
                                                                        <div className="f-switch-content" >
                                                                            <div className="f-content">
                                                                                <div className="f-empty-content">
                                                                                    <img src={require('../../../assets/images/create_post.png')} style={{ borderRadius: '0', maxWidth: '250px' }} />
                                                                                    <div className="f-clearfix">
                                                                                        <Button onClick={() => ListMemberToAdd()}>Add Member</Button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {
                                        !tabloader && sub_item_id == "showMemberToAdd" &&
                                        <div className="f-event-contents f-blanco f-membership-members">
                                            <div className="f-switch-content" >
                                                <div className="f-content">
                                                    <div className="f-member-listing">
                                                        <div className="f-content-searchable">
                                                            <Button className="f-create-form-back" onClick={() => callback('17')} />
                                                            <h4> Members to Add</h4>
                                                            {/* <Search placeholder="Search members" /> */}
                                                        </div>
                                                        <div className="f-member-listing-cards">
                                                            {
                                                                showMemberToAdd.length > 0 && showMemberToAdd.map((item, index) => {
                                                                    return (<Card className="f-member-cardx"
                                                                        key={index}
                                                                    // onClick={() => AddForm(item)}
                                                                    >
                                                                        <div className="f-start">
                                                                            <div className="f-left">
                                                                                <img
                                                                                    onError={(e) => { e.target.onerror = null; e.target.src = require('../../../assets/images/def_x.png') }}
                                                                                    src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../assets/images/def_x.png')} />
                                                                            </div>
                                                                            <div className="f-right">
                                                                                <h4>
                                                                                    {item.full_name} &nbsp;
                                                                                    <em>
                                                                                        <img
                                                                                            style={{ "cursor": "pointer" }}
                                                                                            src={require('../../../assets/images/icon/edit_g.png')}
                                                                                            onClick={(e) => EditForm(e, item)}
                                                                                        />
                                                                                    </em>
                                                                                </h4>
                                                                                <h5><strong>{item.membership_type}</strong></h5>
                                                                                {item.membership_to &&
                                                                                    <h6>Valid Till: {item.membership_period_type_id == 4 ? item.membership_period_type : moment.unix(item.membership_to).format(" MMM DD YYYY")}</h6>}
                                                                                {/* <div className="f-right-combo" style={{ marginTop: '10px' }}>
                                                                                <Button>Send Reminder</Button>
                                                                                </div> */}
                                                                            </div>
                                                                            {/* <Dropdown overlay={membership} trigger={['click']} placement="bottomLeft">
                                                                            <div className="f-edge" style={{ 'marginBottom': '50px' }}><Button /></div>
                                                                            </Dropdown> */}
                                                                        </div>
                                                                    </Card>)
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {
                                        !tabloader && sub_item_id == "showMemberEdit" &&
                                        <div className="f-event-contents f-add-membership">
                                            <div className="f-switch-content" >
                                                <div className="f-title f-randm-ttl">
                                                    <Button className="f-create-form-back" onClick={() => showMembership()} />
                                                    <h3></h3>
                                                </div>
                                                <EditmemberShip
                                                    setValmfrom={setValmfrom}
                                                    AddUserToPlan={AddUserToPlan}
                                                    getFieldDecorator={getFieldDecorator}
                                                    membershipData={membership_item_to_edit}
                                                    changeDrop={changeDrop}
                                                    memberShipTypes={memberShipTypes}
                                                    setValduration={setValduration}
                                                    periodSubLookup={periodSubLookup}
                                                    membershipSaveButtonLoader={membershipSaveButtonLoader}
                                                />
                                            </div>
                                        </div>
                                    }
                                    {tabloader &&
                                        <Spin style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                                        </Spin>
                                    }

                                </div>
                                {sub_item_id == 2 && <div className="f-events-main" id="loaderDiv" ref={setElement}></div>}
                            </section>
                            {enableEdit &&
                                <div>
                                    <Modal title="Add/Edit Image"
                                        visible={enableEdit}
                                        onCancel={() => showCropModel()}
                                        footer={null}
                                        className="f-modal f-modal-wizard">
                                        <CustomImageCrop params={params} callBackimgFromParent={dataCallback}></CustomImageCrop>
                                    </Modal>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            }
            <Modal title="Select Families"
                visible={selectFamilyModal}
                onCancel={() => onClose()}
                className="f-modal f-modal-wizard f-modal-wfooter"
                footer={!tabloader &&
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div className="f-modal-wiz-footer">
                            <Button style={{ marginLeft: '15px' }} onClick={() => {
                                // let encrypted_id = encryption(id)
                                history.push(`/create-family-link/createAndLink/${id}`)
                            }}>Create Family</Button>
                        </div>
                        <div className="f-modal-wiz-footer f-flx-end">
                            <Button disabled={!buttonFlag} loading={button_loading} className="f-fill" onClick={(e) => buttonFlag ? linkToSelectedGroups() : ''}>Done</Button>
                            <Button style={{ marginLeft: '15px' }} onClick={() => onClose()}>Cancel</Button>
                        </div>
                    </div>
                }>

                {tabloader &&
                    <Spin style={{ 'padding': '25% 0 25% 50%' }}>
                    </Spin>
                }
                {!tabloader &&
                    <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                        <div className="f-member-listing">
                            <div className="f-content-searchable">
                                <h4><strong>{selectedGroups ? selectedGroups.length : 0}</strong> Families selected</h4>
                            </div>
                            <div className="f-member-listing-cards f-share-scroll">
                                {
                                    fetchFamilyInfo && fetchFamilyInfo.map((v, index) => {
                                        return (
                                            <React.Fragment>
                                                {
                                                    <Card key={index} className="f-member-cardx">

                                                        <div className="f-start">
                                                            <div className="f-left">
                                                                <img onError={(e) => { e.target.src = require('../../../assets/images/create_post.png') }} src={v.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + v.logo : require('../../../assets/images/create_post.png')} />
                                                            </div>
                                                            <div className="f-right">
                                                                <h4>{v.group_name}</h4>
                                                                <h6>By <strong>{v.created_by}</strong></h6>
                                                                <h6>Member since {v.member_since ? v.member_since : ''}</h6>
                                                                <h6 className="f-pin">{v.base_region}</h6>
                                                            </div>
                                                        </div>
                                                        <div className="f-stop">
                                                            <h5>
                                                            </h5>
                                                            <div className="f-right-combo">
                                                                {(v.is_linked === 'pending' || v.is_linked === 'accepted') &&
                                                                    <Checkbox disabled>
                                                                        {v.is_linked}
                                                                    </Checkbox>
                                                                }
                                                                {(v.is_linked !== 'pending' && v.is_linked !== 'accepted') &&
                                                                    <Checkbox onChange={(e) => handleChooseFamilyList(e, v)}>
                                                                        {selectedGrpsID.indexOf(v.id) == -1 ? 'Select' : 'Selected'}
                                                                    </Checkbox>
                                                                }
                                                            </div>
                                                        </div>
                                                    </Card>}
                                            </React.Fragment>

                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>}
                {/* {!tabloader &&
                <div className="f-modal-wiz-footer f-flx-end">
                <Button className="f-fill" onClick={(e) => selectedGroups && selectedGroups.length > 0 ? linkToSelectedGroups() : ''}>Done</Button>
                <Button style={{ marginLeft: '15px' }} onClick={(e) => onClose(e)}>Cancel</Button>
                </div>
                } */}
                {!tabloader && fetchFamilyInfo && fetchFamilyInfo.length === 0 &&
                    <div>NO DATA</div>
                }
            </Modal>
            {/* Modal - Album */}
            <Modal title="Create Album"
                visible={al_visible}
                onCancel={() => al_handleCancel()}
                onOk={() => al_handleCancel()}
                className="f-modal f-modal-wizard">
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-form-control">
                        <label>Album name</label>
                        <Input placeholder="Eg: Nigeria womens group " />
                    </div>
                    <div className="f-form-control">
                        <label>A little bit about your album</label>
                        <TextArea placeholder="250 characters max" rows={4} />
                    </div>
                </div>
                <div className="f-modal-wiz-footer">
                    <Button className="f-fill">Create Album</Button>
                </div>
            </Modal>

            {/* Modal - Document Folder */}
            <Modal title="Create Folder"
                visible={dc_visible}
                onCancel={() => dc_handleCancel()}
                onOk={() => dc_handleCancel()}
                className="f-modal f-modal-wizard">
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-form-control">
                        <label>Folder name</label>
                        <Input placeholder="Eg: Nigeria womens group " onChange={(e) => onFolderObjChange(e, 'folder_name')} />
                    </div>
                    <div className="f-form-control">
                        <label>A little bit about your folder</label>
                        <TextArea placeholder="250 characters max" onChange={(e) => onFolderObjChange(e, 'description')} rows={4} />
                    </div>
                </div>
                <div className="f-modal-wiz-footer">
                    <Button className="f-fill" onClick={() => createFolderAction()}>Create Folder</Button>
                </div>
            </Modal>

            {/* Modal - Add Document */}
            <Modal title="Add Document"
                visible={dcup_visible}
                onCancel={() => dcup_handleCancel()}
                onOk={() => dcup_handleCancel()}
                className="f-modal f-modal-wizard">
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-form-control">
                        <Upload>
                            <img src={require('../../../assets/images/icon/upload.png')} /> Add Document
                        </Upload>
                    </div>
                </div>
                <div className="f-modal-wiz-footer">
                    <Button className="f-fill">Upload</Button>
                </div>
            </Modal>

            {/* Modal Add Role */}
            <Modal
                title={familyDetails.group_category === 'regular' ? 'Select a Relationship' : 'Select a Role'}
                visible={roleRelation}
                onCancel={() => dc_handleCancel()}
                onOk={() => addRoleOrRelation(relationExist)}
                className="f-modal f-modal-wizard f-modal-wfooter">
                <div className="f-form-control">
                    <Input placeholder="Role" value={listInputValue} onChange={(e) => handleRoleList(e)} />
                </div>
                {roleRelationArray && roleRelationArray.length > 0 &&
                    <div className="f-clearfix" style={{ margin: '15px 0' }}>
                        <div className="f-share-scroll">
                            <ul className="f-role-list">
                                {roleRelationArray.map((item, index) => {
                                    return <li onClick={() => selectedList(item)} key={index} value={item.relationship}>{item.relationship}</li>
                                })}
                            </ul>
                        </div>
                    </div>
                }
            </Modal>
            {openCoverPic &&
                <Lightbox
                    mainSrc={process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + familyDetails.cover_pic
                    }
                    onCloseRequest={() => setfamilyViewState(prev => ({ ...prev, openCoverPic: false }))}
                />}
            {openProfilePic &&
                <Lightbox
                    mainSrc={process.env.REACT_APP_S3_BASE_URL + 'logo/' + familyDetails.logo
                    }
                    onCloseRequest={() => setfamilyViewState(prev => ({ ...prev, openProfilePic: false }))}
                />}
        </section>
    );
}
export default withHooksHOC(Form.create()(FamilyView));