import React from 'react';
import { withRouter } from 'react-router-dom';
// import './style.scss'
// import TopBar from '../../../components/TopBar';
import FamilyDetailsSidebar from '../../../components/FamilyDetailsSidebar';
class familyDetails extends React.Component {
    state;
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }

    render() {
        return (
            <section className="familyDetails f-page">
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <div className="f-event-details">
                            <header className="f-head">
                                <div className="f-poster">
                                    <img src={require('../../../assets/images/demo/family_poster.png')} />
                                    <span className="f-overlay" />
                                    <span className="f-display-pic">
                                        <img src={require('../../../assets/images/demo/sample_logo.png')} />
                                    </span>
                                </div>
                            </header>
                            <section className="f-sect">
                                <FamilyDetailsSidebar />
                                <div className="f-events-main">
                                    <div className="f-event-contents">
                                        <div className="f-head">
                                            <h3>Nigeria Women's Group</h3>
                                            <h5 className="f-time">Association &nbsp; <em>by Alisa Joice</em></h5>
                                            <h5 className="f-location"><em>Houston, Dallas, TX</em></h5>
                                        </div>
                                        <div className="f-switch-content">
                                            <div className="f-title">
                                                <h3>About Us</h3>
                                            </div>
                                            <div className="f-content">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pretium sed quam ac tincidunt. Etiam dapibus tristique accumsan. Pellentesque venenatis magna ligula, vitae feugiat ligula dapibus eu. Morbi pretium, lectus ut venenatis commodo, risus felis congue ex, vel viverra ipsum eros quis nisl. Nunc porta molestie ligula vel finibus. Phasellus in felis ac metus lobortis sodales. Phasellus in lectus condimentum, varius risus id, gravida libero. Nulla euismod, neque non pellentesque egestas, neque massa tristique ligula, at bibendum orci sem ut quam. Cras iaculis neque eu blandit eleifend. Praesent eget tortor mauris. Quisque posuere nunc ac turpis lobortis, eu aliquet elit dapibus. Aliquam efficitur massa id urna commodo aliquet. Etiam mollis orci quis finibus aliquet.</p>
                                                <p>Nunc dictum vestibulum vulputate. Donec non nibh massa. Etiam eget sodales mi, ac efficitur magna. Praesent ac enim id orci mattis eleifend sit amet a eros. Sed lobortis nunc vel scelerisque laoreet. Praesent tempor lectus nisl, condimentum volutpat lacus tincidunt vitae. Integer pretium aliquet dolor. Donec vestibulum sit amet neque et molestie. In hac habitasse platea dictumst. Nunc hendrerit nibh vitae ipsum blandit vehicula.</p>
                                                <p>Maecenas erat tellus, accumsan et nibh at, viverra elementum eros. In interdum odio eu ante rhoncus iaculis. Maecenas vitae egestas massa, a scelerisque felis. Pellentesque mattis scelerisque lorem quis molestie. Nunc risus dui, mattis pellentesque sodales sit amet, dictum quis dolor. Morbi dapibus eu sapien porttitor gravida. Maecenas scelerisque elementum accumsan. Suspendisse felis nisi, imperdiet vitae erat vel, interdum tincidunt felis. Integer porttitor faucibus ipsum, lacinia congue neque convallis at. Aenean venenatis, est vel luctus dapibus, dolor urna varius nisi, nec tincidunt felis odio id sem. Phasellus commodo, velit cursus ultricies imperdiet, erat ligula malesuada risus, a iaculis ipsum mauris sit amet enim. In molestie, nisl ut ultricies porta, elit est placerat eros, at pulvinar nulla lectus vel massa.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default withRouter(familyDetails)