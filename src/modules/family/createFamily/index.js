import React, { useState, useEffect } from 'react';
import './style.scss'
import { Button, Modal, Steps, Icon, Input, Select,  Switch, Checkbox, Radio, Spin, Card } from 'antd';
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import ReactGoogleMapLoader from "react-google-maps-loader"

import { notifications } from '../../../shared/toasterMessage';
import { createFamily, checkforDuplicateFamily, joinToExistingFamily, updateFamily, getAllLinkedFamily, requestLinkFamily } from '../../createFamily/api-family'
import CustomImageCrop from '../../ImageCrop/index';
import { encryption, decryption } from '../../../shared/encryptDecrypt';
import { getFamilDetails } from "../familyView/api-familyview";
// import { log } from 'util';
import { useSelector } from 'react-redux'

const MY_API_KEY = decryption(process.env.REACT_APP_PLACE_ENCRYPT)
const { Step } = Steps;
const { Option } = Select;
const { TextArea } = Input;
const CreateMyFamily = ({ history }) => {
  const [createFamilyState, setcreateFamilyState] = useState({
    user_id: useSelector(state => state.user_details.userId),
    // familyType: 'public',
    familyName: '',
    location: '',
    current: 0,
    limitIndex: 10,
    familyDetails: {},
    disableNext: false,
    joinSuggesion: false,
    groupList: [],
    group_id: '',
    nextStage: false,
    familyIntro: '',
    visibility: true,
    searchable: true,
    is_linkable: false,
    showAdvanced: false,
    member_joining: 2,
    member_approval: 4,
    post_create: 6,
    post_visibilty: 8,
    post_approval: 10,
    link_family: 13,
    announcement_create: 17,
    announcement_visibility: 21,
    request_visibility:27,
    link_approval: 16,
    imageModal: false,
    imageType: '',
    params: {
      crop: {
        unit: "%",
        aspect: 261 / 100,
        // height : 100,
        width: 50
      },
      group_id: '',
      editType: ''
    },
    cover_pic: undefined,
    logo: '',
    linkModal: false,
    fetchFamilyList: [],
    selectedGroups: [],
    selectedGrpsID: [],
    group_category: '',
    pageType: 'create',
    spinLoader: false,
    search: "",
    values: "",
    _title: 'Create New Family',
    linkParam: true,
    group_type: undefined,
    stripe_account_id:null
  })
  const { limitIndex, current, familyName, location, groupList, familyDetails, joinSuggesion, user_id, group_id, nextStage, familyIntro,
    visibility, searchable, is_linkable, showAdvanced, member_joining, member_approval, post_create, post_visibilty, post_approval,
    link_family, link_approval, imageModal, params, cover_pic, logo, linkModal, fetchFamilyList, selectedGroups, selectedGrpsID,
    group_category, pageType, spinLoader, search, values, _title, linkParam, announcement_create, announcement_visibility,request_visibility } = createFamilyState;

  useEffect(() => {
    let cipher = window.location.pathname.split('create-family/').pop();
    // let _cipher = window.location.pathname.split('create-family-link/createAndLink/').pop();
    let tabId = window.location.pathname.split('/tabId=').pop();
    let groupId = (cipher != "/create-family" && !cipher.includes('/create-family-link/createAndLink/')) && decryption(cipher);
    // groupId = _cipher!="create-family-link/createAndLink"&&_cipher
    let _params = params;
    let _linkParam = window.location.pathname.split('/')[2];
    if (_linkParam === "createAndLink") {
      setcreateFamilyState(prev => ({ ...prev, linkParam: false }))
    }
    if (tabId && tabId != '' && tabId == '3') {
      setcreateFamilyState((prev) => ({ ...prev, current: 3, showAdvanced: true }));
    }
    if (groupId) {
      _params.group_id = groupId
      setcreateFamilyState((prev) => ({ ...prev, _title: 'Edit Family' }));
    }
    if (groupId && !nextStage) {
      setcreateFamilyState((prev) => ({ ...prev, group_id: groupId, pageType: 'edit', params: _params, spinLoader: true }));
      getFamilyDetails(groupId)
    }
  }, [current, groupList]);
  const getFamilyDetails = (groupId) => {
    getFamilDetails({ user_id: user_id, group_id: groupId }).then((res) => {
      let data = res.data.data[0]
      setcreateFamilyState((prev) => ({
        ...prev, values: data.base_region, location: data.base_region, group_category: data.group_category, familyName: data.group_name, familyIntro: data.intro,
        visibility: (data.visibility || false), searchable: data.searchable, is_linkable: data.is_linkable, member_joining: data.member_joining, member_approval: data.member_approval,
        post_create: data.post_create, post_visibilty: data.post_visibilty, post_approval: data.post_approval, link_family: data.link_family, link_approval: data.link_approval, cover_pic: data.cover_pic, logo: data.logo,
        spinLoader: false, announcement_create: data.announcement_create, announcement_visibility: data.announcement_visibility, group_type: data.group_type,request_visibility:data.request_visibility,
        stripe_account_id:data.stripe_account_id
      }));
    }).catch((err) => {
      setcreateFamilyState((prev) => ({ ...prev, spinLoader: false }))
    });
  }
  const handleChange = (name, e) => {
    e.persist()
    setcreateFamilyState(prev => ({ ...prev, [name]: e.target.value }))
  }
  const handleSelect = (name, event) => {
    // event.persist()
    setcreateFamilyState(prev => ({ ...prev, [name]: event }))
  }
  const handleAdvSelect = (name, event) => {
    if (name == 'visibility') {
      let newVisibility = {
        member_joining: 3,
        post_visibilty: 9,
        announcement_visibility: 22,
        [name]: event
      }
      setcreateFamilyState(prev => ({ ...prev, ...newVisibility }));
    } else {
      setcreateFamilyState(prev => ({ ...prev, [name]: event }))
    }
  }
  const handleAdvChange = (name, e) => {
    setcreateFamilyState(prev => ({ ...prev, [name]: e.target.value }))
  }
  const createsFamily = () => {
    if (linkParam) {
      if (!familyName || !familyName.trim() || !group_category || !location.trim()) {
        let message = 'Error'
        let description = `Please fill all the required fields`
        notifications('error', message, description)
        return;
      }
      if (current === 0 && pageType === 'create') {
        let famDetails = {
          group_name: familyName,
          group_category: group_category,
          base_region: location,
          user_id: user_id
        }
        setcreateFamilyState(prev => ({ ...prev, disableNext: true, familyDetails: famDetails }));
        checkDuplicateFamily(famDetails);
      } else {
        if (current < 4) {
          let count = current + 1;
          setcreateFamilyState(prev => ({ ...prev, current: count, disableNext: false }));
        }
        if (pageType === 'edit') {
          setcreateFamilyState(prev => ({ ...prev, nextStage: true }));
        }
      }
    }
    else if (!linkParam) {
      if (!familyName || !group_category || !location) {
        let message = 'Error'
        let description = `Please fill all the required fields`
        notifications('error', message, description)
        return;
      }
      if (current === 0 && pageType === 'create') {
        let famDetails = {
          group_name: familyName,
          group_category: group_category,
          base_region: location,
          user_id: user_id
        }
        setcreateFamilyState(prev => ({ ...prev, disableNext: true, familyDetails: famDetails }));
        createNewFamily(famDetails)
      }
      else {
        if (current < 4) {
          let count = current + 1;
          setcreateFamilyState(prev => ({ ...prev, current: count, disableNext: false }));
        }
      }
    }
  }
  const checkDuplicateFamily = (params_chk) => {
    checkforDuplicateFamily(params_chk).then((result) => {
      if (result && result.data && result.data.data.length > 0) {
        setcreateFamilyState(prev => ({ ...prev, joinSuggesion: true, groupList: result.data.data }))
      } else {
        createNewFamily(params_chk)
      }
    }).catch((err) => {
      console.log('errorr', err)
    })
  }
  const createNewFamily = (params_crnew) => {
    params_crnew.created_by = user_id;
    createFamily(params_crnew).then((response) => {
      let count = current + 1;
      let paramValue_n = Object.assign({}, params_crnew);
      paramValue_n.group_id = response.data.data[0].id;
      setcreateFamilyState(prev => ({ ...prev, params: paramValue_n, group_id: response.data.data[0].id, current: count, disableNext: false, joinSuggesion: false, nextStage: true }));
      localStorage.setItem('group_id', response.data.data[0].id);
    }).catch((error) => {
      console.log('error while creating', error);
    })
  }

  const joinExistingFamily = (data) => {
    var params_join = {
      user_id: user_id,
      group_id: `${data.id}`
    }
    joinToExistingFamily(params_join).then((response) => {
      if (response && response.data && response.data.data && response.data.data.status) {
        let groupArray = groupList
        groupArray.map((item, i) => {
          if (item.id == response.data.data.group_id) {
            groupArray[i].req_status = response.data.data.status
          }
        })
        setcreateFamilyState(prev => ({ ...prev, groupList: groupArray }));
      }
      // var current = 0;
      // this.setState({ current: current, disableNext: false, visible: false, familyName: '', familyType: '', location: '' });
    }).catch((err) => {
      console.log('err while joining', err);
    })
  }
  const skipJoinGroup = () => {
    createNewFamily(familyDetails)
    // let created_by = user_id
    // createNewFamily(...familyDetails,created_by )
    let count = current + 1;
    setcreateFamilyState(prev => ({ ...prev, current: count, nextStage: true, joinSuggesion: false }));
  }
  const backToStage = () => {
    let count = current - 1;
    setcreateFamilyState(prev => ({ ...prev, current: count }));
  }
  const updateCreatedFamily = () => {
    const formData = new FormData();
    formData.append('id', group_id.toString());
    formData.append('group_name', familyName);
    // formData.append('logo', logo);
    // formData.append('cover_pic', cover_pic);
    formData.append('base_region', location);
    formData.append('user_id', user_id);
    formData.append('intro', familyIntro);
    formData.append('is_active', true);
    formData.append('searchable', searchable);
    formData.append('group_category', group_category);
    formData.append('visibility', visibility);
    formData.append('member_joining', member_joining);
    formData.append('member_approval', member_approval);
    formData.append('post_create', post_create);
    formData.append('post_approval', post_approval);
    formData.append('post_visibilty', post_visibilty);
    formData.append('link_family', link_family);
    formData.append('link_approval', link_approval);
    formData.append('is_linkable', is_linkable);
    formData.append('announcement_create', announcement_create)
    formData.append('announcement_visibility', announcement_visibility)
    formData.append('request_visibility', request_visibility)
    updateFamily(formData).then((response) => {
      if (response && response.data && response.data.data && response.data.data[0]) {
        linkFamilyCreated(response)
        let groupId = encryption(response.data.data[0].id);
        history.push(`/familyview/${groupId}`)
      }
    }).catch((error) => {
      console.log(error)
    })
  }
  const linkFamilyCreated = (response) => {
    let _from = window.location.pathname.split('create-family-link/createAndLink/').pop()
    let u_id = user_id
    let _to = response.data.data[0].id
    let params_link = {
      from_group: _from,
      requested_by: u_id,
      to_group: [_to]
    }
    requestLinkFamily(params_link).then(res => {
      console.log("res", res)
    })
  }
  const openImageModal = (type) => {
    let paramValue = params;
    paramValue.editType = type
    paramValue.crop = {
      unit: "%",
      aspect: 261 / 100,
      // height : 100,
      width: 50
    }
    setcreateFamilyState(prev => ({ ...prev, params: paramValue, imageModal: true, imageType: type }));
  }
  const handleOk = () => {
    setcreateFamilyState(prev => ({ ...prev, imageModal: false }));
  }
  const handleCancel = () => {
    setcreateFamilyState(prev => ({ ...prev, imageModal: false }));
  }
  const dataCallback = (response, type) => {
    if (response && response !== '') {
      setTimeout(() => {
        setcreateFamilyState((prev) => ({ ...prev, cover_pic: response.data.data[0].cover_pic, logo: response.data.data[0].logo, imageModal: false }))
      }, (3000))
    } else {
      setcreateFamilyState((prev) => ({ ...prev, imageModal: false }))
    }

  }
  const openShareFamilyList = () => {
    setcreateFamilyState((prev) => ({ ...prev, linkModal: true }));
    getAllLinkedFamily({ user_id: `${user_id}`, group_id: `${params.group_id}` }).then((response) => {
      let linkArray = response.data.data;
      setcreateFamilyState((prev) => ({ ...prev, fetchFamilyList: linkArray }));
    }).catch((err) => {
      console.log("error happened")
    })
  }
  const handleChooseFamilyList = (e, v) => {
    var selectedArray = selectedGroups;
    var selectedIds = selectedGrpsID;
    if (e.target.checked) {
      selectedArray.push({ id: v.id });
      selectedIds.push(v.id);
    } else {
      let index = selectedIds.indexOf(v.id);
      selectedArray.splice(index, 1);
      selectedIds.splice(index, 1);
    }
    setcreateFamilyState((prev) => ({ ...prev, selectedGroups: selectedArray, selectedGrpsID: selectedIds }));
  }
  const onClose = () => {
    setcreateFamilyState((prev) => ({ ...prev, linkModal: false }));
  }
  const sednRequestToFamily = () => {
    let _params = {
      to_group: selectedGrpsID,
      from_group: params.group_id,
      requested_by: user_id
    }
    requestLinkFamily(_params).then((res) => {
      setcreateFamilyState((prev) => ({ ...prev, linkModal: false }));
    }).then((err) => {

    })
  }
  const navigateToFamily = () => {
    if (group_id && group_id != '') {
      let groupId = encryption(group_id);
      history.push(`/familyview/${groupId}`)
    }
  }
  const showAllSimilarFamilies = () => {
    setcreateFamilyState((prev) => ({ ...prev, limitIndex: groupList.length }));
  }
  const handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
    setcreateFamilyState((prev) => ({ ...prev, search: "", values: geocodedPrediction.formatted_address }))
    setcreateFamilyState((prev) => ({ ...prev, location: geocodedPrediction.formatted_address }))
  }
  const handleSearchChange = (e) => {
    e.persist()
    setcreateFamilyState((prev) => ({ ...prev, search: e.target.value, values: e.target.value, location: e.target.value }))
  }
  return (
    <section className="CreateFamilheey f-page">
      <div className="f-clearfix f-body">
        {spinLoader &&
          <Spin style={{ 'padding': '25% 0 25% 50%' }}></Spin>}
        {!spinLoader && <div className="f-boxed">
          <Button className="f-create-form-back" onClick={() => pageType === 'create' ? history.push(`my-family`) : navigateToFamily()} />
          <div className="f-clearfix f-create-form-control">
            <div className="f-create-form-head">
              <h3>{_title}</h3>
            </div>
            <div className="f-create-form-body" >
              {/* Steps */}
              {pageType != 'edit' &&
                <Steps className="f-steps" progressDot current={current}>
                  <Step />
                  <Step />
                  <Step />
                  <Step />
                </Steps>
              }

              {/* Body */}
              <div className="f-modal-wiz-body">

                {/* Step 1 */}
                {!joinSuggesion && current === 0 &&
                  <div className="f-modal-wiz-step">
                    <h4>Basic Details</h4>
                    <div className="f-form-control">
                      <label>Family Name*</label>
                      <Input id='familyName' placeholder="Eg: Nigeria womens group" value={familyName} onChange={(e) => handleChange('familyName', e)} />
                    </div>
                    <div className="f-form-control">
                      <label>Family Category*</label>
                      <Select placeholder="Eg: Company" value={group_category} onChange={(e) => handleSelect('group_category', e)}>
                        <Option value="Regular">Regular</Option>
                        <Option value="Company">Company</Option>
                        <Option value="Individual firm">Individual firm</Option>
                        <Option value="Non Profit Organisation">Non Profit Organisation</Option>
                        <Option value="Institute">Institute</Option>
                        <Option value="Religious/Spiritual">Religious/Spiritual</Option>
                        <Option value="Community">Community</Option>
                        <Option value="Association">Association</Option>
                        <Option value="Team">Team</Option>
                        <Option value="Project">Project</Option>
                        <Option value="Branch or division">Branch or division</Option>
                      </Select>
                    </div>
                    <div className="f-form-control">
                      <label>Base Region or Location*</label>
                      <ReactGoogleMapLoader
                        params={{
                          key: MY_API_KEY,
                          libraries: "places,geocode",
                        }}
                        render={googleMaps =>
                          googleMaps && (
                            <ReactGooglePlacesSuggest
                              googleMaps={googleMaps}
                              autocompletionRequest={{
                                input: search,
                              }}
                              onSelectSuggest={handleSelectSuggest}
                              textNoResults="My custom no results text" // null or "" if you want to disable the no results item
                              customRender={prediction => (
                                <div className="customWrapper">
                                  {prediction
                                    ? prediction.description
                                    : "My custom no results text"}
                                </div>
                              )}
                            >
                              <Input className="art-mat-style art-suffix art-sfx-location" placeholder="Location"
                                type="text"
                                value={values}
                                onChange={(e) => handleSearchChange(e)}
                              />
                            </ReactGooglePlacesSuggest>
                          )
                        }
                      />
                    </div>
                  </div>}

                {/* Step 1 - Create Suggestion */}
                {groupList && groupList.length > 0 && joinSuggesion &&
                  <div className="f-modal-wiz-step">
                    <div className="f-clearfix f-top-back">
                      <Button onClick={() => setcreateFamilyState(prev => ({ ...prev, joinSuggesion: false }))}><Icon type="left" /> Back to Basic Details</Button>
                    </div>
                    <div className="f-clearfix f-intro">
                      <h4>Found families with similar names!</h4>
                      <h4><em>would you like to join any of these families, </em></h4>
                      <h5>instead of creating a new family?</h5>
                    </div>
                    <h6 className="f-found-fam"><strong>{groupList.length}</strong> Families found</h6>
                    <div className="f-recommendation-list">
                      {groupList.map((item, index) => {
                        return (limitIndex > index && <div key={index} className="f-family-card">
                          <div className="f-start">
                            <div className="f-left">
                              <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/default_group.png')} />
                            </div>
                            <div className="f-right">
                              <h4>{item.group_name}</h4>
                              <h5>By <strong>{item.created_by_}</strong></h5>
                              <h6>{item.group_category}</h6>
                              <h6 className="f-pin">{item.base_region}</h6>
                            </div>
                          </div>
                          <div className="f-stop">
                            <h5><strong>{item.member_count}</strong> Members</h5>
                            {item.req_status && item.req_status == 'accepted' && <Checkbox >Joined</Checkbox>}
                            {((item.req_status && item.req_status == 'rejected') || !item.req_status) && <Checkbox checked onClick={() => joinExistingFamily(item)}>Join</Checkbox>}
                            {item.req_status && item.req_status == 'pending' && <Checkbox >pending</Checkbox>}
                          </div>
                        </div>)
                      })}

                    </div>
                    {
                      (limitIndex > 10) && <div className="f-clearfix f-more-fam">
                        <p><strong>{groupList.length - limitIndex}</strong> more similar families to show</p>
                        <Button onClick={() => showAllSimilarFamilies()}>Show all</Button>
                      </div>
                    }

                  </div>
                }

                {/* Step 1 - Create Suggestion - End */}

                {/* Step 2 */}
                {nextStage && current == 1 &&
                  <div className="f-modal-wiz-step">
                    <h4>Family Introduction</h4>
                    <div className="f-form-control">
                      <label>A little bit about your family</label>
                      <TextArea placeholder="Eg: Nigeria womens group " rows={6} value={familyIntro} onChange={(e) => handleChange('familyIntro', e)} />
                      <span className="f-tip">250 Characters max</span>
                    </div>
                  </div>
                }
                {/* Step 3 */}
                {current == 2 &&
                  <div className="f-modal-wiz-step" >
                    <h4>Upload Photo</h4>
                    <div className="f-form-control">
                      <label>Please take a moment to upload your family cover pic as well as logo/ profile pic.</label>
                      <div className="f-clearfix">
                        <header className="f-upload-poster">
                          {<div className="f-poster">
                            {/* Edit Poster */}
                            <Button className="f-edit-poster-pic" onClick={() => openImageModal('coverpic')}><Icon type="camera" /></Button>
                            {/* <img src={require('../../../assets/images/demo/demo_poster.png')} /> */}
                            <div className="f-poster-default ">
                              <img className={cover_pic ? "full-width-img" : "no-class"} src={cover_pic ? (process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + cover_pic) : require('../../../assets/images/empty_img.png')} />
                              {!cover_pic ? <h4>Upload your family cover pic !</h4> : ''}
                              {!cover_pic ? <p>Size recommendation: 1044 X 400 px</p> : ''}
                            </div>
                            {/* Shadow */}
                            <span className="f-overlay" />
                          </div>}
                          {/* {cover_pic && <div className="f-poster">
                          <Button className="f-edit-poster-pic" onClick={() => openImageModal('coverpic')}><Icon type="camera" /></Button>
                            <img src={cover_pic ?  process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + cover_pic : require('../../../assets/images/empty_img.png')} />
                          </div>} */}
                          <div className="f-page-overview">
                            {!logo && <span className="f-display-pic">
                              {/* <img src={require('../../../assets/images/demo/demo_poster.png')} /> */}
                              <div className="f-logo-default">
                                <img src={logo ? process.env.REACT_APP_S3_BASE_URL + 'logo/' + logo : require('../../../assets/images/default.png')} />
                                {!cover_pic ? <h4>Logo/ Profile</h4> : ''}
                                {!logo ? <p>120 x 120 px</p> : ''}
                              </div>
                            </span>}
                            {logo &&
                              <span className="f-display-pic">
                                <img src={logo ? process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + logo : require('../../../assets/images/default.png')} />
                              </span>
                            }
                            <Button className="f-edit-family" onClick={() => openImageModal('logo')}><Icon type="camera" /></Button>
                            {/* Error Message */}
                            {/* <p className="f-error">Error message *</p> */}
                          </div>
                        </header>
                      </div>
                    </div>
                  </div>
                }

                {/* Step 4 */}
                {!showAdvanced && current === 3 &&
                  <div className="f-modal-wiz-step" >
                    <div className="f-advanced">
                      <h4>Family Settings</h4>
                      <div className="f-form-control f-wiz-switch">
                        <ul>
                          <li>
                            <h6>Make this family private</h6>
                            <Switch checked={visibility} onChange={(e) => handleAdvSelect('visibility', e)} />
                          </li>
                          <li className="f-inactive">
                            <h6>Make this group searchable</h6>
                            <Switch defaultChecked checked={searchable} onChange={(e) => handleAdvSelect('searchable', e)} />
                          </li>
                          <li>
                            <h6>Link with other families as well</h6>
                            <Switch checked={is_linkable} onClick={(e) => handleAdvSelect('is_linkable', e)} /> {is_linkable && <Button type="link" onClick={() => openShareFamilyList()}>Select</Button>}
                          </li>
                        </ul>
                      </div>
                      <div className="f-form-control f-wiz-switch-alt">
                        <Button style={{ float: 'right' }} type="link" onClick={() => setcreateFamilyState((prev) => ({ ...prev, showAdvanced: true }))}>Advanced Settings</Button>
                      </div>
                    </div>
                  </div>
                }
                {/* Step 4 - Advanced Settings*/}
                {showAdvanced && current === 3 &&
                  <div className="f-modal-wiz-step">
                    {pageType != 'edit' &&
                      <div className="f-clearfix f-top-back">
                        <Button onClick={() => setcreateFamilyState((prev) => ({ ...prev, showAdvanced: false }))}><Icon type="left" /> Back to Basic Details</Button>
                      </div>
                    }
                    <div className="f-advanced">
                      <h4>Advanced Settings </h4>
                      <div className="f-form-control f-wiz-switch">
                        <h5>How members can join?</h5>
                        <Radio.Group defaultValue={member_joining} onChange={(e) => handleAdvChange('member_joining', e)}>
                          <Radio value={1}>Invitation Only</Radio>
                          <Radio value={2}>Anyone can join<br /><small>(with approval)</small></Radio>
                          <Radio value={3}>Anyone can join</Radio>
                        </Radio.Group>
                      </div>
                      {member_joining != 3 &&
                        <div className="f-form-control f-wiz-switch">
                          <h5>Who can approve members?</h5>
                          <Radio.Group defaultValue={member_approval} onChange={(e) => handleAdvChange('member_approval', e)}>
                            <Radio value={4}>Admin</Radio>
                            <Radio value={5}>Members</Radio>
                          </Radio.Group>
                        </div>
                      }
                      <div className="f-form-control f-wiz-switch">
                        <h5>Who can create posts, events, albums and documents?</h5>
                        <Radio.Group defaultValue={post_create} onChange={(e) => handleAdvChange('post_create', e)}>
                          <Radio value={6}>Members</Radio>
                          <Radio value={7}>Admin only</Radio>
                        </Radio.Group>
                      </div>
                      <div className="f-form-control f-wiz-switch">
                        <h5>Who can view posts?</h5>
                        <Radio.Group defaultValue={post_visibilty} onChange={(e) => handleAdvChange('post_visibilty', e)}>
                          {post_visibilty == 8 && <Radio value={8}>Members</Radio>}
                          {post_visibilty == 9 && <Radio value={9}>Public</Radio>}
                        </Radio.Group>
                      </div>
                      {/* <div className="f-form-control f-wiz-switch">
                        <h5>Who can approve posts?</h5>
                        <Radio.Group defaultValue={post_approval} onChange={(e) => handleAdvChange('post_approval', e)}>
                          <Radio value={10}>Not needed</Radio>
                          <Radio value={11}>Admin</Radio>
                          <Radio value={12}>Members<br /><small>(with approval)</small></Radio>
                        </Radio.Group>
                      </div> */}
                      <div className="f-form-control f-wiz-switch">
                        <h5>Who can create announcements ?</h5>
                        <Radio.Group defaultValue={announcement_create} onChange={(e) => handleAdvChange('announcement_create', e)}>
                          <Radio value={17}>Admin only</Radio>
                          <Radio value={18}>Any member</Radio>
                        </Radio.Group>
                      </div>
                      <div className="f-form-control f-wiz-switch">
                        <h5>Who can view announcements ?</h5>
                        <Radio.Group defaultValue={announcement_visibility} onChange={(e) => handleAdvChange('announcement_visibility', e)}>
                          {announcement_visibility == 22 && <Radio value={22}>Public</Radio>}
                          {announcement_visibility == 21 && <Radio value={21}>Members</Radio>}
                        </Radio.Group>
                      </div>
                      <div className="f-form-control f-wiz-switch">
                        <h5>Who can link families to this family?</h5>
                        <Radio.Group defaultValue={link_family} onChange={(e) => handleAdvChange('link_family', e)}>
                          <Radio value={13}>Admins</Radio>
                          <Radio value={14}>Members</Radio>
                        </Radio.Group>
                      </div>
                      <div className="f-form-control f-wiz-switch">
                        <h5>Who can approve link family requests to this family?</h5>
                        <Radio.Group defaultValue={link_approval} onChange={(e) => handleAdvChange('link_approval', e)}>
                          <Radio value={15}>Members</Radio>
                          <Radio value={16}>Admins</Radio>
                        </Radio.Group>
                      </div>
                      <div className="f-form-control f-wiz-switch">
                        <label>Who can view requests?</label>
                        <Radio.Group defaultValue={request_visibility} onChange={(e)=>handleAdvChange('request_visibility',e)}>
                          <Radio value={27}>Members</Radio>
                          <Radio value={26}>Admin only</Radio>
                        </Radio.Group>
                      </div>
                    </div>
                  </div>
                }
              </div>

              {/* Footer */}
              <div className="f-modal-wiz-footer">
                {/* Back Button */}
                {nextStage && current > 0 &&
                  <Button type="link" onClick={() => backToStage()}><Icon type="arrow-left" /> Back</Button>}

                {!joinSuggesion && current < 3 &&
                  <Button className="f-styled" onClick={() => createsFamily()} >Continue <Icon type="arrow-right" /></Button>}
                {!joinSuggesion && current == 2 &&
                  <Button className="f-lined" onClick={() => skipJoinGroup()} >Skip for now <Icon type="arrow-right" /></Button>}

                {/* Step 3 Button */}
                {groupList && groupList.length > 0 && joinSuggesion &&
                  <Button className="f-styled" onClick={() => skipJoinGroup()}>Continue with New Family <Icon type="arrow-right" /> </Button>}

                {/* Finish Button */}
                {current === 3 &&
                  <Button className="f-styled" style={{ minWidth: 150 }} onClick={() => updateCreatedFamily()}>Done</Button>}

                {/* Step 2 Suggestion Buttons */}

                {/* Show if no family selected */}
                {/* <div className="f-clearfix f-not-selected">
                    <Button className="f-styled">Continue <Icon type="arrow-right" /></Button>
                  </div> */}

                {/* Show if any family selected */}
                {/* <div className="f-clearfix f-if-selected">
                    <Button className="f-lined">Reset</Button>
                    <Button className="f-styled">Confirm</Button>
                  </div> */}

                {/* Step 2 Suggestion Buttons - END */}

              </div>

            </div>
          </div>
        </div>}
      </div>
      <Modal
        title="Add/Edit Image"
        visible={imageModal}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        className="f-modal f-modal-wizard"
      >
        <CustomImageCrop params={params} callBackimgFromParent={dataCallback}></CustomImageCrop>
      </Modal>
      <Modal
        title="Link Other Families"
        visible={linkModal}
        // onOk={handleOk}
        onCancel={() => onClose()}
        // footer={null}
        className="f-modal f-modal-wizard"
      >
        <div className="f-modal-wiz-body" >
          <div className="f-member-listing-cards f-share-scroll">
            {fetchFamilyList && fetchFamilyList.length > 0 &&
              fetchFamilyList.map((item, index) => {
                return <Card key={index} className="f-member-cardx">
                  <div className="f-start">
                    <div className="f-left">
                      <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/demo/demo-fam1.png')} />
                    </div>
                    <div className="f-right">
                      <h4>{item.group_name}</h4>
                      <h6>By <strong>{item.created_by}</strong></h6>
                      <h6>Member since {item.member_since ? item.member_since : ''}</h6>
                      <h6 className="f-pin">{item.base_region}</h6>
                    </div>
                  </div>
                  <div className="f-stop">
                    <h5>
                    </h5>
                    <div className="f-right-combo">
                      {(item.is_linked === 'pending' || item.is_linked === 'accepted') &&
                        <Checkbox>
                          {item.is_linked}
                        </Checkbox>
                      }
                      {(item.is_linked !== 'pending' && item.is_linked !== 'accepted') &&
                        <Checkbox onChange={(e) => handleChooseFamilyList(e, item)}>
                          {selectedGrpsID.indexOf(item.id) == -1 ? 'Select' : 'Selected'}
                        </Checkbox>
                      }
                    </div>
                  </div>
                </Card>
              })
            }
          </div>
        </div>
        <div className="f-modal-wizard f-modal-wfooter">
          <Button className="f-fill" onClick={() => sednRequestToFamily()}>Done</Button>
          <Button style={{ marginLeft: '15px' }} onClick={() => onClose()}>Cancel</Button>
        </div>
      </Modal>
    </section>
  )
}
export default CreateMyFamily;