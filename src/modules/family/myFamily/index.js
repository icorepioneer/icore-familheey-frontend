import React, { useState, useEffect, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import './style.scss'
import { Button, Icon, Spin, Input, Modal, notification } from "antd";
import MyFamilySidebar from '../../../components/MyFamilySidebar';
import { viewFamily, pendingRequest, deletePendingRequest } from '../familyList/api-familyList';
import { encryption } from '../../../shared/encryptDecrypt';
import * as lodash from 'lodash'
import { useSelector } from 'react-redux'
const { Search } = Input
const { confirm } = Modal;

const MyFamily = ({ history }) => {
    const [familyState, setfamilyState] = useState({
        user_id: useSelector(state => state.user_details.userId),
        familyList: [],
        loader: true,
        _loading: false,
        searchString: '',
        pendingFlag: false,
        reqArray: [],
        limit: 10,
        offset: 0
    })
    useEffect(() => {
        getMyFamily();
    }, [])
    const removeSpaces = (query) => {
        let value;
        switch (true) {
            case lodash.startsWith(query, ' '):
                value = lodash.trimStart(query, ' ')
                break;
            case lodash.endsWith(query, ' '):
                value = lodash.trimEnd(query, ' ')
                break;
            default:
                value = query
                break;
        }
        return value;
    }
    const viewFam = () =>{
        familyList = [];
        setfamilyState(prev => ({ ...prev, familyList:familyList }))
        getMyFamily();
    }
    const getMyFamily = (query) => {
        searchString = removeSpaces(searchString);
        var params = {
            user_id: user_id,
            query: searchString,
            limit: limit,
            offset: offset
        }
        setfamilyState(prev => ({ ...prev, _loading: true, searchString: searchString, pendingFlag: false, loader: true }))
        viewFamily(params).then((response) => {
            let { data } = response.data;
            let _familyList = [...familyList, ...data];
            if (response && response.data && response.data.data) {
                let _offset = offset + data.length;
                setfamilyState(prev => ({ ...prev, familyList: _familyList, loader: false, _loading: false, offset: _offset }))
            }
        }).catch((error) => {
            console.log('error--->', error);
        })
    }
    // Toggle Sidebar
    const togglemyfamilysidebar = () => {
        document.getElementById('f-wrap').classList.toggle("f-show-myfamily-sider");
    }
    // Close Sidebar
    const closemyfamilyidebar = () => {
        document.getElementById('f-wrap').classList.remove("f-show-myfamily-sider");
    }
    const createFamilyBtn = () => {
        history.push(`create-family`);
    }
    const navigateToFamily = (id) => {
        var familyid = encryption(id)
        history.push(`familyview/${familyid}`)
    }
    const viewPendingRequest = () => {
        searchString = "";
        setfamilyState(prev => ({ ...prev, pendingFlag: true, loader: true, searchString: searchString, offset: 0, reqArray:[] }))
        pendingRequestFlag();
    }
    const pendingRequestFlag = (query) => {
        searchString = removeSpaces(searchString)
        pendingRequest({ user_id: user_id, type: 'request', txt: searchString }).then(res => {
            if (res.data && res.data.data && res.data.data.length) {
                setfamilyState(prev => ({ ...prev, reqArray: res.data.data, loader: false }));
            } else {
                setfamilyState(prev => ({ ...prev, loader: false }));
            }
        }).catch(err => {
            console.log('error', err);
        })
    }
    const requestCancel = (e, id) => {
        e.stopPropagation();
        confirm({
            title: 'Are you sure to cancel the request?',
            onOk() {
                pendingRequestCancel(id);
            },
            onCancel() { },
        });

    }
    const pendingRequestCancel = (reqid) => {
        let data = { id: reqid.toString() }
        deletePendingRequest(data).then(res => {
            if (res && res.data) {
                pendingRequestFlag();
            }
        }).catch(err => {
            notification.info({
                message: `Error occured`,
                description: ''
            });
            console.log('error', err);
        })
    }

    let { user_id, familyList, loader, _loading, searchString, reqArray, pendingFlag, limit, offset } = familyState;

    useEffect(() => {
        let timer_input;
        let discover = document.getElementById("discover_fam")
        discover.addEventListener("keyup", () => {
            clearTimeout(timer_input)
            timer_input = setTimeout(() => {
               Loader1.current()
            }, 1300)
        })
    }, [])
    /** my families pagination loader */
    const [myFamState, myFamsetState] = useState(null)
    const Loader1 = useRef(getMyFamily)
    const observer1 = useRef(new IntersectionObserver((entries) => {
        let last = entries[0];
        last.isIntersecting && Loader1.current();
    }, { threshold: 1 }));
    useEffect(() => {
        const currentElement1 = myFamState;
        const currentObserver1 = observer1.current;
        if (currentElement1) {
            currentObserver1.observe(currentElement1)
        }
        return () => {
            if (currentElement1) currentObserver1.unobserve(currentElement1);
        }
    }, [myFamState]);
    useEffect(() => {
        Loader1.current = getMyFamily;
    }, [getMyFamily]);

    /** pending families pagination */
    const [pendState, pendSetState] = useState(null);
    const Loader2 = useRef(pendingRequestFlag)
    const observer2 = useRef(new IntersectionObserver((entries) => {
        let last = entries[0];
        last.isIntersecting && Loader2.current();
    }, { threshold: 1 }));
    useEffect(() => {
        const currentElement2 = pendState;
        const currentObserver2 = observer2.current;
        if (currentElement2) {
            currentObserver2.observe(currentElement2)
        }
        return () => {
            if (currentElement2) currentObserver2.unobserve(currentElement2);
        }
    }, [pendState]);
    useEffect(() => {
        Loader2.current = pendingRequestFlag;
    }, [pendingRequestFlag]);

    const handleonChange = (e) => {
        searchString = e.target.value;
        offset = 0;
        if (pendingFlag) {
            reqArray = [];
            setfamilyState((prev) => ({
                ...prev,
                reqArray: reqArray,
                offset: offset,
                limit: limit,
                searchString: searchString,
                loader: true
            }))
            pendingRequestFlag();
        } else {
            familyList = [];
            setfamilyState((prev) => ({
                ...prev,
                familyList: familyList,
                offset: offset,
                limit: limit,
                searchString: searchString,
                loader: true
            }))
        }
    }
    return (
        <section className="myFamily f-page">
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-post-listing">

                        <MyFamilySidebar history={history} />
                        {/* Close Sidebar Mobile */}
                        <span className="f-close-myfamily-sidebar" onClick={() => closemyfamilyidebar()} style={{ display: 'none' }} />

                        <div className="f-posts-main">

                            <div className="f-posts-create f-force-hide-tab">
                                <Button onClick={() => createFamilyBtn()}>Create New Family <img src={require('../../../assets/images/icon/plus.svg')} /></Button>
                            </div>
                            <div className="f-member-listing">
                                <div className="f-content-searchable f-alt-non-col f-alt" style={{ margin: '0 0 10px' }}>
                                    {!pendingFlag ? <h4><strong>{searchString.length > 0 ? `Showing results for ${searchString}` : 'All Families'} </strong><Button className="f-line-ttle" onClick={() => viewPendingRequest()}>View Pending Request</Button></h4> : <h4><strong>Pending Requests</strong> <Button className="f-line-ttle" onClick={() => viewFam()}>View Families</Button></h4>}
                                    <Search
                                        id ="discover_fam"
                                        placeholder="Enter family name"
                                        loading={_loading}
                                        onChange={(e) => handleonChange(e)}
                                        value={searchString}
                                    />
                                </div>
                            </div>
                            <div className="f-posts-filters f-force-hide-tab">
                                <Button onClick={() => togglemyfamilysidebar()} className="f-postside-unfold" style={{ display: 'none' }}><Icon type="menu-unfold" /></Button>
                            </div>

                            {/* Post Cards */}
                            <div className="f-family-list">
                                {/* Card */}
                                {!pendingFlag && familyList && familyList.length > 0 &&
                                    familyList.map((item, index) => {
                                        return <div key={index} className="f-family-card" onClick={() => navigateToFamily(item.id)}>
                                            <div className="f-start">
                                                <div className="f-left">
                                                    <img onError={(e) => { e.target.onerror = null; e.target.src = "images/family_logo.png" }}
                                                        src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/family_logo.png')} />
                                                </div>
                                                <div className="f-right">
                                                    <h4>{item.group_name}</h4>
                                                    <h5>By <strong>{item.created_by}</strong></h5>
                                                    <h6>{item.group_category}</h6>
                                                    <h6 className="f-pin">{item.base_region}</h6>
                                                </div>
                                            </div>
                                            <div className="f-stop">
                                                <h5><strong>{item.member_count}</strong> {item.member_count > 1 ? 'Members' : 'Member'}</h5>
                                                <h5><strong>{item.post_count}</strong>  Posts</h5>
                                            </div>
                                        </div>
                                    })

                                }
                                {!loader && !pendingFlag && familyList && familyList.length == 0 &&
                                    <div className="f-no-feed">
                                        <img src={require('../../../assets/images/no-family.png')} />
                                        <h4>No results to show</h4>
                                        <Button onClick={() => createFamilyBtn()}>Create New Family</Button>
                                    </div>
                                }

                                {!loader && pendingFlag && reqArray && reqArray.length > 0 &&
                                    reqArray.map((item, index) => {
                                        return <div key={index} className="f-family-card" onClick={() => navigateToFamily(item.group_id)}>
                                            <div className="f-start">
                                                <div className="f-left">
                                                    <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/family_logo.png')} />
                                                </div>
                                                <div className="f-right">
                                                    <h4>{item.group_name}</h4>
                                                    <h5>By <strong>{item.created_by}</strong></h5>
                                                    <h6>{item.group_category}</h6>
                                                    <h6 className="f-pin">{item.base_region}</h6><div className="f-right-combo" style={{ marginTop: '10px' }}>
                                                        <Button onClick={(e) => requestCancel(e, item.id)} className="f-line">Cancel</Button>
                                                        {/* <Button className="f-line" onClick={() => _invitationResp(item, 'rejected')}>Reject</Button>  */}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    })

                                }
                                {!loader && pendingFlag && reqArray && reqArray.length === 0 && <div>No Pending Requests</div>}

                            </div>

                            {loader &&
                                <div className="loaderDiv">
                                    <Spin tip="Loading..." style={{ 'marginLeft': '50%' }}>
                                    </Spin>
                                </div>
                            }

                        </div>
                    </div>
                </div>
                {!pendingFlag && <div ref={myFamsetState}></div>}
                {pendingFlag && <div ref={pendSetState}></div>}
            </div>
        </section>
    )
}

export default withRouter(MyFamily);