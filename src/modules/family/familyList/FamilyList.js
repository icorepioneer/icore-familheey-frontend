import React from 'react';
// import { withRouter } from 'react-router-dom';
import { Card, Icon, Avatar } from "antd";
import { viewFamily } from './api-familyList.js';
import {encryption} from '../../../shared/encryptDecrypt'
import {connect} from 'react-redux'


const { Meta } = Card;


class FamilyView extends React.Component {
    state;
    constructor(props) {
        super(props);
        let {userID} = props
        this.state = {
            familyList: '',
            S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,
            user_id:userID
        };
        // const socket = openSocket('http://localhost:3001');
        // socket.on('message', timestamp => {
        //     console.log(timestamp);
        //     socket.emit('test1','heloo');
        // });
        // socket.emit('subscribeToTimer', 1000);
    }
    componentDidMount() {
        this.getFamilyDetails();
    }
    navigateToDetails(value) {
        var id= value.id;
        var familyId =  encryption(id)
        this.props.history.push(`/familyview/${familyId}`);

    }
    getFamilyDetails = () => {
        var params = {
            user_id: this.state.user_id
        }
        viewFamily(params).then((response) => {
            console.log('response from listAllGroupMember', response);
            var familyList = response.data.data.reverse();
            this.setState({ familyList: familyList });
            console.log('response from listAllGroupMember', this.state.familyList);
        }).catch((error) => {
            console.log('error in listAllGroupMember', error);
        })
    }
    render() {
        return (
            <div>
                <h1 >
                    <span >Family List</span>
                </h1>
                <div >
                    {this.state.familyList && this.state.familyList.map((value, index) =>

                        <Card key={index}
                            onClick={() => this.navigateToDetails(value)}
                            style={{ width: 300 }}
                            cover={
                                value.logo ? <img
                                    alt="example"
                                    src={process.env.REACT_APP_PRO_PIC_CROP + this.state.S3_BASE_URL + 'logo/' + value.logo}
                                /> : <img
                                        alt="example"
                                        src="https://familheey.s3.amazonaws.com/logo/logo-1571218166499.jpg" />
                            }
                            actions={[
                                <Icon type="setting" key="setting" />,
                                <Icon type="edit" key="edit" />,
                                <Icon type="ellipsis" key="ellipsis" />,
                            ]}
                        >
                            <Meta
                                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                title={value.group_name}
                                description={value.base_region}
                            />
                        </Card>

                    )}
                </div>


            </div>
        )
    }
}

const mapStateToProps=(state)=>{
    let {userId} = state.user_details;
    return {
        userID:userId
    }
}
export default connect(mapStateToProps,null)(FamilyView)