import axios from '../../../services/apiCall';
import axios1 from '../../../services/fileupload';
import axiosDefault from 'axios';

const viewFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/viewFamily`, data)
}
const getFamilyListForPost = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/getFamilyListForPost`, data)
}
const upload_file_tos3 = (data) => {
  return  axios1.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/upload_file_tos3`, data)
}
const generateSignedUrl = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/generateSignedUrl`, data)
}
const uploadFiletoSignedUrl = (preSignedUrl,file,config) => {
  return axiosDefault.put(preSignedUrl,file,config);
}
const mute_conversation = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/mute_conversation`, data)
}
const deletePost = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/deletePost`, data)
}

const remove_post = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/remove_post`, data)
}

const spam_report = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/spam_report`, data)
}

const pendingRequest = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/pending_requests`, data)
}
const deletePendingRequest = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/delete_pending_requests`, data)
}

const getUserAdminGroups = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getUserAdminGroups`, data)
}
export {
  viewFamily,
  getFamilyListForPost,
  upload_file_tos3,
  mute_conversation,
  deletePost,
  remove_post,
  spam_report,
  pendingRequest,
  deletePendingRequest,
  generateSignedUrl,
  uploadFiletoSignedUrl,
  getUserAdminGroups
} 