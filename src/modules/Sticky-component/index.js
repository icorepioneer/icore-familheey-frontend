import React, { useState, useEffect } from 'react';
import { Button, Carousel, Card, Modal, Spin } from 'antd';
import { stickyList, unStickPost } from './api-sticky';
import { Icon } from 'antd';
import { encryption } from '../../shared/encryptDecrypt';
import { useSelector } from 'react-redux'
import { notifications } from '../../shared/toasterMessage';
const { confirm } = Modal;
const { Meta } = Card;

const Sticky = ({ data, history, callbackParent }) => {
    const [state, setState] = useState({
        loader: true,
        stickydata: [],
        user_id: useSelector(stateX => stateX.user_details.userId),
        familyDetails: data.familyDetails
    });

    let { stickydata, user_id, familyDetails, loader } = state;

    useEffect(() => {
        callStickydata();
    }, []);

    useEffect(() => {
        let { callbackFn } = data;
        if (callbackFn && callbackFn.sticky) {
            callStickydata();
        }
    }, [data])

    const callStickydata = () => {
        let params = {
            group_id: data.group_id.toString(),
            user_id: user_id.toString(),
            // sticky_post:familyDetails.sticky_post,
            offset: 0,
            limit: 10
        }
        stickyList(params)
            .then((doc) => {
                let { data:_data } = doc;
                stickydata = _data;
                setState((prev) => ({ ...prev, stickydata: stickydata, loader: false }));
            }).catch((err) => {
                setState((prev) => ({ ...prev, stickydata: [], loader: false }));
            })
    }

    /**
    * 
    * @param {id} val 
    * go to the particlar sticky post detaoils page
    */
    const goToPost = (val) => {
        let { post_id } = val;
        let encrypted_post_id = encryption(post_id);
        history.push(`/post_details/${encrypted_post_id}`)
    }
    const unstickaPost = (val) => {
        let { post_id } = val;
        let unstickParams = {
            group_id: data.group_id.toString(),
            post_id: post_id.toString(),
            type: "unstick"
        }
        confirm({
            title: `Are you sure to unstick this post?`,
            okText: 'unstick it',
            cancelText: 'No',
            onOk() {
                setState((prev) => ({ ...prev, stickydata: [], loader: true }));
                unStickPost(unstickParams).then((doc) => {
                    notifications('success', 'success', 'unsticked the post');
                    callStickydata();
                    callbackParent({ type: 'unsticky' })
                }).catch((err) => {
                    let { data:_Data } = err.response;
                    notifications('error', 'Error', _Data.message);
                })
            },
            onCancel() { },
        });
    }
    return (
        <div className="f-family-wall">
            <h5>Familheey Wall</h5>
            {
                loader &&
                <Spin tip="Loading..." style={{ 'paddingLeft': '45%', 'paddingTop': '35%', 'paddingBottom': '55%' }}>
                </Spin>
            }
            {!loader && <Carousel arrows={true} infinite={false} swipeToSlide={true} dots={false} adaptiveHeight={true} nextArrow={<Button><Icon type="right-circle" /></Button>} prevArrow={<Button><Icon type="left-circle" /></Button>}>

                {
                    stickydata.length > 0 && stickydata.map((value, index) => { console.log(value)
                        let pa = require('../../assets/images/default.png')

                        if( value.post_attachment.length>0){
                            let  { type, filename, video_thumb } = value.post_attachment[0];
                            let typeis = type.split("/")[0];
                            switch (typeis) {
                                case 'image':
                                    pa = `${process.env.REACT_APP_COVER_PIC_CROP}${process.env.REACT_APP_S3_BASE_URL}post/${filename}`
                                break;
                                case 'video':
                                    pa = video_thumb ? `${process.env.REACT_APP_S3_BASE_URL}${video_thumb}` : pa
                                break;
                                case 'application':
                                    pa = require('../../assets/images/pdf.png')
                                break;
                            
                                default:
                                    break;
                            }
                        }
                        

                        //let pa = value.post_attachment.length > 0 ? `${process.env.REACT_APP_COVER_PIC_CROP}${process.env.REACT_APP_S3_BASE_URL}post/${value.post_attachment[0].filename}` : require('../../assets/images/default.png')
                        return (
                            <div className="f-wall-wdel">
                                {familyDetails.user_status=="admin"&&<Icon type="delete" onClick={() => { unstickaPost(value) }} />}
                                <Card onClick={() => goToPost(value)} key={value.id} cover={<img alt={value.snap_description} src={pa} />}>
                                    <Meta title={value.snap_description} description={value.created_user_name} />
                                </Card>
                            </div>
                        );
                    })
                }
                {/* <Card cover={<video preload="none" src="https://familheey-prod.s3.amazonaws.com/post/20200803T134635VID-20200801-WA0017.mp4" preload="auto" controls></video>}>
                <Meta title="Lorem Ipsum is simply dummy text of the printing and typesetting industry." description="By Johnny English" />
                </Card> */}
            </Carousel>}
        </div>
    )
}
export default Sticky;