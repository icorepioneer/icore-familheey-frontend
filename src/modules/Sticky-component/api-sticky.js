import axios from '../../services/apiCall';
const stickyList = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/post_sticky_by_family`, data);
}

const unStickPost = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/post/stickyPost`, data);
}

export {
    stickyList,
    unStickPost
} 