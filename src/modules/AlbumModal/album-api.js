
import axios from '../../services/apiCall';
import axios1 from '../../services/fileupload';


const createAlbum = (album) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/createFolder`, album)
}
const getAllAlbumList = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listUserFolders`, data)
}
const getAllUserAlbums = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listUserFolders`, data)
}
const deleteAlbum = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/removeFolder`, data)
}
const uploadImages = (data)=>{
  return axios1.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/uploadFile`, data)
}

const viewImages = (data)=>{
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/viewContents`, data)
}

const deleteImage = (data)=>{
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/delete_file`, data)
}

const makeAlbumCoverPIc = (data)=>{
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/make_pic_cover`, data)
}

const makeUserCoverPIc = (data)=>{
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/edit_profile`, data)
}

const updateFolder = (data)=>{
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/updateFolder`, data)
}
export {
    createAlbum,
    getAllAlbumList,
    getAllUserAlbums,
    deleteAlbum,
    uploadImages,
    viewImages,
    deleteImage,
    makeAlbumCoverPIc,
    makeUserCoverPIc,
    updateFolder
}
