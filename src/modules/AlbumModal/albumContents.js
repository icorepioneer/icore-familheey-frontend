import React, { useState, useEffect } from 'react'
import { Modal } from 'antd'
import PicturesWall from './uploadFile'
const DisplayContent = ({ getAlbums,setUserCover,title,user_id,flag,changeModelFlag,folder_id }) => {
    const [contentValues, setAlbumContentState] = useState({
        visible: false,
        loading: false
    })
    const { visible } = contentValues;
    const handleOk = (files) => {
        setAlbumContentState({ ...contentValues, loading: true });
        setTimeout(() => {
            setAlbumContentState({ ...contentValues, loading: false, visible: false });
            changeModelFlag();

        }, 3000);
    };
    const handleCancel = () => {
        setAlbumContentState({ ...contentValues, visible: false });
        changeModelFlag();
    };

    useEffect(()=>{
        setAlbumContentState(prev=>({...prev,visible:!visible?flag:!visible}))
    },[flag])
    return (
        <React.Fragment>
            <Modal
                visible={visible}
                title={title}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={null}
            >
                <PicturesWall
                getAlbums={getAlbums}
                setUserCover={setUserCover} 
                visible = {visible} 
                user_id = {user_id}
                folder_id = {folder_id}
                />
            </Modal>
        </React.Fragment>
    );
}

export default DisplayContent