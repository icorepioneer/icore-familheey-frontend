import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input, Select } from 'antd';
import { createAlbum as Create, updateFolder as Update } from './album-api'
import { listAllGroupMember } from '../memberListing/api-memberlist'
import { useSelector } from 'react-redux'
const { Option } = Select;
const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {
    state = {
      // member_list_visible: false
    }
    render() {

      // const onChange = (value) => {
      //   switch (value) {
      //     case 'selected':
      //       this.setState({
      //         member_list_visible: true
      //       })
      //       break;
      //     default:
      //       this.setState({
      //         member_list_visible: false
      //       })
      //       break;
      //   }
      // }

      const { visible, onCancel, onCreate, form, loading, member_list, onChange_, permission_tab_visible, member_list_visible } = this.props;
      const { getFieldDecorator } = form;
      // const { member_list_visible } = this.state;
      return (
        <Modal
          visible={visible}
          title="Create Album"
          okText="Create"
          onCancel={onCancel}
          destroyOnClose={true}
          onOk={onCreate}
          className="f-modal f-modal-wizard f-modal-wfooter f-modal-frm-content"
          footer={[
            <Button key="back" onClick={onCancel}>
              Cancel
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={onCreate}>
              Submit
            </Button>,
          ]}
        >
          <Form layout="vertical" className="f-form-layout">
            <Form.Item label="Album name">
              {getFieldDecorator('folder_name', {
                rules: [{ required: true, message: 'Album without a name...! that\'s not possible...' }],
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Description">
              {getFieldDecorator('description')(<Input type="textarea" />)}
            </Form.Item>
            {permission_tab_visible && <div className="f-clearfix" style={{ margin: '20px 0 0' }}>
              <Form.Item label="Who can see this album">
                {getFieldDecorator('permissions', {
                  initialValue: "all"
                })(
                  <Select
                    showSearch
                    style={{ width: '100%' }}
                    placeholder="Permission type"
                    optionFilterProp="children"
                    onChange={onChange_}
                    filterOption={(input, option) =>
                      option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    <Option value="all">Anyone(Public)</Option>
                    <Option value="selected">My connections</Option>
                    <Option value="only-me">Only me(Private)</Option>
                  </Select>
                )}
              </Form.Item>
              {member_list_visible && <Form.Item label="Select members">
                {getFieldDecorator('users_id')(
                  <Select
                    mode="multiple"
                    showSearch
                    style={{ width: '100%' }}
                    placeholder="Member list"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {member_list && member_list.map((item) => {
                      return <Option key={item.id} value={item.user_id}>{item.full_name}</Option>
                    })}
                  </Select>
                )}
              </Form.Item>}
            </div>}
          </Form>
        </Modal>
      );
    }
  },
);

const CreateAlbum = ({ getAlbums, type, groupId, event_id, item_to_edit, setAlbumToEdit, eventDetails, familyDetails, userDetails }) => {
  let formReff
  const [state, setState] = useState({
    user_id: useSelector(stateX => stateX.user_details.userId),
    visible: false,
    loading: false,
    _visible_button: false,
    _item_to_edit: undefined,
    member_list: '',
    _fam_flag: false,
    permission_tab_visible: false,
    member_list_visible: false,
  })
  let { member_list_visible, loading, visible, _visible_button, user_id, _item_to_edit, member_list, _fam_flag, permission_tab_visible } = state;
  const showModal = () => {
    setState(prev => ({ ...prev, visible: true }));
  };

  const handleCancel = () => {
    setAlbumToEdit(undefined)
    setState(prev => ({ ...prev, visible: false, _item_to_edit: undefined,member_list_visible:false }));
  };

  const handleCreate = () => {
    const { form } = formReff.props;
    form.validateFields((err, values) => {
      console.log(err);
      if (err) {
        return;
      }
      setState(prev => ({ ...prev, loading: true }))
      let data = { ...values, created_by: user_id, folder_type: "albums", folder_for: type, group_id: groupId ? groupId : null, event_id: event_id ? event_id : null }
      if (_item_to_edit) {
        var data_to_update = { ...values, id: `${item_to_edit.id}`, created_by: user_id, folder_type: "albums", folder_for: type, group_id: groupId ? groupId : null, event_id: event_id ? event_id : null }
      }
      _item_to_edit ? (
        Update(data_to_update).then(res => {
          form.resetFields();
          setState(prev => ({ ...prev, visible: false, loading: false }));
          getAlbums(user_id)
          handleCancel()
        })
      ) : (
          Create(data).then(res => {
            form.resetFields();
            setState(prev => ({ ...prev, visible: false, loading: false }));
            getAlbums(user_id)
          })
        )
    });
  };
  useEffect(() => {
    setState(prev => ({ ...prev, _item_to_edit: item_to_edit }))
  }, [item_to_edit])

  useEffect(() => {
    _item_to_edit && setState(prev => ({ ...prev, visible: true,member_list_visible:true }));
    _item_to_edit&& setPermissionEditVisible(_item_to_edit)
    const { form } = formReff.props;
    _item_to_edit ? (form.setFields({
      folder_name: { value: _item_to_edit.folder_name },
      description: { value: _item_to_edit.description },
      permissions: { value: _item_to_edit.permissions },
      // users_id: { value: _item_to_edit.permission_users }
    })
    ) : (form.resetFields())
  }, [_item_to_edit])

  const setPermissionEditVisible=(item)=>{
    let {created_by} = item;
    let flag = created_by == user_id
    setState(prev=>({...prev,permission_tab_visible:flag}))
  }
//   useEffect(()=>{
//     if(member_list_visible){
//         const { form } = formReff.props;
//         _item_to_edit ? (form.setFields({
//             folder_name: { value: _item_to_edit.folder_name },
//             description: { value: _item_to_edit.description },
//             permissions: { value: _item_to_edit.permissions },
//             users_id: { value: _item_to_edit.permission_users }
//         })) : (form.resetFields())
//     }
// },[member_list_visible])
  useEffect(() => {
    let hidden;
    // let fam_flag;
    switch (true) {
      case eventDetails != undefined:
        hidden = eventDetails && eventDetails.created_by == user_id
        setState(prev => ({ ...prev, _visible_button: hidden, _fam_flag: false, permission_tab_visible: false }))
        break;
      case familyDetails != undefined:
        let {user_status,post_create} = familyDetails;
        hidden = user_status === "admin"?true:(post_create == 6?true:false)
        setState(prev => ({ ...prev, _visible_button: hidden, _fam_flag: true, permission_tab_visible: true }))
        break;
      case userDetails != undefined:
        hidden = userDetails.id == user_id;
        setState(prev => ({ ...prev, _visible_button: hidden, _fam_flag: false, permission_tab_visible: true }))
        break;
    }
  }, [eventDetails, familyDetails])

  useEffect(() => {
    return () => {
      setState(prev => ({ ...prev, visible: false, _item_to_edit: undefined }))
      setAlbumToEdit(undefined)
    }
  }, [])
  const saveFormRef = formRef => {
    formReff = formRef;
  };

  const getAllMembers = () => {
    var _params = {
      crnt_user_id: `${user_id}`,
      group_id: `${groupId}`,
    }
    listAllGroupMember(_params).then(res => {
      setState(prev => ({ ...prev, member_list: res.data.data }))
    })
  }
  useEffect(() => {
    getAllMembers()
  }, [])
  const onChange_ = (value) => {
    switch (value) {
      case 'selected':
        setState(prev => ({ ...prev, member_list_visible: true }))
        break;
      //-- this case is dummy used to fix sonarqube  --// 
      //-- if new cases come delete this and add new --//
      case 'condition1':
        console.log("condition1");
        break;
      case 'condition2':
        console.log("condition2");
        break;
      //--- dummy end ---//
      default:
        setState(prev => ({ ...prev, member_list_visible: false }))
        break;
    }
  }
  return (
    <div>
      {_visible_button && <Button className="f-btn" onClick={showModal}>Create Album</Button>}
      <CollectionCreateForm
        wrappedComponentRef={saveFormRef}
        visible={visible}
        onCancel={handleCancel}
        onCreate={handleCreate}
        loading={loading}
        member_list={member_list}
        _fam_flag={_fam_flag}
        permission_tab_visible={permission_tab_visible}
        member_list_visible={member_list_visible}
        onChange_={onChange_}
      />
    </div>
  );
}

export default CreateAlbum;        