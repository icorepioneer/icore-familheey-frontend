import React,{useState,useEffect} from 'react';
import { Upload, Icon, Modal,Button } from 'antd';
import {viewImages,deleteImage,makeAlbumCoverPIc} from './album-api'
const  PicturesWall =({setUserCover,visible,folder_id,user_id,getAlbums})=> {

const [imageStates,setImageState] = useState({
    previewVisible: false,
    previewImage: '',
    fileList: [],
    filesToUpload:[],
    uploadFlag:false,
    loading_album:false,
    loading_user:false,
    newFile:false

})
const {previewImage,previewVisible,fileList,loading_album,newFile}= imageStates
const getBase64=(file)=>{
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
const handleCancel = () => setImageState((prev)=>({...prev,previewVisible: false}));

const handlePreview = async(file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setImageState(prev=>({...prev,
        previewImage: file.url || file.preview,
        previewVisible: true,
      }));
  };

const handleChange = ({ file_List }) =>{
    setImageState(prev=>({...prev,
        fileList:file_List }))
};
const removeFile =async(id)=>{
  let params = {
    id:[id]
  }
  await deleteImage(params).then(res=>{
  }).catch(err=>{
    console.log("error happened",err)
  })
}

const getFiles =async()=>{
  let userFiles = [];
  let _user_id = localStorage.getItem('user_id')
  let params = {
    folder_id:`${folder_id}`,
    user_id: _user_id
  }
  await viewImages(params).then(res=>{
    res.data&&res.data.documents.map(item=>{
      userFiles.push({
        uid:item.id,
        name:item.updatedAt,
        status:'done',
        //url:item.url,
        url:`${process.env.REACT_APP_S3_BASE_URL}/Documents/${item.file_name}`
      })
    })
    setImageState(prev=>({...prev,fileList:userFiles,newFile:false}))
  })
}

useEffect(()=>{
    setImageState(prev=>({...prev,fileList:[]}))
    getFiles();
},[visible])

useEffect(()=>{
  newFile&&setTimeout(()=>{
    getFiles();
  },8000)
},[newFile])
const url = `${process.env.REACT_APP_API_URL}/api/v1/familheey/uploadFile?user_id=${user_id}&&folder_id=${folder_id}&&is_sharable=true`
const uploadButton = (
      <div>
        <Icon type="upload" />
        <div >Add</div>
      </div>
    );
const ButtonContainer =(imageUrl)=> {
  let icon_name = loading_album&&"loading"
  return(
  <div>
    <Button 
      icon = {`${icon_name}`}
      onClick={async ()=>{
      setImageState((prev)=>({...prev,loading_album:true}))
      let params = {
        album_id:`${folder_id}`,
        cover_pic:imageUrl
      }
      await makeAlbumCoverPIc(params).then(async res=>{
        setImageState((prev)=>({...prev,loading_album:false}))
        await getAlbums(user_id)
      })}
      }>{"Set as album cover pic"}</Button>
  </div>
)}
    return (
      <div>
        <Upload
          action={url}
          listType="picture-card"
          fileList={fileList}
          onPreview={handlePreview}
          onChange={handleChange}
          multiple = {true}
          showUploadList = {true}
          onRemove = {(file)=>{
            removeFile(file.uid)
          }}
          onSuccess={async(res)=>{
            setImageState((prev)=>({...prev,newFile:true}))
          }}
        >
          {uploadButton}
        </Upload>
        <Modal visible={previewVisible} style = {{padding:'0px'}} footer={ButtonContainer(previewImage)} onCancel={handleCancel}>
          <img alt="example" src={previewImage} />
        </Modal>
      </div>
    );
}

export default PicturesWall;          