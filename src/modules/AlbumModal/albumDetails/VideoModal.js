import React, { useState, useEffect } from 'react'
import { Modal } from 'antd';
import ReactPlayer from 'react-player';
// import { callbackify } from 'util';

const ModalVideoPop = ({ visible, url, callback }) => {
    let [state, setState] = useState({
        modalVisible: false,
    })
    let { modalVisible } = state;
    useEffect(() => {
        setState(prev => ({ ...prev, modalVisible: visible }))
    }, [visible])
    const onCancel = () => {
        callback()
        setState(prev => ({ ...prev, modalVisible: false }))
    }
    return (
        <React.Fragment>
            <Modal
                destroyOnClose
                onCancel={onCancel}
                visible={modalVisible}
                footer={null}
                title="Video"
                className="f-modal f-modal-wizard"
            >
                <div className="f-modal-wiz-body f-modal-video">
                <ReactPlayer
                    className="f-csl-react-play"
                    controls
                    url={url}
                />
                </div>
            </Modal>
        </React.Fragment>
    )
}
export default ModalVideoPop;