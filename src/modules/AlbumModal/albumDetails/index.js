import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import './style.scss'
import { Modal, Button, Icon, message, Dropdown, Menu, Spin } from "antd";
import { decryption } from '../../../shared/encryptDecrypt';
import { viewImages, uploadImages, deleteImage, makeAlbumCoverPIc } from '../../AlbumModal/album-api';
import CustomImageCrop from '../../ImageCrop/index';
import { encryption } from '../../../shared/encryptDecrypt'
import Lightbox from 'react-image-lightbox';
import ModalVideoPop from './VideoModal';
import { useSelector } from 'react-redux';

let moment = require('moment')
let formData = new FormData();
const { confirm } = Modal;
const AlbumDetails = ({ location, history, folderId }) => {
    const [state, setState] = useState({
        Onloader: true,
        al_visible: false,
        user_id: useSelector(stateX => stateX.user_details.userId),
        folder_id: undefined,
        fileList: [],
        folderDetails: {},
        documents: [],
        post_attachment: [],
        lightBoxImg: '',
        openLightBox: false,
        params: {
            crop: {
                unit: "%",
                aspect: 261 / 100,
                // height : 100,
                width: 50
            },
            group_id: '',
            editType: 'folder_cover'
        },
        imageModal: false,
        cover_pic: '',
        _loading: false,
        _files: '',
        openCarousel: false,
        images: [],
        is_admin: false,
        _loading_delete: false,
        _loading_cover: false,
        videoModal_visible: false,
        video_current_url: ''
    })
    let { Onloader,
        al_visible,
        user_id,
        folder_id,
        // fileList,
        folderDetails,
        documents,
        post_attachment,
        lightBoxImg,
        openLightBox,
        params,
        imageModal,
        // cover_pic,
        _loading,
        _files,
        openCarousel,
        images,
        is_admin,
        _loading_delete,
        _loading_cover,
        videoModal_visible,
        video_current_url } = state

    useEffect(() => {
        let cipher = window.location.pathname.split('album-details/').pop();
        let fId = decryption(cipher);
        setState(prev => ({ ...prev, folder_id: fId }))

        let isAdmin = location.pathname.split('/')[3];
        isAdmin && setState(prev => ({ ...prev, is_admin: true }))
    }, [])

    useEffect(() => {
        folder_id && getFiles(folder_id)
    }, [folder_id])
    // Modal - Album
    const al_showModal = () => {
        setState(prev => ({
            ...prev,
            al_visible: true,
        }))
    };
    const al_handleCancel = () => {
        setState(prev => ({
            ...prev,
            al_visible: false,
            post_attachment: []
        }))
    };
    const getFiles = async (fid) => {
        let userFiles = [];
        let _params = {
            folder_id: `${fid}`,
            user_id: user_id
        }
        await viewImages(_params).then(res => {
            let { folder_details } = res.data;
            let _is_admin = (folder_details.created_by == user_id);
            res.data && res.data.documents.map(item => {
                userFiles.push({
                    uid: item.id,
                    name: item.updatedAt,
                    status: 'done',
                    //url: item.url
                    url: `${process.env.REACT_APP_S3_BASE_URL}/Documents/${item.file_name}`
                })
            })
            setState(prev => ({
                ...prev,
                fileList: userFiles,
                folderDetails: res.data.folder_details,
                documents: res.data.documents,
                cover_pic: res.data.cover_pic,
                Onloader: false,
                is_admin: _is_admin
            }))
        }).catch((err) => {
            console.log(err)
        })
    }
    const uploadPhoto = e => {
        formData.delete('folder_id');
        formData.delete('user_id');
        formData.delete('is_sharable');
        e.persist()
        setState(prev => ({
            ...prev,
            _files: { ..._files, ...e.target.files }
        }))
        let _post_attachment = post_attachment || []
        for (let i = 0; i < e.target.files.length; i++) {
            formData.append('Documents', e.target.files[i]);
            let _fls = e.target.files[i]
            getBase64(e.target.files[i], imageUrl => {
                _post_attachment.push({ base_64: imageUrl, _file: _fls });
                setState(prev => ({ ...prev, post_attachment: _post_attachment }))
            });
        }
    }
    const uploadImg = () => {
        setState(prev => ({
            ...prev,
            _loading: !_loading
        }))
        formData.append('folder_id', folder_id);
        formData.append('user_id', user_id);
        formData.append('is_sharable', true);
        uploadImages(formData)
            .then(response => {
                formData.delete('Documents');
                getFiles(folder_id);
                message.success('Media uploaded successfully');
                al_handleCancel();
                setState(prev => ({
                    ...prev,
                    post_attachment: [],
                    _loading: !_loading
                }))
            });
    }
    const getBase64 = (img, callback) => {
        const _formData = new FormData();
        _formData.append('file', img);
        _formData.append('name', 'post');
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
        setState(prev => ({ ...prev, image: img }));
    }
    const dataCallback = (response, type) => {
        if (response && response !== '') {
            console.log(response, type)
        }
    }
    // const openCoverPicModal = () => {
    //     setState(prev => ({ ...prev, imageModal: true }))
    // }
    const handleCancel = () => {
        setState(prev => ({ ...prev, imageModal: false }))
    }
    const makeCoverPic = async (item) => {
        setState(prev => ({
            ...prev,
            _loading_cover: true
        }))
        let _Params = {
            album_id: `${item.folder_id}`,
            cover_pic: item.url
        }
        await makeAlbumCoverPIc(_Params).then(async res => {
            setState(prev => ({
                ...prev,
                _loading_cover: false
            }))
            await getFiles(item.folder_id)
        })
    }
    const navigateToEventDetails = () => {
        var id = folderDetails.event_id;
        let gid = folderDetails.group_id;
        let _user_id = folderDetails.created_by;
        switch (true) {
            case id != null:
                var eventId = encryption(id)
                history.push(`/event-details/${eventId}/5`);
                break;
            case gid != null:
                var _eventId = encryption(gid)
                history.push(`/familyview/${_eventId}/5`);
                break;
            case _user_id != null:
                var userID = encryption(_user_id)
                history.push(`/user-details/${userID}/4`, { prev: window.location.pathname });
                break;
        }
    }
    const setLoader = (fOne, fTwo) => {
        setState(prev => ({ ...prev, Onloader: fOne, _loading_delete: fTwo }))
    }
    const removeFile = function (item) {
        // let _this = this;
        confirm({
            title: 'Are you sure want to delete?',
            okText: 'Yes',
            cancelText: 'No',
            onOk() {
                setLoader(true, true)
                let params_rf = { id: [item.id] }
                deleteImage(params_rf).then(async res => {
                    setLoader(false, false)
                    getFiles(item.folder_id)
                }).catch(err => {
                    console.log("error happened", err)
                })
            },
            onCancel() {
            },
        });

    }
    const openLightBoxFN = (url, type) => {
        switch (type) {
            case 'video/mp4':
                setState(prev => ({ ...prev, videoModal_visible: true, video_current_url: url }))
                break;
            case 'video/avi':
                setState(prev => ({ ...prev, videoModal_visible: true, video_current_url: url }))
                break;
            case 'video':
                setState(prev => ({ ...prev, videoModal_visible: true, video_current_url: url }))
                break;
            case 'image/png':
                setState(prev => ({ ...prev, lightBoxImg: url, openLightBox: true }))
                break;
            case 'image/jpeg':
                setState(prev => ({ ...prev, lightBoxImg: url, openLightBox: true }))
                break;
        }
    }
    const removeImage = (img) => {
        let _post_attachment = post_attachment
        _post_attachment = _post_attachment.filter(item => item.base_64 != img.base_64)
        setState(prev => ({ ...prev, post_attachment: _post_attachment }))
        formData.delete('Documents');
        _post_attachment.map(item => {
            formData.append('Documents', item._file)
        })
    }
    // const showAlbumCoverPic = () => {
    //     let albums = images;
    //     albums.push(process.env.REACT_APP_COVER_PIC_CROP + cover_pic);
    //     setState(prev => ({ ...prev, images: albums, openCarousel: true }))
    // }
    const videoModalCallback = () => {
        setState(prev => ({ ...prev, videoModal_visible: false }))
    }
    const downloadButtonClicked=(link)=>{
        // console.log(this.state.currentImageURlSelected)
        window.open(`${link}`,'Download')
    }
    return (
        <section className="albumDetails f-page">
            <ModalVideoPop
                visible={videoModal_visible}
                url={video_current_url}
                callback={videoModalCallback}
            />
            {Onloader && (<div className="spin-loader">
                <Spin> </Spin>
            </div>)
            }
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-event-details">
                        <header className="f-head">
                            {/* <div className="f-poster" onClick={() => showAlbumCoverPic()} >

                                {cover_pic != '' ? <img src={cover_pic && (process.env.REACT_APP_COVER_PIC_CROP + cover_pic)} /> :
                                    <div className="f-poster-default">
                                        <img src={require('../../../assets/images/default.png')} />
                                        <h4>Upload a photo to be displayed here !</h4>
                                        <p>Size recommendation: 1044 X 400 px</p>
                                    </div>
                                }
                                <span className="f-overlay" />
                            </div> */}
                            <div className="f-page-overview">
                                <h3 className="f-head-title"><strong>{folderDetails && folderDetails.folder_name ? folderDetails.folder_name : ""}</strong></h3>
                                <div className="f-head">
                                    <div className="f-left">
                                        {/* <h4> by Ben Stokes</h4> */}
                                        <h5><em>Created on {folderDetails && folderDetails.createdAt ? moment(folderDetails.createdAt).format('MMM DD, YYYY') : ''}</em></h5>
                                        <Button type="link" className="f-btn-back" onClick={() => navigateToEventDetails()}><Icon type="left" /> Back to albums</Button>
                                    </div>
                                    <div className="f-right">
                                        <p>{folderDetails && folderDetails.description ? folderDetails.description : ''}</p>
                                        <div className="f-split">
                                            <p><strong>{documents && documents.length > 0 ? documents.length : 0}</strong> Photos</p>
                                            {is_admin && <Button onClick={al_showModal} className="f-button">+ Add Media</Button>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <section className="f-sect">
                            <div className="f-events-main">
                                <div className="f-clearfix">

                                    {/* Empty List */}
                                    {documents && documents.length === 0 &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content">
                                                <div className="f-content">
                                                    <div className="f-empty-content">
                                                        <img src={require('../../../assets/images/empty_img.png')} style={{ borderRadius: '0' }} />
                                                        <h4>Photos and videos will be show up here after you add them</h4>
                                                        <div className="f-clearfix">
                                                            {is_admin && <Button onClick={al_showModal}>Add Media</Button>}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>}
                                    {/* Empty List - END */}

                                    {/* Filled List */}
                                    <div className="f-event-contents f-blanco">
                                        <div className="f-switch-content">
                                            <div className="f-content">
                                                <ul className="f-album-photos">
                                                    {/* <li><img src={require('../../../assets/images/default-album.png')} /></li> */}
                                                    {documents && documents.length > 0 &&
                                                        documents.map((item, index) => {
                                                            return <li key={index}>
                                                                {is_admin && <Dropdown
                                                                    overlay={<Menu className="f-album-context">
                                                                        <Menu.Item key="0">
                                                                            <Button
                                                                                loading={_loading_delete}
                                                                                onClick={() => {
                                                                                    let current = item;
                                                                                    removeFile(current)
                                                                                }}

                                                                            >Delete media</Button>
                                                                        </Menu.Item>
                                                                        {item.file_type === 'image' && <Menu.Item key="1">
                                                                            <Button
                                                                                loading={_loading_cover}
                                                                                onClick={() => {
                                                                                    let current = item;
                                                                                    makeCoverPic(current)
                                                                                }}
                                                                            >Make coverpic</Button>
                                                                        </Menu.Item>}
                                                                    </Menu>}
                                                                    trigger={['click']}
                                                                    placement="bottomLeft"
                                                                >
                                                                    <Button className="f-more" />
                                                                </Dropdown>}
                                                                {openLightBox &&
                                                                    <Lightbox
                                                                    toolbarButtons={[
                                                                        <Icon 
                                                                        onClick={()=>downloadButtonClicked(lightBoxImg)}
                                                                         type="download" ></Icon>
                                                                    ]}
                                                                        onCloseRequest={() => setState(prev => ({ ...prev, openLightBox: false }))}
                                                                        mainSrc={lightBoxImg}
                                                                    />
                                                                }
                                                                <img
                                                                    onClick={() => openLightBoxFN(item.url, item.file_type)}
                                                                    src={
                                                                        item.file_type.indexOf('video') !== -1 ? (item.video_thumb !== '' ? process.env.REACT_APP_S3_BASE_URL + item.video_thumb : require('../../../assets/images/default-album.png')) :
                                                                            (item.url ? item.url : require('../../../assets/images/default-album.png'))}
                                                                />
                                                            </li>
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    {/* Filled List - END */}
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            {/* Modal - Album */}
            <Modal title="Add Media"
                visible={al_visible}
                onCancel={() => al_handleCancel()}
                onOk={() => al_handleCancel()}
                className="f-modal f-modal-wizard"
                footer={[]}
            >
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-form-control">
                        <input id="input-file" onChange={(event) => uploadPhoto(event)} multiple accept=".jpg,.jpeg,.png,.mp4,.mkv,.avi" type="file" className="hide" />
                        <ul className="f-upload-setup">
                            <li className="f-first"><label htmlFor="input-file"><img src={require('../../../assets/images/icon/attachment.png')} /></label></li>
                            {post_attachment && post_attachment.map((img, i) => {
                                // console.log(img)
                                return (<li key={i}><img src={img._file.type == 'video/mp4' ? require('../../../assets/images/default-album.png') : img.base_64} /><Button onClick={() => removeImage(img)} /></li>)
                            })}
                        </ul>
                    </div>
                </div>
                <div className="f-modal-wiz-footer">
                    {(post_attachment || []).length > 0 && <Button loading={_loading} className="f-fill" onClick={(e) => uploadImg()}>Upload</Button>}
                </div>
            </Modal>
            <Modal
                title="Add/Edit Image"
                visible={imageModal}
                // onOk={handleOk}
                onCancel={handleCancel}
                footer={null}
                className="f-modal f-modal-wizard"
            >
                <CustomImageCrop
                    params={params}
                    folderId={folder_id}
                    callBackimgFromParent={dataCallback}
                ></CustomImageCrop>
            </Modal>
            {openCarousel && (
                <Lightbox
                    onCloseRequest={() => setState(prev => ({ ...prev, openCarousel: false }))}
                    mainSrc={images[0]}
                />
            )}
            {/* } */}
        </section>
    )
}

export default withRouter(AlbumDetails)