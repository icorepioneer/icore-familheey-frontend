import axios from '../../services/apiCall';

const getDetails = (data) => {
    return axios.post(`/api/v1/familheey/needs/post_need_detail`, data)
}

const createContribute = (data) => {
    return axios.post(`/api/v1/familheey/needs/contribution_create`, data)
}

const updateContribute = (data) => {
    return axios.post(`/api/v1/familheey/needs/contribution_update`, data)
}

const contributorList = (data) => {
    return axios.post(`/api/v1/familheey/needs/contribution_list`, data)
}

const requestListFull = (data) => {
    return axios.post(`/api/v1/familheey/needs/post_need_list`, data)
}

const deleteRequest = (data) => {
    return axios.post(`/api/v1/familheey/needs/delete`, data)
}

const thankYouImages = (data) => {
    return axios.post(`/api/v1/familheey/get_post_default_image`,data)
}

const publishPost = (data) => {
    return axios.post(`/api/v1/familheey/publish_post`,data)
}

const getPostByUsers = (data) => {
    return axios.post(`/api/v1/familheey/posts/get_my_post`, data)
}

const getContDetails = (data) => {
    return axios.post(`/api/v1/familheey/contributionlistByUser`, data)
}

const SkipThankU = (data) => {
    return axios.post(`/api/v1/familheey/contributionStatusUpdation`, data)
}

const searchUsersNeed = (data) =>{
    return axios.post(`/api/v1/familheey/needs/admin_user_selection`,data) 
}

const contrByAdmin = (data) =>{
    return axios.post(`/api/v1/familheey/needs/add_member_contribution_ByAdmin`,data) 
}

const getContrListByAdmin = (data) =>{
    return axios.post(`/api/v1/familheey/needs/get_contribution_list_Byadmin`,data) 
}

const deleteSelectedEntry = (data) =>{
    return axios.post(`/api/v1/familheey/needs/contribution_delete`,data) 
}

const splitAmount = (data) =>{
    return axios.post(`/api/v1/familheey/needs/get_contribute_itemAmount_split`,data) 
}
export {
    getDetails,
    createContribute,
    updateContribute,
    contributorList,
    requestListFull,
    deleteRequest,
    thankYouImages,
    publishPost,
    getPostByUsers,
    getContDetails,
    SkipThankU,
    searchUsersNeed,
    contrByAdmin,
    getContrListByAdmin,
    deleteSelectedEntry,
    splitAmount
} 