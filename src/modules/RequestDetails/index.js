import React, { useState, useEffect } from 'react';
import './style.scss'
import { Input, Button, Modal, Divider, Spin } from "antd";
import { getDetails, createContribute, updateContribute } from "../RequestDetails/req_service";
import moment from 'moment';
import { notifications } from '../../shared/toasterMessage';
import CommonSideBar from '../../components/commonSideBar';
import { useSelector } from 'react-redux';
import StripeComp from '../../components/Stripe/stripe'
import ThankYOU from '../../components/ThankYouModal/index'
import {encryption} from '../../shared/encryptDecrypt'

// const { Search } = Input;

const RequestDetails = ({ history, location }) => {

    const [state, setState] = useState({
        visible: false,
        thankyou: false,
        reqDetails: undefined,
        user_id: useSelector(stateX => stateX.user_details.userId),
        loader: true,
        modalData: {},
        post_request_id: history.location.state ? history.location.state.post_request_id.toString() : null,
        strip_comp_visible: false,
        current_selected_item_id: undefined,
        support_offline: false,
        support_offline_itemId: undefined,
        group_id_passed: undefined,
        admin_status: false,
        is_admin: false,
        from_fam_id: undefined,
        from_family:undefined
    });

    let { is_admin,reqDetails,from_family,from_fam_id, loader, group_id_passed, visible, modalData, user_id, thankyou, post_request_id, strip_comp_visible, current_selected_item_id, admin_status } = state;

    useEffect(() => {
        getReqDetails();
    }, [window.location.pathname]);

    useEffect(() => {
        getReqDetails();
    }, []);

    useEffect(() => {
        if (location) {
            console.log(location)
            let f_id
            let { state:xstate } = location
            let { group_id_passed:gip, admin_status:ad_s, from_family:from_fm } = xstate
            if (from_fm) {
                let { id } = from_fm
                f_id = id
            }
            setState(prev => ({ ...prev, group_id_passed: gip, admin_status: ad_s,from_fam_id:f_id,from_family:from_fm }))
        }
    }, [location])

    /**
     * get the Need request details
     */
    const getReqDetails = () => {
        setState((prev) => ({
            ...prev,
            loader: true
        }))
        let input = {
            user_id: user_id,
            post_request_id: post_request_id
        }
        getDetails(input).then((response) => {
            let { data, is_admin } = response.data;
            setState((prev) => ({ ...prev, reqDetails: data, loader: false, is_admin: is_admin }))
        }).catch((err) => {
            notifications('error', 'Error', 'Something went wrong')
        })
    }

    /**
     * redirect to contributrs list 
     * with item id 
     */
    const redirectToContr = (itemId) => {
        history.push({
            pathname: '/contributors',
            state: {
                item_id: itemId,
                post_request_id: post_request_id,
                reqDetails: reqDetails,
                admin_status: is_admin,
                group_id_passed: group_id_passed,
                from_family:from_family
            }
        });
    };
    /**
     * 
     * open contribute modal
     */
    const showContrModal = (_item) => {
        setState((prev) => ({
            ...prev,
            visible: true,
            modalData: _item
        }))
    }

    const handleOk = () => {
        setState(prev => ({ ...prev, visible: false }))
    };

    const handleCancel = () => {
        setState((prev) => ({
            ...prev,
            visible: false
        }))
    };

    const handleChange = (e) => {
        let num = e.target.value;
        if (isNaN(num)) {
            modalData.contribute_item_quantity = 0
        } else {
            modalData.contribute_item_quantity = parseInt(e.target.value);
        }
        setState((prev) => ({
            ...prev,
            modalData: modalData
        }))
    }

    /**
     * contribute against an item
     */
    const contributeFn = () => {
        let { contribute_id } = modalData;

        let _data = {
            contribute_user_id: `${user_id}`,
            contribute_item_quantity: modalData.contribute_item_quantity ? modalData.contribute_item_quantity.toString() : '0'
        }

        if (contribute_id) {// already contribute update
            _data = { ..._data, id: modalData.contribute_id.toString() }
            updateContribute(_data).then((response) => {
                setState((prev) => ({
                    ...prev,
                    visible: false,
                    thankyou: true
                }))
                getReqDetails()
            }).catch((err) => {
                notifications('error', 'Error', 'Something went wrong')
            })
        } else {// new request to contribute
            _data = { ..._data, post_request_item_id: modalData.item_id.toString() }
            createContribute(_data).then((response) => {
                setState((prev) => ({
                    ...prev,
                    visible: false,
                    thankyou: true
                }))
                getReqDetails()
            }).catch((err) => {
                notifications('error', 'Error', 'Something went wrong')
            })
        }
    };

    const ty_handleCancel = () => {
        setState((prev) => ({
            ...prev,
            thankyou: false
        }))
    };
    const ty_handleOk = () => {
        setState((prev) => ({
            ...prev,
            thankyou: false
        }))
    };

    const callNow = () => {
        let { phone } = reqDetails;
        setState((prev) => ({
            ...prev,
            thankyou: false
        }));
        window.location.href = `tel://${phone}`;
    }
    const toggleStripeModal = (item) => {
        let { item_id } = item
        setState(prev => ({ ...prev, strip_comp_visible: !strip_comp_visible, current_selected_item_id: item_id }))
    }
    const toggleStripeModal_ = (flag = true) => {
        setState(prev => ({ ...prev, strip_comp_visible: !strip_comp_visible }))
        flag && setTimeout(() => {
            getReqDetails();
        }, 4000)
    }
    const showThanYou = () => {
        setState(prev => ({ ...prev, thankyou: true }))
    }
    const displayOfflineSupportPage = (item) => {
        let { item_id } = item
        history.push('/offlinePayment', { group_id: group_id_passed, item_id: item_id, request_id: post_request_id })
    }
    const navigateToBackPage=()=>{
        if(from_fam_id){
            // console.log(":a")
            let id = encryption(from_fam_id)
            history.push(`/familyview/${id}/16`);
        }
        else {
            // console.log(":b")
            history.push('/requests')
        }
    }
    console.log(admin_status)
    return (
        <section className="RequestDetails f-page">
            <StripeComp
                modalVisible={strip_comp_visible}
                toggleStripeModal={toggleStripeModal_}
                post_request_id={post_request_id}
                reqDetails={reqDetails}
                current_selected_item_id={current_selected_item_id}
                handleChange={handleChange}
                showThanYou={showThanYou}
                callback={getReqDetails}
            />
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-post-listing">
                        <CommonSideBar history={history} />
                        {loader && <Spin style={{ 'paddingLeft': '35%', 'paddingTop': '20%' }}></Spin>}
                        {!loader && <div className="f-clearfix f-request-details-wrap">
                            <div className="f-req-details-header">
                                <div className="f-left">
                                    <Button className="f-create-form-back f-gobck" onClick={() => navigateToBackPage()} />
                                    <h4 className="mr-top">Request Details</h4>
                                </div>
                            </div>
                            {
                                <React.Fragment>
                                    <div className="f-req-author-card">
                                        <div className="f-left">
                                            {reqDetails.propic && <img
                                                onError={(e) => { e.target.onerror = null; e.target.src = '../../assets/images/profile-pic.png' }}
                                                src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + reqDetails.propic}
                                            />}
                                        </div>
                                        <div className="f-right">
                                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                <h2>{ reqDetails.full_name}</h2>
                                                {/* <div className='f-right'>
                                            <Button
                                            onClick={()=>toggleStripeModal()}
                                            className="f-line f-btn-green"
                                        >Support by payment</Button></div> */}
                                            </div>
                                            <h3>Posted in &nbsp;
                                {reqDetails.to_groups && reqDetails.to_groups.length > 0 && reqDetails.to_groups[0].group_name}
                                                {reqDetails.to_groups && reqDetails.to_groups.length > 1 && ` and other ${reqDetails.to_groups.length - 1} groups`}
                                            </h3>
                                            <h4>{`${moment(reqDetails.created_at).format('MMM DD YYYY HH:mm A')}`}</h4>
                                            <span>
                                                <em>
                                                    {`${moment.unix(reqDetails.start_date).format('MMM DD YYYY ')}`} -
                                    {`${reqDetails.end_date?moment.unix(reqDetails.end_date).format('MMM DD YYYY '):''}`}
                                                </em>
                                                <em>{reqDetails.request_location}</em>
                                            </span>
                                        </div>
                                    </div>

                                    <div className="f-req-card-list">
                                        {reqDetails.items && reqDetails.items.length > 0 && reqDetails.items.map((item, index) => {
                                            return (
                                                <div key={index} className="f-req-card">
                                                    <div className="f-top">
                                                        <h4>{item.request_item_title}</h4>
                                                        <p>{item.request_item_description}</p>
                                                    </div>
                                                    <div className="f-bottom">
                                                        <div className="f-left">
                                                            {(item.item_quantity - item.received_amount) > 0 && <strong><em>
                                                                {item.item_quantity - item.received_amount}
                                                            </em> of {item.item_quantity}</strong>}
                                                            <p>{(item.item_quantity - item.received_amount) > 0 ?
                                                                'Still needed' : ' Completed'}</p>
                                                        </div>
                                                        <div className="f-right">
                                                            <Button
                                                                disabled={item.total_contribution === 0 || item.total_contribution === null}
                                                                className="f-line f-btn-green"
                                                                onClick={() => redirectToContr(item.item_id)}>
                                                                View Contribution
                                                    </Button>
                                                            <Button
                                                                className="f-btn-green"
                                                                onClick={() => reqDetails.request_type === 'general' ? showContrModal(item) : toggleStripeModal(item)}
                                                            >Support
                                                </Button>
                                                            {is_admin && reqDetails.request_type === "fund" && <Button
                                                                className="f-btn-green"
                                                                onClick={() => displayOfflineSupportPage(item)}
                                                            >Support Admin
                                                </Button>}
                                                        </div>
                                                    </div>
                                                </div>)
                                        })
                                        }
                                    </div>
                                </React.Fragment>}
                        </div>}
                    </div>
                </div>
            </div>

            {/* Modal - Contribute */}
            <Modal
                visible={visible}
                title=""
                onOk={() => handleOk()}
                onCancel={() => handleCancel()}
                footer={['']}
                className="f-request-modal">
                <h4>Please contribute towards to the request</h4>
                <h2>{modalData.request_item_title}</h2>
                <div className="f-qty">
                    <strong>
                        <em>{
                            (modalData.item_quantity - modalData.received_amount) > 0 ?
                                modalData.item_quantity - modalData.received_amount : 0}</em> of {modalData.item_quantity}
                    </strong>
                    <p>{(modalData.item_quantity - modalData.received_amount) > 0 ?
                        'Still needed' : ' Completed'}</p>
                </div>
                <Divider />
                <div className="f-qty-form">
                    <p>Here is my part of contribution</p>
                    <Input value={modalData.contribute_item_quantity ? modalData.contribute_item_quantity : 0} placeholder="Enter Quantity" onChange={(e) => handleChange(e)} />
                </div>
                <div className="f-mdl-submit">
                    <Button className="f-btn-green" onClick={() => contributeFn()}>Contribute</Button>
                </div>
            </Modal>
            {/* Modal End */}

            {/* Modal - Thanks */}
            <ThankYOU
                thankyou={thankyou}
                ty_handleCancel={ty_handleCancel}
                ty_handleOk={ty_handleOk}
                callNow={callNow}
            />
            {/* Modal End */}

        </section>
    );
}
export default RequestDetails;