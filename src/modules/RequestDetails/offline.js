import React, { useEffect, useState } from 'react'
import OfflineMembership from '../../components/OfflineMembership'
import { searchUsersNeed, contrByAdmin, getContrListByAdmin,deleteSelectedEntry } from "../RequestDetails/req_service";
import { useSelector } from 'react-redux';


const OfflinePay = ({ location }) => {
    const [state, setState] = useState({
        group_id: undefined,
        item_id: undefined,
        user_id: useSelector(stateX => stateX.user_details.userId),
        user_phone: useSelector(stateY => stateY.user_details.phone),
        request_id:undefined
    })
    let { group_id, item_id,user_id,request_id,user_phone } = state
    useEffect(() => {
        let { state:xState } = location
        let { group_id:Gid, item_id:I_id,request_id:rid } = xState;
        setState(prev => ({ ...prev, group_id: Gid, item_id: I_id,request_id:rid }))
    }, [location])
    return (
        <React.Fragment>
            {group_id&&<OfflineMembership
                userList={searchUsersNeed}
                user_id={user_id}
                group_id={group_id}
                item_id={item_id}
                contrByAdmin={contrByAdmin}
                getContrListByAdmin={getContrListByAdmin}
                request_id={request_id}
                user_phone={user_phone}
                deleteSelectedEntry={deleteSelectedEntry}
            />}
        </React.Fragment>
    )
}


export default OfflinePay