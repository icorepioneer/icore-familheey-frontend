import React from 'react';
class Policy extends React.Component {
    render() {
        return (
            <section className="f-clearfix f-static-page">
                <header className="f-head-skelton">
                    <div className="f-boxed">
                        <div className="f-head-dock">
                            <a href="javascript:void(0);" onClick={() => this.props.history.push(`/post`)} className="f-brand"><img src={require('../../assets/images/logo-m.png')} alt="Familheey" /></a>
                        </div>
                    </div>
                </header>
                <section className="f-page">
                    <section className="f-clearfix f-body">
                        <section className="f-boxed">
                            <section className="f-clearfix">
                                {/* <h1>Privacy Policy</h1>
                                <p>My Family Group Inc built the Familheey app as a Freemium app. This SERVICE is provided by My Family Group Inc at no cost and is intended for use as is.</p>
                                <p>This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p>
                                <p>If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p>
                                <p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Familheey unless otherwise defined in this Privacy Policy.</p>
                                <h2>Information Collection and Use</h2>
                                <p>For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to Name, Date of Birth, Gender, City, Email. The information that we request will be retained by us and used as described in this privacy policy.</p>
                                <p>The app does use third party services that may collect information used to identify you.</p>
                                <p>We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</p>
                                <h2>Cookies</h2>
                                <p>Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.</p>
                                <p>This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p>
                                <h2>Service Providers</h2>
                                <p>We may employ third-party companies and individuals due to the following reasons:</p>
                                <ul>
                                    <li>To facilitate our Service;</li>
                                    <li>To provide the Service on our behalf;</li>
                                    <li>To perform Service-related services; or</li>
                                    <li>To assist us in analyzing how our Service is used.</li>
                                </ul>
                                <p>We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>
                                <h2>Security</h2>
                                <p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>
                                <h2>Links to Other Sites</h2>
                                <p>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>
                                <h2>Children’s Privacy</h2>
                                <p>These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>
                                <h2>Changes to This Privacy Policy</h2>
                                <p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.</p>
                                <h2>Contact Us</h2>
                                <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at contact@familheey.com.</p>
                        */}
                                <h1>Privacy Policy</h1>
                                <p>This Privacy Policy is designed to inform users of My Family Group Inc., ("My Family Group") websites and mobile applications (collectively, the "Service") about how we gather and use personal information collected by us in connection with your use of the Services. This Privacy Policy applies to all of our Services unless specified otherwise. Please read Terms of Service ("Terms"), which describes the terms under which you use our Services.</p>
                                <p>Your Personal Information includes information that individually identifies you or is information about you that can be traced back to you, your IP address, or your location. It may include, but is not limited to your name, address, employer, email address, phone number, and other information about you.</p>
                                <p><b>If you do not agree with this Privacy Policy, you must not accept these terms, and may not use the Site or Service.</b></p>
                                <p>My Family Group collects some information during the course of providing our Services to you, including when you register, install, access, or use our Services. The types of information we receive and collect is as follows:</p>
                              <p><h2>INFORMATION YOU PROVIDE</h2></p>  
                                <ul>
                                    <li><b>Your Account Information.</b>You provide your email address and mobile phone number, and other basic information to create a Familheey account. You provide us, all in accordance with applicable laws, the phone numbers in your mobile address book on a regular basis, including those of both the users of our Services and your other contacts. You may provide us additional information in your profile, such as a profile picture, your birthday, your location, and about information.</li>
                                    <li><b>Your Messages.</b>We do not retain your messages in the ordinary course of providing our Services to you. Once your messages, including texts, photos, videos, voice messages, files, and share location information, are delivered, they are deleted from our servers. Your messages are stored on your own device. If a message cannot be delivered immediately (for example, if you are offline), we may keep it on our servers for up to 30 days as we try to deliver it. If a message is still undelivered after 30 days, we delete it. To improve performance and deliver media messages more efficiently, such as when many people are sharing a popular photo or video, we may retain that content on our servers for a longer period of time. We use end-to-end encryption for our Services. End-to-end encryption means that your messages are encrypted to protect against us and third parties from reading them.</li>
                                    <li><b>Your Contacts.</b>To help you communicate with others, we may help you identify your contacts who also use Familheey, and you can create, join, or get added to groups and broadcast lists, and such groups and lists get associated with your account information. You give your groups a name. You may choose to provide a group profile picture or description.</li>
                                    <li><b>Your Use of Our Payments Services.</b>If you use our payment services available in your country, we process purchase and transaction information as described in the applicable Payments Privacy Policy.</li>
                                    <li><b>Customer Support.</b>You may provide us with information related to your use of our Services, including copies of your messages, and how to contact you so we can provide you customer support. For example, you may send us an email with information relating to our app performance or other issues.</li>
                                </ul>
                               <p><h2>INFORMATION WE COLLECT</h2></p> 
                                <ul>
                                    <li><b>Usage and Log Information.</b>We collect information about your activity on our Services, like service-related, diagnostic, and performance information. This includes information about your activity (including how you use our Services, your Services settings, how you interact with others using our Services, and the time, frequency, and duration of your activities and interactions), log files, and diagnostic, crash, website, and performance logs and reports. This also includes information about when you registered to use our Services, the features you use such as messaging, calling, Status, or groups features, profile photo, about information, whether you are online, when you last used our Services, and when you last updated your profile.</li>
                                    <li><b>Device and Connection Information.</b>We collect device and connection-specific information when you install, access, or use our Services. This includes information like hardware model, operating system information, battery level, signal strength, app version, browser information, and mobile network, connection information including phone number, mobile operator or ISP, language and time zone, and IP address, and device operations information.</li>
                                    <li><b>Location Information.</b>We collect device location information if you use our location features, like when you choose to share your location with your contacts, view locations nearby or those others have shared with you, and the like, and for diagnostics and troubleshooting purposes such as if you are having trouble with our app's location features. We use various technologies to determine location, including IP address, GPS, Bluetooth signals, and information about nearby Wi-Fi access points, beacons, and cell towers.</li>
                                    <li><b>Cookies.</b>We use cookies to provide Familheey for web and desktop and other web-based services. Additionally, we may use cookies to remember your choices to provide a safer experience, and otherwise to customize our Services for you.</li>
                                </ul>
                              <p><h2>THIRD PARTY INFORMATION</h2></p>  
                                <ul>
                                    <li><b>Information Others Provide About You.</b>We receive information about you from other users and businesses. For example, when other users or businesses you know use our Services, they may provide your phone number, name, and other information (like information from their mobile address book or in the case of businesses, additional information about you such as unique identifiers), just as you may provide theirs, or they may send you a message, send messages to groups to which you belong, or call you. We require each of these users and businesses to have lawful rights to collect, use, and share your information before providing any information to us.</li>
                                    <li><b>Businesses on Familheey.</b>Businesses you interact with using Familheey provide us information about their interactions with you. A business on Familheey may also use another company to assist it in storing, reading, and responding to your messages on behalf of and in support of that business. Please note that when businesses use third-party services, their own terms and privacy policies will govern your use of those services and their use of your information on those services.</li>
                                    <li><b>Third-Party Service Providers.</b>We work with third-party service providers to help us operate, provide, improve, understand, customize, support, and market our Services. For example, we work with companies to distribute our apps, provide our infrastructure, delivery, and other systems, supply location, map, and places information, process payments, help us understand how people use our Services, market our Services, help you connect with businesses using our Services, conduct surveys and research for us, and help with customer service. These companies may provide us information about you in certain circumstances; for example, app stores may provide us reports to help us diagnose and fix service issues.</li>
                                    <li><b>Third-Party Services.</b>We allow you to use our Services in connection with third-party services and Facebook Company Products. If you use our Services with such third-party services or Facebook Company Products, we may receive information about you from them; for example, if you use the Familheey share button on a news service to share a news article with your Familheey contacts, groups, or broadcast lists on our Services, or if you choose to access our Services through a mobile carrier's or device provider's promotion of our Services. Please note that when you use third-party services or Facebook Company Products, their own terms and privacy policies will govern those services.</li>
                                </ul>
                               <p><h2>HOW WE USE INFORMATION</h2></p> 
                                <p>We use the information we have (subject to choices you make) to operate, provide, improve, understand, customize, support, and market our Services. Here's how:</p>
                                <ul>
                                    <li><b>Our Services.</b>We use the information we have to operate and provide our Services, including providing customer support, and improving, fixing, and customizing our Services. We analyze and use the information we have to evaluate and improve our Services, develop, and test new services and features, and conduct troubleshooting activities. We also use your information to provide customer support when you contact us.</li>
                                    <li><b>Safety and Security within Familheey.</b>We use the information to verify investigate suspicious activity or violations of our Terms, and to ensure our Services are being used legally.</li>
                                    <li><b>No Advertisements.</b>We do not allow third-party advertisement on Familheey.</li>
                                    <li><b>We do not rent, sell, or share Personal Information</b> with other people or non-affiliated companies.</li>
                                    <li><b>We do not disclose Personal Information in response to a legal process,</b> for example, in response to a court order or a subpoena to comply with its applicable legal and regulatory reporting requirements, without your consent.</li>
                                    <li><b>We do not disclose Personal Information to law enforcement agencies in response to a law enforcement agency's</b> request without your consent.</li>
                                    <li><b>Security Measure.</b>We have taken reasonable and necessary steps to ensure that all Personal Information collected will remain secure by applying end to end encryption. We have put in place appropriate physical, electronic, and administrative procedures in an effort to safeguard and help prevent unauthorized access, maintain data security, and correctly use the Personal Information that we collect.</li>
                                </ul>
                               <p><h2>ACCESSING, CHANGING, AND DELETING YOUR INFORMATION</h2></p> 
                                <p>You may request access, changes, or deletions to Personal Information and request information about our collection, use and disclosure of such information by using Service Settings, changing your profile, or deleting your Familheey account. We use best efforts to keep our records as accurate and complete as possible. You can help us maintain the accuracy of your information by notifying us of any changes to Personal Information as soon as possible. Your rights to access, change, or delete Personal Information are not absolute. We may deny you such rights when required by law or if the request would likely reveal Personal Information about a third-party.</p>
                                <p><h2>HOW THE GENERAL DATA PROTECTION REGULATION APPLIES TO OUR EUROPEAN REGION USERS</h2></p>
                                <ul>
                                    <li><b>We collect, use, and share the information we have as described above:</b>
                                        <ul>
                                            <li>as necessary to fulfill our Terms;</li>
                                            <li>consistent with your consent, which you can revoke at any time;</li>
                                            <li>as necessary to comply with our legal obligations;</li>
                                            <li>occasionally to protect your vital interests, or those of others;</li>
                                            <li>as necessary in the public interest; and</li>
                                            <li>as necessary for our legitimate interests, including our interests in providing an innovative, relevant, safe, and profitable service to our users and partners, unless those interests are overridden by your interests or fundamental rights and freedoms that require protection of personal data.</li>
                                        </ul>
                                    </li>
                                    <li><b>How You Exercise Your Rights:</b>Under the General Data Protection Regulation or other applicable local laws, you have the right to access, rectify, port, and erase your information, as well as the right to restrict and object to certain processing of your information. This includes the right to object to our processing of your information for direct marketing and the right to object to our processing of your information where we are performing a task in the public interest or pursuing our legitimate interests or those of a third party. You can access or port your information using our in-app Request Account Info feature available under Settings. You can access tools to rectify, update, and erase your information directly in-app as described in the Managing and Deleting Your Information section. If we process your information based on our legitimate interests or those of a third party, or in the public interest, you can object to this processing, and we will cease processing your information, unless the processing is based on compelling legitimate grounds or is needed for legal reasons.</li>
                                </ul>
                                <p><h2>UPDATES TO OUR POLICY</h2></p>
                                <p>We will notify you before we make changes to this Privacy Policy and give you the opportunity to review the revised Privacy Policy before you choose to continue using our Services.</p>
                                <p><h2>CONTACT INFORMATION</h2></p>
                                <p>My Family Group Inc.</p>    
                                <p>P.O. Box 20327</p>     
                                <p>Houston, TX 77225</p>
                            </section>
                        </section>
                    </section>
                </section>

            </section>
        )
    }
}

export default Policy;
