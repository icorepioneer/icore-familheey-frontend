import React from 'react';
import './advanceSettings.css'
import { submitSettings } from './api-advsettings'
import { Radio ,Switch } from 'antd';
// const { Option } = Select;
// const { confirm } = Modal;
const RadioGroup = Radio.Group;
class AdvanceSettings extends React.Component {
    state;
    constructor(props) {
      super(props);
  
      this.state = {
        advanceSettings:{  
        group_id:'37',
        member_joining: '',
        member_approval:'0',
        post_create: '',
        post_approval: '',
        post_visibilty: '',
        link_family: '',
        link_approval:''
        }
      };
  
    }
    changeField(e, fieldname) {
      this.state.advanceSettings[fieldname] = e.target.value;
      this.setState({ advanceSettings: this.state.advanceSettings });
    }
    submit() {
        submitSettings(this.state.advanceSettings)
        .then((res)=>console.log(res,"response"))
        .catch((err)=>console.log(err));
    }
    render() {
        return (
            <div>
                <div className="rone-accordion-box">

                    <div className="rone-form-control"  value={this.state.member_joining} onChange={(e)=>this.changeField(e,'member_joining')}>
                        <label><strong>How members can Join <i>*</i></strong></label>
                        <RadioGroup className="rone-rgroup-hz">
                            <Radio value={1}>Invitation Only</Radio>
                            <Radio value={2}>Any one can join-with approval</Radio>
                            <Radio value={3}>Anycone can Join</Radio>
                        </RadioGroup>
                    </div>
                    <div className="rone-form-control"  value={this.state.post_create}  onChange={(e)=>this.changeField(e,'post_create')}>
                        <label><strong>Who can create posts, events, albums and documents? <i>*</i></strong></label>
                        <RadioGroup className="rone-rgroup-hz">
                            <Radio value={6}>Members</Radio>
                            <Radio value={7}>Admin only</Radio>
                        </RadioGroup>
                    </div>
                    {/* <div className="rone-form-control" value={this.state.post_approval}  onChange={(e)=>this.changeField(e,'post_approval')}>
                        <label><strong>Post Approval <i>*</i></strong></label>
                        <RadioGroup className="rone-rgroup-hz" >
                            <Radio value={10}>Not needed</Radio>
                            <Radio value={12}>Members </Radio>
                            <Radio value={11}>Admin </Radio>
                        </RadioGroup>
                    </div> */}
                    <div className="rone-form-control" value={this.state.post_visibilty}  onChange={(e)=>this.changeField(e,'post_visibilty')}>
                        <label><strong>Post Visibility <i>*</i></strong></label>
                        <RadioGroup className="rone-rgroup-hz">
                            <Radio value={8}>Members Only </Radio>
                            <Radio value={9}>Public </Radio>
                        </RadioGroup>
                    </div>
                    <div className="rone-form-control"  value={this.state.link_family}  onChange={(e)=>this.changeField(e,'link_family')}>
                        <label><strong>Link Family <i>*</i></strong></label>
                        <RadioGroup className="rone-rgroup-hz">
                            <Radio value={13}>Admins </Radio>
                            <Radio value={14}>Members </Radio>
                        </RadioGroup>
                    </div>
                    <div className="rone-form-control"  value={this.state.link_approval}  onChange={(e)=>this.changeField(e,'link_approval')}>
                        <label><strong>Link Family Approval <i>*</i></strong></label>
                        <RadioGroup className="rone-rgroup-hz">
                            <Radio value={15}>Not Needed </Radio>
                            <Radio value={16}>Admin </Radio>
                        </RadioGroup>
                    </div>
                    <label>Documents</label>
                    <span>
                    <Switch checkedChildren="Yes" unCheckedChildren="No" defaultChecked={true} />
                    </span>
                    <label>History</label>
                    <span>
                    <Switch checkedChildren="Yes" unCheckedChildren="No" defaultChecked={true} />
                    </span>
                    <label>Linked Families</label>
                    <span>
                    <Switch checkedChildren="Yes" unCheckedChildren="No" defaultChecked={true} />
                    </span>
                    <label>Programs</label>
                    <span>
                    <Switch checkedChildren="Yes" unCheckedChildren="No" defaultChecked={true} />
                    </span>
                    <label>Current Vacancies</label>
                    <span>
                    <Switch checkedChildren="Yes" unCheckedChildren="No" defaultChecked={true} />
                    </span>
                    <button onClick={()=>this.submit()}>SUBMIT</button>
                </div>


            </div>
        )
    }
}

export default AdvanceSettings;