import axios from '../../services/apiCall';

const listAllGroupMember = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/viewMembers`, data)
}
const listAllRequest = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/admin_link_request`, data)

}
const actionToRequest = (data) => {

    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/admin_action_linking`, data)

}
const getallRelations = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getallRelations`, data)

}
const updateGroupMap = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_groupmaps`, data)

}
const listAllMembers = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listAllMembers`, data)

}
const listAllGroupRequests = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/admin_view_requests`, data)
}
const getAllBlockedUsers = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/blocked_users`, data)
}
const addMemberToGroup = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/addToGroup`, data)
}
const memberUpdateGroupmaps = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_groupmaps`, data)
}
const leaveFamily = (data) =>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/leave_family`,data) 
}


export {
    listAllGroupMember,
    listAllRequest,
    actionToRequest,
    getallRelations,
    updateGroupMap,
    listAllMembers,
    listAllGroupRequests,
    getAllBlockedUsers,
    addMemberToGroup,
    memberUpdateGroupmaps,
    leaveFamily,
} 