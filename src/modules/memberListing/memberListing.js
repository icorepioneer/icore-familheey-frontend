import React from 'react';
import { Tabs, Icon, Avatar, Tag } from 'antd';
import { listAllGroupMember, listAllRequest, actionToRequest, getallRelations, updateGroupMap, listAllMembers } from './api-memberlist';
import {connect} from 'react-redux'
const { TabPane } = Tabs;
// const { Search } = Input;


class MemberListing extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;
        let { userID } = props;
        this.state = {
            user_id: userID,
            S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,
            memberArray: [],
            showSearch: false,
            listRequest: [],
            visible: false,
            addMemberList: [],
            checkedMembers: []
        }
        this.setState({ group_id: props.group_id });
        this.showGroupMember();
        this.showLinkRequest();
    }
    callback = (key) => {
        console.log(key);
    }

    componentWillMount() {

    }

    showGroupMember = () => {
        if (this.props.group_id) {
            var params = {
                group_id: this.props.group_id.toString(),
                crnt_user_id: this.state.user_id
            }
            listAllGroupMember(params).then((rep) => {
                if (rep && rep.data.data.length > 0) {
                    this.setState({ memberArray: rep.data.data })
                }
            }).catch((error) => {
                console.log('error in listAllGroupMember', error);
            })
        }
    }
    addNewMember = () => e => {
        // this.state.showSearch = true;
        // this.setState({ showSearch: this.state.showSearch })
        var params = {
            client_id: this.state.user_id,
            group_id: this.props.group_id.toString()
        }
        listAllMembers(params).then((r_response) => {
            let response = r_response.data;
            if (response && response.rows && response.rows.length > 0) {
                this.setState({ visible: true, addMemberList: response.rows })
            }
        }).catch((error) => {
            console.log('errorr', error);
        })
    }
    backToMemberlist = () => {
        var showSearch = false;
        this.setState({ showSearch: showSearch })
    }
    showLinkRequest = () => {
        var parasm = {
            group_id: this.props.group_id.toString()
        }
        listAllRequest(parasm).then((data) => {
            if (data && data.requests && data.requests.length > 0) {
                this.setState({ listRequest: data.requests })
            }
        }).catch((error) => {
            console.log('error in listAllRequest', error);
        })
    }
    acceptTheRequest = (value, status) => e => {
        var params = {
            id: value,
        }
        if (status === 'accept') {
            params.status = 'accepted'
        } else {
            params.status = 'rejected'
        }
        actionToRequest(params).then((data) => {
            if (data && data.data) {
                var listRequest = [];
                listRequest = this.state.listRequest.filter(function (item) {
                    return item.id !== value
                })
                this.setState({ listRequest: listRequest })
            }
        }).catch((error) => {
            console.log('errr', error);
        })
    }
    getRelationships = () => {
        getallRelations().then((data) => {
            console.log('data', data);
        }).catch((error) => {
            console.log('error', error);
        })
    }

    blockMember = () => {
        updateGroupMap().then((data) => {
            console.log('data', data);
        }).catch((error) => {
            console.log('error', error);
        })
    }

    checkedValue = (value) => e => {
        var checkedMembers = [];
        if (this.state.checkedMembers.indexOf(value) === -1) {
            checkedMembers.push(value);
            this.setState({ checkedMembers: checkedMembers })
        } else {
            checkedMembers = this.state.checkedMembers.filter(function (item) {
                return item !== value
            })
            this.setState({ checkedMembers: checkedMembers })
        }
        console.log('this.state.checkedMembers', this.state.checkedMembers);
    }


    render() {
        return (
            <div>
                {!this.state.showSearch &&
                    <div>
                        <Tabs defaultActiveKey="1" onChange={this.callback}>
                            <TabPane tab="Tab 1" key="1">
                                {this.state.memberArray && this.state.memberArray.length > 0 &&
                                    <div>
                                        {this.state.memberArray.map((value, index) => {
                                            return <div key={index}>
                                                <Avatar src={process.env.REACT_APP_PRO_PIC_CROP + this.state.S3_BASE_URL + 'propic/' + value.propic} shape="square" icon="user" />

                                                <div >
                                                    {value.full_name}
                                                    <Tag>{value.is_primar_admin}</Tag>
                                                    {value.gender}
                                                </div>
                                                <button><Icon type="more" /></button>

                                            </div>
                                        })
                                        }
                                    </div>
                                }
                                
                            </TabPane>
                            <TabPane tab="Tab 2" key="2">
                                {this.state.listRequest && this.state.listRequest.length > 0 &&
                                    <div>
                                        {this.state.listRequest.map((value, index) => {
                                            return <div key={index}><div key={value.id}>{value.from_group}
                                                <button onClick={this.acceptTheRequest(value.id, 'accept')}>accept</button><button onClick={this.acceptTheRequest(value.id, 'cancel')}>Cancel</button></div>
                                            </div>
                                        })
                                        }
                                    </div>
                                }
                            </TabPane>
                            <TabPane disabled tab="Tab 3" key="3">
                                Content of Tab Pane 3
                            </TabPane>
                        </Tabs>
                    </div>
                }

                {/* {this.state.showSearch &&
                    <div>
                        <button onClick={this.backToMemberlist}><Icon type="arrow-left" /></button>
                        <Search placeholder="input search text" onSearch={value => console.log(value)} enterButton />
                    </div>
                } */}
            </div>
        )
    }

}

const mapStateToProps=(state)=>{
    let {userId} = state.user_details;
    return {
        userID:userId
    }
}

export default connect(mapStateToProps,null)(MemberListing)