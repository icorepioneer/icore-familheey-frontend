import React, { useState, useEffect } from "react";
import "./style.scss";
//import { Link } from 'react-router-dom'
import { Input, Button, Select, DatePicker, TimePicker, Form, Tag, Radio, Icon } from "antd";
import AddRequest from "./components";
import GroupList from './components/single_selectable'
import ListReq from './components/list'
import ReactGoogleMapLoader from "react-google-maps-loader";
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import moment from "moment";
import GroupModal from '../Post/CreatePost/components/groups'
import { CreateNeeds, GetRequestDetails, AddNewItem, UpdateCurrentItem, deleteSelected, updateNeed, generateLink, chekActiveORNot } from './api'
import { notifications } from "../../shared/toasterMessage";
import { useSelector } from 'react-redux'
import {decryption} from '../../shared/encryptDecrypt'
// import Search from "antd/lib/input/Search";
const MY_API_KEY = decryption(process.env.REACT_APP_PLACE_ENCRYPT);
const { Option } = Select;
const { Search } = Input
function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}
const CreateFrom = Form.create({ name: 'need_form' })(
    class extends React.Component {
        render() {
            let { form, search, location, handleSelectSuggest, handleInputChange,
                toggleModalView, list_needs, setItemToEdit, handleCreate, handleSelect,
                current_selected, tagArray, removeIDFromArray, deleteItem, add_button_loader,
                mode, disable_functions, default_type, changeDefaultType, disable_add_button,
                handleBankAndStripe, bankAndStripeHandled, displaybankView, canCreateFund: can_CreateFund
            } = this.props;
            let isDisabled = (mode == 'Edit') ? true : false;
            if (mode == 'Edit') { disable_functions = true }
            const { getFieldDecorator, getFieldsError } = form;
            const disabledStartDate = (start) => {
                return moment(start).isSameOrBefore(moment(new Date()).subtract(1, "day"));
            };
            return (
                <div className="f-clearfix">
                    <Form layout="vertical" >
                        <Form.Item label="Request type">
                            {getFieldDecorator('rq_type', { initialValue: `${default_type}` })(
                                <Radio.Group onChange={(event) => { changeDefaultType(event.target.value) }}>
                                    <Radio disabled={isDisabled} value={"general"}>General</Radio>
                                    {can_CreateFund && <Radio disabled={isDisabled} value={"funding"}>Funding</Radio>}
                                </Radio.Group>
                            )}
                        </Form.Item>
                        {default_type === 'general' && <Form.Item label="Post this request to">
                            {getFieldDecorator('to_group_id', {
                                rules: [{ required: true, message: 'Please choose one of the option ' }],
                                initialValue: current_selected
                            })(<Select
                                disabled={disable_functions}
                                placeholder="Select"
                                style={{ width: "100%" }}
                                onSelect={(e) => handleSelect(e)}
                            >
                                <Option key="all_family">All Families</Option>
                                <Option key="selected_families">Selected families</Option>
                            </Select>)}
                        </Form.Item>}
                        {default_type === 'funding' && <Form.Item label="Post this request to">
                            {getFieldDecorator('to_group_id', {
                                rules: [{ required: true, message: 'Please choose one of the option ' }],
                                initialValue: current_selected
                            })(<Select
                                disabled={disable_functions}
                                placeholder="Select"
                                style={{ width: "100%" }}
                                onSelect={(e) => handleSelect(e)}
                            >
                                <Option key="selected_families">Selected families</Option>
                            </Select>)}
                        </Form.Item>}
                        {current_selected === 'selected_families' &&
                            <div
                                style={{ marginTop: '10px' }}>{tagArray && tagArray.map((item, key) => {
                                    return <Tag
                                        closable={!disable_functions}
                                        key={key}
                                        color="#7e57c2"
                                        onClose={(e) => removeIDFromArray(item, e)}
                                    >{item.group_name || item.full_name}
                                    </Tag>
                                })}</div>
                        }
                        {(default_type === 'funding' && tagArray && tagArray.length === 1 && !bankAndStripeHandled) && handleBankAndStripe()}
                        {(default_type === 'funding' && tagArray && tagArray.length === 1 && bankAndStripeHandled) && displaybankView()}
                        <div className="f-form-control" style={{ height: '80px' }}>
                            <label><span className="star">*</span>Start Date & Time</label>
                            <div className="f-split" style={{ marginTop: "0" }}>
                                <div>
                                    <Form.Item>
                                        {getFieldDecorator('start_date', {
                                            initialValue: moment(new Date(), "MMM/DD/YYYY"),
                                            rules: [{ required: true, message: 'Start date needed' }],
                                        })(
                                            <DatePicker
                                                // disabledDate={disabledStartDate}
                                                format={"MMM/DD/YYYY"} />)}
                                    </Form.Item>
                                </div>
                                <div>
                                    <Form.Item>
                                        {getFieldDecorator('start_time', {
                                            initialValue: moment(new Date(), "hh:mm a"),
                                            rules: [{ required: true, message: 'Start time needed' }]
                                        })(
                                            <TimePicker
                                                format={"hh:mm a"}
                                                use12Hours
                                            />)}
                                    </Form.Item>
                                </div>
                            </div>
                        </div>
                        <div className="f-form-control" style={{ height: '80px' }}>
                            <label><span className="star">*</span>End Date & Time</label>
                            <div className="f-split" style={{ marginTop: "0" }}>
                                <div>
                                    <Form.Item >
                                        {getFieldDecorator('end_date', {
                                            // rules: [{ required: true, message: 'End date needed' }]
                                        })(
                                            <DatePicker
                                                disabledDate={disabledStartDate}
                                                format={"MMM/DD/YYYY"} />)}
                                    </Form.Item>
                                </div>
                                <div>
                                    <Form.Item>
                                        {getFieldDecorator('end_time', {
                                            // rules: [{ required: true, message: 'End time needed' }]
                                        })(
                                            <TimePicker
                                                format={"hh:mm a"}
                                                use12Hours
                                            />)}
                                    </Form.Item>
                                </div>
                            </div>
                        </div>
                        <div className="f-form-control">
                            <label><span className="star">*</span>Location</label>

                            <ReactGoogleMapLoader
                                params={{
                                    key: MY_API_KEY,
                                    libraries: "places,geocode",
                                }}
                                render={(googleMaps) =>
                                    googleMaps && (
                                        <ReactGooglePlacesSuggest
                                            googleMaps={googleMaps}
                                            autocompletionRequest={{
                                                input: search,
                                            }}
                                            onSelectSuggest={handleSelectSuggest}
                                            textNoResults="My custom no results text"
                                        >
                                            <Form.Item className="fh-location-fix-form">
                                                {getFieldDecorator('request_location', {
                                                    rules: [{ required: true, message: 'location needed' },
                                                    {
                                                        initialValue: location
                                                    }
                                                    ]
                                                })(
                                                    <Search
                                                        className="art-mat-style art-suffix art-sfx-location"
                                                        placeholder="Location"
                                                        onChange={(e) => handleInputChange(e)}
                                                        autoComplete="off"
                                                    />
                                                )}
                                            </Form.Item>
                                        </ReactGooglePlacesSuggest>)
                                } />

                        </div>
                        {list_needs.length < 1 && <div className="f-request-listing">
                            <div className="f-listing-empty">
                                <h3>Your family is here for you!</h3>
                                <p><span className="star">*</span>Add your requests and seek help instantly</p>
                                <Button
                                    className="f-submit f-btn-green"
                                    onClick={() => toggleModalView()}
                                >
                                    Add Request
                                        </Button>
                            </div>
                        </div>}
                        {list_needs.length > 0 &&
                            <ListReq
                                default_type={default_type}
                                list={list_needs}
                                toggleModalView={toggleModalView}
                                setItemToEdit={setItemToEdit}
                                deleteItem={deleteItem}
                            />}
                        <div className="f-create-form-footer">
                            <Form.Item>
                                <Button
                                    loading={add_button_loader}
                                    className="f-submit"
                                    onClick={() => handleCreate()}
                                    disabled={
                                        disable_add_button ||
                                        hasErrors(getFieldsError()) ||
                                        list_needs.length == 0 ||
                                        (default_type === 'general' ? false : tagArray && tagArray.length === 0)
                                    }
                                >{mode === 'Add' ? 'Add' : 'Update'}</Button>
                            </Form.Item>
                        </div>
                    </Form>
                </div>

            );
        }
    }
)

const RequestForHelp = ({ history, location }) => {
    let formReff;
    const [state, setRequestState] = useState({
        user_id: useSelector(stateX => stateX.user_details.userId),
        modal_visible: false,
        search: "",
        location_: '',
        list_needs: [],
        item_to_edit: undefined,
        groupList: [],
        fam_modal: false,
        tagArray: [],
        current_selected: undefined,
        mode: "Add",
        need_request_id: undefined,
        add_button_loader: false,
        disable_functions: false,
        default_type: "general",
        generatedLink: '',
        link_generated: false,
        disable_add_button: false,
        from_family_name: undefined,
        from_family_id: undefined,
        from_family: false,
        hasAStripeAccount: true,
        bankAndStripeHandled: false,
        bankViewStatus: undefined,
        canCreateFund: true,
        status_500_message:undefined
    });
    let { modal_visible, search, location_, list_needs, item_to_edit, groupList, fam_modal, tagArray,
        current_selected, user_id, mode, need_request_id, add_button_loader, disable_functions, default_type,
        generatedLink, link_generated, disable_add_button,status_500_message,
        from_family_name, from_family_id, from_family, hasAStripeAccount, bankAndStripeHandled, bankViewStatus, canCreateFund
    } = state;

    const saveFormRef = formRef => {
        if (formRef) { formReff = formRef }
    };
    const toggleModalView = () => {
        setRequestState((prev) => ({ ...prev, modal_visible: !modal_visible, item_to_edit: undefined }));
    };
    const handleSelectSuggest = (geocodedPrediction, originalPrediction, formr = formReff) => {
        setRequestState((prev) => ({
            ...prev,
            search: "",
            value: geocodedPrediction.formatted_address,
            location_: geocodedPrediction.formatted_address,
        }));
        const { form } = formr.props;
        form.setFields({
            request_location: { value: geocodedPrediction.formatted_address },
        })
    };
    const handleInputChange = (e) => {
        e.persist();
        setRequestState((prev) => ({
            ...prev,
            search: e.target.value,
            value: e.target.value,
        }));
    };
    const addToList = (item) => {
        let { item_id, ...rem } = item
        let newItem = { ...rem, item_id: item_id ? item_id : list_needs.length + 1 }
        setRequestState(prev => ({ ...prev, list_needs: [newItem, ...list_needs] }))
    }
    // console.log(list_needs)
    const updateItemFromCurrentList = (item) => {
        let { item_id, id } = item
        let list_needs_ = list_needs
        list_needs_ = list_needs_.filter(itemX => itemX.item_id != (item_id || id))
        let new_set = [item, ...list_needs_]
        setRequestState(prev => ({ ...prev, list_needs: new_set }))
    }
    const setItemToEdit = (item) => {
        setRequestState(prev => ({ ...prev, item_to_edit: item, modal_visible: !modal_visible }))
    }
    const handleCreate = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                setRequestState(prev => ({ ...prev, add_button_loader: false }))
                return;
            }
            let { to_group_id, start_date, end_date, start_time, end_time, request_location } = values
            let start_date_mod = moment(`${moment(start_date).format("MMM/DD/YYYY")} ${moment(start_time).format("hh:mm a")}`).unix()
            let end_date_mod = end_date ? moment(`${moment(end_date).format("MMM/DD/YYYY")} ${moment(end_time).format("hh:mm a")}`).unix() : null
            let to_array = []
            groupList.map(item => {
                to_array.push(item.id)
            })
            let params = {
                user_id: user_id,
                to_group_id: to_array,
                start_date: start_date_mod,
                end_date: end_date_mod,
                request_location: request_location,
                group_type: to_group_id,
                items: list_needs,
                request_type: default_type === 'general' ? 'general' : 'fund'
            }
            if (end_date_mod) {
                if (start_date_mod > end_date_mod) {
                    notifications("error", "error", "End date must be after start date")
                    return;
                }
            }
            setRequestState(prev => ({ ...prev, add_button_loader: true }))
            CreateNeeds(params).then(res => {
                form.resetFields()
                setRequestState(prev => ({ ...prev, groupList: [], tagArray: [], list_needs: [], add_button_loader: false }))
                onBackButtonClicked();
            })
        });
    };
    const handleUpdate = () => {
        const { form } = formReff.props;
        setRequestState(prev => ({ ...prev, add_button_loader: true }))
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            let { to_group_id, start_date, end_date, start_time, end_time, request_location } = values
            let start_date_mod = moment(`${moment(start_date).format("MMM/DD/YYYY")} ${moment(start_time).format("hh:mm a")}`).unix()
            let end_date_mod = moment(`${moment(end_date).format("MMM/DD/YYYY")} ${moment(end_time).format("hh:mm a")}`).unix()
            let to_array = []
            groupList.map(item => {
                to_array.push(item.id)
            })
            let params = {
                post_request_id: `${need_request_id}`,
                user_id: user_id,
                to_group_id: to_array,
                start_date: start_date_mod,
                end_date: end_date_mod,
                request_location: request_location,
                group_type: to_group_id,
            }
            updateNeed(params).then(res => {
                form.resetFields()
                setRequestState(prev => ({ ...prev, groupList: [], tagArray: [], list_needs: [], add_button_loader: false }))
                onBackButtonClicked();
            })
        });
    };
    const toggleModal = () => {
        setRequestState(prev => ({ ...prev, fam_modal: !fam_modal }))
    }
    const fetchSelectedIDS = (idArray, withNames, tag) => {
        switch (tag) {
            case 'family_modal':
                setRequestState(prev => ({ ...prev, groupList: idArray, tagArray: withNames }))
                break;
            case 'user_modal':
                setRequestState(prev => ({ ...prev, userList: idArray, tagArray: withNames }))
                break;
        }
        toggleModal()
    }
    const setTagArrayFromList = (array) => {
        setRequestState(prev => ({ ...prev, tagArray: array }))
    }
    const handleSelect = (e) => {
        if (e === 'selected_families') {
            setRequestState(prev => ({ ...prev, fam_modal: true, current_selected: e }))
        }
        else {
            setRequestState(prev => ({ ...prev, fam_modal: false, current_selected: e }))
        }
    }

    const removeIDFromArray = (item, e) => {
        e.persist()
        e.preventDefault()
        let _tagArray = tagArray
        let _idArray = groupList
        let { id, group_name } = item
        _tagArray = _tagArray.filter(itemY => itemY.id != id && itemY.group_name != group_name)
        _idArray = _idArray.filter(itemZ => itemZ.id != id)
        setRequestState(prev => ({ ...prev, tagArray: _tagArray, groupList: _idArray }))
        default_type === 'funding' && setRequestState(prev => ({
            ...prev, bankAndStripeHandled: false,
            generatedLink: '',
            link_generated: false,
            disable_add_button: false,
            active_or_not_checked: false,
            stripe_active: undefined
        }))
    }
    const onBackButtonClicked = () => {
        history.push('/requests')
    }
    useEffect(() => {
        let { state: Xstate } = location
        if (Xstate.status === 'edit') {
            let { request_id } = Xstate
            let params_req = {
                user_id: user_id,
                post_request_id: `${request_id}`
            }
            GetRequestDetails(params_req).then(res => {
                let { post_request_id, request_location, group_type, start_date, end_date, items, to_groups, request_type } = res.data.data
                const { form } = formReff.props;
                form.setFields({
                    to_group_id: { value: group_type },
                    request_location: { value: request_location },
                    start_date: { value: moment(moment.unix(start_date)) },
                    start_time: { value: moment(moment.unix(start_date)) },
                    end_date: { value: end_date === null ? null : moment(moment.unix(end_date)) },
                    end_time: { value: end_date === null ? null : moment(moment.unix(end_date)) }
                })
                setRequestState(prev => ({
                    ...prev,
                    list_needs: items,
                    mode: "Edit",
                    need_request_id: post_request_id,
                    default_type: `${request_type === 'general' ? 'general' : 'funding'}`,
                    tagArray: to_groups,
                    groupList: to_groups,
                    disable_functions: request_type === 'general' ? false : true,
                    current_selected: group_type
                }))
            }).catch(err => {
                console.log("err", err)
            })
        } else {
            setRequestState(prev => ({ ...prev, mode: 'Add' }))
            if (Xstate.from_family) {
                let { from_family: from_fm } = Xstate
                console.log(from_fm)
                let { id, group_name, stripe_account_id } = from_fm;
                let new_obj = [{ id: id, group_name: group_name, stripe_account_id: stripe_account_id }]
                let new_obj_ = [{ id: id, post_create: 6 }]
                setRequestState(prev => ({
                    ...prev,
                    stripe_account_id_passed: stripe_account_id,
                    current_selected: 'selected_families',
                    disable_functions: true,
                    tagArray: new_obj,
                    groupList: new_obj_,
                    from_family_name: new_obj,
                    from_family_id: new_obj_,
                    from_family: true
                }))
            }
        }
    }, [location])
    const deleteSelectedItem = (item, i) => {
        let list = list_needs
        if (need_request_id) {
            let params = {
                id: `${item.item_id}`,
                user_id: `${user_id}`,
                post_request_id: `${need_request_id}`
            }
            deleteSelected(params).then(res => {
                list = list.filter(it => it.item_id !== item.item_id)
                setRequestState(prev => ({ ...prev, list_needs: list }))
            }).catch(err => {
                console.log("error")
            })
        }
        else {
            list = list.filter(it => (it.item_quantity !== item.item_quantity) && (it.request_item_description !== item.request_item_description))
            setRequestState(prev => ({ ...prev, list_needs: list }))
        }

    }
    const changeDefaultType = (type) => {
        setRequestState(prev => ({
            ...prev, default_type: `${type}`,
            modal_visible: false,
            search: "",
            value: '',
            location_: '',
            list_needs: [],
            item_to_edit: undefined,
            groupList: from_family_id ? from_family_id : [],
            fam_modal: false,
            tagArray: from_family_name ? from_family_name : [],
            current_selected: from_family_id ? 'selected_families' : undefined,
            mode: "Add",
            need_request_id: undefined,
            add_button_loader: false,
            disable_functions: from_family,
            generateStripeButton: false,
            generatedLink: '',
            link_generated: false,
            disable_add_button: false,
            active_or_not_checked: false,
            stripe_active: undefined,
            hasAStripeAccount: type === 'general' ? true : false,
            bankAndStripeHandled: false
        }))
        const { form } = formReff.props;
        form.resetFields()
    }
    const handleBankAndStripe = () => {
        let { stripe_account_id, id } = tagArray[0]
        let params = {
            group_id: `${id}`
        }
        switch (stripe_account_id == null) {
            case true:
                generateLink(params).then(res => {
                    let { data } = res
                    let { link } = data
                    setRequestState(prev => ({ ...prev, 
                        generatedLink: link, 
                        bankViewStatus: 'add_bank_details', 
                        bankAndStripeHandled: true, 
                        disable_add_button: true 
                    }))
                }).catch(err => {
                    console.log(err)
                })
                break;
            case false:
                chekActiveORNot(params).then(res => {
                    let { data } = res
                    let { charges_enabled, payouts_enabled } = data
                    let flag = charges_enabled && payouts_enabled
                    setRequestState(prev => ({
                        ...prev,
                        bankViewStatus: flag ? 'status_active' : 'status_pending',
                        bankAndStripeHandled: true,
                        disable_add_button: !flag
                    }))
                }).catch(({response})=> {
                    let {data} = response
                    setRequestState(prev => ({
                        ...prev,
                        bankViewStatus: 'error_happened',
                        bankAndStripeHandled: true,
                        disable_add_button: true,
                        status_500_message:data.message.replace(" '"," ")
                    }))
                })
                break
        }
    }

    const displaybankView = () => {
        switch (bankViewStatus) {
            case 'add_bank_details':
                return (
                    <Button style={{ marginTop: '20px', marginBottom: '20px' }} onClick={() => {
                        window.open(`${generatedLink}`)
                    }}>Add Banking details</Button>
                )
            case 'status_active':
                return (
                    <div
                        style={{
                            marginTop: '20px',
                            backgroundColor: '#ebebeb',
                            width: '100%',
                            height: '40px',
                            marginBottom: '20px',
                            borderRadius: '4px',
                            padding: '10px'
                        }}>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <span><Icon style={{ marginRight: '10px' }} type='bank' />{`Bank Account`}</span>
                            <span>Active <Icon style={{ marginLeft: '10px' }} type="check-circle" theme="twoTone" twoToneColor="#52c41a" /></span>
                        </div>
                    </div>
                )
            case 'status_pending':
                return (
                    <div
                        style={{
                            marginTop: '20px',
                            backgroundColor: '#ebebeb',
                            width: '100%',
                            height: '60px',
                            marginBottom: '20px',
                            borderRadius: '4px',
                            padding: '10px'
                        }}>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <span><Icon style={{ marginRight: '10px' }} type='bank' />{`Bank Account`}</span>
                            <span>Pending<Icon style={{ marginLeft: '10px' }} type="exclamation-circle" theme="twoTone" twoToneColor="red" /></span>
                        </div>
                        <div style={{ textAlign: 'center' }}><span>You can start fund requesting once your account gets activated</span></div>
                    </div>
                )
            case 'error_happened':
                return (
                    <div
                        style={{
                            marginTop: '20px',
                            backgroundColor: '#ebebeb',
                            width: '100%',
                            height: 'auto',
                            marginBottom: '20px',
                            borderRadius: '4px',
                            padding: '10px'
                        }}>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <span><Icon style={{ marginRight: '10px' }} type='bank' />{`Bank Account`}</span>
                            <span>Error<Icon style={{ marginLeft: '10px' }} type="exclamation-circle" theme="twoTone" twoToneColor="red" /></span>
                        </div>
                        <div style={{ textAlign: 'center' }}>{`${status_500_message.toString()}`}</div>
                    </div>
                )
            default:
                return (
                    <div
                        style={{
                            marginTop: '20px',
                            backgroundColor: '#ebebeb',
                            width: '100%',
                            height: '60px',
                            marginBottom: '20px',
                            borderRadius: '4px',
                            padding: '10px'
                        }}>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <span><Icon style={{ marginRight: '10px' }} type='bank' />{`Bank Account`}</span>
                            <span>Checking....<Icon style={{ marginLeft: '10px' }} type="exclamation-circle" /></span>
                        </div>
                    </div>
                )
        }
    }

    useEffect(()=>{ 
        let { from_family:frm_fam } = location.state;
        if(frm_fam){
            // canCreateFund =  ( parseInt(frm_fam.created_by) == parseInt(user_id) ) ? true : false
            canCreateFund =  ( frm_fam.user_status == 'admin' ) ? true : false
        }
        setRequestState(prev => ({
            ...prev,
            canCreateFund: canCreateFund
        }))
    }, [])
    return (
        <section className="RequestForHelp f-page">
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <Button onClick={() => onBackButtonClicked()} className="f-create-form-back" />
                    <div className="f-clearfix f-create-form-control">
                        <div className="f-create-form-head">
                            <h3>{`${mode} Request`}</h3>
                        </div>
                        <div className="f-create-form-body">
                            <AddRequest
                                modal_visible={modal_visible}
                                toggleModalView={toggleModalView}
                                addToList={addToList}
                                item_to_edit={item_to_edit}
                                mode_from_parent={mode}
                                updateItem={UpdateCurrentItem}
                                addItem={AddNewItem}
                                need_request_id={need_request_id}
                                updateItemFromCurrentList={updateItemFromCurrentList}
                                default_type={default_type}
                            />
                            <CreateFrom
                                canCreateFund={canCreateFund}
                                wrappedComponentRef={saveFormRef}
                                handleCreate={mode === "Edit" ? handleUpdate : handleCreate}
                                search={search}
                                location={location_}
                                handleSelectSuggest={handleSelectSuggest}
                                handleInputChange={handleInputChange}
                                list_needs={list_needs}
                                toggleModalView={toggleModalView}
                                setItemToEdit={setItemToEdit}
                                handleSelect={handleSelect}
                                current_selected={current_selected}
                                tagArray={tagArray}
                                removeIDFromArray={removeIDFromArray}
                                deleteItem={deleteSelectedItem}
                                handleUpdate={handleUpdate}
                                add_button_loader={add_button_loader}
                                mode={mode}
                                disable_functions={disable_functions}
                                default_type={default_type}
                                changeDefaultType={changeDefaultType}
                                link_generated={link_generated}
                                disable_add_button={disable_add_button}
                                hasAStripeAccount={hasAStripeAccount}
                                handleBankAndStripe={handleBankAndStripe}
                                bankAndStripeHandled={bankAndStripeHandled}
                                displaybankView={displaybankView}
                            />
                            {disable_functions ? '' : default_type === 'general' ?
                                <GroupModal
                                    fetchSelectedIDS={fetchSelectedIDS}
                                    family_modal={fam_modal}
                                    toggleGroupModal={toggleModal}
                                    _groupList={groupList}
                                    setTagArrayFromList={setTagArrayFromList}
                                /> :
                                <GroupList
                                    fetchSelectedIDS={fetchSelectedIDS}
                                    family_modal={fam_modal}
                                    toggleGroupModal={toggleModal}
                                    _groupList={groupList}
                                    setTagArrayFromList={setTagArrayFromList}
                                />}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};
export default RequestForHelp;
