import axios from '../../../services/apiCall'

const CreateNeeds = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/needs/create`,data)
}

const GetRequestDetails = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/needs/post_need_detail`,data)
}

const AddNewItem = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/needs/create_items`,data)
}

const UpdateCurrentItem = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/needs/update_items`,data)
}
const deleteSelected = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/needs/delete_items`,data)
}
const updateNeed = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/needs/update`,data)
}
const generateLink = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/payment/stripe_oauth_link_generation`,data)
}
const chekActiveORNot = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/payment/stripeGetaccountById`,data)
}
const memberOfflinePayment = (data)=>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/groupMapUpdate`,data)
}

export {
    CreateNeeds,
    GetRequestDetails,
    UpdateCurrentItem,
    AddNewItem,
    deleteSelected,
    updateNeed,
    generateLink,
    chekActiveORNot,
    memberOfflinePayment
}