

import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input } from 'antd';
import { } from '../api'
import { useSelector } from 'react-redux'
const { TextArea } = Input;

const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    class extends React.Component {
        render() {
            let { visible, form, loading, onCancel, onOk, mode, default_type } = this.props;
            const { getFieldDecorator } = form;
            const validateNumber = (rule, value, callback) => {
                console.log(value,typeof(value))
                if (parseFloat(value) < 0) {
                    return callback('Please provide a value which is not zero')
                }
                else if(!Number.isInteger(parseFloat(value))){
                    return callback('Please provide a whole number value which is not zero')
                }
                callback()
            }
            return (
                <Modal
                    visible={visible}
                    title={`${mode} Request`}
                    onCancel={onCancel}
                    destroyOnClose={true}
                    onOk={onOk}
                    className="f-create-form-body"
                    footer={[
                        <Button key="back" onClick={() => onCancel()}>
                            Cancel
                        </Button>,
                        <Button key="submit" type="primary" loading={loading} onClick={() => onOk()}>
                            Add
                        </Button>,
                    ]}
                >
                    <Form layout="vertical">
                        <Form.Item label="Request Title">
                            {getFieldDecorator('request_item_title', {
                                rules: [{ required: true, message: 'Request title needed' }],
                            })(<Input
                                autoComplete="off"
                                placeholder="Ex: Hand sanitizers, nursing care, personal counselling etc" />)}
                        </Form.Item>

                        <Form.Item label="Description">
                            {getFieldDecorator('request_item_description')(<TextArea
                                rows={4}
                                autoComplete="off"
                                placeholder="Place a concise description that helps portray overview of your request."
                            />)}
                        </Form.Item>

                        <Form.Item label={`${default_type === 'general' ? "Quantity" : 'Amount'}`}>
                            {getFieldDecorator('item_quantity', {
                                rules: [{ required: true, message: `provide ${default_type === 'general' ? "quantity" : 'amount'}` }, { validator: validateNumber }],
                            })(<Input
                                placeholder={`Provide ${default_type === 'general' ? "Quantity" : 'Amount'}`}
                                min={1}
                                autoComplete="off"
                                type="number"
                            />)}
                        </Form.Item>
                    </Form>
                </Modal>
            );
        }
    },
);

const AddRequest = ({ modal_visible, toggleModalView, addToList, item_to_edit, mode_from_parent, need_request_id, updateItem, addItem,
    updateItemFromCurrentList, default_type }) => {
    let formReff
    const [state, setState] = useState({
        user_id: useSelector(stateX => stateX.user_details.userId),
        loading: false,
        mode: 'Add',
        item_id: undefined
    })
    let { loading, mode, item_id, user_id } = state;

    const handleCreate = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            let { request_item_title, request_item_description, item_quantity } = values
            switch (mode_from_parent) {
                case 'Add':
                    if (mode == 'Add') {
                        addToList(values)
                    }
                    else if (mode == 'Edit') {
                        console.log(".....edit....")
                        updateItemFromCurrentList({ ...values, item_id: item_id })
                    }

                    break;
                case 'Edit':
                    if (mode === 'Add') {
                        let params_ = {
                            user_id: `${user_id}`,
                            request_item_title: request_item_title,
                            request_item_description: request_item_description,
                            item_quantity: `${item_quantity}`,
                            post_request_id: `${need_request_id}`
                        }
                        addItem(params_).then(res => {
                            addToList(res.data.data)
                        }).catch(er_r => console.log("err", er_r))
                    }
                    else if (mode === 'Edit') {
                        let params = {
                            id: `${item_id}`,
                            user_id: `${user_id}`,
                            request_item_title: request_item_title,
                            request_item_description: request_item_description,
                            item_quantity: `${item_quantity}`,
                            post_request_id: `${need_request_id}`
                        }
                        updateItem(params).then(res => {
                            updateItemFromCurrentList(res.data.data)
                        }).catch(err_item => {
                            console.log("err", err_item)
                        })
                    }
                    break;
            }
            // addToList(values)
            setState(prev => ({ ...prev, mode: 'Add' }));
            toggleModalView();
        });
    };

    const handleCancel = () => {
        const { form } = formReff.props;
        form.resetFields()
        setState(prev => ({ ...prev, mode: 'Add' }))
        toggleModalView()
    }

    const saveFormRef = formRef => {
        formReff = formRef;
    };

    useEffect(() => {
        if (item_to_edit) {
            const { form } = formReff.props;
            setState(prev => ({ ...prev, mode: 'Edit', item_id: item_to_edit.item_id || item_to_edit.id }))
            form.setFields({
                request_item_title: { value: item_to_edit.request_item_title },
                request_item_description: { value: item_to_edit.request_item_description },
                item_quantity: { value: item_to_edit.item_quantity }
            })
        }
    }, [item_to_edit])
    return (
        <div>
            <CollectionCreateForm
                wrappedComponentRef={saveFormRef}
                visible={modal_visible}
                loading={loading}
                toggleModalView={toggleModalView}
                onOk={handleCreate}
                onCancel={handleCancel}
                mode={mode}
                default_type={default_type}
            />
        </div>
    );
}

export default AddRequest;        