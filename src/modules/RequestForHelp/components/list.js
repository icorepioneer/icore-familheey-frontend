import React from 'react'
import { Button } from 'antd'
const ListReq = ({ list,toggleModalView,setItemToEdit,deleteItem,default_type }) => {
    {/* Requests Listing */ }
    return (
        <div className="f-listing-items">
            <Button
            style={{marginTop:'30px'}} 
            className="f-submit f-btn-green f-btn-line"
            onClick={() => toggleModalView()}
            >Add Request</Button>
            <div className="f-listing-cards">
                {list.length > 0 && list.map((item, i) =>
                    <div key={i} className="f-request-card">
                        <div className="f-start">
                            <div className="f-left">
                                <strong>{`${default_type === 'general'?item.item_quantity:'$'+item.item_quantity}`}</strong>
                                <em>{`${default_type === 'general'?'Quantity':'Amount'}`}</em>
                            </div>
                            <div className="f-right">
                                <h4>{item.request_item_title}</h4>
                                <h5>{item.request_item_description}</h5>
                                <div className="f-right-combo">
                                    <Button onClick={()=>{setItemToEdit(item)}}>Edit</Button>
                                    <Button onClick={()=>{deleteItem(item,i)}} className="f-line">Delete</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    )
}

export default ListReq;