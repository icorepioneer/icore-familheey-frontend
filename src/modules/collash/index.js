//import PropTypes from 'prop-types';
import React, { useEffect, useState, useRef } from 'react';
import Gallery from 'react-grid-gallery';
//import { eSearch } from '../../shared/elasticsearch';
import { useSelector } from 'react-redux';
import { eSearchApi } from './api-collash';
import { encryption } from '../../shared/encryptDecrypt';
import "./style.scss";
import { Spin } from 'antd';

const Collash = ({ history , callbackFromParent }) => { 
    const [state, setState] = useState({
        user_id: useSelector(stateX => stateX.user_details.userId),
        images: [],
        selectAllChecked: false,
        dataArr :[],
        offset: 0,
        limit: 100,
        postAttachments:[],
        loader:true,
        collashIds:[]
    });
    const USER = useSelector(stateA => stateA.user_details);

    let { images, user_id, dataArr, offset, limit, postAttachments, loader, collashIds } = state;

    useEffect(() => {
        elastic_search();
    }, []);

    /**
     * elastic serch for collash 
     * direct elastic search
     */

    /*const elastic_search = (search_text="") => {
        let params = {
            index: 'post',
            userid: user_id,
            offset: 0,
            limit: 50,
            _body: {
                sort: [ //sort the data
                    { sort_date: { "order": "asc" } }
                ],
                query: {
                    bool: {
                        must: [{
                            match: { is_active: true }
                        },{
                            match: { type: 'post' }
                        },{
                            match:{ post_type: 'public'}
                        }]
                    }
                }
            }
        }
        let { _body } = params;
        if( search_text && search_text!=''){ //search posts
            _body = { ..._body,
                multi_match : {
                    query:search_text,
                    fields: [ "description"] 
                }
            }
        }
        params = { ...params, _body:_body }

        eSearch(params).then((doc) => {
            let postAttachments = [];
            let { hits } = doc;
            hits.forEach(elem => {
                let { _source } = elem;
                let { post_attachment } = _source; console.log("=== _source = = =", _source.sort_date)
                post_attachment.length>0 && postAttachments.push({
                    src: `${process.env.REACT_APP_S3_BASE_URL}post/${post_attachment[0].filename}`,
                    thumbnail: `${process.env.REACT_APP_S3_BASE_URL}post/${post_attachment[0].filename}`,
                    enableLightbox:true
                })
            });
            setState((prev)=>({
                ...prev,
                images:postAttachments
            }))
        }).catch((err) => {
            console.log(err)
        })
    }*/


    /**
     * elastci search to backend call
    */
    const elastic_search = () => {
        setState((prev) => ({ ...prev, loader: true }));
        let params = { index:"post", user_id:USER.userId, offset: offset,limit: limit }
        eSearchApi(params)
        .then((doc)=>{
            let { data } = doc; 
            let { hits } = data;
            hits.forEach(elem => {
                let indexis = collashIds.indexOf(elem._id)
                if (indexis == -1) {
                    collashIds.push(elem._id)
                    let { _source } = elem;
                    dataArr.push(_source);
                    let { post_attachment } = _source;
                    if(post_attachment.length > 0){
                        let { type:_type } = post_attachment[0];
                        let isVideo = _type.includes("video");
                        let _src =  isVideo ? 
                        `${process.env.REACT_APP_S3_BASE_URL}${post_attachment[0].video_thumb}`:
                        `${process.env.REACT_APP_S3_BASE_URL}post/${post_attachment[0].filename}`
                        postAttachments.push({
                            src: _src,
                            thumbnail: _src,
                            enableLightbox: true
                        });
                    }
                } else {
                    console.log("- else  =", indexis, elem._id)
                    return;
                }
            });
            images = [...postAttachments];
            let _offset = offset + hits.length;
            setState((prev) => ({
                ...prev,
                images: images,
                offset: _offset,
                loader: false,
                collashIds:collashIds
            }));
            callbackFromParent({ loader:false })
        })
        .catch((err)=>{
            console.log(err)
        });
    }

    /**
     * select the image on click
     */
    const on_SelectImage = (index, image) => { 
        let { id } = dataArr[index];
        var postId = encryption(id);
        history.push(`/post_details/` + postId);
    }

    const [element, setElement] = useState(null);
    const Loader = useRef(elastic_search)
    const observer = useRef(new IntersectionObserver((entries) => {
        let last = entries[0];
        last.isIntersecting && Loader.current();
    }, { threshold: 1 }));
    
    useEffect(() => {
        const currentElement = element;
        const currentObserver = observer.current;
        if (currentElement) {
            currentObserver.observe(currentElement)
        }
        return () => {
            if (currentElement) currentObserver.unobserve(currentElement);
        }
    }, [element]);

    useEffect(() => {
        Loader.current = elastic_search;
    }, [elastic_search]);

    return (
        <div className="main-div">
            { loader && 
                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                </Spin>
            }
            { !loader &&  <div>
                <Gallery images={images} onClickThumbnail={on_SelectImage} showLightboxThumbnails={true} />  
            </div> }
            <div className="loader-div" id="loaderDiv" ref={setElement}></div>
        </div>
    );
}
export default Collash;