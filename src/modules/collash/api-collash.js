import axios from '../../services/apiCall';

const eSearchApi = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/elastic/getRecords`, data)
}

export {
  eSearchApi
} 