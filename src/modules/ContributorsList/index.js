import React, { useState, useEffect } from "react";
import "./style.scss";
import { Input, Button, Spin, Icon, Modal } from "antd";
import {
  contributorList,
  thankYouImages,
  publishPost,
  getPostByUsers,
  getContDetails,
  SkipThankU,
  splitAmount
} from "../RequestDetails/req_service";
import { notifications } from "../../shared/toasterMessage";
import moment from "moment";
import { encryption } from "../../shared/encryptDecrypt";
import CommonSideBar from "../../components/commonSideBar";
import TextArea from "antd/lib/input/TextArea";
import { useSelector } from "react-redux";
import StripeComp from "../../components/Stripe/stripe";
import { getClientSecret } from "../../components/Stripe/stripe-api";
import ThankYOU from "../../components/ThankYouModal";
import UserCard from "../../components/ContributorsList/userCard";
const { Search } = Input;

const ContributorsList = ({ history, location }) => {
  const [state, setState] = useState({
    loader: true,
    user_id: useSelector((stateX) => stateX.user_details.userId),
    item_id: undefined,
    post_request_id: undefined,
    contributors: undefined,
    search_query: undefined,
    request_item_title: "",
    item_quantity: 0,
    total_contribution: 0,
    supporters: 0,
    visible: false,
    thankUimages: [],
    default_thankuMessage:
      " Each Contribution is hugely valued. Thank you all!",
    default_Image: {
      filename: "post_default_image1.png",
      height: 372,
      width: 465,
      type: "image/png",
    },
    reqDetails: {},
    thankYouItem: {},
    itemDetail: {},
    contributeDetails: [],
    defaultView: undefined,
    group_id: undefined,
    stripeVisible: false,
    contrPayLaterObj: undefined,
    thankyou: false,
    current_item: undefined,
    buttonLabel: "Pay Now",
    admin_status: "",
    group_id_passed: undefined,
    from_family: undefined,
    pledged_amount: 0,
    received_amount: 0,
    typOfReq_: undefined
  });

  // Mark Complete Modal
  const showModal = (item) => {
    setState((prev) => ({ ...prev, visible: true, thankYouItem: item }));
  };
  const handleCancel = (e) => {
    setState((prev) => ({ ...prev, visible: false }));
  };

  let {
    loader,
    user_id,
    item_id,
    contributors,
    search_query,
    post_request_id,
    request_item_title,
    item_quantity,
    total_contribution,
    supporters,
    visible,
    thankUimages,
    default_thankuMessage,
    default_Image,
    reqDetails,
    thankYouItem,
    itemDetail,
    contributeDetails,
    defaultView,
    group_id,
    stripeVisible,
    contrPayLaterObj,
    thankyou,
    current_item,
    // buttonLabel,
    admin_status,
    group_id_passed,
    from_family,
    pledged_amount,
    received_amount,
    typOfReq_
  } = state;

  useEffect(() => {
    item_id && getContributorList();
    item_id && split_amount();
  }, [item_id]);
  /**
   * get the contributors list
   */
  const getContributorList = () => {
    setState((prev) => ({ ...prev, loader: true }));
    let params = {
      user_id: `${user_id}`,
      post_request_item_id: `${item_id}`,
      query: search_query ? search_query : "",
    };
    contributorList(params)
      .then((response) => {
        let { data } = response.data;
        setState((prev) => ({
          ...prev,
          loader: false,
          contributors: data,
          defaultView: 1,
        }));
      })
      .catch((err) => {
        notifications("error", "Error", "Something went wrong.");
      });
  };

  useEffect(() => {
    let { state: st } = location;
    if (st) {
      let {
        item_id: it_id,
        post_request_id: prid,
        reqDetails: rd,
        admin_status: ad_s,
        group_id_passed: gid_ps,
        from_family: frm_f,
      } = st;
      let { supporters: spt, items, thank_post_id, to_groups, request_type: typOfReq } = rd;
      let itemDetails = items.find((v) => v.item_id == it_id);
      let {
        request_item_title: rit,
        item_quantity: it_q,
        total_contribution: tc,
      } = itemDetails;
      setState((prev) => ({
        ...prev,
        post_request_id: prid,
        item_id: it_id,
        supporters: spt,
        request_item_title: rit,
        item_quantity: it_q,
        total_contribution: tc,
        reqDetails: rd,
        itemDetail: itemDetails,
        group_id: to_groups[0].id,
        admin_status: ad_s,
        group_id_passed: gid_ps,
        from_family: frm_f,
        typOfReq_: typOfReq
      }));
      if (thank_post_id) getThankYouPost(thank_post_id);
    }
  }, [location]);
  /**
   * search text set to variable
   */
  useEffect(() => {
    search_query && getContributorList();
  }, [search_query]);
  /**
   * redirect to contributor profile
   */
  const navigateToUser = (uid) => {
    var userId = encryption(uid);
    history.push(`/user-details/${userId}`, { prev: window.location.pathname });
  };

  const goBack = () => {
    history.push({
      pathname: "/request-details",
      state: {
        post_request_id: post_request_id,
        admin_status: admin_status,
        group_id_passed: group_id_passed,
        from_family: from_family,
      },
    });
  };

  useEffect(() => {
    let timer_input;
    let discover = document.getElementById("discover_");
    discover.addEventListener("keyup", () => {
      clearTimeout(timer_input);
      timer_input = setTimeout(() => {
        setState((prev) => ({
          ...prev,
          search_query: discover.value.length > 0 ? discover.value : " ",
        }));
      }, 1300);
    });
    thankUimageFn();
  }, []);

  /**
   * pledged and received amount
   */
  const split_amount = () => {
    let split_params = { item_id: `${item_id}` }
    splitAmount(split_params).then((response) => {
      let { data } = response;
      setState((prev) => ({ ...prev, ...data[0] }));
    })
      .catch((err) => {
        console.log("== err in split amount api == ", err)
      });
  }

  const thankUimageFn = () => {
    let params = { post_type: "request" };
    thankYouImages(params)
      .then((doc) => {
        let { data } = doc;
        let Imgz = data.sort((a, b) => Number(a.id) - Number(b.id));
        setState((prev) => ({ ...prev, thankUimages: Imgz }));
      })
      .catch((err) => {
        notifications("error", "error", "something went wrong.");
      });
  };

  const changeDefaultImg = (item) => {
    default_Image = item;
    setState((prev) => ({ ...prev, default_Image: default_Image }));
  };

  /**
   * post thank you to contributor
   */
  const postThankyou = () => {
    default_Image = { ...default_Image, is_play: false };
    let new_groups = [];
    let { contribute_user_id, full_name } = thankYouItem;
    let { post_request_id: pri } = reqDetails;
    let { item_id: _it_id, request_item_title: _rit } = itemDetail;
    let groups = reqDetails.to_groups.map((v) => v.id);
    groups.forEach((elem) => {
      new_groups.push({ id: `${elem}`, post_create: "6" });
    });
    let postdata = {
      category_id: "3",
      conversation_enabled: true,
      created_by: user_id,
      is_shareable: false,
      post_attachment: [default_Image],
      post_info: {},
      post_type: "only_groups",
      privacy_type: "public",
      publish_id: `${pri}`,
      publish_mention_items: [
        { item_id: _it_id, item_name: _rit },
      ],
      publish_mention_users: [
        { user_id: contribute_user_id, user_name: full_name },
      ],
      publish_type: "request",
      selected_groups: new_groups,
      snap_description: default_thankuMessage,
      type: "post",
    };

    publishPost(postdata)
      .then((doc) => {
        thankYouItem = { ...thankYouItem, is_thank_post: true };
        contributors = contributors.filter((v) => v.id != thankYouItem.id);
        contributors.push(thankYouItem);
        getContributorList()
        setState((prev) => ({ ...prev, contributors: contributors }));
        handleCancel();
        notifications("success", "success", "Thank you message sent.");
      })
      .catch((err) => {
        notifications("error", "error", "Something went wrong");
      });
  };

  /**
   * skip the contribution say thanks
   */
  const SkipThankyou = () => {
    let { contribute_user_id, post_request_item_id } = thankYouItem;
    let params = {
      request_item_id: `${post_request_item_id}`,
      contribute_user_id: `${contribute_user_id}`,
      skip_thank_post: true,
    };
    SkipThankU(params)
      .then((doc) => {
        getContributorList()
        notifications("success", "Success", "Skipped thank you.");
        handleCancel();
      })
      .catch((err) => {
        notifications("error", "error", "Something went wrong");
      });
  };
  const ackButtonClicked = (item) => {
    let { contribute_user_id, post_request_item_id, id } = item;
    let params = {
      request_item_id: `${post_request_item_id}`,
      contribute_user_id: `${contribute_user_id}`,
      skip_thank_post: `${true}`,
      contribution_id: `${id}`
    };
    SkipThankU(params)
      .then((doc) => {
        notifications("success", "Success", "Acknowledged...");
        let without = contributeDetails.filter(itemCd => itemCd.id !== id)
        let with_ = contributeDetails.find(item_cd => item_cd.id == id)
        with_.is_acknowledge = true
        let newContr = [...without, with_]
        setState(prev => ({ ...prev, contributeDetails: newContr }))
      })
      .catch((err) => {
        notifications("error", "error", "Something went wrong");
      });
  };

  const handleonChange = (e) => {
    let val = e.target.value;
    setState((prev) => ({ ...prev, default_thankuMessage: val }));
  };

  const getThankYouPost = (postId) => {
    getPostByUsers({ user_id: user_id, post_id: postId, type: "post" }).then(
      (posts) => {
        let { data } = posts;
        let { snap_description, post_attachment } =
          data.data.length > 0 && data.data[0];
        default_thankuMessage = snap_description;
        default_Image = post_attachment && post_attachment[0];
        setState((prev) => ({
          ...prev,
          default_thankuMessage: default_thankuMessage,
          default_Image: default_Image,
        }));
      }
    );
  };

  /**
   * get the contributions details by a user on item
   */
  const getcontDetails = (item) => {
    setState((prev) => ({ ...prev, loader: true, current_item: item }));
    let { post_request_item_id, contribute_user_id, is_anonymous, paid_user_name, full_name } = item;
    let params = {
      user_name: `${contribute_user_id === 2 ? paid_user_name : full_name}`,
      request_item_id: `${post_request_item_id}`,
      contribute_use_id: `${contribute_user_id}`,
      is_anonymous: `${is_anonymous}`,
    };
    getContDetails(params)
      .then((doc) => {
        let { data } = doc;
        setState((prev) => ({
          ...prev,
          loader: false,
          defaultView: 2,
          contributeDetails: data,
        }));
      })
      .catch((err) => {
        setState((prev) => ({ ...prev, loader: false }));
        notifications("error", "error", "Something went wrong.");
      });
  };
  const determineColorBorder = (item) => {
    let { is_acknowledge } = item;
    return is_acknowledge ? "#62FA2E" : "#F8ED11";
  };

  const getStripeInstance = (item) => {
    let {
      contribute_item_quantity,
      request_id,
      post_request_id: post_rid,
      post_request_item_id,
      contribute_user_id,
      id,
    } = item;
    let params = {
      amount: contribute_item_quantity * 100,
      group_id: `${group_id}`,
      to_subtype: "item",
      to_subtype_id: `${post_request_item_id}`,
      to_type: "request",
      to_type_id: `${request_id ? request_id : post_rid}`,
      user_id: `${contribute_user_id}`,
      contributionId: `${id}`,
    };
    getClientSecret(params).then((res) => {
      setState((prev) => ({
        ...prev,
        stripeVisible: true,
        contrPayLaterObj: {
          ...res.data,
          contribute_item_quantity,
          item_id,
          contributionId: id,
          current_item,
        },
      }));
    });
  };
  const toggleStripeModal = () => {
    setState((prev) => ({ ...prev, stripeVisible: false }));
  };

  const ty_handleCancel = () => {
    setState((prev) => ({
      ...prev,
      thankyou: false,
    }));
  };
  const ty_handleOk = () => {
    setState((prev) => ({
      ...prev,
      thankyou: false,
    }));
  };
  // const callNow = () => {
  //   let { phone } = reqDetails;
  //   setState((prev) => ({
  //     ...prev,
  //     thankyou: false,
  //   }));
  //   window.location.href = `tel://${phone}`;
  // };
  const showThanYou = () => {
    setState((prev) => ({ ...prev, thankyou: true }));
  };
  return (
    <section className="ContributorsList f-page">
      {loader && (
        <div className="spin-loader">
          <Spin></Spin>
        </div>
      )}
      <div className="f-clearfix f-body">
        <div className="f-boxed">
          <div className="f-post-listing">
            <CommonSideBar history={history} />

            <div className="f-clearfix f-request-details-wrap">
              <div className="f-req-details-header f-top">
                <div className="f-left">
                  <Button
                    className="f-create-form-back f-gobck"
                    onClick={() => goBack()}
                  />
                  <h4>{request_item_title}</h4>
                </div>
                {
                  <Search
                    style={{ display: "none" }}
                    id="discover_"
                    placeholder="Search"
                  // value={search_query}
                  // onChange={(e) => handleonChange(e)}
                  />
                }
              </div>
              <div className="f-req-details-header f-bot">
                <div className="f-left">
                  <h5>
                    <em>{supporters}</em> Contributors
                  </h5>
                </div>

                {supporters > 0 && (
                  <div className="f-right">
                    {item_quantity - total_contribution > 0 &&
                    <strong>
                      <em>
                        {item_quantity - total_contribution > 0
                          ? item_quantity - total_contribution
                          : 0}{" "}
                      </em>{" "}
                      of {item_quantity}
                    </strong>
                    }
                    <p>
                      {item_quantity - total_contribution > 0
                        ? "Still needed"
                        : " Completed"} <br/>
                        Received: {received_amount ? received_amount : 0} <br />
                        Pledged:{pledged_amount ? pledged_amount : 0}
                    </p>
                  </div>
                )}
              </div>

              {defaultView == 1 && (
                <div className="f-contrib-list">
                  {supporters > 0 &&
                    contributors &&
                    contributors.map(
                      (item, index) => (
                        <UserCard
                          key={index}
                          item={item}
                          determineColorBorder={determineColorBorder}
                          admin_status={admin_status}
                          navigateToUser={navigateToUser}
                          user_id={user_id}
                          getStripeInstance={getStripeInstance}
                          showModal={showModal}
                          index={index}
                          getcontDetails={getcontDetails}
                          SkipThankU={SkipThankU}
                          handleCancel={handleCancel}
                          getContributorList={getContributorList}
                          reqDetails={reqDetails}
                        />
                      )
                    )}
                </div>
              )}

              {defaultView == 2 && (
                <div className="f-contrib-list">
                  {contributeDetails.map((item, index) => {
                    item.full_name =
                      item.contribute_user_id == 2
                        ? item.paid_user_name
                        : item.full_name;
                    return (
                      <div key={index} className="f-req-author-card">
                        <span
                          className="f-req-badge"
                          style={{ background: determineColorBorder(item) }}
                        />
                        <div
                          className="f-left"
                          onClick={() =>
                            !item.is_anonymous &&
                            navigateToUser(item.contribute_user_id)
                          }
                        >
                          <img
                            onError={(e) => {
                              e.target.onerror = null;
                              e.target.src = "../../images/profile-pic.png";
                            }}
                            src={
                              item.is_anonymous
                                ? require("../../assets/images/profile-pic.png")
                                : process.env.REACT_APP_PRO_PIC_CROP +
                                process.env.REACT_APP_S3_BASE_URL +
                                "propic/" +
                                item.propic
                            }
                          />
                        </div>
                        <div
                          className="f-mid"
                          onClick={() =>
                            !item.is_anonymous &&
                            navigateToUser(item.contribute_user_id)
                          }
                        >
                          <h2>
                            {item.is_anonymous
                              ? admin_status == "admin" ||
                                item.contribute_user_id == user_id
                                ? `${item.full_name} (Anonymous)`
                                : `Anonymous`
                              : item.full_name}
                          </h2>
                          <h3>
                            Contributed on:{" "}
                            {moment(item.created_at).format(
                              "MMM DD YYYY h:mm a"
                            )}
                          </h3>
                          {item.requested_by == user_id && (
                            <span className="f-phone">
                              <em>
                                <Icon type="phone" /> {item.phone}
                              </em>
                            </span>
                          )}
                          <span>
                            {/* <em>{item.location}</em> */}
                            <em>
                              {item.is_anonymous
                                ? admin_status == "admin" ||
                                  item.contribute_user_id == user_id
                                  ? `${item.location === null
                                    ? ""
                                    : item.location
                                  }`
                                  : `------------`
                                : item.location === null
                                  ? ""
                                  : item.location}
                            </em>
                          </span>
                        </div>
                        <div className="f-right">
                          <h4>
                            {item.contribute_item_quantity}
                            <br />
                            <em>Total</em>
                          </h4>
                          {
                            item.contribute_user_id == user_id &&
                            !item.is_acknowledge && typOfReq_ === 'fund' && (
                              <span>
                                <div className="f-right">
                                  <Button
                                    style={{
                                      backgroundColor: "var(--green)",
                                      border: "1px solid var(--green)",
                                      height: "32px",
                                      color: "#FFF",
                                      marginRight: "5px",
                                    }}
                                    onClick={() => {
                                      getStripeInstance(item);
                                    }}
                                  >
                                    Pay Now
                                    </Button>
                                </div>
                              </span>
                            )
                            // <StripeComp/>
                          }
                          {(item.requested_by == user_id || admin_status == "admin") && !item.is_acknowledge && (
                            <span className="f-mark-received" onClick={() => ackButtonClicked(item)}>
                              <input
                                type="checkbox"
                                style={{ display: "none" }}
                              />
                              <label>
                                <Icon type="check-circle" />
                                Acknowledge
                              </label>
                            </span>
                          )}
                        </div>
                      </div>
                    );
                  })}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      {/* Mark as complete Modal */}
      <Modal
        title="Say Thanks!"
        visible={visible}
        className="f-modal f-modal-wizard"
        onCancel={() => handleCancel()}
        footer={[]}
      >
        <div className="f-modal-wiz-body">
          <div className="f-form-control">
            <label>Choose Image</label>
            <div
            // style={{display:'flex',alignContent:'stretch'}}
            >
              <ul className="f-thanks-images">
                {thankUimages.length > 0 &&
                  thankUimages.map((item, index) => (
                    <li key={index}>
                      <input
                        onChange={() => changeDefaultImg(item)}
                        checked={
                          default_Image &&
                          default_Image.filename == item.filename
                        }
                        id={`thanks-image-${index}`}
                        type="radio"
                        name="sel_req_ty_image"
                        style={{ display: "none" }}
                      />
                      <label for={`thanks-image-${index}`}>
                        <img
                          style={{ width: "160px", height: "100px" }}
                          src={`${process.env.REACT_APP_S3_BASE_URL}post/${item.filename}`}
                        />
                      </label>
                    </li>
                  ))}
              </ul>
            </div>
          </div>
          <div className="f-form-control">
            <label>Say something</label>
            <TextArea
              onChange={(e) => handleonChange(e)}
              placeholder="Message"
              rows="3"
              value={default_thankuMessage}
            />
          </div>
        </div>
        <div className="f-modal-wiz-footer" key="1">
          <Button key="thankyou" onClick={() => postThankyou()}>
            Say Thanks! <Icon type="arrow-right" />
          </Button>
          <Button key="skip" onClick={() => SkipThankyou()}>
            Skip <Icon type="undo" />
          </Button>
        </div>
      </Modal>
      {
        <StripeComp
          modalVisible={stripeVisible}
          toggleStripeModal={toggleStripeModal}
          post_request_id={item_id}
          reqDetails={undefined}
          current_selected_item_id={null}
          handleChange={() => { }}
          showThanYou={showThanYou}
          contrPayLaterObj={contrPayLaterObj}
          callback={current_item ? getcontDetails : getContributorList}
        />
      }
      <ThankYOU
        thankyou={thankyou}
        ty_handleCancel={ty_handleCancel}
        ty_handleOk={ty_handleOk}
      />
    </section>
  );
};

export default ContributorsList;
