import React, { Fragment, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Calendar, Badge, Tag, Button, Dropdown, Icon, Menu,  Input, Radio, Avatar } from 'antd';
import styled from 'styled-components';
// import PopupModal from './Components/Model';
import { getCalenderDetails, getAllFamily, getEventsByFamily } from './API/Calender-api'
import moment from 'moment';
import FamModal from './Components/FamModal'
import lowerCase from 'lower-case'
import { encryption } from '../../shared/encryptDecrypt';
import {useSelector} from 'react-redux'

const { Search } = Input;
const CalendarContainer = styled.div`
    margin:0px auto;
    margin-top:100px;
    width:50%;
    height:calc(100vh-100px);
`

const Calender = ({ history, passed_id }) => {
  const [calendarState, setCalendarState] = useState({
    visible: false,
    dateDetails: '',
    _dateDetails: '',
    events: [],
    listData: [],
    Month: '',
    Year: '',
    dataStatus: 'All',
    _famModal_visible: false,
    family_list: '',
    _group_name: undefined,
    current_date: moment(new Date()),
    view_type: 1,
    filteredEvent: [],
    user_id:useSelector(state => state.user_details.userId)
  })
  let { Month, Year,user_id, visible, dateDetails, events, dataStatus, _group_name, _famModal_visible, family_list, current_date, filteredEvent } = calendarState;
  // const handleOk = () => {
  //   setCalendarState(calendarState => ({
  //     ...calendarState,
  //     visible: !visible
  //   }))
  // }
  // const handleCancel = () => {
  //   setCalendarState(calendarState => ({
  //     ...calendarState,
  //     visible: !visible
  //   }))
  // }
  const getListData = (value) => {
    let listData = [];
    dateDetails.data && dateDetails.data.map((item) => {
      if (moment.unix(item.from_date).format("YYYY") === value.format("YYYY")) {
        let _from_date = moment.unix(item.from_date).format("YYYY-MM-DD");
        let _date = value.format("YYYY-MM-DD");
        let _end_date = moment.unix(item.to_date).format("YYYY-MM-DD")
        if (moment.unix(item.from_date).format("YYYY") === value.format("YYYY")) {
          if (moment.unix(item.from_date).format("DD MMMM") === value.format("DD MMMM")
            ||
            moment(new Date(`${_date}`)).isSameOrBefore(new Date(`${_end_date}`))
            &&
            moment(new Date(`${_date}`)).isSameOrAfter(new Date(`${_from_date}`))) {
            listData.push({
              type: item.event_type === 'Regular' ? 'warning' : 'success', content: item.event_name, tag: item.event_type, date: moment.unix(item.from_date).format("DD")
            })
          }
        }
      }
    })
    return listData || [];
  }

  const renderEvents = (date) => {
    let current = moment(date).format("YYYY/MMMM/DD")
    let Mn = current.split('/')[1];
    let Yr = current.split('/')[0];
    setCalendarState(prev => ({
      ...prev,
      Month: Mn,
      Year: Yr
    }))
    let eventsToPass = dateDetails.data && dateDetails.data.filter(item => {
      let _from_date = moment.unix(item.from_date).format("YYYY-MM-DD");
      let _date = date.format("YYYY-MM-DD");
      let _end_date = moment.unix(item.to_date).format("YYYY-MM-DD")
      if (moment.unix(item.from_date).format("YYYY") === date.format("YYYY")) {
        return moment.unix(item.from_date).format("DD MMMM") === date.format("DD MMMM")
          ||
          moment(new Date(`${_date}`)).isSameOrBefore(new Date(`${_end_date}`))
          &&
          moment(new Date(`${_date}`)).isSameOrAfter(new Date(`${_from_date}`))
      }
    })
    setCalendarState(prev => ({ ...prev, events: eventsToPass, visible: !visible }))

  }

  const dateCellRender = (value) => {
    let templistData = getListData(value);
    let type_signUp = templistData.filter(item => { return item.tag == 'signup' || item.tag == 'Sign up' });
    let type_Regular = templistData.filter(item => { return item.tag == 'regular' || item.tag == 'Regular' });
    return (
      <ul className="events">
        {templistData.length > 0 && <li>
          {type_Regular.length > 0 && <Badge status="success" /*text={`Regular : ${type_Regular.length}`}*/ />}
          {type_signUp.length > 0 && <Badge status='success' /*text={`Sign up : ${type_signUp.length}`}*/ />}
        </li>}
      </ul>
    );
  }
  const renderSelectedDateAndMonth = (e) => {
    let current = moment(e).format("YYYY/MMMM/DD")
    let _Mn = current.split('/')[1];
    let _Yr = current.split('/')[0];
    setCalendarState(prev => ({
      ...prev,
      Month: _Mn,
      Year: _Yr
    }))
  }
  const _getCalenderDetails = (data) => {
    if (!passed_id) {
      getCalenderDetails(data).then((res) => {
        // setCalendarState(calendarState => ({
        //   ...calendarState,
        //   dateDetails: res.data,
        //   _dateDetails: res.data
        // }))
        setCalendarState(prev => ({ ...prev, dateDetails: res.data,_dateDetails: res.data }));
      })
    }
  }
  const _viewAllFamily = (data) => {
    getAllFamily(data).then(res => {
      setCalendarState(prev => ({ ...prev, family_list: res.data.data }))
    })
  }
  useEffect(() => {

    let data = {
      user_id: user_id
    }
    let group_id = '';
    if((history.location && history.location.state && history.location.state.group_id)) {
      group_id = history.location.state.group_id;
    }
    !passed_id && !group_id && _getCalenderDetails(data)
    _viewAllFamily(data)
  }, [])
  useEffect(() => {
    let group_id ='';let group_name = '';
    if((history.location && history.location.state && history.location.state.group_id)) {
      group_id = history.location.state.group_id;
    }
    if(passed_id ||  group_id) {
      let data = {
        group_id: passed_id ||  group_id
      };
      if(group_id) {
        group_name = history.location.state.group_name;
        // setCalendarState(calendarState => ({...calendarState,_group_name: group_name
        // }))
        setCalendarState(prev => ({ ...prev, _group_name: group_name }))
      }
      getEventsByFamily(data).then(res => {
        let _events = res.data.data.event_share.concat(res.data.data.event_invitation)
        // setCalendarState(calendarState => ({
        //   ...calendarState,
        //   dateDetails: { data: _events },
        //   _dateDetails: { data: _events },
        //   events: _events,
        //   // _famModal_visible: !_famModal_visible
        // }))
        setCalendarState(prev => ({ ...prev,
          dateDetails: { data: _events },
          _dateDetails: { data: _events },
          events: _events, 
        }))
      })
    }
  }, [passed_id])
  useEffect(() => {
    setCalendarState(prev => ({
      ...prev,
      Month: moment(new Date()).format("YYYY/MMMM/DD").split('/')[1],
      Year: moment(new Date()).format("YYYY/MMMM/DD").split('/')[0],
    }))
  }, [])
  // useEffect(() => {
  //   let status = dataStatus == 'Public' ? true : false
  //   let data = {
  //     user_id: user_id,
  //     type: dataStatus == "All" ? -1 : status
  //   }
  //   _getCalenderDetails(data)
  // }, [dataStatus])
  const onMenuSelection = (e) => {
    setCalendarState(prev => ({
      ...prev,
      dataStatus: e.key
    }));
    let status = e.key == 'Public' ? true : false
    let data = {
      user_id: user_id,
      type: e.key == "All" ? -1 : status
    }
    _getCalenderDetails(data)
  }
  //menu List to be shown in the dropdown  
  const menu = (
    <Menu onClick={onMenuSelection}>
      <Menu.Item key="All">
        <div style={{ display: 'flex', justifyContent: 'space-between', width: '80px' }}><span>All</span></div>
      </Menu.Item>
      <Menu.Item key="Private">
        <div style={{ display: 'flex', justifyContent: 'space-between', width: '100px' }}><span>Private</span></div>
      </Menu.Item>
      <Menu.Item key="Public">
        <div style={{ display: 'flex', justifyContent: 'space-between', width: '100px' }}><span>Public</span></div>
      </Menu.Item>
    </Menu>
  );
  //used to display famModal
  const showFamMOdal = () => {
    setCalendarState(prev => ({ ...prev, _famModal_visible: !_famModal_visible }))
  }
  //
  const onOKClicked = () => {
    console.log("clicekeddd")
    setCalendarState(prev => ({ ...prev, _famModal_visible: !_famModal_visible }))
  }
  const onCancel = () => {
    setCalendarState(prev => ({ ...prev, _famModal_visible: !_famModal_visible }))
  }
  const _handleSelect = (item) => {
    console.log(item)
    let group_name = item.group_name
    let data = {
      group_id: `${item.id}`
    }
    getEventsByFamily(data).then(res => {
      let _events = res.data.data.event_share.concat(res.data.data.event_invitation)
      
      // setCalendarState(calendarState => ({
      //   ...calendarState,
      //   dateDetails: { data: _events },
      //   _dateDetails: { data: _events },
      //   events: _events,
      //   _group_name: group_name,
      //   _famModal_visible: !_famModal_visible
      // }))
      setCalendarState(prev => ({ ...prev,
        dateDetails: { data: _events },
        _dateDetails: { data: _events },
        events: _events,
        _group_name: group_name,
        _famModal_visible: !_famModal_visible
      }));
    })
  }
  const clearSelection = () => {
    setCalendarState(prev => ({
      ...prev,
      _group_name: undefined
    }))
    let data = {
      user_id: user_id
    }
    _getCalenderDetails(data)
  }

  const ChangeDateAndMonth = (item, type) => {
    switch (item) {
      case 'month':
        if (type === 'incr') {
          let date = moment(new Date(`${current_date}`)).add(1, 'month').format()
          let Mon_th = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[1]
          let Ye_ar = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[0]
          setCalendarState(prev => ({ ...prev, current_date: moment(new Date(`${date}`)), Month: Mon_th, Year: Ye_ar }))
        } else if(type === 'same'){
          let date = moment(new Date(`${current_date}`)).format()
          let Mo_nth = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[1]
          let Ye_ar = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[0]
          setCalendarState(prev => ({ ...prev, current_date: moment(new Date(`${date}`)), Month: Mo_nth, Year: Ye_ar }))

        }
        else {
          let date = moment(new Date(`${current_date}`)).subtract(1, 'month').format()
          let _Month = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[1]
          let _Year = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[0]
          setCalendarState(prev => ({ ...prev, current_date: moment(new Date(`${date}`)), Month: _Month, Year: _Year }))
        }
        break;
      case 'year':
        if (type === 'incr') {
          let date = moment(current_date).add(1, 'year').format()
          let _Month_ = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[1]
          let _Year_ = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[0]
          setCalendarState(prev => ({ ...prev, current_date: moment(new Date(`${date}`)), Month: _Month_, Year: _Year_ }))

        }
        else {
          let date = moment(current_date).subtract(1, 'year').format()
          let _Mo_nth = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[1]
          let _Ye_ar = moment(new Date(`${date}`)).format("YYYY/MMMM/DD").split('/')[0]
          setCalendarState(prev => ({ ...prev, current_date: moment(new Date(`${date}`)), Month: _Mo_nth, Year: _Ye_ar }))
        }
        break;
    }
  }

  const backCalendar = () => {
    ChangeDateAndMonth('month','same')
    setCalendarState(prev => ({
      ...prev,
      visible: !visible
    }))
  }

  const dateConverter = (datetime, type) => {
    if (type == 'date') {
      let a = new Date(datetime * 1000);
      let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
      let dayOfWeek = days[a.getDay()]
      return `${dayOfWeek} ,${moment.unix(datetime).format('MMM DD YYYY')}`
      // return moment.unix(datetime).format('DD MMM YYYY')

    } else if (type == 'time') {
      return moment.unix(datetime).format('hh:mm a ')
    } else {
      return new Date(datetime)
    }

  }

  const dateFormatTodisplay = (eventDetails) => {
    let { from_date, to_date } = eventDetails;
    let date;
    switch (true) {
        case moment.unix(from_date).isSame(moment.unix(to_date)):
            date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')}`
            break;
        case moment.unix(from_date).get('year') == moment.unix(to_date).get('year'):
            date = `${moment.unix(from_date).format('ddd,MMM DD ')} - ${moment.unix(to_date).format('ddd,MMM DD YYYY')}`
            break;
        case moment.unix(from_date).get('year') != moment.unix(to_date).get('year'):
            date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')} - ${moment.unix(to_date).format('ddd,MMM DD YYYY')}`
            break;
        default:
            date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')} - ${moment.unix(to_date).format('ddd,MMM DD YYYY')}`
            break;
    }
    return date
}

  const navigateToPrev = () => {
    history.goBack();
  }
  const navigateToEvent = (eventid) => {
    history.push(`/event-details/` + encryption(eventid));
  }

  const onSearch = (eventsList, e) => {
    let _eventList = eventsList
    let Filtered = _eventList && _eventList.filter(item => lowerCase(item.event_name).indexOf(lowerCase(e.target.value)) !== -1)
    let _data = e.target.value.length > 0 ? Filtered : eventsList
    setCalendarState(prev => ({
      ...prev,
      filteredEvent: _data
    }))

  }

  const bar = () =>
    <div className="f-calendar-header" style={{ display: 'flex', justifyContent: 'flex-end' }}>
      {/* <Search placeholder="Search" /> */}
      {/* <Button className="f-create-form-back">Back to calendar</Button> */}
      <div className="f-calendar-actions">
        <Radio.Group defaultValue={1} onChange={e => {
          setCalendarState(prev => ({ ...prev, view_type: e.target.value }))
        }}>
          {/* <Radio value={1}><img src={require('../../assets/images/icon/cal-col.svg')} /></Radio> */}
          {/* <Radio value={2}><img src={require('../../assets/images/icon/cal-list.svg')} /></Radio> */}
        </Radio.Group>
      </div>
    </div>
  return (
    <Fragment>
      <div
      // className="f-page"
      >
        <div className="f-body" style={{ margin: '100px 0 70px' }}>
          <div className="f-boxed-calendar">
            <div className="f-calendar-with-sidebar">
              {/* Sidebar */}
              <aside className="f-calendar-sidebar">
                <div className="f-sider-block">
                  <div className="f-calendar-header">
                    <Button className="f-create-form-back" onClick={() => navigateToPrev()} />
                    {/* <div className="f-calendar-nav" style={{ marginLeft: '0', marginRight: '15px' }}> */}
                    {/* <Button icon="left" onClick={() => ChangeDateAndMonth('month', 'decr')} /> */}
                    {/* <Button icon="left" onClick={() => ChangeDateAndMonth('year', 'decr')} /> */}
                    {/* </div> */}
                    <h4>{Month} {Year}</h4>
                    <div className="f-calendar-nav">
                      {/* <Button icon="right" onClick={() => ChangeDateAndMonth('year', 'incr')} /> */}
                      <Button icon="left" onClick={() => ChangeDateAndMonth('month', 'decr')} />
                      <Button icon="right" onClick={() => ChangeDateAndMonth('month', 'incr')} />
                    </div>
                  </div>
                  <div className="f-calendar-body">
                    <div className="f-calendar-filters">
                      <h3>Filter By</h3>
                      {/* <Dropdown size="large" overlay={menu}>
                        <Button>
                          {dataStatus}<Icon type="down" />
                        </Button>
                      </Dropdown> */}
                      {_group_name ?
                        <Button
                          className="f-alt"
                          // shape="box"
                          onClick={clearSelection}
                        ><Icon
                            type="close-circle" /><Tag color="#87d068">{_group_name}</Tag></Button>
                        : <Button
                          className="f-alt"
                          // shape="box"
                          icon={_group_name ? "close-circle" : "team"}
                          // icon="close-circle"
                          onClick={showFamMOdal}
                        >{_group_name ? (<Tag color="#87d068">{_group_name}</Tag>) : `Family`}</Button>}
                    </div>
                    <div className="f-calendar-filters">
                      <h4>Event Type</h4>
                      <Dropdown size="large" overlay={menu}>
                        <Button>
                          {dataStatus}<Icon type="down" />
                        </Button>
                      </Dropdown>
                      {/* <ul>
                        <li><Checkbox defaultChecked={true}>Show All</Checkbox></li>
                        <li><Checkbox 
                        onChange={(e)=>onMenuSelection(e)}
                        defaultChecked={true}
                        value={'Private'}>Private</Checkbox></li>
                        <li><Checkbox 
                        onChange={(e)=>onMenuSelection(e)}
                        value={'Public'} 
                        defaultChecked={true}
                        >Public</Checkbox></li>
                      </ul> */}
                    </div>
                    {/* <div className="f-calendar-filters">
                      <h4>Event Category</h4>
                      <ul>
                        <li><Checkbox defaultChecked={true}>Show All</Checkbox></li>
                        <li><Checkbox>Reunion</Checkbox></li>
                        <li><Checkbox>Birthday</Checkbox></li>
                        <li><Checkbox>Food</Checkbox></li>
                      </ul>
                    </div> */}

                  </div>
                </div>
              </aside>
              {/* Content */}
              <div className="f-calendar-ws-content">
                {visible &&
                  <React.Fragment>
                    <Button onClick={() => backCalendar()} className="f-create-form-back">
                      <span onClick={() => backCalendar()}> Back to calendar</span>
                    </Button>

                  </React.Fragment>
                }

                <div className="f-calendar-vs-listing-wrap"> {/* Listing start */}
                  <div className="f-calendar-header" style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    {/* Listing */}
                    {visible ? 
                    <div className="f-calendar-list-view">
                      <p className="f-calnd-list-tot"> {events && events.length} {events && events.length > 1 ? ' Events' : ' Event'} available</p>
                      <div className="f-form-control">
                        <Search placeholder="Search event" onChange={(e) => onSearch(events, e)} />
                      </div>
                      <ul className="f-calnd-event-listing">

                        {
                          filteredEvent.length > 0 ? filteredEvent.map((item, index) => {
                            return <li>
                              <Avatar src={require('../../assets/images/demo/demo-fam1.png')} />
                              <span>
                                <h5>{item.event_name}</h5>
                                <h6>{dateFormatTodisplay(item)}</h6>
                                {/* <h6>{dateConverter(item.from_date, 'date')} - {dateConverter(item.to_date, 'date')}</h6> */}
                                <h6>{dateConverter(item.from_date, 'time')} - {dateConverter(item.to_date, 'time')}</h6>
                              </span>
                            </li>
                          }) :
                            events && events.map((item, index) => {
                              return <li onClick={() => navigateToEvent(item.event_id)}>
                                {/* <Avatar src={require('../../assets/images/demo/demo-fam1.png')} /> */}
                                <Avatar src={item.event_image ? process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'event_image/' + item.event_image : require('../../assets/images/default_event_image.png')} />

                                <span>
                                  <h5>{item.event_name}</h5>
                                  <h6>{dateFormatTodisplay(item)}</h6>
                                  {/* <h6>{dateConverter(item.from_date, 'date')} - {dateConverter(item.to_date, 'date')}</h6> */}
                                  <h6>{dateConverter(item.from_date, 'time')} - {dateConverter(item.to_date, 'time')}</h6>
                                </span>
                              </li>
                            })}

                      </ul>
                    </div> :
                      <CalendarContainer className="f-clendar-design">
                        <Calendar
                          onChange={renderSelectedDateAndMonth}
                          onSelect={(current) => {
                            renderEvents(current);
                          }}
                          dateCellRender={dateCellRender}
                          headerRender={bar}
                          value={current_date}
                        />
                        {/* <PopupModal
                      visible={visible}
                      eventList={events}
                      handleOk={handleOk}
                      handleCancel={handleCancel}
                      history={history}
                    /> */}
                      </CalendarContainer>}
                  </div>
                </div> {/* Listing END */}
              </div>
            </div>
          </div>
        </div>
        <FamModal
          visible={_famModal_visible}
          handleOk={onOKClicked}
          handleCancel={onCancel}
          family_list={family_list}
          handleSelect={_handleSelect}
        />
      </div>
    </Fragment>
  )
}
export default withRouter(Calender);