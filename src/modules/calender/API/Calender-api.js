import axios from '../../../services/apiCall';

const getCalenderDetails = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/fetch_calender`,data);
}
const responseToEvents = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/respondToEvents`, data)
}
const getAllFamily = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/viewFamily`, data)
}
const getEventsByFamily = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/group_events`, data)
}
export {
    getCalenderDetails,
    responseToEvents,
    getAllFamily,
    getEventsByFamily
}