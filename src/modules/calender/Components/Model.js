import React, {useState,useEffect } from "react";
import { Modal, Card, Input } from 'antd';
import moment from 'moment'
import lowerCase from "lower-case";

let { Search } = Input;
const CryptoJS = require("crypto-js");
let { Meta } = Card;

const Model=({visible, handleOk, handleCancel,eventList,history})=> {

    const [modelState,setModelState] = useState({events:[]})
    const navigateToEventDetails = (value) => {
        var key = process.env.REACT_APP_SECRET_KEY;
        var id = value.event_id;
        var eventId = CryptoJS.AES.encrypt(id.toString(), key);
        eventId = encodeURIComponent(eventId)
        history.push(`/event-details/${eventId}`);
    }
    const filterEvents=(tag)=>{
        let eventsFiltered = eventList.filter(item=>lowerCase(item.event_name).indexOf(lowerCase(tag.target.value)) !== -1 )
        setModelState({...modelState,
            events:eventsFiltered
        })
    }
    useEffect(()=>{
        setModelState({...modelState,events:eventList})
    },[eventList])

        const {events} = modelState;
        return (<Modal
            visible={events.length>0?visible:false}
            closable={false}
            footer={false}
            onOk={handleOk}
            onCancel={handleCancel}
            maskClosable={true}
        >
            <Search
                placeholder="input search text"
                onSearch={value => filterEvents(value)}
                onChange={value => filterEvents(value)}
                style={{ width: '100%',marginBottom:'24px' }}
            />
            {events.map((data) => {
                let dateDetails = `${moment.unix(data.from_date).format("DD MMMM dddd hh:mm a")} to ${moment.unix(data.to_date).format("DD MMMM dddd hh:mm a")}`
                return <Card
                    key={data.event_id}
                    hoverable
                    style={{ width: '100%', marginBottom: '24px' }}
                    onClick={() => navigateToEventDetails(data)}
                >
                    <Meta title={data.event_name} description={dateDetails} />
                </Card>
            })}

        </Modal>)
}
export default Model;