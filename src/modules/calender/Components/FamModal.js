import React from 'react';
import { Modal, Card, Input } from 'antd';
const { Search } = Input;
const EachFAm = ({ handleSelect, family_list }) => {
    return (
        <div style={{ height: '500px' }}>
            {<div className="f-member-listing-cards f-share-scroll">
                {
                    family_list && family_list.map(item =>
                        <React.Fragment key={item.id} >
                            <Card
                                onClick={() => {
                                    handleSelect(item)
                                }}
                                hoverable={true}
                                className="f-member-cardx"
                            >
                                <div className="f-start">
                                    <div className="f-left">
                                        <img
                                            src={
                                                item && item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo :
                                                    require('../../../assets/images/demo/m2.png')} />
                                        {/* <i></i> */}
                                    </div>
                                    <div className="f-right">
                                        <h4>{item.group_name}</h4>
                                        <h6>By {item.created_by}</h6>
                                        <h6>{item.group_type}</h6>
                                        <h6>{item.base_region}</h6>
                                    </div>
                                </div>
                            </Card>
                        </React.Fragment>

                    )
                }
                { family_list &&  family_list.length == 0 && <span>No families found</span>}
            </div>}
        </div>)
}
const FamModal = ({ visible, handleOk, handleCancel, family_list,handleSelect }) => {
    return (
        <React.Fragment>
            <Modal
                width={"680px"}
                footer={null}
                title="Filter by Family"
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
                className="f-modal-filter-by-fam f-filt-fam"
            >
                <div className="f-form-control" style={{margin:'0 0 10px'}}><Search placeholder="Search family" /></div>
                <EachFAm 
                family_list={family_list}
                handleSelect={handleSelect} />
            </Modal>
        </React.Fragment>
    );
}
export default FamModal;