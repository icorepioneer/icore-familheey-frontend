import axios from '../../services/apiCall';

const create = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/viewMembers`, user)
}
const confirmOtp = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/confirmotp`, data)
}
const createFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/create_family`, data)
}
const checkforDuplicateFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/fetch_group`, data)
}

export {
  create,
  confirmOtp,
  createFamily,
  checkforDuplicateFamily
}
