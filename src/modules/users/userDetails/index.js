import React, { useState, useEffect, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Input, Card, Icon, Spin, Modal } from "antd";
import { viewFamily } from '../../family/familyList/api-familyList';
import {
    getUserDetails,
    getPostByUsers,
    getMutualConnections,
    getConnections,
    viewRequest,
    invitationResp,
    pendingRequest,
    deleteRequest,
    updateUserDetails2,
    paymentHistoryByUserid
} from '../userProfile/api-profile';
import { getAllUserAlbums } from '../../AlbumModal/album-api'
import { decryption, encryption } from '../../../shared/encryptDecrypt'
import Lightbox from 'react-image-lightbox';
import CreateAlbum from '../../AlbumModal/createAlbum';
// import DisplayContent from '../../AlbumModal/albumContents';
import UserDetailsSidebar from '../../../components/UserDetailsSidebar';
import PostsCard from '../../../components/PostsCard';
import { getGroupsToAddMember } from '../../../components/SearchResultPage/api-searchResult';
import { addMemberToGroup } from '../../../modules/memberListing/api-memberlist';
import AlbumCard from '../../../components/AlbumCard/index'
import CustomImageCrop from '../../ImageCrop/index'
import { useSelector, useDispatch } from 'react-redux';
import './style.scss'
import 'react-image-lightbox/style.css';
import RequestList from '../../RequestList';
import PaymentHistory from '../../../components/PaymentHistory'
const { TextArea } = Input;
const { Search } = Input;
// const { Meta } = Card

const UserDetails = ({ location, history }) => {
    const dispatch = useDispatch();
    const [profileStates, setProfileState] = useState({
        user_id: useSelector(state => state.user_details.userId),
        userDetails: undefined,
        propic_display: false,
        count: {},
        groups: [],
        showGroup: [],
        postList: [],
        DataFromChild: 1,
        photoIndex: 0,
        isOpen: false,
        curent: 0,
        userId: '',
        loader: true,
        albums: [],
        deleting: false,
        selectedFolder: 0,
        modelFlag: false,
        modelTitle: '',
        folder_id: '',
        showfamily: true,
        cover_pic_url: '',
        profile_id: '',
        connectionArray: [],
        familyvisible: false,
        addFamilyList: [],
        selectedUser: '',
        al_visible: false,
        item_to_edit: undefined,
        initial: '1',
        searchTag: '',
        loading_search: false,
        cover_pic_display: false,
        cropModal: false,
        params: {
            crop: {
                unit: "%",
                aspect: 261 / 100,
                width: 200
            },
            event_id: '',
            editType: '',
            user_id: '',
            by_user: 1
        },
        // searchString: '',
        introEdit: false,
        workEdit: false,
        intro: '',
        work: '',
        modalvisible: false,
        prev_location: '',
        page_loader: true,
        offset:0,
        limit:10,
        search_text:null
    });
    const {
        // folder_id,
        loader,
        userDetails,
        postList,
        DataFromChild,
        count,
        cover_pic_url,
        userId,
        // isOpen,
        // photoIndex,
        user_id,
        groups,
        albums,
        // deleting,
        // selectedFolder,
        // modelFlag,
        // modelTitle,
        showGroup,
        showfamily,
        propic_display,
        profile_id,
        connectionArray,
        familyvisible,
        addFamilyList,
        selectedUser,
        // al_visible,
        item_to_edit,
        searchTag,
        loading_search,
        initial,
        reqArray,
        pendingReq,
        cover_pic_display,
        cropModal,
        params,
        // searchString,
        introEdit,
        workEdit,
        intro,
        work,
        modalvisible,
        // prev_location,
        page_loader,
        offset,
        limit,
        search_text
    } = profileStates

    const setAlbumToEdit = (item) => {
        setProfileState(prev => ({ ...prev, item_to_edit: item }))
    }
    const dataCallback = (type, dataFromChild, _query = null) => {
        if (type == 'change') {
            setProfileState(prev => ({ ...prev, DataFromChild: dataFromChild, loader: true }));
            switch (dataFromChild) {
                case 1:
                    //setProfileState(profileStates => ({ ...profileStates, initial: dataFromChild, loader: false }))
                    setProfileState(prev => ({ ...prev, initial: dataFromChild, loader: false }));
                    break;
                case 2:
                    setProfileState(prev => ({ ...prev, loading_search: true, loader: true }))
                    getPostByUsers({ user_id: userId, type: 'post', offset: 0, limit: 1000, query: _query })
                        .then(posts => {
                            //setProfileState((profileStates) => ({ ...profileStates, initial: dataFromChild, postList: posts.data.data, loading_search: false, loader: false }));
                            setProfileState(prev => ({ ...prev, initial: dataFromChild, postList: posts.data.data, loading_search: false, loader: false }));
                        });
                    break;
                case 4:
                    getAlbums(userId)
                    setProfileState(prev => ({ ...prev, DataFromChild: dataFromChild, initial: dataFromChild, loader: false }))
                    break;
                case 5:
                    getConnections({ user_id: user_id }).then((res) => {
                        // setProfileState((profileStates) => ({ ...profileStates, initial: dataFromChild, connectionArray: res.data, loader: false }));
                        setProfileState(prev => ({ ...prev, initial: dataFromChild, connectionArray: res.data, loader: false }));
                    })
                    break;
                case 6:
                    getMyFamily();
                    // setProfileState(profileStates => ({ ...profileStates, initial: dataFromChild, loader: false }))
                    setProfileState(prev => ({ ...prev, initial: dataFromChild, loader: false }));
                    break;
                case 7:
                    getMutualConnections({ user_one_id: user_id, user_two_id: profile_id.toString() }).then((res) => {
                        //setProfileState((profileStates) => ({ ...profileStates, initial: dataFromChild, connectionArray: res.data.data, loader: false }));
                        setProfileState(prev => ({ ...prev, initial: dataFromChild, connectionArray: res.data.data, loader: false }));
                    }).catch((err) => {
                        console.log("error happened")
                    })
                    break;
                case 8:
                    viewRequest({ user_id: user_id }).then((res) => {
                        let { data } = res;
                        setProfileState(prev => ({ ...prev, initial: dataFromChild, reqArray: data.data, loader: false }));
                        //setProfileState((profileStates) => ({ ...profileStates, initial: dataFromChild, reqArray: data.data, loader: false }));
                    })
                    break;
                case 9:
                    pendingRequest({
                        user_id: user_id.toString(),
                        type: "request"
                    })
                        .then((doc) => {
                            let { data } = doc.data;
                            // setProfileState((profileStates) => ({ ...profileStates, initial: dataFromChild, pendingReq: data, loader: false }));
                            setProfileState(prev => ({ ...prev, initial: dataFromChild, pendingReq: data, loader: false }));
                            setProfileState(prev => ({ ...prev, initial: dataFromChild, pendingReq: data }));
                            // setProfileState((profileStates) => ({ ...profileStates, initial: dataFromChild, pendingReq: data }));
                            // setProfileState((profileStates) => ({ ...profileStates, initial: dataFromChild, pendingReq: data }));
                        })
                        .catch(e => e)
                    break;
                case 10:
                    let _UID = parseInt(profile_id);
                    history.push({
                        pathname: "/topics/create",
                        state: { uid: [_UID], status: 'create' }
                    })
                    break;
                case 11:
                    setProfileState(prev => ({ ...prev, initial: dataFromChild, loader: false }))
                    break;

                case 12:
                    setProfileState(prev => ({ ...prev, initial: dataFromChild, loader: false }))
                    break;
                default:
                    // setProfileState(profileStates => ({ ...profileStates, initial: dataFromChild, loader: false }))
                    setProfileState(prev => ({ ...prev, initial: dataFromChild, loader: false }))
                    break;
            }
        } else if ('update') {
            setProfileState(previous => ({ ...previous, userDetails: dataFromChild, cover_pic_url: dataFromChild.cover_pic }));
        }
    }

    const getUserProfileDetails = (data) => {
        let _Params = {
            user_id: user_id,
            profile_id: data,
        };
        getUserDetails(_Params).then((response) => {
            setProfileState(prev => ({
                ...prev,
                userDetails: response.data.profile,
                count: response.data.count,
                groups: showGroup,
                userId: data, loader: false,
                cover_pic_url: response.data.profile.cover_pic,
                work: response.data.profile.work, intro: response.data.profile.about,
                page_loader: false
            }))
            if (response.data.profile && !response.data.profile.is_active) {
                setProfileState(prev => ({
                    ...prev, modalvisible: true
                }))
            }
        }).catch((error) => {
            console.log('error---->', error);
        })
    }

    const getAlbums = (Uid) => {
        let cipher = location.pathname.split('user-details/').pop()
        let viewId = decryption(cipher);
        let par_ams = {
            profile_id: viewId.toString(),
            from_user: Uid,
            folder_for: "users",
            folder_type: "albums"
        }
        getAllUserAlbums(par_ams).then((resp) => {
            setProfileState(previous => ({ ...previous, albums: resp.data.data }))
        })
    }

    // const changeModelFlag = () => {
    //     setProfileState(prev => ({ ...prev, modelFlag: !modelFlag }))
    // }

    // const deleteAlbumByID = (user_id, folder_id) => {
    //     setProfileState(prevState => ({ ...prevState, deleting: true, selectedFolder: folder_id }))
    //     let params = { user_id: user_id, folder_id: folder_id };
    //     deleteAlbum(params).then(async (resp) => {
    //         await getAlbums(user_id);
    //     })
    // }

    // const showAllGroups = () => {
    //     if (showfamily === true) {
    //         setProfileState(prev => ({ ...prev, groups: groups.concat(showGroup), showfamily: !showfamily }))
    //     } else {
    //         setProfileState(prev => ({ ...prev, groups: groups.splice(0, 4), showfamily: !showfamily }))
    //     }
    // }

    // const setUserCover = (url) => {
    //     setProfileState(prev => ({ ...prev, cover_pic_url: `${url}` }))
    // }
    useEffect(() => {
        let { state } = history.location
        let { prev } = state;
        setProfileState(prev_ => ({ ...prev_, prev_location: prev ? prev : '/post' }))
    }, [])
    useEffect(() => {
        let cipher = location.pathname.split('user-details/').pop()
        let section = location.pathname.split('/')[3]
        section && dataCallback('change', parseInt(section))
        section && setProfileState(prev => ({ ...prev, initial: "4" }))
        let user_Id = decryption(cipher);
        setProfileState(prev => ({ ...prev, profile_id: user_Id.toString() }))
        const user_Details = async () => {
            await getUserProfileDetails(user_Id);
        }
        user_Details();
        const albumList = async () => {
            await getAlbums(user_Id);
        }
        albumList();
    }, [history.location])

    const navigateToUser = (value) => {
        // window.location.reload()
        var _userId = encryption(value.id)
        history.push(`/user-details/${_userId}`, { prev: window.location.pathname });
    }

    // const navigateTo = (value) => {
    //     history.push(`${value}`);
    // }

    const listFamilyToAddMember = (value, e) => {
        e.stopPropagation()
        setProfileState(prev => ({ ...prev, familyvisible: true, selectedUser: value.id }))
        let data = {
            user_id: user_id,
            member_to_add: value.id.toString()
        }
        getGroupsToAddMember(data).then((res) => {
            setProfileState(prev => ({ ...prev, addFamilyList: res.data.data }))
        }).catch((err) => {

        })
    }

    const addMemberToFamily = (value, index) => {
        let params_add = {
            userId: selectedUser.toString(),
            groupId: value.id.toString(),
            from_id: user_id
        }
        addMemberToGroup(params_add).then((resp) => {
            console.log('resp', resp);
            if (resp && resp.data && resp.data.status === 'accepted') {
                let _addFamilyList = addFamilyList
                _addFamilyList[index].JOINED = true;
                setProfileState(prev => ({ ...prev, addFamilyList: _addFamilyList }))
            } else if (resp && resp.data && resp.data.status === 'pending') {
                console.log("== pending ===")
            }
        }).catch((err) => {

        })
    }

    const handleCancel = () => {
        setProfileState((prev) => ({ ...prev, familyvisible: false }));
    };
    // Modal - Album
    // const al_showModal = () => {
    //     setProfileState(prev => ({
    //         ...prev,
    //         al_visible: true,
    //     }))
    // };

    // const al_handleCancel = e => {
    //     setProfileState(prev => ({
    //         ...prev,
    //         al_visible: false,
    //     }))
    // };

    const onAlbumSearch = (event) => {
        setProfileState(prev => ({ ...prev, searchTag: event.target.value }))
    }

    // accept/reject family join request by user
    const _invitationResp = (item, status) => {
        item = {
            ...item,
            id: item.req_id.toString(),
            user_id: user_id.toString(),
            group_id: item.group_id.toString(),
            status: status,
            from_id: item.from_id.toString()
        }
        invitationResp(item).then((res) => {
            dataCallback('change', 8)
        })
    }

    const _deleteRequest = (id) => {
        deleteRequest({ id: id.toString() })
            .then((doc) => {
                dataCallback('change', 9)
            })
            .catch(e => e)
    }
    const enableEditMode = (type = null) => {
        let _params = params
        _params.editType = type
        _params.user_id = user_id
        setProfileState((prev) => ({ ...prev, cropModal: !cropModal, params: _params }))
    }

    const pic_dataCallback = (type, data) => {
        switch (type) {
            case 'user_cover':
                setProfileState(prev => ({ ...prev, userDetails: data, cropModal: false, cover_pic_url: data.cover_pic }));
                dispatch({ type: 'USER_DETAILS_UPDATE', payload: { data: data } });
                break;
            case 'condition1':
                break;
            case 'condition2':
                break;
            default:
                break;
        }
    }
    /**
     * key word search on enter call fet family functaion
     */
    const SearchgetMyFamily = (e) =>{
        if(e.keyCode == 13){ //enter functionality
            let _search_text = e.target.value.trim();
            let _offset_ = 0; let _groups= []; 
            setProfileState(prev => ({ ...prev, groups:_groups, offset:_offset_, search_text: _search_text!='' ? _search_text:null }));
            getMyFamily();
        }
    }

    /**
     * get my families
     */
    const getMyFamily = () => {
        var params_get = {
            user_id: userId != user_id ? userId : user_id,
            query: search_text,
            limit:limit,
            offset:offset
        }
        setProfileState(prev => ({ ...prev, loader: true, loader: true }))
        viewFamily(params_get).then((response) => {
            let { data } = response.data;
            let new_data = [...groups, ...data];
            let _offset  = offset + data.length;
            // setProfileState(prev => ({ ...prev, loader: false }))
            //if (response && response.data && response.data.data) {
            setProfileState((prev)=>({
                ...prev,
                groups:new_data,
                offset:_offset,
                loader:false
            }));
            //setProfileState(prev => ({ ...prev, groups: response.data.data }))
            //}
        }).catch((error) => {
            console.log('error--->', error);
        })
    }
    const goToFamily = (id) => {
        var familyid = encryption(id)
        history.push(`/familyview/${familyid}`)
    }
    const enableEdit = (name) => {
        setProfileState((prev) => ({ ...prev, [name]: true }));

    }
    const editContent = (type, e) => {
        setProfileState(prev => ({ ...prev, [type]: e.target.value }))
    }
    const updateProfile = (type, editFlag) => {
        var up_params;
        if (type == 'intro') {
            up_params = {
                "id": user_id,
                "about": intro.trim()
            }
        }
        if (type == 'work') {
            up_params = {
                "id": user_id,
                "work": work
            }
        }
        setProfileState((prev) => ({ ...prev, loader: true }))

        updateUserDetails2(up_params).then((response) => {
            setProfileState((prev) => ({ ...prev, loader: false }))

            if (response) {
                setProfileState((prev) => ({ ...prev, [editFlag]: false }))
            }
        }).catch(err_prof => {
            console.log(err_prof)
        })
    }

    const handleModalOk = () => {
        history.push('/post');
        setProfileState((prev) => ({ ...prev, modalvisible: false }))
    }

    // implementing lazyload data
    
    const [element,setElement] = useState(null);
    const Loader = useRef(getMyFamily)
    const observer = useRef(new IntersectionObserver((entries)=>{
        let last = entries[0];
        last.isIntersecting && Loader.current();
    }, { threshold:1 } ) );
    useEffect(()=>{
        const currentElement = element;
        const currentObserver = observer.current;
        if(currentElement){
            currentObserver.observe(currentElement)
        }
        return() =>{
            if(currentElement) currentObserver.unobserve(currentElement);
        }
    },[element]);
    useEffect(()=>{
        Loader.current = getMyFamily;
    },[getMyFamily]);

    return (
        <section className="userDetails f-page">
            {cover_pic_display && <Lightbox
                mainSrc={process.env.REACT_APP_S3_BASE_URL + 'user_original_image/' + userDetails.user_original_image}
                onCloseRequest={() => setProfileState(prev => ({ ...prev, cover_pic_display: false }))}
            />}
            {propic_display && <Lightbox
                mainSrc={process.env.REACT_APP_S3_BASE_URL + 'propic/' + userDetails.propic}
                onCloseRequest={() => setProfileState(prev => ({ ...prev, propic_display: false }))}
            />}
            {/* <Lazyloader loader={loader}></Lazyloader> */}
            {page_loader &&
                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                </Spin>
            }
            <Modal
                cancelButtonProps={{ style: { display: 'none' } }}
                title="Basic Modal"
                visible={modalvisible}
                onOk={handleModalOk}
            >
                <p>Oops! User is no longer available.</p>
            </Modal>
            {userDetails && Object.keys(userDetails).length > 0 && userDetails.is_active &&
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <div className="f-event-details">
                            <header className="f-head">
                                <div className="f-poster">
                                    {userDetails.cover_pic ? <img
                                        src={process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + cover_pic_url} /> :
                                        <div className="f-poster-default">
                                            <img src={require('../../../assets/images/default.png')} />
                                            <h4>Upload a photo to be displayed here !</h4>
                                            <p>Size recommendation: 1044 X 400 px</p>
                                        </div>
                                    }
                                    <span className="f-overlay" onClick={() => {
                                        setProfileState(prev => ({ ...prev, cover_pic_display: true }))
                                    }}>
                                        <div className="f-head f-profile-cover-fix">
                                            <div>
                                                <h3>{userDetails.full_name}</h3>
                                                <h5 className="f-location">{userDetails.location}</h5>
                                            </div>
                                            {/* <Button className="f-add-to-fam">Add to Family</Button> */}
                                        </div>
                                    </span>
                                    {userDetails.id == user_id && <Button onClick={() => { enableEditMode('user_cover_pic') }} className="f-edit-poster-pic f-qfix"><Icon type="camera" /></Button>}
                                    <span className="f-display-pic">
                                        {userDetails.propic ? <img onClick={() => {
                                            setProfileState(prev => ({ ...prev, propic_display: true }))
                                        }} onError={(e) => { e.target.onerror = null; e.target.src = "../images/default_propic.png" }} src={process.env.REACT_APP_PRO_PIC_RESIZE + process.env.REACT_APP_S3_BASE_URL + 'propic/' + userDetails.propic} /> :

                                            <div className="f-logo-default">
                                                {userDetails.gender == 'female' ? <img src={require('../../../assets/images/Female-Profile-pic.png')} /> : userDetails.gender == 'male' ? <img src={require('../../../assets/images/Male-Profile-pic.png')} /> : <img src={require('../../../assets/images/default.png')} />}
                                                {/* <img src={require('../../../assets/images/default.png')} /> */}
                                                <h4>Profile Photo</h4>
                                                <p>120 x 120 px</p>
                                            </div>
                                        }
                                        {/* Hide this if no profile pic */}
                                        {userDetails.id == user_id && <Button onClick={() => { enableEditMode('user_profile_pic') }} className="f-edit-display-pic f-qfix"><Icon type="edit" /></Button>}
                                    </span>
                                </div>
                                <div className="f-profile-meta">
                                    <ul>
                                        <li onClick={() => dataCallback('change', 6)} className="f-cursor-pointer">
                                            <strong>{count.familyCount ? count.familyCount : 0}</strong>
                                            <i>Families</i>
                                        </li>
                                        {(user_id === userId) &&
                                            <li onClick={() => dataCallback('change', 5)} className="f-cursor-pointer">
                                                <strong>{count.connections ? count.connections : 0}</strong>
                                                <i>Connections</i>
                                            </li>
                                        }
                                        {(user_id !== userId) &&
                                            <li onClick={() => dataCallback('change', 7)} className="f-cursor-pointer">
                                                <strong>{count.mutual_connections ? count.mutual_connections : 0}</strong>
                                                <i>Mutual Connections</i>
                                            </li>
                                        }

                                        {user_id != userId && <li onClick={() => dataCallback('change', 10)} className="f-cursor-pointer">
                                            <span className="pd-l">
                                                <img src={require('../../../assets/images/icon/chat_d.svg')} />
                                            </span>
                                        </li>}



                                        <li className="f-alt">
                                            {/* <Button /> */}
                                        </li>
                                    </ul>
                                </div>
                                {/* <div className="f-profile-meta" style={{paddingLeft:'0px'}}> 
                                    <div><Button type="link" className="f-btn-back" onClick={()=>history.push({pathname:`${prev_location}`,state: {}})}><Icon type="left" /> Back to previous page</Button></div>
                                </div> */}
                            </header>
                            <section
                                className="f-sect f-pt30"
                            >

                                {userDetails && <UserDetailsSidebar
                                    callbackFromParent={dataCallback}
                                    profileId={userId}
                                    user_details={userDetails}
                                    history={history} initial={initial} />}
                                <div className="f-events-main">

                                    {/* Introduction */}
                                    {DataFromChild == '1' &&
                                        <div
                                            id='description'
                                            className="f-event-contents"
                                        >
                                            <div className="f-switch-content">
                                                {/* INTRODUCTION */}
                                                {/* <div className="f-title">
                                                    <h1>Introduction
                                                    {(user_id === userId) && <Button onClick={() => enableEdit('introEdit')} className="f-title-edit" />}</h1>
                                                </div> */}
                                                {loader &&
                                                    <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                                                    </Spin>
                                                }
                                                {!introEdit && !loader && <div className="f-content">
                                                    <p>{intro ? intro : ''}</p>
                                                </div>}
                                                {introEdit && !loader && <div className="f-content">
                                                    <div className="f-form-control">
                                                        <TextArea rows={4} value={intro} onChange={(e) => editContent('intro', e)}></TextArea>
                                                    </div>
                                                    <div className="f-form-control">
                                                        <Button className="f-button" onClick={() => updateProfile('intro', 'introEdit')}>Save</Button>
                                                        <Button className="f-button f-line" onClick={() => setProfileState((prev) => ({ ...prev, introEdit: false }))}>Cancel</Button>
                                                    </div>
                                                </div>}

                                                <div className="f-title">
                                                    {/* <p>{userDetails.about ? userDetails.about : 'No Data'}</p> */}
                                                    <h1>Origin: <strong>{userDetails.origin}</strong></h1>
                                                </div>
                                                <div className="f-content">
                                                    <p></p>
                                                </div>

                                                {/* WORK */}
                                                <div className="f-title">
                                                    <h1>Work
                                                    {(user_id === userId) && <Button onClick={() => enableEdit('workEdit')} className="f-title-edit" />}</h1>
                                                </div>
                                                {!workEdit && !loader && <div className="f-content">
                                                    <p>{work ? work : ''}</p>
                                                </div>}
                                                {workEdit && !loader && <div className="f-content">
                                                    <div className="f-form-control">
                                                        <TextArea rows={4} value={work} onChange={(e) => editContent('work', e)}></TextArea>
                                                    </div>
                                                    <div className="f-form-control">
                                                        <Button className="f-button" onClick={() => updateProfile('work', 'workEdit')}>Save</Button>
                                                        <Button className="f-button f-line" onClick={() => setProfileState((prev) => ({ ...prev, workEdit: false }))}>Cancel</Button>
                                                    </div>
                                                </div>}

                                            </div>
                                        </div>
                                    }
                                    {/* Posts */}
                                    {
                                        DataFromChild == '2' &&
                                        <div id='description' className="f-event-contents f-blanco">
                                            <div className="f-member-listing">
                                                <div className="f-content-searchable">
                                                    <h4><strong>{postList.length}</strong> Posts</h4>
                                                    <Search loading={loading_search} placeholder="Discover" onSearch={(value) => dataCallback('change', 2, value)} />
                                                </div>
                                            </div>

                                            {postList && postList.map((v, index) => {
                                                return v.shared_user_names ?
                                                    <PostsCard
                                                        type="2"
                                                        data={v}
                                                        key={index}
                                                        history={history}
                                                        chatVisible={false}
                                                    /> : <PostsCard
                                                        type="1"
                                                        from="profile"
                                                        data={v}
                                                        key={index}
                                                        history={history}
                                                        chatVisible={false}
                                                    />;
                                            })}
                                            {loader &&
                                                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                                                </Spin>
                                            }
                                            {postList.length == 0 && !loader && <div>No Data</div>}
                                        </div>

                                    }

                                    {/* Album */}
                                    {DataFromChild == '4' &&
                                        // <div id='album' className="f-event-contents f-blanco">
                                        <div id='album' className="f-event-contents f-blanco">
                                            {/* Album List */}
                                            <div className="f-switch-content">
                                                <div className="f-content">
                                                    <div className="f-clearfix">
                                                        <div className="f-member-listing">
                                                            <div className="f-content-searchable">
                                                                {/* <Button onClick={() => al_showModal()} className="f-btn">Create Album</Button> */}
                                                                <CreateAlbum
                                                                    getAlbums={getAlbums}
                                                                    type={'users'}
                                                                    setAlbumToEdit={setAlbumToEdit}
                                                                    userDetails={userDetails}
                                                                    item_to_edit={item_to_edit}
                                                                />
                                                                <Search onChange={onAlbumSearch} placeholder="Discover" />
                                                            </div>
                                                            <div className="f-content-searchable">
                                                                <h4><strong>{albums && albums.length ? albums.length : 0}</strong> Albums</h4>
                                                            </div>
                                                            <AlbumCard
                                                                searchTag={searchTag}
                                                                setAlbumToEdit={setAlbumToEdit}
                                                                _userID={userDetails.id}
                                                                displayContent={albums}
                                                                groupId={profile_id}
                                                                history={history}
                                                                _getAlbums={getAlbums}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* Album List End */}
                                        </div>
                                    }
                                    {/* Connections */}
                                    {((DataFromChild === 5 && (user_id === profile_id)) || (DataFromChild === 7 && (user_id !== profile_id))) &&
                                        <div id='connection' className="f-event-contents f-blanco">
                                            <div className="f-switch-content">
                                                <div className="f-content">
                                                    <div className="f-clearfix">
                                                        <div className="f-member-listing">
                                                            <div className="f-content-searchable">
                                                                <h4><strong>{connectionArray ? connectionArray.length : 0}</strong> {user_id === profile_id ? 'Connections' : 'Mutual Connections'}</h4>
                                                                {/* <Search placeholder="Search" /> */}
                                                            </div>
                                                            <div className="f-member-listing-cards">
                                                                {connectionArray && connectionArray.length > 0 && !loader &&
                                                                    connectionArray.map((item, index) => {
                                                                        return <Card key={index} className="f-member-cardx" onClick={() => navigateToUser(item)}>
                                                                            <div className="f-inset">
                                                                                <div className="f-start">
                                                                                    <div className="f-left">
                                                                                        <img onError={(e) => { e.target.onerror = null; e.target.src = "../images/default_propic.png" }} src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : (item.gender === 'male' ? require('../../../assets/images/male-avatar.jpg') : require('../../../assets/images/female-avatar.jpg'))} />
                                                                                    </div>
                                                                                    <div className="f-right">
                                                                                        <h4>{item.full_name}</h4>
                                                                                        <h6 className="f-pin">{item.location} {item.origin}</h6>
                                                                                        <div className="f-right-combo" style={{ marginTop: '10px' }}>
                                                                                            <Button onClick={(e) => listFamilyToAddMember(item, e)}>Add to Family</Button>
                                                                                            {/* <Button className="f-line">Block</Button> */}
                                                                                            {/* <Button>Unblock</Button> */}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </Card>
                                                                    })
                                                                }
                                                            </div>
                                                            {loader &&
                                                                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                                                                </Spin>
                                                            }
                                                            <div className="f-clearfix text-center">
                                                                <Button type="link">View All</Button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    {/* Familheey */}
                                    {DataFromChild == '6' &&
                                        <div id='familheey' className="f-event-contents f-blanco">
                                            <div className="f-switch-content">
                                                <div className="f-content">
                                                    <div className="f-clearfix">
                                                        <div className="f-member-listing">
                                                            <div className="f-content-searchable">
                                                                <h4>
                                                                    {/* <strong>{groups.length}</strong> Familheey */}
                                                                </h4>
                                                                <Search placeholder="Type family name and enter" onKeyUp={SearchgetMyFamily}/>
                                                            </div>
                                                            <div className="f-family-list">
                                                                {groups && groups.length > 0  &&
                                                                    groups.map((item, index) => {
                                                                        return <div key={index} className="f-family-card" onClick={() => goToFamily(item.id)}>
                                                                            <div className="f-start">
                                                                                <div className="f-left">
                                                                                    <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/default_logo.png')} />
                                                                                </div>
                                                                                <div className="f-right">
                                                                                    <h4>{item.group_name}</h4>
                                                                                    <h5>By <strong>{item.created_by}</strong></h5>
                                                                                    <h6>{item.group_category}</h6>
                                                                                    <h6 className="f-pin">{item.base_region}</h6>
                                                                                </div>
                                                                            </div>
                                                                            <div className="f-stop">
                                                                                <h5><strong>{item.member_count}</strong> {item.member_count > 1 ? 'Members' : 'Member'}</h5>
                                                                                {/* <h5><strong>{item.post_count}</strong>  Posts</h5> */}
                                                                            </div>
                                                                        </div>
                                                                    })

                                                                }
                                                                {!loader && groups && groups.length === 0 &&
                                                                    <div>No Data</div>
                                                                }
                                                            </div>
                                                            <div className="f-float">
                                                                { loader && <Spin tip="Loading..." style={{ 'paddingLeft': '50%' }}></Spin> }
                                                            </div>
                                                            
                                                            <div className="f-float" ref={setElement}></div>
                                                            
                                                            {/* <div className="f-clearfix text-center">
                                                                <Button type="link" onClick={() => showAllGroups()}>{groups.length > 4 ? 'View Less' : 'View All'}</Button>
                                                            </div> */}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>}

                                    {/* family join request */}
                                    {DataFromChild == '8' &&
                                        <div id='familheey' className="f-event-contents f-blanco">
                                            <div className="f-switch-content">
                                                <div className="f-content">
                                                    <div className="f-clearfix">
                                                        <div className="f-member-listing">
                                                            <div className="f-content-searchable">
                                                                <h4>{/* <strong></strong> */}View Requests</h4>
                                                                <Search placeholder="Discover" />
                                                            </div>
                                                            <div className="f-member-listing-cards">
                                                                {reqArray && reqArray.length > 0 &&
                                                                    reqArray.map((item, index) => {
                                                                        return <Card key={index} className="f-member-cardx">
                                                                            <div className="f-start">
                                                                                <div className="f-left">
                                                                                    <img
                                                                                        onError={(e) => {
                                                                                            e.target.src = require('../../../assets/images/Male-Profile-pic.png')
                                                                                        }}
                                                                                        src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic} />
                                                                                </div>
                                                                                <div className="f-right">
                                                                                    <h4>{item.full_name}</h4>
                                                                                    <h6>{`Invited you to join ${item.group_name}`}</h6>
                                                                                    <div className="f-right-combo" style={{ marginTop: '10px' }}>
                                                                                        <Button onClick={() => _invitationResp(item, 'accepted')}>Accept</Button>
                                                                                        <Button className="f-line" onClick={() => _invitationResp(item, 'rejected')}>Reject</Button>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </Card>
                                                                    })

                                                                } {reqArray && reqArray.length === 0 &&
                                                                    <div>No Pending Requests</div>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>}


                                    {/* pending request send */}
                                    {DataFromChild == '9' &&
                                        <div id='pending' className="f-event-contents f-blanco">
                                            <div className="f-switch-content">
                                                <div className="f-content">
                                                    <div className="f-clearfix">
                                                        <div className="f-member-listing">
                                                            <div className="f-content-searchable">
                                                                <h4><strong></strong> Requests Send</h4>
                                                                <Search placeholder="Discover" />
                                                            </div>
                                                            <div className="f-family-list">
                                                                {pendingReq && pendingReq.length > 0 &&
                                                                    pendingReq.map((item, index) => {
                                                                        return <div key={index} className="f-family-card">
                                                                            <div className="f-start">
                                                                                <div className="f-left">
                                                                                    <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/demo/demo_u3.png')} />
                                                                                </div>
                                                                                <div className="f-right">
                                                                                    <h4>{item.group_name}</h4>
                                                                                    {/* <h5>By <strong>{item.created_by}</strong></h5> */}
                                                                                    <h6>{item.group_category}</h6>
                                                                                    <h6 className="f-pin">{item.base_region}</h6>
                                                                                    <div className="f-right-combo">
                                                                                        <Button onClick={() => _deleteRequest(item.id)} className="f-line">Cancel</Button>
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    })

                                                                } {pendingReq && pendingReq.length === 0 &&
                                                                    <div>No Data</div>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>}
                                    {
                                        DataFromChild == '11' &&
                                        <RequestList
                                            history={history}
                                            by_user={params.by_user}
                                            sideBarVisible={false}
                                            style_passed={{ paddingTop: '0px' }}
                                        />
                                    }

                                    {
                                        DataFromChild == '12' &&
                                        <PaymentHistory
                                            paymentHistoryByUserid={paymentHistoryByUserid}
                                            userId={user_id}
                                            forUsers={true}
                                        />
                                    }



                                    {/* Modal */}
                                    <Modal
                                        className="f-modal f-modal-wizard"
                                        title="Add to Family"
                                        visible={familyvisible}
                                        onOk={handleCancel}
                                        onCancel={handleCancel}
                                        footer={[
                                            //   <button>cancel</button>
                                        ]}
                                    >
                                        <div className="f-modal-wiz-body" >
                                            <div className="f-member-listing-cards f-share-scroll">

                                                {addFamilyList && addFamilyList.length > 0 &&
                                                    addFamilyList.map((item, index) => {
                                                        return <Card key={index} className="f-member-cardx">
                                                            <div className="f-start">
                                                                <div className="f-left">
                                                                    <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../assets/images/def_x.png')} />
                                                                </div>
                                                                <div className="f-right">
                                                                    <h4>{item.group_name}</h4>
                                                                    <h6>By <strong>{item.created_by}</strong></h6>
                                                                    <h6 className="f-pin">{item.base_region}</h6>
                                                                    <div className="f-right-combo" style={{ marginTop: '10px' }}>
                                                                        {
                                                                            item && item.JOINED ?
                                                                                <Button className="f-line" disabled>MEMBER</Button> :
                                                                                (item.status == 'pending') ?
                                                                                    <Button className="f-line" disabled>pending</Button> :
                                                                                    <Button onClick={() => addMemberToFamily(item, index)}>Join</Button>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Card>
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </Modal>
                                </div>
                            </section>
                        </div>
                    </div>
                    <Modal
                        visible={cropModal}
                        destroyOnClose={true}
                        footer={null}
                        onCancel={() => { enableEditMode() }}
                        className="f-modal f-modal-wizard"
                    >
                        <CustomImageCrop
                            params={params}
                            callBackimgFromParent={pic_dataCallback}
                        />

                    </Modal>
                </div>
            }
        </section>
    )
}

export default withRouter(UserDetails)