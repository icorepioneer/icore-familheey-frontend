import axios from '../../../services/apiCall';
import axios1 from '../../../services/fileupload'

const getUserDetails = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/view_profile`, data)
}
const clearNotifications = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/clearNotification`, data)
}
const getUserFolders = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listUserFolders`, data)
}
const updateUserDetails = (data) => {
    return axios1.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/edit_profile`, data)
}
const getPostByUsers = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/get_my_post`, data)
}
const getMutualConnections = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getMutualConnections`, data)
}
const getConnections = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_connections`, data)
}
const viewRequest = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listallInvitation`, data)
}
const invitationResp = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/invitationResp`, data)
}
const pendingRequest = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/pending_requests`, data)
}
const deleteRequest = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/delete_pending_requests`, data)
}
const updateUserDetails2 = (data) =>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/edit_profile`, data)
}
const getNewAccessToken = (data) =>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getAccessToken`, data)
}
const paymentHistoryByUserid = (data) =>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/paymentHistoryByUserid`, data)
}
const paymentHistoryByGroupid = (data) =>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/paymentHistoryByGroupid`, data)
}
const getNotSettings = (data) =>{
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_notification_settings`, data)
}

export {
    getUserDetails,
    clearNotifications,
    getUserFolders,
    updateUserDetails,
    getPostByUsers,
    getMutualConnections,
    getConnections,
    viewRequest,
    invitationResp,
    pendingRequest,
    deleteRequest,
    updateUserDetails2,
    getNewAccessToken,
    paymentHistoryByUserid,
    paymentHistoryByGroupid,
    getNotSettings
}