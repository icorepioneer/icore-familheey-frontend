import React, { useState } from 'react';
import { Button, Input, Upload, Icon,notification } from 'antd';
import { feedBackSubmit } from '../../modules/Post/post_service'
import { notifications } from '../../shared/toasterMessage'
import FooterBar from '../../components/FooterBar';
import { useSelector } from 'react-redux'

const props = {
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    listType: 'picture',
}
const Support = ({ history }) => {
    const [feedBackState, setfeedBackState] = useState({
        user_id: useSelector(state=>state.user_details.userId),
        description: '',
        title: '',
        image: '',
        
    })
    const { user_id, description, title, image } = feedBackState;

    const uploadPhoto = info => {
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, imageUrl =>
                setfeedBackState((prev) => ({...prev, imageUrl, loading: false})),
            );
        }
    }
    const getBase64 = (img, callback) => {
        console.log('imggg', img);
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
        setfeedBackState((prev) => ({...prev, image: img }));
    }
    const handleChange = (name, e)  => {
        e.persist();
        setfeedBackState((prev) => ({...prev, [name]: e.target.value }))
    }
    const handleOk = e => {
        if (!title || !description) {
            let message = 'Error'
            let descr= `Please fill all the required fields`
            notifications('error', message, descr)
            return;
          }
        setfeedBackState((prev) => ({...prev, modelValue: false }));
        const formData = new FormData();
        formData.append('os', 'web');
        formData.append('description', description);
        formData.append('title', title);
        formData.append('user', user_id);
        formData.append('device', navigator.platform);
        formData.append('image', image);
        feedBackSubmit(formData).then((response) => {
            openNotification();
            setTimeout(()=>{history.goBack();
            },500)

        }).catch((error) => {
            console.log("error happened",error)
        })
    };
    const openNotification = () => {
        notification.open({
          message: "Successfully submitted your feedback",
          description: "",
          icon:<Icon type="alert" theme = "twoTone" twoToneColor="#3a2262"/>,
          duration: 3
        });
      };

    return (
        <section className="f-clearfix f-static-page f-support-page">
            <header className="f-head-skelton">
                <div className="f-boxed">
                    <div className="f-head-dock">
                        <a href="/" className="f-brand"><img src={require('../../assets/images/logo-m.png')} alt="Familheey" /></a>
                    </div>
                </div>
            </header>
            <section className="f-page">
                <section className="f-clearfix f-body">
                    <section className="f-boxed">
                        <section className="f-clearfix">
                            <h1>Support</h1>
                            <div className="f-modal-wiz-body">
                                <div className="f-form-control">
                                <label>Title</label>
                                <Input value={title} placeholder="Enter Title" onChange={(e) => handleChange('title',e)}></Input>
                                </div>
                                <div className="f-form-control">
                                <label>Image</label>
                                <Upload {...props} onChange={(event) => uploadPhoto(event)}>

                                <Icon type="upload" /> Click to Upload
                                        
                                </Upload>
                                </div>
                                <div className="f-form-control">
                                <label>Description</label>
                                <Input value={description} placeholder="Enter Title" onChange={(e) => handleChange('description',e)}></Input>
                                </div>
                            </div>
                            <div className="f-modal-wiz-footer" key="1">
                                <Button className="f-button" key="submit" onClick={() => handleOk()}>Submit <Icon type="arrow-right" /></Button>
                            </div>
                        </section>
                    </section>
                </section>
            </section>
            <FooterBar />
        </section>
    )
}

export default (Support);
