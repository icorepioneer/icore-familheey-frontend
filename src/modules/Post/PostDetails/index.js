import React from 'react';
import './style.scss'
import { Button, Card, Modal, Input, Drawer } from "antd";
import { decryption } from '../../../shared/encryptDecrypt'
import PostsCard from '../../../components/PostsCard';
import { getPostByUsers } from '../../users/userProfile/api-profile';
import { postLike, updateComment, publicFeed, getCommentsByPost, updateLastReadMessage, deactivatePost, activatePost } from '../post_service';
import openSocket from 'socket.io-client';
import { connect } from 'react-redux'


// const { Meta } = Card;
const { TextArea } = Input;

var moment = require('moment');

class PostDetails extends React.Component {
    state;
    post_id;
    activeTimer;
    constructor(props) {
        super(props);
        this.props = props;
        let { userID } = props
        this.state = {
            familyDetails: '',
            imgFile: '',
            user_id: userID,
            loader: true,
            postList: [],
            visible: [],
            S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,
            createPostFlag: false,
            modalvisible: false

        };
        this.socket = openSocket(process.env.REACT_APP_SOCKET_URL);
        // this.socket.on('notification_940', timestamp => {
        //     console.log(timestamp);
        //     // socket.emit('test1', 'heloo');
        // });
        // socket.emit('subscribeToTimer', 1000);
    }
    componentDidMount() {

        var post_id = decryption(this.props.match.params.id);
        if (!post_id) {
            this.props.history.goBack();
        }
        this.post_id = post_id;
        this.getALl(post_id);
        this.activatePostforUser(post_id);
        this.activeTimer = setInterval(() => {
            this.activatePostforUser(post_id);
        }, 100000);
    }
    activatePostforUser = (post_id) => {
        activatePost({ user_id: this.state.user_id, post_id: parseInt(post_id), device_type: 'web' })
            .then(res => {
            });
    }
    deactivatePostforUser = (post_id) => {
        deactivatePost({ user_id: this.state.user_id, post_id: parseInt(post_id), device_type: 'web' })
            .then(res => {
            });
    }
    componentWillUnmount() {
        this.deactivatePostforUser(this.post_id);
        clearInterval(this.activeTimer)
    }
    getALl = (post_id) => {
        this.setState({ loader: true });
        getPostByUsers({ user_id: this.state.user_id, post_id: post_id, type: 'post', offset: 0, limit: 1000 })
            .then(res => {
                this.setState({ postList: res.data.data, loader: false });
                if (res.data.data.length == 0) {
                    this.setState({ modalvisible: true });
                }

            });
    }
    publicFeed = () => {
        publicFeed({ user_id: this.state.user_id, type: 'post', offset: 0, limit: 1000 })
            .then(res => {
                this.setState({ postList: res.data.data, loader: false });
            });
    }
    onTabChange = (e) => {
        this.setState({ postList: [], loader: true })

        if (e.target.value == 'public') {
            this.publicFeed();
        } else {
            this.getALl();
        }
    }
    like = (value, index) => {
        postLike({
            post_id: value.post_id.toString(),
            user_id: this.state.user_id
        })
            .then(res => {
                this.state.postList[index].like_status = !this.state.postList[index].like_status;
                this.setState({ postList: this.state.postList });
            })
    }
    handleKeyPress = (value, index) => {
        // this.socket.emit('post_channel_typing', { post_id: value.post_id, user_id: this.state.user_id })
    }
    comment = (value, index) => {
        this.state.visible[index] = !this.state.visible[index];
        getCommentsByPost({ post_id: value.post_id.toString(), user_id: this.state.user_id })
            .then(res => {
                this.state.postList[index].comments = res.data.data;
                this.setState({ postList: this.state.postList });
                let totalConversation = this.state.postList[index].comments.length;
                if (totalConversation > 0) {
                    let conversationId = this.state.postList[index].comments[totalConversation - 1].id.comment_id.toString();
                    updateLastReadMessage({ post_id: value.post_id.toString(), user_id: this.state.user_id, last_read_message_id: conversationId })
                    .then(_res => {
                        console.log(_res)
                    });
                }
            });
        let channel_name = 'post_channel_' + value.post_id;
        this.socket.on('notification_' + this.state.user_id, message => {
            console.log('notification', message);
        });
        this.socket.on(channel_name, timestamp => {
            this.state.postList[index].comments.push(timestamp);
            console.log('timestamp');
            this.setState({ postList: this.state.postList });
        });
        this.setState({ visible: this.state.visible });
    }
    createPost = () => {
        this.props.history.push('/post/create');
    }
    onClose = (index) => {
        this.state.visible[index] = !this.state.visible[index];
        this.setState({ visible: this.state.visible });
    }
    save = (v, index) => {
        var formData = new FormData();
        formData.append('post_id', v.post_id.toString());
        formData.append('group_id', this.state.postList[index].to_group_id.toString());
        formData.append('comment', this.state.comment_data);
        formData.append('commented_by', this.state.user_id);
        formData.append('file_name', this.state.imgFile);

        updateComment(formData)
            .then(res => {
                // this.getALl();
            })
            .catch(err => {

            });
    }
    onChangeField = (e) => {
        this.setState({ comment: e.target.value })

    }
    onSelectFile = e => {
        this.setState({ imgFile: e.target.files[0] })
    };
    // Toggle Sidebar
    togglepostsidebar = () => {
        document.getElementById('f-wrap').classList.toggle("f-show-post-sider");
    }
    // Close Sidebar
    closepostsidebar = () => {
        document.getElementById('f-wrap').classList.remove("f-show-post-sider");
    }
    handleOk = () => {
        this.props.history.push('/post');
        this.setState({ modalvisible: false });
    }
    render() {
        // let path = '/post'
        return (
            <section className="listLandingDetails f-page">
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <Button className="f-create-form-back"
                            onClick={() => {
                                if (this.props.location && this.props.location.state && this.props.location.state.activeTab == 'public') {
                                    this.props.history.push('/post', { type_id: 2 })
                                } else if (this.props.location && this.props.location.state && this.props.location.state.activeTab == 'all') {
                                    this.props.history.push('/post', { type_id: 1 })
                                } else {
                                    this.props.history.goBack();
                                }
                                // this.props.history.push(`${this.props.location.state.backPath?this.props.location.state.backPath:path}`)

                            }}
                        />
                        <div className="f-clearfix f-post-listing">
                            {this.state.loader && <Card className="postsCard f-post-card" loading={this.state.loader}></Card>}
                            {
                                this.state.postList && this.state.postList.length > 0 && this.state.postList.map((v, index) => {
                                    return <div key={index} className="f-clearfix f-listland-card">
                                        <Drawer
                                            title="Basic Drawer"
                                            placement="right"
                                            closable={true}
                                            onClose={(e) => this.onClose(index)}
                                            visible={this.state.visible[index]}
                                        >
                                            <ul>
                                                {
                                                    v.comments && v.comments.map((x, ind) => {
                                                        return (<li key={ind} className={(x.commented_by == this.state.user_id) ? 'li-item align-right' : 'li-item align-left'} key={ind}>{x.comment} <br />
                                                            {x.file_name ? <img src={this.state.S3_BASE_URL + 'file_name/' + x.file_name} /> : ''}
                                                            {x.full_name},{moment(x.createdAt).format('DD-MM-YYYY HH:mm')}
                                                        </li>);
                                                    })

                                                }
                                            </ul>
                                            <input type="file" onChange={this.onSelectFile} />
                                            <TextArea rows={4} onKeyPress={() => this.handleKeyPress(v, index)} placeholder="Description" onChange={(e) => this.onChangeField(e)} />
                                            {/* <Button type="primary" onClick={(e) => this.save(v)}>SAVE</Button> */}
                                            <div>
                                                <button onClick={(e) => this.save(v, index)}>SAVE</button>
                                            </div>
                                        </Drawer>
                                        {v.shared_user_names ? <PostsCard
                                            type="2"
                                            data={v}
                                            key={index}
                                            page_type="details"
                                            history={this.props.history}
                                        /> : <PostsCard
                                                type="1"
                                                data={v}
                                                key={index}
                                                page_type="details"
                                                history={this.props.history}
                                            />} </div>;
                                })
                            }
                            {this.props.match.params.id && this.state.postList && this.state.postList.length == 0 &&
                                <Modal
                                    cancelButtonProps={{ style: { display: 'none' } }}
                                    title="Basic Modal"
                                    visible={this.state.modalvisible}
                                    onOk={this.handleOk}
                                >
                                    <p>Oops! This content is no longer available.</p>
                                </Modal>
                            }
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => {
    let { userId } = state.user_details;
    return {
        userID: userId
    }
}
export default connect(mapStateToProps, null)(PostDetails)