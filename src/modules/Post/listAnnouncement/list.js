import React, { useState, useEffect, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import './style.scss'
import { Button, Icon, Card, Spin, Radio } from "antd";
import PostsSidebar from '../../../components/PostsSidebar';
import PostsCard from '../../../components/PostsCard';
import { announcement_list as _announcement_list } from '../post_service';
import { useSelector } from 'react-redux'

// import TopBar from '../../../components/TopBar';
// import CreatePost from '../CreatePost';
// const { Meta } = Card;
// const { TextArea } = Input;

// var moment = require('moment');

const ListAnnouncementPage = ({ history }) => {
    //var dataCompleted = 0;
    let [state, setState] = useState({
        user_id: useSelector(stateX => stateX.user_details.userId),
        loader: true,
        S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,
        createPostFlag: false,
        family_count: 0,
        announcement_list: [],
        unread: [],
        read_announcement: [],
        unread_announcement: [],
        read_status: false,
        offset: 0,
        limit: 50,
        tabView: 1
    })
    let { user_id, loader, limit, offset,
        //family_count, S3_BASE_URL, createPostFlag, announcement_list, unread, 
        read_status,read_announcement, unread_announcement, tabView } = state;

    // useEffect(() => {
    //     getAll();
    // }, []);

    const getAll = () => {
        loader = true;
        setState(prev => ({ ...prev, loader: loader }));
        let search_params = {
            user_id: user_id,
            type: 'announcement',
            read_status: read_status,
            offset: offset,
            limit: limit
        }
        _announcement_list(search_params)
        .then(res => {
            let { data } = res;
            let new_data;
            let _offset;
            switch (tabView) {
                case 1:
                    new_data = [...unread_announcement, ...data.unread_announcement];
                    _offset =  offset+limit;
                    setState(prev => ({ ...prev, offset: _offset, unread_announcement: new_data, loader: false }));
                break;

                case 2:
                    new_data = [...read_announcement, ...data.read_announcement];
                    _offset =  offset+limit;
                    setState(prev => ({ ...prev, offset: _offset, read_announcement: new_data, loader: false }));
                break;
            
                default:
                break;
            }
        });
    }

    /**
     * scroll load and append new announcement
     */
    const [element, setElement] = useState(null);
    const Loader = useRef(getAll)
    const observer = useRef(new IntersectionObserver((entries) => {
        let last = entries[0];
        last.isIntersecting && Loader.current();
    }, { threshold: 1 }));
    
    useEffect(() => {
        const currentElement = element;
        const currentObserver = observer.current;
        if (currentElement) {
            currentObserver.observe(currentElement)
        }
        return () => {
            if (currentElement) currentObserver.unobserve(currentElement);
        }
    }, [element]);

    useEffect(() => {
        Loader.current = getAll;
    }, [getAll]);

    /* End scroll load and append new announcement */

    const create = () => {
        history.push('/announcement/create', { path_: window.location.pathname, type_: 'announcement' });
    }
    // Toggle Sidebar
    const togglepostsidebar = () => {
        document.getElementById('f-wrap').classList.toggle("f-show-post-sider");
    }
    // Close Sidebar
    const closepostsidebar = () => {
        document.getElementById('f-wrap').classList.remove("f-show-post-sider");
    }
    const familyCountRes = (count) => {
        setState(prev => ({ ...prev, family_count: parseInt(count) }));
    }
    const AfterRead = () => {
        if(read_status){
            offset = 0; read_announcement=[];
            setState(prev => ({ ...prev, offset:offset, read_announcement:read_announcement })); 
        }else{
            offset = 0; unread_announcement=[];
            setState(prev => ({ ...prev, offset:offset, unread_announcement:unread_announcement }));
        } 
        getAll();
    }

    /**
    * Tab change announcement call
    */
    const handleChange = (e) => {
        let { value } = e.target;
        unread_announcement = []; read_announcement = []; offset = 0;
        switch (value) {
            case '1':
                read_status = false; tabView = 1; 
                setState(prev => ({ ...prev, offset:offset, read_status: read_status, tabView: tabView, unread_announcement:unread_announcement }));
                break;
            case '2':
                read_status = true; tabView = 2; 
                setState(prev => ({ ...prev, offset:offset, read_status: read_status, tabView: tabView, unread_announcement:unread_announcement }));
                break;

            default:
                break;
        }
        getAll();//call announcmemnt list
    }
    // render() {
    return (
        <section className="listLanding f-page">
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-post-listing">
                        <PostsSidebar history={history} default='2' familyCountRes={familyCountRes} />
                        {/* {this.state.createPostFlag && <CreatePost visible={this.state.createPostFlag} />} */}
                        {/* Close Sidebar Mobile */}
                        <span className="f-close-post-sidebar" onClick={() => closepostsidebar()} style={{ display: 'none' }} />

                        <div className="f-posts-main">

                            <div className="f-posts-create f-force-hide-tab">
                                <Button onClick={create}>Create New Announcement <img src={require('../../../assets/images/icon/plus.svg')} /></Button>
                                {/* <Modal
                                    visible={true}
                                    onOk={this.handleOk}
                                    onCancel={this.handleCancel}
                                    footer={['']}
                                    className="f-create-event-options"
                                >
                                    <List>
                                        <List.Item>Create Post</List.Item>
                                        <List.Item>Make an Announcement</List.Item>
                                        <List.Item>Create Event</List.Item>
                                    </List>
                                </Modal> */}
                            </div>
                            <div className="f-posts-filters f-mg-btm0 f-force-hide-tab">

                                <Button onClick={() => togglepostsidebar()} className="f-postside-unfold" style={{ display: 'none' }}><Icon type="menu-unfold" /></Button>
                                {/* <div className="f-tab-ui">
                                    <Radio.Group defaultValue={'familheey'} onChange={(e) => this.onTabChange(e)}>
                                        <Radio value={'familheey'}>MY FAMILHEEY</Radio>
                                        <Radio value={'public'}>PUBLIC FEED</Radio>
                                    </Radio.Group>
                                </div> */}
                            </div>

                            <div className="f-event-contents">
                                <div className="f-tab-ui">

                                    <Radio.Group defaultValue="1" onChange={(e) => handleChange(e)}>
                                        <Radio value="1">Unread</Radio>
                                        <Radio value="2">Read</Radio>
                                    </Radio.Group>
                                </div>
                            </div>

                            {loader &&
                                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                                </Spin>
                            }


                            {/* Empty List */}
                            {/* {
                                announcement_list && announcement_list.length == 0 && unread.length == 0 &&
                                <div className="f-event-contents">
                                    <div className="f-switch-content">
                                        <div className="f-content">
                                            <div className="f-empty-content">
                                                <img src={require('../../../assets/images/create_post.png')} />
                                                {loader ? <Card className="eventCard f-event-card" loading={true}></Card> :
                                                    <div>
                                                        <h4>New announcements from your family members will show up here.<br />Search for your family and join them to see announcements that they share with you.</h4>
                                                        <p>If you can't find your family, then you can create one and invite members to join.</p>
                                                        <div className="f-clearfix">
                                                            <Button onClick={() => this.props.history.push('/searchResult', { current: window.location.pathname })}>Search Family</Button>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            } */}
                            {/* Empty List - END */}

                            {/* Post Cards */}
                            {
                                !loader && tabView == 1 && unread_announcement.length > 0 &&
                                <div className="f-post-feed">
                                    {/* <h4 className="f-feed-sep"><strong>{unread.length}  </strong> Announcements to be read </h4> */}
                                    {
                                        unread_announcement && unread_announcement.map((v, i) => {
                                            return (
                                                <div key={i} className="f-clearfix f-listland-card">
                                                    <PostsCard
                                                        type="3"
                                                        data={v}
                                                        callParentAfterDelete={AfterRead}
                                                        history={history}
                                                        chatVisible={false}
                                                    />
                                                </div>
                                            );
                                        })
                                    }
                                    {/* <PostsCard type="1" /> */}
                                    {/* <PostsCard type="2" /> */}
                                    {/* <PostsCard type="3" /> */}
                                </div>
                            }
                            {!loader && tabView == 1 && unread_announcement.length == 0 && <div className="f-event-contents">
                                <div className="f-switch-content">
                                    <div className="f-content">
                                        <div className="f-empty-content">
                                            <img src={require('../../../assets/images/create_post.png')} />
                                            {loader ? <Card className="eventCard f-event-card" loading={true}></Card> :
                                                <div>
                                                    <h4>New announcements from your family members will show up here.<br />Search for your family and join them to see announcements that they share with you.</h4>
                                                    <p>If you can't find your family, then you can create one and invite members to join.</p>
                                                    <div className="f-clearfix">
                                                        <Button onClick={() => this.props.history.push('/searchResult', { current: window.location.pathname })}>Search Family</Button>
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            }
                            {
                                !loader && tabView == 2 && read_announcement.length > 0 &&
                                <div className="f-post-feed">
                                    {/* <h4 className="f-feed-sep"><strong>{announcement_list.length}  </strong> Past announcements</h4> */}
                                    {
                                        read_announcement && read_announcement.map((v, i) => {
                                            return (
                                                <div key={i} className="f-clearfix f-listland-card">
                                                    <PostsCard
                                                        type="3"
                                                        data={v}
                                                        callParentAfterDelete={AfterRead}
                                                        history={history}
                                                        chatVisible={false}
                                                    />
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            }
                            
                        </div>
                        <div className="f-posts-main" ref={setElement}></div>
                    </div>
                </div>
            </div>
        </section>
    )
    // }
}

export default withRouter(ListAnnouncementPage)