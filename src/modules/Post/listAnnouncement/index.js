import React from 'react';
// import { withRouter } from 'react-router-dom';
import './style.scss'
import { Checkbox, Tabs, Progress, Button, Icon, Card, Modal, Menu, message, Dropdown, Input, Carousel } from "antd";
import AnnouncementSidebar from '../../../components/AnnouncementSidebar';
// import PostsCard from '../../../components/PostsCard';
import { getFamilyListForPost, mute_conversation, deletePost, remove_post } from '../../family/familyList/api-familyList';
import { post_share, user_group_members, announcement_list, updateComment, getCommentsByPost } from '../post_service';
import openSocket from 'socket.io-client';
import SocialMediaShare from '../../../components/PostsCard/SocialMedia';
// import TopBar from '../../../components/TopBar';
import ReactPlayer from 'react-player';
import { connect } from 'react-redux';
// const { Meta } = Card;
const { TextArea } = Input;
var moment = require('moment');
const { TabPane } = Tabs;

class listAnnouncement extends React.Component {
    state;
    interval;
    constructor(props) {
        super(props);
        this.props = props;
        let { userID } = props
        this.state = {
            user_id: userID,
            loader: true,
            S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,
            createPostFlag: false,
            width: 0,
            announcement_list: [],
            announcement_data: {},
            item_index: 0,
            base_64_preview: undefined,
            files: [],
            _display: false,
            modalvisible: false,
            modalShareVisible: false,
            familyList: [],
            userList: [],
            share_drawer: false,
            share_title: '',
            selectedGrps: [],
            selectedUsrs: [],
            selectedGrpsID: [],
            commentSaving: false
        };
        this.createInterval();
        this.socket = openSocket(process.env.REACT_APP_SOCKET_URL);
        console.log(this.props);
        // socket.emit('subscribeToTimer', 1000);
    }
    createInterval = () => {
        this.interval = setInterval(function () {
            if (this.state.width == 100) {
                this.next();
                this.setState({ width: 0 });

                return;
            }
            this.state.width = this.state.width + 0.5;

            this.setState({ width: this.state.width });
        }.bind(this), 100);
    }
    clearIntervals = () => {
        clearInterval(this.interval);
    }
    componentDidMount() {
        this.getALl();
    }
    prev = () => {
        if ((this.state.item_index == 0)) {
            return;
        }
        this.setState({ _display: false });

        this.state.item_index--;
        this.fetchComment(this.state.announcement_list[this.state.item_index].post_id);
        this.setState({ width: 0, announcement_data: this.state.announcement_list[this.state.item_index] });
        this.clearIntervals();
        this.createInterval();
    }
    close = () => {
        this.props.history.goBack();
    }
    keyPressed = (e) => {
        if (e.key == 'Enter') {
            this.save();
        }
    }
    next = () => {
        if ((this.state.item_index == this.state.announcement_list.length - 1) || !this.state.announcement_list[this.state.item_index]) {
            return;
        }
        this.setState({ _display: false });

        // console.log(this.state.announcement_list[this.state.item_index])
        this.state.item_index++;
        this.fetchComment(this.state.announcement_list[this.state.item_index].post_id);
        this.setState({ width: 0, announcement_data: this.state.announcement_list[this.state.item_index] });
        this.clearIntervals();
        this.createInterval();
    }
    fetchComment = (post_id) => {
        getCommentsByPost({ post_id: post_id.toString(), user_id: this.state.user_id })
            .then(res => {
                this.setState({ comments: res.data.data });
                let announcement_data_ = this.state.announcement_data;
                announcement_data_['conversation_count'] = this.state.comments.length;
                this.setState({ announcement_data: announcement_data_ });
                let channel_name = 'post_channel_' + post_id.toString();
                this.socket.on(channel_name, timestamp => {
                    this.state.comments.push(timestamp[0]);
                    this.setState({ comments: this.state.comments });
                    let _announcement_data = this.state.announcement_data;
                    _announcement_data['conversation_count'] = this.state.comments.length;
                    this.setState({ announcement_data: _announcement_data });
                });
            });
    }
    save = () => {

        // console.log(v);
        // this.state.postList[index].comments.push({
        //     post_id: v.post_id.toString(),
        //     comment: this.state.comment,
        //     commented_by: this.state.user_id
        // });
        // // console.log('timestamp');

        // this.setState({ postList: this.state.postList });

        if (this.state.commentSaving == true) {
            return;
        }
        if (this.state.comment_data && this.state.comment_data.trim() == '' && this.state.base_64_preview == '') {
            return;
        }
        console.log(this.state.comment_data)
        if (!this.state.comment_data && !this.state.base_64_preview) {
            return;
        }
        this.state.commentSaving = true;
        var formData = new FormData();
        formData.append('post_id', this.state.announcement_data.post_id.toString());
        formData.append('group_id', this.state.announcement_data.to_group_id);
        formData.append('comment', this.state.comment_data);
        formData.append('commented_by', this.state.user_id);
        formData.append('file_name', this.state.files);

        updateComment(formData)
            .then(res => {
                this.fetchComment(this.state.announcement_data.post_id);
                this.setState({ base_64_preview: '' });
                this.setState({ comment_data: '', files: '', commentSaving: false });
            })
            .catch(err => {
                this.setState({ commentSaving: false });

            });

    }

    getALl = () => {
        announcement_list({ user_id: this.state.user_id, type: 'announcement', offset: 0, limit: 1000 })
            .then(res => {
                let { location } = this.props;
                res.data.data = res.data.read_announcement.concat(res.data.unread_announcement);
                this.setState({ announcement_list: res.data.data });
                let posData = {}

                if (location && location.state && location.state.fromStory) {
                    let { fromStory } = location.state;
                    let { announcement_list:ann_list } = this.state;
                    ann_list.forEach((value, index) => {
                        if (value.post_id == fromStory.post_id) {
                            this.setState({ announcement_data: res.data.data[index], item_index: index });
                            if (this.state.announcement_data && !this.state.announcement_data.is_active) {
                                this.setState({ modalvisible: true });
                            }
                            posData = res.data.data[index];
                            this.fetchPost(index);
                            this.setDisplay();
                            this.onCommentClick();

                        }
                    })
                }
                else if (res.data.data.length != 0) {
                    this.setState({ announcement_data: res.data.data[0] });
                    posData = res.data.data[0]
                }
                posData && posData.post_id && this.fetchComment(posData.post_id.toString());
            })
    }

    onMessageChange = (e) => {
        // console.log(e.target.value);
        this.setState({ comment_data: e.target.value });
    }
    uploadPhoto = (e) => {
        this.setState({ files: e.target.files[0] })
        this.getBase64(e.target.files[0], imageUrl => {
            this.setState({ base_64_preview: imageUrl }, () => {
                this.save();
            });
        });
    }
    getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }
    menuAction = (key, d) => {
        if (!d) {
            return;
        }
        // console.log(key);
        if (key == '1') {
            mute_conversation({ user_id: this.state.user_id, post_id: d.post_id.toString() })
                .then(result => {
                    message.success((d.muted ? 'UNMUTED' : 'MUTED') + ' SUCESSFULLY')
                    d.muted = !d.muted;
                    this.setState({ postList: this.state.postList });
                })
        } else if (key == '2') {
            if (d.type == 'post') {
                this.props.history.push('/post/create/' + d.post_id);
            } else {
                this.props.history.push('/announcement/create/' + d.post_id, { path_: '/announcement', type_: 'announcement' });

            }
        } else if (key == '3') {
            let { user_id } = this.state;
            deletePost({ id: d.post_id.toString(), user_id: `${user_id}` })
                .then(result => {
                    message.success('REMOVED SUCESSFULLY');
                    this.props.history.goBack();
                    this.setState({ hide: true });
                })

        } else if (key == '4') {
            remove_post({ user_id: this.state.user_id, post_id: d.post_id.toString() })
                .then(result => {
                    message.success('REMOVED SUCESSFULLY');
                    this.setState({ hide: true });
                })
        } else if (key == '5') {
            // spam_report({user_id:this.state.user_id,post_id:d.post_id.toString()})
            // .then(result=>{

            // })
        }

    }
    getMenuTemplate = (d) => {
        return (
            <Menu >
                <Menu.Item key="1" onClick={(e) => this.menuAction(1, d)}>
                    {d.muted ? 'Unmute' : 'Mute'}
                </Menu.Item>
                {
                    (d.created_by == this.state.user_id) &&
                    <Menu.Item key="2" onClick={(e) => this.menuAction(2, d)}>
                        Edit
                </Menu.Item>

                }
                {
                    (d.created_by == this.state.user_id) &&

                    <Menu.Item key="3" onClick={(e) => this.menuAction(3, d)} >
                        Delete
                </Menu.Item>
                }
                {
                    (d.created_by != this.state.user_id) &&
                    <Menu.Item key="4" onClick={(e) => this.menuAction(4, d)}>
                        Hide
                </Menu.Item>
                }

                {
                    (d.created_by != this.state.user_id) &&
                    <Menu.Item key="5" onClick={(e) => this.menuAction(5, d)}>
                        Report
                </Menu.Item>
                }
            </Menu>
        );
    }
    onCommentClick = () => {
        this.clearIntervals();
    }

    fetchPost = (index) => {
        this.setState({ _display: false });
        this.state.item_index = index;
        this.fetchComment(this.state.announcement_list[index].post_id);
        this.setState({ width: 0, announcement_data: this.state.announcement_list[index] });
        this.clearIntervals();
        this.createInterval();
    }

    /**
     * show and hide comments
     */
    setDisplay = () => {
        this.setState({ _display: !this.state._display });
    }
    handleModalOk = () => {
        this.props.history.push('/post');
        this.setState({ modalvisible: false });
    }
    menu = (post_id, type) => {
        return <Menu onClick={e => this.handleMenuClick(e, post_id, type)}>
            <Menu.Item key="1">
                Share in Familheey
          </Menu.Item>
            <Menu.Item key="2">
                Share in Social Media
            </Menu.Item>
        </Menu>
    }

    handleMenuClick = (e, post_id, type) => {
        console.log('click', e, post_id);
        switch (e.key) {
            case "1":
                this.share_post_modal(post_id, type)
                break;
            case "2":
                this.setState({
                    modalShareVisible: true
                })
                break;
        }
    }
    showSocialModal = () => {
        this.setState({
            modalShareVisible: !this.state.modalShareVisible
        })
    }
    share_post_modal = async (post_id, type) => {
        await getFamilyListForPost({ user_id: this.state.user_id, offset: 0, limit: 10 })
            .then(response => {
                this.setState({ familyList: response.data.data });
            });
        user_group_members({ user_id: this.state.user_id })
            .then(response => {
                this.setState({ userList: response.data.data })
            });
        let share_title = '';
        if (type == 'post') {
            share_title = 'Share Post';
        } else {
            share_title = 'Share Announcement';
        }
        this.setState({ share_drawer: true, share_title: share_title });
    }

    onClose = () => {
        this.setState({ share_drawer: false });
    }
    onTabChange = () => {
        this.setState({ selectedUsrs: [], selectedGrps: [], selectedGrpsID: [] });
    }
    selectFamily = (e, family) => {
        if (e.target.checked) {
            this.state.selectedGrps.push({ id: family.id, post_create: family.post_create });
            this.state.selectedGrpsID.push(family.id);
        } else {
            let index = this.state.selectedGrpsID.indexOf(family.id);
            this.state.selectedGrps.splice(index, 1);
            this.state.selectedGrpsID.splice(index, 1);
        }
        this.setState({ selectedGrps: this.state.selectedGrps });
    }
    selectUsers = (e, family) => {
        if (e.target.checked) {
            // this.state.selectedGrps.push({ id: family.id, post_create: family.post_create });
            this.state.selectedUsrs.push(family.user_id);
        } else {
            let index = this.state.selectedUsrs.indexOf(family.user_id);
            // this.state.selectedGrps.splice(index, 1);
            this.state.selectedUsrs.splice(index, 1);
        }
        this.setState({ selectedUsrs: this.state.selectedUsrs });
    }
    share = (d) => {
        post_share({ shared_user_id: this.state.user_id, post_id: (this.state.announcement_data.post_id).toString(), to_group_id: this.state.selectedGrps, to_user_id: this.state.selectedUsrs })
            .then(response => {
                message.success(this.state.announcement_data.type + ' shared successfully ');
                this.onClose();
                // this.setState({ familyList: response.data.data });
            });
    }
    render() {
        return (
            <section className="listAnnouncement f-page">
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <div className="f-post-listing">
                            <Modal
                                cancelButtonProps={{ style: { display: 'none' } }}
                                title="Basic Modal"
                                visible={this.state.modalvisible}
                                onOk={this.handleModalOk}
                            >
                                <p>Oops! The content is no longer available.</p>
                            </Modal>
                            <SocialMediaShare
                                modalVisible={this.state.modalVisible}
                                post_id={this.state.announcement_data.post_id}
                                showSocialModal={this.showSocialModal}
                            />

                            <AnnouncementSidebar
                                history={this.props.history}
                                temIndex={this.state.item_index}
                                data={this.state.announcement_list}
                                on_click={this.fetchPost}
                            />
                            <div className="f-posts-main">
                                {/* Story */}
                                {this.state.announcement_data &&
                                    this.state.announcement_data.is_active &&
                                    this.state.announcement_data.snap_description &&
                                    <div className="f-story-wrap">
                                        <div className="f-story-wrap-progress">
                                            <Progress
                                                percent={this.state.width}
                                            />
                                            <Button
                                                onClick={this.close}
                                            />
                                        </div>
                                        <div className="f-story-wrap-head">
                                            <div className="f-left">
                                                <img
                                                    onError={(e) => { e.target.src = require('../../../assets/images/create_post.png') }}
                                                    src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + this.state.announcement_data.family_logo}
                                                />
                                                <div>
                                                    <h4>{this.state.announcement_data.group_name}</h4>
                                                    <h5>Posted by <strong>{this.state.announcement_data.created_user_name}</strong></h5>
                                                    <h6>{moment(this.state.announcement_data.createdAt).format('MMM DD YYYY  hh:mm A')}</h6>
                                                </div>
                                            </div>
                                            <div className="f-right">
                                                <Button
                                                    className="f-back"
                                                    onClick={() => this.prev()}>
                                                    <i />
                                                    BACK
                                                    </Button>
                                                <Button
                                                    className="f-next"
                                                    onClick={() => this.next()}>
                                                    NEXT <i />
                                                </Button>
                                            </div>
                                        </div>
                                        <div className="f-story-wrap-body">
                                            {
                                                this.state.announcement_data &&
                                                this.state.announcement_data.post_attachment &&
                                                this.state.announcement_data.post_attachment.length != 0 &&
                                                <Carousel>
                                                    {this.state.announcement_data &&
                                                        this.state.announcement_data.post_attachment &&
                                                        this.state.announcement_data.post_attachment.map((v, i) => {
                                                            return (<div key={i}>
                                                                {v.type.indexOf('image') != -1 ?
                                                                    <img ref="image" src={process.env.REACT_APP_S3_BASE_URL + 'post/' + v.filename} />
                                                                    :
                                                                    ''}
                                                                {v.type.indexOf('video') != -1 &&
                                                                    <div
                                                                        className="f-csl-in">
                                                                        <ReactPlayer
                                                                            controls
                                                                            className="f-csl-react-play"
                                                                            url={process.env.REACT_APP_S3_BASE_URL + 'post/' + v.filename} />
                                                                    </div>}
                                                            </div>)
                                                        })
                                                    }
                                                </Carousel>
                                            }
                                            <div className="f-description">
                                                <p>{this.state.announcement_data.snap_description}</p>
                                            </div>
                                        </div>
                                        <div className="f-story-wrap-footer">
                                            {
                                                this.state.announcement_data.conversation_enabled ?
                                                    <div
                                                        className="f-west f-clickable"
                                                        onClick={this.setDisplay}
                                                    >
                                                        <ul>
                                                            <li><span
                                                                className="f-comment"
                                                                onClick={this.onCommentClick}>
                                                                {this.state.announcement_data.conversation_count}
                                                                Conversations{this.state.announcement_data.conversation_count > 0 ? <i /> : ''}
                                                            </span>
                                                            </li>
                                                        </ul>
                                                    </div> :
                                                    <div className="f-west f-clickable">
                                                    </div>
                                            }
                                            <div className="f-east f-west">
                                                <Dropdown
                                                    overlay={this.getMenuTemplate(this.state.announcement_data)}
                                                    placement="bottomLeft">
                                                    <Button />
                                                </Dropdown>
                                                {this.state.announcement_data.is_shareable &&
                                                    <Dropdown
                                                        overlay={() => this.menu(this.state.announcement_data.post_id, this.state.announcement_data.type)}>
                                                        <Button
                                                            className="f-btn-share share-ant-btn"
                                                        />
                                                    </Dropdown>
                                                }
                                            </div>
                                        </div>
                                    </div>}
                                {/* Story END */}

                                {/* Communications */}
                                {this.state.announcement_data && this.state.announcement_data.is_active && this.state.announcement_data.snap_description &&
                                    <div className="f-story-communications " style={{ display: this.state._display ? 'block' : 'none' }}>
                                        <h4>Conversations</h4>
                                        <div className="f-story-communication">
                                            <div className="f-head">
                                                <p>{this.state.announcement_data.conversation_count} new conversations</p>
                                                {/* <Button /> */}
                                            </div>
                                            <div className="f-conversations">
                                                {this.state.comments && this.state.comments.map((msg_, i) => {
                                                    return <div className="f-conversations-singl">
                                                        {
                                                            msg_.commented_by != this.state.user_id &&
                                                            <div className="f-incoming">
                                                                <img className="f-avatar" src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + msg_.propic} />
                                                                <div className="f-group">
                                                                    <header>
                                                                        <h5>{msg_.full_name}</h5>
                                                                        <h5>{moment(msg_.createdAt).fromNow()}</h5>
                                                                    </header>
                                                                    <div className="f-bubble">
                                                                        {msg_.attachment && msg_.attachment.length != 0 && <img src={this.state.S3_BASE_URL + 'file_name/' + msg_.attachment[0].filename} />}
                                                                        <p>{msg_.comment != "undefined" ? msg_.comment : ''}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        }

                                                        {msg_.commented_by == this.state.user_id &&

                                                            <div className="f-outgoing">
                                                                <div className="f-group">
                                                                    <span className="f-time">{moment(msg_.createdAt).fromNow()}</span>
                                                                    <div className="f-bubble">
                                                                        {msg_.attachment && msg_.attachment.length != 0 && <img src={this.state.S3_BASE_URL + 'file_name/' + msg_.attachment[0].filename} />}
                                                                        <p>{msg_.comment != "undefined" ? msg_.comment : ''}</p>
                                                                    </div>
                                                                    {/* <div className="f-bubble">
                                                                        <p>Will meet you on the X-mas eve soon! Take care. Bye for now.</p>
                                                                    </div> */}
                                                                </div>
                                                            </div>
                                                        }
                                                        {this.state.base_64_preview && this.state.base_64_preview != '' && i == (this.state.comments.length - 1) &&
                                                            <div className="f-outgoing">
                                                                <div className="f-group">
                                                                    <span className="f-time">{moment(msg_.createdAt).fromNow()}</span>
                                                                    <div className="f-bubble">
                                                                        <img src={this.state.base_64_preview} />
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        }
                                                    </div>
                                                })}
                                                {/* <div className="f-incoming">
                                                    <img className="f-avatar" src={require('../../../assets/images/demo/m6.png')} />
                                                    <div className="f-group">
                                                        <header>
                                                            <h5>Mark Robertson</h5>
                                                            <h5>10:35am</h5>
                                                        </header>
                                                        <div className="f-bubble">
                                                            <p>Wow! congratulations. God bless you all. Glad to connect with you today.</p>
                                                        </div>
                                                    </div>
                                                </div> */}

                                            </div>
                                            <div className="f-communication-submit">
                                                <div className="f-send">
                                                    <TextArea disabled={this.state.commentSaving} placeholder="Your words mean more. Say something ..." onChange={(e) => this.onMessageChange(e)} onKeyPress={this.keyPressed} value={this.state.comment_data}>{this.state.comment_data}</TextArea>
                                                    {/* <Input placeholder="Your words mean more. Say something ..." value={this.state.comment_data} onChange={(e) => this.onMessageChange(e)} onKeyPress={this.keyPressed} /> */}
                                                    <input id="file-input1" onChange={(event) => this.uploadPhoto(event)} className="hide" type="file" />
                                                    <label for="file-input1" className="f-attach" />
                                                </div>
                                                <Button className="f-submit" onClick={(e) => this.save()} />
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                            <Modal
                                className="f-modal f-modal-wizard f-share-tabbed"
                                title={this.state.share_title}
                                visible={this.state.share_drawer}
                                onCancel={this.onClose}

                            >
                                <div className="f-modal-wiz-body" >
                                    <Tabs defaultActiveKey="1" onChange={this.onTabChange}>
                                        <TabPane tab="FAMILY" key="1">
                                            <div className="f-member-listing">
                                                <div className="f-content-searchable">
                                                    {/* <h4><strong>{this.state.selectedUsers.length}</strong> Users selected</h4> */}
                                                </div>
                                                <div className="f-member-listing-cards f-share-scroll">
                                                    {
                                                        this.state.familyList.map((d, i) => {
                                                            return (<Card key={i} className="f-member-cardx">
                                                                <div className="f-start">
                                                                    <div className="f-left">
                                                                        <img onError={(e) => { e.target.onerror = null; e.target.src = "images/default_logo.png" }}
                                                                            src={d.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + d.logo : require('../../../assets/images/demo/demo-fam1.png')} />
                                                                    </div>
                                                                    <div className="f-right">
                                                                        <h4>{d.group_name}</h4>
                                                                        <h6>By <strong>{d.created_by}</strong></h6>
                                                                        <h6>Member since {d.member_since}</h6>
                                                                        <h6 className="f-pin">{d.base_region}</h6>
                                                                    </div>
                                                                </div>
                                                                <div className="f-stop">
                                                                    <h5>
                                                                        {/* <strong>5K</strong> Members */}
                                                                    </h5>
                                                                    <div className="f-right-combo">
                                                                        {/* <Checkbox onChange={(e) => this.selectFamily(e, d)}>{d.group_name}</Checkbox> */}

                                                                        <Checkbox onChange={(e) => this.selectFamily(e, d)}>
                                                                            {this.state.selectedGrpsID.indexOf(d.id) == -1 ? 'Select' : 'Selected'}
                                                                        </Checkbox>
                                                                    </div>
                                                                </div>
                                                            </Card>);
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        </TabPane>
                                        <TabPane tab="USERS" key="2">
                                            <div className="f-member-listing">
                                                <div className="f-content-searchable">
                                                    {/* <h4><strong>{this.state.selectedUsers.length}</strong> Users selected</h4> */}
                                                </div>
                                                <div className="f-member-listing-cards f-share-scroll">
                                                    {
                                                        this.state.userList.map((user, i) => {
                                                            return (
                                                                <Card key={i} className="f-member-cardx">
                                                                    <div className="f-start">
                                                                        <div className="f-left">
                                                                            <img onError={(e) => { e.target.onerror = null; e.target.src = "images/default_propic.png" }} src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + user.propic} />
                                                                        </div>
                                                                        <div className="f-right">
                                                                            <h4>{user.full_name}</h4>
                                                                            <h4>{user.location}</h4>
                                                                            <h6>Member since {user.member_since}</h6>
                                                                            <div className="f-right-combo">
                                                                                <Checkbox onChange={(e) => this.selectUsers(e, user)} >{this.state.selectedUsrs.indexOf(user.user_id) == -1 ? 'Select' : 'Selected'}</Checkbox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </Card>);
                                                        })
                                                    }
                                                </div>
                                            </div>

                                        </TabPane>
                                    </Tabs>
                                </div>
                                {/* Footer */}
                                <div className="f-modal-wiz-footer">
                                    <Button onClick={(e) => this.share()} style={{ minWidth: 150 }} >Share <Icon type="share-alt" /></Button>
                                </div>
                            </Modal>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => {
    let { userId } = state.user_details;
    return {
        userID: userId
    }
}
export default connect(mapStateToProps, null)(listAnnouncement)