import React, { useState, useEffect } from 'react'
import { Modal, Button, Input, Checkbox, Card } from 'antd'
import { getFamilyListForPost } from '../../../family/familyList/api-familyList'
import {useSelector} from 'react-redux';


const { Search } = Input
const GroupModal = ({ family_modal, toggleGroupModal, fetchSelectedIDS, _groupList,setTagArrayFromList }) => {
    const [modalState, setModalState] = useState({
        selectedGroupIDS: [],
        selectedGroups: [],
        groupList: undefined,
        user_id:useSelector(state=>state.user_details.userId)
    })
    let { selectedGroupIDS, groupList, selectedGroups,user_id } = modalState;
    useEffect(() => {
        getAllFamily()
    }, [])

    const getAllFamily=(query=null)=>{
        getFamilyListForPost({ user_id:user_id,query:query }).then(res => {
            setModalState(prev => ({ ...prev, groupList: res.data.data }))
        })
    }
    useEffect(() => {
        let _dummy = [...selectedGroups]
        _dummy = _dummy.filter(item => _groupList.includes(item.id))
        setModalState(prev => ({ ...prev, selectedGroupIDS: _groupList, selectedGroups: _dummy }))
    }, [_groupList])

    useEffect(() => {
        if (groupList) {
            let __dummy = groupList && groupList.filter(item => (_groupList||[]).some(_item=>_item.id == item.id))
            _groupList.length>0&&setTagArrayFromList(__dummy)
        }
    }, [groupList, _groupList])
    
    const handleChooseFamily = (event, item) => {
        let { id, group_name, post_create } = item;
        let _newArray = [...selectedGroupIDS];
        let _newArrayWithName = [...selectedGroups]
        switch (event.target.checked) {
            case true:
                !_newArray.some(item1 => item1.id == id && item1.post_create == post_create) && _newArray.push({ id: id, post_create: post_create })
                !_newArrayWithName.some(item2 => item2.id == id && item2.group_name == group_name) && _newArrayWithName.push({ id: id, group_name: group_name })
                break;
            default:
                _newArray = _newArray.filter((item3) => item3.id != id)
                _newArrayWithName = _newArrayWithName.filter(item4 => item4.id != id && item4.group_name != group_name)
                break;
        }
        setModalState(prev => ({ ...prev, selectedGroupIDS: _newArray, selectedGroups: _newArrayWithName }))
    }

    return (
        <Modal title="Select Families"
            visible={family_modal}
            onCancel={() => toggleGroupModal('family_modal')}
            className="f-modal f-modal-wizard"
        >
            <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                <div className="f-member-listing">
                    <div className="f-content-searchable">
                        <h4><strong>{selectedGroupIDS&&selectedGroupIDS.length}</strong> Families selected</h4>
                        <Search placeholder="Discover" onChange={(e)=>getAllFamily(e.target.value)}/>
                    </div>
                    <div className="f-member-listing-cards f-share-scroll">
                        {
                            groupList && groupList.map((v, index) => {
                                return (
                                    <Card key={index} className="f-member-cardx">
                                        <div className="f-start">
                                            <div className="f-left">
                                                <img src={v.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + v.logo : require('../../../../assets/images/create_post.png')} />
                                            </div>
                                            <div className="f-right">
                                                <h4>{v.group_name}</h4>
                                                <h6>By <strong>{v.created_by}</strong></h6>
                                                <h6>Member since {v.member_since}</h6>
                                                <h6 className="f-pin">{v.base_region}</h6>
                                            </div>
                                        </div>
                                        <div className="f-stop">
                                            <div className="f-right-combo">
                                                <Checkbox
                                                    onChange={(e) => handleChooseFamily(e, v)}
                                                    checked={(selectedGroupIDS&&selectedGroupIDS.some(item => item.id == v.id && item.post_create == v.post_create) ? true : false)}
                                                >
                                                    {selectedGroupIDS&&selectedGroupIDS.some(item => item.id == v.id && item.post_create == v.post_create) ? 'Selected' : 'Select'}
                                                </Checkbox>
                                            </div>
                                        </div>
                                    </Card>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
            <div className="f-modal-wiz-footer f-flx-end">
                <Button onClick={() => toggleGroupModal('family_modal')}>Cancel</Button>
                <Button style={{ marginLeft: '15px' }} className="f-fill" onClick={() => fetchSelectedIDS(selectedGroupIDS, selectedGroups, 'family_modal')}>Done</Button>
            </div>
        </Modal>
    )
}

export default GroupModal;