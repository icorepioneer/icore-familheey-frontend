import React, { useState, useEffect } from 'react'
import { Modal, Card, Button, Input, Checkbox } from 'antd'
import { user_group_members } from '../../../Post/post_service'
import { useSelector} from 'react-redux'
const { Search } = Input
const UserModal = ({ user_modal, toggleUserModal, fetchSelectedIDS,_userList }) => {
    const [userState, setUserState] = useState({
        selectedUserIDS: [],
        selectedUsers: [],
        userList: undefined,
        user_id:useSelector(state=>state.user_details.userId)
    })

    let { selectedUserIDS, selectedUsers, userList,user_id } = userState
    useEffect(() => {
        user_group_members({ user_id: user_id }).then(res => {
            setUserState(prev => ({ ...prev, userList: res.data.data }))
        })
    }, [])
    useEffect(()=>{
        let dummyArray = [...selectedUsers]
        dummyArray = dummyArray.filter((item)=>_userList.includes(item.id))
        setUserState(prev=>({...prev,selectedUserIDS:_userList,selectedUsers:dummyArray}))
    },[_userList])
    const handleChooseUserList = (event, item) => {
        let { user_id:U_ID, full_name } = item;
        let _newArray = [...selectedUserIDS]
        let _newArrayWithName = [...selectedUsers]
        switch (event.target.checked) {
            case true:
                !_newArray.includes(U_ID) && _newArray.push(U_ID)
                !_newArrayWithName.some(item1 => item1.id == U_ID && item1.full_name == full_name) && _newArrayWithName.push({ id: U_ID, full_name: full_name })
                break;
            default:
                _newArray = _newArray.filter((item2) => item2 != U_ID)
                _newArrayWithName = _newArrayWithName.filter(item3 => item3.user_id != U_ID && item3.full_name != full_name)
                break;
        }
        setUserState(prev => ({ ...prev, selectedUserIDS: _newArray, selectedUsers: _newArrayWithName }))
    }
    return (
        <Modal title="Select Connections"
            visible={user_modal}
            onCancel={() => toggleUserModal('user_modal')}
            // onCancel={() => this.sh_handleCancel()}
            // onOk={() => this.sh_handleCancel()}
            className="f-modal f-modal-wizard">
            <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                <div className="f-member-listing">
                    <div className="f-content-searchable">
                        <h4><strong>{selectedUserIDS&&selectedUserIDS.length}</strong> Connections selected</h4>
                        <Search placeholder="Discover" />
                    </div>
                    <div className="f-member-listing-cards f-share-scroll">
                        {
                            userList && userList.map(user => {
                                return (<Card key={user.user_id} className="f-member-cardx">
                                    <div className="f-start">
                                        <div className="f-left">
                                            <img onError={(e) => { e.target.src = require('../../../../assets/images/profile-pic.png') }} src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + user.propic} />
                                        </div>
                                        <div className="f-right">
                                            <h4>{user.full_name}</h4>
                                            <h4>{user.location}</h4>
                                            <h6>Member since {user.member_since}</h6>
                                            <div className="f-right-combo">
                                                <Checkbox
                                                    onChange={(e) => handleChooseUserList(e, user)}
                                                    checked={(selectedUserIDS&&selectedUserIDS.includes(user.user_id) ? true : false)}
                                                >
                                                    {selectedUserIDS&&selectedUserIDS.includes(user.user_id) ? 'Selected' : 'Select'}</Checkbox>
                                            </div>
                                        </div>
                                    </div>
                                </Card>)
                            })
                        }
                    </div>
                </div>
            </div>
            <div className="f-modal-wiz-footer f-flx-end">
                <Button onClick={() => toggleUserModal('user_modal')}>Cancel</Button>
                <Button style={{ marginLeft: '15px' }} className="f-fill" onClick={() => fetchSelectedIDS(selectedUserIDS, selectedUsers, 'user_modal')}>Done</Button>
            </div>
        </Modal>
    );
}
export default UserModal