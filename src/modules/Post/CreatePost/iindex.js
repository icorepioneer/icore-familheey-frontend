import React, { useState, useEffect } from 'react'
import { Switch, Spin, Button, Input, Icon, Select, Tag, Form } from 'antd'
import GroupModal from './components/groups'
import UserModal from './components/users'
import { upload_file_tos3 } from '../../family/familyList/api-familyList'
import { createPost, updatePost, getByID, copyBetweenBucket } from '../post_service';
import { useSelector } from 'react-redux'

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const { TextArea } = Input
const { Option } = Select
const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(

    class extends React.Component {

        state = {
            _loading: false,
        }
        render() {
            const { form, loader, history, page_title, handleOptionSelect, drop_options, page_type, tagArray
                , removeIDFromArray, onfieldChange, snap_description, uploadFile, post_attachment, removeAttatchedFile,
                conversation_enabled, handleSelect, savePost, image_uploading, image_loader, path_, type_,
                initial_value, is_shareable, content_type, disable_functions, save_button_loader } = this.props;
            const { getFieldDecorator } = form;
            return (<section className="CreatePost f-page">
                <div className="f-clearfix f-body">
                    {
                        loader && <Spin
                            tip="Loading..."
                            style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}
                        >
                        </Spin>
                    }
                    {!loader &&
                        <div className="f-boxed">
                            <Button className="f-create-form-back" onClick={() => history.push(`${path_}`)} />
                            <div className="f-clearfix f-create-form-control">
                                <div className="f-create-form-head">
                                    <h3> {`${page_title} ${type_ == 'post' ? 'Post' : 'Announcements'}`}</h3>
                                </div>
                                <div className="f-create-form-body" >
                                    <Form layout="vertical">
                                        {
                                            <React.Fragment>
                                                <div className="f-form-control">
                                                    {
                                                        !(page_type == 'update' && content_type == 'announcement') && <Form.Item
                                                            label={`${type_ == 'post' ? 'Post' : 'Announce '} this to`}>
                                                            {getFieldDecorator('post_type', { initialValue: `${initial_value}` })(
                                                                <Select
                                                                    disabled={disable_functions}
                                                                    placeholder="Select"
                                                                    style={{ width: '100%' }}
                                                                    onSelect={e => handleOptionSelect(e)}
                                                                >
                                                                    {drop_options.map(({ label, value, used }) => { return used.includes(`${type_ ? type_ : 'post'}`) && <Option key={value}>{label}</Option> })}
                                                                </Select>
                                                            )}
                                                        </Form.Item>
                                                    }
                                                </div>
                                                {(initial_value == 'only_groups' || initial_value == 'only_users') &&
                                                    <div
                                                        style={{ marginTop: '10px' }}>{tagArray && tagArray.map((item, key) => {
                                                            return <Tag
                                                                disabled={disable_functions}
                                                                key={key}
                                                                closable
                                                                color="#7e57c2"
                                                                onClose={(e) => removeIDFromArray(item, e)}
                                                            >{item.group_name || item.full_name}
                                                            </Tag>
                                                        })}</div>
                                                }
                                                <div className="f-form-control">
                                                    <Form.Item
                                                        style={{ marginBottom: '0px' }}
                                                        label={`What do you want to ${type_ == 'post' ? 'post' : 'announce '}?`}>
                                                        {getFieldDecorator('snap_description', {
                                                            rules: [{ required: true, message: 'Description missing' }],
                                                            initialValue: snap_description
                                                        })(
                                                            <TextArea
                                                                placeholder=""
                                                                rows={6}
                                                                onChange={(e) => onfieldChange(e, 'snap_description')}
                                                            />
                                                        )}
                                                    </Form.Item>
                                                    <span className="f-tip"><i>Tip:</i> You can include # tags as well</span>
                                                </div>
                                                <div className="f-form-control">
                                                    <label>Post Photos/Videos (Optional)</label>
                                                    <input
                                                        id="file-input"
                                                        accept=".mp4,.mkv,.jpg,.jpeg,.png,.gif,.avi,.pdf,.doc,.docx,.rtf,.odt"
                                                        multiple
                                                        onChange={async (event) => await uploadFile(event)}
                                                        className="hide"
                                                        style={{ display: 'none' }}
                                                        type="file" />
                                                    <ul className="f-upload-setup">
                                                        <li className="f-first"><label htmlFor="file-input"><img src={require('../../../assets/images/icon/attachment.png')} /></label></li>
                                                        {post_attachment && post_attachment.map((img, i) => {
                                                            return (JSON.stringify(img.type).indexOf('video') != -1 ?
                                                                <li
                                                                    key={i} >{
                                                                        img.image_loader && <Spin
                                                                            indicator={antIcon}
                                                                        />}
                                                                    <img
                                                                        src={require('../../../assets/images/icon/land10.png')}
                                                                    />{!img.image_loader && <Button
                                                                        onClick={() => removeAttatchedFile(img.index)}
                                                                    />}</li>
                                                                :
                                                                (JSON.stringify(img.type).indexOf('application') != -1 ? <li key={i}>
                                                                    {
                                                                        img.image_loader && <Spin
                                                                            indicator={antIcon}
                                                                        />}
                                                                    <img
                                                                        src={require('../../../assets/images/icon/doc_b.png')}
                                                                    />{!img.image_loader && <Button
                                                                        onClick={() => removeAttatchedFile(img.index)}
                                                                    />}
                                                                </li> : <li
                                                                    key={i} >
                                                                        {img.image_loader && <Spin
                                                                            indicator={antIcon}
                                                                        />}<img
                                                                            src={img.base_64 ? img.base_64 : process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'post/' + img.filename} />{!img.image_loader && <Button
                                                                                onClick={() => removeAttatchedFile(img.index)}
                                                                            />}</li>))
                                                        })}
                                                    </ul>
                                                </div>
                                                <div className="f-form-control f-wiz-switch">
                                                    <div style={{ display: 'flex', justifyContent: 'space-between', height: '40px' }}>
                                                        <h6>Enable sharing</h6>
                                                        <Form.Item>{getFieldDecorator('is_shareable')(<Switch
                                                            checked={is_shareable}
                                                            onChange={() => handleSelect('is_shareable')}
                                                        />)}</Form.Item>
                                                    </div>
                                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                        <h6>Enable conversations</h6>
                                                        <Form.Item>
                                                            {getFieldDecorator('conversation_enabled')(<Switch
                                                                checked={conversation_enabled}
                                                                onChange={() => handleSelect('conversation_enabled')} />)}</Form.Item>
                                                    </div>
                                                </div>
                                                <div className="f-create-form-footer" style={{ padding: '0px' }}>
                                                    {!image_loader && <Button
                                                        loading={save_button_loader}
                                                        disabled={image_uploading}
                                                        className="f-submit"
                                                        onClick={() => savePost()}>{page_type == 'create' ? 'POST' : 'UPDATE'}</Button>}
                                                </div>
                                            </React.Fragment>
                                        }
                                    </Form>
                                </div>
                            </div>
                        </div>

                    }
                </div>
            </section>
            );
        }
    },
);


const CreatePost = ({ history, location }) => { 
    let { description } = location && location.state;
    let hrefis = window.location.href;
    let splitvalue = hrefis.split(`${window.origin}/`)[1].split("/")[0]; 
    let formReff;
    const [postState, setCreatePost] = useState({
        loader: false,
        page_title: 'Create',
        page_type: 'create',
        post_type: 'public',
        //group-modal
        groupList: [],
        userList: [],
        family_modal: false,
        user_modal: false,
        snap_description: (description) ? description: '',
        post_attachment: [],
        post_is_sharable: false,
        image_uploading: false,
        conversation_enabled: true,
        tagArray: undefined,
        //user-modal
        //user_modal: false,
        current_option: 'public',
        _description: '',
        image_loader: false,
        file_list_to_upload: [],
        //post_model
        user_id: useSelector(state => state.user_details.userId),
        created_by: useSelector(state => state.user_details.userId),
        category_id: 3,
        privacy_type: 'private',
        is_shareable: true,
        post_info: {},
        post_id: '',
        path_: '',
        type_: 'post',
        item_to_update_id: undefined,
        item_to_update: undefined,
        post_ref_id: undefined,
        drop_options: [
            {
                label: "All My Families",
                value: "all_family",
                type: 'all',
                used: ['announcement']
            },
            {
                label: "Everyone(Public)",
                value: "public",
                type: 'post',
                used: ['post']
            },
            {
                label: "Selected families",
                value: "only_groups",
                type: 'all',
                used: ['post', 'announcement']
            }, 
            // {
            //     label: "Selected Connections",
            //     value: "only_users",
            //     type: 'post',
            //     used: ['post']
            // }, 
            // {
            //     label: "Anyone",
            //     value: "public",
            //     type: 'post',
            //     used: ['post']
            // },
            {
                label: "Only me",
                value: "private",
                type: 'post',
                used: ['post']
            }
        ],
        initial_value: (splitvalue && splitvalue=='post') ? 'public' : 'all_family',
        update_status: false,
        delete_post: [],
        old_fam_list: undefined,
        inactive_active_array: undefined,
        content_type: undefined,
        groupId: undefined,
        disable_functions: false,
        save_button_loader: false,
    })
    let { loader, page_title, post_type, page_type, groupList, snap_description, post_attachment,
        image_uploading, post_is_sharable, conversation_enabled, family_modal, tagArray, user_modal,
        current_option, userList, file_list_to_upload, image_loader, created_by, category_id,
        is_shareable, post_info, post_id,  path_, type_, item_to_update_id, item_to_update, drop_options,
        update_status, initial_value, post_ref_id, user_id, delete_post, old_fam_list, inactive_active_array, content_type, disable_functions,
        save_button_loader
    } = postState
    const handleOptionSelect = (event) => {
        switch (event) {
            case 'only_groups':
                setCreatePost(prev => ({
                    ...prev, family_modal: !family_modal,
                    tagArray: current_option == event ? tagArray : undefined,
                    current_option: event,
                    initial_value: event,
                    userList: [],
                    post_type: event
                }))
                break;
            case 'only_users':
                setCreatePost(prev => ({
                    ...prev,
                    groupList: [],
                    tagArray: current_option == event ? tagArray : undefined,
                    user_modal: !user_modal, current_option: event,
                    post_type: event,
                    initial_value: event
                }))
                break;
            default:
                setCreatePost(prev => ({ ...prev, tagArray: undefined, post_type: event, initial_value: event }))
                break;
        }
    }
    const toggleModal = (tag) => {
        setCreatePost(prev => ({ ...prev, [tag]: !tag }))
    }
    const fetchSelectedIDS = (idArray, withNames, tag) => {
        switch (tag) {
            case 'family_modal':
                setCreatePost(prev => ({ ...prev, groupList: idArray, tagArray: withNames }))
                break;
            case 'user_modal':
                setCreatePost(prev => ({ ...prev, userList: idArray, tagArray: withNames }))
                break;
        }
        toggleModal(tag)
    }
    const removeIDFromArray = (item, e) => {
        e.persist()
        e.preventDefault()
        let _tagArray, _idArray
        let _delete_post_array = delete_post || []
        let { id, group_name, full_name } = item;
        switch (current_option) {
            case 'only_groups':
                _tagArray = tagArray
                _idArray = groupList
                _tagArray = _tagArray.filter(item1 => item1.id != id && item1.group_name != group_name)
                _idArray = _idArray.filter(item2 => item2.id != id)
                _delete_post_array.push(id)
                setCreatePost(prev => ({ ...prev, tagArray: _tagArray, groupList: _idArray, delete_post: _delete_post_array }))
                break;
            case 'only_users':
                _tagArray = tagArray
                _idArray = userList
                _tagArray = _tagArray.filter(item3 => item3.id != id && item3.full_name != full_name)
                _idArray = _idArray.filter(item4 => item4 != id)
                setCreatePost(prev => ({ ...prev, tagArray: _tagArray, userList: _idArray }))
                break;
        }
    }
    const onfieldChange = (event, tag) => {
        event.persist();
        setCreatePost(prev => ({ ...prev, [tag]: event.target.value }))
    }
    const uploadFile = async (e) => {
        let _files = e.target.files
        let files_to_display = post_attachment || [];
        let _file_list_to_upload = []
        for (let file of _files) {
            let { type } = file
            let keyID = Math.random().toString(36).substring(7);
            const reader = new FileReader()
            reader.readAsDataURL(file)
            reader.addEventListener('load', () => {
                files_to_display.push({ base_64: reader.result, image_loader: true, index: keyID, type: type })
                setCreatePost(prev => ({ ...prev, post_attachment: files_to_display }))
            })
            await getBase64(file, _file_list_to_upload, keyID, (imageURL, file_list) => {
                file_list = [...file_list, ...file_list_to_upload]
                let _files_to_display = files_to_display.filter(item => item.index == keyID)
                _files_to_display[0].image_loader = false
                let files_to_display_ = files_to_display.filter(item => item.index != keyID)
                files_to_display = [..._files_to_display, ...files_to_display_]
                setCreatePost(prev => ({ ...prev, post_attachment: files_to_display, file_list_to_upload: file_list }))
            })
        }
    }
    const getBase64 = async (_files, _file_list_to_upload, key, callback) => {
        const formData = new FormData();
        formData.append('file', _files);
        let {name} = _files
        formData.append('name', 'post')
        await upload_file_tos3(formData).then(res => {
            console.log(res.data)
            let { type, filename } = res.data.data[0]
            const reader = new FileReader()
            reader.readAsDataURL(_files)
            reader.onload = (e) => {
                if (JSON.stringify(e.target.result).indexOf('video') != -1) {
                    _file_list_to_upload.push({ type: type, filename: filename, index: key })
                    callback(e.target.result, _file_list_to_upload)
                }
                else if (JSON.stringify(e.target.result).indexOf('application') != -1) {
                    _file_list_to_upload.push({ type: type, filename: filename,original_name:name,index: key })
                    callback(e.target.result, _file_list_to_upload)
                }
                else {
                    let image = new Image();
                    image.src = e.target.result;
                    image.onload = function () {
                        _file_list_to_upload.push(
                            { type: type, filename: filename, height: this.height, width: this.width, index: key }
                        )
                        callback(e.target.result, _file_list_to_upload)
                    }
                }
            }
        })

    }
    const removeAttatchedFile = (index) => {
        let uploadFiles = [...file_list_to_upload]
        let attachment_list = [...post_attachment]
        attachment_list = attachment_list.filter(file => file.index != index)
        uploadFiles = uploadFiles.filter(file => file.index != index)
        setCreatePost(prev => ({ ...prev, post_attachment: attachment_list, file_list_to_upload: uploadFiles }))
    }
    const handleSelect = (tag) => {
        setCreatePost(prev => ({ ...prev, [tag]: !postState[tag] }))
    }

    const saveFormRef = (formRef) => {
        formReff = formRef;
    };

    const savePost = () => {
        const { form } = formReff.props;
        setCreatePost(prev => ({ ...prev, save_button_loader: !save_button_loader }))
        form.validateFields((err, values) => {
            if (err) {
                setCreatePost(prev => ({ ...prev, save_button_loader: false }))
                return;
            }
            let params = {
                ...values,
                post_attachment: file_list_to_upload.filter(item => !item.hasOwnProperty('base_64')),
                created_by: created_by,
                category_id: `${category_id}`,
                privacy_type: post_type == 'private' ? 'private' : 'public',
                post_type: post_type,
                post_info: post_info,
                conversation_enabled: conversation_enabled,
                is_shareable: is_shareable,
                post_id: `${post_id}`,
                selected_groups: groupList.length > 0 ? groupList : undefined,
                selected_users: userList.length > 0 ? userList : undefined,
                type: type_
            }
            update_status ? (_updatePost(params)) : (createPost(params).then(res => {
                let {type:post_created_type,id:ann_id} = res.data.data
                post_created_type==="announcement"? history.push('/individualStatus', { fromStory: { post_id: ann_id, prev_url: `/announcement` } }):
                (post_type == 'public' ? history.push(`${path_}`, { type_id: 2 }) : history.push(`${path_}`))
                setCreatePost(prev => ({ ...prev, save_button_loader: !save_button_loader }))
            }).catch(err1 => {
                console.log(err1)
            }))
        });
    }
    const _updatePost = (params) => {
        let { 
            // id,
            //  category_id,
            post_type:PT,
            // post_info, 
            selected_groups, ...update } = params
        let update_type = PT == 'only_groups' ? 'multiple' : 'single'
        let _selected_groups = []
        PT == 'only_groups' && (selected_groups || []).forEach(item => {
            old_fam_list.some(fam => fam.id == item.id) ? _selected_groups.push({ ...item, type: 'old' }) 
            : _selected_groups.push({ ...item, type: 'new' })
        })
        let update_params = {
            ...update,
            to_group_id_array: _selected_groups,
            update_type: update_type,
            id: `${post_id}`,
            post_ref_id: post_ref_id,
            user_id: user_id,
            type_id: `${post_id}`,
            delete_post: delete_post,
            post_type: PT,
            inactive_active_array: inactive_active_array,
        }
        updatePost(update_params).then(res => {
            PT == 'public' ? history.push(`${path_}`, { type_id: 2, path: window.location.pathname }) 
            : history.push(`${path_}`, { path: window.location.pathname })
            setCreatePost(prev => ({ ...prev, save_button_loader: !save_button_loader }))
        })
        return true
    }   
    useEffect(() => {
        let { path_:Path_, type_:Type_, groupId, group_name, from_topic, selected } = location.state
        if (groupId) {
            let grp_data = [{ id: groupId, post_create: 6 }]
            let group_tag = [{ id: groupId, group_name: group_name }]
            setCreatePost(prev => ({
                ...prev,
                initial_value: 'only_groups',
                drop_options: drop_options.filter(item => item.value == 'only_groups'),
                disable_functions: true,
                tagArray: group_tag,
                groupList: grp_data,
                post_type: 'only_groups'
            }))
        }
        else if (from_topic) {
            let { comment, attachment } = selected
            setCreatePost(prev => ({ ...prev, snap_description: comment == 'null' ? '' : comment }))
            if (attachment.length > 0) {
                let { filename, type } = attachment[0]
                let params = {
                    source_file: `file_name/${filename}`,
                    destination_file: `post/${filename}`
                }
                copyBetweenBucket(params).then(res => {
                    let keyID = Math.random().toString(36).substring(7);
                    let arrayWithFile = [{ type: type, filename: filename, index: keyID }]
                    setCreatePost(prev => ({ ...prev, post_attachment: arrayWithFile, file_list_to_upload: arrayWithFile }))
                })

            }

        }
        setCreatePost(prev => ({ ...prev, path_: Path_, type_: Type_, groupId: groupId ? groupId : null }))
    }, [location])
    //updates
    useEffect(() => {
        let path_array = window.location.pathname.split('/')
        let _item_to_update_id = path_array[3] ? path_array[3] : undefined
        setCreatePost(prev => ({ ...prev, item_to_update_id: _item_to_update_id }))
    }, [])
    useEffect(() => {
        item_to_update_id && getByID({ id: item_to_update_id }).then(res => {
            setCreatePost(prev => ({ ...prev, item_to_update: res.data.data[0] }))
        })
    }, [item_to_update_id])
    useEffect(() => {
        if (item_to_update) {
            let { id, snap_description:SD, post_attachment:p_a, is_shareable:isShare, conversation_enabled:conv_en, type, familyList, post_type:p_t, post_ref_id:pr_id } = item_to_update
            // const { form } = formReff.props;
            let _inactive_active_array = []
            familyList.map(item => {
                _inactive_active_array.push(item.id)
            })
            setCreatePost(prev => ({ ...prev, current_option: p_t, post_type: p_t, initial_value: p_t, drop_options: drop_options.filter(item => item.value == p_t) }))
            setCreatePost(prev => ({
                ...prev,
                post_id: id,
                groupList: familyList,
                old_fam_list: familyList,
                snap_description: SD,
                is_shareable: isShare,
                conversation_enabled: conv_en,
                post_attachment: p_a,
                file_list_to_upload: p_a,
                update_status: true,
                post_ref_id: pr_id,
                page_title: 'Update',
                page_type: 'update',
                inactive_active_array: _inactive_active_array,
                content_type: type
            }))
        }
    }, [item_to_update])

    const setTagArrayFromList = (array) => {
        setCreatePost(prev => ({ ...prev, tagArray: array }))
    }
    return (
        <React.Fragment>
            <CollectionCreateForm
                wrappedComponentRef={(formRef) => saveFormRef(formRef)}
                loader={loader}
                history={history}
                page_title={page_title}
                handleOptionSelect={handleOptionSelect}
                drop_options={drop_options}
                page_type={page_type}
                post_type={post_type}
                tagArray={tagArray}
                removeIDFromArray={removeIDFromArray}
                snap_description={snap_description}
                onfieldChange={onfieldChange}
                uploadFile={uploadFile}
                post_attachment={post_attachment}
                removeAttatchedFile={removeAttatchedFile}
                post_is_sharable={post_is_sharable}
                handleSelect={handleSelect}
                conversation_enabled={conversation_enabled}
                image_loader={image_loader}
                image_uploading={image_uploading}
                savePost={savePost}
                path_={path_}
                type_={type_}
                initial_value={initial_value}
                is_shareable={is_shareable}
                content_type={content_type}
                disable_functions={disable_functions}
                save_button_loader={save_button_loader}
            />
            <GroupModal
                fetchSelectedIDS={fetchSelectedIDS}
                family_modal={family_modal}
                toggleGroupModal={toggleModal}
                _groupList={groupList}
                setTagArrayFromList={setTagArrayFromList}
            />
            <UserModal
                fetchSelectedIDS={fetchSelectedIDS}
                user_modal={user_modal}
                toggleUserModal={toggleModal}
                _userList={userList}
            />
        </React.Fragment>

    );
}

export default CreatePost;