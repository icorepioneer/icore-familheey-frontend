import React from 'react';
// import { withRouter } from 'react-router-dom';
import './style.scss'
import { Radio, Button, Icon, Card, Input, Drawer, Skeleton } from "antd";
import PostsSidebar from '../../../components/PostsSidebar';
import PostsCard from '../../../components/PostsCard';
import { postList, postLike, updateComment, publicFeed, getCommentsByPost } from '../post_service';
import openSocket from 'socket.io-client';
import { connect } from 'react-redux'

const { TextArea } = Input;
const { Search } = Input
var moment = require('moment');
const limit = 25;
class listLanding extends React.Component {
    state;
    offset = 0;
    dataCompleted = 0;
    constructor(props) {
        super(props);
        this.props = props;
        let { userID } = props
        this.state = {
            familyDetails: '',
            imgFile: '',
            user_id: userID,
            loader: true,
            postList: [],
            visible: [],
            S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,
            createPostFlag: false,
            current_tab: '',
            searchText: ''
        };
        this.socket = openSocket(process.env.REACT_APP_SOCKET_URL);
    }

    componentDidMount() {
        if (this.props.location.state && this.props.location.state.type_id == 2) {
            this.setState({ current_tab: 'public' });
            this.publicFeed();
        } else {
            this.setState({ current_tab: 'all' });
            this.getALl();
        }
        window.addEventListener("scroll", this.handleScroll);
    }
    listData = () => {
        if (this.state.current_tab == 'public') {
            this.publicFeed();
        } else {
            this.getALl();
        }
    }
    handleScroll = () => {
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        const windowBottom = windowHeight + window.pageYOffset;
        docHeight = docHeight - (0.4 * docHeight);
        if (windowBottom >= docHeight && !this.state.loader && this.dataCompleted == 0) {
            this.listData();
        }
    }
    getALl = (searchText = '') => {
        this.state.loader = true;
        this.setState({ loader: true });
        if (this.offset == 0) {
            this.setState({ postList: [] });
        }
        postList({ user_id: this.state.user_id, type: 'post', offset: this.offset, limit: limit, query: searchText })
            .then(res => {
                if (this.state.current_tab == 'all') {
                    if (res.data && res.data.data) {
                        let data_count = res.data.data.length;
                        if (data_count > 0) {
                            if (this.offset == 0) {
                                this.offset = data_count;
                                this.setState({ postList: res.data.data, loader: false });
                            } else {
                                this.offset += data_count;
                                this.state.postList = this.state.postList.concat(res.data.data);
                                this.setState({ postList: this.state.postList, loader: false });
                            }
                            if (data_count < limit) {
                                this.dataCompleted = 1;
                            }
                        } else {
                            this.dataCompleted = 1;
                            this.setState({ loader: false });
                        }
                    } else {
                        this.dataCompleted = 1;
                        this.setState({ loader: false });
                    }
                }
            });
        // announcement_list({ user_id: this.state.user_id, type: 'announcement', offset: 0, limit: 5 })
        //     .then(res => {
        //         this.setState({ announcement_list: res.data.data });
        //     })
    }

    publicFeed = (searchText = '') => {
        this.state.loader = true;
        this.setState({ loader: true });
        if (this.offset == 0) {
            this.setState({ postList: [] });
        }
        publicFeed({ user_id: this.state.user_id, type: 'post', offset: this.offset, limit: limit, query: searchText })
            .then(res => {
                if (this.state.current_tab == 'public') {
                    if (res.data && res.data.data) {
                        let data_count = res.data.data.length;
                        if (data_count > 0) {
                            if (this.offset == 0) {
                                this.offset = data_count;
                                this.setState({ postList: res.data.data, loader: false });
                            } else {
                                this.offset += data_count;
                                this.state.postList = this.state.postList.concat(res.data.data);
                                this.setState({ postList: this.state.postList, loader: false });
                            }
                            if (data_count < limit) {
                                this.dataCompleted = 1;
                            }
                        } else {
                            this.dataCompleted = 1;
                            this.setState({ loader: false });
                        }
                    } else {
                        this.dataCompleted = 1;
                        this.setState({ loader: false });
                    }
                }
                // this.setState({ postList: res.data.data, loader: false });
            });
    }

    onTabChange = (e) => {
        this.setState({ postList: [], loader: true })
        if (e.target.value == 'public') {
            this.dataCompleted = 0;
            this.offset = 0;
            this.publicFeed();
            this.setState({ current_tab: 'public' });
        } else {
            this.dataCompleted = 0;
            this.offset = 0;
            this.getALl();
            this.setState({ current_tab: 'all' });
        }
    }

    like = (value, index) => {
        postLike({
            post_id: value.post_id.toString(),
            user_id: this.state.user_id
        }).then(res => {
            this.state.postList[index].like_status = !this.state.postList[index].like_status;
            this.setState({ postList: this.state.postList });
        })
    }

    handleKeyPress = (value, index) => { }

    comment = (value, index) => {
        this.state.visible[index] = !this.state.visible[index];
        getCommentsByPost({ post_id: value.post_id.toString(), user_id: this.state.user_id })
            .then(res => {
                this.state.postList[index].comments = res.data.data;
                this.setState({ postList: this.state.postList });
            });
        let channel_name = 'post_channel_' + value.post_id;
        this.socket.on('notification_' + this.state.user_id, message => {
            console.log('notification', message);
        });
        this.socket.on(channel_name, timestamp => {
            this.state.postList[index].comments.push(timestamp);
            console.log('timestamp');
            this.setState({ postList: this.state.postList });
        });
        this.setState({ visible: this.state.visible });
    }

    createPost = () => {
        this.props.history.push('/post/create', { path_: window.location.pathname, type_: 'post' });
    }

    onClose = (index) => {
        this.state.visible[index] = !this.state.visible[index];
        this.setState({ visible: this.state.visible });
    }

    save = (v, index) => {
        var formData = new FormData();
        formData.append('post_id', v.post_id.toString());
        formData.append('group_id', this.state.postList[index].to_group_id.toString());
        formData.append('comment', this.state.comment_data);
        formData.append('commented_by', this.state.user_id);
        formData.append('file_name', this.state.imgFile);
        updateComment(formData).then(res => { }).catch(err => { });
    }

    onChangeField = (e) => {
        this.setState({ comment: e.target.value })
    }

    onSelectFile = e => {
        this.setState({ imgFile: e.target.files[0] })
    }

    // Toggle Sidebar
    togglepostsidebar = () => {
        document.getElementById('f-wrap').classList.toggle("f-show-post-sider");
    }

    // Close Sidebar
    closepostsidebar = () => {
        document.getElementById('f-wrap').classList.remove("f-show-post-sider");
    }

    postTabSwitch = (e) => {
        this.setState({ postList: [], loader: true })
        if (e == 'public') {
            this.dataCompleted = 0;
            this.offset = 0;
            this.publicFeed();
            this.setState({ current_tab: 'public' });
        } else {
            this.dataCompleted = 0;
            this.offset = 0;
            this.getALl();
            this.setState({ current_tab: 'all' });
        }
    }

    searchEvent = (e) => {
        this.offset = 0;
        this.dataCompleted = 0;
        this.setState({ searchText: e.target.value });
        window.scrollTo(0, 0);
        if (this.state.current_tab == 'public') {
            this.publicFeed(e.target.value);
            this.setState({ current_tab: 'public' });
        } else {
            this.getALl(e.target.value);
            this.setState({ current_tab: 'all' });
        }
    }

    render() {
        return (
            <section className="listLanding f-page">
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <div className="f-post-listing">

                            <PostsSidebar history={this.props.history} callback={(e) => this.callback()} postTabSwitch={this.postTabSwitch} />
                            {/* Close Sidebar Mobile */}
                            <span className="f-close-post-sidebar" onClick={() => this.closepostsidebar()} style={{ display: 'none' }} />
                            <div className="f-posts-main">

                                <div className="f-posts-create f-force-hide-tab">
                                    <Button onClick={this.createPost}>Create New Post <img src={require('../../../assets/images/icon/plus.svg')} /></Button>
                                </div>
                                <div className="f-posts-search">
                                    <div className="f-member-listing">
                                        <div className="f-content-searchable f-alt-non-col" style={{ margin: '0 0 10px', display: 'block' }} >
                                            <Search
                                                placeholder="Search Posts"
                                                value={this.state.searchText}
                                                loading={this.state.loader}
                                                onChange={(e) => this.searchEvent(e)}
                                                style={{ float: 'right' }} />
                                        </div>
                                    </div>
                                </div>
                                <div className="f-posts-filters f-posts-filters-device" style={{ display: 'none' }}>
                                    <Button onClick={() => this.togglepostsidebar()} className="f-postside-unfold f-force-hide-tab" style={{ display: 'none' }}><Icon type="menu-unfold" /></Button>
                                    <div className="f-tab-ui">
                                        <Radio.Group defaultValue={'familheey'} onChange={(e) => this.onTabChange(e)}>
                                            <Radio value={'familheey'}>MY FAMILHEEY</Radio>
                                            <Radio value={'public'}>PUBLIC FEED</Radio>
                                        </Radio.Group>
                                    </div>
                                </div>

                                {/* Empty List */}
                                {
                                    !this.state.loader && this.state.postList && this.state.postList.length == 0 &&
                                    <div className="f-event-contents">
                                        <div className="f-switch-content">
                                            <div className="f-content">
                                                <div className="f-empty-content">
                                                    <img src={require('../../../assets/images/create_post.png')} />
                                                    <h4>Posts and updates from your family members will show up here.<br />Search for your family and join them to see their posts.</h4>
                                                    <p>If you can't find your family, then you can create one and invite members to join.</p>
                                                    <div className="f-clearfix">
                                                        <Button onClick={() => this.props.history.push('/searchResult', { current: window.location.pathname })}>Search Family</Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                {/* Empty List - END */}


                                {/* Post Cards */}
                                <div className="f-post-feed">
                                    {
                                        this.state.postList && this.state.postList.map((v, index) => {
                                            // console.log(v)
                                            return <div key={index} className="f-clearfix f-listland-card">
                                                <Drawer title="Basic Drawer" placement="right" closable={true} onClose={(e) => this.onClose(index)} visible={this.state.visible[index]} >
                                                    <ul>
                                                        {
                                                            v.comments && v.comments.map((x, _index) => {
                                                                return (<li key={_index} className={(x.commented_by == this.state.user_id) ? 'li-item align-right' : 'li-item align-left'} key={_index}>{x.comment} <br />

                                                                    {x.file_name ? <img src={this.state.S3_BASE_URL + 'file_name/' + x.file_name} /> : ''}
                                                                    {x.full_name},{moment(x.createdAt).format('DD-MM-YYYY HH:mm')}
                                                                </li>);
                                                            })

                                                        }
                                                    </ul>
                                                    <input type="file" onChange={this.onSelectFile} />
                                                    <TextArea rows={4} onKeyPress={() => this.handleKeyPress(v, index)} placeholder="Description" onChange={(e) => this.onChangeField(e)} />
                                                    {/* <Button type="primary" onClick={(e) => this.save(v)}>SAVE</Button> */}
                                                    <div>
                                                        <button onClick={(e) => this.save(v, index)}>SAVE</button>
                                                    </div>
                                                </Drawer>
                                                {v.shared_user_names ? <PostsCard
                                                    searchEvent={this.searchEvent}
                                                    type="2"
                                                    data={v}
                                                    current_tab={this.state.current_tab}
                                                    key={index}
                                                    history={this.props.history}
                                                    chatVisible={false}
                                                /> : <PostsCard
                                                        searchEvent={this.searchEvent}
                                                        current_tab={this.state.current_tab}
                                                        type="1"
                                                        data={v}
                                                        key={index}
                                                        history={this.props.history}
                                                        chatVisible={false}
                                                    />} </div>;
                                        })
                                    }
                                </div>
                                {this.state.loader && <Card className="postsCard f-post-card" style={{ padding: '15px' }}><Skeleton avatar paragraph={{ rows: 4 }} /></Card>}


                            </div>
                        </div>
                    </div>
                </div>

            </section>
        )
    }
}

const mapStateToProps = (state) => {
    let { userId } = state.user_details;
    return {
        userID: userId
    }
}

export default connect(mapStateToProps, null)(listLanding)