import React from 'react';
import { Input, Card, Icon, Avatar, Drawer } from "antd";
import { postList, postLike, updateComment, getCommentsByPost } from '../post_service';
import './tempstyle.css';
import { withHooksHOC } from '../../../shared/propsfunctoin';
import openSocket from 'socket.io-client';
const { Meta } = Card;
var moment = require('moment');

interface IHooksHOCProps {
    configObj: Array;
}
const { TextArea } = Input;

class CreatePost extends React.Component<IHooksHOCProps>{
    state;
    showmodal;
    props;
    socket;
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            familyDetails: '',
            imgFile: '',
            user_id: localStorage.getItem('user_id'),
            loader: true,
            postList: [],
            visible: [],
            S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,

        };
        this.socket = openSocket(process.env.REACT_APP_SOCKET_URL);
        // socket.on('message', timestamp => {
        //     console.log(timestamp);
        //     socket.emit('test1', 'heloo');
        // });
        // socket.emit('subscribeToTimer', 1000);
    }
    componentDidMount() {
        this.getALl();
    }
    getALl = () => {
        postList({ user_id: localStorage.getItem('user_id'),type:'post' })
            .then(res => {
                this.setState({ postList: res.data.data });
            })
    }
    like = (value, index) => {
        postLike({
            post_id: value.post_id.toString(),
            user_id: localStorage.getItem('user_id')
        })
            .then(res => {
                this.state.postList[index].like_status = !this.state.postList[index].like_status;
                this.setState({ postList: this.state.postList });
            })
    }
    handleKeyPress = (value, index) => {
        this.socket.emit('post_channel_typing', { post_id: value.post_id, user_id: localStorage.getItem('user_id') })
    }
    comment = (value, index) => {
        this.state.visible[index] = !this.state.visible[index];
        getCommentsByPost({ post_id: value.post_id.toString(), user_id: localStorage.getItem('user_id') })
            .then(res => {
                this.state.postList[index].comments = res.data.data;
                this.setState({ postList: this.state.postList });
            });
        let channel_name = 'post_channel_' + value.post_id;
        // let post_channel_typing = 'post_channel_typing_' + value.post_id;

        // this.socket.on(post_channel_typing, data => {
        //     console.log(data);
        //     // this.setState({ postList: this.state.postList });
        // });
        this.socket.on('notification_' + localStorage.getItem('user_id'), message => {
            console.log('notification', message);
        });
        this.socket.on(channel_name, timestamp => {
            this.state.postList[index].comments.push(timestamp);
            console.log('timestamp');
            this.setState({ postList: this.state.postList });
        });
        this.setState({ visible: this.state.visible });
    }
    onClose = (index) => {
        this.state.visible[index] = !this.state.visible[index];
        this.setState({ visible: this.state.visible });
    }
    save = (v, index) => {
        console.log(v);
        // this.state.postList[index].comments.push({
        //     post_id: v.post_id.toString(),
        //     comment: this.state.comment,
        //     commented_by: localStorage.getItem('user_id')
        // });
        // // console.log('timestamp');

        // this.setState({ postList: this.state.postList });
        var formData = new FormData();
        formData.append('post_id', v.post_id.toString());
        formData.append('group_id', this.state.postList[index].to_group_id.toString());
        formData.append('comment', this.state.comment);
        formData.append('commented_by', localStorage.getItem('user_id'));
        formData.append('file_name', this.state.imgFile);

        updateComment(formData)
            .then(res => {
                // this.getALl();
            })
            .catch(err => {

            });
    }
    onChangeField = (e) => {
        this.setState({ comment: e.target.value })

    }
    onSelectFile = e => {
        this.setState({ imgFile: e.target.files[0] })
        // if (e.target.files && e.target.files.length > 0) {
        //     const reader = new FileReader();
        //     reader.addEventListener('load', () =>
        //         this.setState({ src: reader.result })
        //     );
        //     reader.readAsDataURL(e.target.files[0]);
        // }
    };
    render() {
        return (
            <section className="familyDetails f-page">
                <div className="f-clearfix f-body">
                    <section className="f-sect">
                        List
                        {this.state.postList && this.state.postList.map((value, index) =>
                            <div>

                                <Card key={index}
                                    // onClick={() => this.navigateToDetails(value)}
                                    style={{ width: 300 }}
                                    cover={
                                        value.thumbnail_image ? (
                                            value.thumbnail_image && value.thumbnail_image.split(',').map(e => {
                                                return (<img
                                                    alt="example"
                                                    src={process.env.REACT_APP_PRO_PIC_CROP + this.state.S3_BASE_URL + 'thumbnail_image/' + e} />)

                                            })
                                        ) : <img
                                                alt="example"
                                                src="https://familheey.s3.amazonaws.com/logo/logo-1571218166499.jpg" />
                                    }
                                    actions={[
                                        (!value.like_status ? <Icon type="like" key="like" onClick={() => this.like(value, index)} /> : <Icon type="like" theme="twoTone" />),
                                        <span><Icon type="mail" key="mail" onClick={() => this.comment(value, index)} /> <span>{value.conversation_count} </span> </span>,
                                        <Icon type="ellipsis" key="ellipsis" />,
                                    ]}
                                >
                                    <Meta
                                        avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                        title={value.title}
                                        description={value.snap_description}
                                    />

                                </Card>
                                <Drawer
                                    title="Basic Drawer"
                                    placement="right"
                                    closable={true}
                                    onClose={(e) => this.onClose(index)}
                                    visible={this.state.visible[index]}
                                >
                                    <ul>
                                        {
                                            value.comments && value.comments.map((v, _ind) => {
                                                return (<li className={(v.commented_by == this.state.user_id) ? 'li-item align-right' : 'li-item align-left'} key={_ind}>{v.comment} <br />
                                                    {v.file_name ? <img src={this.state.S3_BASE_URL + 'file_name/' + v.file_name} /> : ''}
                                                    {v.full_name},{moment(v.createdAt).format('DD-MM-YYYY HH:mm')}
                                                </li>);
                                            })

                                        }
                                    </ul>
                                    <input type="file" onChange={this.onSelectFile} />
                                    <TextArea rows={4} onKeyPress={() => this.handleKeyPress(value, index)} placeholder="Description" onChange={(e) => this.onChangeField(e)} />
                                    {/* <Button type="primary" onClick={(e) => this.save(value)}>SAVE</Button> */}
                                    <div>
                                        <button onClick={(e) => this.save(value, index)}>SAVE</button>
                                    </div>
                                </Drawer>


                            </div>

                        )}
                    </section>

                </div>
            </section>
        );
    }
}

export default withHooksHOC(CreatePost);