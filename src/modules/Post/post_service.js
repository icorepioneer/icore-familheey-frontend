import axios from '../../services/apiCall';
import axios1 from '../../services/fileupload';
const createPost = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/create`, data)
}
const updatePost = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/update_post`, data)
}
const postList = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/post_by_family`, data)
}
const publicFeed = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/public_feed`, data)
}
const postLike = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/like`, data)
}
const updateComment = (data) => {
    return axios1.post(`${process.env.REACT_APP_COMMENT_API_URL}/api/v1/familheey/posts/post_comment`, data)
}
const getCommentsByPost = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/getCommentsByPost`, data)
}
const updateLastReadMessage = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/updateLastReadMessage`, data)
}
const feedBackSubmit = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/receive_feedback`, data)
}
const user_group_members = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/user_group_members`, data)
}
const post_share = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/post_share`, data)
}
const announcement_list = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/announcement_list`, data)
}
const getByID = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/getByID`, data)
}
const list_post_views = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/list_post_views`, data)
}
const getSharedUserListByUserid = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/getSharedUserListByUserid`, data)
}
const getSharedUserList = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/getSharedUserList`, data)
}
const getCommonSharedUserList = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/getCommonSharedUserList`, data)
}
const getSidebarCount = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/getSidebarCount`, data)
}
const makeItUnread = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/unread`, data)
}
const deleteConversation = (data) => {
    return axios.post(`${process.env.REACT_APP_COMMENT_API_URL}/api/v1/familheey/posts/delete_comment`, data)
}
const activatePost = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/activatePost`, data)
}
const deactivatePost = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/deactivatePost`, data)
}
const addTopicComment = (data,progressFunction) => {
    return axios.post(`${process.env.REACT_APP_COMMENT_API_URL}/api/v1/familheey/topic_comment`, data,progressFunction)
}
const getTopicById = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/topic_detail`, data)
}
const addusersToTopic = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/add_users_to_topic`, data)
}
const getTopicUsersList = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/topic_users_list`, data)
}

const copyBetweenBucket = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/copy_file_between_bucket`, data)
}

const acceptUserTopic = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/accept_user_topic`, data)
}

const rejectUserTopic = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/reject_user_topic`, data)
}

const stickaPost = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/post/stickyPost`, data)
}

const addViewCount = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/posts/add_view_count`, data)
}

export {
    createPost,
    updatePost,
    postList,
    publicFeed,
    postLike,
    updateComment,
    getCommentsByPost,
    updateLastReadMessage,
    feedBackSubmit,
    user_group_members,
    post_share,
    announcement_list,
    getByID,
    list_post_views,
    getSharedUserListByUserid,
    getSharedUserList,
    getCommonSharedUserList,
    getSidebarCount,
    makeItUnread,
    deleteConversation,
    activatePost,
    deactivatePost,
    addTopicComment,
    getTopicById,
    addusersToTopic,
    getTopicUsersList,
    copyBetweenBucket,
    acceptUserTopic,
    rejectUserTopic,
    stickaPost,
    addViewCount
} 