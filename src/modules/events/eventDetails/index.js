import React, { useState, useEffect } from 'react';
import './style.scss'
import { Modal, Button, Icon, Input, Card, TimePicker, notification, Spin } from "antd";
import EventDetailsSidebar from '../../../components/EventDetailsSidebar';
import Linkify from 'linkifyjs/react';
import {
    getEventDetails,
    getSignUpEventItems,
    eventAttendingListApi,
    createContact,
    getRsvpGuestDetails,
    getInvitedByUser,
    updateSelectedContact,
    deleteContact,
    addEventSignUpItems,
    getAllEventItems,
    addEventItemsContribution,
    getAllContributionList,
    updateEventitemsContribution,
    listAllAgenda,
    createAgenda,
    updateAgenda,
    _listEventFolders,
    removeFolder,
    getguestsCount,
    getAllFamily,
    getUserGroupMembers,
    ShareORInvite,
    getAllEventFolders,
    getEventsBasedOnTag,
    updateEventSignUpItems

} from './api-eventDetails';
import { decryption } from '../../../shared/encryptDecrypt';
import Lightbox from 'react-image-lightbox';
import AddContactModal from '../../../components/AddContacts';
import SignUp from '../components/SignUp/index'
import Agenda from '../components/Agenda/index';
import Documents from '../components/Documents/index'
import Guests from '../components/Guests/index'
import ShareOrInviteModel from '../components/ShareORInvite/index'
import CreateAlbum from '../../AlbumModal/createAlbum'
import AlbumCard from '../../../components/AlbumCard/index'
import CustomImageCrop from '../../ImageCrop/index.js';
import GoogleMapReact from 'google-map-react';
import { useSelector } from 'react-redux'
// import 'react-image-lightbox/style.css';
const { TextArea } = Input;
var moment = require('moment');
const { Search } = Input;



const AnyReactComponent = ({ text }) => (
    <div
        style={{
            //color: 'white',
            //background: 'grey',
            //padding: '15px 10px',
            display: 'inline-flex',
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            //borderRadius: '100%',
            //transform: 'translate(-50%, -50%)',
            width: '32px',
            height: '32px'
        }}
    >
        {/* {text} */}
        <img style={{ width: '30px' }} src={require('../../../assets/images/icon/pin.svg')} />
    </div>
);

const SimpleMap =({center,zoom = 11},eventDetails)=>{
        return (
            <GoogleMapReact
                bootstrapURLKeys={{ key: decryption(process.env.REACT_APP_PLACE_ENCRYPT) }}
                defaultCenter={center}
                defaultZoom={zoom}
            >
                <AnyReactComponent
                    lat={eventDetails.lat}
                    lng={eventDetails.long}
                    text={eventDetails.location}
                />
            </GoogleMapReact>
        );
}

const { confirm } = Modal;
const EventDetails = ({ location, history }) => {
    const [eventStates, setEventState] = useState({
        user_id: useSelector(state=>state.user_details.userId),
        eventDetails: undefined,
        event_id: '',
        dataFromChild: 1,
        rsvp: '',
        agendaArray: [],
        loader: false,
        isOpen: false,
        count: '',
        contacts: '',
        aj_visible: false,
        su_visible: false,
        al_visible: false,
        dc_visible: false,
        sh_visible: false,
        map_visible: false,
        signupItems: [],
        tabloader: false,
        memberGoing: [],
        guestTab: '1',
        interested: [],
        not_going: [],
        inviteList: [],
        contactModal_visible: false,
        contactToEdit: '',
        loading: false,
        current_id: '',
        _visible: false,
        event_items: '',
        signUp_visible: false,
        current_SignUp_item: '',
        view_SignUP_visible: false,
        items_to_pass: '',
        event_item_contributions: '',
        current_modal_id: 0,
        status_shareORinvite: '',
        albums: '',
        initial: "1",
        params: {
            crop: {
                unit: "%",
                aspect: 261 / 100,
                // height : 100,
                width: 50
            },
            event_id: '',
            editType: '',
            user_id: ''
        },
        enableEdit: false,
        searchTag: '',
        item_to_edit: '',
        event_tag: undefined,
        _loading: false,
        modalvisible: false,
        online_modal: false,
        eventNotFound:false
    })
    let {
        user_id,
        initial,
        dataFromChild,
        eventDetails,
        event_id,
        rsvp,
        //agendaArray,
        loader,
        isOpen,
        //count,
        contacts,
        aj_visible,
        su_visible,
        al_visible,
        dc_visible,
        sh_visible,
        map_visible,
        //signupItems,
        tabloader,
        memberGoing,
        guestTab,
        interested,
        not_going,
        inviteList,
        contactModal_visible,
        contactToEdit,
        loading,
        current_id,
        _visible,
        event_items,
        signUp_visible,
        current_SignUp_item,
        view_SignUP_visible,
        items_to_pass,
        event_item_contributions,
        current_modal_id,
        status_shareORinvite,
        albums,
        params,
        enableEdit,
        searchTag,
        item_to_edit,
        //event_tag,
        _loading,
        modalvisible,
        online_modal,
        eventNotFound
    } = eventStates;
    useEffect(() => {
        if (location.pathname.split('events/')[1]) {
            let event_tag = location.pathname.split('events/')[1]
            _getEventDetailsFn(event_tag)
        }
        else {
            let cipher = location.pathname.split('event-details/').pop()
            let section = location.pathname.split('/')[3]
            section && dataCallback('change', section)
            section && setEventState(prev => ({ ...prev, initial: `${section}` }))
            setEventState((prev) => ({ ...prev, event_id: decryption(cipher) }))
        }

    }, [])
    const _getEventDetailsFn = async (tag) => {
        setEventState((prev) => ({ ...prev, loader: true }))
        var params_evt_det = {
            event_page: tag,
            user_id: user_id
        }
        await getEventsBasedOnTag(params_evt_det).then((response) => {
            let { event,rsvp:_rsvp,contacts:_contacts } = response.data.data
            setEventState(prev => ({
                ...prev,
                eventNotFound:event.length>0?false:true,
                eventDetails: event.length>0?event[0]:undefined,
                rsvp: _rsvp.length>0? _rsvp[0]:undefined,
                // count: response.data.data.contacts[0],
                contacts: _contacts.length>0?_contacts:[],
                loader: false,
                event_id: `${response.data.data.event[0].event_id}`
            }))
        }).catch((error) => {
            console.log('err', error);
        })
    }



    // class SimpleMap extends React.Component {
    //     static defaultProps = {
    //         center: { lat: eventDetails.lat, lng: eventDetails.long },
    //         zoom: 11
    //     };

    //     render() {
    //         return (
    //             <GoogleMapReact
    //                 bootstrapURLKeys={{ key: 'AIzaSyCY7ATYsEBgH4LFYwPmxngMbBm2smQ7bL8' }}
    //                 defaultCenter={this.props.center}
    //                 defaultZoom={this.props.zoom}
    //             >
    //                 <AnyReactComponent
    //                     lat={eventDetails.lat}
    //                     lng={eventDetails.long}
    //                     text={eventDetails.location}
    //                 />
    //             </GoogleMapReact>
    //         );
    //     }
    // }

    const getEventDetailsFn = async () => {
        setEventState((prev) => ({ ...prev, loader: true }))
        var params_getevt = {
            event_id: event_id,
            user_id: user_id
        }
        await getEventDetails(params_getevt).then((response) => {
            let { event,rsvp:rs_vp,contacts:cont_acts } = response.data.data
            setEventState(prev => ({
                ...prev,
                eventNotFound:event.length>0?false:true,
                eventDetails: event.length>0?event[0]:undefined,
                rsvp: rs_vp.length>0? rs_vp[0]:undefined,
                // count: response.data.data.contacts[0],
                contacts: cont_acts.length>0?cont_acts:[],
                loader: false,
            }))
            if (response.data.data.event[0] && !response.data.data.event[0].is_active) {
                setEventState((prev) => ({ ...prev, modalvisible: true }))
            }
        }).catch((error) => {
            console.log('err', error);
        })
    }
    const handleOk = () => {
        history.push('/events');
        setEventState((prev) => ({ ...prev, modalvisible: false }))
    }
    const getEventItems = async () => {
        var params_getevtitem = {
            event_id: event_id,
        }
        await getAllEventItems(params_getevtitem).then((response) => {
            setEventState(prev => ({
                ...prev,
                event_items: response.data.data,
            }))
        }).catch((error) => {
            console.log('err', error);
        })
    }
    useEffect(() => {
        const event_Details = async () => {
            await getEventDetailsFn()
        }
        const eventItems = async () => {
            await getEventItems()
        }
        const _getAlbums = async () => {
            getAlbums()
        }
        event_id && event_Details();
        event_id && eventItems();
        event_id && _getAlbums();

    }, [event_id])
    const dataCallback = (type, data) => {
        switch (type) {
            case 'change':
                setEventState(prev => ({ ...prev, dataFromChild: data, initial: data }));
                break;
            case 'update':
                setEventState(prev => ({ ...prev, eventDetails: data, initial: data }));
                break;
            case 'agenda':
                setEventState(prev => ({ ...prev, agendaArray: data.data.data, initial: data }));
                break;
            case 'coverpic':
                setEventState(prev => ({ ...prev, eventDetails: data.data.data, enableEdit: false, initial: data }))
                break;
            default: 
                setEventState(prev => ({ ...prev, enableEdit: false, initial: data }));
                break;
        }
    }
    useEffect(() => {
        (() => {
            if (dataFromChild === '2') {
                getAllSignUpItem();
            } else if (dataFromChild === '3') {
                setEventState(prev => ({ ...prev, guestTab: '1' }))
                eventAttendingMemberList();
            }
        })()
    }, [dataFromChild])
    // Modal - Signup
    const su_showModal = () => {
        setEventState(prev => ({
            ...prev,
            _visible: !_visible,
        }))
    };
    const contact_showModal = () => {
        setEventState(prev => ({
            ...prev,
            contactModal_visible: !contactModal_visible,
            contactToEdit: ''
        }))
    };
    const su_handleCancel = e => {
        setEventState(prev => ({
            ...prev,
            su_visible: false,
        }))
    }
    // Modal - Agenda
    const aj_showModal = () => {
        setEventState((prev) => ({
            ...prev,
            aj_visible: !aj_visible,
        }))
    };
    // const aj_handleCancel = e => {
    //     setEventState(prev => ({
    //         ...prev,
    //         aj_visible: false,
    //     }))
    // };
    // Modal - Album
    // const al_showModal = () => {
    //     setEventState(prev => ({
    //         ...prev,
    //         al_visible: true,
    //     }))
    // };
    const al_handleCancel = e => {
        setEventState(prev => ({
            ...prev,
            al_visible: false,
        }))
    };
    // Modal - Map
    const map_showModal = () => {
        setEventState(prev => ({
            ...prev,
            map_visible: true,
        }))
    };
    const map_handleCancel = e => {
        setEventState(prev => ({
            ...prev,
            map_visible: false,
        }))
    };
    // Modal - Document
    const dc_showModal = () => {
        setEventState(prev => ({
            ...prev,
            dc_visible: true,
        }))
    };
    const dc_handleCancel = e => {
        setEventState(prev => ({
            ...prev,
            dc_visible: false,
        }))
    };
    // Modal - Share
    const sh_showModal = (buttonType) => {
        setEventState(prev => ({
            ...prev,
            sh_visible: true,
            shareORinvite: buttonType
        }))
    };
    const _sh_showModal = (buttonType) => {
        setEventState(prev => ({
            ...prev,
            sh_visible: true,
            status_shareORinvite: buttonType
        }))
    };
    const sh_handleCancel = e => {
        setEventState(prev => ({
            ...prev,
            sh_visible: false,
        }))
        _loading && sh_setLoader()
    };
    const updateContacts = (data, flag) => {
        let current = contacts;
        if (!flag) {
            current.push(data)
            setEventState(prev => ({ ...prev, contacts: current }))
        }
        else {
            let new_array = current.filter(item => item.id != data.id)
            new_array.push(data);
            setEventState(prev => ({ ...prev, contacts: new_array }))
        }
    }
    const updateSignupItems = (new_item) => {
        let _new_item = { ...new_item, needed: new_item.item_quantity }
        let _event_items = event_items
        _event_items.push(_new_item)
        setEventState((prev) => ({ ...prev, event_items: _event_items }))

    }
    const _getAllContributionList = (item) => {
        var params_getall = {
            item_id: `${item.id}`,
            event_id: event_id,
        }
        return new Promise((resolve, reject) => {
            getAllContributionList(params_getall).then((response) => {
                setEventState(prev => ({
                    ...prev,
                    event_item_contributions: response.data.data,
                }))
                resolve(true)
            }).catch((error) => {
                reject(false)
                console.log('err', error);
            })
        })

    }
    const showSignUpModal = (item) => {
        setEventState(prev => ({ ...prev, signUp_visible: !signUp_visible, current_SignUp_item: item }))
    }
    const _viewSignUpModal = async (item) => {
        item.status != 'Hide' ? (await _getAllContributionList(item).then(() => {
            setEventState(prev => ({ ...prev, view_SignUP_visible: !view_SignUP_visible, items_to_pass: item, current_modal_id: item.id }))
        }).catch(err => {
            console.log("error happend while fetching data")
        })) : (setEventState(prev => ({ ...prev, view_SignUP_visible: !view_SignUP_visible, items_to_pass: item })))

    }
    const getAllSignUpItem = () => {
        var params_getsign = {
            event_id: event_id.toString()
        }
        getSignUpEventItems(params_getsign).then((response) => {
            setEventState(prev => ({ ...prev, signupItems: response.data.data, tabloader: false }))
        }).catch((error) => {
            setEventState(prev => ({ ...prev, tabloader: false }))
        })
    }
    const eventAttendingMemberList = () => {
        setEventState(prev => ({ ...prev, tabloader: true }))
        var params_evtatt = {
            event_id: event_id.toString(),
            current_id: user_id
        }
        eventAttendingListApi(params_evtatt).then((response) => {
            setEventState(prev => ({ ...prev, memberGoing: response.data, tabloader: false }))
        }).catch((error) => {
            setEventState(prev => ({ ...prev, tabloader: false }))
        })
    }
    const handleRadioChange = (e) => {
        setEventState(prev => ({ ...prev, guestTab: e.target.value }))
        // (() => {
        if (e.target.value === '1') {
            eventAttendingMemberList()
        } else if (e.target.value === '2') {
            getMaybeOrNotGoingList('interested')
        } else if (e.target.value === '3') {
            getMaybeOrNotGoingList('not-going')
        } else if (e.target.value === '4') {
            invitedMemberList()
        }
        // }
        // )()
    }
    const getAlbums = (data) => {
        let params_alb = {
            user_id: `${user_id}`,
            event_id: `${event_id}`,
            folder_for: "events",
            folder_type: "albums"
        }
        getAllEventFolders(params_alb).then((resp) => {
            setEventState(previous => ({ ...previous, albums: resp.data.data }))
        })
    }
    const getMaybeOrNotGoingList = (type) => {
        setEventState(prev => ({ ...prev, tabloader: true }))
        var params_may = {
            event_id: event_id.toString(),
            current_id: user_id,
            rsvp_status: type
        }
        getRsvpGuestDetails(params_may).then((response) => {
            setEventState(prev => ({ ...prev, [type]: response.data, tabloader: false }))
        }).catch((error) => {
            setEventState(prev => ({ ...prev, tabloader: false }))
        })
    }
    const invitedMemberList = () => {
        setEventState(prev => ({ ...prev, tabloader: true }))
        var params_invit = {
            event_id: event_id.toString(),
            user_id: user_id,
        }
        getInvitedByUser(params_invit).then((response) => {
            var array = response.data.data.groups.concat(response.data.data.others)
            array = array.concat(response.data.data.users)
            setEventState(prev => ({ ...prev, inviteList: array, tabloader: false }))
        }).catch((error) => {
            setEventState(prev => ({ ...prev, tabloader: false }))
        })
    }
    const toEditContact = (item) => {
        item = { ...item, type:'Edit'}
        setEventState(prev => ({ ...prev, contactModal_visible: !contactModal_visible, contactToEdit: item }))
    }
    const deleteSelectedContact = async (selected) => {
        confirm({
            title: 'Are you sure?',
            okText: 'Yes',
            cancelText: 'No',
            onOk() {
                let current = contacts;
                setEventState(prev => ({ ...prev, loading: true, current_id: selected.id }))
                let data = {
                    id: `${selected.id}`
                }
                deleteContact(data).then(res => {
                    let new_array = current.filter(item => item.id != data.id)
                    setEventState(prev => ({ ...prev, loading: false, contacts: new_array }))
                }).catch((e)=>{
                    console.log(e)
                })
            },
            onCancel() {},
        })
    }
    const enableEditMode = (type) => event => {
        let _params = params
        _params.editType = type
        _params.event_id = event_id
        _params.user_id = user_id
        setEventState((prev) => ({ ...prev, enableEdit: true, params: _params }))
    }
    const showCropModel = () => {
        setEventState((prev) => ({ ...prev, enableEdit: false }))
    }
    const onAlbumSearch = (event) => {
        setEventState(prev => ({ ...prev, searchTag: event.target.value }))
    }
    const setAlbumToEdit = (item) => {
        setEventState(prev => ({ ...prev, item_to_edit: item }))
    }
    const sh_setLoader = () => {
        setEventState(prev => ({
            ...prev,
            _loading: !_loading
        }))
    }
    const _flag = eventDetails && eventDetails.is_public ? true : (eventDetails&& eventDetails.created_by == user_id ? true : false)
    const copyToClipBoard = (tag) => {
        let text = document.getElementById("url_to_copy");
        navigator.clipboard.writeText(`${text.innerText}`)
        notification.open({
            message: "URL copied",
            icon: <Icon type="copy" style={{ color: "#108ee9" }} />
        });
    }
    const openInNewTab = (tag) => {
        navigator.clipboard.writeText(`${tag}`)
        successModal()
    }
    const dateFormatTodisplay = (_eventDetails) => {
        let { from_date, to_date } = _eventDetails;
        let date;
        switch (true) {
            case moment.unix(from_date).isSame(moment.unix(to_date)):
                date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')}`
                break;
            case moment.unix(from_date).get('year') == moment.unix(to_date).get('year'):
                date = `${moment.unix(from_date).format('ddd,MMM DD ')} to ${moment.unix(to_date).format('ddd,MMM DD,YYYY')}`
                break;
            case moment.unix(from_date).get('year') != moment.unix(to_date).get('year'):
                date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')} - ${moment.unix(to_date).format('ddd,MMM DD YYYY')}`
                break;
            default:
                date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')} - ${moment.unix(to_date).format('ddd,MMM DD YYYY')}`
                break;
        }
        return date
    }

    const successModal = () => {
        setEventState(prev => ({ ...prev, online_modal: !online_modal }))
    }

    return (
        <section className="eventDetails f-page">
            {eventDetails &&eventDetails.is_active &&
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <div className="f-event-details">
                            <header className="f-head">
                                <div className="f-poster">
                                    {/* Edit Poster */}
                                    {eventDetails.created_by == user_id && <Button className="f-edit-poster-pic" onClick={enableEditMode('event_cover_pic')} ><Icon type="camera" /></Button>}
                                    {eventDetails.event_image && eventDetails.event_image != 'undefined' ? <img src={process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'event_image/' + eventDetails.event_image} /> :
                                        <div className="f-poster-default">
                                            <img src={require('../../../assets/images/default_event_image.png')} />
                                            {eventDetails.created_by == user_id && <h4>Upload a photo to be displayed here !</h4>}
                                            {eventDetails.created_by == user_id && <p>Size recommendation: 1044 X 400 px</p>}
                                        </div>
                                    }
                                    {/* Shadow */}
                                    <span className="f-overlay" onClick={() => setEventState(prev => ({ ...prev, isOpen: true }))} />
                                </div>
                                <div className="f-page-overview">
                                    <div className="f-head">
                                        <ul>
                                            <li>{eventDetails.category}</li>
                                            { eventDetails && eventDetails.is_public === true &&
                                                <li>Public</li>
                                            }
                                            { eventDetails && eventDetails.is_public === false &&
                                                <li>Private</li>
                                            }
                                            <li>{eventDetails.event_type}</li>
                                        </ul>
                                        <h3 className="f-head-title">{eventDetails.event_name}</h3>
                                        <h5><em>By {eventDetails.created_by_name}</em></h5>
                                        <h5 className="f-date">{dateFormatTodisplay(eventDetails)}</h5>
                                        <h5 className="f-time">{moment.unix(eventDetails.from_date).format('hh:mm a')} - {moment.unix(eventDetails.to_date).format('hh:mm a')}</h5>
                                        {eventDetails.event_type === 'Online' ?
                                            <div>
                                                <div>
                                                    <h5 className="f-webinar">
                                                        <img src={`${process.env.REACT_APP_S3_BASE_URL}${eventDetails.meeting_logo}`} />
                                                        {/* <Linkify
                                                            style={{ marginTop: '0px' }}
                                                            target="_blank"> */}
                                                            <span
                                                                style={{color:"#1890ff",marginTop:'10px'}}
                                                                onClick={() => { openInNewTab(eventDetails.meeting_pin) }}
                                                                id='url_to_open'>
                                                                {`${eventDetails.meeting_link}`}
                                                            </span>
                                                        {/* </Linkify> */}
                                                    </h5>
                                                </div>
                                                {moment().add(15,'m').isBetween(eventDetails.from_date,eventDetails.to_date)&&<div><Button  onClick={() => successModal()}>Join</Button></div>}
                                            </div>
                                            :
                                            <h5 className="f-location f-clickable" onClick={map_showModal}><em>{eventDetails.location}</em></h5>
                                        }
                                        <h5 className="f-eclipse" style={{ marginTop: '7px' }}>
                                            <strong style={{ minWidth: '80px' }}>Event URL :</strong> 
                                            <em  id="url_to_copy" onClick={() => copyToClipBoard()}>
                                                { eventDetails.event_url ?  eventDetails.event_url : `${process.env.REACT_APP_BASE_URL}page/events/${eventDetails.event_page}` }
                                            </em>
                                        </h5>
                                        {/* <Tag color="#ffdd72">{ eventDetails.ticket_type}</Tag> */}
                                    </div>
                                    <div className="f-right">
                                        {_flag && <React.Fragment>
                                            <Button className="f-share" onClick={() => _sh_showModal("share")} />
                                            {
                                                eventDetails && (!eventDetails.is_public || eventDetails.id == user_id) &&
                                                <Button className="f-invite" onClick={() => _sh_showModal("invite")}>Invite</Button>
                                            }
                                            <ShareOrInviteModel
                                                sh_setLoader={sh_setLoader}
                                                sh_visible={sh_visible}
                                                sh_handleCancel={sh_handleCancel}
                                                getUserGroupMembers={getUserGroupMembers}
                                                getAllFamily={getAllFamily}
                                                user_id={user_id}
                                                event_id={event_id}
                                                ShareORInvite={ShareORInvite}
                                                status={status_shareORinvite}
                                                _loading={_loading}
                                            />
                                        </React.Fragment>}
                                    </div>
                                </div>
                            </header>

                            <section className="f-sect">
                                <EventDetailsSidebar history={history} eventInfo={eventDetails} rsvp={rsvp} callBackFromParent={dataCallback} initial={initial} />
                                <div className="f-events-main">
                                    {/* Details */}
                                    {
                                        dataFromChild && dataFromChild == 1 &&
                                        <div className="f-event-contents">
                                            <div className="f-switch-content" >
                                                <div className="f-title">
                                                    <h3>Details</h3>
                                                </div>
                                                <div className="f-content">
                                                    <p><Linkify>{eventDetails.description}</Linkify> </p>
                                                    {eventDetails.meeting_dial_number&&eventDetails.meeting_dial_number.length>0&&<p>Dial in number(s) : {eventDetails.meeting_dial_number}</p>}
                                                    { eventDetails.rsvp && <p> RSVP required</p> }
                                                    {/* {eventDetails.created_by == user_id && <p>Contact for queries <Button onClick={contact_showModal} className="f-green-btn">Add</Button></p>} */}
                                                    <AddContactModal
                                                        updateSelectedContact={updateSelectedContact}
                                                        contactToEdit={contactToEdit}
                                                        updateContacts={updateContacts}
                                                        createContact={createContact}
                                                        contactModal_visible={contactModal_visible}
                                                        event_id={event_id}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {
                                        dataFromChild && dataFromChild == 1 && eventDetails.created_by == user_id && <div className="f-signup-list">
                                            <h4>Contact for queries <Button onClick={contact_showModal} className="f-green-btn">Add</Button></h4>
                                            {contacts && contacts.length > 0 && <div className="f-signup-cards">
                                                {contacts.map((item) => {
                                                    let _loading_ = current_id == item.id && loading
                                                    return <Card key={item.id}>
                                                        <h4>{item.name}</h4>
                                                        <div className="f-flex">
                                                            <div className="f-left">
                                                                <p>{item.phone.replace("-", "")}</p>
                                                            </div>
                                                        </div>
                                                        <div className="f-bottom">
                                                            {eventDetails.created_by == user_id && <Button className="f-gray" loading={_loading_} onClick={() => deleteSelectedContact(item)}>Delete</Button>}
                                                            {eventDetails.created_by == user_id && <Button className="f-green" onClick={() => toEditContact(item)}>Edit</Button>}
                                                        </div>
                                                    </Card>
                                                }
                                                )}

                                            </div>}
                                        </div>}

                                    {/* Sign up */}
                                    {
                                        dataFromChild && dataFromChild == 2 && <SignUp
                                            su_showModal={su_showModal}
                                            addEventSignUpItems={addEventSignUpItems}
                                            _visible={_visible}
                                            event_id={event_id}
                                            user_id={user_id}
                                            getAllEventItems={getEventItems}
                                            updateSignupItems={updateSignupItems}
                                            event_items={event_items}
                                            view_SignUP_visible={view_SignUP_visible}
                                            current_modal_id={current_modal_id}
                                            _viewSignUpModal={_viewSignUpModal}
                                            showSignUpModal={showSignUpModal}
                                            event_item_contributions={event_item_contributions}
                                            getEventItems={getEventItems}
                                            addEventItemsContribution={addEventItemsContribution}
                                            updateEventitemsContribution={updateEventitemsContribution}
                                            signUp_visible={signUp_visible}
                                            current_SignUp_item={current_SignUp_item}
                                            items_to_pass={items_to_pass}
                                            _getAllContributionList={_getAllContributionList}
                                            from_date={eventDetails.from_date}
                                            to_date={eventDetails.to_date}
                                            created_by={eventDetails.created_by}
                                            updateEventSignUpItems={updateEventSignUpItems}
                                        />
                                    }

                                    {/* Guests */}
                                    {
                                        dataFromChild && dataFromChild == 3 &&
                                        <div className="f-clearfix">
                                            <div className="f-event-contents f-blanco">
                                                <div className="f-switch-content">
                                                    <div className="f-content">
                                                        {/* Members Overview */}
                                                        <Guests
                                                            handleRadioChange={handleRadioChange}
                                                            guestTab={guestTab}
                                                            memberGoing={memberGoing}
                                                            tabloader={tabloader}
                                                            interested={interested}
                                                            not_going={not_going}
                                                            inviteList={inviteList}
                                                            getguestsCount={getguestsCount}
                                                            event_id={event_id}
                                                            user_id={user_id}
                                                            created_by={eventDetails.created_by == user_id}
                                                        />
                                                        {/*Members Overview - END */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    }
                                    {/* Agenda */}
                                    {
                                        dataFromChild && dataFromChild == 4 && <Agenda
                                            aj_showModal={aj_showModal}
                                            aj_visible={aj_visible}
                                            event_id={event_id}
                                            listAllAgenda={listAllAgenda}
                                            createAgenda={createAgenda}
                                            user_id={user_id}
                                            updateAgenda={updateAgenda}
                                            getguestsCount={getguestsCount}
                                            created_by={eventDetails.created_by == user_id}
                                            from_date={eventDetails.from_date}
                                            to_date={eventDetails.to_date}

                                        />
                                    }
                                    {/* Agenda List - END */}


                                    {/* Album */}
                                    {
                                        dataFromChild && dataFromChild == 5 &&
                                        <div className="f-clearfix">
                                            {/* Album List */}
                                            <div className="f-event-contents f-blanco">
                                                <div className="f-switch-content">
                                                    <div className="f-content">
                                                        <div className="f-clearfix">
                                                            <div className="f-member-listing">
                                                                <div className="f-content-searchable">
                                                                    <CreateAlbum
                                                                        getAlbums={getAlbums}
                                                                        type={'events'}
                                                                        eventDetails={eventDetails}
                                                                        event_id={event_id}
                                                                        item_to_edit={item_to_edit}
                                                                        setAlbumToEdit={setAlbumToEdit}
                                                                    />
                                                                    <Search onChange={onAlbumSearch} placeholder="Discover" />
                                                                </div>
                                                                <div className="f-content-searchable">
                                                                    <h4><strong>{albums.length}</strong> Albums</h4>
                                                                </div>
                                                                <AlbumCard
                                                                    searchTag={searchTag}
                                                                    _type={'events'}
                                                                    event_id={event_id}
                                                                    _getAlbums={getAlbums}
                                                                    displayContent={albums}
                                                                    history={history}
                                                                    created_by={eventDetails ? eventDetails.created_by : null}
                                                                    setAlbumToEdit={setAlbumToEdit}
                                                                ></AlbumCard>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* Album List End */}
                                        </div>
                                    }
                                    {/* Documents */}
                                    {
                                        dataFromChild && dataFromChild == 6 &&
                                        <div className="f-clearfix">
                                            <Documents
                                                eventDetails={eventDetails}
                                                dc_showModal={dc_showModal}
                                                sh_showModal={sh_showModal}
                                                _listEventFolders={_listEventFolders}
                                                folder_for='events'
                                                user_id={user_id}
                                                event_id={event_id}
                                                dc_visible={dc_visible}
                                                dc_handleCancel={dc_handleCancel}
                                                removeFolder={removeFolder}
                                            />
                                        </div>
                                    }
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            }

            <Modal
                cancelButtonProps={{ style: { display: 'none' } }}
                title="Basic Modal"
                visible={modalvisible}
                onOk={handleOk}
            >
                <p>Oops! This content is no longer available.</p>
            </Modal>

            {isOpen === true &&
                <Lightbox
                    mainSrc={process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'event_original_image/' + eventDetails.event_original_image}
                    onCloseRequest={() => setEventState(prev => ({ ...prev, isOpen: false }))}
                />}
            {loader &&
                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '200px' }}>
                </Spin>
            }

            {/* Modal - Sign Up */}
            <Modal title="Add Sign Ups"
                visible={su_visible}
                onCancel={() => su_handleCancel()}
                onOk={() => su_handleCancel()}
                className="f-modal f-modal-wizard">
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-form-control">
                        <label>Title *</label>
                        <Input />
                    </div>
                    <div className="f-form-control">
                        <label>Description</label>
                        <TextArea placeholder="250 characters max" rows={4} />
                    </div>
                    <div className="f-form-control">
                        <label>Count</label>
                        <Input placeholder="Eg: 1" />
                    </div>
                    <div className="f-form-control">
                        <div className="f-split" style={{ margin: '0' }}>
                            <div>
                                <label>Time</label>
                                <TimePicker placeholder="From" />
                            </div>
                            <div>
                                <label>&nbsp;</label>
                                <TimePicker placeholder="To" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="f-modal-wiz-footer">
                    <Button className="f-fill">Add to Sign Ups</Button>
                </div>
            </Modal>


            {/* Modal - Album */}
            <Modal title="Create Album"
                visible={al_visible}
                onCancel={() => al_handleCancel()}
                onOk={() => al_handleCancel()}
                className="f-modal f-modal-wizard">
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-form-control">
                        <label>Album name</label>
                        <Input placeholder="Eg: Nigeria womens group " />
                    </div>
                    <div className="f-form-control">
                        <label>A little bit about your album</label>
                        <TextArea placeholder="250 characters max" rows={4} />
                    </div>
                </div>
                <div className="f-modal-wiz-footer">
                    <Button className="f-fill">Create Album</Button>
                </div>
            </Modal>

            {/* Modal - Map */}
            <Modal title="Event Location"
                visible={map_visible}
                onCancel={() => map_handleCancel()}
                onOk={() => map_handleCancel()}
                className="f-modal f-modal-wizard">
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div style={{ width: '100%', height: '400px' }}>
                        {eventDetails&&<SimpleMap 
                        eventDetails={eventDetails}
                        center={{ lat: eventDetails.lat, lng: eventDetails.long }} />}
                    </div>
                </div>
            </Modal>

            {/* Modal - Document */}
            <Modal title="Create Folder"
                visible={dc_visible}
                onCancel={() => dc_handleCancel()}
                onOk={() => dc_handleCancel()}
                className="f-modal f-modal-wizard">
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <div className="f-form-control">
                        <label>Folder name</label>
                        <Input placeholder="Eg: Nigeria womens group " />
                    </div>
                    <div className="f-form-control">
                        <label>A little bit about your folder</label>
                        <TextArea placeholder="250 characters max" rows={4} />
                    </div>
                </div>
                <div className="f-modal-wiz-footer">
                    <Button className="f-fill">Create Folder</Button>
                </div>
            </Modal>
            {enableEdit &&
                <div>
                    <Modal
                        title="Add new cover pic"
                        visible={enableEdit}
                        onCancel={() => showCropModel()}
                        footer={null}
                        className="f-modal f-modal-wizard">
                        <CustomImageCrop
                            params={params}
                            callBackimgFromParent={dataCallback}
                        ></CustomImageCrop>
                    </Modal>
                </div>
            }
            <Modal
                visible={online_modal}
                onOk={() => successModal()}
                onCancel={() => successModal()}
                footer={null}
            >
                <div>
                    <h3>Meeting pin has been copied !</h3>
                    <span></span>
                    <h5>Click the below link to join the meeting</h5>
                </div>
                <Linkify  style={{ color:"#1890ff", marginTop: '0px' }} target="_blank"><span onClick={() => { openInNewTab(eventDetails&&eventDetails.meeting_pin) }} id='url_to_open'>{`${eventDetails&&eventDetails.meeting_link}`}</span></Linkify>
            </Modal>
                <Modal
                    visible={eventNotFound}
                    title = "!!OOPS....  Content no longer available"
                    footer={null}
                    closable={true}
                >
                    <div 
                    style={{ display:'flex',justifyContent:'center'}}
                    >
                        <Button
                            onClick={()=>{
                                let {state} = location
                                let path_ = state&&state.prev
                                history.push(state?`${path_}/4`:'/post'
                                )
                            }}
                        >Go Back</Button></div>
                </Modal>
        </section>
    )
}

export default EventDetails