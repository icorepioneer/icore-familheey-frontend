import axios from '../../../services/apiCall';

const getEventDetails = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_event_byId`, data);
}
const responseToEvents = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/respondToEvents`, data)
}
const updateEventDetails = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_events`, data)
}
const listEventAgenda = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/list_agenda`, data)
}
const getGuestsCount = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_guests_count`, data)
}
const getGoingGuestDetails = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_going_guest_details`, data)
}
const cancelOrDeleteFunction = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/event_deleteorcancel`, data)
}
const getSignUpEventItems = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_eventitems`, data)
}
const eventAttendingListApi = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_going_guest_details`, data)
}
const getRsvpGuestDetails = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getRSVP_guest_details`, data)
}
const getInvitedByUser = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/event_invited_byUser`, data)
}
const createContact = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/addEventContact`, data)
}
const updateSelectedContact = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/editEventContact`, data)
}
const deleteContact = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/deleteEventContact`, data)
}
const addEventSignUpItems = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/add_event_signupitems`, data)
}
const getAllEventItems = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_eventitems`, data)
}
const addEventItemsContribution = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/add_eventitems_contribution`, data)
}
const getAllContributionList = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/item_contributor_list`, data)
}
const updateEventitemsContribution = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_eventitems_contribution`, data)
}
const listAllAgenda = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/list_agenda`, data)
}
const createAgenda = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/create_agenda`, data)
}
const updateAgenda = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_agenda`, data)
}
const _listEventFolders = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listEventFolders`, data)
}
const createFolder = (album) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/createFolder`, album)
}
const removeFolder = (album) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/removeFolder`, album)
}
const getguestsCount = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_guests_count`, data)
}
const getUserGroupMembers = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/user_group_members`, data)
}
const getAllFamily = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/viewFamily`, data)
}
const ShareORInvite = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/event_shareor_invite`, data)
}
const getAllEventFolders = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/listEventFolders`, data)
}
const getEventsBasedOnTag = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/get_event_basedOnEventPage`, data)
}
const updateFolder = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/updateFolder`, data)
}
const updateEventSignUpItems = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_event_signupitems`, data)
}

export {
    getAllEventFolders,
    getEventDetails,
    responseToEvents,
    updateEventDetails,
    listEventAgenda,
    getGuestsCount,
    getGoingGuestDetails,
    cancelOrDeleteFunction,
    getSignUpEventItems,
    eventAttendingListApi,
    getRsvpGuestDetails,
    getInvitedByUser,
    createContact,
    updateSelectedContact,
    deleteContact,
    addEventSignUpItems,
    getAllEventItems,
    addEventItemsContribution,
    getAllContributionList,
    updateEventitemsContribution,
    listAllAgenda,
    createAgenda,
    updateAgenda,
    _listEventFolders,
    createFolder,
    removeFolder,
    getguestsCount,
    getUserGroupMembers,
    getAllFamily,
    ShareORInvite,
    getEventsBasedOnTag,
    updateFolder,
    updateEventSignUpItems
}