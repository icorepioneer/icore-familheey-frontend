import React, { useEffect, useState } from 'react';
import { Button, Modal, Form, Input, DatePicker, Upload, Icon, message } from 'antd';
import * as moment from 'moment';
import getStateFromLocal from '../../../../../shared/getUserDetailsFromLocal';
const { TextArea } = Input;
const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        state = {
            _loading: false,
            imageUrl: '',
            startValue: null,
            endValue: null,
        }
        render() {
            const { imageUrl } = this.state;
            const { visible, onCancel, form, loading, imageURL, from_date, to_date,modal_title } = this.props;
            const handleChange = info => {
            let { response } = info.file;
                if (info.file.status === "uploading") {
                    this.setState({ loading: true });
                    return;
                }
                if (info.file.status === "done") {
                    let { data } = response;
                    // Get this url from response in real world.
                    getBase64(info.file.originFileObj, image_url => {
                        this.setState({
                            imageUrl:image_url,
                            loading: false
                        })
                    }
                    );
                    this.setState({ imageUrl: data.filename });
                }
            };
            const changeDefaultImage = () => {
                this.setState({
                    imageUrl: null
                })
            }
            const uploadButton = (
                <div>
                    <Icon
                        style={{ fontSize: "60px", color: "#7e57c2" }}
                        type={this.state.loading ? "loading" : "cloud-upload"}
                    />
                    <div className="ant-upload-text">Upload</div>
                </div>
            );
            
            const beforeUpload = (file) => {
                const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
                if (!isJpgOrPng) {
                    message.error("You can only upload JPG/PNG file!");
                }
                const isLt2M = file.size / 1024 / 1024 < 2;
                if (!isLt2M) {
                    message.error("Image must smaller than 2MB!");
                }
                return isJpgOrPng && isLt2M;
            }
            const getBase64 = (img, callback) => {
                const reader = new FileReader();
                reader.addEventListener("load", () => callback(reader.result));
                reader.readAsDataURL(img);
            }
            const disabledStartDate = startValue => {
                // console.log(moment(startValue).unix(),moment(startValue).unix()>from_date)
                // const { endValue } = this.state;

                // if (!startValue || !endValue) {
                //     return false;
                // }
                return moment(startValue).unix() < from_date;
            };
            const disabledEndDate = endValue => {
                return moment(endValue).unix()>to_date;
                // const { startValue } = this.state;
                // console.log(startValue && startValue.valueOf(), endValue && endValue.valueOf())
                // if (!endValue || !startValue) {
                //     return false;
                // }
                // return endValue.valueOf() <= startValue.valueOf();
            };
            const onStartChange = value => {
                onChange('startValue', value);
            };

            const onEndChange = value => {
                onChange('endValue', value);
            };
            const onChange = (field, value) => {
                this.setState({
                    [field]: value,
                });
            };
            const onOkay = () => {
                this.setState({
                    startValue: null,
                    endValue: null,
                })
                this.props.onCreate(changeDefaultImage)
            }
            const { getFieldDecorator } = form;
            const getExactDate=(epoch)=>{
                let date;
                date = moment(moment(`${moment.unix(epoch).format("MMM DD YYYY")} ${moment.unix(epoch).format("hh:mm a")}`).format())
                return date;
            }
            let button_key = "submit"
            return (
                <Modal
                    visible={visible}
                    title={`${modal_title}`}
                    onCancel={onCancel}
                    onOk={onOkay}
                    className="f-modal f-modal-wizard f-modal-wfooter"
                    footer={[
                        <div key={button_key} className="f-modal-wiz-footer" style={{ marginTop: '0' }}>
                            <Button loading={loading} className="f-fill" onClick={() => { onOkay() }}>Add to Agenda</Button>
                        </div>
                    ]}
                >
                    <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                        <Form layout="vertical">
                            <div className="f-form-control" style={{ marginTop: '0' }}>
                                <div className="f-split" style={{ margin: '0' }}>
                                    <Form.Item label="Start date" hasFeedback>
                                        {getFieldDecorator("start_date", {
                                            initialValue: getExactDate(from_date),
                                            rules: [
                                                {
                                                    required: true,
                                                    message: "Please choose starting date..!"
                                                }
                                            ]
                                        })(<DatePicker
                                            disabledDate={disabledStartDate}
                                            format="MMM DD YYYY hh:mm a "
                                            showTime={true}
                                            onChange={onStartChange}
                                        />)}
                                    </Form.Item>
                                    <Form.Item label="End date" hasFeedback>
                                        {getFieldDecorator("end_date", {
                                            initialValue:getExactDate(to_date),
                                            rules: [
                                                {
                                                    required: true,
                                                    message: "Please choose ending date..!"
                                                }
                                            ]
                                        })(<DatePicker
                                            disabledDate={disabledEndDate}
                                            onChange={onEndChange}
                                            format="MMM DD YYYY hh:mm a"
                                            showTime={true} />)}
                                    </Form.Item>
                                </div>
                            </div>
                            <div className="f-form-control" style={{ marginTop: '0' }}>
                                <Form.Item label="Title" hasFeedback>
                                    {getFieldDecorator("title", {
                                        rules: [
                                            {
                                                required: true,
                                                message: "Please have some title for this agenda..!"
                                            }
                                        ]
                                    })(<Input placeholder="Title" />)}
                                </Form.Item>
                            </div>
                            <div className="f-form-control" style={{ marginTop: '0' }}>
                                <Form.Item label="Description" hasFeedback>
                                    {getFieldDecorator("description", {})(<TextArea
                                        placeholder="250 characters max"
                                        rows={4} />)}
                                </Form.Item>
                            </div>
                            <div className="f-form-control" style={{ marginTop: '0', height: 'auto' }}>
                                <Form.Item hasFeedback>
                                    {getFieldDecorator("document", {})(
                                        <Upload
                                            name="agenda_pic"
                                            listType="picture-card"
                                            className="avatar-uploader"
                                            showUploadList={false}
                                            headers={{
                                                'logined_user_id':`${getStateFromLocal()&&getStateFromLocal()[0]}`,
                                                'Authorization':`Bearer ${getStateFromLocal()&&getStateFromLocal()[1]}`
                                            }}
                                            action={`${process.env.REACT_APP_API_URL}/api/v1/familheey/dummy`}
                                            beforeUpload={beforeUpload}
                                            onChange={handleChange}
                                        >
                                            {imageUrl || imageURL ? (
                                                <img src={imageUrl || imageURL} alt="avatar" style={{ width: "100%" }} />
                                            ) : (
                                                    uploadButton
                                                )}
                                        </Upload>
                                    )}
                                </Form.Item>
                            </div>
                        </Form>
                    </div>
                </Modal>
            );
        }
    },
);

const AddAgenda = ({ aj_visible, event_id, user_id, createAgenda, getAgenda, item, _updateAgenda, from_date, to_date }) => {
    let formReff;
    const [modalState, setModalState] = useState({
        visible: false,
        loading: false,
        imageUrl: '',
        modal_title:'Create agenda'
    })
    let { visible, loading, fileList, fileToDisplay, imageUrl,modal_title } = modalState
    const handleCancel = () => {
        const { form } = formReff.props;
        form.resetFields()
        setModalState(prev => ({ ...prev, visible: false }))
    };

    const handleCreate = (fn) => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            let formData = new FormData();
            item && formData.append('agenda_id', item.id)
            formData.append('event_id', event_id);
            formData.append('user_id', user_id);
            formData.append('title', values.title);
            formData.append('start_date', moment(values.start_date).format());
            formData.append('end_date', moment(values.end_date).format());
            formData.append('description', values.description);
            formData.append('agenda_pic', values.document && values.document.file ? values.document.file.originFileObj :
                (item && item.agenda_pic ? item.agenda_pic : ''))
            setModalState(prev => ({ ...prev, loading: true }))

            item ? (_updateAgenda(item.id, formData).then(res => {
                setModalState(prev => ({ ...prev, loading: false, visible: false, imageUrl: '' }))
                getAgenda();
                fn();
                form.resetFields()
            })) : (createAgenda(formData).then(res => {
                setModalState(prev => ({ ...prev, loading: false, visible: false, imageUrl: '' }))
                getAgenda();
                fn();
                form.resetFields()
                form.setFields({
                    document: { value: null }
                })
            }))
        });
    };

    const saveFormRef = (formRef) => {
        formReff = formRef;
    };
    useEffect(() => {
        return () => {
            setModalState(prev => ({ ...prev, visible: true }))
        }
    }, [aj_visible])
    useEffect(() => {
        const { form } = formReff.props;
        setModalState(prev => ({ ...prev, imageUrl: item.agenda_pic ? process.env.REACT_APP_S3_BASE_URL + 'agenda_pic/' + item.agenda_pic : null,modal_title:"Edit agenda" }))
        item !== '' ? (form.setFields({
            start_date: { value: moment(new Date(item.start_date), 'YYYY-MM-DD HH:mm:ss') },
            end_date: { value: moment(new Date(item.end_date), 'YYYY-MM-DD HH:mm:ss') },
            title: { value: item.title },
            description: { value: ( item.description != 'undefined' ?  item.description :'') },
            // document:{value:<img src={process.env.REACT_APP_S3_BASE_URL + 'agenda_pic/' + item.agenda_pic} alt="avatar" style={{ width: "100%" }} />}
        })
        ) : ((() => {
            form.resetFields()
            setModalState(prev => ({ ...prev,modal_title:"Create agenda", imageUrl: '' }))
        })())
    }, [item])
    return (
        <div>
            <CollectionCreateForm
                wrappedComponentRef={(formRef) => saveFormRef(formRef)}
                visible={visible}
                onCancel={handleCancel}
                onCreate={handleCreate}
                loading={loading}
                fileList={fileList}
                fileToDisplay={fileToDisplay}
                imageURL={imageUrl}
                from_date={from_date}
                to_date={to_date}
                modal_title={modal_title}
            />
        </div>
    );
}

export default AddAgenda;        