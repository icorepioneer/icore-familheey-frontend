import React, { useState, useEffect } from 'react';
import { Button, Timeline, Avatar, Icon, Modal,DatePicker } from 'antd'
import AddAgenda from '../Agenda/createAgenda/index'
import moment from 'moment';
const { confirm } = Modal;
const Agenda = ({ aj_showModal, event_id, listAllAgenda, createAgenda, user_id, aj_visible, updateAgenda,from_date,created_by,to_date }) => {
    const [state, setState] = useState({
        agenda_list: '',
        loading: false,
        current_id: '',
        agenda_to_edit: '',
        date_selected:'',
        oldSetOfData:'',
        agenda_list_:''
    })
    let { agenda_list, loading, current_id, agenda_to_edit,agenda_list_ } = state;
    const getAgenda = async () => {
        let params = {
            event_id: event_id
        }
        await listAllAgenda(params).then(res => {
            setState(prev => ({ ...prev, agenda_list: res.data.data,agenda_list_:res.data.data }))
        })
    }
    const _updateAgenda = async (id,updated) => {
        setState(prev => ({ ...prev, loading: true, current_id: id }))
        console.log(id)
        let params = {
            agenda_id: `${id}`,
            is_active: false
        }

        await updateAgenda(updated?updated:params).then(res => {
            getAgenda();
            setState(prev => ({ ...prev, loading: false }))
        })
    }
    useEffect(() => {
        getAgenda();
    }, [event_id])

    const showConfirm = (id) => {
        confirm({
            title: 'Do you want to delete this agenda?',
            onOk() {
                return new Promise((resolve, reject) => {
                    _updateAgenda(id).then(() => resolve()).catch(err => {
                        reject();
                        console.log(err)
                    })
                })
            },
            onCancel() { },
        });
    }
    const setItemToEdit=(item)=>{
        aj_showModal();
        setState(prev=>({...prev,agenda_to_edit:item}))
    }
    const filterAgendaBasedOndate=(dateObj)=>{
        let selected = moment(dateObj).format('LL')
        let newSetOfData = agenda_list_&&agenda_list_.filter((item)=>moment(item.date).format('LL')==selected)
        let dataToPass = selected!=='Invalid date'?newSetOfData:agenda_list_;
        setState(prev=>({...prev,agenda_list:dataToPass}))
    }
    return (
        <div>
            <div className="f-clearfix">
                {/* Agenda List */}
                <div className="f-event-contents f-blanco">
                    <div className="f-switch-content">
                        <div className="f-content">
                            <div className="f-clearfix">
                                {created_by&&<div className="f-gen-create">
                                    <Button onClick={()=>setItemToEdit('')}>Create Agenda <img src={require('../../../../assets/images/icon/plus.svg')} /></Button>
                                </div>}
                                <div className="f-clearfix">
                                    {/* Timeline */}
                                    <div className="f-agenda">
                                        {/* Day 1 */}
                                        <div className="f-agenda-day-group">
                                            <div className="f-agenda-day" style={{ display: 'flex', justifyContent: 'flex-end' }}>
                                                <DatePicker onChange={filterAgendaBasedOndate}/>
                                            </div>
                                            {
                                            agenda_list && agenda_list.map((item, key) => {
                                                return <Timeline key={key}>
                                                    <h4><strong></strong>{moment(item.date).format('ddd, ll')}</h4>
                                                    {
                                                        item.data.map((cont, i) => {
                                                            return <Timeline.Item key={cont.id}>
                                                                <div className="f-agenda-item">
                                                                    <header className="f-agenda-head">
                                                                        <h5>{moment(new Date(cont.start_date)).format('LT')} - {moment(new Date(cont.end_date)).format('LT')}</h5>
                                                                        <span className="f-group">
                                                                            {created_by&&<Button onClick={()=>setItemToEdit(cont)}>
                                                                                <Icon type="edit" theme="twoTone" twoToneColor="#7e57c2" />
                                                                            </Button>}
                                                                            {created_by&&<Button onClick={() => { showConfirm(cont.id) }}>
                                                                                <Icon type={current_id == cont.id && loading ? "loading" : "delete"} theme="twoTone" twoToneColor="#7e57c2" />
                                                                            </Button>}
                                                                        </span>
                                                                    </header>
                                                                    <section className="f-agenda-sect">
                                                                        <Avatar src={cont.agenda_pic ? process.env.REACT_APP_S3_BASE_URL + 'agenda_pic/' + cont.agenda_pic : require('../../../../assets/images/default_event_image.png')}
                                                                        />
                                                                        <span className="f-group">
                                                                            <h5>{cont.title}</h5>
                                                                            <p>{cont.description !='undefined' ? cont.description :  ''}</p>
                                                                        </span>
                                                                    </section>
                                                                </div>
                                                            </Timeline.Item>
                                                        }
                                                        )}
                                                </Timeline>
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <AddAgenda
                    aj_visible={aj_visible}
                    createAgenda={createAgenda}
                    user_id={user_id}
                    event_id={event_id}
                    getAgenda={getAgenda}
                    item ={agenda_to_edit}
                    _updateAgenda={_updateAgenda}
                    from_date={from_date}
                    to_date={to_date}
                />
            </div>
            {/* Agenda List - END */}

        </div>
    );
}
export default Agenda