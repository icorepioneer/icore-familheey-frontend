import React, { useState, useEffect } from 'react'
import { Button, Input, Card, notification, Modal, Icon } from 'antd'
import * as moment from 'moment'
import FolderDetails from '../FolderDetails';
import lowerCase from "lower-case";
const { Search } = Input


const ListFolders = ({ setFolderToEdit, dc_showModal, eventFolders, removeFolder, user_id, listEventFolders, sh_showModal, created_by, eventDetails, familyDetails }) => {
    const [state, setState] = useState({
        folders: '',
        backUp: '',
        openFolderFlag: false,
        folder_data: '',
        deleteModal: false,
        deleteFolderID: '',
        user_status:'',
        post_create:''
    })
    let { folders, deleteModal, deleteFolderID, backUp, openFolderFlag, folder_data,user_status,post_create } = state;
    const openNotification = (message, description) => {
        notification.open({
            message: `${message}`,
            description: `${description}`,
            onClick: () => {
                console.log("Notification Clicked!");
            }
        });
    };
    const filterFolders = (tag) => {
        let foldersFileterd = backUp.filter(item => lowerCase(item.folder_name).indexOf(lowerCase(tag.target.value)) !== -1)
        setState((prev => ({
            ...prev,
            folders: foldersFileterd,
            openFolderFlag: false
        })))
    }
    const confirmModal = (id) => {

        setState((prev => ({
            ...prev,
            deleteModal: true,
            deleteFolderID: id
        })))
    }
    const handleCancel = () => {
        setState((prev => ({
            ...prev,
            deleteModal: false,
            deleteFolderID: 'id'
        })))
    }
    const deleteFolder = async (id) => {
        let params = {
            folder_id:[`${id}`],
            user_id: `${user_id}`
        }
        await removeFolder(params).then(res => {
            openNotification("Successfully deleted", "")
            listEventFolders();
            setState(prev => ({ ...prev, deleteModal: false }))

        }).catch(err => {
            openNotification("you are not authorised", "Only the creator has the previlege to delete")
        })
    }
    const cbfunction = () => {
        setState(prev => ({ ...prev, openFolderFlag: false }))
    }
    const openFolder = (item) => {
        setState(prev => ({ ...prev, openFolderFlag: true, folder_data: item }))
    }
    useEffect(() => {
        setState(pre => ({ ...pre, folders: eventFolders, backUp: eventFolders, openFolderFlag: false }))
    }, [eventFolders])
    useEffect(()=>{
        familyDetails&&setState(prev=>({...prev,user_status:familyDetails.user_status,post_create:familyDetails.post_create}))
    },[familyDetails])
    return (
        <div className="f-switch-content">
            {!openFolderFlag &&
                <div className="f-content">
                    <div className="f-clearfix">
                        <div className="f-member-listing">
                            <div className="f-content-searchable">
                                {((created_by == user_id || user_status == "admin") ? true : (post_create == 6 ? true : false)) && <Button onClick={dc_showModal} className="f-btn">Create Folder</Button>}
                                <Search onChange={value => {
                                    filterFolders(value)
                                }} placeholder="Discover" />
                            </div>
                            <div className="f-content-searchable">
                                <h4><strong>{folders && folders.length}</strong> Folders</h4>
                            </div>
                            {folders && folders.length == 0 && <div className="f-event-contents">
                                <div className="f-switch-content">
                                    <div className="f-content">
                                        <div className="f-empty-content">
                                            <img src={require('../../../../../assets/images/empty_docs.png')} />
                                            {(eventDetails && eventDetails.created_by == user_id) ?
                                                <React.Fragment>
                                                    <h4>Haven’t added any documents, yet ?</h4>
                                                    <p>Let's create a folder and add one !</p>
                                                    <div className="f-clearfix">
                                                        <Button onClick={() => dc_showModal()}>Create Folder</Button>
                                                    </div>
                                                </React.Fragment>
                                            :<React.Fragment><h4>No Data Found</h4></React.Fragment>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            }
                            <div className="f-doc-listing-cards">
                                {folders && folders.map(item =>
                                    <Card className="f-doc" key={item.id} >
                                    {(item.created_by == user_id || user_status == "admin") &&
                                        <div style={{ display: 'flex', justifyContent: 'flex-end' }}><Icon type="edit" onClick={() => { setFolderToEdit(item) }} /></div>
                                    }
                                        <div className="f-doc-top" onClick={(e) => openFolder(item)}>
                                            <span className="f-doc-count"></span>
                                            <div className="f-doc-title">
                                                <h4>{item.folder_name}</h4>
                                                <h5>By {item.created_by_name}</h5>
                                                <p>{moment(item.created_at).format('LL')}</p>
                                            </div>
                                            {/* <Button className="f-more" onClick={(e) => { e.stopPropagation(); e.nativeEvent.stopImmediatePropagation();}} /> */}
                                        </div>
                                        <div className="f-doc-bot">
                                            <div className="f-left">
                                                {/* <Button className="f-comment"><i /> 15</Button> */}
                                                {/* <Button onClick={sh_showModal} className="f-share" /> */}
                                            </div>
                                            {(item.created_by == user_id || user_status == "admin") &&
                                                <div className="f-right">
                                                    <Button
                                                        // disabled={eventDetails && eventDetails.created_by != user_id}
                                                        onClick={() => {
                                                            // console.log("delete",item)
                                                            confirmModal(item.id)
                                                        }} className="f-trash" />
                                                </div>
                                            }
                                        </div>
                                    </Card>
                                )}
                            </div>
                            <Modal
                                title="Confirm"
                                visible={deleteModal}
                                // onOk={deleteFolder}
                                onCancel={handleCancel}
                                footer={null}
                                className="f-modal f-modal-wizard f-modal-wfooter"
                                footer={[
                                    <div >
                                        <div >
                                            <Button onClick={() => deleteFolder(deleteFolderID)}>DELETE</Button>
                                            <Button onClick={() => handleCancel()} className='f-styled'>Cancel</Button>
                                        </div>
                                    </div>
                                ]}
                            >
                                <h2 >Do you want to delete  </h2>
                            </Modal>
                        </div>
                    </div>
                </div>
            }
            {
                openFolderFlag && <FolderDetails folder_data={folder_data} cbFn={cbfunction} />
            }


        </div>
    );
}
export default ListFolders;