import React, { useState, useEffect } from 'react'
import CreateFolder from './CreateFolderModal/index'
import ListFolders from './ListFolders/index'
const Documents = ({ _listEventFolders, event_id, user_id, removeFolder, folder_for, folder_type, sh_showModal, familyDetails, eventDetails,groupId }) => {
    const [state, setState] = useState({
        dc_visible: false,
        eventFolders: '',
        folder_to_edit: undefined
    });
    let { dc_visible, eventFolders, folder_to_edit } = state;
    const listEventFolders = async () => {
        let params = {
            folder_for: folder_for,
            folder_type: "documents",
            user_id: `${user_id}`,
            // event_id: `${event_id}`,
        }
        if (folder_for == 'groups') {
            params.group_id = event_id;
        } else {
            params.event_id = event_id;
        }
        await _listEventFolders(params).then(res => {
            setState(prev => ({ ...prev, eventFolders: res.data.data }))
        })
    }
    const dc_showModal = () => {
        setState(prev => ({
            ...prev,
            dc_visible: !dc_visible,
        }))
    };
    const dc_handleCancel = e => {
        setState(prev => ({
            ...prev,
            dc_visible: !dc_visible,
        }))
    };
    useEffect(() => {
        listEventFolders();
    }, [])
    const setFolderToEdit = (item) => {
        setState(prev => ({ ...prev, folder_to_edit: item }))
    }
    let { created_by } = familyDetails || eventDetails
    return (
        <div className="f-event-contents f-blanco">
            <ListFolders
                dc_showModal={dc_showModal}
                eventFolders={eventFolders}
                removeFolder={removeFolder}
                folder_for={folder_for}
                folder_type={folder_type}
                user_id={user_id}
                listEventFolders={listEventFolders}
                sh_showModal={sh_showModal}
                created_by={created_by}
                eventDetails={eventDetails}
                setFolderToEdit={setFolderToEdit}
                familyDetails={familyDetails}
            />
            <CreateFolder
                eventDetails={eventDetails}
                familyDetails={familyDetails}
                dc_visible={dc_visible}
                dc_handleCancel={dc_handleCancel}
                user_id={user_id}
                event_id={event_id}
                folder_for={folder_for}
                folder_type={folder_type}
                listEventFolders={listEventFolders}
                folder_to_edit={folder_to_edit}
                groupId={groupId}
                setFolderToEdit={setFolderToEdit}
            />
        </div>
    )
}
export default Documents;