import React, { useState, useEffect } from 'react'
import { Modal, Input, Button, Form, Select, } from 'antd'
import { createFolder, updateFolder } from '../../../eventDetails/api-eventDetails'
import { listAllGroupMember } from '../../../../memberListing/api-memberlist'
import './style.scss';
const { Option } = Select
const { TextArea } = Input
const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        state = {
            _loading: false,
        }
        render() {
            const { visible, onCancel, onCreate, form, loading, dropdown_visible, member_list,onChange_,member_list_visible,modal_title } = this.props;
            const { getFieldDecorator } = form;
            let button_key = "submit"
            // const onChange = (value) => {
            //     console.log(value)
            //     switch (value) {
            //         case 'selected':
            //             this.setState({
            //                 member_list_visible: true
            //             })
            //             break;
            //         default:
            //             this.setState({
            //                 member_list_visible: false
            //             })
            //             break;
            //     }
            // }
            return (
                <Modal
                    className="f-modal f-modal-wizard f-modal-altx"
                    visible={visible}
                    title={`${modal_title}`}
                    okText="Create"
                    onCancel={onCancel}
                    onOk={onCreate}
                    destroyOnClose
                    footer={[
                        <div key={button_key} className="f-modal-wiz-footer">
                            <Button loading={loading} onClick={onCreate} className="f-fill">{`${modal_title}`}</Button>
                        </div>
                    ]}
                >
                    <Form layout="vertical">
                        <Form.Item label="Folder name">
                            {getFieldDecorator('folder_name', {
                                rules: [{ required: true, message: 'Folder without a name...! that\'s not possible...' }],
                            })(<Input placeholder="Eg: Nigeria womens group " />)}
                        </Form.Item>
                        <Form.Item label="A little bit about your folder">
                            {getFieldDecorator('description')(<TextArea placeholder="250 characters max" rows={4} />)}
                        </Form.Item>
                        {dropdown_visible && <Form.Item label="Who can see your folder">
                            {getFieldDecorator('permissions', {
                                initialValue: "all"
                            })(
                                <Select
                                    showSearch
                                    style={{ width: '100%' }}
                                    placeholder="Permission type"
                                    optionFilterProp="children"
                                    onChange={onChange_}
                                    filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    <Option value="all">All Members</Option>
                                    <Option value="selected">Selected Members</Option>
                                    <Option value="only-me">Only me</Option>
                                </Select>
                            )}
                        </Form.Item>}
                        {member_list_visible && <Form.Item label="Select members">
                            {getFieldDecorator('users_id')(
                                <Select
                                    mode="multiple"
                                    showSearch
                                    style={{ width: '100%' }}
                                    placeholder="Member list"
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {member_list && member_list.map((item) => {
                                        return <Option key={item.id} value={item.id}>{item.full_name}</Option>
                                    })}
                                </Select>
                            )}
                        </Form.Item>}
                    </Form>
                </Modal>

            );
        }
    },
);

const CreateFolder = ({ dc_visible, event_id, user_id, listEventFolders, folder_for, folder_to_edit, familyDetails, eventDetails,userDetails, groupId,setFolderToEdit }) => {
    console.log("CreateFolder props event id",event_id)
    let formReff;
    const [modalState, setModalState] = useState({
        visible: false,
        loading: false,
        dropdown_visible: false,
        member_list: '',
        member_list_visible:false,
        modal_title:"Create Folder"
    })
    let { visible, loading, dropdown_visible, member_list,member_list_visible,modal_title } = modalState
    const onChange_ = (value) => {
        switch (value) {
            case 'selected':
                setModalState(prev=>({...prev,dropdown_visible:true,member_list_visible:true}))
                break;
            default:
                setModalState(prev=>({...prev,dropdown_visible:true,member_list_visible:false}))
                break;
        }
    }
    const handleCancel = () => {
        setFolderToEdit(undefined)
        setModalState(prev => ({ ...prev, visible: false,member_list_visible:false,modal_title:"Create Folder" }))
    };

    const handleCreate = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            setModalState(prev => ({ ...prev, loading: true }))
            let data = { ...values, created_by: user_id, folder_type: "documents", folder_for: folder_for }
            let data_to_update
            if (folder_to_edit) {
                data_to_update = { ...values, 
                    id: `${folder_to_edit.id}`,
                    created_by:user_id, 
                    folder_type: "documents", 
                    folder_for: folder_for, 
                    group_id: groupId ? groupId : null, 
                    event_id: eventDetails ? eventDetails.event_id : null }
            }
            if (folder_for == 'groups') {
                data.group_id = event_id;
            } else {
                data.event_id = event_id;
            }
            folder_to_edit?(
                updateFolder(data_to_update).then(res => {
                    form.resetFields();
                    setModalState(prev => ({ ...prev, visible: false, loading: false }));
                    listEventFolders();
                    handleCancel()
                  })):(createFolder(data).then(res => {
                console.log(res)
                form.resetFields();
                setModalState(prev=>({...prev, visible: false, loading: false }));
                listEventFolders();
            }))
        });
    };

    const saveFormRef = (formRef) => {
        formReff = formRef;
    };
    useEffect(() => {
        return () => {
            console.log(dc_visible)
            setModalState(prev => ({ ...prev, visible:true }))
        }
    }, [dc_visible])

    useEffect(() => {
        const { form } = formReff.props;
        folder_to_edit && setModalState(prev => ({ ...prev, 
            visible: true,
            member_list_visible:folder_to_edit.permissions=="selected"?true:false,
            dropdown_visible:folder_to_edit.folder_for=="events"?false:true,
            modal_title:"Update Folder"
         }))
        folder_to_edit ? (form.setFields({
            folder_name: { value: folder_to_edit.folder_name },
            description: { value: folder_to_edit.description },
            permissions: { value: folder_to_edit.permissions },
            users_id: { value: folder_to_edit.permission_users }
        })) : (
                form.resetFields()
            )
    }, [folder_to_edit])
    useEffect(()=>{
        if(member_list_visible&&folder_to_edit&&folder_to_edit.permissions =='selected'){
            const { form } = formReff.props;
            folder_to_edit ? (form.setFields({
                folder_name: { value: folder_to_edit.folder_name },
                description: { value: folder_to_edit.description },
                permissions: { value: folder_to_edit.permissions },
                users_id: { value: folder_to_edit.permission_users }
            })) : (form.resetFields())
        }
    },[member_list_visible])
    useEffect(() => {
        switch (true) {
            case eventDetails != undefined:
                setModalState(prev => ({ ...prev, dropdown_visible: false }))
                break;
            case familyDetails != undefined:
                setModalState(prev => ({ ...prev, dropdown_visible: true }))
                break;
            default:
                setModalState(prev => ({ ...prev, dropdown_visible: false }))
                break;
        }
    }, [eventDetails, familyDetails])
    const getAllMembers = () => {
        var _params = {
            crnt_user_id: `${user_id}`,
            group_id: `${groupId}`,
        }
        listAllGroupMember(_params).then(res => {
            setModalState(prev => ({ ...prev, member_list: res.data.data }))
        })
    }
    useEffect(() => {
        getAllMembers();
    }, [groupId])
    return (
        <div>
            <CollectionCreateForm
                wrappedComponentRef={(formRef) => saveFormRef(formRef)}
                visible={visible}
                onCancel={handleCancel}
                onCreate={handleCreate}
                loading={loading}
                dropdown_visible={dropdown_visible}
                member_list={member_list}
                onChange_={onChange_}
                member_list_visible={member_list_visible}
                modal_title={modal_title}
            />
        </div>
    );
}

export default CreateFolder;        