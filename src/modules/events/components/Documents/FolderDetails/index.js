import React, { useState, useEffect } from 'react'
import { Button, Modal, Card, Icon, Menu, message, Dropdown } from 'antd'
import { viewImages, uploadImages, deleteImage as deleteFile } from '../../../../AlbumModal/album-api';
import * as moment from 'moment'
import { useSelector } from 'react-redux'
// import config from '../../../../../config/config';
// import lowerCase from "lower-case";
// const { Search } = Input
let formData = new FormData();
// const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const FolderDetails = ({ dc_showModal, cbFn, folder_data }) => {
    const [state, setState] = useState({
        user_id: useSelector(red_state=>red_state.user_details.userId),
        openFolderFlag: false,
        folder_data: undefined,
        post_attachment: [],
        al_visible: false,
        btnLoad: false
    })
    let { user_id, document_data, al_visible, post_attachment, btnLoad } = state;

    // const openFolder = () => {
    //     // setEventState(prev => ({ ...prev, openFolderFlag: true }))
    // }
    const al_handleCancel = e => {
        setState(prev => ({ ...prev, al_visible: false,post_attachment:[],btnLoad:false }))
        getFiles();
    }
    const addDoc = () => {
        setState(prev => ({ ...prev, al_visible: true }))

    }
    const openDoc = (url) => {
        window.open(
            window.location.href = url,
            '_blank' // <- This is what makes it open in a new window.
        );
    }

    const uploadImg = async () => {
        formData.append('folder_id', `${folder_data.id}`);
        formData.append('user_id', `${user_id}`);
        formData.append('is_sharable', true);
        return new Promise(async(resolve,reject)=>{
        await uploadImages(formData)
            .then(response => {
                // getFiles();
                resolve(response)
                formData.delete('Documents');
                formData.delete('file_name');
                formData.delete('folder_id');
                formData.delete('user_id');
                formData.delete('is_sharable');
                setState(prev => ({ ...prev,
                    //  post_attachment: [],
                      btnLoad: false }))
                message.success('Document uploaded successfully');
                // al_handleCancel();
            }).catch(err=>{
                reject(err)
            })
        })

    }
    const uploadPhoto = async (e) => {
        let files = e.target.files
        // albumFile = e.target.files[0];
        let _post_attachment = post_attachment
        let current_key;
        for (let file of files) {
            formData.append('Documents', file);
            formData.append('file_name', file.name)
            const reader = new FileReader()
            reader.readAsDataURL(file)
            reader.addEventListener('load',()=>{
                current_key = `${Math.random().toString(36).substring(7)}`
                _post_attachment.push({ base_64: reader.result, _base_64: current_key, _file: file,image_loader:true });
                setState(prev => ({ ...prev, post_attachment: _post_attachment }))
            })
            await uploadImg().then(resp => {
                let id = resp.data.document.id
                getBase64(file, imageUrl => {
                    let _post_attachment_ = post_attachment.filter(data=>data._base_64 == current_key)
                    _post_attachment_[0].image_loader = false
                    _post_attachment_[0].document_id = id;
                    post_attachment = [ ...post_attachment, ..._post_attachment_]
                    post_attachment = post_attachment.filter((obj, pos, arr) => {
                        return arr.map(mapObj => mapObj._base_64).indexOf(obj._base_64) == pos;
                    });
                    setState(prev => ({ ...prev, post_attachment: post_attachment }))
                });
            })
        }
        setState(prev => ({ ...prev, btnLoad: true }))
    }
    const getBase64 = async(img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }
    const getFiles = () => {
        viewImages({ user_id: user_id, folder_id: folder_data.id.toString() })
            .then(docs => {
                setState(prev => ({ ...prev, document_data: docs.data.documents }))

            });
    }
    const _deleteFile = async( id ) => {
        return new Promise(async(resolve,reject)=>{
            await deleteFile({ id: [id] }).then(res => {
                let flag = true;
                return flag;
            }).then(flag => {
                getFiles();
                resolve(flag)
            }).catch(err => {
                reject(err)
            })
        }) 
    }
    const menu = (item) => (

        <Menu>
            {item.user_id == user_id && <Menu.Item onClick={() => { _deleteFile(item.id) }}>
                Delete
            </Menu.Item>}
        </Menu>
    );

    const Overlay = (item) => (
        <Dropdown overlay={menu(item)}>
            <Icon type="dash" rotate={90}></Icon>
        </Dropdown>
    )

    const removeImage = (file, i) => {
        console.log(file)
        post_attachment = post_attachment.filter(item => item._base_64 != file._base_64)
        _deleteFile(file.document_id).then(resp=>{
            setState(prev => ({ ...prev, post_attachment: post_attachment }))
        }).catch(err=>{
            message.warning('Error happened while deleting file')
        })
    }
    // const filterPost = ()=>{
    //     let post_filter_true = post_attachment.filter(data =>data.image_loader)
    //     let post_filter_false = post_attachment.filter(data =>!data.image_loader)
    //     let actual = post_attachment.length 
    //     switch(true){
    //         case actual == 0:
    //             return false
    //         case post_filter_false == actual:
    //             return true 
            
    //     }
    // }
    useEffect(() => {
        getFiles();
        formData.has('folder_id') && formData.delete('folder_id');
        formData.has('user_id') && formData.delete('user_id');
        formData.has('is_sharable') && formData.delete('is_sharable');
        folder_data && setState(prev => ({ ...prev, folder_data: folder_data }))
    }, [folder_data])
    return (
        <div className="f-clearfix">
            <div className="f-event-contents">
                <div className="f-switch-content">
                    <div className="f-content">
                        <div className="f-clearfix">
                            <div className="f-member-listing">
                                <div className="f-content-searchable">
                                    <Button type="link" className="f-btn-back" onClick={() => cbFn()}><Icon type="left" /> Back to folders</Button>
                                    {/* <Search placeholder="discover" /> */}
                                </div>
                                <div className="f-doc-details" >
                                    <div className="f-doc-name">
                                        <div>
                                            <h4>{folder_data.folder_name}</h4>
                                            <h5>Created by {folder_data.created_by_name} on {moment(folder_data.created_at).format('MMM DD, YYYY')} </h5>
                                        </div>
                                        <div style={{ paddingLeft: '15px' }}>
                                            <Button className="f-button" onClick={addDoc}>Add Document</Button>
                                        </div>
                                    </div>
                                    <p>{folder_data.description}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="f-clearfix">
                {
                    document_data && document_data.length == 0 &&
                    <div className="f-event-contents" style={{ marginTop: '20px' }}>
                        <div className="f-switch-content">
                            <div className="f-content">
                                <div className="f-empty-content">
                                    <img src={require('../../../../../assets/images/icon/add_doc.png')} style={{ borderRadius: '0' }} />
                                    <h4>You haven't added any documents, yet!</h4>
                                    <div className="f-clearfix">
                                        <Button onClick={addDoc}>Add Document</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }


                <div className="f-event-contents f-blanco" style={{ marginTop: '20px' }}>
                    <div className="f-switch-content">
                        <div className="f-content">
                            <div className="f-album-listing-cards">
                                {
                                    document_data && document_data.map((docs, index) => {
                                        return (
                                            <Card
                                                className="f-album f-doc-crd"
                                                key={index}
                                                extra={Overlay(docs)}
                                            >
                                                <div className="f-bot" onClick={() => openDoc(docs.url)}>
                                                    {(docs.file_type.indexOf('pdf') == -1) &&
                                                        <h4><img src={require('../../../../../assets/images/icon/icon-doc.png')} /> {docs.original_name}</h4>}
                                                    {(docs.file_type.indexOf('pdf') != -1) && <h4><img src={require('../../../../../assets/images/icon/icon-pdf.png')} /> {docs.original_name}</h4>}
                                                    <p>{moment(docs.created_at).format('MMM DD,YYYY')}</p>
                                                </div>
                                            </Card>);
                                    })
                                }


                            </div>
                        </div>
                    </div>
                    <Modal title="Add Documents"
                        visible={al_visible}
                        onCancel={() => al_handleCancel()}
                        onOk={() => al_handleCancel()}
                        className="f-modal f-modal-wizard"
                        footer={[]}
                    >
                        <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                            <div className="f-form-control">
                                <input
                                    accept=".odt,.doc,application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf"
                                    id="input-file"
                                    onChange={(event) => uploadPhoto(event)}
                                    multiple
                                    type="file"
                                    className="hide" />
                                <ul className="f-upload-setup">
                                    <li className="f-first"><label htmlFor="input-file"><img src={require('../../../../../assets/images/icon/attachment.png')} /></label></li>
                                    {post_attachment && post_attachment.map((file, i) => {
                                        return (<li key={i}>
                                            <img src={require('../../../../../assets/images/icon/doc_b.png')} />
                                            <Button onClick={() => removeImage(file, i)} />
                                        </li>)
                                    })}
                                </ul>
                            </div>
                        </div>
                        <div className="f-modal-wiz-footer">
                            {btnLoad&& <Button className="f-fill" onClick={(e) => al_handleCancel()}>Done</Button>}
                        </div>
                    </Modal>
                </div>

            </div>
        </div>
    );
}
export default FolderDetails;