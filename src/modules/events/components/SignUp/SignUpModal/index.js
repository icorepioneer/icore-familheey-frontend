import React, { useEffect, useState } from 'react'
import { Button, Modal, Form, InputNumber } from 'antd';
// import { id } from 'postcss-selector-parser';
const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form, loading,item } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="please enter your contribution"
                    onCancel={onCancel}
                    onOk={onCreate}
                    footer={[
                        <Button key="back" onClick={onCancel}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={loading} onClick={onCreate}>Done</Button>,
                    ]}>
                        <h4>{item.slot_title} </h4>
                        <p> {item.slot_description}</p>
                        <p>{item.needed} of {item.item_quantity} available</p>
                    <Form layout="inline">
                        <Form.Item label="please enter no: of items" hasFeedback>
                            {getFieldDecorator("contribute_item_quantity", {
                                rules: [
                                    {
                                        required: true,
                                        message: "Please input some numbers.. !"
                                    }
                                ],
                                initialValue :`${item.each_quantity?item.each_quantity:0}`
                            })(<InputNumber min={0} max={item.each_quantity?item.each_quantity+parseInt(item.needed):parseInt(item.needed)} />)}
                        </Form.Item>
                    </Form>
                </Modal>
            );
        }
    },
);
const SignUpModal = ({signUpModalVisible,item,event_id,user_id,addEventItemsContribution,getEventItems,updateEventitemsContribution,items_to_pass,_getAllContributionList,from_date,to_date}) => {
    let formReff;
    const [currentState, setCurrentState] = useState({
        visible: false,
        loading:false
    })
    let { visible,loading } = currentState;
    const handleCancel=()=> {
        setCurrentState(prev=>({
            ...prev,
            visible: false,
        }))
    };
    const saveFormRef = (formRef) => {
        formReff = formRef;
    };
    const handleCreate = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            setCurrentState(prev=>({...prev,loading:true}))
            let { contribute_item_quantity } = values
            let data = {
                contribute_items_id:`${item.id}`,
                contribute_item_quantity:`${contribute_item_quantity}`,
                contribute_user_id:`${user_id}`,
                event_id:`${event_id}`
            }
            let update_data = {
                id:`${item.contr_id}`,
                contribute_item_quantity:`${contribute_item_quantity}`
            }
            !item.update?(addEventItemsContribution(data).then(res=>{
                getEventItems()
                setCurrentState(prev=>({...prev,loading:false,visible:false}))
            })):(
                updateEventitemsContribution(update_data).then(res=>{
                    getEventItems()
                    _getAllContributionList(items_to_pass)
                    setCurrentState(prev=>({...prev,loading:false,visible:false}))
                })
            )
        });
    };
    useEffect(()=>{
        return ()=>{
            setCurrentState(prev=>({...prev,visible:true}))
        }
    },[signUpModalVisible])
    return (
        <CollectionCreateForm
                wrappedComponentRef={(formRef) => saveFormRef(formRef)}
                visible={visible}
                onCancel={handleCancel}
                onCreate={handleCreate}
                loading={loading}
                item={item}
                from_date={from_date}
                to_date = {to_date}
            />
    );
}

export default SignUpModal;