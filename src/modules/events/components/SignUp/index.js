import React,{useState} from 'react';
import SignUpModal from './SignUpModal/index'
import AddSignUPItem from './AddSignUpItemModal/index'
import { Button, Input, Card, Avatar, Icon } from 'antd'
import * as moment from 'moment'

const { TextArea } = Input
const SignUp = ({
    su_showModal,
    addEventSignUpItems,
    _visible,
    event_id,
    user_id,
    getAllEventItems,
    updateSignupItems,
    event_items,
    view_SignUP_visible,
    current_modal_id,
    _viewSignUpModal,
    showSignUpModal,
    event_item_contributions,
    getEventItems,
    addEventItemsContribution,
    updateEventitemsContribution,
    signUp_visible,
    current_SignUp_item,
    items_to_pass,
    _getAllContributionList,
    from_date,
    to_date,
    created_by,
    updateEventSignUpItems
}) => {
    const [state,setState] = useState({signUpToEdit:undefined})
    let {signUpToEdit} = state;
    const setItemToEdit =(item)=>{
        setState(prev=>({...prev,signUpToEdit:item}))
    }
    const toggleItemToEdit =()=>{
        setState(prev=>({...prev,signUpToEdit:undefined}))
    }
    let _event_item_contributions = event_item_contributions && event_item_contributions.filter(item => item.quantity_collected > 0)
    return (
        <div className="f-clearfix">
            {/* Filled List */}
            <div className="f-event-contents f-blanco">
                <div className="f-switch-content">
                    <div className="f-content">
                        <div className="f-clearfix">
                            <div className="f-gen-create">
                                {created_by == user_id &&
                                    <Button onClick={su_showModal}>Add Sign Ups <img src={require('../../../../assets/images/icon/plus.svg')} /></Button>
                                }
                            </div>
                            <AddSignUPItem
                                signUpToEdit={signUpToEdit}
                                addEventSignUpItems={addEventSignUpItems}
                                signUpModalVisible={_visible}
                                event_id={event_id}
                                user_id={user_id}
                                getAllEventItems={getAllEventItems}
                                updateSignupItems={updateSignupItems}
                                from_date={from_date}
                                to_date={to_date}
                                toggleItemToEdit={toggleItemToEdit}
                                updateEventSignUpItems={updateEventSignUpItems}
                            />
                            {/* <div className="f-signup-description">
                                <h4>Sign Up Drescription</h4>
                                <TextArea rows={2} placeholder="Please sign up if you can help during this event." />
                            </div> */}
                        </div>
                        <div className="f-signup-list">
                            <div className="f-clearfix" style={{ marginBottom: '25px' }}>
                                <h4 className="f-clearfix">Sign Up Description</h4>
                                <p className="f-clearfix">Please sign up if you can help during this event.</p>
                            </div>
                            <h4 className="f-clearfix">Sign up list</h4>
                            <div className="f-signup-cards">
                                {event_items && event_items.map((item) =>
                                    <React.Fragment key={item.id}>
                                        <Card >
                                            <div style ={{display:'flex',justifyContent:'space-between'}}>
                                                <h4>{item.slot_title}</h4>
                                                <Icon onClick={()=>setItemToEdit(item)} type="edit" />
                                            </div>
                                            <div className="f-flex">
                                                <div className="f-left">
                                                    <p><em>{item.slot_description}</em></p>
                                                    <p>{moment(item.start_date).format("ddd,MMM Do YYYY")} - {moment(item.end_date).format("ddd,MMM Do YYYY")}</p>
                                                </div>
                                                {item.needed > 0 ? <div className="f-right">
                                                    <p><strong>{item.needed}</strong> of {item.item_quantity}</p>
                                                    <p style={{ minWidth: '65px' }}><em>Available</em></p>
                                                </div> : <div className="f-right">
                                                        <span>Filled</span>
                                                    </div>}
                                            </div>
                                            <div className="f-bottom">
                                                <Button className="f-gray"
                                                    disabled={item.needed === `${item.item_quantity}` ? true : false}
                                                    onClick={() => {
                                                        let status = view_SignUP_visible && current_modal_id == item.id ? 'Hide' : 'View Sign Up';
                                                        _viewSignUpModal({ ...item, status: status })
                                                    }}>{view_SignUP_visible && current_modal_id == item.id && _event_item_contributions.length > 0 ? 'Hide' : 'View Sign Up'}</Button>
                                                {item.needed > 0 && <Button className="f-green" onClick={() => showSignUpModal(item)}>Sign Up</Button>}
                                            </div>
                                        </Card>
                                        {current_modal_id == item.id && view_SignUP_visible && _event_item_contributions.length > 0 && <Card key={item.contr_id}>
                                            {
                                                _event_item_contributions && _event_item_contributions.map((items, i) => {
                                                    return (<div
                                                        key={i}
                                                        style={{
                                                            display: 'flex',
                                                            borderRadius: 2,
                                                            marginTop: '20px',
                                                            justifyContent: 'space-between',
                                                            backgroundColor: '#fff',
                                                            padding: '10px',
                                                            width: '100%'
                                                        }}>
                                                        <div style={{ width: 'auto', height: 'auto' }}>
                                                            <Avatar
                                                                src={process.env.REACT_APP_S3_BASE_URL + 'propic/' + items.propic}
                                                                shape="square"
                                                                size={110}
                                                                icon="user"
                                                            />
                                                        </div>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', width: '90%', flexWrap: 'wrap', paddingLeft: '10px' }}>
                                                            <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between' }}>
                                                                <span style={{ marginLeft: '1%' }}>{items.full_name}</span>
                                                                <Button disabled={items.user_id == user_id ? false : true}
                                                                    icon={'edit'} onClick={() => showSignUpModal({
                                                                        ...item,
                                                                        each_quantity: items.quantity_collected,
                                                                        update: true,
                                                                        contr_id: items.contr_id
                                                                    })}>
                                                                    {items.quantity_collected}
                                                                </Button>
                                                            </div>
                                                            <div style={{ marginLeft: '1%', textAlign: 'left' }}><span>Responded on : {moment(items.updated_at).format("MMM Do YY")}</span></div>
                                                        </div>
                                                    </div>)
                                                }
                                                )
                                            }
                                        </Card>}
                                    </React.Fragment>
                                )}
                                <SignUpModal
                                    getEventItems={getEventItems}
                                    addEventItemsContribution={addEventItemsContribution}
                                    updateEventitemsContribution={updateEventitemsContribution}
                                    user_id={user_id}
                                    event_id={event_id}
                                    signUpModalVisible={signUp_visible}
                                    item={current_SignUp_item}
                                    items_to_pass={items_to_pass}
                                    _getAllContributionList={_getAllContributionList}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* Filled List - END */}

        </div>
    )
}

export default SignUp;