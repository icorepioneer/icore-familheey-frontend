import React, { useEffect, useState } from 'react';
import { Button, Modal, Form, Input, Select, DatePicker } from 'antd';
import * as moment from 'moment'
const { Option } = Select;
const { TextArea } = Input;

const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form, loading, to_date, title, initialValue } = this.props;
            const { getFieldDecorator } = form;
            const disabledStartDate = startValue => {

                return moment(startValue).unix() < moment().unix()
                    ||
                    moment(startValue).unix() >= moment(moment.unix(to_date).add('1', 'day').format()).unix()
                // ||
                // moment(startValue).unix()<moment.unix(from_date);
            };
            const disabledEndDate = endValue => {
                return moment(endValue).unix() > moment(moment.unix(to_date).add('1', 'day').format()).unix() || moment(endValue).unix() < moment().unix();
            };
            const customCountValidator = (rule, value, callback) => {
                if (value < 0) {
                    return callback('Please provide valid input')
                }
                callback()
            }
            return (
                <Modal
                    visible={visible}
                    title={`${title} Sign Ups`}
                    onCancel={onCancel}
                    onOk={onCreate}
                    footer={[
                        <Button key="back" onClick={onCancel}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={loading} onClick={onCreate}>{`${title}`}</Button>,
                    ]}>
                    <Form layout="vertical">
                        <Form.Item label="Title" hasFeedback>
                            {getFieldDecorator("slot_title", {
                                rules: [
                                    {
                                        required: true,
                                        message: "Please input title..!"
                                    }
                                ]
                            })(<Input placeholder="Ex: Soft drinks, Volunteers etc" />)}
                        </Form.Item>
                        <Form.Item label="Description" hasFeedback>
                            {getFieldDecorator("slot_description", {})(<TextArea placeholder="Ex: Case of 1 each, College graduates to welcome guests... etc" style={{ width: "100%" }} />)}
                        </Form.Item>
                        <Form.Item label="Count" hasFeedback >
                            {getFieldDecorator("item_quantity", {
                                initialValue: initialValue,
                                rules: [
                                    {
                                        required: true,
                                        message: "Please input how much you need..!"
                                    },
                                  { validator:customCountValidator,}
                                ],
                            })(<Input
                                min={0}
                                type="number"
                            />)}
                        </Form.Item>
                        <div style={{
                            display: 'flex', justifyContent: 'space-around'
                        }}>
                            <Form.Item label="Start date & time" hasFeedback>
                                {getFieldDecorator("start_date", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please choose start date"
                                        }
                                    ]
                                })(<DatePicker disabledDate={disabledStartDate} format={"MMM DD YYYY"} showTime={true} />)}
                            </Form.Item>
                            <Form.Item label="End date & time" hasFeedback>
                                {getFieldDecorator("end_date", {
                                })(<DatePicker disabledDate={disabledEndDate} format={"MMM DD YYYY"} showTime={true} />)}
                            </Form.Item>
                        </div>
                    </Form>
                </Modal>
            );
        }
    },
);

const AddSignUPItem = ({ toggleItemToEdit, signUpToEdit, signUpModalVisible, addEventSignUpItems, event_id, user_id, updateSignupItems, from_date, to_date, updateEventSignUpItems, getAllEventItems }) => {
    let formReff;
    const [modalState, setModalState] = useState({
        visible: false,
        loading: false,
        item_to_edit: undefined,
        title: "Add",
        initialValue: 0
    })
    let { visible, loading, item_to_edit, title, initialValue } = modalState
    const handleCancel = () => {
        setModalState(prev => ({ ...prev, visible: false, title: 'Add' }))
        toggleItemToEdit();
    };

    const handleCreate = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            setModalState(prev => ({ ...prev, loading: true }))
            let data = { ...values, event_id: `${event_id}`, user_id: `${user_id}` }
            createEventSignUP(data, form)
        });
    };
    const createEventSignUP = (data, form) => {
        addEventSignUpItems(data).then(res => {
            // updateSignupItems(res.data.data[0])
            form.resetFields()
            setModalState(prev => ({ ...prev, loading: false, visible: false }))
        })
        getAllEventItems()
    }
    const handleUpdate = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            console.log(values)
            if (err) {
                return;
            }
            setModalState(prev => ({ ...prev, loading: true }))
            let data = { ...values, id: `${item_to_edit.id}`, event_id: `${event_id}`, user_id: `${user_id}` }
            updateEventSignUp(data, form)
        });
    };
    const updateEventSignUp = (data, form) => {
        updateEventSignUpItems(data).then(res => {
            // updateSignupItems(res)
            form.resetFields()
            setModalState(prev => ({ ...prev, loading: false, visible: false }))
            getAllEventItems()
        })
    }
    const saveFormRef = (formRef) => {
        formReff = formRef;
    };
    useEffect(() => {
        return () => {
            setModalState(prev => ({ ...prev, visible: true }))
        }
    }, [signUpModalVisible])
    useEffect(() => {
        setModalState(prev => ({ ...prev, item_to_edit: signUpToEdit }))
    }, [signUpToEdit])
    useEffect(() => {
        const { form } = formReff.props;
        item_to_edit && setModalState(prev => ({ ...prev, visible: !visible, title: "Update", initialValue: item_to_edit.item_quantity }))
        item_to_edit ? form.setFields({
            slot_title: { value: item_to_edit.slot_title },
            slot_description: { value: item_to_edit.slot_description },
            item_quantity: { value: item_to_edit.item_quantity },
            start_date: { value: moment(item_to_edit.start_date) },
            end_date: { value: moment(item_to_edit.end_date) },
        }) : form.resetFields()
    }, [item_to_edit])
    return (
        <div>
            <CollectionCreateForm
                wrappedComponentRef={(formRef) => saveFormRef(formRef)}
                visible={visible}
                onCancel={handleCancel}
                onCreate={title == 'Add' ? handleCreate : handleUpdate}
                loading={loading}
                from_date={from_date}
                to_date={to_date}
                title={title}
                initialValue={initialValue}
            />
        </div>
    );
}

export default AddSignUPItem;        