import React, {  useState } from "react";
import { Button } from "antd";
import * as moment from 'moment'
const AllSignUpItems = ({ items, item }) => {
    console.log(items)
    const [states, setState] = useState({
        visible: false,
        current_id: ''
    })
    let { visible } = states
    const showDrawer = (id) => {
        setState(prev => ({
            ...prev,
            current_id: id,
            visible: !visible
        }))
    };
    // const onClose = () => {
    //     setState(prev => ({
    //         visible: false
    //     }))
    // };
    return (
        <div>
            <div
                style={{
                    position: "relative",
                    border: "1px solid #ebedf0",
                    borderRadius: 2,
                    padding: 48,
                    textAlign: "center",
                    background: "#fafafa"
                }}
            >
                        <div
                            style={{
                                display: 'flex',
                                borderRadius: 2,
                                marginTop: '20px',
                                justifyContent: 'space-between',
                                backgroundColor: '#fff',
                                padding: '10px',
                                width: '100%'
                            }}>
                            <div style={{ width: '33%', height: '100px' }}>
                                <img style={{ width: 'auto', height: '100px' }} src={process.env.REACT_APP_S3_BASE_URL + 'propic/' + items.propic} ></img>
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '90%', flexWrap: 'wrap' }}>
                                <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between' }}>
                                    <span style={{ marginLeft: '1%' }}>{items.full_name}</span>
                                    <Button  icon={'edit'} onClick={() => showDrawer(items.contr_id)}>
                                        {items.quantity_collected}
                                    </Button>
                                </div>
                                <div style={{ marginLeft: '1%', textAlign: 'left' }}><span>responded on : {moment(items.updated_at).format("MMM Do YY")}</span></div>
                            </div>
                        </div> 
            </div>
        </div>
    );
}

export default AllSignUpItems