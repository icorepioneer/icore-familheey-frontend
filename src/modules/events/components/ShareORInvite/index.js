import React, { useState, useEffect } from 'react'
import { Modal, Card, Checkbox, Button, Radio, notification } from 'antd'
import './style.scss';
import CollectionCreateForm from './Others/others'
const ShareOrInviteModel = ({ sh_visible, sh_handleCancel, getUserGroupMembers, getAllFamily, user_id, ShareORInvite, event_id, status, sh_setLoader }) => {
    let formReff;
    const saveFormRef = (formRef) => {
        formReff = formRef;
    };
    const [state, setState] = useState({
        Tab: '',
        userData: [],
        FamData: [],
        selectedUserIDs: [],
        selectedGroupIDs: [],
        currentTab: '',
        _loading: false
    })
    let { Tab, userData, FamData, selectedUserIDs, selectedGroupIDs, currentTab, _loading } = state;
    const openNotification = (event) => {
        let placement = 'bottomRight'
        notification.info({
            message: `Error happened while ${event}ing`,
            description: '',
            placement,
        });
    };
    const handleRadioChange = (e) => {
        setState(prev => ({ ...prev, Tab: e.target.value }))
        if (e.target.value === '1') {
            setState(prev => ({ ...prev, currentTab: 'Family', selectedGroupIDs: [] }))
            let params;
            let _params = {
                user_id: `${user_id}`,
            }
            let params_ = {
                user_id: `${user_id}`,
                event_id: `${event_id}`
            }

            params = status == 'invite' ? params_ : _params
            getAllFamily(params).then(res => {
                setState(prev => ({ ...prev, FamData: res.data.data }))
            })
        }
        else if (e.target.value === '2') {
            setState(prev => ({ ...prev, currentTab: 'People', selectedUserIDs: [] }))
            let params;
            let _params = {
                user_id: `${user_id}`,
            }
            let params_ = {
                user_id: `${user_id}`,
                event_id: `${event_id}`
            }

            params = status == 'invite' ? params_ : _params
            getUserGroupMembers(params).then(res => {
                setState(prev => ({ ...prev, userData: res.data.data }))
            })
        }
        else if (e.target.value === '3') {
            setState(prev => ({ ...prev, currentTab: 'Others' }))
        }
    }
    const userHandleSelection = (id) => {
        let idArray = selectedUserIDs;
        idArray.includes(id) ? idArray.pop(id) : idArray.push(id)
        setState(prev => ({ ...prev, selectedUserIDs: idArray }))
    }
    const GroupHandleSelection = (id) => {
        let idArray = selectedGroupIDs;
        idArray.includes(id) ? idArray.pop(id) : idArray.push(id)
        setState(prev => ({ ...prev, selectedGroupIDs: idArray }))
    }
    const shareORInvite = () => {
        sh_setLoader()
        switch (currentTab) {
            case 'Family':
                let paramsFamily = {
                    event_id: `${event_id}`,
                    from_user: user_id,
                    view: "template1",
                    type: status == 'invite' ? 'invitation' : status,
                    group_id: selectedGroupIDs
                }
                setState(prev => ({ ...prev, _loading: true }))
                ShareORInvite(paramsFamily).then(res => {
                    let params;
                    let _params = {
                        user_id: `${user_id}`,
                    }
                    let params_ = {
                        user_id: `${user_id}`,
                        event_id: `${event_id}`
                    }
        
                    params = status == 'invite' ? params_ : _params
                    getAllFamily(params).then(res1 => {
                        setState(prev => ({ ...prev, FamData: res1.data.data, _loading: false,Tab:3 }))
                        sh_handleCancel()
                    })
                }).catch(err => {
                    openNotification("shar")
                })

                break;
            case 'People':
                let paramsUsers = {
                    event_id: `${event_id}`,
                    from_user: user_id,
                    view: "template1",
                    type: status == 'invite' ? 'invitation' : status,
                    user_id: selectedUserIDs
                }
                setState(prev => ({ ...prev, _loading: true }))
                ShareORInvite(paramsUsers).then(res => {
                    let params;
                    let _params = {
                        user_id: `${user_id}`,
                    }
                    let params_ = {
                        user_id: `${user_id}`,
                        event_id: `${event_id}`
                    }
        
                    params = status == 'invite' ? params_ : _params
                    getUserGroupMembers(params).then(res2 => {
                        setState(prev => ({ ...prev, userData: res2.data.data, _loading: false,Tab:3 }))
                        sh_handleCancel()
                    })
                }).catch(err => {
                    openNotification("shar")
                })
                break;
            case 'Others':
                const { form } = formReff.props;
                form.validateFields((err, values) => {
                    if (err) {
                        return;
                    }
                    let { prefix, phone, name, email } = values
                    let country_code = prefix.slice(prefix.indexOf('+'))
                    let phone_code = `${country_code}${phone}`
                    let _paramsUsers = {
                        event_id: `${event_id}`,
                        from_user: user_id,
                        view: "template1",
                        type: status == 'invite' ? 'invitation' : status,
                        others: [{
                            full_name: name,
                            email: email,
                            phone: phone_code
                        }]
                    }
                    setState(prev => ({ ...prev, _loading: true }))
                    ShareORInvite(_paramsUsers).then(res => {
                        setState(prev => ({ ...prev, _loading: false,Tab:3 }))
                        sh_handleCancel()
                    }).catch(err1 => {
                        setState(prev => ({ ...prev, _loading: false }))
                        openNotification("Error occurred")
                    })
                });
                break;
        }
    }
    const clearAll = () => {
        setState(prev => ({ ...prev, selectedUserIDs: [], Tab: 3, selectedGroupIDs: [], FamData: [], userData: [] }))
        sh_handleCancel()
    }
    const onResetButtonCLicked = () => {
        let tabState;
        switch (currentTab) {
            case 'Others':
                tabState = 3;
                const { form } = formReff.props;
                form.resetFields()
                break;
            case 'People':
                tabState = 2;
                break;
            case 'Family':
                tabState = 1
                break;
        }
        setState(prev => ({ ...prev, selectedUserIDs: [], Tab: tabState, selectedGroupIDs: [] }))
    }
    useEffect(() => {
        setState(prev => ({ ...prev, Tab: 3, currentTab: 'Others', FamData: [], userData: [] }))
    }, [status])
    return (
        <Modal
            destroyOnClose={true}
            title={status}
            visible={sh_visible}
            onCancel={clearAll}
            onOk={() => sh_handleCancel()}
            className="f-modal f-modal-altx f-modal-wizard f-modal-altxz"
            footer={
                [<div key="submit" className="f-modal-wiz-footer f-flx-start">
                    <Button onClick={shareORInvite} loading={_loading} className="f-fill">{status}</Button>
                    <Button onClick={onResetButtonCLicked} style={{ marginLeft: '15px' }}>Reset</Button>
                </div>]
            }
        >
            <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                <div className="f-member-listing">
                    {currentTab == "People" && <div className="f-content-searchable"><React.Fragment>
                        <h4><strong>{`${selectedUserIDs.length} Selected`}</strong> {`from ${userData.length} members`}</h4>
                        {/* <Checkbox>Share with all family members</Checkbox> */}
                    </React.Fragment></div>}
                    {currentTab == "Family" && <div className="f-content-searchable"><React.Fragment>
                        <h4><strong>{`${selectedGroupIDs.length} Selected`}</strong> {`from ${FamData.length} families`}</h4>
                        {/* <Checkbox>Share with all my family</Checkbox> */}
                    </React.Fragment></div>}
                    {currentTab == "Others" && <div className="f-content-searchable"><React.Fragment>
                        <h4><strong>{`Fill in the details to share `}</strong> </h4>
                        {/* <Checkbox>Share with all my family</Checkbox> */}
                    </React.Fragment></div>}
                    <Radio.Group
                        className="f-radio-to-tab"
                        size="large"
                        defaultValue="3"
                        buttonStyle="solid"
                        onChange={(e) => handleRadioChange(e)}
                    >
                        <Radio.Button value={'1'}>FAMILY</Radio.Button>
                        <Radio.Button value={'2'}>PEOPLE</Radio.Button>
                        <Radio.Button value={'3'}>OTHERS</Radio.Button>
                    </Radio.Group>
                    {(Tab == 2 || Tab == '2') && <div className="f-member-listing-cards f-share-scroll">
                        {
                            userData.map(item => {
                                return <Card key={item.user_id} className="f-member-cardx">
                                    <div className="f-start">
                                        <div className="f-left">
                                            <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../../assets/images/default_propic.png')} />
                                        </div>
                                        <div className="f-right">
                                            <h4>{item.full_name}</h4>
                                            <h6>{item.location}</h6>
                                            <div className="f-right-combo">
                                                <Checkbox
                                                    defaultChecked={item.invitation_status}
                                                    disabled={item.invitation_status ? true : false}
                                                    onClick={() => {
                                                        let id = item.user_id;
                                                        userHandleSelection(id)
                                                    }} >{item.invitation_status ? "Invited" : (selectedUserIDs.includes(item.user_id) ? "Un-Select" : "Select")}</Checkbox>
                                            </div>
                                        </div>
                                    </div>
                                </Card>
                            }
                            )
                        }
                    </div>}
                    {(Tab == 1 || Tab == '1') && <div className="f-member-listing-cards f-share-scroll">
                        {
                            FamData && FamData.map(item =>
                                <Card key={item.id} className="f-member-cardx">
                                    <div className="f-start">
                                        <div className="f-left">
                                            <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../../../assets/images/default_propic.png')} />
                                            <i>{item.group_type}</i>
                                        </div>
                                        <div className="f-right">
                                            <h4>{item.group_name}</h4>
                                            <h6>{item.created_by}</h6>
                                            <h6>{item.base_region}</h6>
                                            <div className="f-right-combo">
                                                <Checkbox
                                                    defaultChecked={item.invitation_status}
                                                    disabled={item.invitation_status ? true : false}
                                                    onClick={() => {
                                                        let id = item.id;
                                                        GroupHandleSelection(id)
                                                    }}>{item.invitation_status ? "Invited" : (selectedUserIDs.includes(item.user_id) ? "Un-Select" : "Select")}</Checkbox>
                                            </div>
                                        </div>
                                    </div>
                                </Card>
                            )
                        }
                    </div>}
                    {(Tab == 3 || Tab == '3') && <div className="f-member-listing-cards f-share-scroll">
                        {<CollectionCreateForm wrappedComponentRef={(formRef) => saveFormRef(formRef)} />}
                    </div>}
                </div>
            </div>
        </Modal>
    );
}

export default ShareOrInviteModel