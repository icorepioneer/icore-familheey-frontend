import React from 'react'
import {Form,Input,Select} from 'antd'
import countries from '../../../../../common/countrylist'
const { Option } = Select;
const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        render() {
            const {form } = this.props;
            const { getFieldDecorator } = form;
            const prefixSelector = getFieldDecorator("prefix",{
                 rules: [
                {
                    required: true,
                    message: "Please choose the country!"
                }
            ]})(
                <Select showSearch style={{ width: 180 }}>
                    {countries.countries.map((item, i) =>
                        <Option key={i} value={`${item.name+item.code}`}>{item.code}</Option>
                    )}
                </Select>
            );
            return (
                <Form layout="vertical" className="f-form-layout">
                <Form.Item label="Name" hasFeedback>
                    {getFieldDecorator("name", {
                        rules: [
                            {
                                required: true,
                                message: "Please input the name!",
                                whitespace: true,
                            }
                        ]
                    })(<Input />)}
                </Form.Item>
                <div className="f-phone-combined">
                <Form.Item label="Country" className="f-ctry-code" hasFeedback>
                    {prefixSelector}
                </Form.Item>

                <Form.Item label="Phone Number" hasFeedback>
                    {getFieldDecorator("phone", {
                        rules: [
                            { required: true, message: "Please input your phone number!" }
                        ]
                    })(<Input type="number" style={{ width: "100%" }} />)}
                </Form.Item>
                </div>
              
                <Form.Item label="E-mail" hasFeedback>
                    {getFieldDecorator("email", {
                        rules: [
                            {
                                type: "email",
                                message: "The input is not valid E-mail!"
                            },
                            {
                                required: true,
                                message: "Please input your E-mail!"
                            }
                        ]
                    })(<Input />)}
                </Form.Item>
            </Form>
            );
        }
    },
);
export default CollectionCreateForm