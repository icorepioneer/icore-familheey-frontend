import React, { useState, useEffect } from "react";
import {
    Radio,
    Button,
    Icon,
    Input,
    Switch,
    Select,
    // Steps,
    Upload,
    DatePicker,
    Modal,
    TimePicker,
} from "antd";
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import ReactGoogleMapLoader from "react-google-maps-loader";
import moment from "moment";
import { createEvent, checkEventLink } from "../../../createFamily/api-family";
import { notifications } from "../../../../../src/shared/toasterMessage";
import { encryption,decryption } from '../../../../shared/encryptDecrypt'
import { useSelector } from 'react-redux'
import getStateFromLocal from "../../../../shared/getUserDetailsFromLocal";
const MY_API_KEY = decryption(process.env.REACT_APP_PLACE_ENCRYPT);

const { Option } = Select;
const EventCreate = ({
    _modal_visible,
    handleButtonClick,
    group_id,
    callback,
    history
}) => {
    const [state, setState] = useState(
        {
            modal_visible: false,
            nextpage: false,
            event_name: undefined,
            event_type: "Online",
            category: "Activities",
            public_event: false,
            is_sharable: false,
            allow_others: false,
            event_host: "",
            search: "",
            value: "",
            location: "",
            start_date: undefined,
            start_time: undefined,
            end_date: undefined,
            end_time: undefined,
            description: "",
            rsvp: false,
            event_page: undefined,
            imageUrl: "",
            categoryList: [
                "Activities",
                "Art",
                "Business",
                "Causes",
                "Crafts",
                "Culture",
                "Dance",
                "Death",
                "Education",
                "Education Training",
                "Environment",
                "Family",
                "Fashion",
                "Film",
                "Food",
                "Health and wellness",
                "Investment and Finance",
                "Literature",
                "Movies",
                "Music",
                "Networking",
                "Others",
                "Party",
                "Real Estate",
                "Religion",
                "Re-union",
                "Sports",
                "Theatre",
                "Travel and Tourism",
            ],
            _loading: false,
            logoUrl: "",
            user_id: useSelector(stateX => stateX.user_details.userId),
            from_date: undefined,
            to_date: undefined,
            event_image: "",
            start_date_flag: true,
            end_date_flag: true,
            meeting_link: '',
            meeting_dial_number: '',
            meeting_pin: ''
        }
    );
    let {
        value,
        location,
        modal_visible,
        nextpage,
        event_name,
        event_type,
        category,
        public_event,
        is_sharable,
        allow_others,
        event_host,
        search,
        start_date,
        start_time,
        end_date,
        end_time,
        description,
        rsvp,
        event_page,
        imageUrl,
        categoryList,
        _loading,
        // logoUrl,
        user_id,
        from_date,
        to_date,
        event_image,
        start_date_flag,
        end_date_flag,
        meeting_dial_number,
        meeting_link,
        meeting_pin
    } = state;
    useEffect(() => {
        setState((prev) => ({ ...prev, modal_visible: _modal_visible && true }));
    }, [_modal_visible]);
    useEffect(() => {
        let jwt = JSON.parse(localStorage.getItem("jwt"));
        setState((prev) => ({
            ...prev,
            event_host: (jwt && jwt.full_name) || null,
        }));
    }, []);
    const uploadButton = (
        <div>
            <Icon
            // type={ loading ? 'loading' : 'plus'}
            />
            <div className="ant-upload-text">Upload</div>
        </div>
    );
    const handleChange = (tag, event) => {
        event.persist && event.persist();
        setState((prev) => ({ ...prev, [tag]: event.target.value }));
    };
    const handleSelect = (tag, event) => {
        setState((prev) => ({
            ...prev,
            [tag]: tag === "public_event" ? !event : event,
        }));
    };
    const eventContinue = () => {
        setState((prev) => ({ ...prev, nextpage: !nextpage }));
    };
    const handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
        setState((prev) => ({
            ...prev,
            search: "",
            value: geocodedPrediction.formatted_address,
            location: geocodedPrediction.formatted_address,
        }));
    };
    const handleInputChange = (e) => {
        e.persist();
        setState((prev) => ({
            ...prev,
            search: e.target.value,
            value: e.target.value,
        }));
    };
    const disabledStartDate = (start) => {
        return moment(start).isSameOrBefore(moment(new Date()).subtract(1, "day"));
    };
    const disableEndDate = (end) => {
        return moment(end).isSameOrBefore(moment(new Date()).subtract(1, "day"));
    };
    const handleTimeChange = (type, event) => {
        if (type === "start_date" || type === "end_date") {
            setState((prev) => ({
                ...prev,
                [`${type}_flag`]: moment(event).isValid() ? false : true,
                [type]: event == null ? "" : moment(event).format("YYYY/MM/DD"),
            }));
        } else
            setState((prev) => ({ ...prev, [type]: moment(event).format("HH:mm") }));
    };
    const getBase64 = (img, type, cb) => {
        const reader = new FileReader();
        reader.addEventListener("load", () => cb(reader.result));
        reader.readAsDataURL(img);
        if (type === "cover") {
            setState((prev) => ({ ...prev, coverpic: img }));
        } else if (type === "event_image") {
            setState((prev) => ({ ...prev, event_image: img }));
        } else {
            setState((prev) => ({ ...prev, propic: img }));
        }
    };
    const uploadPhoto = (name, info) => {
        if (info.file.status === "uploading") {
            if (name === "cover" || name === "event_image")
                setState((prev) => ({ ...prev, loading: true }));
            else setState((prev) => ({ ...prev, logoLoad: true }));
            return;
        }
        if (info.file.status === "done") {
            if (name === "cover" || name === "event_image") {
                getBase64(info.file.originFileObj, name, (image_Url) =>
                    setState((prev) => ({ ...prev, imageUrl: image_Url, loading: false }))
                );
            } else {
                this.getBase64(info.file.originFileObj, "logo", (logoUrl) =>
                    setState((prev) => ({ ...prev, logoUrl: logoUrl, loading: false }))
                );
            }
        }
    };
    const runValidationAndSave = () => {
        var message = "Error";
        var _description = "";
        if (event_type !== 'Online' && !location) {
            _description = "Please Provide Location";
            notifications("error", message, _description);
            return;
        }
        else if (event_type === 'Online') {
            if (!meeting_link.length > 0) {
                _description = "Please Provide Webinar link";
                notifications("error", message, _description);
                return
            }
            else if (!start_date || !start_time) {
                _description = "Please Select Start Date and Time";
                notifications("error", message, _description);
                return;
            }
            else if (!end_date || !end_time) {
                _description = "Please Select End Date and Time";
                notifications("error", message, _description);
                return;
            }
        }
        else if (!start_date || !start_time) {
            if (event_type != "Sign up") {
                _description = "Please Select Start Date and Time";
                notifications("error", message, _description);
                return;
            } else {
                if (!start_date) {
                    _description = "Please Select Start Date ";
                    notifications("error", message, _description);
                    return;
                }
                setState((prev) => ({ ...prev, start_time: "" }));
            }
        }
        else if (!end_date || !end_time) {
            if (event_type != "Sign up") {
                _description = "Please Select End Date and Time";
                notifications("error", message, _description);
                return;
            } else {
                if (!end_date) {
                    _description = "Please Select End Date ";
                    notifications("error", message, _description);
                    return;
                }
                setState((prev) => ({ ...prev, end_time: "" }));
            }
        }

        if (event_page) {
            let params = {
                text: event_page,
            };
            checkEventLink(params)
                .then((data) => {
                    console.log("check link", data)
                    processDate();
                })
                .catch((error) => {
                    notifications("error", "Error", "link already exist");
                });
        }
        else if (!event_page) {
            _description = "Please provide an event url ";
            notifications("error", message, _description);
            return;
        }
        else {
            processDate();
        }
    };
    const processDate = () => {
        console.log("check link after")
        let fromDate =
            start_date && start_time ? start_date + "," + start_time : start_date;
        let toDate = end_date && end_time ? end_date + "," + end_time : end_date;
        console.log(fromDate, toDate)
        setState((prev) => ({
            ...prev,
            from_date: moment(fromDate).unix(),
            to_date: moment(toDate).unix(),
            _loading: !_loading,
        }));
    };
    useEffect(() => {
        from_date && to_date && _createEvent();
    }, [from_date, to_date]);
    const setToInitialState = () => {
        setState((prev) => ({
            ...prev,
            modal_visible: false,
            nextpage: false,
            event_name: undefined,
            event_type: undefined,
            category: undefined,
            public_event: false,
            is_sharable: false,
            allow_others: false,
            event_host: "",
            search: "",
            value: "",
            location: "",
            start_date: undefined,
            start_time: undefined,
            end_date: undefined,
            end_time: undefined,
            description: "",
            rsvp: false,
            event_page: "",
            imageUrl: "",
            categoryList: [
                "Activities",
                "Art",
                "Business",
                "Causes",
                "Crafts",
                "Culture",
                "Dance",
                "Death",
                "Education",
                "Education Training",
                "Environment",
                "Family",
                "Fashion",
                "Film",
                "Food",
                "Health and wellness",
                "Investment and Finance",
                "Literature",
                "Movies",
                "Music",
                "Networking",
                "Others",
                "Party",
                "Real Estate",
                "Religion",
                "Re-union",
                "Sports",
                "Theatre",
                "Travel and Tourism",
            ],
            _loading: false,
            logoUrl: "",
            user_id: localStorage.getItem("user_id"),
            from_date: undefined,
            to_date: undefined,
            event_image: "",
            meeting_link: '',
            meeting_dial_number: '',
            meeting_pin: ''
        }));
        handleButtonClick();
    };
    const _createEvent = () => {
        const formData = new FormData();
        formData.append("event_name", event_name);
        formData.append("event_type", event_type);
        formData.append("event_host", event_host);
        formData.append("category", category);
        formData.append("created_by", user_id);
        formData.append("location", location);
        formData.append("from_date", from_date);
        formData.append("to_date", to_date);
        formData.append("is_public", public_event);
        formData.append("is_sharable", is_sharable);
        formData.append("allow_others", allow_others);
        formData.append("rsvp", rsvp);
        formData.append("description", description);
        formData.append("event_image", event_image);
        formData.append("group_id", group_id);
        formData.append("meeting_dial_number", meeting_dial_number);
        formData.append("meeting_link", meeting_link);
        formData.append("meeting_pin", meeting_pin);
        if (event_page) formData.append("event_page", event_page);
        createEvent(formData)
            .then((data) => {
                let eventId = data.data.data.id
                notifications("success", "Success", "new event created successfully");
                callback();
                handleButtonClick();
                setToInitialState();
                console.log(eventId)
                let _eventId = encryption(eventId)
                console.log(_eventId)
                console.log(history)
                history.push(`/event-details/${_eventId}`);
                // this.showMyCreatedEvents();
                // let _myEventList = this.state.myEventList
                // _myEventList.unshift(data.data.data)
                // this.setState({ eventModal: false, myEventList: _myEventList, eventTab: 'myevent', _loading: !this.state._loading })
            })
            .catch((error) => {
                console.log(error)
                notifications("error", "Error", "Something went wrong");
            });
    };
    const disableHourStart = () => {
        let hr = moment().format("HH");
        let hourArray = [];
        for (let i = parseInt(hr) - 1; i > 0; i--) {
            // console.log(i)
            hourArray.push(i);
        }
        return moment(start_date).format("DD") == moment().format("DD")
            ? hourArray
            : [];
    };
    const disableHourEnd = () => {
        let hr = moment().format("HH");
        let hourArray = [];
        for (let i = parseInt(hr) - 1; i > 0; i--) {
            // console.log(i)
            hourArray.push(i);
        }
        // console.log(moment(end_date).format("DD"), moment().format("DD"))
        return moment(end_date).format("DD") == moment().format("DD")
            ? hourArray
            : [];
    };
    // const disableHourNew = () => {
    //     let hr = moment().format("HH");
    //     let hourArray = [];
    //     for (let i = parseInt(hr); i > 0; i--) {
    //         hourArray.push(i);
    //     }
    //     return hourArray;
    // };
    const disableMinutesStart = () => {
        let mnt = moment().minute();
        let mntArray = [];
        for (let i = parseInt(mnt) - 1; i >= 0; i--) {
            // console.log(i)
            mntArray.push(i);
        }
        return moment(start_date).format("DD") == moment().format("DD")
            ? mntArray
            : [];
    };
    const disableMinutes = () => {
        let mnt = moment().minute();
        let mntArray = [];
        for (let i = parseInt(mnt) - 1; i >= 0; i--) {
            // console.log(i)
            mntArray.push(i);
        }
        return moment(end_date).format("DD") == moment().format("DD")
            ? mntArray
            : [];
    };
    return (
        <React.Fragment>
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-event-listing">
                        <Modal
                            className="f-modal f-modal-wizard"
                            title="Create New Event"
                            visible={modal_visible}
                            destroyOnClose={true}
                            onCancel={() => setToInitialState()}
                            destroyOnClose
                            maskClosable={false}
                        >
                            {nextpage === false && (
                                <div className="f-modal-wiz-body">
                                    <div className="f-form-control">
                                        <label>Event Name<span style={{ color: 'red' }}>*</span></label>
                                        <Input
                                            placeholder="Enter Event Name"
                                            value={event_name}
                                            onChange={(e) => handleChange("event_name", e)}
                                        ></Input>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Event Type</label>
                                        <Select
                                            defaultValue={event_type ? event_type : ""}
                                            placeholder="Select"
                                            style={{ width: "100%" }}
                                            onChange={(e) => handleSelect("event_type", e)}
                                        >
                                            <Option key="Sign up">Sign up</Option>
                                            <Option key="Online">Online</Option>
                                            <Option key="Regular">Regular</Option>
                                        </Select>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Category</label>
                                        <Select
                                            defaultValue={category ? category : ""}
                                            placeholder="Select"
                                            style={{ width: "100%" }}
                                            onChange={(e) => handleSelect("category", e)}
                                        >
                                            {(categoryList || []).map((item, index) => {
                                                return (
                                                    <Option value={item} key={index}>
                                                        {item}
                                                    </Option>
                                                );
                                            })}
                                        </Select>
                                    </div>
                                    <div className="f-form-control f-wiz-switch">
                                        <ul>
                                            <li>
                                                <h6>Make Your Event Private</h6>
                                                <Switch
                                                    defaultChecked={public_event ? false : true}
                                                    onChange={(e) => handleSelect("public_event", e)}
                                                ></Switch>
                                            </li>
                                            <li>
                                                <h6>Allow guest to invite others</h6>
                                                <Switch
                                                    defaultChecked={is_sharable ? true : false}
                                                    onChange={(e) => handleSelect("is_sharable", e)}
                                                ></Switch>
                                            </li>
                                            <li>
                                                <h6>Allow guest to bring people</h6>
                                                <Switch
                                                    disabled={event_type === "Online"}
                                                    defaultChecked={allow_others ? true : false}
                                                    onChange={(e) => handleSelect("allow_others", e)}
                                                ></Switch>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            )}
                            {nextpage === true && (
                                <div className="f-modal-wiz-body">
                                    <div className="f-form-control">
                                        <label>Hosted by<span style={{ color: 'red' }}>*</span></label>
                                        <Input
                                            defaultValue={event_host}
                                            placeholder="Enter Host Name"
                                            onChange={(e) => handleChange("event_host", e)}
                                        ></Input>
                                    </div>
                                    {event_type === "Online" && (
                                        <React.Fragment>
                                            <div className="f-form-control">
                                                <label>Webinar Link<span style={{ color: 'red' }}>*</span></label>
                                                <Input
                                                    placeholder="Enter Webinar link "
                                                    onChange={(e) => handleChange("meeting_link", e)}
                                                ></Input>
                                            </div>
                                            <div className="f-form-control">
                                                <label>Dial In number</label>
                                                <Input
                                                    placeholder="Enter Dial In number"
                                                    onChange={(e) => handleChange("meeting_dial_number", e)}
                                                ></Input>
                                            </div>

                                            <div className="f-form-control">
                                                <label>Participant PIN</label>
                                                <Input
                                                    placeholder="Enter  Participant PIN"
                                                    onChange={(e) => handleChange("meeting_pin", e)}
                                                ></Input>
                                            </div>
                                        </React.Fragment>
                                    )}
                                    {event_type !== 'Online' && (
                                        <div className="f-form-control">
                                            <label>Venue</label>
                                            {/* <Input placeholder="Enter Venue" id="auto-complete" value={ location} onChange={handleChange('location')}></Input> */}
                                            <ReactGoogleMapLoader
                                                params={{
                                                    key: MY_API_KEY,
                                                    libraries: "places,geocode",
                                                }}
                                                render={(googleMaps) =>
                                                    googleMaps && (
                                                        <ReactGooglePlacesSuggest
                                                            googleMaps={googleMaps}
                                                            autocompletionRequest={{
                                                                input: search,
                                                            }}
                                                            onSelectSuggest={handleSelectSuggest}
                                                            textNoResults="My custom no results text" // null or "" if you want to disable the no results item
                                                            customRender={(prediction) => (
                                                                <div className="customWrapper">
                                                                    {prediction
                                                                        ? prediction.description
                                                                        : "My custom no results text"}
                                                                </div>
                                                            )}
                                                        >
                                                            <Input
                                                                className="art-mat-style art-suffix art-sfx-location"
                                                                placeholder="Location"
                                                                type="text"
                                                                value={value}
                                                                onChange={(e) => handleInputChange(e)}
                                                                defaultValue={location ? location : ""}
                                                            />
                                                        </ReactGooglePlacesSuggest>
                                                    )
                                                }
                                            />
                                        </div>
                                    )}
                                    <div className="f-form-control">
                                        <label>Start Date & Time<span style={{ color: 'red' }}>*</span></label>
                                        <div className="f-split" style={{ marginTop: "0" }}>
                                            <div>
                                                <DatePicker
                                                    disabledDate={disabledStartDate}
                                                    format={"MMM/DD/YYYY"}
                                                    onChange={(e) => handleTimeChange("start_date", e)}
                                                />
                                            </div>
                                            <div>
                                                <TimePicker
                                                    disabled={start_date_flag}
                                                    format={"hh:mm a"}
                                                    use12Hours
                                                    onChange={(e) => handleTimeChange("start_time", e)}
                                                    disabledHours={() => disableHourStart()}
                                                    disabledMinutes={(e) =>disableMinutesStart()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="f-form-control">
                                        <label>End Date & Time<span style={{ color: 'red' }}>*</span></label>
                                        <div className="f-split" style={{ marginTop: "0" }}>
                                            <div>
                                                <DatePicker
                                                    disabledDate={disableEndDate}
                                                    format={"MMM/DD/YYYY"}
                                                    onChange={(e) => handleTimeChange("end_date", e)}
                                                />
                                            </div>
                                            <div>
                                                <TimePicker
                                                    disabled={end_date_flag}
                                                    format={"hh:mm a"}
                                                    use12Hours
                                                    onChange={(e) => handleTimeChange("end_time", e)}
                                                    disabledHours={() => disableHourEnd()}
                                                    disabledMinutes={(e) =>disableMinutes()}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Description</label>
                                        <Input
                                            defaultValue={description}
                                            placeholder="Enter description"
                                            value={description}
                                            onChange={(e) => handleChange("description", e)}
                                        ></Input>
                                    </div>
                                    <div className="f-form-control">
                                        <label>RSVP Required</label>
                                        <Radio.Group
                                            defaultValue={rsvp}
                                            onChange={(e) => handleChange("rsvp", e)}
                                            style={{ display: "flex", float: "left" }}
                                        >
                                            <Radio value={true}>Yes</Radio>
                                            <Radio value={false}>No</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control">
                                        <Upload
                                            type="file"
                                            accept="image/*"
                                            name="avatar"
                                            listType="picture-card"
                                            className="avatar-uploader"
                                            showUploadList={false}
                                            action={`${process.env.REACT_APP_API_URL}/api/v1/familheey/dummy`}
                                            // beforeUpload={beforeUpload}
                                            headers={{
                                                'logined_user_id':`${getStateFromLocal()&&getStateFromLocal()[0]}`,
                                                'Authorization':`Bearer ${getStateFromLocal()&&getStateFromLocal()[1]}`
                                            }}
                                            onChange={(info) => uploadPhoto("event_image", info)}
                                        >
                                            {imageUrl ? (
                                                <img
                                                    src={imageUrl}
                                                    alt="avatar"
                                                    style={{ width: "100%" }}
                                                />
                                            ) : (
                                                    uploadButton
                                                )}
                                        </Upload>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Event Page URL<span style={{ color: 'red' }}>*</span></label>
                                        <div
                                            style={{
                                                display: "flex",
                                                width: "100%",
                                                alignItems: "center",
                                            }}
                                        >
                                            <span
                                                style={{ fontFamily: "monospace", paddingRight: "5px" }}
                                                className="f-eve-pgurl"
                                            >
                                                {`${process.env.REACT_APP_BASE_URL}events/`}
                                            </span>
                                            <Input
                                                defaultValue={event_page}
                                                placeholder="Enter URL"
                                                onChange={(e) => handleChange("event_page", e)}
                                            />
                                        </div>
                                    </div>
                                </div>
                            )}

                            <br></br>
                            <div className="f-modal-wiz-footer">
                                {nextpage === false && (
                                    <Button
                                        onClick={() => eventContinue()}
                                        disabled={
                                            event_name && event_type && category ? false : true
                                        }
                                    >
                                        Continue
                                        <Icon type="arrow-right" />
                                    </Button>
                                )}
                                {nextpage === true && (
                                    <div
                                        style={{
                                            display: "flex",
                                            justifyContent: "space-between",
                                            width: "100%",
                                        }}
                                    >
                                        <Button loading={_loading} onClick={runValidationAndSave}>
                                            Save
                      <Icon type="arrow-right" />
                                        </Button>
                                        <Button
                                            type="link"
                                            onClick={() => {
                                                setState((prev) => ({ ...prev, nextpage: false }));
                                            }}
                                        >
                                            <Icon type="arrow-left" />
                      Back
                    </Button>
                                    </div>
                                )}
                            </div>
                        </Modal>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};
export default EventCreate;