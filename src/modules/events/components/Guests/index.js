import React, { useState, useEffect } from 'react';
import Count from './CountCards/index'
import { Radio, Input, Spin, Card, Tag } from 'antd'
import { getGuestsCount } from '../../eventDetails/api-eventDetails';
import lowerCase from 'lower-case'
const { Search } = Input
const Guests = ({ handleRadioChange, guestTab, memberGoing, tabloader, interested, not_going, inviteList, getguestsCount, user_id, event_id,created_by }) => {
    const [state, setState] = useState({
        countData: [],
        Interested: '',
        InterestedCopy: '',
        NotGoing: '',
        NotGoingCopy: '',
        MemberGoing: '',
        MemberGoingCopy: '',
        InviteList: '',
        InviteListCopy: ''
    })
    let { countData, Interested, NotGoing, MemberGoing, MemberGoingCopy, NotGoingCopy, InterestedCopy,InviteList,InviteListCopy } = state;
    const getAllGuestCount = async () => {
        let params = {
            // current_id:`${user_id}`,
            current_id: `${user_id}`,
            event_id: `${event_id}`
        }
        await getGuestsCount(params).then(res => {
            setState(prev => ({ ...prev, countData: res.data.data }))
        })
    }
    useEffect(() => {
        getAllGuestCount();
    }, [event_id])
    useEffect(() => {
        setState((prev) => ({ ...prev, Interested: interested, InterestedCopy: interested }))
    }, [interested])
    useEffect(() => {
        setState((prev) => ({ ...prev, NotGoing: not_going, NotGoingCopy: not_going }))
    }, [not_going])
    useEffect(() => {
        setState((prev) => ({ ...prev, MemberGoing: memberGoing, MemberGoingCopy: memberGoing }))
    }, [memberGoing])
    useEffect(() => {
        setState((prev) => ({ ...prev, InviteList: inviteList, InviteListCopy: inviteList }))
    }, [inviteList])
    const filterTags = (tags, key) => {
        let Filtered;
        switch (tags) {
            case `MemberGoing`:
                Filtered = MemberGoingCopy.filter(item => lowerCase(item.full_name).indexOf(lowerCase(key.target.value)) !== -1)
                console.log(Filtered)
                setState((prev => ({
                    ...prev,
                    MemberGoing: Filtered
                })))
                break;

            case `Interested`:
                Filtered = InterestedCopy.filter(item => lowerCase(item.full_name).indexOf(lowerCase(key.target.value)) !== -1)
                setState((prev => ({
                    ...prev,
                    Interested: Filtered
                })))
                break;
            case `NotGoing`:
                Filtered = NotGoingCopy.filter(item => lowerCase(item.full_name || item.group_name).indexOf(lowerCase(key.target.value)) !== -1)
                setState((prev => ({
                    ...prev,
                    NotGoing: Filtered
                })))
                break;
            // case `NotGoing`:
            //     Filtered = NotGoingCopy.filter(item => lowerCase(item.full_name || item.group_name).indexOf(lowerCase(key.target.value)) !== -1)
            //     console.log(Filtered)
            //     // setState((state => ({
            //     //     ...state,
            //     //     NotGoing: Filtered
            //     // })))
            //     setState((prev) => ({ ...prev, NotGoing: Filtered }))
            //     break;
            case `InviteList`:
                Filtered = InviteListCopy.filter(item => lowerCase(item.full_name || item.group_name).indexOf(lowerCase(key.target.value)) !== -1)
                setState((prev => ({
                    ...prev,
                    InviteList: Filtered
                })))
                break;

        }

    }
    return (
        <React.Fragment>
            <Count countData={countData} />
            {created_by&&<div className="f-clearfix">
                <div className="f-posts-filters">
                    <div className="f-tab-ui">
                        <Radio.Group defaultValue="1" onChange={(e) => handleRadioChange(e)}>
                            <Radio value={'1'}>ATTENDING</Radio>
                            <Radio value={'2'}>MAY ATTEND</Radio>
                            <Radio value={'3'}>NOT ATTENDING</Radio>
                            <Radio value={'4'}>INVITATION SENT</Radio>
                        </Radio.Group>
                    </div>
                </div>
                <div className="f-member-list">
                    {guestTab === '1' &&
                        <div className="f-member-listing">
                            <div className="f-content-searchable">
                                <h4><strong>{memberGoing.length}</strong> Members</h4>
                                <Search onChange={e => filterTags('MemberGoing', e)} placeholder="Discover" />
                            </div>
                            {tabloader && <Spin style={{ 'paddingLeft': '50%', 'paddingTop': '20%' }}></Spin>}
                            <div className="f-member-listing-cards">
                                {MemberGoing && MemberGoing.length > 0 && !tabloader &&
                                    MemberGoing.map((item, index) => {
                                        return <Card className="f-member-cardx" key={index}>
                                            <div className="f-start">
                                                <div className="f-left">
                                                    <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../../assets/images/demo/m2.png')} />
                                                </div>
                                                <div className="f-right">
                                                   {item.others_count>0 ?<h4>{`${item.full_name} and ${item.others_count} others`}</h4>:<h4>{`${item.full_name}`}</h4>}
                                                    <h6 className="f-pin">{item.location}</h6>
                                                    <div className="f-stop">
                                                        <h5><strong>{item.total_family}</strong> Families</h5>
                                                        <h5><strong>{item.mutual_contact}</strong> Mutual </h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </Card>
                                    })
                                }
                                {MemberGoing && MemberGoing.length === 0 && !tabloader &&
                                    <div>NO DATA</div>
                                }
                            </div>
                        </div>
                    }
                    {guestTab === '2' &&
                        <div className="f-member-listing">
                            <div className="f-content-searchable">
                                <h4><strong>{interested.length}</strong> Members</h4>
                                <Search onChange={e => filterTags('Interested', e)} placeholder="Discover" />
                            </div>
                            {tabloader && <Spin style={{ 'paddingLeft': '50%', 'paddingTop': '20%' }}></Spin>}
                            <div className="f-member-listing-cards">
                                {Interested && Interested.length > 0 && !tabloader &&
                                    Interested.map((item, index) => {
                                        return <Card className="f-member-cardx" key={index}>
                                            <div className="f-start">
                                                <div className="f-left">
                                                    <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../../assets/images/demo/m2.png')} />
                                                </div>
                                                <div className="f-right">
                                                   {item.others_count>0 ?<h4>{`${item.full_name} and ${item.others_count} others`}</h4>:<h4>{`${item.full_name}`}</h4>}
                                                    <h6 className="f-pin">{item.location}</h6>
                                                    <div className="f-stop">
                                                        <h5><strong>{item.total_family}</strong> Families</h5>
                                                        <h5><strong>{item.mutual_contact}</strong> Mutual</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </Card>
                                    })
                                }
                                {interested && interested.length === 0 && !tabloader &&
                                    <div>NO DATA</div>
                                }

                            </div>
                        </div>
                    }
                    {guestTab === '3' &&
                        <div className="f-member-listing">
                            <div className="f-content-searchable">
                                <h4><strong>{not_going.length}</strong> Members</h4>
                                <Search onChange={e => filterTags('NotGoing', e)} placeholder="Discover" />
                            </div>
                            {tabloader && <Spin style={{ 'paddingLeft': '50%', 'paddingTop': '20%' }}></Spin>}
                            <div className="f-member-listing-cards">
                                {NotGoing && NotGoing.length > 0 && !tabloader &&
                                    NotGoing.map((item, index) => {
                                        return <Card className="f-member-cardx" key={index}>
                                            <div className="f-start">
                                                <div className="f-left">
                                                    <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../../assets/images/demo/m2.png')} />
                                                </div>
                                                <div className="f-right">
                                                   {item.others_count>0 ?<h4>{`${item.full_name} and ${item.others_count} others`}</h4>:<h4>{`${item.full_name}`}</h4>}
                                                    <h6 className="f-pin">{item.location}</h6>
                                                    <div className="f-stop">
                                                        <h5><strong>{item.total_family}</strong> Families</h5>
                                                        <h5><strong>{item.mutual_contact}</strong> Mutual</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </Card>
                                    })
                                }
                                {NotGoing && NotGoing.length === 0 && !tabloader &&
                                    <div>NO DATA</div>
                                }
                            </div>
                        </div>
                    }
                    {guestTab === '4' &&
                        <div className="f-member-listing">
                            <div className="f-content-searchable">
                                <h4><strong>{inviteList.length}</strong> Members</h4>
                                <Search onChange={e => filterTags('InviteList', e)} placeholder="Search friends in the family" />
                            </div>
                            {tabloader && <Spin style={{ 'paddingLeft': '50%', 'paddingTop': '20%' }}></Spin>}
                            <div className="f-member-listing-cards">
                                {InviteList && InviteList.length > 0 && !tabloader &&
                                    InviteList.map((item, index) =>
                                        item.full_name ? (<Card className="f-member-cardx" key={index}>
                                            <div className="f-start">
                                                <div className="f-left">
                                                    <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../../assets/images/default_propic.png')} />
                                                </div>
                                                <div className="f-right">
                                                    <h4>{item.full_name}</h4>
                                                    <h6 className="f-pin">{item.event_name}</h6>
                                                    <div className="f-stop">
                                                        <h5><strong></strong></h5>
                                                        {/* <h5><strong>12</strong> New Posts</h5> */}
                                                        <Tag color="purple">User</Tag>
                                                    </div>
                                                </div>
                                            </div>
                                        </Card>) : (
                                                <Card className="f-member-cardx" key={index}>
                                                    <div className="f-start">
                                                        <div className="f-left">
                                                            <img src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../../../assets/images/default_logo.png')} />
                                                        </div>
                                                        <div className="f-right">
                                                            <h4>{item.group_name}</h4>
                                                            <h6 className="f-pin">{item.event_name}</h6>
                                                            <div className="f-stop">
                                                                <h6 className="f-pin">{item.location}</h6>
                                                                {/* <h5><strong>12</strong> New Posts</h5> */}
                                                                <Tag color="purple">Family</Tag>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Card>
                                            )
                                    )
                                }
                                {inviteList && inviteList.length === 0 && !tabloader &&
                                    <div>NO DATA</div>
                                }
                            </div>
                        </div>
                    }
                </div>
            </div>}
        </React.Fragment>
    );
}
export default Guests;