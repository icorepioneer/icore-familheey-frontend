import React from 'react'
const Count = ({ countData }) => {
    return (
        <div className="f-member-at-glance">
            <h4>At a glance</h4>
            <div className="f-member-ow-cards">
                <div className="f-overview-card">
                    <h3>{countData.allCount ? countData.allCount : 0}</h3>
                    <p>Attending</p>
                </div>
                <div className="f-overview-card">
                    <h3>{countData.interested ? countData.interested : 0}</h3>
                    <p>May attend</p>
                </div>
                <div className="f-overview-card">
                    <h3>{countData.notGoing ? countData.notGoing : 0}</h3>
                    <p>Not attending</p>
                </div>
                <div className="f-overview-card">
                    <h3>{countData.invitationsSend ? countData.invitationsSend : 0}</h3>
                    <p>Invitation sent</p>
                </div>
            </div>
        </div>
    );
}
export default Count;