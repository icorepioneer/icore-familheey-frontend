import axios from '../../../services/apiCall';

const exploreEvents = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/explore_events`, data)
}

const eventInvitations = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/event_invitations`, data)
}

const createdByMe = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/created_by_me`, data)
}

const groupEvents = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/group_events`, data)
}

const getEventShareList = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/event_share_list`, data)
}
export {
    exploreEvents,
    eventInvitations,
    createdByMe,
    groupEvents,
    getEventShareList
}