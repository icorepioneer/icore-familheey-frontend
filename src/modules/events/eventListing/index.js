import React from "react";
// import { withRouter } from "react-router-dom";
import "./style.scss";
import { Radio, Button, Icon, DatePicker, Modal, TimePicker } from "antd";
import EventSidebar from "../../../components/EventSidebar";
import PostsSidebar from "../../../components/PostsSidebar";
import {
  requestLinkFamily,
  createEvent,
  checkEventLink,
} from "../../../modules/createFamily/api-family";
import EventCard from "../../../components/EventCard";
import { exploreEvents, eventInvitations, createdByMe } from "./api-eventList";
import { notifications } from "../../../shared/toasterMessage";
import * as moment from "moment";
import EventCreate from "../components/CreateEvent/index";
import {connect} from 'react-redux'


class eventListing extends React.Component {
  state;
  constructor(props) {
    super(props);
    let {userID} = props
    this.state = {
      // user_id: localStorage.getItem('user_id'),
      user_id: userID,
      invitations: [],
      myEventList: [],
      imageUrl: "",
      exploreEventArray: [],
      eventTab: "explore",
      from_date: "",
      to_date: "",
      query: "",
      filter: "",
      dateModal: false,
      categoryList: [
        "Activities",
        "Art",
        "Business",
        "Causes",
        "Crafts",
        "Culture",
        "Dance",
        "Death",
        "Education",
        "Education Training",
        "Environment",
        "Family",
        "Fashion",
        "Film",
        "Food",
        "Health and wellness",
        "Investment and Finance",
        "Literature",
        "Movies",
        "Music",
        "Networking",
        "Others",
        "Party",
        "Real Estate",
        "Religion",
        "Re-union",
        "Sports",
        "Theatre",
        "Travel and Tourism",
      ],
      start_date: moment(new Date()).format("MMM/DD/YYYY"),
      start_time: moment(new Date()).format("hh:mm a"),
      end_date: moment(new Date()).add(1, "day").format("MMM/DD/YYYY"),
      end_time: moment(new Date()).format("hh:mm a"),
      loader: true,
      eventListObj: {},
      exploreEventObj: {},
      upComingEventList: [],
      eventModal: false,
      nextpage: false,
      full_name: "",
      public_event: false,
      is_sharable: false,
      allow_others: false,
      rsvp: false,
      activeTab: "explore",
      search: "",
      pastEventObj: {},
      _loading: false,
      defaultOpenKey: null,
      propsTabName:
        this.props.location.state && this.props.location.state.key
          ? this.props.location.state.key
          : "explore",
      start_date_flag: true,
      end_date_flag: true,
    };
  }
  componentDidMount() {
    this.showInvitationList();
    this.showMyCreatedEvents();
    // console.log(this.props.location.state.key);
    if (this.props.location.state && this.props.location.state.key) {
      this.setState({ eventTab: this.props.location.state.key });
    }
  }
  
  handleRefresh=()=>{
    this.showInvitationList();
    this.showMyCreatedEvents();
  }

  componentWillMount() {
    this.showEvents();
    let jwt = JSON.parse(localStorage.getItem("jwt"));
    this.setState({
      event_host: (jwt && jwt.full_name) || null,
    });
    if (window.location.pathname == "/events") {
      this.setState({
        defaultOpenKey: "sub1",
      });
    }
  }
  handleCancel = () => {
    this.setState({
      visible: false,
      eventModal: false,
      current: 0,
      dateModal: false,
    });
  };
  handleChange = (name) => (e) => {
    this.setState({ [name]: e.target.value });
  };
  handleSelect = (name) => (event) => {
    this.setState({ [name]: name == "public_event" ? !event : event });
  };
  searchEvent = (param) => {
    this.setState({ query: param.target.value });
    if (this.state.eventTab === "myevent") {
      this.showMyCreatedEvents();
    } else if (this.state.eventTab === "invitation") {
      this.showInvitationList();
    } else {
      this.showEvents();
    }
  };
  showEvents = () => {
    var params = {
      user_id: this.state.user_id,
      query: this.state.query,
      from_date: this.state.from_date,
      to_date: this.state.to_date,
      filter: this.state.filter,
    };
    exploreEvents(params)
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.data &&
          response.data.data.explore
        ) {
          response.data.data.explore.sharedEvents.map((item, index) => {
            item.shared_events = true;
          });
          this.setState({ exploreEventObj: response.data.data.explore });
          var explore = response.data.data.explore.sharedEvents.concat(
            response.data.data.explore.basedOnLocation
          );
          var _exploreEventArray = explore.concat(
            response.data.data.explore.publicEvents
          );
          if (_exploreEventArray.length === 0) {
            // _exploreEventArray.push({ no_data: true })
          }
          this.setState({
            exploreEventArray: _exploreEventArray,
            loader: false,
          });
        }
      })
      .catch((error) => {
        console.log("error--->", error);
      });
  };

  showInvitationList = () => {
    var params = {
      user_id: this.state.user_id,
      query: this.state.query,
      from_date: this.state.from_date,
      to_date: this.state.to_date,
      filter: this.state.filter,
    };
    eventInvitations(params)
      .then((response) => {
        if (response && response.data && response.data.data) {
          var invitations = response.data.data;
          if (invitations.length === 0) {
            // invitations.push({ no_data: true })
          }
          this.setState({ invitations: invitations });
        }
      })
      .catch((error) => {
        console.log("error--->", error);
      });
  };
  showMyCreatedEvents = () => {
    var params = {
      user_id: this.state.user_id,
      query: this.state.query,
      from_date: this.state.from_date,
      to_date: this.state.to_date,
      filter: this.state.filter,
    };
    createdByMe(params)
      .then((response) => {
        if (
          response &&
          response.data &&
          response.data.data &&
          response.data.data.my_event_list
        ) {
          this.setState({
            eventListObj: response.data.data.my_event_list,
            pastEventObj: response.data.data.past_event_list
              ? response.data.data.past_event_list
              : { data: [] },
          });

          var myevents = response.data.data.my_event_list.created_by_me.concat(
            response.data.data.my_event_list.rsvp_yes
          );
          var myEventList = myevents.concat(
            response.data.data.my_event_list.rsvp_maybe
          );
          if (myEventList.length === 0) {
            // myEventList.push({ no_data: true })
          }
          this.setState({ myEventList: myEventList });
        }
        this.upComingEvents();
      })
      .catch((error) => {
        console.log("error--->", error);
      });
  };
  onChange = (e) => {
    this.setState({ eventTab: e.target.value });
  };
  eventModalFn = (val) => {
    this.setState({ eventModal: val });
  };
  eventTabSwitch = (val) => {
    this.setState({ eventTab: val });
  };

  // Toggle Sidebar
  toggleEventsidebar = () => {
    document.getElementById("f-wrap").classList.toggle("f-show-post-sider");
  };
  // Close Sidebar
  closeEventsidebar = () => {
    document.getElementById("f-wrap").classList.remove("f-show-post-sider");
  };
  filterByDate = () => (event) => {
    var filter = "";
    if (event.target && event.target.value === 2) {
      filter = "today";
    } else if (event.target && event.target.value === 3) {
      filter = "tomorrow";
    } else if (event.target && event.target.value === 4) {
      filter = "week";
    } else if (event.target && event.target.value === 5) {
      filter = "month";
    }
    this.setState({ filter: filter, from_date: "", to_date: "" }, () => {
      this.showEvents();
      this.showInvitationList();
      this.showMyCreatedEvents();
    });
  };
  openCustomeDateSelector = () => (e) => {
    this.setState({ dateModal: true });
  };
  handleOk = (e) => {
    if (
      this.state.start_date &&
      this.state.end_date &&
      this.state.start_time &&
      this.state.end_time
    ) {
      this.setState({ dateModal: false });
      var from_date = this.state.start_date + "," + this.state.start_time;
      var to_date = this.state.end_date + "," + this.state.end_time;
      this.setState(
        {
          from_date: Date.parse(from_date) / 1000,
          to_date: Date.parse(to_date) / 1000,
        },
        () => {
          this.customeFilter();
        }
      );
    } else {
      notifications("error", "Please fill all the fields");
    }
  };

  // handleCancel = e => {
  //     this.setState({ dateModal: false });
  // };
  handleChangeDate = (type) => (event) => {
    if (event == null) {
      this.setState({ [type]: "" });
    } else {
      if (type === "start_date" || type === "end_date") {
        this.setState({ [type]: moment(event).format("YYYY-MM-DD") });
      } else this.setState({ [type]: moment(event).format("HH:mm") });
    }
  };
  customeFilter = () => {
    this.setState({ filter: "" }, () => {
      this.showEvents();
      this.showInvitationList();
      this.showMyCreatedEvents();
    });
  };
  upComingEvents = () => {
    var myevents = this.state.eventListObj.rsvp_yes.concat(
      this.state.eventListObj.rsvp_maybe
    );
    myevents = myevents.concat(this.state.exploreEventObj.basedOnLocation);
    myevents = myevents.concat(this.state.exploreEventObj.publicEvents);
    var date = Date.parse(new Date()) / 1000;
    var upComingEventList = myevents.filter((id) => id.from_date > date);
    upComingEventList.sort((a, b) => a.from_date - b.from_date);
    this.setState({ upComingEventList: upComingEventList });
  };
  onClose = () => {
    this.setState({ drawervisible: false });
  };
  checkedValue = (value) => (e) => {
    if (this.state.checkedGroups.indexOf(value) === -1) {
      this.state.checkedGroups.push(value);
      this.setState({ checkedGroups: this.state.checkedGroups });
    } else {
      var checkedGroups = [];
      checkedGroups = this.state.checkedGroups.filter(function (item) {
        return item !== value;
      });
      this.setState({ checkedGroups: checkedGroups });
    }
    console.log("this.state.checkedGroups", this.state.checkedGroups);
  };
  linkToSelectedGroups = () => {
    var params = {
      to_group: this.state.checkedGroups,
      from_group: this.state.group_id,
      requested_by: this.state.user_id,
    };
    requestLinkFamily(params)
      .then((response) => {
        console.log("response", response);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  showEventModal = () => {
    this.setState({ eventModal: true });
  };
  createEvent = () => {
    var from_date =
      this.state.start_date && this.state.start_time
        ? this.state.start_date + "," + this.state.start_time
        : this.state.start_date;
    var to_date =
      this.state.end_date && this.state.end_time
        ? this.state.end_date + "," + this.state.end_time
        : this.state.end_date;
    const formData = new FormData();
    this.setState(
      {
        from_date: from_date ? Date.parse(from_date) / 1000 : "",
        to_date: to_date ? Date.parse(to_date) / 1000 : "",
        _loading: !this.state._loading,
      },
      () => {
        formData.append("event_name", this.state.event_name);
        formData.append("event_type", this.state.event_type);
        formData.append("event_host", this.state.event_host);
        formData.append("category", this.state.category);
        formData.append("created_by", this.state.user_id);
        formData.append("location", this.state.location);
        formData.append("from_date", this.state.from_date);
        formData.append("to_date", this.state.to_date);
        formData.append("is_public", this.state.public_event);
        formData.append("is_sharable", this.state.is_sharable);
        formData.append("allow_others", this.state.allow_others);
        formData.append("rsvp", this.state.rsvp);
        formData.append("description", this.state.description);
        formData.append("event_image", this.state.event_image);
        if (this.state.event_page)
          formData.append("event_page", this.state.event_page);
      }
    );
    createEvent(formData)
      .then((data) => {
        notifications("success", "Success", "new event created successfully");
        this.showMyCreatedEvents();
        this.setState({
          event_name: "",
          event_type: "",
          event_host: "",
          category: "",
          location: "",
          search: "",
          value: "",
          from_date: "",
          to_date: "",
          public_event: false,
          is_sharable: false,
          allow_others: false,
          rsvp: false,
          description: "",
          event_page: "",
          nextpage: false,
        });
        let _myEventList = this.state.myEventList;
        _myEventList.unshift(data.data.data);
        this.setState({
          eventModal: false,
          myEventList: _myEventList,
          eventTab: "myevent",
          _loading: !this.state._loading,
        });
      })
      .catch((error) => {
        notifications("error", "Error", "Something went wrong");
      });
  };
  checkEventLink = () => {
    var message = "Error";
    var description = "";
    if (!this.state.location) {
      description = "Please Provide Location";
      notifications("error", message, description);
      return;
    }
    if (!this.state.start_date || !this.state.start_time) {
      if (this.state.event_type != "Sign up") {
        description = "Please Select Start Date and Time";
        notifications("error", message, description);
        return;
      } else {
        if (!this.state.start_date) {
          description = "Please Select Start Date ";
          notifications("error", message, description);
          return;
        }
        this.setState({
          start_time: "",
        });
      }
    }
    if (!this.state.end_date || !this.state.end_time) {
      if (this.state.event_type != "Sign up") {
        description = "Please Select End Date and Time";
        notifications("error", message, description);
        return;
      } else {
        if (!this.state.end_date) {
          description = "Please Select End Date ";
          notifications("error", message, description);
          return;
        }
        this.setState({
          end_time: "",
        });
      }
    }
    var params = {
      text: this.state.event_page,
    };
    if (this.state.event_page) {
      checkEventLink(params)
        .then((data) => {
          this.createEvent();
        })
        .catch((error) => {
          notifications("error", "Error", "link already exist");
        });
    } else {
      this.createEvent();
    }
  };

  handleTimeChange = (type) => (event) => {
    if (type === "start_date" || type === "end_date") {
      this.setState({
        [type]: event == null ? "" : moment(event).format("YYYY-MM-DD"),
        [`${type}_flag`]: moment(event).isValid() ? false : true,
      });
    } else {
      this.setState({
        [type]: event == null ? "" : moment(event).format("HH:mm"),
      });
    }
  };
  eventContinue = () => {
    var message = "Error";
    var description = "";
    if (!this.state.event_name || !this.state.event_name.trim()) {
      description = "Please provide event name";
      notifications("error", message, description);
    } else if (!this.state.event_type) {
      description = "Please select a event type";
      notifications("error", message, description);
    } else if (!this.state.category) {
      description = "Please select a event category";
      notifications("error", message, description);
    } else {
      this.setState({ nextpage: true });
    }
  };
  handleInputChange = (e) => {
    console.log("e.target.value", e.target.value);
    this.setState({ search: e.target.value, value: e.target.value });
  };
  handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
    console.log(geocodedPrediction, originalPrediction); // eslint-disable-line
    this.setState({ search: "", value: geocodedPrediction.formatted_address });
    this.setState({ location: geocodedPrediction.formatted_address });
  };
  uploadPhoto = (name) => (info) => {
    if (info.file.status === "uploading") {
      if (name === "cover" || name === "event_image")
        this.setState({ loading: true });
      else this.setState({ logoLoad: true });
      return;
    }
    if (info.file.status === "done") {
      if (name === "cover" || name === "event_image") {
        this.getBase64(info.file.originFileObj, name, (imageUrl) =>
          this.setState({
            imageUrl,
            loading: false,
          })
        );
      } else {
        this.getBase64(info.file.originFileObj, "logo", (logoUrl) =>
          this.setState({
            logoUrl,
            logoLoad: false,
          })
        );
      }
    }
  };

  getBase64(img, type, callback) {
    console.log("imggg", img);
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
    if (type === "cover") {
      this.setState({ coverpic: img });
    } else if (type === "event_image") {
      this.setState({ event_image: img });
    } else {
      this.setState({ propic: img });
    }
  }
  disabledStartDate = (start) => {
    return moment(start).isSameOrBefore(moment(new Date()).subtract(1, "day"));
  };
  disableEndDate = (end) => {
    return moment(end).isSameOrBefore(moment(this.state.start_date));
  };
  disableHour = () => {
    let { start_date } = this.state;
    let hr = moment().format("HH");
    let hourArray = [];
    for (let i = parseInt(hr) - 1; i > 0; i--) {
      // console.log(i)
      hourArray.push(i);
    }
    return moment(start_date).format("DD") == moment().format("DD")
      ? hourArray
      : [];
  };
  disableHourNew = () => {
    let hr = moment().format("HH");
    let hourArray = [];
    for (let i = parseInt(hr); i > 0; i--) {
      // console.log(i)
      hourArray.push(i);
    }
    return hourArray;
  };
  disableMinutes = (start) => {
    let { start_date } = this.state;
    let mnt = moment().minute();
    let mntArray = [];
    for (let i = parseInt(mnt) - 1; i >= 0; i--) {
      // console.log(i)
      mntArray.push(i);
    }
    return moment(start_date).format("DD") == moment().format("DD")
      ? mntArray
      : [];
  };
  render() {
    const {
      //search,
      //value,
      //imageUrl,
      defaultOpenKey,
      start_time,
      start_date,
      end_date,
      end_time,
      //location,
      //description,
      //rsvp,
      //event_page,
    } = this.state;
    // const uploadButton = (
    //   <div>
    //     <Icon type={this.state.loading ? "loading" : "plus"} />
    //     <div className="ant-upload-text">Upload</div>
    //   </div>
    // );
    //let { start_date_flag, end_date_flag } = this.state;
    return (
      <section className="eventListing f-page f-alt">
        <div className="f-clearfix f-body">
          <div className="f-boxed">
            <div className="f-event-listing">
            <EventCreate
              history = {this.props.history}
              callback={this.handleRefresh}
              group_id={''}
              _modal_visible={this.state.eventModal}
              handleButtonClick={this.handleCancel}
            />
              {/* {this.state.familyList && this.state.familyList.length > 0 && */}
              {this.state.eventTab === "explore" && (
                <PostsSidebar
                  defaultKey="4"
                  defaultOpen={defaultOpenKey}
                  history={this.props.history}
                  eventTabSwitch={this.eventTabSwitch}
                />
              )}
              {this.state.eventTab === "invitation" && (
                <PostsSidebar
                  defaultKey="7"
                  defaultOpen={defaultOpenKey}
                  history={this.props.history}
                  eventTabSwitch={this.eventTabSwitch}
                />
              )}
              {this.state.eventTab === "myevent" && (
                <PostsSidebar
                  defaultKey="8"
                  defaultOpen={defaultOpenKey}
                  history={this.props.history}
                  eventTabSwitch={this.eventTabSwitch}
                />
              )}

              <EventSidebar
                eventfn={this.eventModalFn}
                familyList={this.state.familyList}
                upComingEventList={this.state.upComingEventList}
                loader={this.state.loader}
                history={this.props.history}
              />
              {/* Close Sidebar Mobile */}
              <span
                className="f-close-post-sidebar"
                onClick={() => this.closeEventsidebar()}
                style={{ display: "none" }}
              />
              {/* } */}
              <div className="f-events-main">
                {/* Tab / Filters */}
                <div className="f-events-filters">
                  <Button
                    onClick={() => this.toggleEventsidebar()}
                    className="f-eventside-unfold f-force-hide-tab"
                    style={{ display: "none" }}
                  >
                    <Icon type="menu-unfold" />
                  </Button>
                  <div className="f-posts-create f-force-hide-tab">
                    <Button onClick={() => this.showEventModal()}>
                      Create Event{" "}
                      <img
                        src={require("../../../assets/images/icon/plus.svg")}
                      />
                    </Button>
                  </div>
                  <div
                    className="f-tab-ui f-posts-filters-device"
                    style={{ display: "none" }}
                  >
                    <Radio.Group
                      defaultValue={this.state.activeTab}
                      onChange={this.onChange}
                      value={this.state.eventTab}
                    >
                      <Radio value={"explore"}>EXPLORE</Radio>
                      <Radio value={"invitation"}>INVITATIONS</Radio>
                      <Radio value={"myevent"}>MY EVENTS</Radio>
                    </Radio.Group>
                  </div>
                  <div className="f-filter-ui">
                    <Radio.Group
                      defaultValue={1}
                      onChange={this.filterByDate()}
                    >
                      <Radio value={1}>All</Radio>
                      <Radio value={2}>Today</Radio>
                      <Radio value={3}>Tomorrow</Radio>
                      <Radio value={4}>This Week</Radio>
                      <Radio value={5}>This Month</Radio>
                    </Radio.Group>
                    {/* More Button */}
                    <Button onClick={this.openCustomeDateSelector()} />
                  </div>
                </div>

                {/* Event Cards */}
                <div className="f-event-feed f-event-mob-fix">
                  {this.state.eventTab === "explore" && (
                    <EventCard
                      exploreEventArray={this.state.exploreEventArray}
                      history={this.props.history}
                      eventimageUri={require("../../../assets/images/default_event_image.png")}
                      type={"explore"}
                      searchEvent={this.searchEvent}
                    />
                  )}
                  {this.state.eventTab === "invitation" && (
                    <EventCard
                      exploreEventArray={this.state.invitations}
                      history={this.props.history}
                      eventimageUri={require("../../../assets/images/default_event_image.png")}
                      type={"invitation"}
                      searchEvent={this.searchEvent}
                    />
                  )}
                  {this.state.eventTab === "myevent" && (
                    <EventCard
                      exploreEventArray={this.state.eventListObj}
                      history={this.props.history}
                      eventimageUri={require("../../../assets/images/default_event_image.png")}
                      type={"myevent"}
                      pasteventObj={this.state.pastEventObj}
                      searchEvent={this.searchEvent}
                    />
                  )}
                  {/* Lazy Loader for Cards*/}
                  {/* <LazyLoader /> */}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          title="Select Date"
          visible={this.state.dateModal}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          className="f-modal f-modal-wizard f-modal-wfooter"
        >
          <div className="f-modal-wiz-body">
            <div className="f-form-control">
              <label>Start Date</label>
              <div className="f-split">
                <div>
                  <DatePicker
                    defaultValue={
                      start_date
                        ? moment(moment(start_date), "MMM/DD/YYYY")
                        : moment(new Date(), "MMM/DD/YYYY")
                    }
                    format={"MMM DD YYYY"}
                    onChange={this.handleChangeDate("start_date")}
                  />
                </div>
                <div>
                  <TimePicker
                    use12Hours
                    defaultValue={
                      start_time
                        ? moment(start_time, "hh:mm")
                        : moment(new Date(), "hh:mm")
                    }
                    format={"hh:mm a"}
                    onChange={this.handleChangeDate("start_time")}
                  />
                </div>
              </div>
            </div>
            <div className="f-form-control">
              <label>End Date</label>
              <div className="f-split">
                <div>
                  <DatePicker
                    defaultValue={
                      start_date
                        ? moment(moment(end_date), "MMM/DD/YYYY")
                        : moment(new Date(), "MMM/DD/YYYY")
                    }
                    format={"MMM DD YYYY"}
                    onChange={this.handleChangeDate("end_date")}
                  />
                </div>
                <div>
                  <TimePicker
                    use12Hours
                    defaultValue={
                      start_time
                        ? moment(end_time, "hh:mm")
                        : moment(new Date(), "hh:mm")
                    }
                    format={"hh:mm a"}
                    onChange={this.handleChangeDate("end_time")}
                  />
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </section>
    );
  }
}

const mapStateToProps=(state)=>{
  let {userId} = state.user_details;
  return {
      userID:userId
  }
}

export default connect(mapStateToProps,null)(eventListing);
