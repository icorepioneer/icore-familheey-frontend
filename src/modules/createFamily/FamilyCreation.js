import React from 'react';
import { Select, Modal, Input, Upload, Icon, Radio, Drawer, Checkbox } from "antd";
import { createFamily, checkforDuplicateFamily, joinToExistingFamily, getAllLinkedFamily, updateFamily, requestLinkFamily } from './api-family.js'
const { Option } = Select;
const { confirm } = Modal;
const { TextArea } = Input;


class FamilyCreation extends React.Component {

    state = {
        familyName: '',
        familyType: [
            { name: 'public' },
            { name: 'private' },
            { name: 'regular' }
        ],
        selectType: '',
        region: '',
        nextSetting: true,
        intro: '',
        loading: false,
        logoLoad: false,
        basicSetting: false,
        postAvailable: '',
        user_id: localStorage.getItem('user_id'),
        // user_id: "939",
        searchable: 1,
        group_id: '',
        visible: false,
        familyList: '',
        logo: '',
        cover_pic: '',
        checkedGroups: [],
        linkable: 1
    }

    handleChange = name => event => {
        if (name === 'selectType' || name === 'postAvailable') {
            this.setState({ [name]: event })
        } else {
            this.setState({ [name]: event.target.value })
        }
    }

    cancelFamilyCreation = () => {
        this.setState({ familyName: '', selectType: '', region: '' })
    }

    createFamily = () => {
        if (!this.state.familyName || !this.state.familyName.trim()||!this.state.selectType||!this.state.region) {
            return
        }
        var familyDetails = {
            group_name: this.state.familyName,
            group_category: this.state.selectType,
            base_region: this.state.region,
            user_id: this.state.user_id
        }
        this.checkDuplicateFamily(familyDetails)
    }

    checkDuplicateFamily = (familyDetails) => {
        checkforDuplicateFamily(familyDetails).then((result) => {
            console.log('duplcate', result);
            var $this = this
            if (result && result.data.length > 0) {
                var message = 'This group exist, do you want to merge with it ?';
                familyDetails.group_id = result.data[0].id
                $this.setState({ group_id: result.data[0].id })
                localStorage.setItem('group_id', result.data[0].id);
                $this.confirmCreation(familyDetails, message)
            } else {
                $this.createNewFamily(familyDetails)
            }
        }).catch((err) => {
            console.log('errorr', err)
        })
    }

    confirmCreation = (params, message) => {
        var $this = this
        confirm({
            title: message,
            okText: 'Yes',
            cancelText: 'No',
            onOk() {
                $this.joinExistingFamily(params);
            },
            onCancel() {
                $this.createNewFamily(params);
            },
        });
    }

    createNewFamily = (data) => {
        data.created_by = data.user_id;
        createFamily(data).then((res) => {
            this.setState({ nextSetting: true, group_id: res.data[0].id });
            localStorage.setItem('group_id', res.data[0].id);
        }).catch((error) => {
            console.log('error while creating', error);
        })
    }

    joinExistingFamily = (data) => {
        var params = {
            user_id: data.user_id,
            group_id: JSON.stringify(data.group_id)
            // group_id: '347'
        }
        joinToExistingFamily(params).then((dt) => {
            console.log('JOINED successfully', dt);
        }).catch((err) => {
            console.log('err while joining', err);
        })
    }

    openNextSettings = () => {
        this.setState({ basicSetting: true })
    }

    uploadPhoto = name => info => {
        if (info.file.status === 'uploading') {
            if (name === 'cover')
                this.setState({ loading: true });
            else
                this.setState({ logoLoad: true });
            return;
        }
        if (info.file.status === 'done') {
            if (name === 'cover') {
                this.getBase64(info.file.originFileObj, 'cover', imageUrl =>
                    this.setState({
                        imageUrl,
                        loading: false,
                    }),
                );
            } else {
                this.getBase64(info.file.originFileObj, 'logo', logoUrl =>
                    this.setState({
                        logoUrl,
                        logoLoad: false,
                    }),
                );
            }
        }
    };

    getBase64(img, type, callback) {
        console.log('imggg=============',img);
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
        if (type === 'cover') {
            this.setState({ cover_pic: img })
        } else {
            this.setState({ logo: img })
        }
    }
    listFamily = () => {
        var params = {
            user_id: this.state.user_id,
            group_id: this.state.group_id
            // group_id: '620'
        }
        getAllLinkedFamily(params).then((response) => {
            console.log('response', response)
            if (response.data && response.data.length > 0) {
                this.setState({ visible: true, familyList: response })
            }
        }).catch((error) => {
            console.log('error', error)
        })
    }
    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    updateFamily = () => {
        var params = {
            logo: this.state.logo,
            cover_pic: this.state.cover_pic,
            base_region: this.state.region,
            created_by: this.state.user_id,
            group_level: null,
            intro: this.state.intro,
            is_active: null,
            searchable: this.state.searchable,
            group_type: null,
            // visibility: null,
            // member_joining: 1,
            // member_approval: 5,
            // post_create: 6,
            // post_approval: 10,
            post_visibilty: 8,
            link_family: 13,
            // link_approval: 16,
            is_linkable: false
        }
        updateFamily(params).then((response) => {

        }).catch((error) => {

        })
    }
    checkedValue = (value) => e => {
        if (this.state.checkedGroups.indexOf(value) === -1) {
            this.state.checkedGroups.push(value);
            this.setState({ checkedGroups: this.state.checkedGroups })
        } else {
            var checkedGroups = [];
            checkedGroups = this.state.checkedGroups.filter(function (item) {
                return item !== value
            })
            this.setState({ checkedGroups: checkedGroups })
        }
        console.log('this.state.checkedGroups', this.state.checkedGroups);
    }

    linkToSelectedGroups = () => {
        var params = {
            to_group: this.state.checkedGroups,
            from_group: this.state.group_id,
            requested_by: this.state.user_id
        }
        requestLinkFamily(params).then((response) => {
            console.log('response', response);
        }).catch((error) => {
            console.log('error', error);
        })
    }

    render() {
        const uploadButton = (
            <div>
                <Icon type={this.state.loading ? 'loading' : 'plus'} />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        const logoButton = (
            <div>
                <Icon type={this.state.logoLoad ? 'loading' : 'plus'} />
                <div className="ant-upload-text">Upload</div>
            </div>
        )
        const { imageUrl, logoUrl } = this.state;
        return (
            <div>
                {!this.state.nextSetting &&
                    <div>
                        Family Creation
                        <div>Family name</div>
                        <div><input id='familyname' placeholder="family name" value={this.state.familyName} onChange={this.handleChange('familyName')}></input></div>
                        <div>Family Type</div>
                        <div><Select
                            value={this.state.selectType}
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select a type"
                            onChange={this.handleChange('selectType')}
                        >
                            {this.state.familyType.map((value, index) => {
                                return <Option key={index} value={value.name}>{value.name}</Option>
                            })}
                        </Select>
                        </div>
                        <div>Region</div>
                        <div><input id='region' placeholder="Region" value={this.state.region} onChange={this.handleChange('region')}></input></div>
                        <span><button onClick={this.cancelFamilyCreation}>cancel</button><button onClick={this.createFamily}>create</button></span>
                    </div>
                }
                {this.state.nextSetting && !this.state.basicSetting &&
                    <div>
                        <div>Introduction of Family</div>
                        <div className='intro-fam'> <TextArea rows={4} value={this.state.intro} placeholder="write something about your family" onChange={this.handleChange('intro')} /></div>
                        <div>Cover Image</div>
                        <span>
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                action={`${process.env.REACT_APP_API_URL}/api/v1/familheey/dummy`}
                                // beforeUpload={beforeUpload}
                                onChange={this.uploadPhoto('cover')}
                            >
                                {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                            </Upload>
                            <div>Group Logo</div>
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                action={`${process.env.REACT_APP_API_URL}/api/v1/familheey/dummy`}
                                onChange={this.uploadPhoto('logo')}
                            >
                                {logoUrl ? <img src={logoUrl} alt="avatar" style={{ width: '100%' }} /> : logoButton}
                            </Upload>
                        </span>
                        <span><button onClick={this.cancelFamilyCreation}>cancel</button><button onClick={this.openNextSettings}>Next</button></span>

                    </div>
                }
                {this.state.basicSetting &&
                    <div>
                        <div>Basic Settings</div>
                        <div>who can see the posts</div>
                        <div><Select
                            value={this.state.postAvailable}
                            showSearch
                            style={{ width: 200 }}
                            placeholder="Select a type"
                            onChange={this.handleChange('postAvailable')}
                        >
                            <Option value='public'>public</Option>
                            <Option value='private'>private</Option>
                        </Select>
                        </div>
                        <div>Searchable</div>
                        <div>
                            <Radio.Group onChange={this.handleChange('searchable')} value={this.state.searchable}>
                                <Radio value={1}>Yes</Radio>
                                <Radio value={2}>No</Radio>
                            </Radio.Group>
                        </div>
                        <div>Linkable</div>
                        <div>
                            <Radio.Group onChange={this.handleChange('linkable')} value={this.state.linkable}>
                                <Radio value={1}>Yes</Radio>
                                <Radio value={2}>No</Radio>
                            </Radio.Group>
                        </div>
                        {this.state.linkable === 1 &&
                            <div>
                                Link to other Family
                            <button onClick={this.listFamily}>
                                    <Icon type="arrow-right" /></button>
                                {this.state.familyList && this.state.familyList.data && this.state.familyList.data.length > 0 &&
                                    <Drawer
                                        title="Basic Drawer"
                                        placement="right"
                                        closable={true}
                                        onClose={this.onClose}
                                        visible={this.state.visible}
                                    >
                                        {this.state.familyList.data.map((value, index) => {
                                            return <Checkbox key={index} onChange={this.checkedValue(value.id)}>{value.group_name}</Checkbox>
                                        })}
                                        <div>
                                            <button onClick={this.linkToSelectedGroups}>link to family</button>
                                        </div>
                                    </Drawer>
                                }
                            </div>
                        }

                        <div>
                            <span><button onClick={this.cancelFamilyCreation}>cancel</button><button onClick={this.updateFamily}>Create</button></span>
                        </div>
                    </div>
                }

            </div>
        )
    }
}

export default FamilyCreation;