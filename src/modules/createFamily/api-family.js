import axios from '../../services/apiCall';
import axios1 from '../../services/fileupload';

const createFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/create_family`, data)
}

const checkforDuplicateFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/fetch_group`, data)
}

const joinToExistingFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/joinFamily`, data)
}

const inviteViaSms = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/inviteViaSms`, data)

}

const getAllLinkedFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/fetch_link_family`, data)
}

const updateFamily = (data) => {
  return axios1.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_family`, data)
}

const updateFamilyJSON = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_family`, data)
}

const updateFamilyCover = (data) => {
  return axios1.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/update_family`, data)
}

const createEvent = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/create_events`, data)
}

const checkEventLink = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/check_link`, data)
}

const getLInkedFamilyList = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/list_linked_family`, data)
}
const actionToLinkedFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/action_linked_family`, data)
}
const fetchCategory = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/fetchCategory`, data)
}
const unfollow = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/unfollow`,data)
}
const follow = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/follow`,data)
}
const bulkUpload = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/add_family_member`,data)
}

const requestLinkFamily = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/request_link_family`, data)
}
const listAllGroupRequests = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/admin_view_requests`, data)
}
const linkRequestStatus = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/admin_request_action`, data)
}
export {
  createFamily,
  checkforDuplicateFamily,
  joinToExistingFamily,
  getAllLinkedFamily,
  updateFamilyCover,
  updateFamily,
  requestLinkFamily,
  createEvent,
  checkEventLink,
  getLInkedFamilyList,
  actionToLinkedFamily,
  updateFamilyJSON,
  fetchCategory,
  unfollow,
  follow,
  bulkUpload,
  listAllGroupRequests,
  linkRequestStatus,
  inviteViaSms
}
