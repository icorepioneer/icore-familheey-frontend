import React, { useState, useEffect, useRef } from 'react';
import './style.scss'
import { Input, Button, Modal, Spin, Dropdown, Menu } from "antd";
import { requestListFull, deleteRequest } from "../RequestDetails/req_service";
import { notifications } from '../../shared/toasterMessage';
import moment from 'moment';
import CommonSideBar from '../../components/commonSideBar'
import { useSelector } from 'react-redux';
const { Search } = Input;
const { confirm } = Modal;

const RequestList = ({ history, groupId, sideBarVisible, style_passed, by_user, from_family, admin_status }) => {
    console.log(from_family)
    const [state, setState] = useState({
        loader: true,
        user_id: useSelector(stateX => stateX.user_details.userId),
        requestData: [],
        search_query: '',
        offset: 0,
        limit: 10,
        _sideBarVisible: true,
        group_id: null,
        _by_user: by_user
    });

    let { user_id, loader, requestData, search_query, offset, limit, _sideBarVisible, group_id, _by_user } = state;
    /**
     * search text set to variable
     */
    const handleonChange = (e) => {
        search_query = e.target.value;
        offset = 0;
        setState((prev) => ({
            ...prev,
            search_query: search_query,
            offset: offset,
            limit: limit,
            requestData: []
        }));
        // getRequestList();    
    }

    useEffect(() => {
        let timer_input;
        let discover = document.getElementById("req_search")
        discover.addEventListener("keyup", () => {
            clearTimeout(timer_input)
            timer_input = setTimeout(() => {
                Loader.current()
            }, 1300)
        })
    }, [])

    useEffect(() => {
        if (!groupId) {
            getRequestList()
        }
    }, [])

    useEffect(() => {
        sideBarVisible === false && setState(prev => ({ ...prev, _sideBarVisible: sideBarVisible }))
    }, [sideBarVisible])

    useEffect(() => {
        setState(prev => ({ ...prev, group_id: groupId }))
    }, [groupId])
    /**
     * list all the needs of my groups
     */
    const getRequestList = () => {
        setState((prev) => ({ ...prev, loader: true }))
        let params = {
            user_id: user_id,
            txt: search_query,
            offset: offset,
            limit: limit
        }
        group_id && (params.group_id = `${group_id}`)
        if (_by_user) params = { ...params, by_user: `${_by_user}` }
        requestListFull(params).then((response) => {
            let { data } = response.data;
            let new_data = [...requestData, ...data];
            let _offset = offset + data.length;
            setState((prev) => ({
                ...prev,
                requestData: new_data,
                offset: _offset,
                loader: false
            }))
        }).catch((err) => {
            notifications('error', 'Error', 'Something went wrong.');
        })
    }

    // implementing lazyload data
    const [element, setElement] = useState(null);
    const Loader = useRef(getRequestList)
    const observer = useRef(new IntersectionObserver((entries) => {
        let last = entries[0];
        last.isIntersecting && Loader.current();
    }, { threshold: 1 }));
    useEffect(() => {
        const currentElement = element;
        const currentObserver = observer.current;
        if (currentElement) {
            currentObserver.observe(currentElement)
        }
        return () => {
            if (currentElement) currentObserver.unobserve(currentElement);
        }
    }, [element]);
    useEffect(() => {
        Loader.current = getRequestList;
    }, [getRequestList])

    /**
     * more three dot button
     */
    const GetMenu = (d) => {
        return (
            <Menu>
                <Menu.Item key="0" onClick={() => { redirectToEdit(d.post_request_id) }}>Edit</Menu.Item>
                <Menu.Item key="1" onClick={() => { DeleteRequest(d.post_request_id) }}>Delete</Menu.Item>
            </Menu>
        )
    }
    /**
     * redirects to edit page of needs creation
     */
    const redirectToEdit = (rid) => {
        history.push({
            pathname: '/request-help',
            state: { request_id: rid, status: 'edit' }
        })
    }

    /**
     * redirect to request details page
     */
    const redirectTodetails = (item) => {
        let { post_request_id, type, to_groups } = item
        let group_Id = to_groups[0].id
        history.push({
            pathname: '/request-details',
            state: { post_request_id: post_request_id, group_id_passed: group_Id, admin_status: type,from_family:from_family }
        })
    }

    /**
     * 
     * delete post request 
     */
    const DeleteRequest = (rid) => {
        let input = {
            post_request_id: `${rid}`,
            user_id: `${user_id}`
        }

        confirm({
            title: 'Are you sure want to delete this request?',
            onOk() {
                deleteRequest(input).then((response) => {
                    let _requestData = requestData
                    _requestData = _requestData.filter(item => item.post_request_id != rid)
                    setState((prev) => ({
                        ...prev,
                        requestData: _requestData
                    }));
                }).catch((err) => {
                    notifications('error', 'Error', 'Something went wrong.');
                });
            },
            onCancel() { }
        });
    }


    const myContr = (val) => {
        offset = 0; _by_user = val; requestData = [];
        setState((prev) => ({
            ...prev,
            offset: offset,
            _by_user: _by_user,
            requestData: requestData
        }));
        getRequestList();
    }
    return (
        <section className="RequestList f-page" style={style_passed}>
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-post-listing">
                        {_sideBarVisible && <CommonSideBar history={history} />}
                        {/* <Button className="f-create-form-back" onClick={()=> history.push("/")} /> */}

                        <div className="f-clearfix f-request-details-wrap">
                            <div className="f-posts-create f-force-hide-tab">
                                <Button
                                    onClick={() => history.push({ pathname: '/request-help', state: { status: 'add', from_family: from_family } })}
                                > Create a Request <img src={require('../../assets/images/icon/plus.svg')} /></Button>
                            </div>
                            <div className="f-req-details-header">
                                <div className="f-left">
                                    <h4>Request List</h4>
                                    {/* <h5><em>124</em> Needs</h5> */}

                                </div>
                                <Search
                                    id="req_search"
                                    placeholder="Search"
                                    value={search_query}
                                    onChange={(e) => handleonChange(e)}
                                />
                            </div>
                            <div className="f-req-loop">
                                <div style={{ "textAlign": 'right' }}>
                                    {
                                        _by_user == 1 &&
                                        <a style={{ 'cursor': 'pointer' }}
                                            onClick={(e) => myContr(2)}
                                            className="f-btn">
                                            View My Contribution
                                </a>
                                    }
                                    {
                                        _by_user == 2 &&
                                        <a style={{ 'cursor': 'pointer' }}
                                            onClick={(e) => myContr(1)}
                                            className="f-btn">
                                            View My requests
                                </a>
                                    }
                                </div>

                                {requestData.length > 0 &&
                                    requestData.map((item, index) => (
                                        <div
                                            className="f-req-author-card"
                                            onClick={() => redirectTodetails(item)}
                                            style={{ 'cursor': 'pointer' }}
                                            key={index}
                                        >
                                            <div className="f-req-author-card-header">
                                                <div className="f-left">
                                                    <img onError={(e) => { e.target.onerror = null; e.target.src = "images/default_propic.png" }} src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic} />
                                                </div>
                                                <div className="f-right">
                                                    <h2>{item.full_name}</h2>
                                                    <h3>Posted in {item.to_groups.length > 0 &&
                                                        item.to_groups[0].group_name
                                                    } {item.to_groups.length > 1 &&
                                                        `and other ${item.to_groups.length - 1} groups`
                                                        } </h3>
                                                    <h4>{moment(item.created_at).format('MMM DD YYYY  hh:mm A')}</h4>
                                                </div>
                                                <div className="f-more" onClick={(e) => e.stopPropagation()}>
                                                    {user_id == item.user_id &&
                                                        <Dropdown overlay={() => GetMenu(item)} trigger={['click']}>
                                                            <Button />
                                                        </Dropdown>
                                                    }
                                                </div>
                                            </div>
                                            <div className="f-req-author-card-body">
                                                <ul className="f-req-author-items-list">
                                                    {item.items.length > 0 &&
                                                        item.items.map((doc, _ind) => (
                                                            <li key={_ind}>
                                                                <span className="f-left">
                                                                    <h4>{doc.request_item_title}</h4>
                                                                    <p>{doc.request_item_description}</p>
                                                                </span>
                                                                <span className="f-right">
                                                                    {
                                                                        doc.item_quantity - doc.received_amount > 0 ? (
                                                                            <strong><em>{(doc.item_quantity - doc.received_amount)}</em> of {doc.item_quantity}</strong>
                                                                        ) : ('')
                                                                    }
                                                                    {/* <strong><em>{ (doc.item_quantity - doc.total_contribution)}</em> of {doc.item_quantity}</strong> */}
                                                                    {(doc.item_quantity - doc.received_amount) > 0 ?
                                                                        <p>Still needed</p> : <p style={{ color: '#4bae50' }}>Completed</p>}

                                                                </span>
                                                            </li>
                                                        ))

                                                    }

                                                </ul>
                                                {/* <div className="f-clearfix f-more">
                                        <Button className="f-btn">View More</Button>
                                    </div> */}
                                            </div>
                                            <div className="f-req-author-card-footer">
                                                <div className="f-left">
                                                    <em className="f-tm">
                                                        {moment.unix(item.start_date).format('MMM DD YYYY  hh:mm A')}
                                                    </em>
                                                    <em className="f-lc">{item.request_location}</em>
                                                </div>
                                                <div className="f-right">
                                                    <strong>{item.supporters}</strong>
                                                    <p>Supporters</p>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                            {loader && <Spin
                                tip="Loading..."
                                style={{ 'marginLeft': '50%' }}></Spin>}
                        </div>
                    </div>
                </div>
                <div ref={setElement}></div>
            </div>
        </section>
    );
}
export default RequestList;