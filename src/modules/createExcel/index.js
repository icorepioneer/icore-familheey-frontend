import React, { useState } from 'react';
import { Icon } from "antd";
import { createExcel } from './index-api';

const CreateExcel = () =>{
    const [state, setState] = useState({
        index:0,
        username:'',
        phone:'',
        userarray:[]
    });

    let { index, username, phone, userarray } = state;

    const saveData = () =>{
        index++
        let params = { username:username, phone:phone, index:index }
        userarray.push(params)
        setState((prev)=>({
            ...prev,
            userarray:userarray,
            index:index
        }));
        createExcel(params).then((doc)=>{
            console.log(doc)
        }).catch((err)=>{
            console.log(err)
        })
    }

    const handleChange = (e) => {
        const { name, value } = e.target; 
        setState((prev)=>({
            ...prev,
            [name]: value
        }));
    }

    return (
        <section className="RequestList f-page">

            <div className="f-clearfix f-listland-card">
                <div className="f-modal-wiz-body">
                    <div className="f-form-control">
                        <table border="1">
                            <tr>
                                <td>Name</td>
                                <td>Phone Number</td>
                                <th>Actions</th>
                            </tr>
                            <tr>
                            <td>
                                <input placeholder="Name" name="username" value={username} onChange={(e)=>handleChange(e)}/>
                            </td>
                            <td>
                                <input placeholder="Phone" name="phone" value={phone} onChange={(e)=>handleChange(e)}/>
                            </td>
                            <td><Icon className="f-clickable" type="plus-square" onClick={saveData} /></td>

                            </tr>
                            
                        </table>


                        <table>
                            {
                                userarray.map((value, ind_ex) => {
                                    return(<tr>
                                        <td>{ value.index}</td>
                                        <td>{value.username}</td>
                                        <td>{value.phone}</td>
                                    </tr>)
                                })
                            }
                        </table>
                    </div>
                </div>
            </div>


        </section>
    )
}
export default CreateExcel;