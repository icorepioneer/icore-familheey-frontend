import axios from '../../services/apiCall';

const contactForm = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/contactForm`, data)
}

export {
  contactForm
}
