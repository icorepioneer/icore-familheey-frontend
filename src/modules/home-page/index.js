import React from 'react';
import { Carousel, Spin } from 'antd';
import { contactForm } from './home-api';
import { notifications } from '../../shared/toasterMessage';
import './style.scss'
class HomePage extends React.Component {
	constructor(props){
		super(props)
		if (localStorage.getItem('user_id')) {
			window.location.href='/post'
		}
		this.state = {
			_loader:false,
			mainLoader:true
		}
	}

	componentDidMount(){
		this.setState({ mainLoader:false })
		window.addEventListener('scroll', this.stickyMenu)
	}
	submitForm = (e) => {
		e.preventDefault();
		this.setState({ _loader:true })
		var elements = e.target.elements;
		let formVal = {
			firstname:elements.firstname.value,
			lastname:elements.lastname.value,
			phone:elements.phone.value,
			email:elements.email.value,
			discuss:elements.discuss.value
		}
		contactForm(formVal).then(()=>{
			this.setState({ _loader:false });
			document.getElementById("Form").reset();
			notifications("success","success","Thank you! Your message has been successfully sent. We will contact you very soon!")
		}).catch((err)=>{
			console.log(err)
			notifications("error","Error","Something went wrong")
		})
	}

	toggleMenu = ()=> {
		document.getElementById('mega-menu-holder').classList.toggle("show-menu");
	}

	scrollTop = () => {
		window.scrollTo({ top:0, behavior: 'smooth'});
	}
	
	hideMenu = (e,type) => {
		e.preventDefault();
		document.getElementById('mega-menu-holder').classList.remove("show-menu");
		document.getElementById(`${type}`).scrollIntoView({behavior: 'smooth'});
	}

	stickyMenu = () => {
		let elem1 = document.getElementById('theme-main-menu');
		let elem2 = document.getElementById('home-page-react');
		if (window.pageYOffset >= 100) {
			if(elem1) elem1.classList.add('fixed');
			if(elem2) elem2.classList.add('react-fixed');
		}
		else {
			if(elem1) elem1.classList.remove('fixed');
			if(elem2) elem2.classList.remove('react-fixed');
		}
	}
	
	render() {
		return (
			<section className="home-page-react" id="home-page-react">
				<div className="main-page-wrapper">
					{this.state.mainLoader && <div id="loader-wrapper">
						<div id="loader" />
					</div>}
					<div className="html-top-content">
						<div className="theme-top-section">
							<header className="theme-main-menu" id="theme-main-menu">
								<div className="container">
									<div className="menu-wrapper clearfix">
										<div className="logo"><a onClick={(e) => this.hideMenu(e,'home-page-react')}><img src={require('./images/logo/logo1.png')} alt="Logo" /></a>
										</div>
										<ul className="right-widget celarfix">
											<li className="login-button"><a onClick={ ()=> this.props.history.push('/signin') }>Login <i>&rarr;</i></a></li>
										</ul>
										<nav className="navbar navbar-expand-lg" id="mega-menu-holder">
											<div className="container">
												<button className="navbar-toggler" type="button" onClick={() => this.toggleMenu()}>
													<i className="icomoon-menu" />
												</button>
												<div className="collapse navbar-collapse" id="navbarResponsive">
													<ul className="navbar-nav">
														<li className="nav-item">
															<a className="nav-link js-scroll-trigger" href="#" onClick={(e) => this.hideMenu(e,'services')}>Features</a>
														</li>
														<li className="nav-item">
															<a className="nav-link js-scroll-trigger" href="#" onClick={(e) => this.hideMenu(e,'apps-review')}>Download</a>
														</li>
														<li className="nav-item">
															<a className="nav-link js-scroll-trigger" href="#" onClick={(e) => this.hideMenu(e,'education')}>FamilheeyLearn</a>
														</li>
														<li className="nav-item">
															<a className="nav-link js-scroll-trigger" href="#" onClick={(e) => this.hideMenu(e,'testimonial')}>Testimonials</a>
														</li>
														<li className="nav-item">
															<a className="nav-link js-scroll-trigger" href="#" onClick={(e) => this.hideMenu(e,'contact')}>Contact us</a>
														</li>
														<li className="nav-item">
															<a className="nav-link" href="http://blog.familheey.com" target="_blank" rel="noopener noreferrer">Blog</a>
														</li>
													</ul>
												</div>
											</div>
										</nav>
									</div>
								</div>
							</header>

							<div id="theme-banner" className="theme-banner-one">
								<img src={require('./images/home/k-1.png')} alt="" className="illustration" />
								<img src={require('./images/icon/1.png')} alt="" className="icon-shape-one" />
								<img src={require('./images/icon/2.png')} alt="" className="icon-shape-two" />
								<img src={require('./images/icon/3.png')} alt="" className="icon-shape-three" />
								<div className="round-shape-one" />
								<div className="round-shape-two"><img src={require('./images/icon/4.png')} alt="" /></div>
								<div className="round-shape-three" />
								<div className="container">
									<div className="main-text-wrapper">
										<h1>Welcome to <br />Familheey!</h1>
										<p>The mobile app that brings <br />your family and community together</p>
										<ul className="button-group clearfix">
											<li><a href="#apps-review" onClick={(e) => this.hideMenu(e,'apps-review')}>Download</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div className="our-features-one" id="features">
							<div className="container">
								<div className="theme-title">
									<h2>Begin your New Life at <span><em> <b> Familheey! </b></em> </span></h2>
								</div>
								<div className="row">
									<div className="col-md-4 col-xs-12">
										<div className="single-feature">
											<div className="icon-box">
												<img src={require('./images/icon/a1.png')} alt="" className="primary-icon" />
											</div>
											<h3>Personal</h3>
											<p>Bring your family closer now with your own exclusive family group.</p>
										</div>
									</div>
									<div className="col-md-4 col-xs-12">
										<div className="single-feature border-fix">
											<div className="icon-box">
												<img src={require('./images/icon/a2.png')} alt="" className="primary-icon" />
											</div>
											<h3>Professional</h3>
											<p>Create a space for professional work team to bond and achieve together.</p>
										</div>
									</div>
									<div className="col-md-4 col-xs-12">
										<div className="single-feature">
											<div className="icon-box">
												<img src={require('./images/icon/a3.png')} alt="" className="primary-icon" />
											</div>
											<h3>Community</h3>
											<p>Build a space where your community members can stand together.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="our-feature-two" id="services">
							<div className="container">
								<div className="row single-block">
									<div className="col-lg-6">
										<div className="text">
											<div className="number">01</div>
											<h2 className="title"><span>Posts</span> Family Broadcast</h2>
											<p>Share your videos, photos and posts with your family groups. Get instant access to all the posts shared across various family groups.</p>
										</div>
									</div>
									<div className="col-lg-6 img-box">
										<div><img src={require('./images/shape/1.png')} alt="" /></div>
									</div>
								</div>

								<div className="row single-block">
									<div className="col-lg-6 order-lg-last">
										<div className="text">
											<div className="number">02</div>
											<h2 className="title"><span>Conversations</span> Engage with Everyone</h2>
											<p>Engage with all your family members. Keep all your conversations on track. Bring everyone together.</p>
										</div>
									</div>
									<div className="col-lg-6 order-lg-first img-box">
										<div><img src={require('./images/shape/1.1.png')} alt="" /></div>
									</div>
								</div>

								<div className="row single-block">
									<div className="col-lg-6">
										<div className="text">
											<div className="number">03</div>
											<h2 className="title"><span>Announcements</span> </h2>
											<p>Make your important announcements stand out from the other. Ensure that everyone gets the important messagesâ€”on time, every time.</p>
										</div>
									</div>
									<div className="col-lg-6 img-box">
										<div><img src={require('./images/shape/2.png')} alt="" /></div>
									</div>
								</div>

								<div className="row single-block">
									<div className="col-lg-6 order-lg-last">
										<div className="text">
											<div className="number">04</div>
											<h2 className="title"><span>Requests</span> </h2>
											<p>Publish your requests so that everyone can contribute to support the cause of your family, company or community at this time of need.</p>
										</div>
									</div>
									<div className="col-lg-6 order-lg-first img-box">
										<div><img src={require('./images/shape/1_2.png')} alt="" /></div>
									</div>
								</div>

								<div className="row single-block">
									<div className="col-lg-6">
										<div className="text">
											<div className="number">05</div>
											<h2 className="title"><span>Showcase</span> </h2>
											<p>Upload your favourite pictures and videos in the album for a lifetime of sharing. Store all your important documents in the cloud and share it with everyone.</p>
										</div>
									</div>
									<div className="col-lg-6 img-box">
										<div><img src={require('./images/shape/2_2.png')} alt="" /></div>
									</div>
								</div>

								<div className="row single-block">
									<div className="col-lg-6 order-lg-last">
										<div className="text">
											<div className="number">06</div>
											<h2 className="title"><span>Calendar</span></h2>
											<p>Be the perfect planner. Create memories by inviting your family members to online events. Use our calendar and third-party applications to fill your calendar with memories across the world.</p>
										</div>
									</div>
									<div className="col-lg-6 order-lg-first img-box">
										<div><img src={require('./images/shape/1_1.png')} alt="" /></div>
									</div>
								</div>

							</div>
						</div>
						<div className="apps-overview color-one" id="apps-review">
							<div className="overlay-bg">
								<div className="container">
									<div className="inner-wrapper">
										<img src={require('./images/home/s8.png')} alt="" className="s8-mockup" />
										<img src={require('./images/home/x.png')} alt="" className="x-mockup" />
										<div className="row">
											<div className="col-lg-5 offset-lg-7">
												<div className="text">
													<h3>Letâ€™s Get Together!</h3>
													<h2>Download the Familheey App Now!</h2>
													<h6>Now you can stay connected with your family 24x7 around the world!</h6>
													<p>Familheey enables you to stay connected with the latest alerts and updates that will keep you posted about your family. Access this information in a media-rich environment that stores your information securely.</p>
													<ul className="button-group">
														<li><a href="https://apps.apple.com/us/app/familheey/id1485617876" target="_blank" rel="noopener noreferrer"><img src={require('./images/icon/appstore.png')} alt="" /> Apple Store</a></li>
														<li><a
														//  href="https://familheey-android-apk.s3.amazonaws.com/app-release.apk" 
														href="https://play.google.com/store/apps/details?id=com.familheey.app&hl=en"
														 target="_blank" rel="noopener noreferrer"><img src={require('./images/icon/playstore.png')} alt="" /> Google Play</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="our-features-one" id="education">
							<div className="container">
								<div className="theme-title">
									<h2>Education begins at <span><em>FamilheeyLearn!</em> </span></h2>
								</div>
								<div className="row">
									<div className="col-md-4 col-xs-12">
										<div className="single-feature">
											<div className="icon-box">
												<img src={require('./images/icon/e1.png')} alt="" className="primary-icon" />
											</div>
											<h3>Communication</h3>
											<p>Familheey brings students, teachers and learning communities on a single platform to enable online learning.</p>
										</div>
									</div>
									<div className="col-md-4 col-xs-12">
										<div className="single-feature border-fix">
											<div className="icon-box">
												<img src={require('./images/icon/e2.png')} alt="" className="primary-icon" />
											</div>
											<h3>Collaboration</h3>
											<p>Familheey enables students to learn and collaborate effectively through online classes, academic projects, and class assessments.</p>
										</div>
									</div>
									<div className="col-md-4 col-xs-12">
										<div className="single-feature">
											<div className="icon-box">
												<img src={require('./images/icon/e3.png')} alt="" className="primary-icon" />
											</div>
											<h3>Co-learning Spaces</h3>
											<p>Familheey enables the creation of learning spaces where academic curriculum can be stored and shared in a secure environment.</p>
										</div>
									</div>
								</div>
								<div className="row">
								<div className="col-md-12 col-xs-12 text-center">
									<p>Learn about our exclusive online learning initiative for the education sector: <a href="https://blog.familheey.com/familheey-community-app-for-education" target="_blank">Read More</a></p>
								</div>
								</div>
							</div>
						</div>
						<div className="testimonial-section" id="testimonial">
							<div className="container">
								<div className="theme-title text-center">
									<h2><span>Testimonials</span></h2>
									<p>Listen to our Familheey Community speak on their experience of using our mobile app to bring together their family, work teams and communities. </p>
								</div>

								{/* <div className="testimonial-slider"> */}
								<Carousel autoplay={true} dots={true} dotPosition={'bottom'}>
									<div className="item">
										<div className="single-block clearfix">
											<div className="img-block"><img src={require('./images/home/1.jpg')} alt="" /></div>
											<div className="text">
												<h3 className="name">C. Chris Ulasi, Ph.D.</h3>
												<span>Writer, Producer, Director at Diaspora Media Network, Inc</span>
												<p>â€œThis incredible platform will transform the way we communicate, collaborate, learn, and socialize in the future.â€</p>
											</div>
										</div>
									</div>
									<div className="item">
										<div className="single-block clearfix">
											<div className="img-block"><img src={require('./images/home/2.jpg')} alt="" /></div>
											<div className="text">
												<h3 className="name">Antony Satyadas</h3>
												<span>CEO at Innovation Incubator Group of Companies</span>
												<p>â€œFamilheey enables individuals to communicate &amp; collaborate in a structured way in their personal, professional &amp; social life with right way of transparency.â€</p>
											</div>
										</div>
									</div>
									<div className="item">
										<div className="single-block clearfix">
											<div className="img-block"><img src={require('./images/home/4.jpg')} alt="" /></div>
											<div className="text">
												<h3 className="name">Dr Tanya Unni</h3>
												<span>Director at Amtan Medical Group and Skin Lab &amp; Beauty</span>
												<p>â€œThe strucutred communication within Familheey enable physicians to communicate with their clients and colleagues in a much better way than any other applications out there.â€</p>
											</div>
										</div>
									</div>
									<div className="item">
										<div className="single-block clearfix">
											<div className="img-block"><img src={require('./images/home/5.jpg')} alt="" /></div>
											<div className="text">
												<h3 className="name">Hari Namboodiri</h3>
												<span>Executive Director, SavaSeniorCare Administrative Services LLC</span>
												<p>â€œLet us bring Familheey a new Happiness Index. It should include all the aspects of identifying the needs and wants of that particular family or particular group ...particular community, or particular organization.â€</p>
											</div>
										</div>
									</div>
								</Carousel>
								{/* </div> */}

							</div>
						</div>

						<div className="contact-us-one" id="contact">
							<div className="overlay">
								<div className="container">
									<div className="theme-title text-center">
										<h2>Get in Touch with Us</h2>
										<p>At Familheey, we are ready to connect you with your family, team and community.<br />If you have a question, please take a moment to fill the form below. We will revert to you as soon as possible. </p>
									</div>
									<form id="Form" ref='form' className="form-validation" autoComplete="off" onSubmit={(e)=>this.submitForm(e)}>
										<div className="row">
											<div className="col-md-6">
												<label>First Name*</label>
												<input type="text" placeholder="First Name" name="firstname" required/>
											</div>
											<div className="col-md-6">
												<label>Last Name*</label>
												<input type="text" placeholder="Last Name" name="lastname" required/>
											</div>
											<div className="col-md-6">
												<label>Email*</label>
												<input type="email" placeholder="Email Address" name="email" required/>
											</div>
											<div className="col-md-6">
												<label>Phone</label>
												<input type="text" placeholder="Phone Number" name="phone" />
											</div>
											<div className="col-12">
												<label>Message*</label>
												<input type="text" name="discuss" placeholder="Message" required/>
											</div>
										</div>
										<button type="submit">{this.state._loader &&<Spin></Spin>}Send Message</button>
									</form>
									<div className="alert-wrapper" id="alert-success">
										<div id="success">
											<button className="closeAlert"><i className="icomoon-cross" /></button>
											<div className="wrapper">
												<p>Your message was sent successfully.</p>
											</div>
										</div>
									</div>
									<div className="alert-wrapper" id="alert-error">
										<div id="error">
											<button className="closeAlert"><i className="icomoon-cross" /></button>
											<div className="wrapper">
												<p>Sorry!Something Went Wrong.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<footer className="theme-footer">
							<div className="container">
								<div className="inner-wrapper">
									<div className="top-footer-data-wrapper">
										<div className="row">
											<div className="col-lg-3 col-sm-6 footer-logo">
												<a href="mailto:contact@familheey.com" className="email">contact@familheey.com</a>
												<a href="tel:+17134949882" className="mobile">+1 (713) 494-9882</a>
											</div>
											<div className="col-lg-3 col-sm-6 footer-list">
											</div>
											<div className="col-lg-3 col-sm-6 footer-list">
												<h4 className="title">Downloads</h4>
												<ul>
													<li><a href="https://apps.apple.com/us/app/familheey/id1485617876">Appstore</a></li>
													<li><a 
													//  href="https://familheey-android-apk.s3.amazonaws.com/app-release.apk"
													href="https://play.google.com/store/apps/details?id=com.familheey.app&hl=en"
													>Playstore</a></li>
												</ul>
											</div>
											<div className="col-lg-3 col-sm-6 footer-list">
												<h4 className="title">Quick Links</h4>
												<ul>
													<li><a href="http://blog.familheey.com" target="_blank" rel="noopener noreferrer">Blog</a></li>
													<li><a href="https://www.familheey.com/termsofservice" target="_blank" rel="noopener noreferrer">Terms and Conditions</a></li>
													<li><a href="https://www.familheey.com/policy" target="_blank" rel="noopener noreferrer">Privacy Policy</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<div className="bottom-footer clearfix">
									<p className="copyright">&copy; 2020 Familheey All Right Reserved</p>
									<ul>
										<li><a href="https://www.facebook.com/familheey/" target="_blank" rel="noopener noreferrer"><i className="icomoon-facebook" /></a></li>
										<li><a href="https://www.instagram.com/familheey" target="_blank" rel="noopener noreferrer"><i className="icomoon-instagram" /></a></li>
										<li><a href="https://twitter.com/familheey" target="_blank" rel="noopener noreferrer"><i className="icomoon-twitter" /></a></li>
										<li><a href="https://www.linkedin.com/company/familheey/" target="_blank" rel="noopener noreferrer"><i className="icomoon-linkedin2" /></a></li>
										<li><a href="https://www.youtube.com/channel/UC_wFd3B_peDftT72sz8FCYA/" target="_blank" rel="noopener noreferrer"><i className="icomoon-youtube" /></a></li>
									</ul>
								</div>
							</div>
						</footer>
					</div>
					<button className="scroll-top tran3s color-one-bg" onClick={()=>this.scrollTop()}>
						<i className="icomoon-arrow-up2" />
					</button>
				</div>
			</section>
		)
	}
}

export default HomePage;