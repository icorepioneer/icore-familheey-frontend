import React, { useState, useEffect } from 'react'
import { Form, Input, Button, Switch } from 'antd'
import { getClientSecret, getAllCardList } from './stripe-api'
import { useSelector } from 'react-redux'
import { createContribute } from '../../modules/RequestDetails/req_service'
const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        state = {
            _loading: false,
        }
        render() {
            const { form, handleSubmit, handleChange, payLater } = this.props;
            const { getFieldDecorator } = form;
            const customCountValidator = (rule, value, callback) => {
                if (value) {
                    if (value <= 0) {
                        return callback('Please provide valid digit greater than zero')
                    }
                }
                callback()
            }
            return (
                <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                    <Form layout="vertical" autoComplete="off" >
                        <div style={{display:'flex',height:'40px'}}>
                            <h6>Pay as anonymous</h6>
                            <Form.Item style={{ float: 'left',marginLeft:'10px' }}>
                                {getFieldDecorator('is_anonymous', {initialValue:false})(<Switch />)}
                            </Form.Item>
                        </div>
                        <div className="f-form-control" style={{ marginTop: '0' }}>
                            <Form.Item  hasFeedback>
                                {getFieldDecorator("amount", {
                                    rules: [
                                        {
                                            required: true,
                                            message: ""
                                        },
                                        { validator: customCountValidator, }
                                    ]
                                })(<Input
                                    placeholder="Please input the amount here"
                                    min={0}
                                    type="number"
                                    autoComplete="off"
                                    onChange={(e) => handleChange(e)}
                                />)}
                            </Form.Item>
                            <div>
                                <Form.Item style={{ float: 'left' }}>
                                    <Button
                                        type="submit"
                                        onClick={payLater}
                                    >Pay Later</Button>
                                </Form.Item>
                                <Form.Item style={{ float: 'right' }}>
                                    <Button
                                        type="submit"
                                        onClick={handleSubmit}
                                    >Pay Now</Button>
                                </Form.Item>
                            </div>
                        </div>
                    </Form>
                </div>
            );
        }
    },
);

const FirstPage = ({ togglePage, groupId, post_request_id, current_selected_item_id, handleChange, showThanYou, toggleStripeModal,changeAnonmStatus }) => {
    let formReff;
    let red_state = useSelector(rd_state => rd_state)
    const [state, setState] = useState({
        user_id: undefined,
        cardList: undefined,
        full_name:undefined,
        phone_no:undefined
    })
    let { user_id,full_name,phone_no } = state

    const saveFormRef = (formRef) => {
        formReff = formRef;
    };
    const handleSubmit = () => {
        const { form } = formReff.props
        form.validateFields((err, values) => {
            console.log(" = = = values = = =", values)
            if (err) {
                return
            }
            let { amount,is_anonymous } = values
            let params = {
                amount: parseInt(amount) * 100,
                user_id: `${user_id}`,
                group_id: `${groupId}`,
                to_type: "request",
                to_type_id: `${post_request_id}`,
                to_subtype: "item",
                to_subtype_id: `${current_selected_item_id}`,
                is_anonymous:is_anonymous,
                paid_user_name:full_name,
                phone_no:`${phone_no}`
            }
            console.log(params)
            getClientSecret(params).then(resp => {
                let { data } = resp
                let { client_secret } = data
                form.resetFields()
                changeAnonmStatus(is_anonymous)
                togglePage("stripe", amount, client_secret)
            })
            setState(prev => ({ ...prev, amount: amount }))
        })
    }

    const payLater = () => {
        const { form } = formReff.props
        form.validateFields((err, values) => {
            if (err) {
                return
            }
            let { amount,is_anonymous } = values

            let params = {
                contribute_user_id: `${user_id}`,
                post_request_item_id: `${current_selected_item_id}`,
                contribute_item_quantity: `${amount}`,
                group_id:groupId,
                is_anonymous:is_anonymous
            }
            createContribute(params).then(res => {
                form.resetFields()
                toggleStripeModal()
            })
        })
    }
    useEffect(() => {
        let { user_details } = red_state
        let { userId } = user_details
        setState(prev => ({ ...prev, user_id: userId,full_name:user_details.user_details.full_name,phone_no:user_details.user_details.phone }))
    }, [red_state])

    useEffect(() => {
        if (user_id) {
            let params = {
                user_id: `${user_id}`
            }
            getAllCardList(params).then(res => {
                let { data } = res
                setState(prev => ({ ...prev, cardList: data }))
            })
        }
    }, [user_id])
    return (
        <React.Fragment>
            <CollectionCreateForm
                wrappedComponentRef={(formRef) => saveFormRef(formRef)}
                handleSubmit={handleSubmit}
                handleChange={handleChange}
                payLater={payLater}
            />
        </React.Fragment>
    )
}

export default FirstPage