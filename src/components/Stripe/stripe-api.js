import axios from '../../services/apiCall';
const getClientSecret = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/payment/stripeCreatePaymentIntent`, data)
}
const getAllCardList = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/payment/listAllCards`, data)
}

export {
    getClientSecret,
    getAllCardList
} 