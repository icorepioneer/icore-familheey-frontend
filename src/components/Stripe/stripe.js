import React, { useState, useEffect } from 'react'
import { loadStripe } from '@stripe/stripe-js'
//stripe comps
import { Elements, CardElement, useStripe, useElements } from '@stripe/react-stripe-js'
import { Button, Modal, Radio, Input } from 'antd';
import FirstPage from './firstPage'
import styled from 'styled-components'
import CheckoutError from './checkoutError'
import { createContribute } from '../../modules/RequestDetails/req_service'
import { useSelector } from 'react-redux'
const CardElementContainer = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  margin-top: 20px;

  & .StripeElement {
    width: 100%;
    padding: 15px;
  }
`;

const Row = styled.div`
  width: 475px;
  margin: 30px auto;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 white;
  border-radius: 4px;
  background-color: relative;
  position: relative;
`;

const ButtonRow = styled.div`
  width: 475px;
  margin: 30px auto;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 white;
  border-radius: 4px;
  background-color: relative;
  position: relative;
`;

const ButtonRowOffline = styled.div`
  width: 475px;
  margin-top:31px;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 white;
  border-radius: 4px;
  background-color: relative;
  position: relative;
`;
const stripePromise = loadStripe(`${process.env.REACT_APP_STRIPE_KEY}`)
const { TextArea } = Input
const CheckoutForm = ({ amount, client_id, toggleStripeModal, showThanYou, togglePage }) => {
    const [isProcessing, setProcessingTo] = useState(false);
    const [checkoutError, setCheckoutError] = useState();
    const stripe = useStripe()
    const elements = useElements()

    const handleCardDetailsChange = ev => {
        ev.error ? setCheckoutError(ev.error.message) : setCheckoutError();
    };

    const handleFormSubmit = async ev => {
        ev.preventDefault();
        if (!stripe || !elements) {
            return
        }
        setProcessingTo(true);

        const cardElement = elements.getElement("card");
        try {
            await stripe.createPaymentMethod({
                type: "card",
                card: cardElement,
            }).then(async result => {
                let { error } = result
                if (error) {
                    setCheckoutError(error.message);
                    setProcessingTo(false);
                    return;
                }
                await stripe.confirmCardPayment(`${client_id}`, {
                    payment_method: { card: cardElement }
                }).then((_result) => {
                    let { error:e1 } = _result
                    if (e1) {
                        setCheckoutError(e1.message)
                        setProcessingTo(false)
                        return
                    }
                    toggleStripeModal()
                    showThanYou()
                    setProcessingTo(false)
                    togglePage("amount_page", '0', '')
                })
            })
        } catch (err) {
            console.log(err)
            setCheckoutError(err.message);
        }
    };
    const cardElementOpts = {
        iconStyle: "solid",
        hidePostalCode: true,
    };

    return (
        <form>
            <Row>
                <CardElementContainer>
                    <CardElement
                        options={cardElementOpts}
                        onChange={handleCardDetailsChange}
                    />
                </CardElementContainer>
            </Row>
            {checkoutError && <CheckoutError>{checkoutError}</CheckoutError>}
            <ButtonRow>
                <Button
                    onClick={handleFormSubmit}
                    // style={{ width: '100%', height: '40px' }}
                    style={{
                        width:'100%',
                        height:'40px',
                        backgroundColor: 'var(--green)',
                        border: '1px solid var(--green)',
                        color: '#FFF',
                    }}
                    type="submit"
                    disabled={isProcessing || !stripe}
                >
                    {isProcessing ? "Processing..." : `Pay $${amount}`}
                </Button>
            </ButtonRow>
            <div style={{ textAlign: 'center' }}><h6>powered by Stripe</h6></div>
        </form>
    )
}
const StripeComp = ({ contrPayLaterObj,
    modalVisible,
    toggleStripeModal,
    post_request_id,
    reqDetails,
    current_selected_item_id,
    handleChange,
    showThanYou,
    callback,
    MembPayLaterObj,
    memberOfflinePayment }) => {
    let red_state = useSelector(rd_state => rd_state)
    const [state, setState] = useState({
        current: "amount_page",
        amount: 0,
        client_id: '',
        requestDetails: undefined,
        requestId: 0,
        groupId: undefined,
        defaultPayment: 'online',
        defaultOffline: 'cash',
        user_id: undefined,
        item_id: undefined,
        offline_payment_loading: false,
        contributionId: undefined,
        note: '',
        current_item: undefined,
        disable_online_payment: false,
        incoming_payment_type: undefined,
        is_anonymous:false
    })
    let { disable_online_payment, current, amount, user_id, client_id, note, current_item, groupId, defaultPayment, defaultOffline, item_id,
        offline_payment_loading, contributionId, incoming_payment_type,is_anonymous } = state

    const togglePage = (tag, am, cid) => {
        setState(prev => ({ ...prev, current: `${tag}`, amount: am, client_id: cid }))
    }
    useEffect(() => {
        if (reqDetails) {
            let { to_groups } = reqDetails
            let { id } = to_groups[0]
            setState(prev => ({ ...prev, groupId: id, requestDetails: reqDetails }))
        }
    }, [reqDetails])

    useEffect(() => {
        let { user_details } = red_state
        let { userId } = user_details
        setState(prev => ({ ...prev, user_id: userId }))
    }, [red_state])

    const togglePaymentModal = (flag = null) => {
        setState(prev => ({
            ...prev,
            defaultPayment: 'online',
            defaultOffline: 'cash',
            note: '',
        }))
        toggleStripeModal(flag)
    }
    const onlineOfflineTab = () => {
        switch (defaultPayment) {
            case 'online':
                return (
                    <Elements stripe={stripePromise} >
                        <CheckoutForm
                            amount={amount}
                            client_id={client_id}
                            toggleStripeModal={() => togglePaymentModal()}
                            showThanYou={showThanYou}
                            togglePage={togglePage}
                        />
                    </Elements>
                )
            case 'offline':
                return (
                    <div style={{ marginLeft: '55px', marginTop: '20px', borderTop: '.5px solid black', paddingTop: '20px' }}>
                        <div style={{ display: 'flex' }}>
                            <Radio.Group
                                value={defaultOffline}
                                onChange={(e) => changePaymentMode(e, 'defaultOffline')}
                            >
                                <Radio value={'cash'}>Cash</Radio>
                                <Radio value={'cheque'}>Cheque</Radio>
                                <Radio value={'other'}>Other</Radio>
                            </Radio.Group>
                        </div>
                        <div style={{ marginTop: '15px' }}>
                            <h6>Note</h6>
                            <TextArea
                                defaultValue={note}
                                style={{ width: '475px' }}
                                placeholder="please specify cheque number, transaction id etc.."
                                rows={3}
                                onChange={(e) => { setOfflineNote(e) }}
                            />
                        </div>
                        <ButtonRowOffline>
                            <Button
                                loading={offline_payment_loading}
                                onClick={() => { payLaterByOffline() }}
                                // style={{ width: '100%', height: '40px', marginLeft: '0px' }}
                                style={{
                                    width:'100%',
                                    height:'40px',
                                    marginLeft: '0px',
                                    backgroundColor: 'var(--green)',
                                    border: '1px solid var(--green)',
                                    color: '#FFF',
                                }}
                                disabled={offline_payment_loading}
                            >
                                {offline_payment_loading ? 'Processing....' : `Pay $${amount}`}
                            </Button>
                        </ButtonRowOffline>
                    </div>
                )
        }

    }
    const setOfflineNote = (event) => {
        event.persist()
        setState(prev => ({ ...prev, note: event.target.value }))
    }
    const payLaterByOffline = () => {
        if (incoming_payment_type === 'Member') {
            let memberPaymentParams={
                id:`${contributionId}`,
                membership_payment_notes:note,
                membership_payment_type:"OFFLINE",
                membership_payment_method:defaultOffline,
                membership_payed_amount:`${amount}`
            }
            setState(prev => ({ ...prev, offline_payment_loading: true }))
            memberOfflinePayment(memberPaymentParams).then(res=>{
                setState(prev => ({ ...prev, offline_payment_loading: false }))
                showThanYou()
                toggleStripeModal()
                callback()
            })

        } else {
            let params = {
                contribute_user_id: `${user_id}`,
                post_request_item_id: `${item_id ? item_id : current_selected_item_id}`,
                contribute_item_quantity: `${amount}`,
                payment_note: note,
                payment_type: defaultOffline,
                contributionId: contributionId,
                is_anonymous:`${is_anonymous}`,
                group_id:groupId
            }
            setState(prev => ({ ...prev, offline_payment_loading: true }))
            createContribute(params).then(res => {
                showThanYou()
                toggleStripeModal()
                togglePage("amount_page", '0', '')
                setState(prev => ({
                    ...prev,
                    offline_payment_loading: false,
                    thankyou: true,
                    defaultPayment: 'online',
                    defaultOffline: 'cash',
                    note: '',
                }))
                current_item ? callback(current_item) : callback()
            })
        }

    }
    const changePaymentMode = (event, tag) => {
        let valueSelected = event.target.value
        setState(prev => ({ ...prev, [`${tag}`]: valueSelected }))
    }
    useEffect(() => {
        if (contrPayLaterObj) {
            let { client_secret, contribute_item_quantity, item_id:it_id, contributionId:cId, current_item:c_item } = contrPayLaterObj
            setState(prev => ({
                ...prev,
                current: 'stripe',
                amount: contribute_item_quantity,
                client_id: client_secret,
                item_id: it_id,
                contributionId: cId,
                current_item: c_item
            }))
        }
    }, [contrPayLaterObj])

    useEffect(() => {
        if (MembPayLaterObj) {
            let { membership_fees, contributionId:c_id, stripe_valid,client_secret } = MembPayLaterObj
            setState(prev => ({
                ...prev,
                current: 'stripe',
                amount: membership_fees,
                defaultPayment: stripe_valid ? 'online' : 'offline',
                disable_online_payment: stripe_valid ? false : true,
                incoming_payment_type: 'Member',
                contributionId: c_id,
                client_id: client_secret,
            }))
        }
    }, [MembPayLaterObj])

    const changeAnonmStatus=(value)=>{
        setState(prev=>({...prev,is_anonymous:value}))
    }
    return (
        <React.Fragment>
            <Modal
                title=""
                visible={modalVisible}
                className="f-modal f-modal-wizard"
                style={{ height: '400px' }}
                onCancel={() => togglePaymentModal(false)}
            >
                <div style={{ marginTop: '20px' }}>{current === "amount_page" ?
                    <FirstPage
                        togglePage={togglePage}
                        groupId={groupId}
                        post_request_id={post_request_id}
                        current_selected_item_id={current_selected_item_id}
                        handleChange={handleChange}
                        showThanYou={showThanYou}
                        toggleStripeModal={toggleStripeModal}
                        changeAnonmStatus={changeAnonmStatus}
                    /> :
                    <React.Fragment>
                        <Radio.Group
                            value={defaultPayment}
                            style={{ marginLeft: '55px' }}
                            onChange={(e) => changePaymentMode(e, 'defaultPayment')}
                        >
                            <Radio disabled={disable_online_payment} value={'online'}>Credit/Debit Card</Radio>
                            <Radio value={'offline'}>Pay Offline</Radio>
                        </Radio.Group>
                        {onlineOfflineTab()}
                    </React.Fragment>
                }</div>
            </Modal>
        </React.Fragment>

    )
}

export default StripeComp