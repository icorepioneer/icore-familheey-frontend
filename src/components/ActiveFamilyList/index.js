import React, { useState, useEffect, useRef } from 'react';
import { Button, Avatar,Spin } from 'antd';
import { encryption } from '../../shared/encryptDecrypt';
import { viewFamily } from '../../modules/family/familyList/api-familyList'
import { useSelector,useDispatch } from 'react-redux'

const ActiveFamilyList = ({ history }) => {
    let {active_fam_loaded,active_fam} = useSelector(red_state=>red_state.side_bar_data)
    const [activeFamilyList, setactiveFamilyList] = useState({
        user_id: useSelector(state => state.user_details.userId),
        familyList: active_fam?active_fam:[],
        count: 0,
        loader: active_fam?false:true
    })
    const dispatch = useDispatch()
    const { familyList, user_id, count, loader } = activeFamilyList;
    const navigateToFamily = (value) => {
        var id = value;
        var familyId = encryption(id)
        history.push(`/familyview/${familyId}`);
    }
    const navigateToFamilyList = () => {
        history.push(`/my-family`);
    }

    const showFamilyList = () => {
        var params = {
            user_id: user_id,
            offset: count + 3,
            limit: 3
        }
        viewFamily(params).then((response) => {
            if (response && response.data && response.data.data) {
                var concatArray = familyList.concat(response.data.data)
                dispatch({type:'FAMILY_ADD',payload:{data:concatArray}})
                setactiveFamilyList((prev) => ({ ...prev, familyList: concatArray, count: count + 3,loader:false }))
            }
        }).catch((error) => {
            console.log('error--->', error);
        })
    }
    const contentLoader = new useRef(showFamilyList)
    const observer = useRef(new IntersectionObserver(inputs => {
        let latest = inputs[0]
        latest.isIntersecting &&!active_fam_loaded&&contentLoader.current()
    }, { threshold: 1 }))
    const [element, setElement] = useState(undefined)
    useEffect(() => {
        let current_element = element;
        let current_observer = observer.current;
        current_element && current_observer.observe(current_element)
        return () => {
            current_element && current_observer.unobserve(current_element)
        }
    }, [element])

    useEffect(() => {
        contentLoader.current = showFamilyList;
    }, [showFamilyList])
    return (
        <div className="f-sider-block">
            <div className="f-sider-head">
                <img src={require('../../assets/images/icon/land3.svg')} />
                <h4>Active Families</h4>
            </div>
            {!loader&&<React.Fragment>
                <div className="f-sider-body">
                    <ul>
                        {familyList && familyList.length > 0 &&
                            familyList.map((item, index) => {
                                return <li key={index} onClick={() => navigateToFamily(item.id)}>
                                    <Avatar src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../assets/images/family_logo.png')} />
                                    <span>
                                        <h5>{item.group_name}</h5>
                                        <h6>{item.group_category}</h6>
                                    </span>
                                </li>
                            })
                        }
                    </ul>
                    {familyList && familyList.length == 0 && <span>No family found</span>}
                </div>
                <div className="f-sider-foot">
                    {familyList && familyList.length > 0 && <Button type="link" onClick={() => { navigateToFamilyList() }}>more</Button>}
                </div>
            </React.Fragment>}
            {loader && <div style={{ textAlign: 'center', marginTop: '100px', marginBottom: '100px' }} >
                <div ref={setElement}>
                    <Spin></Spin>
                </div>
            </div>}
        </div>
    )
}
export default ActiveFamilyList;