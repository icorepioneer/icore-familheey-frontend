import React from 'react'
import { Table } from 'antd'
// import { deleteSelectedEntry } from '../../modules/RequestDetails/req_service'

const TableInvited = ({ invited_members }) => {
    const columns = [
        {
            title: 'Full name',
            dataIndex: 'full_name',
            key: 'full_name',
        },
        {
            title: 'Phone number',
            dataIndex: 'phone_no',
            key: 'phone_no',
        }
    ];
    console.log(invited_members)
    return (
        <React.Fragment>
            <Table
                columns={columns}
                dataSource={invited_members&&invited_members.map((item, i) => {
                    let { phone,full_name } = item
                    return {
                        key: i,
                        full_name:full_name,
                        phone_no:phone
                    }
                })}
                pagination={false}
            />
        </React.Fragment>
    )
}

export default TableInvited
