import React from 'react'
import { Modal, Button } from 'antd'
const ThankYOU = ({ thankyou, ty_handleCancel, ty_handleOk, callNow, member }) => {
    return (
        <Modal
            visible={thankyou}
            title=""
            onOk={() => ty_handleOk()}
            onCancel={() => ty_handleCancel()}
            footer={['']}
            className="f-request-modal"
        >
            <div className="f-clearfix"><img src={require('../../assets/images/icon/ok_tik.png')} /></div>
            {member ?
                <React.Fragment>
                    <h1>Payment Completed</h1> 
                </React.Fragment>
                : <React.Fragment>
                    <h1></h1>
                    <h4>Thank you for your contribution</h4>
                    <h5>You can initiate a call to the requester<br />in order to fulfil the request</h5>
                </React.Fragment>}
            <div className="f-mdl-submit">
                <Button className="f-btn-green" onClick={() => callNow()}>OKAY</Button> &nbsp; &nbsp;
            </div>
        </Modal>
    )
}

export default ThankYOU