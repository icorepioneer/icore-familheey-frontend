import React, { useState, useEffect, useRef } from 'react';
import { Button, Avatar, Spin } from 'antd';
import { announcement_list } from './../../modules/Post/post_service';
import { useSelector,useDispatch } from 'react-redux'
const AnnouncementList = ({ history }) => {
    const dispatch = useDispatch()
    let {announcement_data_loaded,announcement} = useSelector(red_state=>red_state.side_bar_data)
    const [announceList, setannounceList] = useState({
        user_id: useSelector(state => state.user_details.userId),
        announcement_list_data: announcement?announcement:[],
        loader: announcement?false:true
    })
    const { announcement_list_data, user_id, loader } = announceList;
    const showAnnouncementList = () => {
        announcement_list({ user_id: user_id, type: 'announcement', offset: 0, limit: 3 })
            .then(res => {
                dispatch({type:'ANNOUNCEMENT_ADD',payload:{data:res.data.read_announcement.concat(res.data.unread_announcement)}})
                setannounceList((prev) => ({ ...prev, loader: false, announcement_list_data: res.data.read_announcement.concat(res.data.unread_announcement) }));
            })
    }
    const launchPage = (path) => {
        history.push(path);
    }
    const loadAnnouncementPage = (data) => {
        let { post_id} = data
        history.push('/individualStatus', { fromStory: { post_id: post_id, prev_url: `${window.location.pathname}` } });
    }

    const contentLoader = new useRef(showAnnouncementList)
    const observer = useRef(new IntersectionObserver(inputs => {
        let latest = inputs[0]
        latest.isIntersecting &&!announcement_data_loaded&&contentLoader.current()
    }, { threshold: 1 }))
    const [element, setElement] = useState(undefined)
    useEffect(() => {
        let current_element = element;
        let current_observer = observer.current;
        current_element && current_observer.observe(current_element)
        return () => {
            current_element && current_observer.unobserve(current_element)
        }
    }, [element])

    useEffect(() => {
        contentLoader.current = showAnnouncementList;
    }, [showAnnouncementList])

    return (
        <div className="f-sider-block">
            <div className="f-sider-head">
                <img src={require('../../assets/images/icon/land2.svg')} />
                <h4 onClick={() => launchPage('/announcement')}>Latest Announcements</h4>
            </div>
            {!loader && <React.Fragment>
                <div className="f-sider-body">
                    <ul>
                        {
                            announcement_list_data && announcement_list_data.map((ann, i) => {
                                return (<li key={i} onClick={() => loadAnnouncementPage(ann)}>
                                    <Avatar 
                                    src={ ann.propic?process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + ann.propic:require('../../assets/images/default_propic.png') } />
                                    <span>
                                        <h5>{ann.group_name}</h5>
                                        <h6 className="f-text-ellipse">{ann.snap_description}</h6>
                                    </span>
                                </li>);
                            })
                        }
                        {
                            announcement_list_data && announcement_list_data.length == 0 && <span>No Announcement Available</span>
                        }
                    </ul>
                </div>
                <div className="f-sider-foot">
                    {announcement_list_data && announcement_list_data.length != 0 &&
                        <Button type="link" onClick={() => launchPage('/announcement')} >more</Button>}
                </div>
            </React.Fragment>}
            {loader && <div style={{ textAlign: 'center', marginTop: '100px', marginBottom: '100px' }} >
                <div ref={setElement}>
                    <Spin></Spin>
                </div>
            </div>}
        </div>
    )
}
export default AnnouncementList;