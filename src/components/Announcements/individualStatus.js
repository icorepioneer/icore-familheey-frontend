import React, { useState, useEffect } from 'react'
import { Icon, Spin, Carousel } from 'antd'
import * as moment from 'moment'
import ReactPlayer from 'react-player';
import { getPostByUsers } from '../../modules/RequestDetails/req_service'
import ChatSection from '../../components/ChatSection'
import { getCommentsByPost, updateComment, deleteConversation } from '../../modules/Post/post_service'
import './style.scss'
import { useSelector } from 'react-redux'
import io from 'socket.io-client';


const IndividualStatus = ({ history, location }) => {
    const [state, setState] = useState({
        announcement: undefined,
        width: 0,
        snap_description: '',
        family_logo: undefined,
        group_name: '',
        created_user_name: '',
        createdAt: null,
        post_attachment: null,
        conversation_enabled: false,
        conversation_count: 0,
        chatBoxVisible: false,
        announcement_id: undefined,
        user_id: useSelector(stateX => stateX.user_details.userId),
        comments: [],
        tag: '',
        newMessage: undefined,
        remove_chat_id: undefined,
        post_id: undefined,
        to_group_id: undefined,
        loader: true,
        prev_url:undefined
    })
    let { announcement,
        snap_description,
        family_logo,
        group_name,
        created_user_name,
        createdAt,
        post_attachment,
        conversation_enabled,
        conversation_count,
        chatBoxVisible,
        announcement_id,
        user_id,
        comments,
        tag, newMessage, remove_chat_id, to_group_id, post_id, loader,prev_url
    } = state

    let socket = io(process.env.REACT_APP_SOCKET_URL)

    useEffect(() => {
        let { state:l1 } = location;
        let { fromStory } = l1;
        let { post_id:pid,prev_url:purl } = fromStory;
        setState(prev => ({ ...prev, announcement_id: pid, tag: `post_channel_${pid}`,prev_url:purl }))
    }, [location])
    useEffect(() => {
        if (announcement_id) {
            getPostDetails();
            fetchComment();
        }
    }, [announcement_id])
    useEffect(() => {
        socket.on(tag, (data) => {
            modChat(data)
        })
    }, [tag])

    const modChat = (data) => {
        let { type, delete_id } = data[0]
        switch (type) {
            case "delete_comment":
                setState(prev => ({ ...prev, remove_chat_id: delete_id[0] }))
                break;
            //-- this case is dummy used to fix sonarqube  --// 
            //-- if new cases come delete this and add new --//
            case 'condition1': 
                console.log("condition1");
                break;
            case 'condition2': 
                console.log("condition2");
                break;
            //--- dummy end ---//
            default:

                setState(prev => ({ ...prev, newMessage: data }))
                break;
        }
    }
    useEffect(() => {
        if (remove_chat_id) {
            let current = comments
            current = current.filter(data => data.comment_id != remove_chat_id)
            setState(prev => ({ ...prev, comments: current }))
            return (() => {
                setState(prev => ({ ...prev, remove_chat_id: undefined }))
            })
        }
    }, [remove_chat_id])

    useEffect(() => {
        if (newMessage) {
            let current = [...comments, newMessage[0]]
            // current.push(newMessage[0])
            setState(prev => ({ ...prev, comments: current }))
            return (() => {
                setState(prev => ({ ...prev, newMessage: undefined }))
            })
        }
    }, [newMessage])


    const getPostDetails = () => {
        let params = {
            user_id: user_id, post_id: announcement_id, type: 'announcement', offset: 0, limit: 1000
        }
        getPostByUsers(params).then(res => {
            let { data } = res.data
            setState(prev => ({ ...prev, announcement: data[0], loader: false }))
        })
    }
    useEffect(() => {
        if (announcement) {
            let { snap_description:sd, family_logo:fl, group_name:gn, created_user_name:c_uname, createdAt:cr_at, post_attachment:p_att,
                conversation_enabled:c_enabled, conversation_count:c_count, post_id:p_id, to_group_id:to_gid
            } = announcement
            setState(prev => ({
                ...prev,
                snap_description: sd,
                family_logo: fl,
                group_name: gn,
                created_user_name: c_uname,
                createdAt: cr_at,
                post_attachment: p_att,
                conversation_enabled: c_enabled,
                conversation_count: c_count,
                post_id: p_id,
                to_group_id: to_gid
            }))
        }
    }, [announcement])

    const displayyAttatchment = (file, key) => {
        // console.log(file)
        // console.log(process.env.REACT_APP_S3_BASE_URL + 'post/' + file.filename)
        let { type } = file
        if (type.includes('image')) {
            return (
                <div style={{ width: '400px' }} key={key}>
                    <img
                        style={{
                            width: '400px',
                            maxHeight: '600px',
                            margin: '0px auto',
                            objectFit: 'scale-down',
                        }}
                        width="400" maxHeight="500"
                        src={process.env.REACT_APP_S3_BASE_URL + 'post/' + file.filename}
                    />
                </div>
            )
        }
        else if (type.includes('video')) {
            return (
                <div key={key}>
                    <ReactPlayer
                        controls
                        url={process.env.REACT_APP_S3_BASE_URL + 'post/' + file.filename} />
                </div>
            )
        }
        else if (type.includes('application')) {
            return (
                <div
                    key={key}
                >
                    <a href={process.env.REACT_APP_S3_BASE_URL + 'post/' + file.filename}>
                        <div
                            style={{
                                width: '300px',
                                height: '100px',
                                // backgroundColor:'green',
                                margin: '0px auto',
                                borderRadius: '5px',
                                border: '1px solid white',
                                padding: '20px'
                            }}
                        >
                            <Icon type="file" style={{ fontSize: '60px', color: 'white' }} />
                            <span
                                style={{}}>
                                {file.filename}
                            </span>
                        </div>
                    </a>
                </div>
            )
        }


    }
    const chatBoxToggle = () => {
        setState(prev => ({
            ...prev,
            chatBoxVisible: !chatBoxVisible
        }))
    }

    const fetchComment = () => {
        let params = {
            post_id: announcement_id,
            user_id: `${user_id}`
        }
        getCommentsByPost(params).then(res => {
            let { data } = res.data
            setState(prev => ({ ...prev, comments: data }))
        })
    }
    const displayContent = () => {
        switch (loader) {
            case true:
                return (
                    <div
                        style={{
                            width: '100%',
                            height: '400px',
                            margin: '0px auto',
                            textAlign: 'center',
                            marginTop: '45%'
                        }}
                    >
                        <Spin />
                    </div>)
            case false:
                return (
                    <div
                        style={{
                            backgroundColor: '#555555',
                            width: '720px',
                            float: 'right',
                            padding: '25px',
                            borderRadius: '5px'
                        }}
                    >
                        {/* heading with details */}
                        <div
                            style={{
                                // backgroundColor: 'red',
                                width: '100%',
                                height: '100px',
                                display: 'flex',
                                justifyContent: 'space-between'
                            }}
                        >
                            <div
                                style={{
                                    flexGrow: '.7',
                                    // backgroundColor: 'yellow',
                                    padding: '10px'
                                }}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                    }}
                                >
                                    <div
                                        style={{
                                            width: '15%',
                                            height: '80%',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            overflow: 'hidden',
                                            borderRadius: '5px'
                                        }}
                                    >
                                        <img
                                            onError={(e) => { e.target.src = require('../../assets/images/family_logo.png') }}
                                            src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + family_logo}
                                        />
                                    </div>
                                    <div
                                        style={{
                                            padding: '10px',
                                            color: 'white',
                                            height: '100%',
                                        }}
                                    >
                                        <h4
                                            style={{
                                                color: '#ffffff',
                                                fontSize: '.9rem',
                                                margin: '0 0 6px 0',
                                                lineHeight: '1',
                                                fontWeight: '600'
                                            }}
                                        >{group_name}</h4>
                                        <h5
                                            style={{
                                                fontWeight: '300',
                                                margin: '0 0 5px',
                                                color: '#e3e3e3',
                                                fontSize: ' 0.8rem'
                                            }}
                                        >Posted by <strong>{created_user_name}</strong></h5>
                                        <h6
                                            style={{
                                                color: '#9e9e9e',
                                                fontWeight: '400',
                                                fontSize: '0.75rem',
                                                lineHeight: '1',
                                                margin: '0'
                                            }}
                                        >{moment(createdAt).format('MMM DD YYYY  hh:mm A')}</h6>
                                    </div>
                                </div>
                            </div>
                            <div
                                onClick={() => { history.push(`${prev_url}`) }}
                                style={{
                                    padding: '20px'
                                }}
                            >
                                <Icon type="close" style={{ fontSize: '30px', color: 'white' }} />
                            </div>
                        </div>
                        {/* content */}
                        <div
                            style={{
                                padding: '10px'
                            }}
                        >
                            <Carousel style={{ paddingBottom: '50px' }} dots={true} >
                                {post_attachment && post_attachment.map((data, i) => {
                                    return displayyAttatchment(data, i)
                                })}
                            </Carousel>
                        </div>
                        {/* footer description */}
                        <div><span
                            style={{
                                padding: '10px',
                                color: '#9e9e9e',
                                fontWeight: '400',
                                fontSize: '1rem',
                                lineHeight: '1',
                                margin: '0'
                            }}
                        >{snap_description}</span></div>
                        {/* main footer */}
                        {conversation_enabled &&
                            <div
                                onClick={chatBoxToggle}
                                style={{ display: 'flex', justifyContent: 'flex-start', padding: '10px', marginTop: '10px', width: '200px' }}>
                                <Icon
                                    type="message"
                                    style={{ fontSize: '30px', color: 'white' }}
                                />
                                <span
                                    style={{
                                        color: 'rgb(255, 255, 255)',
                                        fontSize: ' 0.9rem',
                                        margin: '0px 0px 6px',
                                        lineHeight: '1',
                                        fontWeight: '600',
                                        padding: '10px'
                                    }}
                                >{`${conversation_count} Conversations`}</span>
                            </div>
                        }
                    </div>
                )
        }
    }
    return (
        <React.Fragment>
            <div
                style={{
                    padding: '66px 0 75px',
                    // transition: 'all ease-in-out 0.2s',
                    // overflowWrap: 'break-word',
                    // wordBreak: 'break-word',
                    display: 'flex',
                    flexDirection: 'column'
                }}
            >
                <div
                    style={{
                        display: 'flex',
                        margin: '0px auto',
                        width: '720px',
                        minHeight: '200px'
                    }}>
                    {displayContent()}

                </div>
                {chatBoxVisible ? <div
                    style={{
                        margin: '0px auto',
                        width: '720px',
                        minHeight: '200px',
                        marginTop: '20px'
                    }}
                    className="f-story-communications f-have-drop-menu">
                    <h4>Conversations</h4>
                    <div className="f-story-communication">
                        <ChatSection
                            one_Accepted={true}
                            is_accept={true}
                            data={comments}
                            userId={user_id}
                            comment={updateComment}
                            topicId={post_id}
                            trigger_={false}
                            deleteConversation={deleteConversation}
                            history={history}
                            created_user={""}
                            acceptOrRejectUserTopic={() => { }}
                            topicOwner={false}
                            chatloader={false}
                            post_id={post_id}
                            to_group_id={to_group_id}
                        />
                    </div>

                </div> : null}

            </div>
        </React.Fragment >
    )
}
export default IndividualStatus;