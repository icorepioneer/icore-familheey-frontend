import React from 'react'
import { Button, Table,Modal} from 'antd'

const TableMembershipData = ({ list_data,deleteSelected }) => {
    const warning = (id,contribute_user_id)=>{
        Modal.confirm({
            title:"Do you want to delete this contribution",
            content:"",
            onOk:()=>{
                deleteSelected(id,contribute_user_id)
            },
            // cancelText:'Cancel'
        })
    }
    const columns = [
        {
            title: 'Full name',
            dataIndex: 'full_name',
            key: 'full_name',
        },
        {
            title: 'Phone number',
            dataIndex: 'phone_no',
            key: 'phone_no',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Payment mode',
            dataIndex: 'payment_type',
            key: 'payment_type',
        },
        {
            title: 'Note',
            dataIndex: 'payment_note',
            key: 'payment_note',
        },
        {
            title: 'Amount',
            dataIndex: 'contribute_item_quantity',
            key: 'contribute_item_quantity',
        },
        {
            title: 'Payment status',
            dataIndex: 'payment_status',
            key: 'payment_status',
        },
        {
            dataIndex: 'edit',
            key: 'edit',
            className: 'f-no-lrp',
            render: (row,data) =>{
                let {payment_status,id,contribute_user_id} = data 
                return (
                <Button disabled={payment_status==='Processing'||payment_status==='pending'} onClick={()=>warning(id,contribute_user_id)} type="link" icon="delete" />
            )
            } 
        },
    ];

    return (
        <React.Fragment>
            <Table
                columns={columns}
                dataSource={list_data.map((item, i) => {
                    let { id,is_anonymous,contribute_user_id,full_name, phone_no, email, payment_type, payment_note, contribute_item_quantity, payment_status, paid_user_name } = item
                    return {
                        key: i,
                        id:id,
                        contribute_user_id:contribute_user_id,
                        full_name: paid_user_name ? (is_anonymous?`${paid_user_name} (Anonymous)`:paid_user_name) : (is_anonymous?`${full_name} (Anonymous)`:full_name),
                        phone_no: phone_no,
                        email: email,
                        payment_type: payment_type,
                        payment_note: payment_note?payment_note:'',
                        contribute_item_quantity: contribute_item_quantity,
                        payment_status: payment_status,
                    }
                })}
                pagination={false}
            />
        </React.Fragment>
    )
}

export default TableMembershipData
