import React, { useState, useEffect } from 'react'
import { Button, Switch, Input, Form, Select, Radio, Spin } from 'antd';
import TableMembershipData from './table'
import io from 'socket.io-client';
import countries from '../../common/countrylist.json'
import './style.scss'
import { getClientSecret } from '../../components/Stripe/stripe-api'
import CheckoutForm from './checkoutForm'

const { Option } = Select
const { TextArea } = Input

let socket = io(process.env.REACT_APP_SOCKET_URL)
const DropDownOverLay = ({ drop_List, onUserSelected }) => {
    return (
        <React.Fragment>
            <div className="f-offline-drop-i">{
                drop_List.length > 0 ?
                    <div className="f-offline-drop-j">
                        {drop_List.map((item, key) =>
                            <div
                                className="f-offline-drop-k"
                                key={key}
                                onClick={() => onUserSelected(item)}
                            >{`${item.full_name ? item.full_name : item.phone_no}`}</div>
                        )}
                    </div> : <div></div>
            }
            </div>
        </React.Fragment>
    )
}


const CreateForm = Form.create({ name: 'memb_form_offline' })(
    class extends React.Component {
        render() {
            let { form, payment_field_disabled, display_overlay, drop_List, onUserSelected,
                checkAmountField, onPaySubmit, default_payment_mode, onInputActive, is_anonymos,
                onPayAsAnonymousClicked, non_anonymousFields_disabled, onPaymentModeChange, online_mode_selected,
                payment_intent_created, amount_to, client_secret, afterStripePayment,
                default_contry_drop
            } = this.props
            const { getFieldDecorator } = form
            const validateNumber = (rule, value, callback) => {
                if (parseFloat(value) < 0) {
                    return callback('Please provide a value which is greater than zero')
                }
                if(!value){
                    return callback('Please provide a value')
                }
                    callback()
                // if (!Number.isInteger(parseFloat(value))) {
                //     return callback('Please provide a whole number value which is not zero')
                // }
                // callback()
            }
            const validatePhoneNumber = (rule, value, callback) => {
                if(/[!@#$%^&*(),.?":{}|<>+/_-]/.test(value)){
                    return callback("no symbols and characters are allowed")
                }
                if (!Number.isInteger(parseFloat(value))) {
                    return callback('Please provide a cell number')
                }
                callback()
            }
            return (
                <div className="f-offline-req-form-in">
                    <div className="f-create-form-body" >
                        <Form >
                            <div className="f-form-control">
                                <div style={{ width: '20%', display: 'flex', justifyContent: 'flex-start' }}>
                                    <span style={{ marginTop: '10px', marginRight: '10px' }}>Pay as anonymous : </span>
                                    <Form.Item>{
                                        getFieldDecorator('is_anonymous')(
                                            <Switch
                                                onChange={e => onPayAsAnonymousClicked(e)}
                                                checked={is_anonymos}
                                            />
                                        )
                                    }</Form.Item>
                                </div>
                                <div className="f-split">
                                    <div className="f-alt">
                                        <Form.Item>
                                            {getFieldDecorator('user',
                                                { rules: [{ required: true, message: 'Please add a name before submitting' }] }
                                            )(
                                                <Input
                                                    disabled={non_anonymousFields_disabled}
                                                    placeholder="Choose a person"
                                                    allowClear
                                                    onChange={onInputActive}
                                                    autoComplete="off"
                                                />
                                            )}
                                        </Form.Item>
                                        <div className="f-offline-drop">
                                            {display_overlay && <DropDownOverLay
                                                drop_List={drop_List}
                                                onUserSelected={onUserSelected}
                                            />}
                                        </div>
                                    </div>
                                    <div className="f-phone-combined">
                                        <Form.Item>
                                            {
                                                getFieldDecorator('code', {
                                                    initialValue: default_contry_drop ? default_contry_drop : 'United States-+1',
                                                    rules: [{ required: is_anonymos ? false : true, message: 'country code missing' }]
                                                })(
                                                    <Select
                                                        disabled={non_anonymousFields_disabled}
                                                        showSearch
                                                    >
                                                        {countries.countries.map((item, id) => {
                                                            return (
                                                                <Option
                                                                    key={id}
                                                                    value={item.name + '-' + item.code}
                                                                >
                                                                    {item.name}
                                                                </Option>
                                                            )
                                                        })}
                                                    </Select>
                                                )
                                            }
                                        </Form.Item>
                                        <Form.Item>
                                            {getFieldDecorator('user_phone',
                                                {
                                                    rules: [{
                                                        required: is_anonymos ? false : true,
                                                        // message: ' Cell number required',
                                                        validator: is_anonymos ? null : validatePhoneNumber
                                                    }]
                                                }
                                            )(<Input
                                                disabled={non_anonymousFields_disabled}
                                                autoComplete="off"
                                                placeholder="Cell number" />)}
                                        </Form.Item>
                                    </div>
                                </div>
                            </div>

                            <div className="f-form-control">
                                <div className="f-split">
                                    <div>
                                        <Form.Item>
                                            {getFieldDecorator('amount', {
                                                rules: [{
                                                    required: true,
                                                    // message: 'Please input the amount',
                                                    validator: validateNumber
                                                }]
                                            })(<Input
                                                type="number"
                                                placeholder="Amount"
                                                autoComplete="off"
                                                onChange={(e) => { checkAmountField(e) }}
                                            />)}
                                        </Form.Item>
                                    </div>
                                    <div>
                                        <Form.Item>
                                            {getFieldDecorator('payment_mode', {
                                                initialValue: 'offline',
                                                rules: [{
                                                    required: true,
                                                    message: 'Please select a payment mode',
                                                }]
                                            })(<Select
                                                disabled={payment_field_disabled}
                                                onChange={(e) => onPaymentModeChange(e)}
                                            >
                                                <Option value="offline">Offline</Option>
                                                <Option value="online">Online</Option>
                                            </Select>)}
                                        </Form.Item>
                                    </div>
                                    <div>
                                        {!online_mode_selected && <Form.Item>
                                            {getFieldDecorator('type', { initialValue: default_payment_mode })(
                                                <Radio.Group disabled={payment_field_disabled} >
                                                    <Radio value={'cash'}>Cash</Radio>
                                                    <Radio value={'cheque'}>Cheque</Radio>
                                                    <Radio value={'other'}>Other</Radio>
                                                </Radio.Group>
                                            )}
                                        </Form.Item>}
                                    </div>
                                </div>
                            </div>
                            {online_mode_selected ?
                                (payment_intent_created ?
                                    <div>
                                        <CheckoutForm
                                            amount={amount_to}
                                            client_id={client_secret}
                                            toggleStripeModal={() => { }}
                                            showThanYou={() => afterStripePayment()}
                                            togglePage={() => { }}
                                        />
                                    </div>
                                    :
                                    <div style={{ textAlign: 'center' }}><Spin size="large" /></div>)
                                :
                                <React.Fragment>
                                    <div className="f-form-control">
                                        <Form.Item className="f-txt-area">
                                            {getFieldDecorator('note')(<TextArea
                                                rows={5}
                                                disabled={payment_field_disabled}
                                                placeholder="Please specify cheque number, transaction id etc.."
                                            />)}
                                        </Form.Item>
                                    </div>
                                    <div className="f-form-control f-frm-submit">
                                        <Form.Item>
                                            <Button className="f-button" onClick={() => 
                                                // console.log("hahahah")
                                                onPaySubmit()
                                                } >Submit</Button>
                                        </Form.Item>
                                    </div>
                                </React.Fragment>}
                        </Form>
                    </div>
                </div>
            )
        }
    }
)

const OfflineMembership = ({ userList, user_id, group_id, item_id, contrByAdmin, getContrListByAdmin, request_id, user_phone, deleteSelectedEntry }) => {
    let formReff;
    const saveFormRef = formRef => {
        formReff = formRef;
    };
    const [state, setState] = useState({
        usersList: [],
        selectedUser: undefined,
        payment_field_disabled: true,
        default_payment_mode: 'cash',
        list_data: [],
        index_id: undefined,
        index_id_real: undefined,
        index_id_status: undefined,
        display_overlay: false,
        drop_List: [],
        is_anonymos: false,
        non_anonymousFields_disabled: false,
        online_mode_selected: false,
        payment_intent_created: undefined,
        client_secret: undefined,
        amount_to: undefined,
        default_contry_drop: 'United States-+1'
    })
    let { usersList, selectedUser, payment_field_disabled, default_payment_mode, list_data, index_id, index_id_status, display_overlay, drop_List,
        is_anonymos, non_anonymousFields_disabled, online_mode_selected, payment_intent_created, client_secret, amount_to, index_id_real, default_contry_drop } = state
    useEffect(() => {
        fetchList()
        fetchTableData()
    }, [])

    useEffect(() => {
        if (user_phone) {
            let country_code_details = countries.countries.find(item => user_phone.toLowerCase().indexOf(item.code) >= 0)
            let { code, name } = country_code_details
            setState(prev => ({ ...prev, default_contry_drop: `${name}-${code}` }))
        }
    }, [user_phone])
    const fetchList = () => {
        let params = {
            user_id: `${user_id}`,
            group_id: `${group_id}`,
        }
        userList(params).then(res => {
            let { data } = res
            let list = data
            setState(prev => ({ ...prev, usersList: list.data }))
        }).catch(err => {
            console.log(err)
        })
    }
    const fetchTableData = () => {
        let params = {
            group_id: `${group_id}`,
            admin_id: `${user_id}`,
            post_request_item_id: `${item_id}`
        }
        getContrListByAdmin(params).then(res => {
            let { data } = res
            setState(prev => ({ ...prev, list_data: data }))
        })
    }
    const onUserSelected = (item) => {
        let { phone, full_name } = item;
        let user_selected_phone = phone;
        let country_code_details = countries.countries.find(item1 => phone.toLowerCase().indexOf(item1.code) >= 0)
        let { code, name } = country_code_details
        if (code) {
            user_selected_phone = user_selected_phone.split(`${code}`)[1]
        }
        const { form } = formReff.props;
        form.setFields({
            user_phone: { value: user_selected_phone },
            user: { value: full_name },
            code: { value: name + '-' + code }
        })
        setState(prev => ({ ...prev, display_overlay: false, selectedUser: item }))
    }
    const checkAmountField = (e) => {
        e.persist()
        const { form } = formReff.props
        form.validateFields((err, values) => {
            if (err) {
                return
            }
            console.log(e.target.value)
            setState(prev => ({ ...prev, payment_field_disabled: !e.target.value > 0, amount_to: e.target.value }))
        })
        
    }
    const onPaySubmit = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                return
            }
            let { user, user_phone:u_ph, amount, type, note, code, is_anonymous } = values
            let country_code = code.split('-')[1]
            let params = {
                group_id: `${group_id}`,
                admin_id: `${user_id}`,
                contribute_user_id: selectedUser ? selectedUser.user_id : `2`,
                post_request_item_id: `${item_id}`,
                contribute_item_quantity: `${amount}`,
                phone_no: `${country_code + u_ph}`,
                payment_note: `${note ? note : ''}`,
                payment_type: `${type}`,
                intex: `${list_data.length + 1}`,
                full_name: user,
                paid_user_name: user,
                is_anonymous: is_anonymous
            }
            let new_list = [{ ...params, id: list_data.length + 1, payment_status: 'Processing' }, ...list_data]
            setState(prev => ({ ...prev, list_data: new_list, payment_field_disabled: true, selectedUser: undefined, is_anonymos: false }))
            form.resetFields()
            contrByAdmin(params).then(res => {
            }).catch(err_ad => {
                console.log(err_ad)
            })
        })

    }
    const alterListData = (data) => {
        let { intex, status, id } = data
        setState(prev => ({ ...prev, index_id: intex, index_id_status: status, index_id_real: id }))
    }
    useEffect(() => {
        if (index_id) {
            let item = list_data.find(item_ld => item_ld.intex == index_id)
            let item_list = list_data.filter(_item => (_item.intex && _item.intex != index_id || _item.id != index_id))
            item.payment_status = index_id_status
            item.id = index_id_real
            let new_list = [item, ...item_list]
            setState(prev => ({ ...prev, list_data: new_list }))
            return () => {
                setState(prev => ({ ...prev, index_id: undefined, index_id_status: undefined }))
            }
        }
    }, [index_id])
    useEffect(() => {
        let tag = 'admin_contribution_' + user_id + group_id + item_id
        socket.on(tag, (data) => {
            alterListData(data)
        })
    }, [])
    const onInputActive = (event) => {
        if (event.target.value.length === 0) {
            const { form } = formReff.props
            setState(prev => ({
                ...prev,
                payment_field_disabled: true,
                amount_to: undefined,
                payment_intent_created: undefined,
                client_secret: undefined,
                is_anonymos: false,
                selectedUser: undefined,
                online_mode_selected: false,
                display_overlay: false
            }))
            form.resetFields()
            setState(prev => ({ ...prev, selectedUser: undefined }))
        }
        else {
            let tag = event.target.value
            let copy_list = usersList
            copy_list = copy_list.filter(item => {
                return item.email && item.email.toLowerCase().indexOf(tag.toLowerCase()) >= 0 ||
                    item.phone_no && item.phone_no.toLowerCase().indexOf(tag.toLowerCase()) >= 0 ||
                    item.full_name && item.full_name.toLowerCase().indexOf(tag.toLowerCase()) >= 0
            })
            setState(prev => ({ ...prev, drop_List: copy_list, display_overlay: copy_list.length > 0 }))
        }

    }

    const onPayAsAnonymousClicked = (e) => {
        // const { form } = formReff.props;
        // form.setFields({
        //     user_phone: { value: null },
        //     user: { value: null },
        //     code: { value: 'United States-+1' }
        // })
        setState(prev => ({
            ...prev, is_anonymos: e,
            //  non_anonymousFields_disabled: e 
        }))
    }
    const onPaymentModeChange = (val) => {
        const { form } = formReff.props
        form.validateFields((err, values) => {
            if (err) {
                return
            }
            let flag = val === 'online'
            setState(prev => ({ ...prev, online_mode_selected: flag, payment_intent_created: flag ? false : undefined }))
        })

    }
    useEffect(() => {
        payment_intent_created === false && createPaymentIntent()
    }, [payment_intent_created])
    const createPaymentIntent = () => {
        const { form } = formReff.props
        form.validateFields((err, values) => {
            let { is_anonymous, amount, user, user_phone:uphone, code } = values
            let country_code = code.split('-')[1]
            let params = {
                amount: parseInt(amount) * 100,
                user_id: `${selectedUser ? selectedUser.user_id?selectedUser.user_id:2 : 2}`,
                group_id: `${group_id}`,
                to_type: "request",
                to_type_id: `${request_id}`,
                to_subtype: "item",
                to_subtype_id: `${item_id}`,
                is_anonymous: is_anonymous,
                admin_id: `${user_id}`,
                paid_user_name: user,
                phone_no: `${country_code + uphone}`,
            }
            getClientSecret(params).then(res => {
                let { data } = res
                let { client_secret:cl_s } = data
                setState(prev => ({ ...prev, client_secret: cl_s, payment_intent_created: true }))

            })
        })
    }
    const afterStripePayment = () => {
        const { form } = formReff.props
        form.validateFields((err, values) => {
            if (err) {
                return
            }
            let { user, user_phone:_uph, amount, type, note, code, is_anonymous } = values
            let country_code = code.split('-')[1]
            let params = {
                group_id: `${group_id}`,
                admin_id: `${user_id}`,
                contribute_user_id: selectedUser ? selectedUser.user_id : null,
                post_request_item_id: `${item_id}`,
                contribute_item_quantity: `${amount}`,
                phone_no: `${country_code + _uph}`,
                payment_note: `${note ? note : ''}`,
                payment_type: `${type ? type : 'Online'}`,
                intex: `${list_data.length + 1}`,
                full_name: user,
                paid_user_name: user,
                is_anonymous: is_anonymous,
            }
            let new_list = [{ ...params, id: list_data.length + 1, payment_status: 'Processing' }, ...list_data]
            setState(prev => ({
                ...prev,
                list_data: new_list,
                payment_field_disabled: true,
                amount_to: undefined,
                payment_intent_created: undefined,
                client_secret: undefined,
                is_anonymos: false,
                selectedUser: undefined,
                online_mode_selected: false
            }))
            form.resetFields()
        })
    }

    const deleteSelected = (id, contr_id) => {
        let item_list = list_data.filter(item => (item.id != id))
        setState(prev => ({ ...prev, list_data: item_list }))
        let params = {
            id: `${id}`,
            contribute_user_id: `${contr_id}`
        }
        deleteSelectedEntry(params).then(res => {
            console.log(res)
        })
    }
    return (
        <React.Fragment>
            <div className="f-page f-offlinemembership">
                <div className="f-clearfix f-body">
                    <div className="f-boxed">
                        <CreateForm
                            wrappedComponentRef={saveFormRef}
                            usersList={usersList}
                            onUserSelected={onUserSelected}
                            selectedUser={selectedUser}
                            payment_field_disabled={payment_field_disabled}
                            checkAmountField={checkAmountField}
                            onPaySubmit={onPaySubmit}
                            default_payment_mode={default_payment_mode}
                            onInputActive={onInputActive}
                            display_overlay={display_overlay}
                            drop_List={drop_List}
                            onUserSelected={onUserSelected}
                            is_anonymos={is_anonymos}
                            onPayAsAnonymousClicked={onPayAsAnonymousClicked}
                            non_anonymousFields_disabled={non_anonymousFields_disabled}
                            onPaymentModeChange={onPaymentModeChange}
                            online_mode_selected={online_mode_selected}
                            payment_intent_created={payment_intent_created}
                            amount_to={amount_to}
                            client_secret={client_secret}
                            afterStripePayment={afterStripePayment}
                            default_contry_drop={default_contry_drop}
                        />
                        <div className="f-offline-req-table">
                            <div className="f-offline-req-table-in">
                                <TableMembershipData
                                    deleteSelected={deleteSelected}
                                    list_data={list_data}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default OfflineMembership