import React, { useState } from 'react'
import { CardElement, useStripe, useElements, Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import { Button } from 'antd';
import styled from 'styled-components'
import CheckoutError from '../Stripe/checkoutError'
const CardElementContainer = styled.div`
  height: 40px;
  width:100%;
  display: flex;
  align-items: center;
  margin-bottom:'10px';
  & .StripeElement {
    width: 100%;
    padding: 15px;
    box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 white;
  }
`;

const Row = styled.div`
text-align:center;
`;


const ButtonRow = styled.div`
text-align:center;
width:100%;
`;
// const ButtonRow = styled.div`
//   width: 475px;
//   margin: 30px auto;
//   box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
//     inset 0 1px 0 white;
//   border-radius: 4px;
//   background-color: relative;
//   position: relative;
// `;
const stripePromise = loadStripe(`${process.env.REACT_APP_STRIPE_KEY}`)
const CheckoutForm = ({ amount, client_id, toggleStripeModal, showThanYou, togglePage }) => {
    const [isProcessing, setProcessingTo] = useState(false);
    const [checkoutError, setCheckoutError] = useState();
    const stripe = useStripe()
    const elements = useElements()

    const handleCardDetailsChange = ev => {
        ev.error ? setCheckoutError(ev.error.message) : setCheckoutError();
    };

    const handleFormSubmit = async ev => {
        ev.preventDefault();
        if (!stripe || !elements) {
            return
        }
        setProcessingTo(true);

        const cardElement = elements.getElement("card");
        try {
            await stripe.createPaymentMethod({
                type: "card",
                card: cardElement,
            }).then(async result => {
                let { error } = result
                if (error) {
                    setCheckoutError(error.message);
                    setProcessingTo(false);
                    return;
                }
                await stripe.confirmCardPayment(`${client_id}`, {
                    payment_method: { card: cardElement }
                }).then((result1) => {
                    let { error:e1 } = result1;
                    if (e1) {
                        setCheckoutError(e1.message)
                        setProcessingTo(false)
                        return
                    }
                    toggleStripeModal()
                    showThanYou()
                    setProcessingTo(false)
                    togglePage("amount_page", '0', '')
                })
            })
        } catch (err) {
            console.log(err)
            setCheckoutError(err.message);
        }
    };
    const cardElementOpts = {
        iconStyle: "solid",
        hidePostalCode: true,
    };

    return (
        <React.Fragment>
            <Row>
                <CardElementContainer>
                    <CardElement
                        options={cardElementOpts}
                        onChange={handleCardDetailsChange}
                    />
                </CardElementContainer>
            </Row>
            {checkoutError && <CheckoutError style={{ backgroundColor: 'red' }}>{checkoutError}</CheckoutError>}
            <ButtonRow>
                <Button
                    onClick={handleFormSubmit}
                    style={{
                        width: '30%',
                        height: '40px',
                        backgroundColor: 'var(--green)',
                        border: '1px solid var(--green)',
                        color: '#FFF',
                        marginTop: '15px',
                    }}
                    type="submit"
                    disabled={isProcessing || !stripe}
                >
                    {isProcessing ? "Processing..." : `Pay $${amount}`}
                </Button>
            </ButtonRow>
            <div style={{ textAlign: 'center' }}><h6>powered by Stripe</h6></div>

        </React.Fragment>
    )
}

const StripeComp = ({ amount, client_id, toggleStripeModal, showThanYou, togglePage }) => {
    return (
        <div>
            <Elements stripe={stripePromise}>
                <CheckoutForm
                    amount={amount}
                    client_id={client_id}
                    toggleStripeModal={toggleStripeModal}
                    showThanYou={showThanYou}
                    togglePage={togglePage}
                />
            </Elements>
        </div>
    )
}
export default StripeComp