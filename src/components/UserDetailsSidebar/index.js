import React, { useState, useEffect } from 'react';
import './style.scss'
import { Button, Modal, Select, Input, Menu, DatePicker, Switch } from 'antd';
import { updateUserDetails } from '../../modules/users/userProfile/api-profile'
import { useSelector } from 'react-redux';
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import ReactGoogleMapLoader from "react-google-maps-loader";
import { decryption } from '../../shared/encryptDecrypt'
const { Option } = Select;
// const { confirm } = Modal;
const MY_API_KEY = decryption(process.env.REACT_APP_PLACE_ENCRYPT)

const moment = require('moment');
const UserDetailsSidebar = ({ user_details, callbackFromParent, profileId, history, initial }) => {
    const [state, setState] = useState({
        user_id: useSelector(stateX => stateX.user_details.userId),
        editModal: false,
        proLoad: false,
        loading: false,
        cover_pic: '',
        pro_pic: '',
        userDetails: undefined,
        confirmLoading: false,
        searchable: undefined,
        search_location: "",
        search_origin: "",
        value_location: "",
        value_origin: ""
    })
    let { value_origin, value_location, search_location, search_origin, user_id, editModal, cover_pic, pro_pic, userDetails, confirmLoading, searchable } = state;

    useEffect(() => {
        setState(prev => ({ ...prev, userDetails: user_details, searchable: user_details.searchable, value_location: user_details.location, value_origin: user_details.origin }))
    }, [user_details])

    const radioOnChange = (e, key) => {
        callbackFromParent('change', key);
    }

    const handleOk = e => {
        let { full_name, gender } = userDetails;
        let errcount = 0;
        if (!full_name) errcount++;
        if (gender == '-1') errcount++;
        if (errcount > 0) return;
        setState(prev => ({ ...prev, confirmLoading: true }))
        const formData = new FormData();
        formData.append('id', userDetails.id.toString());
        if (cover_pic) {
            formData.append('cover_pic', cover_pic);
        }
        if (pro_pic) {
            formData.append('propic', pro_pic);
        }
        formData.append('about', userDetails.about);
        formData.append('full_name', userDetails.full_name);
        formData.append('gender', userDetails.gender);
        formData.append('location', userDetails.location);
        formData.append('origin', userDetails.origin);
        formData.append('searchable', userDetails.searchable);
        formData.append('dob', userDetails.dob)
        setState(prev => ({ ...prev, userDetails: userDetails }));
        updateUserDetails(formData).then((response) => {
            setTimeout(() => {
                setState(prev => ({ ...prev, editModal: false, confirmLoading: false }));
                callbackFromParent('update', response.data);
            }, 2000)
        }).catch((error) => {
            console.log('error', error)
        })
    };

    const handleCancel = (e) => {
        setState(prev => ({ ...prev, editModal: false }));
    };

    const handleChange = (e, name) => {
        let _userDetails = userDetails
        if (name == 'dob') {
            _userDetails[name] = moment(e).format('YYYY-MM-DD');
            setState(prev => ({ ...prev, userDetails: _userDetails }));
        } else {
            _userDetails[name] = e.target.value;
            setState(prev => ({ ...prev, userDetails: _userDetails }));
        }
    }

    const handleSelect = (name, e) => {
        let _userDetails = userDetails
        _userDetails[name] = e
        setState(prev => ({ ...prev, userDetails: _userDetails }))
    }

    const handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
        userDetails = { ...userDetails, location: geocodedPrediction.formatted_address }
        setState((prev) => ({
            ...prev,
            search_location: "",
            value_location: geocodedPrediction.formatted_address,
            userDetails: userDetails,
            location: geocodedPrediction.formatted_address
        }));
    }

    const handleSelectSuggestOrigin = (geocodedPrediction, originalPrediction) => {
        userDetails = { ...userDetails, origin: geocodedPrediction.formatted_address }
        setState((prev) => ({
            ...prev,
            search_origin: "",
            value_origin: geocodedPrediction.formatted_address,
            userDetails: userDetails,
            location: geocodedPrediction.formatted_address
        }));
    }

    const handleInputChange = (e, _type) => {
        console.log("== type == ", _type)
        e.persist();
        if (_type == 'location') {
            setState((prev) => ({
                ...prev,
                search_location: e.target.value,
                value_location: e.target.value,
                location: e.target.value
            }));
        } else {
            setState((prev) => ({
                ...prev,
                search_origin: e.target.value,
                value_origin: e.target.value,
                origin: e.target.value
            }));
        }
    }

    // const deleteConfirmation = () => {
    //     var message = `Are you sure you want to delete this Account`
    //     confirm({
    //         title: message,
    //         okText: 'Yes',
    //         cancelText: 'No',
    //         onOk() {
    //             let params = {
    //                 id: profileId,
    //                 is_active: false,
    //             }
    //             updateUserDetails(params).then((res) => {
    //                 history.push(`events/`)
    //             })
    //         },
    //         onCancel() { },
    //     });
    // }

    const onChangePrivacySettings = (type, e) => {
        let _userDetails = userDetails
        _userDetails[type] = e;
        setState(prev => ({ ...prev, userDetails: _userDetails, [type]: e }));
    }

    return (
        <aside className="userDetailsSidebar f-inside-sticky">
            {/* Sidebar Block */}
            {userDetails && <div className="f-sider-block f-sider-menu-incl f-drop-mob-compat">
                <Menu mode="inline" className="f-sider-menu" selectedKeys={[`${initial}`]}>
                    <Menu.Item key="1" onClick={(e) => radioOnChange(e, 1)}>
                        <img
                            src={require('../../assets/images/icon/land1.svg')}
                        />
                        <span>About {user_id == profileId ? 'Me' : userDetails.full_name}</span>
                    </Menu.Item>
                    {(user_id === profileId) &&
                        <Menu.Item key="2" onClick={(e) => radioOnChange(e, 2)}>
                            <img src={require('../../assets/images/icon/land8.svg')} />
                            <span> Posts</span>
                        </Menu.Item>
                    }
                    <Menu.Item key="4" onClick={(e) => radioOnChange(e, 4)}>
                        <img src={require('../../assets/images/icon/land10.svg')} />
                        <span>Album</span>
                    </Menu.Item>
                    {(user_id !== profileId) &&
                        <Menu.Item key="7" onClick={(e) => radioOnChange(e, 7)}>
                            <img src={require('../../assets/images/icon/link.svg')} />
                            <span>Mutual Connections</span>
                        </Menu.Item>
                    }
                    <Menu.Item key="6" onClick={(e) => radioOnChange(e, 6)}>
                        <img src={require('../../assets/images/icon/land7.svg')} />
                        <span>Families</span>
                    </Menu.Item>

                    {(user_id == profileId) && <Menu.Item key="11" onClick={(e) => radioOnChange(e, 11)}>
                        <img src={require('../../assets/images/icon/icon-requests.svg')} />
                        <span>My requests</span>
                    </Menu.Item>
                    }

                    {(user_id === profileId) &&
                        <Menu.Item key="8" onClick={(e) => radioOnChange(e, 8)}>
                            <img src={require('../../assets/images/icon/land7.svg')} />
                            <span>View Requests</span>
                        </Menu.Item>
                    }
                    {(user_id === profileId) &&
                        <Menu.Item key="12" onClick={(e) => radioOnChange(e, 12)}>
                            <img src={require('../../assets/images/icon/list.png')} />
                            <span>Payment history</span>
                        </Menu.Item>
                    }
                </Menu>
            </div>}
            {/* Sidebar Block */}
            {(user_id === profileId) &&
                <div className="f-sider-block f-ord2">
                    <ul className="f-action-btns">
                        <li><Button type="link" className="f-btn-edit" onClick={() => { setState(prev => ({ ...prev, editModal: true })) }}>SETTINGS</Button></li>
                    </ul>
                </div>
            }
            {userDetails && <Modal
                title="User Settings"
                visible={editModal}
                onOk={() => handleOk()}
                confirmLoading={confirmLoading}
                onCancel={e => handleCancel(e)}
                className="f-modal f-modal-wizard f-modal-wfooter"
            >
                <div className="f-modal-wiz-body">
                    <h3>Basic Details</h3>
                    <div className="f-form-control">
                        <label>Name*</label>
                        <Input
                            value={userDetails.full_name ? userDetails.full_name : ''}
                            onChange={(e) => handleChange(e, 'full_name')} />
                        {!userDetails.full_name.trim() && <span style={{ color: '#5c0011' }} >*Required Field</span>}
                    </div>
                    <div className="f-form-control">
                        <label>Phone*</label>
                        <Input
                            value={userDetails.phone ? userDetails.phone : ''}
                            onChange={(e) => handleChange(e, 'phone')}
                            disabled={true}
                        />
                    </div>
                    <div className="f-form-control">
                        <label>Email*</label>
                        <Input
                            value={userDetails.email ? userDetails.email : ''}
                            onChange={(e) => handleChange(e, 'email')}
                            disabled={true} />
                    </div>
                    <div className="f-form-control">
                        <label>Date of Birth</label>
                        <DatePicker
                            format={'MMM/DD/YYYY'}
                            onChange={(e) => handleChange(e, 'dob')}
                            defaultValue={userDetails.dob ? moment(userDetails.dob) : ''} />
                    </div>
                    <div className="f-form-control">
                        <label>Gender*</label>
                        <Select
                            style={{ width: '100%' }}
                            defaultValue={userDetails.gender ? userDetails.gender : ''} onChange={(e) => handleSelect('gender', e)}>
                            <Option value="-1">Select your gender</Option>
                            <Option value="male">Male</Option>
                            <Option value="female">Female</Option>
                            <Option value="other">Other</Option>
                        </Select>
                        {userDetails.gender == '-1' && <span style={{ color: '#5c0011' }} >*Required Field</span>}
                    </div>
                    <div className="f-form-control">
                        <label>City you are residing in</label>
                        {
                            <ReactGoogleMapLoader
                                params={{
                                    key: MY_API_KEY,
                                    libraries: "places,geocode",
                                }}
                                render={(googleMaps) =>
                                    googleMaps && (
                                        <ReactGooglePlacesSuggest
                                            googleMaps={googleMaps}
                                            autocompletionRequest={{
                                                input: search_location
                                            }}
                                            onSelectSuggest={handleSelectSuggest}
                                            textNoResults="No Result" // null or "" if you want to disable the no results item
                                            customRender={(prediction) => (
                                                <div className="customWrapper">
                                                    {prediction ? prediction.description : "No Result"}
                                                </div>
                                            )}>
                                            <Input
                                                className="art-mat-style art-suffix art-sfx-location"
                                                placeholder="Location"
                                                type="text"
                                                value={value_location}
                                                onChange={(e) => handleInputChange(e, 'location')}
                                                defaultValue={userDetails.location ? userDetails.location : ""} />
                                        </ReactGooglePlacesSuggest>
                                    )
                                }
                            />
                        }
                        {/* <Input value={userDetails.location ? userDetails.location : ''} onChange={(e) => handleChange(e, 'location')} /> */}
                    </div>
                    <div className="f-form-control">
                        <label>City you are originally from</label>
                        {
                            <ReactGoogleMapLoader
                                params={{
                                    key: MY_API_KEY,
                                    libraries: "places,geocode",
                                }}
                                render={(googleMaps) =>
                                    googleMaps && (
                                        <ReactGooglePlacesSuggest
                                            googleMaps={googleMaps}
                                            autocompletionRequest={{
                                                input: search_origin
                                            }}
                                            onSelectSuggest={handleSelectSuggestOrigin}
                                            textNoResults="No Result" // null or "" if you want to disable the no results item
                                            customRender={(prediction) => (
                                                <div className="customWrapper">
                                                    {prediction ? prediction.description : "No Result"}
                                                </div>
                                            )}>
                                            <Input
                                                className="art-mat-style art-suffix art-sfx-location"
                                                placeholder="Origin"
                                                type="text"
                                                value={value_origin}
                                                onChange={(e) => handleInputChange(e, 'origin')}
                                                defaultValue={userDetails.origin ? userDetails.origin : ""} />
                                        </ReactGooglePlacesSuggest>
                                    )
                                }
                            />
                        }
                        {/* <Input value={userDetails.origin ? userDetails.origin : ''} onChange={(e) => handleChange(e, 'origin')} /> */}
                    </div>
                    <div className="f-form-control f-wiz-switch">
                        <h3 style={{ margin: '10px 0 0' }}>Privacy Settings</h3>
                        <ul>
                            <li>
                                <h6>Allow user searchable</h6>
                                <Switch checked={searchable} onClick={(e) => onChangePrivacySettings('searchable', e)} />
                            </li>
                        </ul>
                    </div>
                </div>
            </Modal>}
        </aside>
    )
}
export default UserDetailsSidebar;