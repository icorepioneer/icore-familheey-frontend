import React, { useState,useEffect } from 'react';
import { Button, Modal, Input, Upload, Icon,notification } from 'antd';
import { feedBackSubmit } from '../../modules/Post/post_service';
import { notifications } from '../../shared/toasterMessage';
import { useSelector } from 'react-redux'


const props = {
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    listType: 'picture',
}

const FeedbackModal = ({ feedbackModel, callbackFromParent,toggleFeedbackModel }) => {
 const [feedBackState, setfeedBackState] = useState({
    user_id: useSelector(state=>state.user_details.userId),
        description: '',
        title: '',
        image: '',
        modelValue : false
    })
    useEffect(()=>{
        setfeedBackState(prev=>({...prev,modelValue:feedbackModel}))
        return ()=>feedbackModel=null
    },[feedbackModel])
    const { user_id, description, title, image, modelValue } = feedBackState;

    const handleChange = (name, e)  => {
        e.persist();
        setfeedBackState((prev) => ({...prev, [name]: e.target.value }))
    }
    const handleCancel = e => {
        setfeedBackState((prev) => ({...prev, modelValue: false }));
        callbackFromParent(false);
        toggleFeedbackModel();
    };
    const handleOk = e => {
        if (!title || !description) {
            let message = 'Error'
            let descr= `Please fill all the required fields`
            notifications('error', message, descr)
            return;
          }
        setfeedBackState((prev) => ({...prev, modelValue: false }));
        toggleFeedbackModel();
        const formData = new FormData();
        formData.append('os', 'web');
        formData.append('description', description);
        formData.append('title', title);
        formData.append('user', user_id);
        formData.append('device', navigator.platform);
        formData.append('image', image);
        feedBackSubmit(formData).then((response) => {
            openNotification()
        }).catch((error) => {
            console.log("error happened",error)
        })
    };
    const openNotification = () => {
        notification.open({
          message: "Successfully submitted your feedback",
          description: "",
          icon:<Icon type="alert" theme = "twoTone" twoToneColor="#3a2262"/>,
          duration: 3
        });
      };
    const uploadPhoto = info => {
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, imageUrl =>
                setfeedBackState((prev) => ({...prev, imageUrl, loading: false})),
            );
        }
    }
    const getBase64 = (img, callback) => {
        console.log('imggg', img);
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
        setfeedBackState((prev) => ({...prev, image: img }));
    }

    return (
        <Modal
            title="FeedBack"
            visible={modelValue}
            className="f-modal f-modal-wizard"
            onCancel={() => handleCancel()}
            footer={[
                
            ]}
        >
            <div className="f-modal-wiz-body">
                <div className="f-form-control">
                    <label>Title</label>
                    <Input minLength={1} maxLength={35} value={title} placeholder="Enter Title" onChange={(e) => handleChange('title',e)}></Input>
                </div>
                <div className="f-form-control">
                    <label>Image</label>
                    <Upload {...props} onChange={(event) => uploadPhoto(event)}>
                        
                            <Icon type="upload" /> Click to Upload
                                    
                    </Upload>
                </div>
                <div className="f-form-control">
                    <label>Description</label>
                    <Input value={description} placeholder="Enter Title" onChange={(e) => handleChange('description',e)}></Input>
                </div>
            </div>
            <div className="f-modal-wiz-footer" key="1">
                <Button key="submit" onClick={() => handleOk()}>Submit <Icon type="arrow-right" /></Button>
            </div>
        </Modal>
    )
}
export default FeedbackModal;