import React, { useState, useEffect, useContext } from 'react';
import { withRouter } from 'react-router-dom';
import { Input, Card, Avatar, Spin, Button, Modal, Tag, Carousel } from 'antd';
import { globalSearchResult, getGroupsToAddMember } from './api-searchResult';
import { encryption } from '../../shared/encryptDecrypt';
import { addMemberToGroup } from '../../modules/memberListing/api-memberlist';
import { joinToExistingFamily } from '../../modules/createFamily/api-family';
import './style.scss';
import SearchSidebar from '../SearchSidebar';
import { UserContext } from '../TopBar/GlobalContext';
import PostsCard from '../PostsCard';
import { useSelector, useDispatch } from 'react-redux';
import Collash from '../../modules/collash';
//import { eSearch } from '../../shared/elasticsearch';

const { Search } = Input;
// const { Meta } = Card;

var moment = require('moment')
const ResultPage = ({ history }) => {

    const { user } = useContext(UserContext);
    const dispatch = useDispatch();
    // console.log(user);
    // const type = {
    //     family: "family",
    //     users: "users",
    //     posts: "posts",
    //     events: "events",
    // }
    const [globalState, setglobalState] = useState({
        searchResult: {},
        loader: false,
        user_id: useSelector(state => state.user_details.userId),
        openModal: false,
        familyListArray: [],
        selectedUser: '',
        groupButton: '',
        sidebarKey: (user && user.sidebarKey) ? user.sidebarKey:'-1',
        search_text_f: '',
        search_tag_key: (user && user.search_tag_key) ? user.search_tag_key: '',
        previous_page: ''
    })
    let { searchResult, search_text_f, loader, user_id, openModal, familyListArray, selectedUser, sidebarKey, search_tag_key, previous_page } = globalState;
    useEffect(() => {
        //result('');
        //elastic_Search('');
        let { state } = history.location
        let { current } = state;
        setglobalState(prev => ({ ...prev, previous_page: current }))
    }, [])

    useEffect(() => {
        return () => {//umnount event
            dispatch({ type: 'DISCOVER_UNMOUNT', payload: { discover:" " } })
        }
    }, [])

    useEffect(()=>{
        setglobalState(prev => ({ ...prev, search_tag_key: user.search_tag_key  }))
    },[user])

    useEffect(() => {
        if(search_tag_key) result(search_text_f);
    }, [search_tag_key, search_text_f])

    // const elastic_Search = (val) => {
    //     //setglobalState((prev) => ({ ...prev, search_text_f: value, loader: true, searchResult: {} }))
    //     let params = {
    //         index:'post',
    //         searchtxt: val,
    //         userid: user_id,
    //         type: `${search_tag_key}`
    //     }
    //     eSearch(params).then((response) => {
    //         let { hits } = response;
    //         let data_is = hits.map(item => item._source);
    //         searchResult = { ...searchResult, posts:data_is }
    //         //setglobalState((prev) => ({ ...prev, searchResult: datais, loader: false }))
    //     }).catch((error) => {
    //         console.log('error', error)
    //     })
    // }

    const result = (value) => {
        setglobalState((prev) => ({ ...prev, search_text_f: value, loader: true }))
        let params = {
            searchtxt: value,
            userid: user_id,
            type: `${search_tag_key}`
        }
        console.log("== params === ", params)
        // if(sidebarKey == '-1' && !search_tag_key){ 
        //     params = { ...params, type:'family' } 
        // }
        if(value.trim()!=''){
            dispatch({ type: 'SEARCH_TERM', payload: { search_term:'family' } })
        }
        globalSearchResult(params).then((response) => {
            setglobalState((prev) => ({ ...prev, searchResult: response.data, loader: false }))
        }).catch((error) => {
            console.log('error', error)
        })
    }
    if (user.search_text && user.search_text != search_text_f) {
        result(user.search_text);
    }

    const navigateToUserPage = (value) => {
        var profileId = encryption(value.id)
        history.push(`/user-details/${profileId}`, { prev: window.location.pathname })
    }
    const navigateToFamilyPage = (value) => {
        var familyId = encryption(value.group_id)
        history.push(`/familyview/${familyId}`)
    }
    const addToFamily = (value, e) => {
        e.stopPropagation();
        setglobalState((prev) => ({ ...prev, familyListArray: [], openModal: true, selectedUser: value.id, loader: true }))
        let params = {
            user_id: user_id,
            member_to_add: value.id.toString()
        }
        getGroupsToAddMember(params).then((resp) => {
            setglobalState((prev) => ({ ...prev, familyListArray: resp.data.data, loader: false }))
        }).catch((err) => {

        })
    }
    const addMemberToFamily = (value, index) => {
        let params = {
            userId: selectedUser.toString(),
            groupId: value.id.toString(),
            from_id: user_id
        }
        addMemberToGroup(params).then((resp) => {
            let { data } = resp
            let listArray = familyListArray
            if (data && data.status === 'accepted') {
                listArray[index].JOINED = true
            } else if (data && data.status === 'pending') {
                listArray[index].status = 'pending'
            }
            setglobalState((prev) => ({ ...prev, familyListArray: [...listArray] }))
        }).catch((err) => {
            console.log(err)
        })
    }
    const handleOk = () => {
        setglobalState((prev) => ({ ...prev, openModal: false }));
    };

    const handleCancel = () => {
        setglobalState((prev) => ({ ...prev, openModal: false }));
    };
    const requestToFamily = (value, index, e) => {
        e.stopPropagation();
        let params = {
            user_id: user_id,
            group_id: value.group_id.toString()
        }
        joinToExistingFamily(params).then((resp) => {
            if (resp && resp.data && resp.data.data) {
                searchResult.groups[index].status = 'pending';
                setglobalState((prev) => ({ ...prev, searchResult: searchResult }));
            }
        }).then((err) => {

        })
    }
    const searchCallBack = (key, value) => {
        let _type;
        switch (key) {

            case '-1':
                _type = 'collash'
                break;
            case '0':
                history.push(`${previous_page}`)
                break;
            case '1':
                _type = 'family'
                break;
            case '2':
                _type = 'users'
                break;
            case '4':
                _type = 'posts'
                break;
            case '3':
                _type = 'events'
                break;
            case '5':
                _type = 'announcement'
                break;
        }
        setglobalState((prev) => ({ ...prev, sidebarKey: key, search_tag_key: _type, search_text_f: value }));
    }

    const collashCallBack = (callBackparams)=> {
        setglobalState(prev => ({ ...prev, ...callBackparams }))
    }

    const redirectToMessage = (e, id) => {
        e.stopPropagation();
        history.push({
            pathname: "/topics/create",
            state: { uid: [id], status: 'create' }
        })
    }

    const displayButtonsInFamilyView = (item,index) => {
        let { JOINED, status } = item
        switch (JOINED) {
            case true:
                return (
                    <Button className="f-line" disabled>Member</Button>
                )
            case false:
                    switch(status){
                        case 'pending':
                            return(
                                <Button className="f-line" disabled>Pending</Button>
                            )
                        default:
                            return(
                                <Button onClick={() => addMemberToFamily(item, index)}>Add</Button>
                            )
                    }
        }
    }
    return (
        <section className="ResultPage f-page">
            <div className="f-clearfix f-body">
                <div className="f-boxed">
                    <div className="f-event-details">
                        <section className="f-sect">
                            <SearchSidebar callbackFromParent={searchCallBack} search_text={search_text_f} loader={loader} />
                            <div className="f-events-main">
                                <Search
                                    placeholder="Enter search text"
                                    size="large"
                                    onSearch={value => result(value)}
                                    className="f-results-search-bar f-on-mob"
                                    style={{ display: 'none' }}
                                />

                                { sidebarKey == '-1' && <Collash history={history} callbackFromParent={collashCallBack}></Collash> }
                                
                                {/* <div className="f-search-bar-header"> */}
                                {/* <h5>{`Suggested ${user.search_text ? `results for ` + user.search_text : ''}`}</h5> */}
                                {/* <Button className="f-filter-icon">FILTER</Button> */}
                                {/* </div> */}
                                {/* ########################## Family ########################## */}
                                {searchResult && searchResult.groups && searchResult.groups.length > 0 && sidebarKey == '1' ?
                                    <div className="f-clearfix">
                                        <div className="f-search-bar-header">
                                            <h5>{`Search ${user.search_text ? `results for '` + user.search_text + `'` : ''}`}</h5>
                                        </div>
                                        <div className="f-search-result f-family-list f-search-group">
                                            {
                                                searchResult.groups.map((item, index) => {
                                                    return <div key={index} className="f-family-card" onClick={() => navigateToFamilyPage(item)}>
                                                        <div className="f-inset f-clickable">
                                                            <div className="f-start">
                                                                <div className="f-left">
                                                                    <img onError={(e) => { e.target.src = require('../../assets/images/default_logo.png') }} src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo} />
                                                                </div>
                                                                <div className="f-right">
                                                                    <h4>{item.group_name}</h4>
                                                                    <h5>By <strong>{item.created_by_name}</strong></h5>
                                                                    <h6>{item.intro}</h6>
                                                                    <h6 className="f-pin">{item.base_region}</h6>
                                                                </div>
                                                            </div>
                                                            <div className="f-stop">
                                                                <h5><strong>{item.membercount ? item.membercount : 0}</strong> Members</h5>
                                                                {item.knowncount > 0 &&
                                                                    <h5><strong>{item.knowncount}</strong> Known Members</h5>
                                                                }
                                                            </div>
                                                        </div>
                                                        <footer>
                                                            {item.is_joined && (item.is_blocked || item.is_removed) && item.member_joining !== 1 &&
                                                                <Button onClick={(e) => requestToFamily(item, index, e)}>Join</Button>
                                                            }{item.is_joined && !item.is_blocked && !item.is_removed &&
                                                                <Button onClick={(e) => e.stopPropagation()} disabled>Member</Button>
                                                            }{!item.is_joined && item.status === 'pending' &&
                                                                <Button onClick={(e) => e.stopPropagation()} disabled>Pending</Button>
                                                            }{!item.is_joined && item.member_joining === 1 &&
                                                                <Button onClick={(e) => e.stopPropagation()} disabled>Private</Button>
                                                            }{!item.is_joined && item.status === 'rejected' &&
                                                                <Button onClick={(e) => e.stopPropagation()} disabled>Rejected</Button>
                                                            }{!item.is_joined && !item.status &&
                                                                <Button onClick={(e) => requestToFamily(item, index, e)}>Join</Button>
                                                            }
                                                        </footer>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    </div>
                                    : (searchResult && searchResult.groups && searchResult.groups.length == 0 && sidebarKey === '1') ? <div className="f-search-bar-header">
                                        <h5>No results to show</h5>
                                    </div> : ''}
                                {/* ########################## People ########################## */}
                                {searchResult && searchResult.users && searchResult.users.length > 0 && sidebarKey === '2' ?
                                    <div className="f-clearfix">
                                        <div className="f-search-bar-header">
                                            <h5>{`Search ${user.search_text ? `results for '` + user.search_text + `'` : ''}`}</h5>
                                        </div>
                                        <div className="f-search-result f-family-list f-search-user">
                                            {
                                                searchResult.users.map((item, index) => {
                                                    return <div key={index} className="f-family-card f-clickable" onClick={() => navigateToUserPage(item)}>
                                                        <div className="f-inset">
                                                            <div className="f-start">
                                                                <div className="f-left">
                                                                    <img
                                                                        style={{ background: '#dcdbdb', borderRadius: '13px' }}
                                                                        src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic
                                                                            :
                                                                            (item.gender === 'male' ? require('../../../src/assets/images/Male-Profile-pic.png')
                                                                                :
                                                                                item.gender == 'female' ? require('../../../src/assets/images/Female-Profile-pic.png')
                                                                                    :
                                                                                    require('../../../src/assets/images/default.png'))}
                                                                    />
                                                                </div>
                                                                <div className="f-right">
                                                                    <h4>{item.full_name}</h4>
                                                                    <h6 className="f-pin">{item.location}</h6>
                                                                    <div className="f-stop">
                                                                        <h5><strong>{item.familycount}</strong> Families</h5>
                                                                    </div>
                                                                    {
                                                                        <div className="f-right-combo" style={{ paddingLeft: '60px' }}>
                                                                            <span style={{ marginRight: '10px' }} className="pd-l" onClick={(e) => { redirectToMessage(e, item.id) }}> <img src={require('../../assets/images/icon/chat_d.svg')} /></span>
                                                                            <Button onClick={(e) => addToFamily(item, e)}>Add to Family</Button>
                                                                        </div>
                                                                    }

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    </div>
                                    : (searchResult && searchResult.users && searchResult.users.length == 0 && sidebarKey === '2') ?
                                        <div className="f-search-bar-header">
                                            <h5>No results to show</h5>
                                        </div> : ''
                                }

                                {/* ########################## Events ########################## */}
                                {searchResult && searchResult.events && searchResult.events.length > 0 && sidebarKey === '3' ?
                                    <div className="f-search-result f-family-list f-search-events">
                                        <div className="f-search-bar-header">
                                            <h5>{`Search ${user.search_text ? `results for '` + user.search_text + `'` : ''}`}</h5>
                                        </div>
                                        {
                                            searchResult.events.map((item, index) => {
                                                let encrypted_id = encryption(item.id)
                                                return <Card onClick={() => { history.push(`/event-details/${encrypted_id}`) }} className="eventCard f-event-card" key={index}>
                                                    <header className="f-head">
                                                        <div className="f-west">
                                                            {/* Use If No User Image */}
                                                            {/* <Avatar icon="user" /> */}
                                                            <Avatar src={item.propic ? process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../assets/images/demo/demo_e2.png')} />
                                                            <span>
                                                                <h5>{item.created_user}</h5>
                                                                <h6>Posted in <strong>People Group</strong></h6>
                                                                {/* <h6>Nov 11, 2019  14:20 PM</h6> */}
                                                                <h6>{moment.unix(item.from_date).format('MMM DD, YYYY h:mm a')}</h6>
                                                            </span>
                                                        </div>
                                                        <div className="f-east">
                                                            {/* <Button className="f-btn-group" /> */}
                                                            {item.is_sharable && <Button className="f-btn-share" />}
                                                        </div>
                                                    </header>
                                                    <section className="f-sect">
                                                        <div className="f-poster">
                                                            <img src={item.event_image ? process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'event_image/' + item.event_image : require('../../assets/images/default_event_image.png')} />
                                                            <span className="f-overlay">
                                                                <Tag color="#ffdd72">Free</Tag>
                                                                <ul>
                                                                    <li>{item.event_type}</li>
                                                                    <li>Signup</li>
                                                                </ul>
                                                            </span>
                                                        </div>
                                                        <div className="f-agenda">
                                                            <div className="f-west">
                                                                <span className="f-date">
                                                                    <strong>{moment.unix(item.from_date).format('DD')}</strong>
                                                                    <i>{moment.unix(item.from_date).format('MMM')}</i>
                                                                </span>
                                                            </div>
                                                            <div className="f-east">
                                                                <h3>{item.event_name}</h3>
                                                                <h5 className="f-time">{moment(item.from_date).format('h:mm a')}- {moment(item.to_date).format('h:mm a')}</h5>
                                                                <h5 className="f-location">{item.location}<em></em></h5>
                                                            </div>
                                                        </div>

                                                        {/* COMMENTED AS IT IS NOT IN APP */}
                                                        {/* <div className="f-meta">
                                                            <ul>
                                                                <li>{item.going_count} Going</li>
                                                                <li>{item.interested_count} Interested</li>
                                                                {item.going_count && item.going_count > 0 &&
                                                                    <li>{item.first_person_going} {(item.going_count > 1 ? ('and ' + (item.going_count - 1) + ' others going') : 'is going')} </li>
                                                                }
                                                            </ul>
                                                        </div> */}
                                                    </section>
                                                </Card>
                                            })
                                        }
                                    </div>
                                    : (searchResult && searchResult.events && searchResult.events.length == 0 && sidebarKey === '3') ?
                                        <div className="f-search-bar-header">
                                            <h5>No results to show</h5>
                                        </div> : ''
                                }
                                {searchResult && searchResult.posts && searchResult.posts.data.length > 0 && sidebarKey === '4' ?
                                    <div>
                                        <div className="f-search-bar-header">
                                            <h5>{`Search ${user.search_text ? `results for '` + user.search_text + `'` : ''}`}</h5>
                                        </div>
                                        {searchResult.posts.data.map((v, i) => {
                                            let { post_id } = v
                                            let encrypted_post_id = encryption(post_id)
                                            return <PostsCard
                                                key={i}
                                                onClick={() => {
                                                    history.push(`/post_details/${encrypted_post_id}`
                                                    )
                                                }}
                                                type="1"
                                                data={v}
                                                history={history}
                                                chatVisible={false}
                                            ></PostsCard>
                                        })
                                        }
                                    </div>

                                    : (searchResult && searchResult.posts && searchResult.posts.data.length == 0 && sidebarKey === '4') ?
                                        <div className="f-search-bar-header">
                                            <h5>No results to show</h5>
                                        </div> : ''
                                }
                                {searchResult && searchResult.announcement && searchResult.announcement.data.length > 0 && sidebarKey === '5' ?
                                    <div>
                                        <div className="f-search-bar-header">
                                            <h5>{`Search ${user.search_text ? `results for '` + user.search_text + `'` : ''}`}</h5>
                                        </div>
                                        {searchResult.announcement.data.map((v, i) => {
                                            return <PostsCard history={history} type="1" data={v}></PostsCard>
                                        })
                                        }
                                    </div>
                                    : (searchResult && searchResult.announcement && searchResult.announcement.data.length == 0 && sidebarKey === '5') ?
                                        <div className="f-search-bar-header">
                                            <h5>No results to show</h5>
                                        </div> : ''
                                }
                                {/* ########################## Posts ########################## */}
                                <div className="f-search-result f-family-list f-search-posts" style={{ display: 'none' }}>
                                    <Card className="postsCard f-post-card">
                                        <header className="f-head">
                                            <div className="f-west">
                                                {/* Use If No User Image */}
                                                {/* <Avatar icon="user" /> */}
                                                <Avatar src={require('../../assets/images/demo/demo_u1a.png')} />
                                                <span>
                                                    <h5>Angela Chinweze</h5>
                                                    <h6>Posted in <strong>People Group</strong></h6>
                                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                                </span>
                                            </div>
                                            <div className="f-east">
                                                <Button className="f-btn-group" />
                                                <Button className="f-btn-share" />
                                            </div>
                                        </header>
                                        <section className="f-sect" >
                                            <div className="f-poster">
                                                <Carousel autoplay>
                                                    <div>
                                                        <img src={require('../../assets/images/demo/demo_e2.png')} />
                                                    </div>
                                                    <div>
                                                        <img src={require('../../assets/images/demo/demo_e1.png')} />
                                                    </div>
                                                    <div>
                                                        <img src={require('../../assets/images/demo/demo_e3.png')} />
                                                    </div>
                                                </Carousel>
                                            </div>
                                            <div className="f-post-description">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere fringilla leo, volutpat pretium ex lacinia nec. Maecenas mollis orci augue...
                                                    <Button type="link">view more</Button>
                                                </p>
                                            </div>
                                        </section>
                                        <footer className="f-foot">
                                            <div className="f-west">
                                                <ul>
                                                    <li><span className="f-comment">150 <i /></span></li>
                                                    <li><span className="f-share">10</span></li>
                                                    <li><span className="f-viewed">2.5k</span></li>
                                                </ul>
                                            </div>
                                            <div className="f-east">
                                                <Button />
                                            </div>
                                        </footer>
                                    </Card>
                                    <Card className="postsCard f-post-card">
                                        <header className="f-head">
                                            <div className="f-west">
                                                {/* Use If No User Image */}
                                                {/* <Avatar icon="user" /> */}
                                                <Avatar src={require('../../assets/images/demo/demo_u1a.png')} />
                                                <span>
                                                    <h5>Angela Chinweze</h5>
                                                    <h6>Posted in <strong>People Group</strong></h6>
                                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                                </span>
                                            </div>
                                            <div className="f-east">
                                                <Button className="f-btn-group" />
                                                <Button className="f-btn-share" />
                                            </div>
                                        </header>
                                        <section className="f-sect" >
                                            <div className="f-post-description">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere fringilla leo, volutpat pretium ex lacinia nec. Maecenas mollis orci augue...
                                                    <Button type="link">view more</Button>
                                                </p>
                                            </div>
                                        </section>
                                        <footer className="f-foot">
                                            <div className="f-west">
                                                <ul>
                                                    <li><span className="f-comment">150 <i /></span></li>
                                                    <li><span className="f-share">10</span></li>
                                                    <li><span className="f-viewed">2.5k</span></li>
                                                </ul>
                                            </div>
                                            <div className="f-east">
                                                <Button />
                                            </div>
                                        </footer>
                                    </Card>
                                    <Card className="postsCard f-post-card">
                                        <header className="f-head">
                                            <div className="f-west">
                                                {/* Use If No User Image */}
                                                {/* <Avatar icon="user" /> */}
                                                <Avatar src={require('../../assets/images/demo/demo_u1a.png')} />
                                                <span>
                                                    <h5>Angela Chinweze</h5>
                                                    <h6>Posted in <strong>People Group</strong></h6>
                                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                                </span>
                                            </div>
                                            <div className="f-east">
                                                <Button className="f-btn-group" />
                                                <Button className="f-btn-share" />
                                            </div>
                                        </header>
                                        <section className="f-sect" >
                                            <div className="f-shared-post">
                                                <header className="f-head">
                                                    <div className="f-west">
                                                        {/* Use If No User Image */}
                                                        {/* <Avatar icon="user" /> */}
                                                        <Avatar src={require('../../assets/images/demo/demo_u1c.png')} />
                                                        <span>
                                                            <h5>Angela Chinweze</h5>
                                                            <h6>Posted in <strong>People Group</strong></h6>
                                                        </span>
                                                    </div>
                                                </header>
                                                <section className="f-sect" >
                                                    <div className="f-poster">
                                                        <Carousel autoplay>
                                                            <div>
                                                                <img src={require('../../assets/images/demo/demo_e2.png')} />
                                                            </div>
                                                            <div>
                                                                <img src={require('../../assets/images/demo/demo_e1.png')} />
                                                            </div>
                                                            <div>
                                                                <img src={require('../../assets/images/demo/demo_e3.png')} />
                                                            </div>
                                                        </Carousel>
                                                    </div>

                                                </section>
                                            </div>
                                            <div className="f-post-description">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere fringilla leo, volutpat pretium ex lacinia nec. Maecenas mollis orci augue...
                                                    <Button type="link">view more</Button>
                                                </p>
                                            </div>
                                        </section>
                                        <footer className="f-foot">
                                            <div className="f-west">
                                                <ul>
                                                    <li><span className="f-comment">150 <i /></span></li>
                                                    <li><span className="f-share">10</span></li>
                                                    <li><span className="f-viewed">2.5k</span></li>
                                                </ul>
                                            </div>
                                            <div className="f-east">
                                                <Button />
                                            </div>
                                        </footer>
                                    </Card>
                                </div>

                                {/* ########################## Announcements ########################## */}
                                <div className="f-search-result f-family-list f-search-announcements" style={{ display: 'none' }}>
                                    <Card className="postsCard f-post-card">
                                        <header className="f-head">
                                            <div className="f-west">
                                                {/* Use If No User Image */}
                                                {/* <Avatar icon="user" /> */}
                                                <Avatar src={require('../../assets/images/demo/demo_u1b.png')} />
                                                <span>
                                                    <h5>Winchester family</h5>
                                                    <h6>Posted By <strong>Mackenzy Manual</strong></h6>
                                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                                </span>
                                            </div>
                                        </header>
                                        <section className="f-sect">
                                            <div className="f-post-description">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere fringilla leo, volutpat pretium ex lacinia nec. Maecenas mollis orci augue...</p>
                                            </div>
                                        </section>
                                        <footer className="f-foot">
                                            <div className="f-west">
                                                <ul>
                                                    <li><span className="f-comment">150 Conversations<i /></span></li>
                                                    <li><span className="f-share">10</span></li>
                                                    <li><span className="f-viewed">2.5k</span></li>
                                                </ul>
                                            </div>
                                            <div className="f-east">
                                                <span className="f-docs">2 Docs</span>
                                                <Button />
                                            </div>
                                        </footer>
                                    </Card>
                                    <Card className="postsCard f-post-card">
                                        <header className="f-head">
                                            <div className="f-west">
                                                {/* Use If No User Image */}
                                                {/* <Avatar icon="user" /> */}
                                                <Avatar src={require('../../assets/images/demo/demo_u1b.png')} />
                                                <span>
                                                    <h5>Winchester family</h5>
                                                    <h6>Posted By <strong>Mackenzy Manual</strong></h6>
                                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                                </span>
                                            </div>
                                        </header>
                                        <section className="f-sect">
                                            <div className="f-post-description">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere fringilla leo, volutpat pretium ex lacinia nec. Maecenas mollis orci augue...</p>
                                            </div>
                                        </section>
                                        <footer className="f-foot">
                                            <div className="f-west">
                                                <ul>
                                                    <li><span className="f-comment">150 Conversations<i /></span></li>
                                                    <li><span className="f-share">10</span></li>
                                                    <li><span className="f-viewed">2.5k</span></li>
                                                </ul>
                                            </div>
                                            <div className="f-east">
                                                <span className="f-docs">2 Docs</span>
                                                <Button />
                                            </div>
                                        </footer>
                                    </Card>
                                    <Card className="postsCard f-post-card">
                                        <header className="f-head">
                                            <div className="f-west">
                                                {/* Use If No User Image */}
                                                {/* <Avatar icon="user" /> */}
                                                <Avatar src={require('../../assets/images/demo/demo_u1b.png')} />
                                                <span>
                                                    <h5>Winchester family</h5>
                                                    <h6>Posted By <strong>Mackenzy Manual</strong></h6>
                                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                                </span>
                                            </div>
                                        </header>
                                        <section className="f-sect">
                                            <div className="f-post-description">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere fringilla leo, volutpat pretium ex lacinia nec. Maecenas mollis orci augue...</p>
                                            </div>
                                        </section>
                                        <footer className="f-foot">
                                            <div className="f-west">
                                                <ul>
                                                    <li><span className="f-comment">150 Conversations<i /></span></li>
                                                    <li><span className="f-share">10</span></li>
                                                    <li><span className="f-viewed">2.5k</span></li>
                                                </ul>
                                            </div>
                                            <div className="f-east">
                                                <span className="f-docs">2 Docs</span>
                                                <Button />
                                            </div>
                                        </footer>
                                    </Card>
                                </div>



                                {loader &&
                                    <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}>
                                    </Spin>
                                }
                                {/* Modal */}
                                <Modal
                                    className="f-modal f-modal-wizard"
                                    title="Add to Family"
                                    visible={openModal}
                                    onOk={() => handleOk()}
                                    onCancel={() => handleCancel()}
                                    footer={[
                                        //   <button>cancel</button>
                                    ]}
                                >
                                    <div className="f-modal-wiz-body" >
                                        <div className="f-member-listing-cards f-share-scroll">
                                            {loader && familyListArray.length === 0 &&
                                                <Spin tip="Loading..." style={{ 'paddingLeft': '50%', 'paddingTop': '25%' }}></Spin>
                                            }
                                            {familyListArray && familyListArray.length > 0 &&

                                                familyListArray.map((item, index) => {
                                                    return <Card key={index} className="f-member-cardx">
                                                        <div className="f-start">
                                                            <div className="f-left">
                                                                <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../assets/images/male-avatar.jpg')} />
                                                            </div>
                                                            <div className="f-right">
                                                                <h4>{item.group_name}</h4>
                                                                <h6>By <strong>{item.created_by}</strong></h6>
                                                                <h6>Religious / Spiritual</h6>
                                                                <h6 className="f-pin">{item.base_region}</h6>
                                                                <div className="f-right-combo" style={{ marginTop: '10px' }}>
                                                                    {displayButtonsInFamilyView(item,index)}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Card>
                                                })
                                            }
                                        </div>
                                    </div>
                                </Modal>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

        </section>
    )
}
export default withRouter(ResultPage);