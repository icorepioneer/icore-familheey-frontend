
import axios from '../../services/apiCall';

// let newAxios = require('axios');
// let _call;

const globalSearchResult = (data) => {
    // if(data.type == "undefined"){
    //     data = { ...data, type:'family' }
    // }
    //let { searchtxt } = data;
    //if(searchtxt && searchtxt!='')
    //if (_call) _call.cancel();
    //_call = newAxios.CancelToken.source();
    //{ cancelToken: _call.token } add as third param to cancel prev request
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/globalsearch`,data);
}
const getGroupsToAddMember = (data) => {
    return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/getGroupsToAddMember`,data);
}
export {
    globalSearchResult,
    getGroupsToAddMember
}