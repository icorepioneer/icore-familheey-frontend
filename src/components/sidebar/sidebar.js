import React from 'react';
import './sidebar.min.css'
class Sidebar extends React.Component {
  
    render() {
        return (
            <div class="d-flex" id="wrapper">
            <div class="bg-light border-right" id="sidebar-wrapper">
              <div class="sidebar-heading">Start Bootstrap </div>
              <div class="list-group list-group-flush">
                <a href="#1" class="list-group-item list-group-item-action bg-light">Dashboard</a>
                <a href="#2" class="list-group-item list-group-item-action bg-light">Shortcuts</a>
                <a href="#3" class="list-group-item list-group-item-action bg-light">Overview</a>
                <a href="#4" class="list-group-item list-group-item-action bg-light">Events</a>
                <a href="#5" class="list-group-item list-group-item-action bg-light">Profile</a>
                <a href="#6" class="list-group-item list-group-item-action bg-light">Status</a>
              </div>
            </div>
            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
            </div>
        )
    }
}
export default Sidebar;