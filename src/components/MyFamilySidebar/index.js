import React, { useState, useEffect } from 'react';
import './style.scss'
import { Button, Avatar, Menu } from 'antd';
import { getSidebarCount, postList } from '../../modules/Post/post_service';
import EventListSide from '../EventListing/index';
import AnnouncementList from '../Announcements/index';
import FeedbackModal from '../FeedbackModal/feedBack';
import { useSelector } from 'react-redux'

const { SubMenu } = Menu;
const MyFamilySidebar = ({ history, postTabSwitch }) => {
    let {sideBarCount_loaded,sideBarCount} = useSelector(red_state=>red_state.side_bar_data)
    const [familyState, setfamilyState] = useState({
        feedbackModel: false,
        description: '',
        title: '',
        user_id: useSelector(state=>state.user_details.userId),
        imageLoading: false,
        myEventList: [],
        exploreEvent: [],
        sidebar_data: sideBarCount?sideBarCount:{},
        upComingEventList: [],
        postlist: []

    })
    const { feedbackModel, postlist, user_id, sidebar_data } = familyState;

    const openFeedbackModel = () => {
        //setfamilyState((familyState) => ({ ...familyState, feedbackModel: true }))
        setfamilyState((prev)=>({ ...prev, feedbackModel:true }))
    }
    const navigateToEvents = (key) => {
        history.push(`/events`, {
            key: key
        })
    }
    const toggleFeedbackModel=()=>{
        setfamilyState(prev=>({...prev,feedbackModel:!feedbackModel}))
    }
    const navigate = (path, obj) => {
        if (obj) {
            history.push(path, obj);
        } else {
            history.push(path);
        }
    }
    const getSidebarCountFn = () => {
        getSidebarCount({ user_id: user_id })
            .then(response => {
                //setfamilyState((familyState) => ({ ...familyState, sidebar_data: response.data }))
                setfamilyState((prev)=>({ ...prev, sidebar_data: response.data }));
            });
    }
    const getPostList=()=>{
        postList({ user_id: user_id, offset: 0, limit: 3, type: 'post' })
        .then(response => {
            //setfamilyState((familyState) => ({ ...familyState, postlist: response.data.data }))
            setfamilyState((prev)=>({ ...prev, postlist: response.data.data }));
        });
    }
    const callback = (key) => {
        //setfamilyState((familyState) => ({ ...familyState, feedbackModel: key }))
        setfamilyState((prev)=>({ ...prev,feedbackModel: key  }))
    }

    useEffect(() => {
        !sideBarCount_loaded&&getSidebarCountFn();
        getPostList()
        getSidebarCountFn()//added to update family count : Anand
    }, [])
    return (
        <aside className="MyFamilySidebar f-sider f-force-hide-tab">

            {/* Sidebar Block */}
            <div className="f-sider-block f-sider-menu-incl">
                <Menu
                    mode="inline"
                    defaultSelectedKeys={['3']}
                    className="f-sider-menu"
                >
                    <SubMenu
                        key="sub2"
                        title={
                            <span>
                                <img src={require('../../assets/images/icon/land1.svg')} />
                                <span>Feed</span>
                                <i>{sidebar_data['post_count']}</i>
                            </span>
                        }
                    >
                        <Menu.Item key="1" onClick={() => postTabSwitch ? postTabSwitch('family') : history.push('/post', { type_id: 1 })}>
                            <span>My Familheey</span>
                        </Menu.Item>
                        <Menu.Item key="9" onClick={() => postTabSwitch ? postTabSwitch('public') : history.push('/post', { type_id: 2 })}>
                            <span>Public</span>
                        </Menu.Item>
                    </SubMenu>
                    <Menu.Item key="16" onClick={() => navigate('requests')}>
                        <img src={require('../../assets/images/icon/icon-requests.svg')} />
                        <span>Requests</span>
                    </Menu.Item>
                    <Menu.Item key="2" onClick={() => navigate('announcement')}>
                        <img src={require('../../assets/images/icon/land2.svg')} />
                        <span>Announcements</span>
                        <i>{sidebar_data['announcement_count']}</i>
                    </Menu.Item>
                    <Menu.Item key="10" onClick={() => navigate('topics')}>
                            <img src={require('../../assets/images/icon/chat.svg')} />
                            <span>Messages</span>
                        </Menu.Item>
                    <Menu.Item key="3" onClick={() => navigate()}>
                        <img src={require('../../assets/images/icon/land3.svg')} />
                        <span>Families</span>
                        <i>{sidebar_data['family_count']}</i>
                    </Menu.Item>
                    <SubMenu
                        key="sub1"
                        title={
                            <span>
                                <img src={require('../../assets/images/icon/land4.svg')} />
                                <span>Events</span>
                                <i>{sidebar_data['my_event_count']}</i>
                            </span>
                        }
                    >
                        <Menu.Item key="4" onClick={() => navigateToEvents('explore')}>
                            <span>Explore</span>
                        </Menu.Item>
                        <Menu.Item key="7" onClick={() => navigateToEvents('invitation')}>
                            <span>Invitations</span>
                        </Menu.Item>
                        <Menu.Item key="8" onClick={() => navigateToEvents('myevent')}>
                            <span>My Events</span>
                        </Menu.Item>
                    </SubMenu>
                    <Menu.Item key="5" onClick={() => history.push('/calender')}>
                        <img src={require('../../assets/images/icon/land5.svg')} />
                        <span>Calendar</span>
                    </Menu.Item>
                    <Menu.Item key="6" onClick={() => openFeedbackModel()}>
                        <img src={require('../../assets/images/icon/land6.svg')} />
                        <span>Feedback</span>
                    </Menu.Item>
                </Menu>
                {feedbackModel &&
                    <FeedbackModal feedbackModel={feedbackModel} toggleFeedbackModel={toggleFeedbackModel} callbackFromParent={() => callback()}></FeedbackModal>
                }
            </div>

            {/* Sidebar Block */}
            <div className="f-sider-block">
                <div className="f-sider-head">
                    <img src={require('../../assets/images/icon/land1.svg')} />
                    <h4>Recent Feed</h4>
                </div>
                <div className="f-sider-body">
                    <ul>
                        {
                            postlist && postlist.map((value, i) => {
                                return (
                                    <li key={i}>
                                        <Avatar src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + value.propic} />
                                        <span>
                                            <h5>{value.created_user_name}</h5>
                                            <h6>Posted in <strong>{value.group_name}</strong></h6>
                                        </span>
                                        <i className="f-dot" />
                                    </li>
                                );
                            })
                        }

                    </ul>
                </div>
                <div className="f-sider-foot">
                    <Button type="link" onClick={() => { history.push('/post') }}> more</Button>
                </div>
            </div>

            {/* Sidebar Block */}
            <AnnouncementList history={history}></AnnouncementList>

            {/* Sidebar Block */}
            <EventListSide history={history}></EventListSide>

        </aside>
    )
}
export default MyFamilySidebar;