import React from 'react';
import './style.scss'
import { Button, Card } from 'antd';
var moment = require('moment');



class AnnouncementSidebar extends React.Component {
    state;
    constructor(props) {
        super(props);

        this.state = {

        }
    }
    close = () =>{
        this.props.history.goBack();
    }
    render() {
        return (
            <aside className="AnnouncementSidebar f-sider">
                <div className="f-announce-back">
                    <Button onClick={this.close}><i /></Button> New Announcements
                </div>
                <div className="f-announce-list">
                    <div className="f-announce-cards">
                        {
                            this.props.data && this.props.data.map((ann, i) => {
                                return (<Card key={i} onClick={() => this.props.on_click(i)} className={`f-announce-card ${this.props.itemIndex == i ? 'active' : ''}`}>
                                    <div className="f-start">
                                        <div className="f-left">
                                            <img onError={(e) => { e.target.src = require('../../assets/images/create_post.png') }} src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + ann.family_logo} />
                                        </div>
                                        <div className="f-right">
                                            <h4>{ann.group_name}</h4>
                                            <h5>Posted by <strong>{ann.created_user_name}</strong></h5>
                                            <h6>{moment(ann.createdAt).format('DD MMM,YYYY  hh:mm A')}</h6>
                                        </div>
                                        {/* <Button /> */}
                                    </div>
                                </Card>);
                            })
                        }
                        {/* <Card className="f-announce-card active">
                            <div className="f-start">
                                <div className="f-left">
                                    <img src={require('../../assets/images/demo/m1.png')} />
                                </div>
                                <div className="f-right">
                                    <h4>Ibgo Congress</h4>
                                    <h5>Posted by <strong>Alica Joice</strong></h5>
                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                </div>
                                <Button />
                            </div>
                        </Card>
                        <Card className="f-announce-card">
                            <div className="f-start">
                                <div className="f-left">
                                    <img src={require('../../assets/images/demo/m1.png')} />
                                </div>
                                <div className="f-right">
                                    <h4>Ibgo Congress</h4>
                                    <h5>Posted by <strong>Alica Joice</strong></h5>
                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                </div>
                                <Button />
                            </div>
                        </Card>
                        <Card className="f-announce-card">
                            <div className="f-start">
                                <div className="f-left">
                                    <img src={require('../../assets/images/demo/m2.png')} />
                                </div>
                                <div className="f-right">
                                    <h4>Ibgo Congress</h4>
                                    <h5>Posted by <strong>Alica Joice</strong></h5>
                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                </div>
                                <Button />
                            </div>
                        </Card>
                        <Card className="f-announce-card">
                            <div className="f-start">
                                <div className="f-left">
                                    <img src={require('../../assets/images/demo/m3.png')} />
                                </div>
                                <div className="f-right">
                                    <h4>Ibgo Congress</h4>
                                    <h5>Posted by <strong>Alica Joice</strong></h5>
                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                </div>
                                <Button />
                            </div>
                        </Card>
                        <Card className="f-announce-card">
                            <div className="f-start">
                                <div className="f-left">
                                    <img src={require('../../assets/images/demo/m4.png')} />
                                </div>
                                <div className="f-right">
                                    <h4>Ibgo Congress</h4>
                                    <h5>Posted by <strong>Alica Joice</strong></h5>
                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                </div>
                                <Button />
                            </div>
                        </Card>
                        <Card className="f-announce-card">
                            <div className="f-start">
                                <div className="f-left">
                                    <img src={require('../../assets/images/demo/m1.png')} />
                                </div>
                                <div className="f-right">
                                    <h4>Ibgo Congress</h4>
                                    <h5>Posted by <strong>Alica Joice</strong></h5>
                                    <h6>Nov 11, 2019 14:20 PM</h6>
                                </div>
                                <Button />
                            </div>
                        </Card> */}

                    </div>
                </div>
                <div className="f-announce-all">
                    <Button>Show All</Button>
                </div>
            </aside>
        )
    }
}
export default AnnouncementSidebar;