import React, { useEffect, useState } from 'react';
import { Button, Modal, Form, Input, Select } from 'antd';
import countries from '../../common/countrylist'
const { Option } = Select;

const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form, loading, type } = this.props;
            const { getFieldDecorator } = form;
            const prefixSelector = getFieldDecorator("prefix",{
                rules: [
               {
                   required: true,
                   message: "Please choose the country!"
               }
           ]})(
                <Select showSearch style={{ width: 180 }}>
                    {countries.countries.map((item, i) =>
                        <Option key={i} value={item.name}>{`${item.name} : ${item.code}`}</Option>
                    )}
                </Select>
            );
            return (
                <Modal
                    visible={visible}
                    destroyOnClose = {true}
                    title={ `${type} New Contact` }
                    okText="Create"
                    onCancel={onCancel}
                    onOk={onCreate}
                    className="f-modal f-modal-wizard f-modal-wfooter"
                    footer={[
                        <div className="f-modal-wiz-footer" style={{ marginTop: '0' }}>
                            <Button type="line" key="back" onClick={onCancel}>Cancel</Button>
                            <Button key="submit" type="primary" loading={loading} onClick={onCreate}>Submit</Button>
                        </div>
                    ]}
                    >
                    <div className="f-modal-wiz-body" style={{ margin: '0' }}>
                        <Form layout="vertical" className="f-form-layout">
                            <div className="f-form-control" style={{ marginTop: '0' }}>
                            <Form.Item label="Name" hasFeedback>
                                {getFieldDecorator("name", {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please input the name!"
                                        }
                                    ]
                                })(<Input/>)}
                            </Form.Item>
                            </div>
                            <div className="f-phone-combined">
                            <Form.Item  label="Country" className="f-ctry-code" hasFeedback>
                               {prefixSelector}
                            </Form.Item>

                            <Form.Item label="Phone Number" hasFeedback>
                                {getFieldDecorator("phone", {
                                    rules: [
                                        { required: true, message: "Please input your phone number!" }
                                    ]
                                })(<Input  type="number"  style={{ width: "100%" }} />)}
                            </Form.Item>
                            </div>
                            <div className="f-form-control" style={{ marginTop: '0' }}>
                            <Form.Item label="E-mail" hasFeedback>
                                {getFieldDecorator("email", {
                                    rules: [
                                        {
                                            type: "email",
                                            message: "The input is not valid E-mail!"
                                        },
                                        {
                                            required: true,
                                            message: "Please input your E-mail!"
                                        }
                                    ]
                                })(<Input />)}
                            </Form.Item>
                            </div>
                        </Form>
                    </div>
                </Modal>
            );
        }
    },
);

const CreateContact = ({ contactModal_visible, createContact, event_id, updateContacts, contactToEdit, updateSelectedContact }) => {
    let formReff;
    const [modalState, setModalState] = useState({
        visible: false,
        loading: false,
        contactToDisplay: ''
    })
    let { visible, loading } = modalState
    const handleCancel = () => {
        setModalState(prev => ({ ...prev, visible: false }))
    };

    const handleCreate = () => {
        const { form } = formReff.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            setModalState(prev => ({ ...prev, loading: true }))
            let { prefix, phone } = values
            let country = countries.countries.filter(item => item.name === prefix && item.code)
            let phone_code = `${country && country[0].code}-${phone}`
            let data = { ...values, event_id: event_id, phone: phone_code }
            let data_to_edit = { ...values, event_id: event_id, phone: phone_code, id: `${contactToEdit.id}` }
            !contactToEdit.id ? createContact(data).then(res => {
                form.resetFields();
                updateContacts(res.data.data, false)
                setModalState(prev => ({ ...prev, visible: false, loading: false }))
            }) : updateSelectedContact(data_to_edit).then(res => {
                form.resetFields();
                updateContacts(res.data.data, true)
                setModalState(prev => ({ ...prev, visible: false, loading: false }))
            })
        });
    };

    const saveFormRef = (formRef) => {
        formReff = formRef;
    };
    useEffect(() => {
        return () => {
            // setModalState(prev => ({ ...prev, visible: contactModal_visible ? true : true }))
            setModalState(prev => ({ ...prev, visible: true }))
        }
    }, [contactModal_visible])
    useEffect(() => {
        let { form } = formReff.props;
        let country_code = contactToEdit && contactToEdit.phone.split('-')[0]
        let phone_number = contactToEdit && contactToEdit.phone.split('-')[1]
        let country = countries.countries.filter(item => item.code === country_code && item.name)
        let country_name = country[0] && country[0].name
        contactToEdit != '' ? ((() => {
            form.setFields({
                name: { value: contactToEdit.name },
                email: { value: contactToEdit.email },
                prefix: { value: country_name },
                phone: { value: phone_number }
            })
        })()) : ((() => {
            form.resetFields();
        })())
    }, [contactToEdit])
    return (
        <div>
            <CollectionCreateForm
                wrappedComponentRef={(formRef) => saveFormRef(formRef)}
                visible={visible}
                onCancel={handleCancel}
                onCreate={handleCreate}
                loading={loading}
                type =  { contactToEdit.type ? contactToEdit.type : 'Add'  }
            />
        </div>
    );
}

export default CreateContact;        