import React, { useState, useEffect } from 'react';
import { Dropdown, Menu, Icon, Button } from 'antd';
import * as moment from 'moment';
import { useSelector } from 'react-redux';
import { notifications } from '../../../../shared/toasterMessage';
import { adminRequestAction } from '../../../../modules/family/familyView/api-familyview';
import { invitationResp } from '../../../../modules/users/userProfile/api-profile'
const FamNotification = ({ content,
    history,
    navigateToPage,
    markNotificationAsRead,
    key_position,
    openPopover,
    markAsDelete,
    sub_type }) => {
    let User = useSelector(stateX => stateX.user_details)
    const [state, setState] = useState({
        body: undefined,
        category: undefined,
        cover_image: undefined,
        created_by_propic: undefined,
        created_by_user: undefined,
        description: undefined,
        device_type: undefined,
        from_id: undefined,
        link_to: undefined,
        message: '',
        message_title: '',
        notify_type: undefined,
        privacy_type: undefined,
        propic: '',
        r_id: undefined,
        sub_type: "",
        title: '',
        to_group_name: undefined,
        to_id: null,
        token: undefined,
        type: undefined,
        type_id: null,
        location: '',
        membercount: 0,
        visible_status: '',
        create_time: undefined,
        group_id: '',
    })
    let {
        // body,
        category,
        cover_image,
        //created_by_propic,
        created_by_user,
        //description,
        //device_type,
        from_id,
        link_to,
        message,
        //message_title,
        //notify_type,
        //privacy_type,
        //propic,
        //r_id,
        //sub_type,
        //title,
        //to_group_name,
        to_id,
        //token,
        type,
        type_id,
        location,
        membercount,
        visible_status,
        create_time,
        group_id
    } = state
    useEffect(() => {
        let {
            body,
            category,
            cover_image: cimg,
            created_by_propic,
            created_by_user: c_user,
            description,
            device_type,
            from_id,
            link_to,
            message: msg,
            message_title,
            notify_type,
            privacy_type,
            propic,
            r_id,
            sub_type: sub_Type,
            title,
            to_group_name,
            to_id,
            token,
            type,
            group_id: g_id,
            type_id, location: lc, membercount: mc, visible_status: vs, create_time: ct
        } = content
        setState(prev => ({
            ...prev,
            body: body,
            category: category,
            cover_image: cimg,
            created_by_propic: created_by_propic,
            created_by_user: c_user,
            description: description,
            device_type: device_type,
            from_id: from_id,
            link_to: link_to,
            message: msg,
            message_title: message_title,
            notify_type: notify_type,
            privacy_type: privacy_type,
            propic: propic,
            r_id: r_id,
            sub_type: sub_Type,
            title: title,
            to_group_name: to_group_name,
            to_id: to_id,
            token: token,
            type: type,
            type_id: type_id,
            location: lc,
            membercount: mc,
            visible_status: vs,
            create_time: ct,
            group_id: g_id
        }))
    }, [content])
    const displayExtraInfo = () => {
        // require('../../../assets/images/family_logo.png')
        return (
            <div style={{ display: 'flex', justifyContent: 'flex-start', flexWrap: 'nowrap' }}>
                <div >
                    <img
                        style={{
                            width: '12px',
                            borderRadius: '50%'
                        }}
                        src={require('../../../../assets/images/icon/location.png')}
                    />
                </div>
                <div
                    style={{
                        width: '200px',
                        margin: 'auto',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        marginLeft: '5px',
                        fontSize: '10px',
                        textAlign: 'left',
                        textOverflow: 'ellipsis'
                    }}>{`${location}`}
                </div>
            </div>
        )
    }
    const displaySubHeading = () => {
        return (
            <span><span style={{ fontSize: '12px' }}>{`By ${created_by_user}`}</span></span>
        )
    }
    const displayMainHeading = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'flex-start',
                }}>
                <span
                    style={{
                        fontSize: '15px',
                        color: '#1D1E1F',
                    }}>{`${message}`}
                </span>
            </div>
        )
    }
    const displayMembersCount = () => {
        return (
            <div>{content.membercount &&
                <span style={{ fontSize: '14px', fontWeight: 'bold' }}>
                    {content.membercount} members
                </span>}
            </div>
        )
    }
    const displayExtraButtonstoJoin = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    width: '40%',
                    justifyContent: 'space-between',
                    marginTop: '14px'
                }}
            >
                <div
                    style={{
                        width: '73px',
                        height: '28px',
                        border: '.8px solid #1FA152',
                        textAlign: 'center',
                        borderRadius: '3px'
                    }}
                >
                    <Button
                        onClick={(e) => _invitationResp(e, 'accepted')}
                        style={{
                            zIndex: '1',
                            width: '73px',
                            height: '28px',
                            color: '#1FA152',
                        }}
                    >ACCEPT</Button>
                </div>
                <div
                    style={{
                        width: '77px',
                        height: '28px',
                        border: '.8px solid #666A72',
                        textAlign: 'center',
                        borderRadius: '3px'
                    }}
                >
                    <Button
                        onClick={(e) => _invitationResp(e, 'rejected')}
                        style={{
                            zIndex: '1',
                            width: '77px',
                            height: '28px',
                            color: '#666A72',
                        }}
                    >REJECT</Button>
                </div>
            </div>
        )
    }
    const menu = (
        <Menu>
            <Menu.Item key="0" onClick={() => {
                markAsDelete(content)
            }}>
                Delete
          </Menu.Item>
        </Menu>
    );
    const clickedOnContent = () => {
        markNotificationAsRead(content, key_position)
        navigateToPage(history, content, openPopover)
    }
    const respondtoInvitation = (status) => {
        let param = {
            req_id: 1802,
            from_id: `${from_id}`,
            group_id: `${group_id}`,
            user_id: `${to_id}`,
            status: `${status}`
        }
        invitationResp(param).then((res) => {
            markNotificationAsRead(content, key_position)
        })
    }
    const adminRespondstoRequest = (param) => {
        adminRequestAction(param).then(res => {
            markNotificationAsRead(content, key_position)
        })
    }
    // accept/reject family join request by user
    const _invitationResp = (e, status) => {
        e.stopPropagation();
        switch (type) {
            case 'user':
                switch (category) {
                    case 'invitation':
                    case 'request':
                        respondtoInvitation(status)
                        break;
                }
            case 'family':
                switch (sub_type) {
                    case 'family_link':
                        let params = {
                            type: "fetch_link",
                            status: `${status}`,
                            responded_by: `${to_id}`,
                            from_group:`${from_id}`,
                            to_group:`${type_id}`
                        }
                        adminRespondstoRequest(params)
                    break;
                    case 'request':
                        let param = {
                            type: "request",
                            status: `${status}`,
                            responded_by: `${to_id}`,
                            user_id:`${from_id}`,
                            group_id:`${link_to}`
                        }
                        adminRespondstoRequest(param)
                    break;
                }
        }
    }
return (
    <div style={{ position: 'relative' }}>
        <div
            style={{
                width: '500px',
                display: 'flex',
                justifyContent: 'flex-start',
                marginTop: '1px',
                padding: '16px',
                backgroundColor: 'white',
            }}
            onClick={() => { !((message.includes('requested') || message.includes('invited')) && visible_status === 'unread') && clickedOnContent() }}
        >
            <div
                style={{
                    width: '15%',
                    height: '80%',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    overflow: 'hidden',
                    borderRadius: '5px'
                }}
            >
                <img
                    style={{
                        flexShrink: '0',
                        minWidth: '100%',
                        minHeight: '68px'
                    }}
                    onError={(e) => {
                        e.target.src = require("./../../../../assets/images/family_logo.png")
                    }}
                    src={(cover_image == "" || cover_image == null) ? require('./../../../../assets/images/family_logo.png') : process.env.REACT_APP_S3_BASE_URL + 'logo/' + cover_image} />
            </div>
            <div
                style={{
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    marginLeft: '10px',
                    // marginTop: '10px',
                    width: '85%'
                }}
            >
                {displayMainHeading()}
                {displaySubHeading()}
                {displayExtraInfo()}
                {displayMembersCount()}
                {
                    !(message.includes('joined')
                        || message.includes('approved')
                        || message.includes('accepted')
                        || message.includes('has added')
                        || visible_status === 'read')
                    &&
                    displayExtraButtonstoJoin()}
            </div>

        </div >
        <div style={{
            position: 'absolute',
            zIndex: '1',
            top: '10px',
            right: '10px'
        }}>
            <Dropdown
                overlay={menu} >
                <Icon
                    type="dash" rotate={90} />
            </Dropdown>
        </div>
        <div style={{
            position: 'absolute',
            zIndex: '1',
            bottom: '0px',
            right: '10px'
        }}>
            {moment(new Date(create_time)).toNow(true)}
        </div>
        {visible_status === 'unread' && <div style={{
            position: 'absolute',
            zIndex: '1',
            top: '15px',
            left: '15px',
            backgroundColor: 'green',
            width: '10px',
            height: '10px',
            borderRadius: '50%'
        }}>
        </div>}
    </div>
)
}

export default FamNotification