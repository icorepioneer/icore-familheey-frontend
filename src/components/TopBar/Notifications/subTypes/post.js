import React, { useState, useEffect } from 'react'
import { Icon, Dropdown, Menu } from 'antd'
import * as moment from 'moment'
const PostNotification = ({ content,openPopover,history,navigateToPage,markNotificationAsRead,key_position,markAsDelete }) => {
    const [state, setState] = useState({
        body: undefined,
        category: undefined,
        cover_image: undefined,
        created_by_propic: undefined,
        created_by_user: undefined,
        description: undefined,
        device_type: undefined,
        from_id: undefined,
        link_to: undefined,
        message: '',
        message_title: '',
        notify_type: undefined,
        privacy_type: undefined,
        propic: '',
        r_id: undefined,
        sub_type: "",
        title: '',
        to_group_name: undefined,
        to_id: null,
        token: undefined,
        type: undefined,
        type_id: null,
        visible_status:'',
        create_time:undefined
    })
    let {
        // body,
        //category,
        cover_image,
        created_by_propic,
        created_by_user,
        description,
        //device_type,
        //from_id,
        //link_to,
        message,
        //message_title,
        //notify_type,
        privacy_type,
        propic,
        //r_id,
        //sub_type,
        //title,
        to_group_name,
        //to_id,
        //token,
        //type,
        //type_id,
        visible_status,
        create_time
    } = state
    useEffect(() => {
        let {
            body,
            category,
            cover_image:cimg,
            created_by_propic,
            created_by_user:cuser,
            description:descp,
            device_type,
            from_id,
            link_to,
            message:msg,
            message_title,
            notify_type,
            privacy_type:pt,
            propic:_pic,
            r_id,
            sub_type,
            title,
            to_group_name:tgn,
            to_id,
            token,
            type,
            type_id,
            visible_status:vs,
            create_time:ct
        } = content
        setState(prev => ({
            ...prev,
            body: body,
            category: category,
            cover_image: cimg,
            created_by_propic: created_by_propic,
            created_by_user: cuser,
            description: descp,
            device_type: device_type,
            from_id: from_id,
            link_to: link_to,
            message: msg,
            message_title: message_title,
            notify_type: notify_type,
            privacy_type: pt,
            propic: _pic,
            r_id: r_id,
            sub_type: sub_type,
            title: title,
            to_group_name: tgn,
            to_id: to_id,
            token: token,
            type: type,
            type_id: type_id,
            visible_status:vs,
            create_time:ct
        }))
    }, [content])
    const menu = (
        <Menu>
            <Menu.Item key="0" onClick={() => {
                markAsDelete(content)
            }}>
                Delete
          </Menu.Item>
        </Menu>
    );
    const displayExtraInfo = () => {
        return (
            <div style={{ display: 'flex', justifyContent: 'flex-start', flexWrap: 'nowrap' }}>
                <div >
                    <img
                        style={{ 
                            width: '12px',
                            borderRadius:'50%'
                         }}
                        onError={(e) => { 
                            e.target.onerror = null; 
                            e.target.src = require("./../../../../assets/images/default_propic.png")
                         }}
                        src={(created_by_propic == ""||created_by_propic ==null)?require("./../../../../assets/images/default_propic.png"): process.env.REACT_APP_PRO_PIC_RESIZE + process.env.REACT_APP_S3_BASE_URL + 'propic/' + created_by_propic}
                    />
                </div>
                <div
                    style={{
                        margin: 'auto',
                        marginLeft: '5px',
                        marginRight: '5px',
                        marginTop: '5px',
                        marginBottom: '0',
                        fontSize: '10px',
                        width: 'auto',
                        textAlign: 'center',
                    }}>{`${created_by_user}  `}
                </div>
                {!(message.includes('added a message')||message.includes('started a conversation'))&&<div
                    style={{
                        margin: 'auto',
                        marginLeft: '0px',
                        marginRight: '5px',
                        marginTop: '5px',
                        marginBottom: '0',
                        fontSize: '10px',
                        width: 'auto',
                        textAlign: 'center'
                    }}
                >{`|${to_group_name ? to_group_name : privacy_type}`}</div>}
            </div>
        )
    }
    const displaySubHeading = () => {
        return (
            <div style={{ 'width': '100%', 'overflow': 'hidden', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis' }}>
                <span style={{ fontSize: '12px' }}>
                    {`${description}`}
                </span>
            </div>
        )
    }
    const displayMainHeading = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                <span
                    style={{
                        fontSize: '15px',
                        color: '#1D1E1F',
                    }}>{`${message}`}
                </span>
            </div>
        )
    }

    const clickedOnContent=()=>{
        markNotificationAsRead(content,key_position)
        navigateToPage(history,content,openPopover)
    }
    return (
        <div style={{ position: 'relative' }}>
            <div
            style={{
                width: '500px',
                height: '120px',
                display: 'flex',
                justifyContent: 'flex-start',
                marginTop: '1px',
                padding:'16px',
                backgroundColor:'white',
            }}
            onClick={()=>{clickedOnContent()}}
            >
            <div
                style={{
                    width: '15%',
                    height: '80%',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    overflow: 'hidden',
                    borderRadius: '5px'
                }}
            >
                <img
                    style={{
                        flexShrink: '0',
                        minWidth: '100%',
                        minHeight: '100%'
                    }}
                    onError={(e) => { 
                        e.target.onerror = null; 
                        e.target.src = require("./../../../../assets/images/family_logo.png")
                     }}
                    src={(cover_image == "" || cover_image == null) ? require('./../../../../assets/images/family_logo.png') : process.env.REACT_APP_S3_BASE_URL + 'post/' + cover_image} />
            </div>
            <div
                style={{
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    marginLeft: '10px',
                    // marginTop: '10px',
                    width: '85%'
                }}
            >
                {displayMainHeading()}
                { description!==""&&displaySubHeading()}
                {displayExtraInfo()}
            </div>

        </div >
        <div style={{
            position: 'absolute',
            zIndex: '1',
            top: '10px',
            right:'10px'
        }}>
        <Dropdown 
         overlay={menu} >
            <Icon
                type="dash" rotate={90} />
        </Dropdown>
    </div>
    <div style={{
                    position: 'absolute',
                    zIndex: '1',
                    bottom: '0px',
                    right:'10px'
                }}>
                    {moment(new Date(create_time)).toNow(true)}
            </div>
            {visible_status === 'unread'&&<div style={{
                    position: 'absolute',
                    zIndex: '1',
                    top: '15px',
                    left:'15px',
                    backgroundColor:'green',
                    width:'10px',
                    height:'10px',
                    borderRadius:'50%'
                }}>
            </div>}
        </div>
    )
}

export default PostNotification