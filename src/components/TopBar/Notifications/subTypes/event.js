import React, { useState, useEffect } from 'react'
import { Button, Icon, Dropdown, Menu } from 'antd'
import * as moment from 'moment'
import { responseToEvents } from '../../../../modules/events/eventDetails/api-eventDetails'
const EventNotification = ({ content, markNotificationAsRead, key_position, history, navigateToPage, markAsDelete, openPopover }) => {
    const [state, setState] = useState({
        body: undefined,
        category: undefined,
        cover_image: undefined,
        created_by_propic: undefined,
        created_by_user: undefined,
        description: undefined,
        device_type: undefined,
        from_id: undefined,
        link_to: undefined,
        message: '',
        message_title: '',
        notify_type: undefined,
        privacy_type: undefined,
        propic: '',
        r_id: undefined,
        sub_type: "",
        title: '',
        to_group_name: undefined,
        to_id: null,
        token: undefined,
        type: undefined,
        type_id: null,
        location: '',
        membercount: 0,
        from_date: undefined,
        rsvp: false,
        user_id: undefined,
        visible_status: '',
        event_type: undefined,
        create_time: undefined
    })
    let {
        // body,
        cover_image,
        created_by_user,
        from_id,
        message,
        sub_type,
        type_id,
        location,
        from_date,
        rsvp,
        user_id,
        visible_status,
        event_type,
        create_time
    } = state
    useEffect(() => {
        let {
            body,
            category,
            cover_image: cimage,
            created_by_propic,
            created_by_user: cuser,
            description,
            device_type,
            from_id: fid,
            link_to,
            message: msg,
            message_title,
            notify_type,
            privacy_type,
            propic,
            r_id,
            sub_type: st,
            title,
            to_group_name,
            to_id,
            token,
            type,
            type_id: tid, event_type: et, location: lc, membercount, from_date: f_date, rsvp: _rsvp, user_id: uid, visible_status: vs, create_time: ct
        } = content
        setState(prev => ({
            ...prev,
            body: body,
            category: category,
            cover_image: cimage,
            created_by_propic: created_by_propic,
            created_by_user: cuser,
            description: description,
            device_type: device_type,
            from_id: fid,
            link_to: link_to,
            message: msg,
            message_title: message_title,
            notify_type: notify_type,
            privacy_type: privacy_type,
            propic: propic,
            r_id: r_id,
            sub_type: st,
            title: title,
            to_group_name: to_group_name,
            to_id: to_id,
            token: token,
            type: type,
            type_id: tid,
            location: lc,
            membercount: membercount,
            from_date: f_date,
            rsvp: _rsvp,
            user_id: uid,
            visible_status: vs,
            event_type: et,
            create_time: ct
        }))
    }, [content])

    const responseToEvent = (type, flag = 0) => {
        markNotificationAsRead(content, key_position)
        var params = {
            event_id: `${type_id}`,
            user_id: `${user_id}`,
            resp: type,
            others_count: 0,
            created_by: from_id
        };
        responseToEvents(params).then((response) => {
            setState(prev => ({ ...prev, rsvp: false, visible_status: 'read' }))
        }).catch((error) => {
            console.log(error)
        });
    }
    const displayExtraInfo = () => {
        // require('../../../assets/images/family_logo.png')
        return (
            <div style={{ display: 'flex', justifyContent: 'flex-start', flexWrap: 'nowrap' }}>
                <div style={{ fontSize: '12px', marginRight: '5px', marginTop: '2px', width: '80px' }}>
                    <span>{moment.unix(from_date).format("DD MMM YYYY")}</span>
                </div>
                <div >
                    <img
                        style={{
                            width: '12px',
                            borderRadius: '50%'
                        }}
                        src={require('../../../../assets/images/icon/location.png')}
                    />
                </div>
                <div
                    style={{
                        width: '140px',
                        margin: 'auto',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        marginLeft: '5px',
                        fontSize: '10px',
                        textAlign: 'left',
                        textOverflow: 'ellipsis'
                    }}>{`${location ? location : event_type}`}
                </div>
            </div>
        )
    }
    const displaySubHeading = () => {
        return (
            <span><span style={{ fontSize: '12px' }}>{`By ${created_by_user}`}</span></span>
        )
    }
    const displayMainHeading = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'flex-start',
                }}>
                <span
                    style={{
                        fontSize: '15px',
                        color: '#1D1E1F',
                    }}>{`${message}`}
                </span>
            </div>
        )
    }
    const displayExtraButtonstoJoin = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    marginTop: '14px'
                }}
            >
                <div
                    style={{
                        width: '63px',
                        height: '28px',
                        border: '.8px solid #1FA152',
                        textAlign: 'center',
                        borderRadius: '3px'
                    }}
                >
                    <Button
                        onClick={() => responseToEvent('going')}
                        style={{
                            width: '63px',
                            height: '28px',
                            color: '#1FA152',
                        }}
                    >GOING</Button>
                </div>
                <div
                    style={{
                        width: '100px',
                        height: '28px',
                        border: '.8px solid #666A72',
                        textAlign: 'center',
                        borderRadius: '3px'
                    }}
                >
                    <Button
                        onClick={() => responseToEvent('interested')}
                        style={{
                            width: '100px',
                            height: '28px',
                            color: '#666A72',
                        }}
                    >INTERESTED</Button>
                </div>
                <div
                    style={{
                        width: '120px',
                        height: '28px',
                        border: '.8px solid #666A72',
                        textAlign: 'center',
                        borderRadius: '3px'
                    }}
                >
                    <Button
                        onClick={() => responseToEvent('not-going')}
                        style={{
                            width: '120px',
                            height: '28px',
                            color: '#666A72',
                        }}
                    >NOT INTERESTED</Button>
                </div>
            </div>
        )
    }

    const menu = (
        <Menu>
            <Menu.Item key="0" onClick={() => {
                markAsDelete(content)
            }}>
                Delete
          </Menu.Item>
        </Menu>
    );

    const clickedOnContent = () => {
        markNotificationAsRead(content, key_position)
        navigateToPage(history, content, openPopover)

    }
    return (
        <div style={{ position: 'relative' }}>
            <div
                style={{
                    width: '500px',
                    display: 'flex',
                    justifyContent: 'flex-start',
                    marginTop: '1px',
                    padding: '16px',
                    backgroundColor: 'white',
                }}
                onClick={() => {
                    // !(rsvp && sub_type === "") && clickedOnContent()
                    clickedOnContent()
                }}
            >
                <div
                    style={{
                        width: '15%',
                        height: '80%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        overflow: 'hidden',
                        borderRadius: '5px'
                    }}
                >
                    <img
                        style={{
                            flexShrink: '0',
                            minWidth: '100%',
                            minHeight: '100%'
                        }}
                        onError={(e) => {
                            e.target.src = require("./../../../../assets/images/family_logo.png")
                        }}
                        src={(cover_image == "" || cover_image == null) ? require('./../../../../assets/images/family_logo.png') : process.env.REACT_APP_S3_BASE_URL + 'post/' + cover_image} />
                </div>
                <div
                    style={{
                        justifyContent: 'flex-start',
                        flexDirection: 'column',
                        marginLeft: '10px',
                        // marginTop: '10px',
                        width: '85%'
                    }}
                >
                    {displayMainHeading()}
                    {displayExtraInfo()}
                    {displaySubHeading()}
                    {(rsvp && sub_type === "" && message.includes('invited')) && visible_status != "read" && <div
                        style={{
                            marginTop: '14px',
                            height: '30px',
                            width: '95%',
                        }}
                    >
                    </div>}
                    {/* {(rsvp && sub_type === "" && message.includes('invited')) && visible_status != "read" && displayExtraButtonstoJoin()} */}

                </div>
            </div >
            <div
                style={{
                    position: 'absolute',
                    zIndex: '1',
                    top: '10px',
                    right: '10px'
                }}
            >
                <Dropdown overlay={menu} trigger={['click']} >
                    <Icon
                        type="dash" rotate={90} />
                </Dropdown>
            </div>
            <div style={{
                position: 'absolute',
                zIndex: '1',
                bottom: '0px',
                right: '10px'
            }}>
                {moment(new Date(create_time)).toNow(true)}
            </div>
            {visible_status === 'unread' && <div style={{
                position: 'absolute',
                zIndex: '1',
                top: '15px',
                left: '15px',
                backgroundColor: 'green',
                width: '10px',
                height: '10px',
                borderRadius: '50%'
            }}>
            </div>}
            {(rsvp && sub_type === "" && message.includes('invited')) && visible_status != "read" &&
                <div style={{
                    position: 'absolute',
                    zIndex: '1',
                    top: '80px',
                    bottom: '0px',
                    left: '90px',
                    // marginTop:'10px'
                }}>
                    {displayExtraButtonstoJoin()}
                </div>
            }
        </div>
    )
}

export default EventNotification