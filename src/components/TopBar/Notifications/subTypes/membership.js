import React, { useState, useEffect } from 'react'
import { Button, Icon, Dropdown, Menu } from 'antd'
import * as moment from 'moment'
const MembershipNotification = ({ content, markNotificationAsRead, key_position, history, openPopover,navigateToPage, markAsDelete }) => {
    const [state, setState] = useState({
        body: undefined,
        category: undefined,
        cover_image: undefined,
        created_by_propic: undefined,
        created_by_user: undefined,
        description: undefined,
        device_type: undefined,
        from_id: undefined,
        link_to: undefined,
        message: '',
        message_title: '',
        notify_type: undefined,
        privacy_type: undefined,
        propic: '',
        r_id: undefined,
        sub_type: "",
        title: '',
        to_group_name: undefined,
        to_id: null,
        token: undefined,
        type: undefined,
        type_id: null,
        location: '',
        membercount: 0,
        from_date: undefined,
        rsvp: false,
        user_id: undefined,
        visible_status: '',
        create_time:undefined
    })
    let {
        // body,
        //category,
        cover_image,
        //created_by_propic,
        created_by_user,
        //description,
        //device_type,
        //from_id,
        //link_to,
        message,
        //message_title,
        //notify_type,
        //privacy_type,
        //propic,
        //r_id,
        //sub_type,
        //title,
        //to_group_name,
        //to_id,
        //token,
        //type,
        //type_id,
        location,
        //membercount,
        //from_date,
        //rsvp,
        //user_id,
        visible_status,
        create_time
    } = state
    useEffect(() => {
        let {
            body,
            category,
            cover_image:cimg,
            created_by_propic,
            created_by_user:cuser,
            description,
            device_type,
            from_id,
            link_to,
            message:msg,
            message_title,
            notify_type,
            privacy_type,
            propic,
            r_id,
            sub_type,
            title,
            to_group_name,
            to_id,
            token,
            type,
            type_id, location:loc,create_time:ct, membercount, from_date, rsvp, user_id, visible_status:vs
        } = content
        setState(prev => ({
            ...prev,
            body: body,
            category: category,
            cover_image: cimg,
            created_by_propic: created_by_propic,
            created_by_user: cuser,
            description: description,
            device_type: device_type,
            from_id: from_id,
            link_to: link_to,
            message: msg,
            message_title: message_title,
            notify_type: notify_type,
            privacy_type: privacy_type,
            propic: propic,
            r_id: r_id,
            sub_type: sub_type,
            title: title,
            to_group_name: to_group_name,
            to_id: to_id,
            token: token,
            type: type,
            type_id: type_id,
            location: loc,
            membercount: membercount,
            from_date: from_date,
            rsvp: rsvp,
            user_id: user_id,
            visible_status: vs,
            create_time:ct
        }))
    }, [content])

    const displayExtraInfo = () => {
        // require('../../../assets/images/family_logo.png')
        return (
            <div style={{ display: 'flex', justifyContent: 'flex-start', flexWrap: 'nowrap' }}>
                <div >
                    <img
                        style={{
                            width: '12px',
                            borderRadius: '50%'
                        }}
                        src={require('../../../../assets/images/icon/location.png')}
                    />
                </div>
                <div
                    style={{
                        margin: 'auto',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        marginLeft: '5px',
                        fontSize: '10px',
                        textAlign: 'left',
                        textOverflow: 'ellipsis'
                    }}>{`${location}`}
                </div>
            </div>
        )
    }
    const displaySubHeading = () => {
        return (
            <span><span style={{ fontSize: '12px' }}>{`By ${created_by_user}`}</span></span>
        )
    }
    const displayMainHeading = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'flex-start',
                }}>
                <span
                    style={{
                        fontSize: '15px',
                        color: '#1D1E1F',
                    }}>{`${message}`}
                </span>
            </div>
        )
    }

    const menu = (
        <Menu>
            <Menu.Item key="0" onClick={() => {
                markAsDelete(content)
            }}>
                Delete
          </Menu.Item>
        </Menu>
    );

    const displayExtraButtonstoJoin = () => {
        return (
            <div
                style={{
                    display: 'flex',
                    width: '55%',
                    justifyContent: 'space-between',
                    marginTop: '14px'
                }}
            >
                <div
                    style={{
                        width: '110px',
                        height: '28px',
                        border: '.8px solid #1FA152',
                        textAlign: 'center',
                        borderRadius: '3px',
                        fontSize: '12px'
                    }}
                >
                    <Button
                        style={{
                            width: '110px',
                            height: '28px',
                            color: '#1FA152',
                        }}
                    >PAY NOW</Button>
                </div>
            </div>
        )
    }
    const clickedOnContent = () => {
        markNotificationAsRead(content, key_position)
        navigateToPage(history, content,openPopover)
    }
    return (
        <div style={{ position: 'relative' }}>
            <div
                style={{
                    width: '500px',
                    display: 'flex',
                    justifyContent: 'flex-start',
                    marginTop: '1px',
                    backgroundColor: 'white',
                    padding: '16px'
                }}
                onClick={() => { clickedOnContent() }}
            >
                <div
                    style={{
                        width: '15%',
                        height: '80%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        overflow: 'hidden',
                        borderRadius: '5px'
                    }}
                >
                    <img
                        style={{
                            flexShrink: '0',
                            minWidth: '100%',
                            minHeight: '100%'
                        }}
                        onError={(e) => {
                            e.target.src = require("./../../../../assets/images/family_logo.png")
                        }}
                        src={(cover_image == "" || cover_image == null) ? require('./../../../../assets/images/family_logo.png') : process.env.REACT_APP_S3_BASE_URL + 'post/' + cover_image} />
                </div>
                <div
                    style={{
                        justifyContent: 'flex-start',
                        flexDirection: 'column',
                        marginLeft: '10px',
                        // marginTop: '10px',
                        width: '85%'
                    }}
                >
                    {displayMainHeading()}
                    {displaySubHeading()}
                    {displayExtraInfo()}
                    {/* {message.includes('renew') && displayExtraButtonstoJoin()} */}
                </div>

            </div >
            <div style={{
                position: 'absolute',
                zIndex: '1',
                top: '10px',
                right: '10px'
            }} >
                <Dropdown overlay={menu} trigger={['click']} >
                    <Icon
                        type="dash" rotate={90} />
                </Dropdown>
            </div>
            <div style={{
                    position: 'absolute',
                    zIndex: '1',
                    bottom: '0px',
                    right:'10px'
                }}>
                    {moment(new Date(create_time)).toNow(true)}
            </div>
            {visible_status === 'unread'&&<div style={{
                    position: 'absolute',
                    zIndex: '1',
                    top: '15px',
                    left:'15px',
                    backgroundColor:'green',
                    width:'10px',
                    height:'10px',
                    borderRadius:'50%'
                }}>
            </div>}
        </div>
    )
}

export default MembershipNotification