import React, { useState, useEffect } from 'react'
import PostNotification from './subTypes/post'
import FamNotification from './subTypes/family'
import NeedNotification from './subTypes/need'
import EventNotification from './subTypes/event'
import MembershipNotification from './subTypes/membership'
const NotificationRow = ({ content, markNotificationAsRead, index, navigateToPage, history, markAsDelete, openPopover }) => {
    // console.log(index)
    const [state, setState] = useState({
        category: undefined,
        type: undefined,
        sub_type: undefined
    })
    let {
        category, type, sub_type
    } = state
    useEffect(() => {
        if (content) {
            let {
                category: ct, type: _type, sub_type: st
            } = content
            setState(prev => ({
                ...prev,
                type: _type,
                category: ct,
                sub_type: st
            }))
        }
    }, [content])

    const deterMineNotificationUI = () => {
        switch (type) {
            //Added to display notification if some one commented on a post: Anand 06/02/2021
            case 'post':
                if (sub_type === "conversation") {
                    return (
                        <PostNotification
                            content={content}
                            markNotificationAsRead={markNotificationAsRead}
                            key_position={index}
                            navigateToPage={navigateToPage}
                            history={history}
                            conversation={true}
                            markAsDelete={markAsDelete}
                            openPopover={openPopover}
                        />
                    )
                }
                /////////////////////////////////////////////////
            case 'user':
                if (sub_type === "request") {
                    return (
                        <FamNotification
                            content={content}
                            markNotificationAsRead={markNotificationAsRead}
                            key_position={index}
                            navigateToPage={navigateToPage}
                            history={history}
                            markAsDelete={markAsDelete}
                            openPopover={openPopover}
                            sub_type={sub_type}
                        />
                    )
                }
            case 'family':
                switch (sub_type) {
                    case '':
                    case "family_link":
                    case 'request':
                    case "fetch_link":
                        return (
                            <FamNotification
                                content={content}
                                markNotificationAsRead={markNotificationAsRead}
                                key_position={index}
                                navigateToPage={navigateToPage}
                                history={history}
                                markAsDelete={markAsDelete}
                                openPopover={openPopover}
                                sub_type={sub_type}
                            />
                        )
                    case 'about':
                    case "member":
                        return (
                            <MembershipNotification
                                content={content}
                                markNotificationAsRead={markNotificationAsRead}
                                key_position={index}
                                navigateToPage={navigateToPage}
                                history={history}
                                markAsDelete={markAsDelete}
                                openPopover={openPopover}
                            />
                        )
                }
            case 'home':
                switch (sub_type) {
                    case "familyfeed":
                        return (
                            <PostNotification
                                content={content}
                                markNotificationAsRead={markNotificationAsRead}
                                key_position={index}
                                navigateToPage={navigateToPage}
                                history={history}
                                conversation={true}
                                markAsDelete={markAsDelete}
                                openPopover={openPopover}
                            />
                        )
                    case 'requestFeed':
                        return (
                            <NeedNotification
                                content={content}
                                markNotificationAsRead={markNotificationAsRead}
                                key_position={index}
                                navigateToPage={navigateToPage}
                                history={history}
                                markAsDelete={markAsDelete}
                                openPopover={openPopover}
                            />
                        )
                }
            case "announcement":
                switch (sub_type) {
                    case '':
                        return (
                            <PostNotification
                                content={content}
                                markNotificationAsRead={markNotificationAsRead}
                                key_position={index}
                                navigateToPage={navigateToPage}
                                history={history}
                                conversation={true}
                                markAsDelete={markAsDelete}
                                openPopover={openPopover}
                            />
                        )
                }
            case "topic":
                switch (sub_type) {
                    case 'topic':
                    case "conversation":
                        return (
                            <PostNotification
                                content={content}
                                markNotificationAsRead={markNotificationAsRead}
                                key_position={index}
                                navigateToPage={navigateToPage}
                                history={history}
                                conversation={true}
                                markAsDelete={markAsDelete}
                                openPopover={openPopover}
                            />
                        )
                }
            case 'event':
                switch (sub_type) {
                    case '':
                    case "guest_attending":
                    case "guest_interested":
                        return (
                            <EventNotification
                                content={content}
                                markNotificationAsRead={markNotificationAsRead}
                                key_position={index}
                                navigateToPage={navigateToPage}
                                history={history}
                                markAsDelete={markAsDelete}
                                openPopover={openPopover}
                            />
                        )
                }
        }
    }
    // const deterMineNotificationUI = () => {
    //     if (category === "") {
    //         switch (type) {
    //             case "event":
    //                 return <EventNotification
    //                     content={content}
    //                     markNotificationAsRead={markNotificationAsRead}
    //                     key_position={index}
    //                     navigateToPage={navigateToPage}
    //                     history={history}
    //                     markAsDelete={markAsDelete}
    //                     openPopover={openPopover}
    //                 />
    //         }
    //     }
    //     else {
    //         switch (category) {
    //             case "Notifications":
    //                 switch (sub_type) {
    //                     case "requestFeed":
    //                         return <NeedNotification
    //                             content={content}
    //                             markNotificationAsRead={markNotificationAsRead}
    //                             key_position={index}
    //                             navigateToPage={navigateToPage}
    //                             history={history}
    //                             markAsDelete={markAsDelete}
    //                             openPopover={openPopover}
    //                         />
    //                 }
    //                 break;
    //             case "event":
    //             case 'rsvp':
    //                 return <EventNotification
    //                     content={content}
    //                     markNotificationAsRead={markNotificationAsRead}
    //                     key_position={index}
    //                     navigateToPage={navigateToPage}
    //                     history={history}
    //                     markAsDelete={markAsDelete}
    //                     openPopover={openPopover}
    //                 />
    //             case "create_topic":
    //             case 'conversation':
    //             case 'post_create':
    //             case 'post_share':
    //             case 'announcement_create':
    //                 
    //             case 'invitation':
    //             case 'family':
    //                 return (
    //                     <FamNotification
    //                         content={content}
    //                         markNotificationAsRead={markNotificationAsRead}
    //                         key_position={index}
    //                         navigateToPage={navigateToPage}
    //                         history={history}
    //                         markAsDelete={markAsDelete}
    //                         openPopover={openPopover}
    //                         sub_type={sub_type}
    //                     />
    //                 )
    //             case 'create_need':
    //             case "contribution_create":
    //             case 'request':
    //                 return (
    //                     <NeedNotification
    //                         content={content}
    //                         markNotificationAsRead={markNotificationAsRead}
    //                         key_position={index}
    //                         navigateToPage={navigateToPage}
    //                         history={history}
    //                         markAsDelete={markAsDelete}
    //                         openPopover={openPopover}
    //                     />
    //                 )
    //             case 'membership':
    //             case 'Familheey Membership reminder':
    //                 return (
    //                     <MembershipNotification
    //                         content={content}
    //                         markNotificationAsRead={markNotificationAsRead}
    //                         key_position={index}
    //                         navigateToPage={navigateToPage}
    //                         history={history}
    //                         markAsDelete={markAsDelete}
    //                         openPopover={openPopover}
    //                     />
    //                 )
    //         }
    //     }
    // }
    return (
        <React.Fragment>
            {deterMineNotificationUI()}
        </React.Fragment>
    )
}

export default NotificationRow