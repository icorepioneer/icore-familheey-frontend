import React from 'react'
import { Modal, Button, Checkbox, Input } from 'antd'


let { Search } = Input
const FamilySelectModal = ({
    visible,
    families,
    toggleMOdal,
    chosenFam,
    //fetchFamilyList,
    selectAllClicked,
    modNotFamArray,
    setSearch,
    searchString,
    DeselectAllClicked
}) => {
    // const [state, setState] = useState({
    //     modalFams: []
    // })
    // let {  modalFarms } = state
    const addtoArray = (e, data) => {
        modNotFamArray(e.target.checked, data)
    }
    return (
        <React.Fragment>
            <Modal
                maskClosable={true}
                visible={visible}
                footer={[
                    <Button key={1} onClick={() => toggleMOdal()}>Done</Button>
                ]}
                title="Select specific families for notification"
                onCancel={toggleMOdal}
                style={{
                    maxHeight: '600px'
                }}
            >
                <div>
                    {families.length > 0 && <Search
                        id="searchFam"
                        placeholder="search your family here"
                        style={{ marginBottom: '10px' }}
                        value={searchString}
                        onChange={e => setSearch(e)}
                    />}<div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between'
                        }}
                    >
                        <span
                            style={{
                                fontWeight: 'bold'
                            }}
                        >{`${chosenFam.length} Selected`}</span>
                        <Button
                            disabled ={searchString === null?false:searchString.length>0}
                            onClick={() => { chosenFam.length==families.length?DeselectAllClicked():selectAllClicked() }}
                            style={{
                                border: 'none',
                                color: '#4bae50'
                            }}
                        >{chosenFam.length==families.length?`Deselect all`:`Select all`}</Button>
                    </div>
                    <div
                        style={{
                            maxHeight: '450px',
                            overflow: 'auto'
                        }}
                    >

                        {families.map((data, key) => {
                            return <React.Fragment key={key}>
                                <div
                                    key={key}
                                    style={{
                                        width: '98%',
                                        height: '100px',
                                        margin: 'auto',
                                        display: 'flex',
                                        boxShadow: '0 1px 25px rgba(40, 53, 86, 0.1)',
                                        padding: '10px',
                                        marginTop: '10px'
                                    }}>
                                    {/* image */}
                                    <div
                                        style={{
                                            width: '20%',
                                            height: '95%',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            overflow: 'hidden',
                                            borderRadius: '5px'
                                        }}
                                    >
                                        <img
                                            style={{
                                                flexShrink: '0',
                                                minWidth: '100%',
                                                minHeight: '100%'
                                            }}
                                            onError={(e) => {
                                                e.target.src = require("../../../../assets/images/family_logo.png")
                                            }}
                                            src="null"
                                            src={(data.logo == "" || data.logo == null) ?
                                                require('./../../../../assets/images/family_logo.png')
                                                : process.env.REACT_APP_S3_BASE_URL + 'logo/' + data.logo}
                                        />
                                    </div>
                                    {/* details */}
                                    <div
                                        style={{
                                            // flexDirection:'column',
                                            marginLeft: '5px',
                                            flexGrow: '1'
                                        }}>
                                        <h6 style={{ marginBottom: '0' }}>{data.group_name}</h6>
                                        <div>{`By ${data.created_by}`}</div>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'flex-start',
                                            }}>
                                            <div >
                                                <img
                                                    style={{
                                                        width: '12px',
                                                        borderRadius: '50%',
                                                        marginRight: '5px'
                                                    }}
                                                    src={require('../../../../assets/images/icon/location.png')}
                                                />
                                            </div>
                                            <div
                                                style={{
                                                    width: '240px',
                                                    // margin: 'auto',
                                                    whiteSpace: 'nowrap',
                                                    overflow: 'hidden',
                                                    // marginLeft: '5px',
                                                    // fontSize: '10px',
                                                    // textAlign: 'center',
                                                    textOverflow: 'ellipsis'
                                                }}
                                            >{`${data.base_region}`}</div>
                                        </div>
                                    </div>
                                    <div>
                                        <Checkbox
                                            onChange={(e) => {
                                                addtoArray(e, data)
                                            }}
                                            checked={chosenFam.includes(data.id)}
                                        ></Checkbox>
                                    </div>
                                </div>
                            </React.Fragment>
                        })}
                    </div>
                </div>
            </Modal>
        </React.Fragment>
    )
}

export default FamilySelectModal