import React, { useEffect, useState } from 'react'
import { Switch, Button, Icon } from 'antd'
import { useSelector } from 'react-redux';
import { getNotSettings, updateUserDetails2 } from '../../../modules/users/userProfile/api-profile'
import { viewFamily } from '../../../modules/family/familyList/api-familyList'
import FamilySelectModal from './Modal/familyModal'
import '../settings-style.scss'
const SettingsPage = ({ history }) => {
    const [state, setNotificationState] = useState({
        user_id: useSelector(stateX => stateX.user_details.userId),
        announcement_notification: false,
        conversation_notification: false,
        event_notification: false,
        family_notification_off: [],
        family_notification_off_status: true,
        notification: false,
        public_notification: false,
        families: [],
        familySelection_modalVisible: false,
        loader: false,
        searchString:null
    })
    let { user_id,
        announcement_notification,
        conversation_notification,
        event_notification,
        familySelection_modalVisible,
        family_notification_off,
        family_notification_off_status,
        notification,
        public_notification,
        families,
        loader,
        searchString
    } = state
    useEffect(() => {
        getNotificationSettings()
        fetchFamilyList()
    }, [])
    const getNotificationSettings = () => {
        let params = {
            user_id: `${user_id}`
        }
        getNotSettings(params).then(res => {
            let { data } = res
            let { announcement_notification:a_not, conversation_notification:c_not, event_notification:e_not, family_notification_off:f_not, notification:notif, public_notification:p_notif } = data[0]
            setNotificationState(prev => ({
                ...prev,
                announcement_notification: a_not,
                conversation_notification: c_not,
                event_notification: e_not,
                family_notification_off: f_not,
                notification: notif,
                public_notification: p_notif,
                // family_notification_off_status: family_notification_off.length > 0
            }))
        })
    }
    const fetchFamilyList = (query = "") => {
        let params = {
            user_id: `${user_id}`,
            query: query
        }
        viewFamily(params).then(res => {
            let { data } = res
            let { pending_count, ...rem } = data
            let new_array = rem.data.map(item=>item.id)
            setNotificationState(prev => ({ ...prev, families: rem.data,family_notification_off:new_array }))
        })
    }
    const ChangeFlag = (e, flag) => {
        setNotificationState(prev => ({ ...prev, [`${flag}`]: e }))
    }
    useEffect(() => {
        !family_notification_off_status && setNotificationState(prev => ({ ...prev, familySelection_modalVisible: true, family_notification_off_status: true }))
    }, [family_notification_off_status])

    const toggleMOdal = () => {
        setNotificationState(prev => ({ ...prev, familySelection_modalVisible: !familySelection_modalVisible }))
    }

    const modNotFamArray = (flag, data) => {
        let newfamily_notification_off = family_notification_off
        if (flag) {
            newfamily_notification_off = [...newfamily_notification_off, data.id]
            setNotificationState(prev => ({ ...prev, family_notification_off: newfamily_notification_off }))
        }
        else {
            let id = data.id
            let rem = newfamily_notification_off.filter(_data => _data != id)
            setNotificationState(prev => ({ ...prev, family_notification_off: rem }))

        }
    }
    const selectAllClicked = () => {
        let newArray = []
        families.map(data => {
            newArray.push(data.id)
        })
        setNotificationState(prev => ({ ...prev, family_notification_off: newArray }))
    }
    const DeselectAllClicked = () => {
        let newArray = []
        setNotificationState(prev => ({ ...prev, family_notification_off: newArray }))
    }
    const updateFamilySettings = () => {
        setNotificationState(prev => ({ ...prev, loader: true }))
        let params = {
            id: `${user_id}`,
            announcement_notification: `${announcement_notification}`,
            conversation_notification: `${conversation_notification}`,
            event_notification: `${event_notification}`,
            family_notification_off: family_notification_off,
            notification: `${notification}`,
            public_notification: `${public_notification}`,

        }
        updateUserDetails2(params).then(res => {
            getNotificationSettings()
            setNotificationState(prev => ({ ...prev, loader: false }))
        }).catch(err => {
            console.log(err)
        })
    }
    useEffect(() => {
            let timer_input;
            let discover = document.getElementById("searchFam")
            discover && discover.addEventListener("keyup", () => {
                clearTimeout(timer_input)
                timer_input = setTimeout(() => {
                    fetchFamilyList(discover.value)
                }, 1300)
            })
    }, [searchString])
    const setSearch = (e) => {
        e.persist()
        setNotificationState(prev => ({ ...prev, searchString: e.target.value }))
    }
    const goback = () =>{
        history.push('/post', { type_id: 1 })
    }
    return (
        <React.Fragment>
            <div
                style={{
                    width: '600px',
                    height: 'auto',
                    backgroundColor: '#FFF',
                    margin: 'auto',
                    marginTop: '80px',
                    padding: '20px'
                }}
            >
                <div>
                    <Button type="link" onClick={goback}>
                        <Icon type="arrow-left" /> Back
                    </Button>
                </div>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '20px'
                    }}
                >
                    <div style={{ width: '300px' }}><span>All Notifications</span></div>
                    <div style={{ float: 'right' }}><Switch checked={notification} onChange={(e) => ChangeFlag(e, `notification`)} /></div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '20px'
                    }}
                >
                    <div style={{ width: '300px' }}><span>Public Notifications</span></div>
                    <div style={{ float: 'right' }}><Switch checked={public_notification} onChange={(e) => ChangeFlag(e, `public_notification`)} /></div>
                </div><div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '20px'
                    }}
                >
                    <div style={{ width: '300px' }}><span>Conversation Notifications</span></div>
                    <div style={{ float: 'right' }}>
                        <Switch
                            checked={conversation_notification}
                            onChange={(e) => ChangeFlag(e, `conversation_notification`)}
                        /></div>
                </div>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '20px'
                    }}
                >
                    <div style={{ width: '300px' }}><span>Selected family Notifications</span></div>
                    <div style={{ float: 'right' }}>
                        <Switch
                            checked={family_notification_off_status}
                            onChange={(e) => ChangeFlag(e, `family_notification_off_status`)}
                        /></div>
                </div><div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '20px'
                    }}
                >
                    <div style={{ width: '300px' }}><span>Announcements</span></div>
                    <div style={{ float: 'right' }}><Switch checked={announcement_notification} onChange={(e) => ChangeFlag(e, `announcement_notification`)} /></div>
                </div><div
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        padding: '20px'
                    }}
                >
                    <div style={{ width: '300px' }}><span>Event Notifications</span></div>
                    <div style={{ float: 'right' }}><Switch checked={event_notification} onChange={(e) => ChangeFlag(e, `event_notification`)} /></div>
                </div>
                <div
                    style={{ display: 'flex', flexDirection: 'row-reverse' }}
                >
                    <Button loading={loader} onClick={() => updateFamilySettings()}>{`${loader?'Updating...':'Update'}`}</Button>
                </div>
            </div>
            {familySelection_modalVisible && <FamilySelectModal
                visible={familySelection_modalVisible}
                families={families}
                toggleMOdal={toggleMOdal}
                chosenFam={family_notification_off}
                fetchFamilyList={fetchFamilyList}
                modNotFamArray={modNotFamArray}
                selectAllClicked={selectAllClicked}
                DeselectAllClicked={DeselectAllClicked}
                setSearch={setSearch}
                searchString={searchString}
            />}
        </React.Fragment>
    )
}


export default SettingsPage