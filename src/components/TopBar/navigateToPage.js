import { encryption } from '../../shared/encryptDecrypt'
const navigateToPage = (history, item, openPopover) => {
    let { sub_type, type, link_to } = item;
    let encrypted_id = encryption(link_to);
    switch (type) {
        case "topic":
            switch (sub_type) {
                case "conversation":
                case "topic":
                    history.push(`/topics/message`, { messageId: link_to });
                    break;
            }
            break;
        case "conversation":
            switch (sub_type) {
                case "conversation":
                    history.push(`/topics/message`, { messageId: link_to });
                    break;
            }
            break;
        case 'family':
            switch (sub_type) {
                case 'request':
                    history.push(`/familyview/${encrypted_id}/3/11`);
                    break;
                case 'family_link':
                    history.push(`/familyview/${encrypted_id}/7/14`);
                    break;
                default:
                    history.push(`/familyview/${encrypted_id}`);
                    break;
            }
            break;
        case 'user':
            switch (sub_type) {
                case 'request':
                    history.push(`/user-details/${encrypted_id}/8`, { prev: window.location.pathname });
                    break;
                default:
                    history.push(`/user-details/${encrypted_id}`, { prev: window.location.pathname });
                    break;
            }
            break;
        case 'home':
            switch (sub_type) {
                case 'familyfeed':
                    console.log(encrypted_id)
                    history.push(`/post_details/${encrypted_id}`);
                    break;
                case 'publicfeed':
                    history.push(`/post_details/${encrypted_id}`);
                    break;
                case "requestFeed":
                    history.push(`/request-details`, { post_request_id: link_to });
                    break;
                default:
                    history.push(`/post_details/${encrypted_id}`);
                    break;
            }
            break;
        case 'announcement':
            switch (sub_type) {
                default:
                    history.push('/individualStatus', { fromStory: { post_id: link_to, prev_url: `${window.location.pathname}` } });
                    break;
            }
            break;
        case 'post':
            history.push(`/post_details/${encrypted_id}`);
            break;
        case 'event':
            switch (sub_type) {
                case 'guest_interested':
                    history.push(`/event-details/${encrypted_id}/3`);
                    break;
                case 'guest_attending':
                    history.push(`/event-details/${encrypted_id}/3`);
                    break;
                case 'calendar':
                    history.push(`/calender`);
                    break;
                default:
                    history.push(`/event-details/${encrypted_id}`);
                    break;
            }
            break;
        default:
            break;
    }
    setTimeout(() => {
        openPopover()
    }, 100);
}
export {
    navigateToPage
}