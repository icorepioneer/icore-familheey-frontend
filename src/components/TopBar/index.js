import React, { useState, useEffect, useContext } from 'react';
import './style.scss'
import { Input, Button, Avatar, Dropdown, Icon, Menu, Modal, Popover, Badge } from 'antd';
import firebaseConfig from '../../shared/notification/notification';
import { encryption } from '../../shared/encryptDecrypt';
import { getUserDetails, clearNotifications, getNewAccessToken } from '../../modules/users/userProfile/api-profile';
import { UserContext } from './GlobalContext';
import { navigateToPage } from './navigateToPage'
import { useSelector, useDispatch } from 'react-redux';
import NotificationRow from './Notifications'
import firebase from 'firebase'
const { Search } = Input;
const environment = process.env.REACT_APP_STAGE === 'PROD' ? 'prod' : (process.env.REACT_APP_STAGE === 'STAGING' ? 'staging' : 'dev');
const { confirm } = Modal;

const TopBar = ({ history }) => {
    let dispatch = useDispatch();
    const { setUser } = useContext(UserContext);
    let {userId:u_id,phone:u_phone,token:u_token,user_details_loaded,user_details:u_details} = useSelector(red_state=>red_state.user_details)
    let [state, setState] = useState({
        user_id: useSelector(stateX => stateX.user_details.userId),
        notificationUrl: new firebaseConfig({}).config().ref('/' + environment + '_' + u_id + '_notification'),
        notificationArray: [],
        unreadCound: 0,
        visible: false,
        profileId: u_id,
        userDetails: u_details?u_details:[],
        phone_num: u_phone,
        token: u_token,
        searching_text:'',
        
    })
    const user_selector = useSelector(stateC=>stateC.user_details);

    let { searching_text, user_id, notificationArray, notificationUrl, unreadCound, visible, userDetails, token, phone_num } = state;
        
    useEffect(() => {
        searching_text = user_selector.discover;
        if(user_selector.discover) setState(prev => ({ ...prev, searching_text: searching_text }));
    },[user_selector.discover]);

    useEffect(() => {//update propic when ever change
        if(user_selector.user_details){
            userDetails = { ...userDetails, propic: user_selector.user_details.propic }
            setState(prev => ({ ...prev, userDetails: userDetails }));
        } 
    },[user_selector.user_details]);

    useEffect(() => {
        window.onscroll = function () { feviQuick(); };
        var mainwr = document.getElementById('f-wrap');
        function feviQuick() {
            if (window.pageYOffset >= 40) {
                mainwr.classList.add('f-sticky');
            } else {
                mainwr.classList.remove('f-sticky');
            }
        }
        notificationArray = []; unreadCound = 0;
        setState(prev => ({ ...prev, notificationArray: notificationArray, unreadCound: unreadCound }));
        showNotification();
        !user_details_loaded&&getUserProfileDetails()

    }, [window.location.pathname])
    // Toggle Search
    const getUserProfileDetails = () => {
        let params = {
            user_id: user_id,
            profile_id: user_id,
        }
        getUserDetails(params).then((response) => {
            setState(prev => ({ ...prev, userDetails: response.data.profile }))
            dispatch({ type: "USER_DETAILS_UPDATE", payload: { data: response.data.profile } });
        }).catch((error) => {
            console.log('error---->', error);
        })
    }
    const toggleSearchbar = () => {
        document.getElementById('f-wrap').classList.toggle("f-show-search");
    }

    const logout = () => {
        localStorage.clear();
        dispatch({ type: "USER_LOGGED_OUT",payload:{data:[]} });
        history.push(`/home`);
        //window.location.href="/"
    }
    const logoutConfirm = (event) => {
        confirm({
            title: 'Are you sure?',
            onOk() {
                logout();
            },
            onCancel() { },
        });
    }
    const showNotification = () => {
        
        let now = new Date().toISOString();
        // setState(prev => ({ ...prev, notificationArray: [],unreadCound:0 }));
        notificationUrl.limitToLast(250).once('value', (snapshot) => { 
            unreadCound=0;//to solve showing different notification count on different
            notificationInsertFunction(snapshot);
            notificationUrl.orderByChild('create_time').startAt(now).on('child_added', function (data) {
                notificationInsertFunction([data]);
                
            });
        });
    }
    const notificationInsertFunction = (notification) => {
        if (notification && notification != null && notification != undefined) {
            let tempArray = [];
            let unreadCounter = 0;   
            notification.forEach(function (childSnapshot) {
                if (childSnapshot.val()['visible_status'] == 'unread') {
                    unreadCounter++;
                }
                tempArray.push({ ...childSnapshot.val(), firebaseKey: childSnapshot.key });
            });
            tempArray = tempArray.filter(function (e) { return e; });
            tempArray.sort(function (a, b) { return a.create_time < b.create_time ? 1 : -1 });
            notificationArray = [...tempArray, ...notificationArray]
            unreadCound = unreadCound + unreadCounter;
            setState(prev => ({ ...prev, notificationArray: notificationArray, unreadCound: unreadCound }));
        }
    }

    const openPopover = () => {
        setState(prev => ({ ...prev, visible: !visible }));
    }
    useEffect(()=>{
        visible&&openPopover()
    },[window.location.pathname])
    
    //hide popover
    const handleVisibleChange = vs => {
        setState(prev => ({ ...prev, visible:vs }));
    };

    const markNotificationAsRead = (item, position) => {
        //let { firebaseKey } = item
        if (item.visible_status == 'read') {
            console.log("== read==");
        } else {
            let markAsReadId = (process.env.REACT_APP_STAGE.toLowerCase()) + '_' + parseInt(user_id) + '_notification' + '/' + item.firebaseKey;
            firebase.database().ref(markAsReadId).update({ visible_status: 'read' })
            // let temp = {};
            let _unreadCount = unreadCound;
            _unreadCount--;
            _unreadCount = _unreadCount <= 0 ? 0 : _unreadCount;
            // temp[item['key'] + '/visible_status'] = 'read';
            // notificationUrl.update(temp);
            let tempArray = [];
            tempArray = Array.from(notificationArray);
            item['visible_status'] = 'read';
            tempArray[position] = item;
            setState(prev => ({ ...prev, notificationArray: tempArray, unreadCound: _unreadCount }));
            showNotification()
        }
    }
    const markAllAsRead = () => {
        let newNotArray=[]
        notificationArray.map((data) => {
            newNotArray.push({...data,visible_status:'read'})
            let { firebaseKey } = data
            let markAsReadId = (process.env.REACT_APP_STAGE.toLowerCase()) + '_' + parseInt(user_id) + '_notification'+ '/' + firebaseKey;
            firebase.database().ref(markAsReadId).update({ visible_status: 'read' }).catch(err=>{
                console.log(err)
            })
        })
        setState(prev=>({...prev,notificationArray:newNotArray}))
    }
    const markAsDelete = (item) => {
        let { firebaseKey } = item
        let remainingArray = notificationArray.filter(data => data.firebaseKey != firebaseKey)
        let cancel_id = (process.env.REACT_APP_STAGE.toLowerCase()) + '_' + parseInt(user_id) + '_notification' + '/' + firebaseKey;
        firebase.database().ref(cancel_id).remove().then(message => {
            setState(prev => ({ ...prev, notificationArray: remainingArray }));
        }).catch(err => {
            console.log(err)
        })
    }
    const clearNotification = () => {
        let params = {
            user_id: user_id
        }
        clearNotifications(params).then((response) => {
            setState(prev => ({ ...prev, unreadCound: 0 }));
            setState(prev => ({ ...prev, notificationArray: [] }));
        }).catch((error) => {
            console.log('error---->', error);
        })
    }

    const showConfirm = () => {
        confirm({
            title: 'Do you want to clear all notifications?',
            onOk() {
                clearNotification();
            },
            onCancel() { },
        });
    }

    const naviagateToUserPage = () => e => {
        let id = encryption(user_id);
        history.push(`/user-details/${id}`, { prev: window.location.pathname });
    }
    // const announce = () => {
    //     history.push(`/announcement`);
    // }
    // const onSearch = (e) => {
    //     setUser({ search_text: e.target.value });
    // }
    const menu = (
        <Menu >
            <Menu.Item key="1" onClick={naviagateToUserPage()}>
                <Icon type="user" /> Profile
            </Menu.Item>
            {/* <Menu.Item key="2">
                <Icon type="info-circle" /> Help Center
        </Menu.Item> */}
            {/* <Menu.Item key="3">
                <Icon type="setting" /> Settings
            </Menu.Item> */}
            <Menu.Item key="4" onClick={(e) => logoutConfirm(e)}>
                <Icon type="logout" /> Logout
            </Menu.Item>
        </Menu>
    );
    const menumobile = (
        <Menu>
            <Menu.Item key="1" onClick={(e) => { history.push('/post/create') }} >Create Post</Menu.Item>
            <Menu.Item key="2" onClick={(e) => { history.push('/announcement/create') }} >Create Announcement</Menu.Item>
            <Menu.Item key="3">Create Event</Menu.Item>
        </Menu>
    );
    const dropmobile = (
        <Menu>
            <Menu.Item key="1" onClick={(e) => { history.push('/announcement') }}>Announcements</Menu.Item>
            <Menu.Item key="2" onClick={(e) => { history.push('/calender') }}>Calendar</Menu.Item>
            <Menu.Item key="3">Feedback</Menu.Item>
        </Menu>
    );
    const content = (newArray) => {
        
        return (
            <React.Fragment>
                {newArray && newArray.length > 0 ?
                    newArray.map((item, index) => {
                        return <NotificationRow
                            key={index}
                            index={index}
                            content={item}
                            markNotificationAsRead={markNotificationAsRead}
                            navigateToPage={navigateToPage}
                            history={history}
                            markAsDelete={markAsDelete}
                            openPopover={openPopover}
                        />
                    }) :
                    <div style={{ textAlign: 'center', padding: '20px' }}>
                        <span>No new notifications</span>
                    </div>
                }
            </React.Fragment>
        );
    }
    const notMenu = (
        <Menu>
            <Menu.Item key="0">
                <div style={{ display: 'inline-flex', justifyContent: 'space-between' }}>
                    <Button
                        onClick={() => {
                            openPopover()
                            history.push('/notificationSettings')
                        }}
                        style={{ border: 'none' }}
                    ><Icon
                            type="setting" />
                        <span>Notification settings</span>
                    </Button>
                </div>
            </Menu.Item>
            {/* markAllDelete */}
            <Menu.Item key="1">
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Button
                        onClick={() => {
                            markAllAsRead()
                        }}
                        style={{ border: 'none' }}
                    ><Icon
                            type="check-circle" />
                        <span>Mark all as read</span>
                    </Button>
                </div>
            </Menu.Item>
            <Menu.Item key="2">
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Button
                        onClick={(e) => showConfirm()}
                        style={{ border: 'none' }}
                    ><Icon
                            type="check-circle" />
                        <span>Delete all</span>
                    </Button>
                </div>
            </Menu.Item>
        </Menu>
    );
    const text = (
        <div
            className="f-notif-ttl">
            <span>Notifications</span>
            <Dropdown
                overlay={notMenu}
                trigger={['click']}
            ><Icon
                    type="ellipsis"
                    style={{
                        fontWeight: 'bold',
                        fontSize: '30px'
                    }} />
            </Dropdown>

        </div>
    );
    useEffect(() => {
        let timer_input;
        let discover = document.getElementById("discover")
        discover.addEventListener("keyup", () => {
            clearTimeout(timer_input)
            timer_input = setTimeout(() => {
                let theSearch = discover.value.length > 0 ? discover.value : ' '
                setState(prev => ({ ...prev, searching_text: theSearch }));
                setUser({ search_text:theSearch  })
            }, 1300)
        })
    }, [])

    //useEffect for getting new authToken
    useEffect(() => {
        let int_ = setInterval(() => getNewToken(), 3000000)
        return (() =>
            clearInterval(int_)
        )
    }, [])
    useEffect(()=>{
        getNewToken()
    },[])
    const getNewToken = () => {
        let params = {
            phone: phone_num,
            refresh_token: token,
        }
        getNewAccessToken(params).then(res => {
            let { accessToken } = res.data
            localStorage.removeItem('hash_key')
            dispatch({ type: "AUTH_TOKEN_UPDATE", payload: { data: accessToken } });

        })
    }
    return (
        <header className="f-head-skelton">
            <div className="f-boxed">
                <div className="f-head-dock">
                    <span style={{ 'cursor': 'pointer' }} onClick={() => history.push("/post")} className="f-brand"><img src={require('../../assets/images/logo-m.png')} alt="Familheey" /></span>
                    <div className="f-graph-search">
                        <Search id="discover" placeholder="Discover"
                            //  onChange={(e) => onSearch(e)}
                            onClick={() => history.push('/searchResult', { current: window.location.pathname })} />
                    </div>
                    <div className="f-control-center" id="f-control-center">
                        <Button className="f-icon-search" onClick={() => toggleSearchbar()} style={{ display: 'none' }} /> {/* For Mobile Only */}
                        <Badge className="f-notif-badge" count={unreadCound > 50 ? '50+' : unreadCound}>
                            <Popover
                                getPopupContainer={() => document.getElementById('f-control-center')}
                                placement="bottomRight"
                                title={text}
                                content={content(notificationArray)}
                                visible={visible}
                                trigger="click"
                                onVisibleChange={handleVisibleChange}
                                id="f-notification-popover"
                            >
                                <Button className="f-icon-bell" onClick={openPopover} />
                            </Popover></Badge>
                        {/* <Button onClick={() => announce()} className="f-icon-announce" /> */}
                        {/* Use If No User Image */}
                        {/* <Avatar className="f-avatar" size={45} icon="user" /> */}
                        <Dropdown overlay={menu} placement="bottomLeft">
                            <Avatar className="f-avatar" size={34} src={userDetails.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + userDetails.propic : (userDetails.gender === 'male' ? require('../../../src/assets/images/Male-Profile-pic.png') : userDetails.gender == 'female' ? require('../../../src/assets/images/Female-Profile-pic.png') : require('../../../src/assets/images/male-avatar.jpg'))} />
                        </Dropdown>
                        <Dropdown overlay={dropmobile} placement="bottomLeft">
                            <Button style={{ display: 'none' }} className="f-top-more" />
                        </Dropdown>
                    </div>
                    {/* Mobile Menu */}
                    <div className="f-mobile-app-bar" style={{ display: 'none' }}>
                        <Menu
                            mode="horizontal"
                            forceSubMenuRender={false}
                            defaultSelectedKeys={['1']}
                            className="f-mob-only-menu"
                        >
                            <Menu.Item key="1" onClick={(e) => { history.push('/post') }}>
                                <img src={require('../../assets/images/icon/mob_home.svg')} />
                            </Menu.Item>
                            <Menu.Item key="2" onClick={(e) => { history.push('/my-family') }}>
                                <img src={require('../../assets/images/icon/mob_family.svg')} />
                            </Menu.Item>
                            <Menu.Item key="3">
                                <Dropdown overlay={menumobile} placement="topCenter" getPopupContainer={trigger => trigger.parentNode}>
                                    <span>
                                        <img src={require('../../assets/images/icon/mob_add.svg')} />
                                    </span>
                                </Dropdown>
                            </Menu.Item>
                            <Menu.Item key="4">
                                <img src={require('../../assets/images/icon/worldwide.svg')} onClick={(e) => { history.push('/searchResult', { current: window.location.pathname }) }} />
                            </Menu.Item>
                            <Menu.Item key="5" className="f-last" onClick={(e) => { history.push('/events') }}>
                                <img src={require('../../assets/images/icon/mob_event.svg')} />
                            </Menu.Item>
                        </Menu>
                    </div>
                </div>
            </div>
        </header>
    )
}
export default TopBar;