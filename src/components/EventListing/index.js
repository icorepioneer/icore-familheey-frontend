import React, { useEffect, useState, useRef } from 'react';
import { Button, Spin } from 'antd';
import { exploreEvents, createdByMe } from '../../modules/events/eventListing/api-eventList';
import { encryption } from '../../shared/encryptDecrypt';
import { useSelector,useDispatch } from 'react-redux'
let moment = require('moment');
const EventListSide = ({ history }) => {
    let {events_loaded,events_up,events_rem} = useSelector(red_state=>red_state.side_bar_data)
    const dispatch = useDispatch()
    const [eventList, seteventList] = useState({
        user_id: useSelector(state => state.user_details.userId),
        myEventList: [],
        exploreEvent: '',
        upComingEventList: events_up?events_up:[],
        remainigEventList: events_rem?events_rem:[],
        count: 0,
        loader: events_up?false:true
    })
    const { myEventList, exploreEvent, upComingEventList, user_id, remainigEventList, count, loader } = eventList;

    useEffect(() => {
         exploreEvent && upComingEvents();
    }, [ exploreEvent])
    const exploreEventlist = () => {
        var params = {
            user_id: user_id,
            filter: ''
        }
        exploreEvents(params).then((response) => {
            let result = []
            if (response && response.data && response.data.data && response.data.data.explore) {
                result = response.data.data.explore
            }
            seteventList((prev) => ({ ...prev, exploreEvent: result,loader:false }))
        }).catch((error) => {
            console.log('error--->', error);
        })
    }
    const upComingEvents = () => {
        var _upComingEventList = [];
        var myevents = myEventList
        myevents = myevents.concat(exploreEvent.basedOnLocation);
        myevents = myevents.concat(exploreEvent.publicEvents);
        var date = (Date.parse(new Date())) / 1000
        _upComingEventList = myevents.filter(id => id.from_date > date);
        _upComingEventList.sort((a, b) => a.from_date - b.from_date);
        var upComingEventListShow = _upComingEventList.splice(count, 3);
        dispatch({type:'EVENT_ADD',payload:{data:{upcoming:upComingEventListShow,remaining:upComingEventList}}})
        seteventList((prev) => ({ ...prev, upComingEventList: upComingEventListShow, remainigEventList: upComingEventList }))
    }
    const showmore = () => {
        if (remainigEventList.length > 3) {
            var showupComingEventList = remainigEventList.splice(count + 3, 3)
            seteventList((prev) => ({ ...prev, upComingEventList: upComingEventList.concat(showupComingEventList), remainigEventList: remainigEventList, count: count + 3 }))
        } else {
            seteventList((prev) => ({ ...prev, upComingEventList: upComingEventList.concat(remainigEventList), remainigEventList: [] }))
        }

    }
    const navigateToEventPage = (value) => {
        let eventId = encryption(value.event_id)
        history.push(`/event-details/${eventId}`)
    }

    const contentLoader = new useRef(exploreEventlist)
    const observer = useRef(new IntersectionObserver(inputs => {
        let latest = inputs[0]
        latest.isIntersecting &&!events_loaded&&contentLoader.current()
    }, { threshold: 1 }))
    const [element, setElement] = useState(undefined)
    useEffect(() => {
        let current_element = element;
        let current_observer = observer.current;
        current_element && current_observer.observe(current_element)
        return () => {
            current_element && current_observer.unobserve(current_element)
        }
    }, [element])

    useEffect(() => {
        contentLoader.current = exploreEventlist;
    }, [exploreEventlist])
    return (
        <div className="f-sider-block">
            <div className="f-sider-head" onClick={() => history.push('/events')}>
                <img src={require('../../assets/images/icon/land4.svg')} />
                <h4>Upcoming Events</h4>
            </div>
            {!loader&&<React.Fragment>
                <div className="f-sider-body">
                    {upComingEventList && upComingEventList.length > 0 &&
                        <ul>
                            {upComingEventList.map((item, index) => {
                                return <li key={index} className="div-hover" onClick={() => navigateToEventPage(item)}>
                                    <div className="f-date">
                                        <strong>{moment.unix(item.from_date).format('DD')}</strong>
                                        <i>{moment.unix(item.from_date).format('MMM')}</i>
                                    </div>
                                    <span>
                                        <h5>{item.event_name}</h5>
                                        <h6>{item.location}<br />{moment.unix(item.from_date).format('MMM DD')} - {moment.unix(item.to_date).format('MMM DD')}, {moment.unix(item.to_date).format('YYYY')}</h6>
                                    </span>
                                </li>
                            })}
                        </ul>}
                </div>
                <div className="f-sider-foot">
                    {remainigEventList && remainigEventList.length > 0 &&
                        <Button type="link" onClick={() => showmore()}>see more</Button>}
                </div>
            </React.Fragment>}
            {loader && <div style={{ textAlign: 'center', marginTop: '100px', marginBottom: '100px' }} >
                <div ref={setElement}>
                    <Spin></Spin>
                </div>
            </div>}
        </div>
    )
}
export default EventListSide;