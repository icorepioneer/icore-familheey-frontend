import React, { useState, useEffect } from 'react';
import { Card, Button, Icon, Modal, Menu, Dropdown, Badge } from 'antd';

import { deleteAlbum } from '../../modules/AlbumModal/album-api';
import { encryption } from '../../shared/encryptDecrypt';
import lowerCase from 'lower-case'
import { useSelector } from 'react-redux';


const { confirm } = Modal;
var moment = require('moment');

const AlbumCard = ({ displayContent, history, _getAlbums, setAlbumToEdit, searchTag, created_by, is_admin, _userID }) => {
    const [albumCard, setalbumCard] = useState({
        user_id: useSelector(state => state.user_details.userId),
        groupAlbums: [],
        visible: false,
        initialValue: '',
        description: '',
        is_sharable: 'false',
        type: 'public',
        folder_name: '',
        cover_pic_url: '',
        folder_id: '',
        imgvisible: false,
        displayContent_: ''
    })
    const { user_id, groupAlbums, displayContent_, imgvisible } = albumCard
    useEffect(() => {
        let Filtered = displayContent && Array.isArray(displayContent) && displayContent.filter(item => lowerCase(item.folder_name || item.created_by_name).indexOf(lowerCase(searchTag)) !== -1)
        let _data = searchTag.length > 0 ? Filtered : displayContent
        setalbumCard(prev => ({ ...prev, displayContent_: _data }))
    }, [searchTag])
    useEffect(() => {
        setalbumCard(prev => ({ ...prev, displayContent_: displayContent }))
    }, [displayContent])
    const blockOrRemoveConfirmation = (type, message, id) => {
        message = message ? message : `Are you sure you want to ${type} this person`
        confirm({
            title: message,
            okText: 'Yes',
            cancelText: 'No',
            onOk() {
                deleteAlbumByID(id);
            },
            onCancel() {
            },
        });
    }

    const deleteAlbumByID = (id) => {         
        let params = {
            folder_id: [id.toString()],
            user_id: user_id
        }
        deleteAlbum(params).then((res) => {            
            if (res && res.data && res.data.code === 200) {
                let albumArray = groupAlbums.filter(value => value.id !== id);
                setalbumCard(prev => ({ ...prev, groupAlbums: albumArray }))
                _getAlbums();
            }
        }).then((err) => {
            console.log('err->',err);      
        })
    }
    const editAlbum = (item) => {
        setAlbumToEdit(item)
    }

    const showalbumContent = (item) => {
        setalbumCard(prev => ({ ...prev, imgvisible: true }))
        var folderId = encryption(item.id)
        created_by == user_id ? history.push(`/album-details/${folderId}/admin`) : history.push(`/album-details/${folderId}`)
    }
    return (
        <div className="f-album-listing-cards">
            {!imgvisible && displayContent && displayContent.length == 0 &&
                <div className="f-event-contents">
                    <div className="f-switch-content">
                        <div className="f-content">
                            {_userID == user_id &&
                                <div className="f-empty-content">
                                    <img src={require('../../assets/images/empty_album.png')} />
                                    <h4>Haven't created any albums,yet?</h4>
                                    <p> Let's add a new one!</p>
                                    <div className="f-clearfix">
                                        {/* <CreateAlbum getAlbums={getAlbums} type={'events'} event_id={event_id} /> */}
                                        {/* <Button onClick={al_showModal}>Create Album</Button> */}
                                    </div>
                                </div>
                            }
                            {_userID != user_id &&
                                <div className="f-empty-content">
                                    <h4>No photos or videos shared by the user</h4>
                                    <div className="f-clearfix">
                                        {/* <CreateAlbum getAlbums={getAlbums} type={'events'} event_id={event_id} /> */}
                                        {/* <Button onClick={al_showModal}>Create Album</Button> */}
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            }
            {!imgvisible && displayContent_ && displayContent_.length > 0 &&
                displayContent_.map((item, index) => {
                    return <Card key={index} className="f-album" >
                        <div className="f-top">
                            <img onError={(e) => { e.target.src = require('../../assets/images/empty_album.png') }} onClick={() => showalbumContent(item)} src={item.cover_pic ? item.cover_pic : require('../../assets/images/default-album.png')} />
                            {(item.created_by == user_id || is_admin) && <Dropdown
                                overlay={<Menu className="f-album-context">
                                    <Menu.Item key="0">
                                        <Button className="f-trash" onClick={() => blockOrRemoveConfirmation('albumDelete', 'Delete this Album ?', item.id)}><Icon type="delete" /> Delete</Button>
                                    </Menu.Item>
                                    <Menu.Item key="1">
                                        <Button onClick={() => editAlbum(item)}><Icon type="edit" /> Edit</Button>
                                    </Menu.Item>
                                </Menu>}
                                trigger={['click']}
                                placement="bottomRight"
                            >
                                <Button className="f-more" />
                            </Dropdown>}
                            <Badge className="badge-count badge-count-fix">{item.doc_count}</Badge>
                        </div>
                        <div className="f-bot" onClick={() => showalbumContent(item)}>
                            <h4>{item.folder_name}</h4>
                            <h5>{item.created_by_name}</h5>
                            <p>{moment(item.created_at).format('MMM DD, YYYY')}</p>
                        </div>
                    </Card>
                })
            }
        </div>
    )
}
export default AlbumCard