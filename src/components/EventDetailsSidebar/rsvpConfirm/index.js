import React, { useState, useEffect } from 'react'
import { Modal, Result, Button } from 'antd'
const RSVPBox = ({ modalVisible, callback }) => {
    console.log(modalVisible)
    let [state, setState] = useState({
        visible: false,
        primary: true
    })
    let { visible, primary } = state;
    const handleOk = e => {
        setState(prev => ({
            ...prev,
            visible: false
        }));
    };

    const handleCancel = e => {
        callback()
        // setState(prev=>({
        //     ...prev,
        //     visible: false
        //   }));
    };
    const changeReminder = () => {
        let _reminder = [
            <Button type="primary" onClick={() => { setState(prev => ({ ...prev, primary: false })) }} key="console">
                change reminder
            </Button>
        ]
        let time_buttons = [
                <Button 
                type="primary" 
                onClick={() => { setState(prev => ({ ...prev, primary: false })) }} 
                key="1">
                    1 Hour before
                </Button>,
                <Button 
                type="primary" 
                onClick={() => { setState(prev => ({ ...prev, primary: false })) }} 
                key="2">
                    2 Hour before
                </Button>,
                <Button 
                type="primary" 
                onClick={() => { setState(prev => ({ ...prev, primary: false })) }} 
                key="5">
                    5 Hour before
                </Button>,
                <Button 
                style={{marginTop:'10px'}} 
                type="primary" 
                onClick={() => { setState(prev => ({ ...prev, primary: false })) }} 
                key="24">
                    24 Hour before
                </Button>,
        ]
        return primary ? _reminder : time_buttons

    };

    useEffect(() => {
        setState(prev => ({ ...prev, visible: modalVisible }))
    }, [modalVisible])
    return (
        <React.Fragment>
            <Modal
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={null}
            >
                <Result
                    status="success"
                    title="Event added successfully to your calender"
                    subTitle="You will recieve a reminder on"
                    extra={changeReminder()}
                />
            </Modal>
        </React.Fragment>
    )
}

export default RSVPBox