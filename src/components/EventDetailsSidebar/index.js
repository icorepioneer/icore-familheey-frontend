import React, { useState, useEffect } from 'react';
import './style.scss'
import { Button, Checkbox, Radio, Input, Select, Switch, Modal, Menu, DatePicker, Icon, Form } from 'antd';
import { responseToEvents, updateEventDetails, listEventAgenda, cancelOrDeleteFunction } from '../../modules/events/eventDetails/api-eventDetails';
import { checkEventLink } from '../../modules/createFamily/api-family';
import { notifications } from '../../shared/toasterMessage';
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import ReactGoogleMapLoader from "react-google-maps-loader";
import { useSelector } from 'react-redux'
import {decryption} from '../../shared/encryptDecrypt'

const MY_API_KEY = decryption(process.env.REACT_APP_PLACE_ENCRYPT)
const { Option } = Select;
const { confirm } = Modal;

var moment = require('moment');
const EventDetailsSidebar = ({ callBackFromParent, eventInfo, rsvp, initial, history, form }) => {
    const { getFieldDecorator, validateFields } = form;

    const [eventDetailsState, setEventDetails] = useState({
        selected: {
            'going': false,
            'not-going': false,
            'interested': false
        },
        user_id: useSelector(state => state.user_details.userId),
        editModal: false,
        categoryList: [
            "Activities",
            "Art",
            "Business",
            "Causes",
            "Crafts",
            "Culture",
            "Dance",
            "Death",
            "Education",
            "Education Training",
            "Environment",
            "Family",
            "Fashion",
            "Film",
            "Food",
            "Health and wellness",
            "Investment and Finance",
            "Literature",
            "Movies",
            "Music",
            "Networking",
            "Others",
            "Party",
            "Real Estate",
            "Religion",
            "Re-union",
            "Sports",
            "Theatre",
            "Travel and Tourism",
        ],
        nextpage: false,
        eventDetails: [],
        previousValue: [],
        buttonText: 'Continue',
        countModal: false,
        others_count: (rsvp && rsvp.others_count) ? rsvp.others_count : 0,
        extra_count: (rsvp && rsvp.others_count) ? rsvp.others_count : 0,
        current_rsvp: rsvp,
        _loading: false,
        rsvp_modal_visible: false,
        _value: "",
        search: ""
    })
    let { search, _value, selected, user_id, editModal, categoryList, nextpage, eventDetails,countModal, others_count, extra_count, current_rsvp, _loading } = eventDetailsState;
    useEffect(() => {
        if (current_rsvp && current_rsvp.attendee_type) {
            setResponseType(current_rsvp.attendee_type);
        }
    }, [current_rsvp])
    useEffect(() => {
        setEventDetails(prev => ({ ...prev, eventDetails: eventInfo }))
    }, [eventInfo])
    const setResponseType = (type) => {
        selected[type] = true;
        setEventDetails(prev => ({ ...prev, selected: selected }))
    }
    const selectRespond = (type) => e => {
        Object.keys(selected).forEach(key => {
            if (key === type) {
                selected[key] = true;
            }
            else {
                selected[key] = false;
            }
            setEventDetails(prev => ({ ...prev, selected: selected }))
        });
        if (type === 'going' && eventInfo.allow_others === true) {
            setEventDetails(prev => ({ ...prev, countModal: true }))
        }
        responseToEvent(type);
    }
    const onRadioChange = (e) => {
        // this.props.callbackFromParent(e.key);
        callBackFromParent('change', e.key);
        switch (e.key) {
            case 1: 
                console.log(" case 1 ")
                break;
            case 2: 
                console.log(" case 2 ")
                break;
            case 3: 
            console.log(" case 3 ")
                break;
            case 4: { showAgenda() }
                break;
        }
    }
    const editEvent = () => {
        setEventDetails(prev => ({ ...prev, editModal: true, previousValue: eventDetails, _value: eventDetails.location }))
    }
    const handleChange = (e, name) => {
        if (name == 'from_date' || name == 'to_date') {
            eventDetails[name] = e != null ? new Date(moment(e).format('YYYY-MM-DD')).getTime() / 1000 : '';
            setEventDetails(prev => ({ ...prev, eventDetails: eventDetails }))
        }
        else {
            eventDetails[name] = e.target.value
            setEventDetails(prev => ({ ...prev, eventDetails: eventDetails }))
        }
    }
    const disabledStartDate = (start) => {
        return moment(start).isSameOrBefore(moment(new Date()).subtract(1, 'day'))
    }
    const disabledEndDate = (end) => {
        if (eventDetails.from_date && moment.unix(eventDetails.from_date, "MMM DD YYYY hh mm ss") >= moment(new Date())) {
            return moment(end).isSameOrBefore(moment.unix(eventDetails.from_date, "MMM DD YYYY hh mm ss"))
        } else
            return moment(end).isSameOrBefore(moment(new Date()))
    }
    const handleSelect = (name) => e => {
        if (name == 'is_public') {
            setEventDetails(prev => ({ ...prev, eventDetails: { ...eventDetails, [name]: !e } }))
        } else {
            setEventDetails(prev => ({ ...prev, eventDetails: { ...eventDetails, [name]: e } }))
        }
    }
    const handleCancel = e => {
        setEventDetails(prev => ({
            ...prev,
            editModal: false, countModal: false
        }))
    };
    /*const checkEventLinks = async (e) => {
        var data = {
            text: eventDetails.event_page
        }
        await checkEventLink(data).then((data) => {
            updateEvent(e)
        }).catch((error) => {
            notifications('error', 'Error', 'link already exist')
        })
    }*/
    const updateEvent = (e) => {
        e.preventDefault();
        eventDetails.created_by = user_id;
        eventDetails.user_id = user_id;
        eventDetails.id = eventDetails.event_id ? eventDetails.event_id.toString() : eventDetails.id.toString()
        let { event_id, ...rem } = eventDetails;
        validateFields(async(err,value)=>{
            if( eventDetails.event_page != value.event_page){
                try{
                    await checkEventLink({ text: value.event_page })
                    console.log(" = = = = ok   = = = ")
                } catch(error){
                    notifications("error","error","Link already exist");
                    return
                }
            }
            

            if(value.location == ""){
                notifications("error","error","Location is mandatory");
                return
            }
            if( moment(value.to_date).diff(moment(value.from_date)) <=0 ){
                notifications("error","error","Start date cannot be less than or equal as end date.");
                return
            }
            if(!err){
                value = { ...value,
                    from_date:moment(value.from_date).unix(),
                    to_date:moment(value.to_date).unix()
                }
                rem = { ...rem, ...value }
                setEventDetails(prev => ({ ...prev, _loading: !_loading }))
                updateEventDetails(rem).then((data) => {
                    setEventDetails(prev => ({ ...prev, editModal: false, _loading: false, nextpage: false }))
                    data.data.data.created_by_name = value.created_by_name
                    data.data.data.propic = eventDetails.propic
                    callBackFromParent('update', data.data.data);
                    notifications('success', 'Success', 'Event updated successfully')
                }).catch((error) => {
                    notifications('error', 'Error', 'Something went wrong')
                })
            }
        })
    }

    const handleCountCancel = () => {
        setEventDetails(prev => ({ ...prev, countModal: false, extra_count: others_count ? others_count : 0 }))
    }
    const handleCountOk = e => {
        setEventDetails(prev => ({ ...prev, countModal: false, others_count: extra_count }))
        responseToEvent('going', 1)
    };
    const changeGoingCount = (type) => e => {
        if (type === 'extra_count' && e.target.value >= 0) {
            setEventDetails(prev => ({ ...prev, [type]: e.target.value }))
        }
    }
    const responseToEvent = (type, flag = 0) => {
        var params = {
            event_id: (eventInfo.event_id).toString(),
            user_id: user_id,
            resp: type,
            others_count: others_count ? others_count : 0,
            created_by: eventInfo.created_by
        };
        if (flag == 1) {
            params.guest_count_update = true;
        }
        if (!current_rsvp || (current_rsvp && current_rsvp.attendee_type != type) || flag == 1) {
            responseToEvents(params).then((response) => {
                setEventDetails(prev => ({ ...prev, rsvp_modal_visible: true, current_rsvp: response.data.data }))
                console.log('response', response);
            }).catch((error) => {
                console.log('error---->', error);
            });
        }
    }
    const showAgenda = async () => {
        var params = {
            event_id: eventDetails.event_id.toString()
        }
        await listEventAgenda(params).then((response) => {
            callBackFromParent('agenda', response);
        }).catch((error) => {
            console.log('error', error)
        })
    }
    // const addToCalender = () => e => {
    //     notifications('info', 'Coming Soon')
    // }
    const cancelOrDelete = (type) => e => {
        var message = `Are you sure you want to ${type} this event`;
        console.log(message)
        confirm({
            title: message,
            okText: 'Yes',
            cancelText: 'No',
            onOk() {
                var params = {
                    event_id: (eventInfo.event_id).toString(),
                    user_id: user_id,
                    status: type
                }
                cancelOrDeleteFunction(params).then((response) => {
                    console.log('response', response);
                    if (response && response.data && response.data.message) {
                        if (response.data.message == 'success') {
                            notifications('success', 'Success', 'Deleted Successfully')
                            history.push('/events')
                        } else {
                            notifications('error', 'Error', response.data.message);
                        }
                    } else {
                        notifications('error', 'Error', 'Could not delete event');
                    }
                }).catch((error) => {
                    console.log('error', error);
                    notifications('error', 'Error', 'Could not delete event');
                })
            },
            onCancel() {
            },
        });

    }
    // const changeRsvp_modal_visible = () => {
    //     setEventDetails(prev => ({ ...prev, rsvp_modal_visible: !rsvp_modal_visible }))
    // }

    const nextPage = () => {
        if (eventDetails.event_name == "") {
            notifications('error', 'Error', 'Event Name is mandatory');
            return;
        }
        setEventDetails(prev => ({ ...prev, nextpage: true }))
    }

    const handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
        eventDetails = { ...eventDetails, location: geocodedPrediction.formatted_address }
        setEventDetails((prev) => ({
            ...prev,
            search: "",
            _value: geocodedPrediction.formatted_address,
            eventDetails: eventDetails,
            location:geocodedPrediction.formatted_address
        }));
    };
    const handleInputChange = (e) => {
        e.persist();
        setEventDetails((prev) => ({
            ...prev,
            search: e.target.value,
            _value: e.target.value,
            location:e.target.value
        }));
    };

    return (
        <aside className="eventDetailsSidebar f-inside-sticky">
            {/* <RSVPBox modalVisible={rsvp_modal_visible} callback = {changeRsvp_modal_visible}/> */}
            {/* Sidebar Block */}
            {((eventInfo.created_by).toString() !== user_id) &&
                <div className="f-sider-block f-ord2">
                    <div className="f-action">
                        {/* Disable other checkboxes if any option selected */}
                        <Checkbox checked={selected['going']} onClick={selectRespond('going')} >Going</Checkbox>
                        <Checkbox checked={selected['interested']} onClick={selectRespond('interested')} >Interested</Checkbox>
                        <Checkbox checked={selected['not-going']} onClick={selectRespond('not-going')}>Not Going</Checkbox>

                        {/* If Webinar */}
                        {/* <Button className="f-join-webinar">
<img src={require('../../assets/images/logo/logo_gotomeeting.png')} /> */}
                        {/* <img src={require('../../assets/images/logo/logo_webex.png')} />
<img src={require('../../assets/images/logo/logo_bluejeans.png')} />
<img src={require('../../assets/images/logo/logo_hangouts.png')} />
<img src={require('../../assets/images/logo/logo_zoom.png')} /> */}
                        {/* Join
</Button> */}

                    </div>
                </div>
            }

            {/* Sidebar Block */}
            {/* <div className="f-sider-block f-ord3 f-clickable" onClick={addToCalender()}>
<div className="f-btn-combo">
<Button className="f-btn-calendar">Calender</Button>
{this.props.eventInfo.is_sharable === 'true' &&
<span>
<i />
<Button className="f-btn-share" onClick={this.addToCalender()}>Share</Button>
</span>
}
</div>
</div> */}

            {/* Sidebar Block */}
            <div className="f-sider-block f-sider-menu-incl">
                <Menu
                    mode="inline"
                    // defaultSelectedKeys={['1']}
                    className="f-sider-menu"
                    onClick={(e) => onRadioChange(e)}
                    selectedKeys={[`${initial}`]}
                >
                    <Menu.Item key="1">
                        <img src={require('../../assets/images/icon/ev1.svg')} />
                        <span>Details</span>
                    </Menu.Item>
                    {(eventDetails.event_type == "Sign Up" || eventDetails.event_type == "Sign up") && <Menu.Item key="2">
                        <img src={require('../../assets/images/icon/ev2.svg')} />
                        <span>Sign Up</span>
                    </Menu.Item>}
                    <Menu.Item key="3">
                        <img src={require('../../assets/images/icon/land3.svg')} />
                        <span>Guests</span>
                    </Menu.Item>
                    <Menu.Item key="4">
                        <img src={require('../../assets/images/icon/ev3.svg')} />
                        <span>Agenda</span>
                    </Menu.Item>
                    <Menu.Item key="5">
                        <img src={require('../../assets/images/icon/land10.svg')} />
                        <span>Album</span>
                        {/* <i className="f-dot" /> */}
                    </Menu.Item>
                    <Menu.Item key="6">
                        <img src={require('../../assets/images/icon/land8.svg')} />
                        <span>Documents</span>
                        {/* <i className="f-dot" /> */}
                    </Menu.Item>
                </Menu>
            </div>

            {/* Sidebar Block */}
            {/* <div className="f-sider-block f-hr f-ord1">
<div className="f-admin">
<Avatar src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + this.props.eventInfo.propic} />
<span>
<h5>{this.props.eventInfo.created_by_name}</h5>
<h6>{moment(this.props.eventInfo.from_date).format('DD MMM YYYY')} &nbsp; | &nbsp; {moment(this.props.eventInfo.from_date).format('HH:mm a')}</h6>
</span>
</div>
</div> */}

            {/* Sidebar Block */}
            {/* <div className="f-sider-block f-ord5">
<ul className="f-meta">
<li>Going <strong>100</strong></li>
<li>Interested <strong>5</strong></li>
</ul>
</div> */}

            {/* Sidebar Block */}
            {((eventInfo.created_by).toString() === user_id) &&
                <div className="f-sider-block f-ord6">
                    <ul className="f-action-btns">
                        <li><Button type="link" className="f-btn-edit" onClick={editEvent}>Edit Event</Button></li>
                        <li><Button type="link" className="f-btn-cancel" onClick={cancelOrDelete('cancel')}>Cancel Event</Button></li>
                        <li><Button type="link" className="f-btn-delete" onClick={cancelOrDelete('delete')}>Delete Event</Button></li>
                    </ul>
                </div>
            }

            <Modal
                title="Edit Event Details"
                className="f-modal f-modal-wizard"
                visible={editModal}
                // onOk={this.handleOk}
                onCancel={handleCancel}
                // okText={  buttonText}
                // cancelText="Cancel"
                footer={[]}
            >
                <Form layout="vertical" onSubmit={(e)=>updateEvent(e)}>
                    {nextpage === false && eventDetails &&

                        <div className="f-modal-wiz-body">
                            <div className="f-form-control">
                                <label>Event Name*</label>
                                <Form.Item style={{ "marginBottom": "0px", "paddingBottom": "0px" }}>
                                {getFieldDecorator("event_name", {
                                    initialValue: eventDetails.event_name,
                                    rules: [
                                        {
                                            required: true,
                                            message: "Event name is mandatory."
                                        }
                                    ]
                                })(<Input placeholder="Event name" onChange={(e)=>handleChange(e,'event_name')}/>)}
                                </Form.Item>
                                {/* <Input value={eventDetails.event_name} onChange={(e)=>handleChange(e,'event_name')}></Input> */}
                            </div>
                            <div className="f-form-control">
                                <label>Event Type*</label>
                                {getFieldDecorator('event_type', {
                                    initialValue: eventDetails.event_type,
                                    rules: [
                                        {
                                            required: true,
                                            message: "Event Type is mandatory."
                                        }
                                    ]
                                })(
                                    <Select style={{ width: '100%' }} placeholder="Event Type" onChange={handleSelect('event_type')}>
                                        <Option key='Sign Up'>Sign up</Option>
                                        <Option key='Regular'>Regular</Option>
                                        <Option key='Online'>Online</Option>
                                    </Select>
                                )}
                                {/* <Select style={{ width: '100%' }} defaultValue={eventDetails.event_type} onChange={handleSelect('event_type')}>
<Option key='Sign Up'>Sign up</Option>
<Option key='Regular'>Regular</Option>
<Option key='Online'>Online</Option>
</Select> */}
                            </div>
                            <div className="f-form-control">
                                <label>Category*</label>
                                {getFieldDecorator('category', {
                                    initialValue: eventDetails.category,
                                    rules: [
                                        {
                                            required: true,
                                            message: "Category is mandatory."
                                        }
                                    ]
                                })(
                                    <Select style={{ width: '100%' }} placeholder="Event Type" onChange={handleSelect('event_type')}>
                                        {categoryList.map((item, index) => {
                                            return <Option value={item} key={index} >{item}</Option>
                                        })}
                                    </Select>
                                )}

                                {/* <Select defaultValue={eventDetails.category} style={{ width: '100%' }} onChange={handleSelect('category')} >
{categoryList.map((item, index) => {
return <Option value={item} key={index} >{item}</Option>
})}
</Select> */}
                            </div>
                            <div className="f-form-control f-wiz-switch">
                                <ul>
                                    <li>
                                        <h6>Make Your Event Private</h6>
                                        {getFieldDecorator('is_public')(
                                            <Switch onChange={handleSelect('is_public')} defaultChecked={!eventDetails.is_public}></Switch>
                                        )}
                                    </li>
                                    <li>
                                        <h6>Allow guest to invite others</h6>
                                        {
                                            getFieldDecorator('is_sharable')(
                                                <Switch onChange={handleSelect('is_sharable')} defaultChecked={eventDetails.is_sharable} ></Switch>
                                            )
                                        }

                                    </li>
                                    <li>
                                        <h6>Allow others to join the event</h6>
                                        {
                                            getFieldDecorator('allow_others')(
                                                <Switch disabled = {eventDetails.event_type==='Online'} onChange={handleSelect('allow_others')} defaultChecked={eventDetails.allow_others} ></Switch>
                                            )
                                        }
                                    </li>
                                </ul>
                            </div>
                        </div>
                    }
                    {nextpage === true &&
                        <div className="f-modal-wiz-body">
                            <div className="f-form-control">
                                <label>Hosted by</label>
                                {
                                    getFieldDecorator("created_by_name", {
                                        initialValue: eventDetails.created_by_name
                                    })(<Input onChange={(e) => handleChange(e, 'created_by_name')} />)
                                }

                            </div>
                            {eventDetails.event_type === "Online" && (
                                <React.Fragment>
                                    <div className="f-form-control">
                                        <label>Webinar Link*</label>
                                        <Form.Item style={{ "marginBottom": "0px", "paddingBottom": "0px" }}>
                                        {
                                            getFieldDecorator("meeting_link",{
                                                initialValue:eventDetails.meeting_link,
                                                rules:[{
                                                    required:true,
                                                    message:"Webinar Link is Mandatory"
                                                }]
                                            })(<Input placeholder="Enter Webinar link" onChange={(e) => handleChange(e, "meeting_link")}/>)
                                        }
                                        </Form.Item>
                                        
                                    </div>
                                    <div className="f-form-control">
                                        <label>Dial In number</label>
                                        {
                                            getFieldDecorator("meeting_dial_number",{
                                                initialValue:eventDetails.meeting_dial_number
                                            })(<Input placeholder="Enter Dial In number" onChange={(e) => handleChange(e, "meeting_dial_number")}/>)
                                        }                                        
                                    </div>

                                    <div className="f-form-control">
                                        <label>Participant PIN</label>
                                        {
                                            getFieldDecorator("meeting_pin",{
                                                initialValue:eventDetails.meeting_pin
                                            })(<Input placeholder="Enter  Participant PIN" onChange={(e) => handleChange(e, "meeting_pin")}/>)
                                        }
                                    </div>
                                </React.Fragment>
                            )}
                            {eventDetails.event_type !== 'Online' &&
                                <div className="f-form-control">
                                    <label>Venue*</label>
                                    <Form.Item style={{ "marginBottom": "0px" }}>
                                    {
                                        getFieldDecorator("location",{
                                            initialValue:_value
                                        })(
                                            <ReactGoogleMapLoader
                                                params={{
                                                    key: MY_API_KEY,
                                                    libraries: "places,geocode",
                                                }}
                                                render={(googleMaps) =>
                                                    googleMaps && (
                                                        <ReactGooglePlacesSuggest
                                                        googleMaps={googleMaps}
                                                        autocompletionRequest={{
                                                            input: search,
                                                        }}
                                                        onSelectSuggest={handleSelectSuggest}
                                                        textNoResults="No Result" // null or "" if you want to disable the no results item
                                                        customRender={(prediction) => (
                                                            <div className="customWrapper">
                                                                { prediction ? prediction.description: "No Result" }
                                                            </div>
                                                        )}>
                                                            <Input
                                                            className="art-mat-style art-suffix art-sfx-location"
                                                            placeholder="Location"
                                                            type="text"
                                                            value={_value}
                                                            onChange={(e) => handleInputChange(e)}
                                                            defaultValue={eventDetails.location ? eventDetails.location : ""}/>
                                                        </ReactGooglePlacesSuggest>
                                                    )
                                                }
                                            />
                                        )
                                    }
                                    </Form.Item>
                                </div>
                            }
                            <div className="f-form-control">
                                <label>Start Date &amp; Time* </label>
                                <Form.Item style={{ "marginBottom": "0px" }}>
                                {
                                    getFieldDecorator("from_date",{
                                        initialValue:moment.unix(eventDetails.from_date),
                                        rules:[{
                                            required:true,
                                            message:'Start Date & Time is mandatory'
                                        }]
                                    })(<DatePicker
                                        disabledDate={disabledStartDate}
                                        onChange={(e) => handleChange(e, 'from_date')}
                                        showTime
                                        format="MMM DD YYYY  hh:mm:ss"/>
                                    )
                                }
                                </Form.Item>
                            </div>
                            <div className="f-form-control">
                                <label>End Date &amp; Time* </label>
                                <Form.Item style={{ "marginBottom": "0px" }}>
                                {
                                    getFieldDecorator("to_date",{
                                        initialValue:moment.unix(eventDetails.to_date),
                                        rules:[{
                                            required:true,
                                            message:'End Date & Time is mandatory'
                                        }]
                                    })(<DatePicker
                                        disabledDate={disabledEndDate}
                                        onChange={(e) => handleChange(e, 'to_date')}
                                        showTime
                                        format="MMM DD YYYY  hh:mm:ss"/>
                                    )
                                }
                                </Form.Item>
                            </div>
                            <div className="f-form-control">
                                <label>Description</label>
                                {
                                    getFieldDecorator("description",{
                                        initialValue:eventDetails.description
                                    })(<Input onChange={(e) => handleChange(e, 'description')}/>)
                                }
                            </div>
                            <div className="f-form-control">
                                <label>RSVP Required</label>
                                {
                                    getFieldDecorator("rsvp",{
                                        initialValue:eventDetails.rsvp ? 'yes' : 'no'
                                    })(<Radio.Group onChange={(e) => handleChange(e, 'rsvp')} style={{ display: 'flex', float: 'left' }}>
                                            <Radio value='yes'>Yes</Radio>
                                            <Radio value='no'>No</Radio>
                                        </Radio.Group>
                                    )
                                }
                            </div>
                            <div className="f-form-control">
                                <label>Event Page URL</label>
                                <div style={{ display: 'flex', width: '100%', alignItems: 'center' }}>
                                    <span style={{ fontFamily: 'monospace', paddingRight: '5px' }} className="f-eve-pgurl">{`${process.env.REACT_APP_BASE_URL}events/`}</span>
                                    {
                                        getFieldDecorator("event_page",{
                                            initialValue:eventDetails.event_page
                                        })(<Input/>)
                                    }
                                </div>
                            </div>
                        </div>
                    }
                    <div className="f-modal-wiz-footer">
                        {nextpage === false &&
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <Button type="link" onClick={() => { setEventDetails(prev => ({ ...prev, editModal: false })) }}>Cancel</Button>
                                <Button onClick={() => nextPage()}>Continue <Icon type="arrow-right" /></Button>
                            </div>
                        }
                        {nextpage === true &&
                            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                <Button type="link" onClick={() => { setEventDetails(prev => ({ ...prev, nextpage: false })) }}><Icon type="arrow-left" /> Back</Button>
                                <Button htmlType="submit">Update Event <Icon type="arrow-right" /></Button>
                            </div>
                        }
                    </div>
                </Form>
            </Modal>
            <Modal
                title="Going Count"
                visible={countModal}
                onOk={handleCountOk}
                onCancel={handleCountCancel}
            >
                <div><input type="text" value={extra_count} onChange={changeGoingCount('extra_count')}></input></div>
            </Modal>
        </aside>
    )
    // }
}
export default Form.create()(EventDetailsSidebar);