import React from 'react';
import './style.scss'
import { Button, Card } from 'antd';
var moment = require('moment');
const MessageSidebar = ({ history, userList }) => {
    const backToPrevious = () => {
        history.push('/topics')
    }
    return (
        <aside className="MessageSidebar f-sider">
            <div className="f-announce-back">
                <Button onClick={() => backToPrevious()}><i /></Button> Back
                </div>
            <div className="f-announce-list">
                <div className="f-announce-cards">
                    {userList.map(({user_id,full_name,is_accept,location,propic}) =>
                        <Card key = {user_id} className="f-announce-card active">
                            <div className="f-start">
                                <div className="f-left">
                                    <img src={propic? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + propic: require('../../assets/images/default_propic.png')} />
                                </div>
                                <div className="f-right">
                                    <h4>{full_name}</h4>
                                </div>
                            </div>
                        </Card>
                    )}
                </div>
            </div>
            {/* <div className="f-announce-all">
                <Button>Show All</Button>
            </div> */}
        </aside>
    )
}
export default MessageSidebar;