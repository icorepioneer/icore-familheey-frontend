import React from 'react';
// import { Select, Modal, Input, Upload, Icon } from "antd";
import './header.scss'
class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <a href="#default" className="logo">FamilHeey</a>
                <div className="header-right">
                    <a className="active" href="#home">Home</a>
                    <a href="#contact">Contact</a>
                    <a href="#about">About</a>
                </div>
            </div>
        )
    }
}
export default Header;