import React from 'react';
import './style.scss'
class LazyLoader extends React.Component {
    state;
    props;
    constructor(props) {
        super(props);
        this.props = props;
        // console.log('this.pros',this.props.loader)
    }
    render() {
        return (
            <div className="lazyLoader">{this.props.loader === true &&
                <div className="f-lazyLoader">
                    <div />
                    <div />
                    <div />
                    <div />
                    <div />
                    <div />
                    <div />
                </div>
            }
            </div>
        )
    }
}
export default LazyLoader;