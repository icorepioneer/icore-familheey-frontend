import React, { useState, useEffect } from "react";
import * as moment from "moment";
import { Icon, Button } from "antd";
import { notifications } from '../../shared/toasterMessage'
const UserCard = ({
  item,
  determineColorBorder,
  admin_status,
  navigateToUser,
  user_id,
  getcontDetails,
  getStripeInstance,
  showModal,
  index,
  SkipThankU,
  getContributorList,
  reqDetails
}) => {
  const [state, setState] = useState({
    contribute_item_quantity: 0,
    contribute_user_id: 0,
    contribution_count: "0",
    created_at: undefined,
    full_name: undefined,
    id: undefined,
    is_acknowledge: false,
    is_anonymous: false,
    is_pending_thank_post: false,
    is_thank_post: false,
    location: "",
    paid_user_name: null,
    payment_status: undefined,
    phone: undefined,
    post_request_id: undefined,
    post_request_item_id: undefined,
    propic: null,
    request_item_title: "",
    requested_by: undefined,
    skip_thank_post: false,
    total_contribution: 0,
    item_loaded: false,
    flag: undefined,
  });
  let {
    contribute_user_id,
    contribution_count,
    created_at,
    full_name,
    is_acknowledge,
    is_anonymous,
    is_pending_thank_post,
    location,
    post_request_item_id,
    propic,
    requested_by,
    skip_thank_post,
    total_contribution,
    item_loaded,
    flag
  } = state;

  useEffect(() => {
    if (item) {
      let {
        contribute_item_quantity,
        contribute_user_id:c_uid,
        contribution_count:c_count,
        created_at:c_at,
        full_name:f_name,
        id,
        is_acknowledge:is_ak,
        is_anonymous:is_ans,
        is_pending_thank_post:is_ptp,
        is_thank_post,
        location:loc,
        paid_user_name,
        payment_status,
        phone,
        post_request_id,
        post_request_item_id:p_ri_id,
        propic:p_pic,
        request_item_title,
        requested_by:rq_by,
        skip_thank_post:sk_tp,
        total_contribution:t_cont,
      } = item;
      setState((prev) => ({
        ...prev,
        contribute_item_quantity: contribute_item_quantity,
        contribute_user_id: c_uid,
        contribution_count: c_count,
        created_at: c_at,
        full_name: c_uid == 2 ? paid_user_name : f_name,
        id: id,
        is_acknowledge: is_ak,
        is_anonymous: is_ans,
        is_pending_thank_post: is_ptp,
        is_thank_post: is_thank_post,
        location: loc,
        paid_user_name: paid_user_name,
        payment_status: payment_status,
        phone: phone,
        post_request_id: post_request_id,
        post_request_item_id: p_ri_id,
        propic: p_pic,
        request_item_title: request_item_title,
        requested_by: rq_by,
        skip_thank_post: sk_tp,
        total_contribution: t_cont,
        item_loaded: true,
        flag: admin_status || c_uid == user_id,
      }));
    }
  }, [item]);

  const displayName = () => {
    let name;
    switch (is_anonymous) {
      case true:
        if (flag) {
          switch (contribute_user_id) {
            case 2:
              name = full_name ? full_name+` (Anonymous)` :`Other members (Anonymous)`;
              break;
            //-- this case is dummy used to fix sonarqube  --// 
            //-- if new cases come delete this and add new --//
            case 'condition1':
              console.log("condition1");
              break;
            case 'condition2':
              console.log("condition2");
              break;
            //--- dummy end ---//
            default:
              name = `${full_name} (Anonymous)`;
              break;
          }
        } else {
          name = `Anonymous`;
        }
        break;
      case false:
        switch (contribute_user_id) {
          case 2:
            name = full_name ? full_name : "Other members";
            break;
          //-- this case is dummy used to fix sonarqube  --// 
          //-- if new cases come delete this and add new --//
          case 'condition1':
            console.log("condition1");
            break;
          case 'condition2':
            console.log("condition2");
            break;
          //--- dummy end ---//
          default:
            name = `${full_name}`;
            break;
        }
        break;
    }
    return name;
  };

  const displayLocation = () => {
    let loc;
    switch (is_anonymous) {
      case true:
        if (flag) {
          loc = location === null ? "" : location;
        } else {
          loc = `---------------`;
        }
        break;
      case false:
        loc = location === null ? "" : location;
        break;
      default:
        break;
    }
    return loc;
  };
  //   console.log(
  //     requested_by,
  //     user_id,
  //     is_pending_thank_post,
  //     requested_by == user_id && is_pending_thank_post
  //   );
  const displayButtonGroup = () => {
    let ad_flag = requested_by == user_id || admin_status;
    let _flag = is_pending_thank_post;
    switch (is_anonymous) {
      case true:
        if (ad_flag) {
          switch (is_acknowledge) {
            case false:
              return (
                <span className="f-mark-received" onClick={()=>SkipThankyou()}>
                  <input type="checkbox" style={{ display: "none" }} />
                  <label>
                    <Icon type="check-circle" />
                    Acknowledge
                  </label>
                </span>
              );
          }
        }
        break;
      case false:
        if (ad_flag) {
          switch (_flag) {
            case true:
              return contribute_user_id==2?'': (
                <span className="f-mark-received">
                  <input
                    type="checkbox"
                    id={"mark-received-" + index}
                    style={{ display: "none" }}
                  />
                  <label
                    onClick={() => showModal(item)}
                    htmlFor={"mark-received-" + index}
                  >
                    <Icon type="check-circle" />
                    Say Thanks
                  </label>
                </span>
              );
            case false:
              return (
                <span className="f-mark-received">
                  <input type="checkbox" style={{ display: "none" }} />
                  <label>
                    <Icon type="check-circle" />
                    Acknowledged
                  </label>
                </span>
              );
          }
        }
        break;
        default:
          break;
    }
  };

  const SkipThankyou = () => {
    let params = {
        request_item_id: `${post_request_item_id}`,
        contribute_user_id: `${contribute_user_id}`,
        skip_thank_post: `${skip_thank_post}`,
        is_anonymous:true
    }
    SkipThankU(params).then((doc) => {
        notifications("success", "Success", "Acknowledged...")
        setState(prev=>({...prev,is_acknowledge:true}))
    }).catch((err) => {
        notifications("error", "error", "Something went wrong")
    })
}
  return (
    <React.Fragment>
      {item_loaded && (
        <div className="f-req-author-card">
          {contribution_count === "1" && (
            <span
              className="f-req-badge"
              style={{ background: determineColorBorder(item) }}
            />
          )}
          <div
            className="f-left"
            onClick={() => !is_anonymous && navigateToUser(contribute_user_id)}
          >
            <img
              onError={(e) => {
                e.target.onerror = null;
                e.target.src = "../../images/profile-pic.png";
              }}
              src={
                is_anonymous
                  ? require("../../assets/images/profile-pic.png")
                  : process.env.REACT_APP_PRO_PIC_CROP +
                    process.env.REACT_APP_S3_BASE_URL +
                    "propic/" +
                    propic
              }
            />
          </div>
          <div className="f-mid">
            <h2>{displayName()}</h2>
            {contribute_user_id ==2 &&<h6>Non App Member</h6>}
            <h3>
              Contributed on: {moment(created_at).format("MMM DD YYYY h:mm a")}
            </h3>
            <span>
              <em>{displayLocation()}</em>
            </span>
          </div>
          <div className="f-right">
            <h4
              style={{ cursor: "pointer" }}
              onClick={() => getcontDetails(item)}
            >
              {total_contribution}
              <br />
              <em>Total</em>
            </h4>

            <div
              style={{
                width: "230px",
                // display: "flex",
                float: "right",
              }}
            >
              {displayButtonGroup()}
              {/* {is_anonymous &&} */}
              {contribute_user_id == user_id &&
                !is_acknowledge &&
                contribution_count == "1" && (reqDetails.request_type!="general") &&
                (
                  <span>
                    <div className="f-right">
                      <Button
                        style={{
                          backgroundColor: "var(--green)",
                          border: "1px solid var(--green)",
                          height: "32px",
                          color: "#FFF",
                          marginRight: "5px",
                        }}
                        onClick={() => {
                          getStripeInstance(item);
                        }}
                      >
                        Pay Now
                      </Button>
                    </div>
                  </span>
                )}
            </div>
          </div>
          <div style={{ cursor: "pointer" }}>
            {parseInt(contribution_count) > 1 ? (
              <img
                onClick={() => getcontDetails(item)}
                style={{ width: "14px" }}
                src={require("../../assets/images/icon/multiple.png")}
              />
            ) : (
              <Icon style={{ visibility: "hidden" }} type="arrows-alt" />
            )}
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default UserCard;
