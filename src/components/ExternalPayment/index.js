import React, { useState } from 'react'
import { Elements, CardElement, useStripe, useElements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import styled from 'styled-components'
import { Button, Modal } from 'antd';
const CardElementContainer = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  margin-top: 20px;

  & .StripeElement {
    width: 100%;
    padding: 15px;
  }
`;

const Row = styled.div`
  width:360px;
  margin: 30px auto;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 white;
  border-radius: 4px;
  background-color: relative;
  position: relative;
`;

const ButtonRow = styled.div`
  width: 360px;
  margin: 30px auto;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 white;
  border-radius: 4px;
  background-color: relative;
  position: relative;
`;
const stripePromise = loadStripe(`${process.env.REACT_APP_STRIPE_KEY}`);
const CheckoutForm = ({ amount, client_id, showThanYou,openAppBack }) => {
    const [isProcessing, setProcessingTo] = useState(false);
    const [checkoutError, setCheckoutError] = useState();
    const stripe = useStripe()
    const elements = useElements()

    const handleCardDetailsChange = ev => {
        let checkoutError_ = checkoutError
        checkoutError_ = ev.error?ev.error.message:null
        setCheckoutError(checkoutError_)
    };

    const handleFormSubmit = async ev => {
        ev.preventDefault();
        if (!stripe || !elements) {
            return
        }
        setProcessingTo(true);

        const cardElement = elements.getElement("card");
        try {
            await stripe.createPaymentMethod({
                type: "card",
                card: cardElement,
            }).then(async result => {
                let { error } = result
                if (error) {
                    setCheckoutError(error.message);
                    setProcessingTo(false);
                    return;
                }
                await stripe.confirmCardPayment(`${client_id}`, {
                    payment_method: { card: cardElement }
                }).then((result1) => {
                    let { error:e1 } = result1
                    if (e1) {
                        return
                    }
                    cardElement.clear()
                    showThanYou()
                    setProcessingTo(false)
                })
            })
        } catch (err) {
            console.log(err)
            setCheckoutError(err.message);
        }
    };
    const cardElementOpts = {
        iconStyle: "solid",
        hidePostalCode: true,
    };

    return (
        <form>
            <Row>
                <CardElementContainer>
                    <CardElement
                        options={cardElementOpts}
                        onChange={handleCardDetailsChange}
                    />
                </CardElementContainer>
            </Row>
            <ButtonRow>
                <Button
                    onClick={handleFormSubmit}
                    style={{
                        width:'100%',
                        height:'40px',
                        backgroundColor: 'var(--green)',
                        border: '1px solid var(--green)',
                        color: '#FFF',
                    }}
                    type="submit"
                    disabled={isProcessing || !stripe}
                >
                    {isProcessing ? "Processing..." : `Pay $${amount}`}
                </Button>
            </ButtonRow>
            <div style={{ textAlign: 'center' }}>
                <h6>Powered by Stripe</h6>
            </div>
            <ButtonRow>
            <Button
                    onClick={()=>openAppBack()}
                    style={{ width: '100%', height: '40px' }}
                >
                  Cancel and Go back 
                </Button>
            </ButtonRow>

        </form>
    )
}
const ExternalPayment = () => {
    let amount = window.location.search.split('?')[1].split('&')[0].split('=')[1]
    let client_id = window.location.search.split('?')[1].split('&')[1].split('=')[1]
    //let os = window.location.search.split('?')[1].split('&')[2].split('=')[1]
    let type = window.location.search.split('?')[1].split('&')[3].split('=')[1]
    let type_id = window.location.search.split('?')[1].split('&')[4].split('=')[1]
    const [state, setState] = useState({
        thankyou: false
    })
    let { thankyou } = state
    const ty_handleCancel = () => {
        setState((prev) => ({
            ...prev,
            thankyou: false
        }))
        openAppBack()
    };
    const ty_handleOk = () => {
        setState((prev) => ({
            ...prev,
            thankyou: false
        }))
    };
    const showThanYou = () => {
        setState(prev => ({ ...prev, thankyou: true }))
    }
    const openAppBack = () => {
        window.location.href = 'familheey://send?type=' + type + '&id=' + type_id;
        setTimeout(() => { window.close() }, 3000)
    }
    return (
        <React.Fragment>
            <div style={{ marginTop: '200px' }}>
                <img
                    src={require('../../assets/images/logo-m.png')}
                    alt="Familheey"
                    style={{
                        display: 'block',
                        marginLeft: 'auto',
                        marginRight: 'auto',
                    }} />
                <Elements stripe={stripePromise} >
                    <CheckoutForm
                        amount={amount / 100}
                        client_id={client_id}
                        showThanYou={showThanYou}
                        openAppBack={openAppBack}
                    />
                </Elements>
                <Modal

                    visible={thankyou}
                    title=""
                    onOk={() => ty_handleOk()}
                    onCancel={() => ty_handleCancel()}
                    footer={['']}
                    className="f-request-modal"
                >
                    <div className="f-clearfix"><img src={require('../../assets/images/icon/ok_tik.png')} /></div>
                    <h1>Great!</h1>
                    <h4>Thank you for your contribution</h4>
                    <Button onClick={() => openAppBack()}>Back to App</Button>
                </Modal>
            </div>
        </React.Fragment >
    )
}

export default ExternalPayment;