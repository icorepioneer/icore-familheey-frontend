import React, { useState, useEffect, useRef } from 'react'
import * as moment from 'moment'
import { Input, Button, Menu, Dropdown, notification, Icon } from 'antd'
import ReactPlayer from 'react-player'
import LightBox from 'react-image-lightbox'

const { TextArea } = Input;

const DisplayAttatchments = ({ file, setImages = () => { } }) => {
    let length = file.length
    let { filename, type } = file[0]
    switch (true) {
        case length == 1:
            return (
                <div onClick={() => type.indexOf('image') != -1&&setImages(file)} >
                    {
                        type.indexOf('image') != -1 && <img
                            src={process.env.REACT_APP_CHAT_PIC_RESIZE + process.env.REACT_APP_S3_BASE_URL + 'file_name/' + filename}
                        />}
                    {
                        type.indexOf('video') != -1 && <ReactPlayer
                            playing={false}
                            className="f-csl-react-play"
                            url={process.env.REACT_APP_S3_BASE_URL + 'file_name/' + filename}
                            controls />}
                    {
                        type.indexOf('application') != -1 &&
                        <a href={process.env.REACT_APP_S3_BASE_URL + 'file_name/' + filename}>{filename}</a>
                    }
                    {
                        type.indexOf('audio') != -1 &&
                        <audio controls controlsList="nodownload">
                            <source src={process.env.REACT_APP_S3_BASE_URL + 'file_name/' + filename} />
                        </audio>
                    }
                </div>
            )
        case length > 2:
            let files = file[0]
            return (
                <React.Fragment>
                    {
                        files.type.indexOf('image') != -1 &&
                        <div style={{ position: 'relative' }} onClick={() => setImages(file)}>
                            <img
                                src={process.env.REACT_APP_CHAT_PIC_RESIZE + process.env.REACT_APP_S3_BASE_URL + 'file_name/' + filename}
                            />
                            <span
                                style={{
                                    position: 'absolute',
                                    left: '5%',
                                    top: '40%',
                                    border: 'none',
                                    padding: '30px',
                                    background: 'transparent',
                                    color: 'white',
                                    textShadow:'5px 5px 10px grey',
                                    fontSize: '40px',
                                    textAlign: 'center'
                                }}
                            >{`+${length - 1} more`}</span>
                        </div>
                    }
                </React.Fragment>
            )
    }
}
const ChatCurrent = (data, deleteSelectedChat, forwardSelectedMessage, copyToClipBoard, setContentToQuote, setImages) => {
    let { comment, createdAt, comment_id, attachment, quoted_id, quoted_date, quoted_item, quoted_user } = data
    let attatchmentFlag = attachment && attachment.length > 0;
    let hideForward = (attatchmentFlag && attachment[0].type.indexOf('audio') != -1)
    const menu = (
        <Menu style={{ minWidth: "100px" }}>
            {!attatchmentFlag && <Menu.Item key="0" onClick={() => copyToClipBoard(comment_id)}>Copy</Menu.Item>}
            <Menu.Item key="1" onClick={() => { deleteSelectedChat(comment_id) }}>Delete</Menu.Item>
            {!hideForward &&<Menu.Item key="2" onClick={() => { forwardSelectedMessage(data) }} >Forward</Menu.Item>}
            {!attatchmentFlag && <Menu.Item key="3" onClick={() => { setContentToQuote(data) }}>Quote</Menu.Item>}
        </Menu>
    );
    return (
        <React.Fragment key={comment_id}>
            <div className="f-outgoing">
                <div className="f-group">
                    <span className="f-time">{`${moment(createdAt).toNow()}`}</span>
                    <div className="f-bubble">

                        {/* Quoted Message : Start */}
                        {quoted_id != null && <div className="f-quoted-msg-preview">
                            <p>{quoted_item}</p>
                            <em>{`${quoted_user}, ${moment(quoted_date).toNow()}`}</em>
                        </div>}
                        {/* Quoted Message : End */}

                        {attatchmentFlag ?
                            <DisplayAttatchments
                                setImages={setImages}
                                file={attachment}
                            /> : <p id={`cont_to_copy${comment_id}`}>{comment}</p>}
                        <span className="f-bubble-actions">
                            <Dropdown overlay={menu} trigger={['click']} placement="bottomRight">
                                <Button />
                            </Dropdown>
                        </span>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
const ChatIncoming = (data, forwardSelectedMessage, copyToClipBoard, setContentToQuote, setImages) => {
    let { comment, propic, full_name, createdAt, comment_id, attachment, quoted_id, quoted_date, quoted_item, quoted_user } = data
    let attatchmentFlag = attachment && attachment.length > 0;
    let hideForward = (attatchmentFlag && attachment[0].type.indexOf('audio') != -1)
    const menu = (
        <Menu style={{ minWidth: "100px" }}>
            {!attatchmentFlag && <Menu.Item key="0" onClick={() => copyToClipBoard(comment_id)}>Copy</Menu.Item>}
            {!hideForward && <Menu.Item key="2" onClick={() => { forwardSelectedMessage(data) }} >Forward</Menu.Item>}
            {!attatchmentFlag && <Menu.Item key="3" onClick={() => { setContentToQuote(data) }}>Quote</Menu.Item>}
        </Menu>
    );
    return (
        <React.Fragment key={comment_id}>
            <div className="f-incoming">
                <img className="f-avatar" src={propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + propic : require('../../assets/images/default_propic.png')} />
                <div className="f-group">
                    <header>
                        <h5>{full_name}</h5>
                        <h5>{`${moment(createdAt).toNow()}`}</h5>
                    </header>
                    <div className="f-bubble">

                        {/* Quoted Message : Start */}
                        {quoted_id != null && <div className="f-quoted-msg-preview">
                            <p>{quoted_item}</p>
                            <em>{`${quoted_user}, ${moment(quoted_date).toNow()}`}</em>
                        </div>}
                        {/* Quoted Message : End */}

                        {attatchmentFlag ?
                            <DisplayAttatchments
                                file={attachment}
                                setImages={setImages}
                            /> : <p id={`cont_to_copy${comment_id}`}>{comment}</p>}
                        {<span className="f-bubble-actions">
                            <Dropdown overlay={menu} trigger={['click']} placement="bottomRight">
                                <Button />
                            </Dropdown>
                        </span>}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

const InputChatField = ({ setCurrentMessage, handleSelectFile, submitOnClick, loading, messageToSend, item_to_quote_present, item_to_quote, removeContentToQuote, auto_focus, toggle_auto_focus }) => {
    useEffect(() => {
        document.getElementById("chatField").focus()
        toggle_auto_focus()
    }, [auto_focus])
    return (
        <div className="f-communication-submit">
            <div className={`${item_to_quote_present ? "f-send f-compose-quoted" : "f-send"}`}> {/* add class "f-compose-quoted" if its a quoted messag */}

                {/* Compose Quoted message : Start */}
                {item_to_quote_present &&
                    <div className="f-quoted-msg-preview">
                        <p>{item_to_quote.comment}</p>
                        <em>{`${item_to_quote.full_name}, ${moment(item_to_quote.createdAt).toNow()}`}</em>
                        <Button className="f-qt-mg-cl" onClick={() => removeContentToQuote()} />
                    </div>}
                {/* Compose Quoted message : End */}

                <TextArea
                    id="chatField"
                    autoFocus
                    value={messageToSend}
                    onChange={(e) => setCurrentMessage(e)}
                    placeholder="Say something ..." />
                <input
                    id="file-input1"
                    className="hide"
                    type="file"
                    accept=".jpg,.jpeg,.png,.mp4,.mkv,.avi,.pdf"
                    onChange={e => { handleSelectFile(e) }}
                />
                <label htmlFor="file-input1" className="f-attach" />
            </div>
            <Button
                disabled={messageToSend == null}
                loading={loading}
                onClick={() => submitOnClick()}
                className="f-submit"
            />
        </div>

    )
}
const ChatSection = ({ 
    data, 
    userId,
    comment, 
    topicId, 
    trigger_, 
    deleteConversation,
    history, 
    is_accept,
    created_user,
    acceptOrRejectUserTopic, 
    topicOwner, 
    one_Accepted, 
    chatloader,
    post_id,to_group_id
 }) => {
    const formData = new FormData()
    const [state, setState] = useState({
        previousChat: undefined,
        user_id: undefined,
        messageToSend: null,
        topic_id: undefined,
        loading: false,
        trigger: undefined,
        item_to_quote_present: false,
        item_to_quote: undefined,
        Chat: undefined,
        auto_focus: false,
        photoIndex: 0,
        isOpen: false,
        images: []
    })
    let { previousChat, user_id, messageToSend, topic_id, loading, item_to_quote_present,
        item_to_quote, Chat, auto_focus, photoIndex, isOpen, images } = state;
    formData.append('topic_id', `${topic_id}`)
    formData.append('commented_by', `${user_id}`)
    post_id&&formData.append('post_id', `${post_id}`);
    to_group_id&&formData.append('group_id', `${to_group_id}`);
    useEffect(() => {
        if (data) {
            setState(prev => ({ ...prev, previousChat: data, Chat: data }))
        }
        if (topicId) {
            setState(prev => ({ ...prev, topic_id: topicId }))
        }
        if (trigger_) {
            setState(prev => ({ ...prev, trigger: trigger_ }))
        }
    }, [data, topicId, trigger_])

    useEffect(() => {
        userId && setState(prev => ({ ...prev, user_id: userId }))
    }, [userId])

    const messageRef = useRef(null)
    const scrollToBottom = () => {
        messageRef.current && messageRef.current.scrollIntoView({ behavior: "smooth" })
        setTimeout(() => {
            setState(prev => ({ ...prev, trigger: undefined, Chat: undefined, auto_focus: true }))
        }, 500)

    }
    useEffect(() => {
        Chat && scrollToBottom()
    }, [Chat])
    const setCurrentMessage = (e) => {
        e.persist()
        let message = e.target.value
        setState(prev => ({ ...prev, messageToSend: message }))
    }
    const submitOnClick = () => {
        messageToSend = (messageToSend) ? messageToSend.trim() : '';
        if (item_to_quote_present) {
            formData.append('quoted_date', item_to_quote.createdAt)
            formData.append('quoted_user', item_to_quote.full_name)
            formData.append('quoted_item', item_to_quote.comment)
            formData.append('quoted_id', item_to_quote.comment_id)
        }
        formData.append('comment', messageToSend)
        setState(prev => ({ ...prev, loading: true }))
        comment(formData, null).then(res => {
            formData.delete('comment')
            formData.delete('file_name')
            removeContentToQuote()
            setState(prev => ({ ...prev, messageToSend: null, loading: false, trigger: true }))
            // setTimeout(()=>{
            //     setState(prev => ({ ...prev, trigger: true }))
            // },1000)
        })
    }
    const openNotification = () => {
        notification.open({
            message: 'Please choose another file',
            description:
                'This kind of files are not supported with this chat feature',
        });
    };
    const checkFileType = (file) => {
        if (file.type.includes('image/') || file.type.includes('video/')||file.type.includes('/pdf')
        ) {
            return true
        }
        return false
    }
    const handleSelectFile = async (event) => {
        let files = event.target.files
        for (let file of files) {
            if (checkFileType(file)) {
                formData.append('file_name', file)
                submitOnClick()
            }
            else {
                openNotification()
            }
        }
    }
    const deleteSelectedChat = (id) => {
        let params = {
            comment_id: [id],
            // topic_id: topic_id,
            [`${post_id?'post_id':'topic_id'}`]: post_id?post_id:topic_id
        }
        deleteConversation(params).then(res => {
        }).catch(err => { console.log("error happened") })
    }

    const forwardSelectedMessage = (selected) => {
        history.push('/post/create', { from_topic: true, selected: selected, path_: '/post', type_: 'post' })
    }

    const copyToClipBoard = (tag) => {
        let text = document.getElementById(`cont_to_copy${tag}`);
        navigator.clipboard.writeText(`${text.innerText}`)
        notification.open({
            message: "Message copied",
            icon: <Icon type="copy" style={{ color: "#108ee9" }} />
        });
    }

    const setContentToQuote = (item) => {
        setState(prev => ({ ...prev, item_to_quote_present: true, item_to_quote: item }))
    }

    const removeContentToQuote = () => {
        setState(prev => ({ ...prev, item_to_quote_present: false, item_to_quote: undefined }))
    }

    const toggle_auto_focus = () => {
        setState(prev => ({ ...prev, auto_focus: false }))
    }

    const setImages = (files) => {
        setState(prev => ({ ...prev, images: files, isOpen: !isOpen }))
    }
    const renderComp = () => {
        // console.log("is_accept", is_accept);
        // console.log("one_Accepted", one_Accepted)
        switch (is_accept) {
            case true:
                switch (one_Accepted) {
                    case true:
                        return (
                            <React.Fragment>
                                <div className="f-head">
                                    <p>New conversations</p>
                                </div>
                                <div className="f-conversations">
                                    <div className="f-conversations-singl">
                                        {previousChat && previousChat.map((data1) => {
                                            return data1.commented_by == user_id ?
                                                ChatCurrent(data1, deleteSelectedChat, forwardSelectedMessage, copyToClipBoard, setContentToQuote, setImages)
                                                :
                                                ChatIncoming(data1, forwardSelectedMessage, copyToClipBoard, setContentToQuote, setImages)
                                        })}
                                        <div ref={messageRef} className="f-incoming">
                                            {/* this div is used inorder to work with fn:scrollToBottom */}
                                        </div>
                                    </div>
                                </div>

                                {/* Hide this section when message accept / reject is showing */}
                                <InputChatField
                                    setCurrentMessage={setCurrentMessage}
                                    handleSelectFile={handleSelectFile}
                                    submitOnClick={submitOnClick}
                                    loading={loading}
                                    messageToSend={messageToSend}
                                    item_to_quote_present={item_to_quote_present}
                                    item_to_quote={item_to_quote ? item_to_quote : {}}
                                    removeContentToQuote={removeContentToQuote}
                                    auto_focus={auto_focus}
                                    toggle_auto_focus={toggle_auto_focus}
                                />
                                {isOpen && <LightBox
                                    mainSrc={process.env.REACT_APP_S3_BASE_URL + 'file_name/' + images[photoIndex].filename}
                                    nextSrc={process.env.REACT_APP_S3_BASE_URL + 'file_name/' + images[(photoIndex + 1) % images.length].filename}
                                    prevSrc={process.env.REACT_APP_S3_BASE_URL + 'file_name/' + images[(photoIndex + images.length - 1) % images.length].filename}
                                    onCloseRequest={() => setState(prev => ({ ...prev, isOpen: false, images: [] }))}
                                    onMovePrevRequest={() =>
                                        setState(prev => ({
                                            ...prev,
                                            photoIndex: (photoIndex + images.length - 1) % images.length,
                                        }))
                                    }
                                    onMoveNextRequest={() =>
                                        setState(prev => ({
                                            ...prev,
                                            photoIndex: (photoIndex + 1) % images.length,
                                        }))
                                    }
                                />}
                            </React.Fragment>
                        )
                    case false:
                        return (
                            <div className="f-topic-new-request">
                                <p>Once user accepts your request, you can start the conversation.</p>
                            </div>
                        )
                }
                break;
            case false:
                return (
                    <div className="f-topic-new-request">
                        <p><strong>{created_user}</strong> want to start a conversation with you.</p>
                        <div className="f-btn-group">
                            <Button onClick={() => { acceptOrRejectUserTopic("accept") }} className="f-button">Accept</Button>
                            <Button onClick={() => { acceptOrRejectUserTopic("reject") }} className="f-button f-line">Ignore</Button>
                        </div>
                    </div>
                )
        }
    }
    return (
        <React.Fragment  >
            {renderComp()}
        </React.Fragment>)
}

export default ChatSection