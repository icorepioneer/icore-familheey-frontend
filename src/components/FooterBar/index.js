import React from 'react';
class FooterBar extends React.Component {
    render() {
        return (
            <footer className="f-site-footer">
                <div className="f-boxed">
                    <ul>
                        <li><a href="/termsofservice">Terms and Conditions</a></li>
                        <li><a href="/policy">Privacy Policy</a></li>
                        <li><a href="/support">Support</a></li>
                    </ul>
                </div>
            </footer>
        )
    }
}

export default FooterBar;