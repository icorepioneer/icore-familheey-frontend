import React, { useState, useEffect } from 'react';
import './style.scss'
import { Button, message,Icon,Progress } from "antd";
import { generateSignedUrl,uploadFiletoSignedUrl } from '../../modules/family/familyList/api-familyList';
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const FileUpload = (props) => { 
    const maxBandWidth      = 50 * 1024 * 1024; //50Mb
    const [state, setState] = useState({
        uploadedFiles: props.uploadedFiles || [],
        fileType:props.fileType || '', // '.png,.pdf,.docx'
        fileMaxSize:props.fileMaxSize || null,  // 20  = Max 20 MB
        fileMaxCount:props.fileMaxCount || null,
        styleType:props.styleType || 1, // 0 for using functionality only
        enablePreview:props.enablePreview || 1, // 0 | 1
        filePath:props.filePath || '',
        acceptedFiles:'',
        elementKey:'',
        activeFileCount:0,
        activeBandWidth:0,
        inprogress:0,
        // videoUploads:[]
    });
    useEffect(() => {
       setState((prev) => ({ ...prev, elementKey:generateKey(4) }))
    },[]);
    const generateKey = (length=5) => {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    };
    const onFileChange  = async(event) => {
        let Files = event.target.files;
        await validateFile(Files);
        let currentIndex = state.uploadedFiles.length;
        for (var i = 0; i < Files.length; i++) {
            Files[i].fileKey          = generateKey(6);
            Files[i].percentCompleted = 0;
            Files[i].uploadStatus     = 1;//not started
            if(Files[i].type.indexOf('image') >=0) {
                Files[i].previewType   = 'image';
            } else if(Files[i].type.indexOf('video') >=0) {
                Files[i].previewType   = 'video';
            }  else if(Files[i].type.indexOf('audio') >=0) {
                Files[i].previewType   = 'audio';
            } else {
                Files[i].previewType   = 'other';
            }
            state.uploadedFiles.push(Files[i]);
            if(state.enablePreview == 1) {
                if(Files[i].previewType == 'image') {
                    let reader = new FileReader();
                    reader.fileIndex = currentIndex+i;
                    reader.addEventListener('load', () => {
                        state.uploadedFiles[reader.fileIndex].imageSrc      = reader.result;
                        setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles }));
                    });
                    reader.readAsDataURL(Files[i]);
                }  
                // else if(Files[i].previewType == 'video') {
                //     let reader = new FileReader();
                //     reader.fileIndex = currentIndex+i;
                //     let videoIndex = state.videoUploads.length;
                //     reader.videoIndex   = videoIndex;
                //     state.videoUploads.push(1);
                //     setState((prev) => ({ ...prev, videoUploads:state.videoUploads }));
                //     reader.addEventListener('load', () => {
                //         generateVideoThumbnail(state.uploadedFiles[reader.fileIndex],videoIndex,function(thumbnail) {
                //             state.uploadedFiles[reader.fileIndex].imageSrc      = thumbnail;
                //             setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles }));
                //         });
                //     });
                //     reader.readAsDataURL(Files[i]);
                // }
            }
            
        }
        if(props.getFiles && typeof props.getFiles === "function") {
            props.getFiles(
                state.uploadedFiles
            );
        }
        if(props.statusObject) {
            props.statusObject.OrginalFiles  = state.uploadedFiles;
        }
        if(state.inprogress == 0) {
            uploadStart(currentIndex);
        }
    };
    const reUpload = (index) => {
        state.uploadedFiles[index].uploadStatus     = 1;
        state.uploadedFiles[index].percentCompleted = 0;
        uploadFile(index);
    }
    const uploadStart  = async(index=0,flag=0) => {
        if(index < state.uploadedFiles.length && (state.activeFileCount <= 0 || (state.activeBandWidth + state.uploadedFiles[index].size) < maxBandWidth)) {
            if(state.uploadedFiles[index].uploadStatus != 1) {
                index++;
                uploadStart(index)
            } else {
                uploadFile(index);
            }
        } else if(flag ==1 && index >= state.uploadedFiles.length) {
            state.inprogress = 0;
            setState((prev) => ({ ...prev, inprogress:state.inprogress }));
            let responseArray = [];
            for(let i=0;i<state.uploadedFiles.length;i++) {
                if(state.uploadedFiles[i].uploadStatus == 3) {
                    responseArray.push({
                        filePath:state.uploadedFiles[i].filePath,
                        previewType:state.uploadedFiles[i].previewType
                    });
                }
            }
            if(typeof props.UploadCompleted === "function") {
                props.UploadCompleted(responseArray);
            }
            if(props.statusObject) {
                props.statusObject.UploadStatus     = state.inprogress;
                props.statusObject.UploadedFiles    = responseArray;
            }
        }
    };
    const uploadFile  = async(index=0) => {
        state.inprogress = 1;
        if(props.statusObject) {
            props.statusObject.UploadStatus = state.inprogress;
        }
        state.activeFileCount++;
        state.activeBandWidth += state.uploadedFiles[index].size;
        let newIndex     = index +1;
        state.uploadedFiles[index].uploadStatus = 2;//inprogress
        setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles,activeFileCount:state.activeFileCount,activeBandWidth:state.activeBandWidth,inprogress:state.inprogress }));
        uploadStart(newIndex);
        fetchSignedUrl({filename:state.filePath+state.uploadedFiles[index].name,filetype:state.uploadedFiles[index].type},async function(res) {
            if(res && res.data && res.data.error == 0) {
                let filePath = res.data.path;
                let uploadStatus = await uploadS3(res.data.signedUrl,index);
                state.activeFileCount--;
                state.activeBandWidth -= state.uploadedFiles[index].size;
                setState((prev) => ({ ...prev, activeFileCount:state.activeFileCount,activeBandWidth:state.activeBandWidth }));
                if(uploadStatus) {
                    state.uploadedFiles[index].uploadStatus = 3; //upload completed
                    state.uploadedFiles[index].filePath = filePath;
                    setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles }));
                } else {
                    state.uploadedFiles[index].uploadStatus = 4; //error in uploading
                    setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles }));
                }
                uploadStart(newIndex,1);
            } else {
                message.error('Error occured while uploading');
                state.uploadedFiles[index].uploadStatus = 4;  //error in uploading
                state.activeFileCount--;
                state.activeBandWidth -= state.uploadedFiles[index].size;
                setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles,activeFileCount:state.activeFileCount,activeBandWidth:state.activeBandWidth }));
                uploadStart(newIndex);
            }
        });
    }
    const uploadS3  = (signedUrl,index=0) => {
        return new Promise((resolve, reject) => {
            var config = {
                headers: {
                    // 'Content-type': state.uploadedFiles[index].type
                    'Content-type': 'multipart/form-data'

                },
                onUploadProgress: function(progressEvent) {
                    var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                    state.uploadedFiles[index].percentCompleted = percentCompleted;
                    setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles }));
                    if(props.onProgress && typeof props.onProgress === "function") {
                        props.onProgress(state.uploadedFiles[index]);
                    }  
                }
            };
            uploadFiletoSignedUrl(signedUrl, state.uploadedFiles[index],config).then(function (res) {
                if(res.status==200) {
                    if(props.onProgress && typeof props.onSuccess === "function") {
                        let fileDetails = {
                            error:0,
                            url:res.data.path,
                            message:'Uploaded successfully',
                            fileKey:state.uploadedFiles[index].fileKey,
                            index:index
                        };
                        props.onSuccess(fileDetails);
                    }
                    resolve(true);
                } else {
                    if(props.onProgress && typeof props.onSuccess === "function") {
                        let fileDetails = {
                            error:1,
                            message:'Error in file Upload',
                            fileKey:state.uploadedFiles[index].fileKey,
                            index:index
                        };
                        props.onError(fileDetails);
                    }
                    message.error('Error occured while uploading');
                    resolve(false);
                }
            }).catch(function() {
                if(props.onProgress && typeof props.onSuccess === "function") {
                    let fileDetails = {
                        error:1,
                        message:'Error in file Upload',
                        fileKey:state.uploadedFiles[index].fileKey,
                        index:index
                    };
                    props.onError(fileDetails);
                }
                message.error('Error occured while uploading');
                resolve(false);
            })
        });
    }
    const validateFile  =  (files) => {
        let totalFiles  = files.length;
        if(state.fileMaxCount && totalFiles > state.fileMaxCount) {
            return message.error('You can only choose a maximum of '+state.fileMaxCount+' file(s)');
        } else {
            if(state.fileType || state.fileMaxSize) {
                for (let i=0;i<totalFiles;i++) {
                    let extension = files[i].name.split('.').pop().toLowerCase();
                    if(state.fileType) {
                        let allowedExtension = state.fileType.split(',');
                        if(allowedExtension.length > 0 && allowedExtension.indexOf('.'+extension) < 0) {
                            return message.error('Please upload file(s) having '+state.fileType+' extension');
                        }
                    }
                    if(state.fileMaxSize && state.fileMaxSize > 0) {
                        let fileBytes = state.fileMaxSize * 1024 * 1024;
                        if(files[i].size > fileBytes) {
                            return message.error('plese upload files(s) size below '+state.fileMaxSize+' mb');
                        }
                    }
                }
            }  
        }
        return true;

    } 
    const removeFile = (index) => {
        state.uploadedFiles.splice(index,1);
        setState((prev) => ({ ...prev, uploadedFiles:state.uploadedFiles }));
    }
    // const generateVideoThumbnail = (file,videoIndex,callback) => {
    //     var url = URL.createObjectURL(file);
    //     var videoId = 'video_file_'+videoIndex;
    //     var video = document.getElementById(videoId);
    //     var timeupdate = function() {
    //         if (snapImage()) {
    //             video.removeEventListener('timeupdate', timeupdate);
    //             video.pause();
    //         }
    //     };
    //     video.addEventListener('loadeddata', function() {
    //         if (snapImage()) {
    //             video.removeEventListener('timeupdate', timeupdate);
    //         }
    //     });
    //     var snapImage = function() {
    //         var canvas = document.getElementById('canvas_file_'+videoIndex);
    //         canvas.width = video.videoWidth;
    //         canvas.height = video.videoHeight;
    //         canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
    //         var image = canvas.toDataURL();
    //         callback(image);
    //         var success = image.length > 100000;
    //         return success;
    //     };
    //     video.addEventListener('timeupdate', timeupdate);
    //     video.preload = 'metadata';
    //     video.src = url;
    //     video.muted = true;
    //     video.playsInline = true;
    //     video.play();
    // }
    const fetchSignedUrl = (params,callback) => {
        generateSignedUrl(params) 
        .then(function (result) {
            callback(result);
        });
    }

    return (
        <div>
            <input id={'file-input-'+state.elementKey} accept={state.fileType} multiple onChange={(event) => onFileChange(event)} className="hide" type="file" />
            <ul className="f-upload-setup">
                {/* {state.videoUploads && state.videoUploads.map((val, i) => {
                    return (<div key={'video_'+i}><video id={'video_file_'+i} className='hide'></video>
                            <canvas id={'canvas_file_'+i}  className='hide'></canvas></div>)
                })} */}
                <li className="f-first"><label htmlFor={'file-input-'+state.elementKey}><img src={require('../../assets/images/icon/attachment.png')} /></label></li>
                 {state.enablePreview==1 && state.uploadedFiles && state.uploadedFiles.map((img, i) => {
                    return (
                        (img.previewType=='image' ) && (img.imageSrc || img.filePath) ? 
                            <li key={i} className={img.uploadStatus==1 ? 'image-upload-pending' : img.uploadStatus==2 ? 'image-upload-progress':img.uploadStatus==4 ? 'image-upload-error':''}>
                                {img.uploadStatus==2 &&  <Progress style={{position: "absolute"}} type="circle" percent={img.percentCompleted} width={50} strokeWidth={11}/>}
                                <img src={img.imageSrc || img.filePath || require('../../assets/images/icon/land10.png')} />
                                {img.uploadStatus==4 && <label onClick={()=>reUpload(i)}>Retry</label>}
                                {img.uploadStatus!=2 && <Button onClick={() => removeFile(i)} />}
                            </li>
                        :
                        <li key={i} className={img.uploadStatus==1 ? 'image-upload-pending' : img.uploadStatus==2 ? 'image-upload-progress':img.uploadStatus==4 ? 'image-upload-error':''}>
                            {img.uploadStatus==2 && <Progress style={{position: "absolute"}} type="circle" percent={img.percentCompleted} width={50} strokeWidth={11}/>}
                            <img src={require('../../assets/images/icon/land10.png')} />
                            {img.uploadStatus==4 && <label onClick={()=>reUpload(i)}>Retry</label>}
                            {img.uploadStatus!=2 && <Button onClick={() => removeFile(i)}/>}
                        </li>)
                })}  
            </ul>
        </div>
    );
    
}
export default FileUpload;