import React, { useState, useEffect } from 'react'
import { Card,Icon,Modal } from 'antd'
import * as moment from 'moment'
const PaymentCard = ({ item ,forUsers}) => {
    const [state, setState] = useState({
        group_id: undefined,
        group_name: '',
        id: undefined,
        logo: null,
        membership_fees: undefined,
        membership_from: undefined,
        membership_id: undefined,
        membership_name: undefined,
        membership_paid_on: undefined,
        membership_payed_amount: undefined,
        membership_to: undefined,
        membership_total_payed_amount: undefined,
        paid_on: undefined,
        payment_status:'',
        membership_payment_notes:''
    })
    let { group_name, logo, membership_fees, membership_name, membership_payed_amount, membership_to, membership_total_payed_amount,payment_status,membership_payment_notes
    } = state

    useEffect(() => {
        if (item) {
            let { group_id, group_name:g_name,full_name, id, logo:lg, membership_fees:m_f, membership_from, membership_id, membership_name:m_name,
                membership_paid_on, membership_payed_amount:mpa, membership_to:mt, membership_total_payed_amount:mtpa, paid_on,propic,payment_status:p_status,
                membership_payment_notes:mpn
            } = item

            setState(prev => ({
                ...prev,
                group_id: group_id,
                group_name:forUsers ? g_name:full_name,
                id: id,
                logo: forUsers ? lg:propic,
                membership_fees: m_f,
                membership_from: membership_from,
                membership_id: membership_id,
                membership_name: m_name,
                membership_paid_on: membership_paid_on,
                membership_payed_amount: mpa,
                membership_to: mt,
                membership_total_payed_amount: mtpa,
                paid_on: paid_on,
                payment_status:p_status,
                membership_payment_notes:mpn
            }))
        }
    }, [item])
    const success=()=> {
        Modal.info({
          content: `${membership_payment_notes}`,
          maskClosable:true
        });
      }
    return (
        <Card
            style={{
                width: '100%',
                marginBottom: '10px',
                borderRadius: '5px',
                overflow:'hidden',
                // border: 'white',
            }}>
            <div style={{
                display: 'flex',

            }}>
                <div>
                    <img
                        style={{
                            width: '100px',
                            height: '100px',
                            marginRight: '15px'
                        }}
                        onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = "../../assets/images/family_logo.png";
                        }}
                        src={logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + `${forUsers?'logo':'propic'}/` + logo : require("../../assets/images/family_logo.png")} />
                </div>
                <div
                    style={{
                        flexGrow: '1'
                    }}
                >
                    <h1>{group_name}</h1>
                    <span>{membership_name}</span>
                    <h2>{`Till : ${moment.unix(membership_to).format("DD MMM YYYY")}`}</h2>
                    <span>{`Payment Due : ${membership_fees-(membership_total_payed_amount)}`}</span>
                </div>
                <div
                    style={{
                        width:'60px',
                        flexDirection: 'column',
                        alignContent:'stretch'
                    }}
                >
                    <div>
                        <h2>{`$${membership_payed_amount}`}</h2>
                    </div>
                    <div>
                        <Icon onClick={()=>success()} type="book" />
                    </div>
                    <div>
                        <span
                            style={{
                                position: "absolute",
                                width:' 100px',
                                height: '12px',
                                top: '70%',
                                right: '0',
                                transform: 'rotate(135deg)',
                                transformOrigin: '55px 27px',
                                background: payment_status&&(payment_status.toLowerCase()==="success"||payment_status.toLowerCase()=== 'completed')?'rgb(98, 250, 46)':'rgb(248, 237, 17)'
                            }}
                            ></span>
                    </div>
                </div>
            </div>
        </Card>
    )
}

export default PaymentCard