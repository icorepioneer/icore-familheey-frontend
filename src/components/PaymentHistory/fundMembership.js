import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';
import PaymentCard from './membCard'
import {Spin,DatePicker} from 'antd'
import * as moment from 'moment'
const PaymentCardMembership = ({ paymentHistoryByUserid, userId, groupId, forUsers }) => {
    const [state, setState] = useState({
        list: undefined,
        user_id: useSelector(stateX => stateX.user_details.userId),
        type: "membership",
        loader: true,
        endDate:moment(new Date()).unix(),
        startDate:moment(new Date()).subtract(60,'d').unix()
    })

    let { endDate,startDate, list, type, loader } = state

    const fetchList = () => {
        let params = {
            user_id: `${userId}`,
            type: type,
            group_id: groupId ? `${groupId}` : null
        }
        paymentHistoryByUserid(params).then(res => {
            let { data } = res.data
            setState(prev => ({ ...prev, list: data, loader: false }))
        })
    }

    useEffect(() => {
        fetchList()
    }, [])
    // console.log(list)
    const whatTorender = () => {
        switch (list) {
            case undefined:
                if (loader) {
                    return (<div style={{ height: '600px', textAlign: 'center', paddingTop: '300px' }}><Spin /></div>)
                }
                break;
            default:
                switch (list.length > 0) {
                    case true:
                        return (
                            <React.Fragment>
                                {list.map((item) =>
                                    <PaymentCard
                                        key={item.id}
                                        item={item}
                                        forUsers={forUsers}
                                    />
                                )}
                            </React.Fragment>
                        )
                    case false:
                        return (
                            <div style={{ height: '600px', textAlign: 'center', paddingTop: '300px' }}>
                                Nothing found
                            </div>
                        )
                }

        }
    }
    const setDate=(tag,e)=>{
        setState(prev=>({...prev,[`${tag}`]:moment(e).unix()}))
    }
    useEffect(()=>{
        fetchList()
    },[endDate,startDate])
    return (
        <React.Fragment>
            <div style={{display:'flex',justifyContent:'space-between',marginBottom:'10px'}}>
            <React.Fragment>
            <span style={{paddingTop:'5px'}}>From : {" "}</span>
            <DatePicker
                format="MM/DD/YYYY"
                value={moment(moment.unix(startDate))}
                placeholder="From"
                onChange={(e)=>setDate('startDate',e)}
            />
            </React.Fragment>
            <React.Fragment>
            <span style={{paddingTop:'5px'}}>To : {" "}</span>
            <DatePicker
                format="MM/DD/YYYY"
                value={moment(moment.unix(endDate))}
                placeholder="To"
                onChange={(e)=>setDate('endDate',e)}
            />
            </React.Fragment>
            </div>
            {
                whatTorender()
            }
        </React.Fragment>
    )
}
export default PaymentCardMembership