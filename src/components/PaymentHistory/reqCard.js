import React, { useState, useEffect } from 'react'
import { Card,Icon,Modal } from 'antd'
import * as moment from 'moment'
const ReqPaymentCard = ({ item,forUsers }) => {
    const [state, setState] = useState({
        contribute_item_quantity: 0,
        group_name: undefined,
        is_anonymous: undefined,
        item_id: undefined,
        logo: undefined,
        paid_on: undefined,
        payment_status: '',
        request_item_title: '',
        payment_note:''
    })
    let { contribute_item_quantity,group_name,logo,paid_on,payment_status,request_item_title,payment_note } = state

    useEffect(() => {
        if (item) {
            let { contribute_item_quantity:ciq,group_name:g_name,is_anonymous,item_id,logo:lg,paid_on:p_on,payment_status:ps,request_item_title:rit,propic,full_name,payment_note:pn } = item
            setState(prev => ({
                ...prev,
                contribute_item_quantity: ciq,
                group_name: forUsers? g_name:full_name,
                is_anonymous: is_anonymous,
                item_id: item_id,
                logo: forUsers?lg:propic,
                paid_on: p_on,
                payment_status: ps,
                request_item_title: rit,
                payment_note:pn
            }))
        }
    }, [item])
    const success=()=> {
        Modal.info({
          content: `${(payment_note === null||payment_note === '')?'Nothing provided':payment_note}`,
          maskClosable:true
        });
      }
    return (
        <Card
            style={{
                width: '100%',
                marginBottom: '10px',
                borderRadius: '5px',
                overflow: 'hidden',
                // border: 'white',
            }}>
            <div style={{
                display: 'flex',

            }}>
                <div>
                    <img
                        style={{
                            width: '100px',
                            height: '100px',
                            marginRight: '15px'
                        }}
                        onError={(e) => {
                            e.target.onerror = null;
                            e.target.src = "../../assets/images/family_logo.png";
                        }}
                        src={logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + `${forUsers?'logo':'propic'}/` + logo : require("../../assets/images/family_logo.png")} />
                </div>
                <div
                    style={{
                        flexGrow: '1'
                    }}
                >
                    <h1>{group_name}</h1>
                    <span>{`For  ${request_item_title}`}</span>
                    <h2></h2>
                    <span>{moment.unix(paid_on).format("MMM DD YYYY")}</span>
                </div>
                <div
                    style={{
                        width: '60px',
                        flexDirection: 'column',
                        alignContent: 'stretch'
                    }}
                >
                    <div>
                        <h2>{`$${contribute_item_quantity}`}</h2>
                    </div>
                    <div>
                        <Icon onClick={()=>success()} type="book" />
                    </div>
                    <div>
                        <span
                            style={{
                                position: "absolute",
                                width: ' 100px',
                                height: '12px',
                                top: '70%',
                                right: '0',
                                transform: 'rotate(135deg)',
                                transformOrigin: '55px 27px',
                                background: (payment_status.toLowerCase()==="success"||payment_status.toLowerCase()=== 'completed')?'rgb(98, 250, 46)':'rgb(248, 237, 17)'
                            }}
                        ></span>
                    </div>
                </div>
            </div>
        </Card>
    )
}

export default ReqPaymentCard