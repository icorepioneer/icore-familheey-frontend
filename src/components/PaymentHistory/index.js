import React from 'react'
import { Tabs } from 'antd'
import PaymentCardFundRequest from './fundRequests'
import PaymentCardMembership from './fundMembership'
const { TabPane } = Tabs
const PaymentHistory = ({paymentHistoryByUserid,userId,groupId,forUsers}) => {
    return (
        <div 
        style ={{
            backgroundColor:'#FFF',
            padding:'15px'
            }}>
            <Tabs 
            destroyInactiveTabPane 
            size="large"
            tabBarGutter={2}
            >
                <TabPane tab="FUND REQUEST" key="1">
                    <PaymentCardFundRequest
                        paymentHistoryByUserid={paymentHistoryByUserid}
                        userId={userId}
                        groupId={groupId}
                        forUsers={forUsers}
                    />
                </TabPane>
                <TabPane tab="MEMBERSHIP" key="2">
                    <PaymentCardMembership
                        paymentHistoryByUserid={paymentHistoryByUserid}
                        userId={userId}
                        groupId={groupId}
                        forUsers={forUsers}
                    />
                </TabPane>
            </Tabs>
        </div>
    )
}
export default PaymentHistory