import React from 'react';
import './style.scss'
import { Menu } from 'antd';
import { viewFamily } from '../../modules/family/familyList/api-familyList'
import { encryption } from '../../shared/encryptDecrypt';
import { getSidebarCount } from '../../modules/Post/post_service'
import EventListSide from '../EventListing/index';
import ActiveFamilyList from "../ActiveFamilyList/index";
import AnnouncementList from '../Announcements/index';
import FeedbackModal from '../FeedbackModal/feedBack';
import {connect} from 'react-redux'

const { SubMenu } = Menu;

class PostsSidebar extends React.Component {
    state;
    constructor(props) {
        super(props);
        let {userID,sideBarLoaded,sideBar_count} = props
        this.state = {
            user_id: userID,
            familyList: [],
            feedbackModel: false,
            upComingEventList: [],
            description: '',
            title: '',
            image: '',
            count: 0,
            sidebar_data:sideBar_count?sideBar_count:{},
            selectedKey: this.props.defaultKey
        }
        !sideBarLoaded&&this.showFamilyList();
        this.showFamilyList();//added to update family count:Anand
    }
    showFamilyList = () => {
        let { dispatch } = this.props
        getSidebarCount({ user_id: this.state.user_id })
            .then(response => {
                dispatch({type:'COUNT_ADD',payload:{data:response.data}})
                this.setState({ sidebar_data: response.data })
                //if(this.props.familyCountRes) this.props.familyCountRes(response.data.family_count);
            });
    }
    navigate = (path, obj) => {
        if (obj) {
            this.props.history.push(path, obj);
        } else {
            this.props.history.push(path);
        }
    }
    navigateToFamily = (value) => {
        var id = value;
        var familyId = encryption(id)
        this.props.history.push(`/familyview/${familyId}`);
    }
    openFeedbackModel = () => {
        this.setState({ feedbackModel: true })
    }
    callback = (key) => {
        this.setState({ feedbackModel: key })
    }
    navigateToEvents = (key) => {
        if (window.location.pathname.includes('/events') && this.props.eventTabSwitch) {
            this.props.eventTabSwitch(key);
            this.setState({ selectedKey: key == 'explore' ? 4 : key == 'invitation' ? 7 : key == 'myevent' ? 8 : 4 });
        } else {
            this.props.history.push(`/events`, {
                key: key
            });
        }
    }
    toggleFeedbackModel=()=>{
        this.setState({
            feedbackModel:!this.state.feedbackModel
        })
    }
    render() {
        let { defaultOpen } = this.props
        return (
            <aside className="PostsSidebar f-sider f-force-hide-tab">
                {/* Sidebar Block */}
                <div className="f-sider-block f-sider-menu-incl">
                    <Menu
                        mode="inline"
                        defaultSelectedKeys={[`${this.state.selectedKey}` || '1']}
                        className="f-sider-menu"
                        defaultOpenKeys={[`${defaultOpen}`]}
                    >
                        <SubMenu
                            key="sub2"
                            title={
                                <span>
                                    <img src={require('../../assets/images/icon/land1.svg')} />
                                    <span>Feed</span>
                                    <i>{this.state.sidebar_data['post_count']}</i>
                                </span>
                            }
                        >
                            <Menu.Item key="1" onClick={() => {
                                this.props.postTabSwitch ? this.props.postTabSwitch('family') : this.props.history.push('/post', { type_id: 1 })
                            }}>
                                <span>My Familheey</span>
                            </Menu.Item>
                            <Menu.Item key="9" onClick={() => {
                                this.props.postTabSwitch ? this.props.postTabSwitch('public') : this.props.history.push('/post', { type_id: 2 })
                            }}>
                                <span>Public</span>
                            </Menu.Item>
                        </SubMenu>
                        <Menu.Item key="16" onClick={() => { this.props.history.push('/requests') }}>
                            <img src={require('../../assets/images/icon/icon-requests.svg')} />
                            <span>Requests</span>
                        </Menu.Item>
                        { parseInt(this.state.sidebar_data['family_count'])>0 &&  <Menu.Item key="2" onClick={() => { this.props.history.push('/announcement') }}>
                            <img src={require('../../assets/images/icon/land2.svg')} />
                            <span>Announcements</span>
                            <i>{this.state.sidebar_data['announcement_count']}</i>
                        </Menu.Item>}
                        <Menu.Item key="10" onClick={() => { this.props.history.push('/topics') }}>
                            <img src={require('../../assets/images/icon/chat.svg')} />
                            <span>Messages</span>
                        </Menu.Item>
                        {/* <Menu.Item key="10" onClick={() => { this.props.history.push('/messages') }}>
                            <img src={require('../../assets/images/icon/chat.svg')} />
                            <span>Messages</span>
                            <i>5</i>
                        </Menu.Item> */}
                        <Menu.Item key="3" onClick={() => { this.props.history.push('/my-family') }}>
                            <img src={require('../../assets/images/icon/land3.svg')} />
                            <span>Families</span>
                            <i>  {this.state.sidebar_data['family_count']} </i>
                        </Menu.Item>
                        <SubMenu
                            key="sub1"
                            title={
                                <span>
                                    <img src={require('../../assets/images/icon/land4.svg')} />
                                    <span>Events</span>
                                    <i>{this.state.sidebar_data['my_event_count'] ? this.state.sidebar_data['my_event_count'] : 0} </i>
                                </span>
                            }
                        >
                            <Menu.Item key="4" onClick={() => this.navigateToEvents('explore')}>
                                <span>Explore</span>
                            </Menu.Item>
                            <Menu.Item key="7" onClick={() => this.navigateToEvents('invitation')}>
                                <span>Invitations</span>
                            </Menu.Item>
                            <Menu.Item key="8" onClick={() => this.navigateToEvents('myevent')}>
                                <span>My Events</span>
                            </Menu.Item>
                        </SubMenu>
                        <Menu.Item key="5" onClick={() => this.props.history.push('/calender')}>
                            <img src={require('../../assets/images/icon/land5.svg')} />
                            <span>Calendar </span>
                        </Menu.Item>
                        <Menu.Item key="6" onClick={() => this.openFeedbackModel()}>
                            <img src={require('../../assets/images/icon/land6.svg')} />
                            <span>Feedback</span>
                        </Menu.Item>
                    </Menu>
                    {this.state.feedbackModel &&
                        <FeedbackModal toggleFeedbackModel={this.toggleFeedbackModel} feedbackModel={this.state.feedbackModel} callbackFromParent={this.callback}></FeedbackModal>
                    }
                </div>
                <AnnouncementList history={this.props.history}></AnnouncementList>
                <ActiveFamilyList familyList={this.state.familyList} history={this.props.history}></ActiveFamilyList>
                <EventListSide history={this.props.history} ></EventListSide>
            </aside>
        )
    }
}
const mapStateToProps=(state)=>{
    let {user_details,side_bar_data} = state;
    let { userId } = user_details
    let { sideBarCount_loaded,sideBarCount } = side_bar_data
    return {
        userID:userId,
        sideBar_count:sideBarCount,
        sideBarLoaded:sideBarCount_loaded
    }
}
export default connect(mapStateToProps)(PostsSidebar);