import React from 'react';
import './style.scss'
import { Button, Avatar, Modal, Steps, Icon, Input, Select, Upload, Switch, Checkbox, Drawer, List, Radio, DatePicker, TimePicker, Spin } from 'antd';
import { createFamily, checkforDuplicateFamily, joinToExistingFamily, updateFamily, getAllLinkedFamily, requestLinkFamily, createEvent, checkEventLink } from '../../modules/createFamily/api-family'
import { viewFamily } from '../../modules/family/familyList/api-familyList';
import { notifications } from '../../shared/toasterMessage';
import { encryption,decryption } from '../../shared/encryptDecrypt';
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import ReactGoogleMapLoader from "react-google-maps-loader"
import {connect} from 'react-redux'
const { Step } = Steps;
const { Option } = Select;
const { TextArea } = Input;
const { confirm } = Modal;
const MY_API_KEY = decryption(process.env.REACT_APP_PLACE_ENCRYPT)
var moment = require('moment');
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
// Sample

// const fileList = [
//     {
//         uid: '-1',
//         name: 'coverphoto.png',
//         status: 'done',
//         url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
//         thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
//     },
//     {
//         uid: '-2',
//         name: 'logo.png',
//         status: 'done',
//         url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
//         thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
//     },
// ];
// const _props = {
//     action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
//     listType: 'picture',
//     // defaultFileList: [...fileList],
// };
// Sample-end

class EventSidebar extends React.Component {
    state;
    constructor(props) {
        super(props);
        let {userID} = props
        this.state = {
            user_id: userID,
            familyName: '',
            visible: false,
            familyType: '',
            location: '',
            current: 0,
            familyIntro: '',
            coverpic: '',
            propic: '',
            visibility: true,
            searchable: true,
            is_linkable: false,
            showAdvSetting: false,
            drawervisible: false,
            checkedGroups: [],
            member_joining: 2,
            member_approval: 4,
            post_create: 6,
            post_visibilty: 8,
            post_approval: 10,
            link_family: 13,
            link_approval: 16,
            request_visibility:27,
            disableNext: false,
            eventModal: false,
            categoryList: [
                "Activities",
                "Art",
                "Business",
                "Causes",
                "Crafts",
                "Culture",
                "Dance",
                "Death",
                "Education",
                "Education Training",
                "Environment",
                "Family",
                "Fashion",
                "Film",
                "Food",
                "Health and wellness",
                "Investment and Finance",
                "Literature",
                "Movies",
                "Music",
                "Networking",
                "Others",
                "Party",
                "Real Estate",
                "Religion",
                "Re-union",
                "Sports",
                "Theatre",
                "Travel and Tourism",
            ],
            event_type: '',
            public_event: false,
            nextpage: false,
            rsvp: false,
            event_name: '',
            event_host: '',
            description: '',
            event_page: '',
            loader: true,
            joinSuggesion: false,
            groupList: [],
            familyDetails: {},
            loading: false,
            event_image: '',
            from_date: '',
            to_date: '',
            start_date: '',
            start_time: '',
            end_date: '',
            end_time: '',
            familyList: [],
            count: 0,
            allow_others: false,
            is_sharable: false,
            search: "",
            value: "",
            _user: JSON.parse(sessionStorage.getItem('jwt')),
            full_name: '',
            _loading: false
        }

        this.showFamilyList(0);
    }
    componentDidUpdate = () => {
        this.state.eventModal = this.props.eventModal;
    }
    showFamilyList = (offset) => {
        var params = {
            user_id: this.state.user_id,
            offset: this.state.count + offset,
            limit: 3
        }
        viewFamily(params).then((response) => {
            if (response && response.data && response.data.data) {
                var concatArray = this.state.familyList.concat(response.data.data)
                this.setState({ familyList: concatArray, count: this.state.count + offset, loader: false })
            } else {
                this.setState({ loader: false })
            }
        }).catch((error) => {
            console.log('error--->', error);
        })
    }
    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleCancel = () => {
        // console.log(this.props)
        this.props.eventfn(false)
        this.setState({ visible: false, eventModal: false, current: 0 });
    };
    handleChange = (name) => e => {
        this.setState({ [name]: e.target.value })
    }
    handleSelect = (name) => event => {
        this.setState({ [name]: event })
    }
    createFamily = () => e => {
        if (!this.state.familyName || !this.state.familyType || !this.state.location) {
            var message = 'Error'
            var description = `Please fill all the required fields`
            notifications('error', message, description)
            return;
        }
        if (this.state.current === 0) {
            var familyDetails = {
                group_name: this.state.familyName,
                group_category: this.state.familyType,
                base_region: this.state.location,
                user_id: this.state.user_id
            }
            this.setState({ disableNext: true, familyDetails: familyDetails }, () => {
                this.checkDuplicateFamily();
            })
        } else {
            if (this.state.current < 4) {
                var current = this.state.current + 1;
                this.setState({ current: current, disableNext: false });
            }

        }
    }
    checkDuplicateFamily = () => {
        checkforDuplicateFamily(this.state.familyDetails).then((result) => {
            console.log('duplcate', result);
            var $this = this
            if (result && result.data && result.data.data.length > 0) {
                // var message = 'This group exist, do you want to merge with it ?';
                // familyDetails.group_id = result.data.data[0].id;
                $this.setState({ joinSuggesion: true, groupList: result.data.data })
                // localStorage.setItem('group_id', result.data.data[0].id);
                // $this.confirmCreation(familyDetails, message)
            } else {
                $this.createNewFamily()
            }
        }).catch((err) => {
            console.log('errorr', err)
        })
    }
    // confirmCreation = (params, message) => {
    //     var $this = this
    //     confirm({
    //         title: message,
    //         okText: 'Yes',
    //         cancelText: 'No',
    //         onOk() {
    //             $this.joinExistingFamily(params);
    //         },
    //         onCancel() {
    //             $this.createNewFamily(params);
    //         },
    //     });
    // }
    createNewFamily = () => {
        var params = this.state.familyDetails
        params.created_by = params.user_id;
        createFamily(params).then((response) => {
            console.log('new family created', response);
            var current = this.state.current + 1;
            this.setState({ group_id: response.data.data[0].id, current: current, disableNext: false, joinSuggesion: false });
            localStorage.setItem('group_id', response.data.data[0].id);
        }).catch((error) => {
            console.log('error while creating', error);
        })
    }

    joinExistingFamily = (data) => e => {
        var params = {
            user_id: this.state.user_id,
            group_id: JSON.stringify(data.id)
            // group_id: '347'
        }
        joinToExistingFamily(params).then((data1) => {
            console.log('JOINED successfully', data1);
            var current = 0;
            this.setState({ current: current, disableNext: false, visible: false, familyName: '', familyType: '', location: '' });
        }).catch((err) => {
            console.log('err while joining', err);
        })
    }
    previousPage = () => e => {
        if (this.state.current > 0 && this.state.joinSuggesion === false) {
            var current = this.state.current - 1;
            this.setState({ current: current });
        } else if (this.state.joinSuggesion === true) {
            this.setState({ joinSuggesion: false });
        }
    }
    uploadPhoto = name => info => {
        if (info.file.status === 'uploading') {
            if (name === 'cover' || name === 'event_image')
                this.setState({ loading: true });
            else
                this.setState({ logoLoad: true });
            return;
        }
        if (info.file.status === 'done') {
            if (name === 'cover' || name === 'event_image') {
                this.getBase64(info.file.originFileObj, name, imageUrl =>
                    this.setState({
                        imageUrl,
                        loading: false,
                    }),
                );
            } else {
                this.getBase64(info.file.originFileObj, 'logo', logoUrl =>
                    this.setState({
                        logoUrl,
                        logoLoad: false,
                    }),
                );
            }
        }
    }

    getBase64(img, type, callback) {
        console.log('imggg', img);
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
        if (type === 'cover') {
            this.setState({ coverpic: img })
        } else if (type === 'event_image') {
            this.setState({ event_image: img })
        }
        else {
            this.setState({ propic: img })
        }
    }

    showAdvSettings = () => {
        this.setState({ showAdvSetting: true })
    }
    updateFamily = () => {
        const formData = new FormData();
        console.log('formData', this.state.visibility);
        formData.append('id', this.state.group_id.toString());
        formData.append('logo', this.state.propic);
        formData.append('cover_pic', this.state.cover_pic);
        formData.append('base_region', this.state.location);
        formData.append('created_by', this.state.user_id);
        formData.append('intro', this.state.familyIntro);
        formData.append('is_active', true);
        formData.append('searchable', this.state.searchable);
        formData.append('group_type', this.state.familyType);
        formData.append('visibility', this.state.visibility);
        formData.append('member_joining', this.state.member_joining);
        formData.append('member_approval', this.state.member_approval);
        formData.append('post_create', this.state.post_create);
        formData.append('post_approval', this.state.post_approval);
        formData.append('post_visibilty', this.state.post_visibilty);
        formData.append('link_family', this.state.link_family);
        formData.append('link_approval', this.state.link_approval);
        formData.append('is_linkable', this.state.is_linkable);
        updateFamily(formData).then((response) => {
            this.setState({ visible: false })
        }).catch((error) => {

        })
    }
    listLinkFamily = () => e => {
        console.log('this.state.is_linkable', this.state.is_linkable)
        var params = {
            user_id: this.state.user_id,
            // group_id: this.state.group_id
            group_id: '630'
        }
        getAllLinkedFamily(params).then((response) => {
            console.log('response', response)
            if (response.data && response.data.data.length > 0) {
                this.setState({ drawervisible: true, linkFamilyList: response.data })
            }
        }).catch((error) => {
            console.log('error', error)
        })
    }
    onClose = () => {
        this.setState({ drawervisible: false })
    };
    checkedValue = (value) => e => {
        if (this.state.checkedGroups.indexOf(value) === -1) {
            this.state.checkedGroups.push(value);
            this.setState({ checkedGroups: this.state.checkedGroups })
        } else {
            var checkedGroups = [];
            checkedGroups = this.state.checkedGroups.filter(function (item) {
                return item !== value
            })
            this.setState({ checkedGroups: checkedGroups })
        }
        console.log('this.state.checkedGroups', this.state.checkedGroups);
    }
    linkToSelectedGroups = () => {
        var params = {
            to_group: this.state.checkedGroups,
            from_group: this.state.group_id,
            requested_by: this.state.user_id
        }
        requestLinkFamily(params).then((response) => {
            console.log('response', response);
        }).catch((error) => {
            console.log('error', error);
        })
    }

    showEventModal = () => {
        this.setState({ eventModal: true })
    }
    createEvent = () => {
        this.setState({
            _loading: !this.state._loading
        })
        var from_date = this.state.start_date + ',' + this.state.start_time;
        var to_date = this.state.end_date + ',' + this.state.end_time;
        const formData = new FormData();
        this.setState({ from_date: (Date.parse(from_date)) / 1000, to_date: (Date.parse(to_date)) / 1000 }, () => {
            formData.append('event_name', this.state.event_name);
            formData.append('event_type', this.state.event_type);
            formData.append('event_host', this.state.event_host);
            formData.append('category', this.state.category);
            formData.append('created_by', this.state.user_id);
            formData.append('location', this.state.location);
            formData.append('from_date', this.state.from_date);
            formData.append('to_date', this.state.to_date);
            formData.append('is_public', !this.state.public_event);
            formData.append('is_sharable', this.state.is_sharable);
            formData.append('allow_others', this.state.allow_others);
            formData.append('rsvp', this.state.rsvp);
            formData.append('description', this.state.description);
            formData.append('event_image', this.state.event_image);
            formData.append('event_original_image', this.state.event_image);

            if (this.state.event_page)
                formData.append('event_page', this.state.event_page);
        })
        createEvent(formData).then((data) => {
            notifications('success', 'Success', 'new event created successfully');
            this.handleCancel();
            this.setState({ eventModal: false, _loading: false })
            var eventId = encryption(data.data.data.id)
            this.props.history.push(`/event-details/${eventId}`);
        }).catch((error) => {
            notifications('error', 'Error', 'Something went wrong')
        })
    }
    checkEventLink = () => {
        var message = 'Error';
        var description = ''
        if (!this.state.location) {
            description = 'Please Provide Location';
            notifications('error', message, description);
            return;
        }
        else if (!this.state.start_date || !this.state.start_time) {
            description = 'Please Select Start Date and Time';
            notifications('error', message, description);
            return;
        } else if (!this.state.end_date || !this.state.end_time) {
            description = 'Please Select End Date and Time';
            notifications('error', message, description);
            return;
        }
        var params = {
            text: this.state.event_page
        }
        if (this.state.event_page) {
            checkEventLink(params).then((data) => {
                this.createEvent();
            }).catch((error) => {
                notifications('error', 'Error', 'link already exist')
            })
        } else {
            this.createEvent();
        }
    }
    navigateToFamily = (value) => e => {
        var id = value;
        var familyId = encryption(id)
        this.props.history.push(`/familyview/${familyId}`);
    }
    navgateToEvent = (value) => e => {
        var id = value;
        var eventId = encryption(id)
        this.props.history.push(`/event-details/${eventId}`);
    }
    handleTimeChange = (type) => event => {
        if (type === 'start_date' || type === 'end_date') {
            this.setState({ [type]: moment(event).format('YYYY-MM-DD') })
        }
        else
            this.setState({ [type]: moment(event).format('HH:mm') })
    }
    eventContinue = () => {
        var message = 'Error';
        var description = ''
        if (!this.state.event_name || (this.state.event_name && this.state.event_name.trim() == '')) {
            description = 'Please provide event name';
            notifications('error', message, description);
        } else if (!this.state.event_type) {
            description = 'Please select a event type';
            notifications('error', message, description)
        } else if (!this.state.category) {
            description = 'Please select a event category';
            notifications('error', message, description)
        } else {
            this.setState({ nextpage: true })
        }
    }
    handleInputChange = e => {
        console.log('e.target.value', e.target.value)
        this.setState({ search: e.target.value, value: e.target.value })
    }
    handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
        console.log(geocodedPrediction, originalPrediction) // eslint-disable-line
        this.setState({ search: "", value: geocodedPrediction.formatted_address })
        this.setState({ location: geocodedPrediction.formatted_address })
    }
    disabledDate = (date) => {
        return date && date < moment().startOf('day');
    }
    disabledEndDate = (date) => {
        return date && date < moment().startOf('day');
    }
    componentDidMount() {
        let jwt = JSON.parse(localStorage.getItem('jwt'))
        if (jwt && jwt.full_name) {
            this.setState({
                full_name: jwt.full_name
            })
        }

    }

    // handleNoResult = () => {
    //     console.log('No results for ', this.state.search)
    // }

    // handleStatusUpdate = (status) => {
    //     console.log(status)
    // }

    render() {
        const { search, value } = this.state
        const uploadButton = (
            <div>
                <Icon type={this.state.loading ? 'loading' : 'plus'} />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        const { imageUrl } = this.state;
        return (
            <aside className="eventSidebar f-clearfix">
                {/* Sidebar Block */}
                <div className="f-sider-block hide">
                    <div className="f-sider-head f-wadd">
                        <img src={require('../../assets/images/icon/land3.svg')} />
                        <h4>Familheey</h4>
                        <Button onClick={() => this.props.history.push(`/create-family`)}>+</Button>
                        {/* Create Family Modal - Start */}
                        <Modal
                            className="f-modal f-modal-wizard"
                            title="Create New Family"
                            visible={this.state.visible}
                            onOk={this.handleOk}
                            onCancel={this.handleCancel}
                            maskClosable={false}
                            footer={[
                                // <Button key="back" onClick={this.handleCancel}>
                                //     Return
                                // </Button>,
                                // <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
                                //     Submit
                                // </Button>,
                            ]}
                        >
                            {/* Steps */}
                            <Steps className="f-steps" progressDot current={this.state.current}>
                                <Step />
                                <Step />
                                <Step />
                                <Step />
                            </Steps>

                            {/* Body */}
                            <div className="f-modal-wiz-body">

                                {/* Step 1 */}
                                {this.state.joinSuggesion === false && this.state.current === 0 &&
                                    <div className="f-modal-wiz-step">
                                        <h4>Basic Details</h4>
                                        <div className="f-form-control">
                                            <label>Family Name</label>
                                            <Input id='familyName' placeholder="Eg: Nigeria womens group" onChange={this.handleChange('familyName')} />
                                        </div>
                                        <div className="f-form-control">
                                            <label>Family Type</label>
                                            <Select placeholder="Eg: Company" value={this.state.familyType} onChange={this.handleSelect('familyType')}>
                                                <Option value="public">Public</Option>
                                                <Option value="private">Private</Option>
                                                <Option value="regular">regular</Option>
                                            </Select>
                                        </div>
                                        <div className="f-form-control">
                                            <label>Base region or location</label>
                                            <Input placeholder="Eg: Newyork" onChange={this.handleChange('location')} />
                                        </div>
                                    </div>
                                }


                                {/* Step 1 - Create Suggestion */}
                                {this.state.joinSuggesion === true &&
                                    <div className="f-modal-wiz-step">
                                        <h4>Found families with similar names!</h4>
                                        <div className="f-form-control">
                                            <label>Jones, would you like to join any of these families instead of creating own your own?</label>
                                        </div>

                                        <div className="f-recommendation-list">
                                            {this.state.groupList && this.state.groupList.length > 0 &&
                                                this.state.groupList.map((item, index) => {
                                                    return <div key={index} className="f-card-recommendation">
                                                        <div className="f-east">
                                                            <span className="f-north">
                                                                <img src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../assets/images/demo/demo_u0.png')} />
                                                            </span>
                                                            <div className="f-south">
                                                                <h3>{item.group_name}</h3>
                                                                <h5><em>by,</em> {item.created_by_}</h5>
                                                                <h5 className="f-location">{item.base_region}
                                                                    {/* <em>Dallas, TX</em> */}
                                                                </h5>
                                                            </div>
                                                        </div>
                                                        <div className="f-west">
                                                            <div className="f-north">
                                                                <h5>{item.member_count} <em>members</em></h5>
                                                                <h5>{item.known_count} <em>known</em></h5>
                                                            </div>
                                                            <Button className="f-south" onClick={this.joinExistingFamily(item)}>Join</Button>
                                                        </div>
                                                    </div>
                                                })
                                            }
                                            {/* <div className="f-card-recommendation">
                                            <div className="f-east">
                                                <span className="f-north">
                                                    <img src={require('../../assets/images/demo/demo_recomm_2.png')} />
                                                </span>
                                                <div className="f-south">
                                                    <h3>Nigeria Women�s Group</h3>
                                                    <h5><em>by,</em> Alen Joe Root</h5>
                                                    <h5 className="f-location">Hotel Fairmount <em>Dallas, TX</em></h5>
                                                </div>
                                            </div>
                                            <div className="f-west">
                                                <div className="f-north">
                                                    <h5>5K <em>members</em></h5>
                                                    <h5>250 <em>unknown</em></h5>
                                                </div>
                                                <Button className="f-south">Join</Button>
                                            </div>
                                        </div> */}

                                        </div>

                                        <div className="f-form-control">
                                            <p className="f-clearfix text-center">10 more similar families to show &nbsp; | &nbsp; <Button type="link">Show all</Button></p>
                                            <label>I do not belongs to any of the families above</label>
                                        </div>

                                    </div>}
                                {/* Step 1 - Create Suggestion - End */}

                                {/* Step 2 */}
                                {this.state.current === 1 &&
                                    <div className="f-modal-wiz-step" >
                                        <h4>Family Details</h4>
                                        <div className="f-form-control">
                                            <label>A little bit about your family</label>
                                            <TextArea placeholder="250 characters max" rows={6} onChange={this.handleChange('familyIntro')} />
                                        </div>
                                    </div>
                                }


                                {/* Step 3 */}
                                {this.state.current === 2 &&
                                    <div className="f-modal-wiz-step" >
                                        <h4>Upload Photo</h4>
                                        <div className="f-form-control">
                                            <label>Please take a moment to upload your family Cover pic as well as Logo.</label>
                                            <div className="f-split">
                                                <div>
                                                    <Upload onChange={this.uploadPhoto('cover')}>
                                                        <Button>
                                                            <img src={require('../../assets/images/icon/upload.png')} /> Upload cover photo
                                                    </Button>
                                                    </Upload>
                                                </div>
                                                <div>
                                                    <Upload onChange={this.uploadPhoto('logo')}>
                                                        <Button>
                                                            <img src={require('../../assets/images/icon/upload.png')} /> Upload logo
                                                    </Button>
                                                    </Upload>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }


                                {/* Step 4 */}
                                {this.state.current === 3 &&
                                    <div className="f-modal-wiz-step" >
                                        <h4>Family Settings</h4>
                                        <div className="f-form-control f-wiz-switch">
                                            <ul>
                                                <li>
                                                    <h6>Make this family private</h6>
                                                    <Switch defaultChecked onChange={this.handleSelect('visibility')} />
                                                </li>
                                                <li>
                                                    <h6>Make this group searchable</h6>
                                                    <Switch defaultChecked onChange={this.handleSelect('searchable')} />
                                                </li>
                                                <li>
                                                    <h6>Link with other families as well</h6>
                                                    <Switch onClick={this.handleSelect('is_linkable')} /> {this.state.is_linkable && <Button type="link">Select</Button>}
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="f-form-control">
                                            <Button type="link" onClick={this.showAdvSettings}>Advanced Settings</Button>
                                        </div>
                                        {/* Advanced Settings */}
                                        {this.state.showAdvSetting &&
                                            <div className="f-advanced">
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>How members can join?</label>
                                                    <Radio.Group defaultValue={2} onChange={this.handleChange('member_joining')}>
                                                        <Radio value={1}>Invitation Only</Radio>
                                                        <Radio value={2}>Anyone can join<br /><small>(with approval)</small></Radio>
                                                        <Radio value={3}>Anyone can join</Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Who can create posts, events, albums and documents?</label>
                                                    <Radio.Group defaultValue={6} onChange={this.handleChange('post_create')}>
                                                        <Radio value={6}>Members</Radio>
                                                        <Radio value={7}>Admin only</Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Post Visibility</label>
                                                    <Radio.Group defaultValue={8} onChange={this.handleChange('post_visibilty')}>
                                                        <Radio value={8}>Members only</Radio>
                                                        <Radio value={9}>Any one</Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Who can approve posts?</label>
                                                    <Radio.Group defaultValue={10} onChange={this.handleChange('post_approval')}>
                                                        <Radio value={10}>Not needed</Radio>
                                                        <Radio value={11}>Admin</Radio>
                                                        <Radio value={12}>Members<br /><small>(with approval)</small></Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Who can link family?</label>
                                                    <Radio.Group defaultValue={13} onChange={this.handleChange('link_family')}>
                                                        <Radio value={13}>Admins</Radio>
                                                        <Radio value={14}>Member</Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Who can approve link request?</label>
                                                    <Radio.Group defaultValue={16} onChange={this.handleChange('link_approval')}>
                                                        <Radio value={15}>Member</Radio>
                                                        <Radio value={16}>Admins</Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Who can create announcements ?</label>
                                                    <Radio.Group defaultValue={17} onChange={this.handleChange('announcement_create')}>
                                                        <Radio value={17}>Admin only</Radio>
                                                        <Radio value={18}>Any member</Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Who can view announcemnts ?</label>
                                                    <Radio.Group defaultValue={21} onChange={this.handleChange('announcement_visibility')}>
                                                        <Radio value={22}>Public</Radio>
                                                        <Radio value={21}>Members</Radio>
                                                    </Radio.Group>
                                                </div>
                                                <div className="f-form-control f-wiz-checkbox">
                                                    <label>Who can view requests?</label>
                                                    <Radio.Group defaultValue={27} onChange={this.handleChange('request_visibility')}>
                                                        <Radio value={27}>Members</Radio>
                                                        <Radio value={26}>Admin only</Radio>
                                                    </Radio.Group>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                }


                            </div>

                            {/* Footer */}
                            <div className="f-modal-wiz-footer">
                                {this.state.current < 3 && this.state.joinSuggesion === false &&
                                    <Button disabled={this.state.disableNext} onClick={this.createFamily()}>Continue <Icon type="arrow-right" /></Button>
                                }
                                {this.state.current === 3 &&
                                    <Button onClick={this.updateFamily} style={{ minWidth: 150 }}>Done</Button>
                                }
                                {(this.state.current > 0 || this.state.joinSuggesion === true) &&
                                    <Button type="link" onClick={this.previousPage()}><Icon type="arrow-left" /> Back</Button>
                                }
                                {this.state.joinSuggesion === true &&
                                    <Button onClick={() => { this.createNewFamily() }}>Create New Family <Icon type="arrow-right" /></Button>
                                }
                                {/* For recommendation card */}
                                {/* <Button>Create new family <Icon type="arrow-right" /></Button> */}

                                {/* For final Submit Button */}
                                {/* <Button style={{minWidth:150}}>Done</Button> */}
                            </div>
                        </Modal>
                        {/* Create Family Modal - END */}
                    </div>
                    {this.state.linkFamilyList && this.state.linkFamilyList.data && this.state.linkFamilyList.data.length > 0 &&
                        <Drawer
                            title="Family List"
                            placement="right"
                            closable={true}
                            onClose={this.onClose}
                            visible={this.state.drawervisible}
                        >
                            <List
                                size="small"
                                footer={<div><button onClick={this.linkToSelectedGroups}>link to family</button></div>}
                                bordered
                                dataSource={this.state.linkFamilyList.data}
                                renderItem={(item, index) => (
                                    <List.Item>
                                        <Checkbox key={index} onChange={this.checkedValue(item.id)}>{item.group_name}</Checkbox>
                                    </List.Item>
                                )}
                            />
                        </Drawer>
                    }
                    <div className="f-sider-body">
                        {this.state.familyList && this.state.familyList.length == 0 && this.state.loader && <div style={{ float: 'left', width: '100%', textAlign: 'center' }}><Spin indicator={antIcon} /></div>}
                        <ul>
                            {this.state.familyList && this.state.familyList.length > 0 &&
                                this.state.familyList.map((item, index) => {
                                    return <li key={index} className="div-hover" onClick={this.navigateToFamily(item.id)} >
                                        <Avatar src={item.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + item.logo : require('../../assets/images/demo/demo_u5.png')} />
                                        <span>
                                            <h5>{item.group_name}</h5>
                                            <h6>{item.new_events_count} Events</h6>
                                        </span>
                                    </li>
                                })
                            }
                            {this.state.familyList && this.state.familyList.length == 0 && !this.state.loader &&
                                <div>No Families Found For the User</div>
                            }
                        </ul>
                    </div>

                    {this.state.familyList && this.state.familyList.length > 0 &&
                        <div className="f-sider-foot">
                            <Button type="link" onClick={() => { this.showFamilyList(3) }}>See more</Button>
                        </div>
                    }
                </div>

                {/* Sidebar Block */}
                <div className="f-sider-block hide ">
                    <div className="f-sider-head f-wadd">
                        <img src={require('../../assets/images/icon/land5.svg')} />
                        <h4>Create New Event</h4>
                        <Button onClick={this.showEventModal}>+</Button>
                        <Modal
                            className="f-modal f-modal-wizard"
                            title="Create New Event"
                            destroyOnClose={true}
                            visible={(this.props.eventModal)}
                            onCancel={this.handleCancel}
                            footer={[
                                //   <button>cancel</button>
                            ]}
                        >
                            {this.state.nextpage === false &&
                                <div className="f-modal-wiz-body">
                                    <div className="f-form-control">
                                        <label>Event Name</label>
                                        <Input placeholder="Enter Event Name" value={this.state.event_name} onChange={this.handleChange('event_name')}></Input>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Event Type</label>
                                        <Select placeholder="Select" style={{ width: '100%' }} onChange={this.handleSelect('event_type')}>
                                            <Option key='Sign Up'>Sign up</Option>
                                            <Option key='Regular'>Regular</Option>
                                        </Select>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Category</label>
                                        <Select placeholder="Select" style={{ width: '100%' }} onChange={this.handleSelect('category')}>
                                            {this.state.categoryList.map((item, index) => {
                                                return <Option value={item} key={index} >{item}</Option>
                                            })}
                                        </Select>
                                    </div>
                                    <div className="f-form-control f-wiz-switch">
                                        <ul>
                                            <li>
                                                <h6>Make Your Event Private</h6>
                                                <Switch defaultChecked onChange={this.handleSelect('public_event')}></Switch>
                                            </li>
                                            <li>
                                                <h6>Allow guest to invite others</h6>
                                                <Switch onChange={this.handleSelect('is_sharable')}></Switch>
                                            </li>
                                            <li>
                                                <h6>Allow others to join the event</h6>
                                                <Switch onChange={this.handleSelect('allow_others')}></Switch>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            }
                            {this.state.nextpage === true &&
                                <div className="f-modal-wiz-body">
                                    <div className="f-form-control">
                                        <label>Hosted by</label>
                                        <Input defaultValue={this.state.full_name} placeholder="Enter Host Name" onChange={this.handleChange('event_host')}></Input>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Venue</label>
                                        {/* <Input placeholder="Enter Venue" id="auto-complete" value={this.state.location} onChange={this.handleChange('location')}></Input> */}
                                        <ReactGoogleMapLoader
                                            params={{
                                                key: MY_API_KEY,
                                                libraries: "places,geocode",
                                            }}
                                            render={googleMaps =>
                                                googleMaps && (
                                                    <ReactGooglePlacesSuggest
                                                        googleMaps={googleMaps}
                                                        autocompletionRequest={{
                                                            input: search,
                                                        }}
                                                        onSelectSuggest={this.handleSelectSuggest}
                                                        textNoResults="My custom no results text" // null or "" if you want to disable the no results item
                                                        customRender={prediction => (
                                                            <div className="customWrapper">
                                                                {prediction
                                                                    ? prediction.description
                                                                    : "My custom no results text"}
                                                            </div>
                                                        )}
                                                    >
                                                        <Input className="art-mat-style art-suffix art-sfx-location" placeholder="Location"
                                                            type="text"
                                                            value={value}
                                                            onChange={this.handleInputChange}
                                                        />
                                                    </ReactGooglePlacesSuggest>
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="f-form-control">
                                        <label>Start Date & Time</label>
                                        <div className="f-split" style={{ marginTop: '0' }}>
                                            <div><DatePicker format={'MMM/DD/YYYY'} disabledDate={this.disabledDate} onChange={this.handleTimeChange('start_date')} /></div>
                                            <div><TimePicker format={'hh:mm a'} onChange={this.handleTimeChange('start_time')} /></div>
                                        </div>
                                    </div>
                                    <div className="f-form-control">
                                        <label>End Date & Time</label>
                                        <div className="f-split" style={{ marginTop: '0' }}>
                                            <div><DatePicker format={'MMM/DD/YYYY'} disabledDate={this.disabledEndDate} onChange={this.handleTimeChange('end_date')} /></div>
                                            <div><TimePicker format={'hh:mm a'} onChange={this.handleTimeChange('end_time')} /></div>
                                        </div>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Description</label>
                                        <Input placeholder="Enter description" value={this.state.description} onChange={this.handleChange('description')}></Input>
                                    </div>
                                    <div className="f-form-control">
                                        <label>RSVP Required</label>
                                        <Radio.Group defaultValue='yes' onChange={this.handleChange('rsvp')} style={{ display: 'flex', float: 'left' }}>
                                            <Radio value='yes'>Yes</Radio>
                                            <Radio value='no'>No</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control">
                                        <Upload
                                            accept=".jpg,.jpeg"
                                            name="avatar"
                                            listType="picture-card"
                                            className="avatar-uploader"
                                            showUploadList={false}
                                            action={`${process.env.REACT_APP_API_URL}/api/v1/familheey/dummy`}
                                            // beforeUpload={beforeUpload}
                                            onChange={this.uploadPhoto('event_image')}
                                        >
                                            {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                                        </Upload>
                                    </div>
                                    <div className="f-form-control">
                                        <label>Event Page URL</label>
                                        <div style={{ display: 'flex', width: '100%', alignItems: 'center' }}>
                                            <span style={{ fontFamily: 'monospace', paddingRight: '5px' }} className="f-eve-pgurl">{`${process.env.REACT_APP_BASE_URL}events/`}</span>
                                            <Input placeholder="Enter URL" onChange={this.handleChange('event_page')} />
                                        </div>
                                    </div>
                                </div>
                            }

                            <br></br>
                            <div className="f-modal-wiz-footer">
                                {this.state.nextpage === false &&
                                    <Button onClick={() => this.eventContinue()}>Continue <Icon type="arrow-right" /></Button>
                                }
                                {this.state.nextpage === true &&
                                    <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                        <Button loading={this.state._loading} onClick={this.checkEventLink}> Save <Icon type="arrow-right" /></Button>
                                        <Button type="link" onClick={() => { this.setState({ nextpage: false }) }}><Icon type="arrow-left" /> Back</Button>
                                    </div>
                                }
                            </div>
                        </Modal>

                    </div>
                </div>

                {/* Sidebar Block */}
                {/* <div className="f-sider-block">
                    <div className="f-sider-head">
                        <h4>Upcomming Events</h4>
                        <Button className="f-alt" />
                    </div>
                    <div className="f-sider-body">
                        {this.props.upComingEventList && this.props.upComingEventList.length > 0 &&
                            <ul>
                                {
                                    this.props.upComingEventList.map((item, index) => {
                                        return <li key={index}>
                                            <Avatar src={item.event_image ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'event_image/' + item.event_image : require('../../assets/images/demo/demo_u5.png')} onClick={this.navgateToEvent(item.event_id)} />
                                            <span>
                                                <h5>{item.event_name}</h5>
                                                <h6>{moment(item.from_date * 1000).format('DD MMM YYYY, h:mm:ss a')}</h6>
                                            </span>
                                        </li>
                                    })
                                } */}
                {/* <li> */}
                {/* Use If No Event Image */}
                {/* <Avatar icon="calendar" /> */}
                {/* <Avatar src={require('../../assets/images/demo/demo_u5.png')} />
                                <span>
                                    <h5>{this.props.upComingEventList[5].event_name}</h5>
                                    <h6>{moment(this.props.upComingEventList[5].from_date * 1000).format('DD MMM YYYY, h:mm:ss a')}</h6>
                                </span>
                            </li>
                            <li>
                                <Avatar src={require('../../assets/images/demo/demo_u7.png')} />
                                <span>
                                    <h5>{this.props.upComingEventList[6].event_name}</h5>
                                    <h6>{moment(this.props.upComingEventList[6].from_date * 1000).format('DD MMM YYYY, h:mm:ss a')}</h6>
                                </span>
                            </li>
                             */}
                {/* </ul>
                        }
                    </div>
                    {this.props.upComingEventList && this.props.upComingEventList.length > 0 &&
                        <div className="f-sider-foot">
                            <Button type="link">See more</Button>
                        </div>
                    }

                </div> */}
                {/* <EventListSide className="hide" history={this.props.history}></EventListSide> */}
            </aside>
        )
    }
}
const mapStateToProps=(state)=>{
    let {userId} = state.user_details;
    return {
        userID:userId
    }
}
export default connect(mapStateToProps,null)(EventSidebar);