import React, { useState, useEffect } from 'react';
import { Modal } from 'antd';
import {
    FacebookShareButton,
    InstapaperShareButton,
    LinkedinShareButton,
    PinterestShareButton,
    RedditShareButton,
    TelegramShareButton,
    TwitterShareButton,
    WhatsappShareButton,
    FacebookIcon,
    InstapaperIcon,
    LinkedinIcon,
    PinterestIcon,
    RedditIcon,
    TelegramIcon,
    TwitterIcon,
    WhatsappIcon,
} from 'react-share'
const SocialMediaShare = ({ modalVisible, showSocialModal,post_id }) => {
    const [state, setState] = useState({
        visible: false,
        _url:""
    })
    let {_url } = state;
     useEffect(()=>{
        let urlis = `${process.env.REACT_APP_BASE_URL}page/posts/${post_id}`
        if(typeof post_id == "object"){
            urlis = (post_id && post_id.url) ?  post_id.url : ''
        }
        setState(prev=>({ ...prev,_url:urlis }))
     },[post_id])
    return (
        <React.Fragment>
            <Modal
                title="Share to Social Media"
                destroyOnClose
                onCancel={showSocialModal}
                visible={modalVisible}
                footer={null}
            >
                <div style={{display:'flex',justifyContent:'space-between'}}>
                <FacebookShareButton  url={_url}>
                    <FacebookIcon size={32} />
                </FacebookShareButton>
                <InstapaperShareButton  url={_url}>
                    <InstapaperIcon size={32} />
                </InstapaperShareButton>
                <LinkedinShareButton  url={_url}>
                    <LinkedinIcon size={32} />
                </LinkedinShareButton>
                <PinterestShareButton  url={_url}>
                    <PinterestIcon size={32} />
                </PinterestShareButton>
                <RedditShareButton  url={_url}>
                    <RedditIcon size={32} />
                </RedditShareButton>
                <TelegramShareButton  url={_url}>
                    <TelegramIcon size={32} />
                </TelegramShareButton>
                <TwitterShareButton  url={_url}>
                    <TwitterIcon size={32} />
                </TwitterShareButton>
                <WhatsappShareButton  url={_url}>
                    <WhatsappIcon size={32} />
                </WhatsappShareButton>
                </div>
            </Modal>
        </React.Fragment>
    );
}
export default SocialMediaShare