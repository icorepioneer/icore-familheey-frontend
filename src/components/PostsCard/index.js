import React from 'react';
import './style.scss'
import { Card, Button, Avatar, Input, Checkbox, Drawer, Tabs, Modal, Dropdown, Menu, Carousel, message, Icon, notification, Tag } from 'antd';
import { getFamilyListForPost, mute_conversation, deletePost, remove_post, spam_report } from '../../modules/family/familyList/api-familyList';
import { user_group_members, post_share, list_post_views, getSharedUserList, getCommonSharedUserList, makeItUnread, stickaPost } from '../../modules/Post/post_service';
import ReactPlayer from 'react-player';
import { updateComment, getCommentsByPost, updateLastReadMessage, deleteConversation, addViewCount } from '../../modules/Post/post_service';
import openSocket from 'socket.io-client';
import RightSidebar from '../RightSidebar';
import Lightbox from 'react-image-lightbox';
import { encryption } from '../../shared/encryptDecrypt'
import Linkify from 'linkifyjs/react';
import SocialMediaShare from './SocialMedia'
import TrackVisibility from 'react-on-screen';
import * as linkify from 'linkifyjs';
import hashtag from 'linkifyjs/plugins/hashtag';
import { connect } from 'react-redux'
import ChatSection from '../../components/ChatSection'
hashtag(linkify);

const { Meta } = Card;
var moment = require('moment');
const { TabPane } = Tabs;
const { TextArea } = Input;
const { confirm } = Modal;
const limit = 20;

const imageChecker = (data) => {
    let flag;
    let url_flag = data.includes('https://') || data.includes('http://')
    switch (url_flag) {
        case true:
            flag = true
            break;
        case false:
            flag = false
            break;
    }
    return flag
}
const URLDisplay = (data) => {
    let { url_metadata } = data
    let { urlMetadataResult } = url_metadata
    let { description, image, title, url } = urlMetadataResult
    let url_fetched_type = imageChecker(image)
    let url_image = url_fetched_type ? image : `${url}${image}`
    return (
        <Card className="post-card-url">
            <Meta
                avatar={
                    <Avatar
                        // onError={(e)=>{
                        //     console.log(e.target.src)
                        // }}
                        style={{ width: 64, height: 64, borderRadius: 5 }}
                        src={url_image}
                    />
                }
                title={title}
                description={description}
            />
        </Card>
    )
}
class PostsCard extends React.Component {
    state;
    constructor(props) {
        super(props);
        let _this = this;
        let { userID } = props
        this.state = {
            share_drawer: false,
            title: '',
            data: [],
            loader: false,
            openDrawer: false,
            familyList: [],
            userList: [],
            S3_BASE_URL: process.env.REACT_APP_S3_BASE_URL,
            user_id: userID,
            selectedGrps: [],
            selectedUsrs: [],
            selectedGrpsID: [],
            post_id: 0,
            files: '',
            comment_data: '',
            hide: false,
            base_64_preview: '',
            reportFlag: false,
            reportId: 0,
            description: "",
            modalVisible: false,
            _post_id: '',
            currentImageURlSelected: '',
            displayImage: false,
            images: undefined,
            photoIndex: 0,
            disableComment: false,
            linkifyOptions:
            {
                attributes: {
                    onClick: (event) => {
                        setTimeout(() => {
                            let urlSplitArray = window.location.href.split('#');
                            let clickedHashTag = urlSplitArray[1];
                            if (clickedHashTag) _this.props && _this.props.searchEvent && _this.props.searchEvent({ target: { value: '#' + clickedHashTag } })
                        }, 200);
                    }
                }
            }
        }
        // this.menuAction = this.menuAction.bind(this);
        // this.deletePostFn = this.deletePostFn.bind(this)
        if (this.props.page_type == 'details') {
            this.comment(this.props.data.post_id);
        }
    }

    share_post_modal = async (post_id, type) => {
        await getFamilyListForPost({ user_id: this.state.user_id, offset: 0, limit: 10 })
            .then(response => {
                this.setState({ familyList: response.data.data });
            });
        user_group_members({ user_id: this.state.user_id })
            .then(response => {
                this.setState({ userList: response.data.data })
            });
        this.setState({ post_id: post_id });
        let share_title = '';
        if (type == 'post') {
            share_title = 'Share Post';
        } else {
            share_title = 'Share Announcement';
        }
        this.setState({ share_drawer: true, share_title: share_title });
    }

    onClose = () => {
        this.setState({ share_drawer: false, showDeleteModal: false, openChatModal: false, reportFlag: false });
    }

    share = (d) => {
        post_share({ shared_user_id: this.state.user_id, post_id: this.state.post_id.toString(), to_group_id: this.state.selectedGrps, to_user_id: this.state.selectedUsrs })
            .then(response => {
                message.success(this.props.data.type + ' shared successfully ');
                this.onClose();
            });
    }
    onTabChange = () => {
        this.setState({ selectedUsrs: [], selectedGrps: [], selectedGrpsID: [] });
    }
    selectFamily = (e, family) => {
        if (e.target.checked) {
            this.state.selectedGrps.push({ id: family.id, post_create: family.post_create });
            this.state.selectedGrpsID.push(family.id);
        } else {
            let index = this.state.selectedGrpsID.indexOf(family.id);
            this.state.selectedGrps.splice(index, 1);
            this.state.selectedGrpsID.splice(index, 1);
        }
        this.setState({ selectedGrps: this.state.selectedGrps });
    }
    selectUsers = (e, family) => {
        // console.log(e.target.checked);
        console.log(family);
        if (e.target.checked) {
            // this.state.selectedGrps.push({ id: family.id, post_create: family.post_create });
            this.state.selectedUsrs.push(family.user_id);
        } else {
            let index = this.state.selectedUsrs.indexOf(family.user_id);
            // this.state.selectedGrps.splice(index, 1);
            this.state.selectedUsrs.splice(index, 1);
        }
        this.setState({ selectedUsrs: this.state.selectedUsrs });

        console.log(this.state.selectedUsrs);
    }
    menuAction = (key, d) => {
        var { callbackParent } = this.props;
        if (!d) {
            return;
        }
        console.log(d);
        let _this = this;
        switch (key) {
            case 1:
                mute_conversation({ user_id: this.state.user_id, post_id: d.post_id.toString() })
                    .then(result => {
                        message.success((d.muted ? 'UNMUTED' : 'MUTED') + ' SUCESSFULLY')
                        d.muted = !d.muted;
                        this.setState({ postList: this.state.postList });
                    })
                break;
            case 2:
                let path_ = window.location.pathname;
                let _this1 = this;
                d.type == 'announcement' ?
                    (_this1.props.history.push('/announcement/create/' + d.post_id, { path_: path_, type_: 'announcement' })) :
                    (_this1.props.history.push('/post/create/' + d.post_id, { path_: path_, type_: 'post' }))
                break;
            case 3:
                confirm({
                    title: 'Are you sure you want to delete this ?',
                    content: '',
                    okText: 'Yes',
                    okType: 'danger',
                    cancelText: 'No',
                    onOk() {
                        _this.deletePostFn();
                        // console.log('OK');
                    },
                    onCancel() {
                        console.log('Cancel');
                        // this.setState({ showDeleteModal: true });

                    },
                });
                break;
            case 4:
                confirm({
                    title: 'Are you sure you want to hide this ?',
                    content: '',
                    okText: 'Yes',
                    okType: 'danger',
                    cancelText: 'No',
                    onOk() {
                        _this.removeSelectedPost(d)
                    },
                    onCancel() {
                        console.log('Cancel');

                    },
                });
                break;
            case 5:
                _this.setState({ reportFlag: true, reportId: d.post_id })
                break;
            case 6:
                let txt = d.read_status ? `unread`:`read`;
                confirm({
                    title: `Are you sure you want mark it as ${txt}?`,
                    content: '',
                    okText: 'Yes',
                    okType: 'danger',
                    cancelText: 'No',
                    onOk() {
                        _this.setState({ loader: true });
                        _this.markAsreadUnread(d)
                    },
                    onCancel() {
                        console.log('Cancel');
                    },
                });
                break;
            case 7:
                let sticky_params = {
                    group_id: d.to_group_id.toString(),
                    post_id: d.id.toString(),
                    type: "stick"
                }

                confirm({
                    title: `Are you sure to stick this post?`,
                    okText: 'stick it',
                    cancelText: 'No',
                    onOk() {
                        _this.setState({ loader: true });
                        stickaPost(sticky_params).then((res) => {
                            _this.openNotification();
                            callbackParent({ type: 'sticky' });
                            _this.setState({ loader: false });
                        }).catch((err) => {
                            _this.setState({ loader: false });
                            let { data } = err && err.response;
                            notification.open({
                                message: data.message,
                                icon: <Icon type="stop" style={{ color: "#4bae50" }} />
                            });
                        })
                    },
                    onCancel() { },
                });
                break;
            default:
                break;
        }
    }
    removeSelectedPost = (d) => {
        let {page_type:pt} = this.props
        remove_post({ user_id: this.state.user_id, post_id: d.post_id.toString() })
            .then(result => {
                message.success('HIDDEN SUCCESSFULLY');
                d.hide = true;
                this.setState({ hide: true, postList: this.state.postList });
                pt==='details'&&this.props.history.push('/post')
                return d
            })
    }
    openNotification = () => {
        notification.open({
            message: "Done",
            description: "",
            icon: <Icon type="smile" style={{ color: "#4bae50" }} />
        });
    };
    getConversationMenuTemplate = (post_id, comment_id, index) => {
        return (
            <Menu >
                <Menu.Item key="1" onClick={(e) => this.deleteConversation(post_id, comment_id, index)}>
                    Delete
                </Menu.Item>
            </Menu>
        )
    }
    getMenuTemplate = (d) => {
        return (
            <Menu >
                <Menu.Item key="1" onClick={(e) => this.menuAction(1, d)}>
                    {d.muted ? 'Unmute' : 'Mute'}
                </Menu.Item>
                {
                    (d.created_by == this.state.user_id) &&
                    <Menu.Item disabled={d.publish_type == 'request'} key="2" onClick={(e) => this.menuAction(2, d)}>
                        Edit
                </Menu.Item>

                }
                {
                    (d.created_by == this.state.user_id) &&

                    <Menu.Item key="3" onClick={(e) => this.menuAction(3, d)} >
                        Delete
                </Menu.Item>
                }
                {
                    (d.created_by != this.state.user_id) &&
                    <Menu.Item key="4" onClick={(e) => this.menuAction(4, d)}>
                        Hide
                </Menu.Item>
                }

                {
                    (d.created_by != this.state.user_id) &&
                    <Menu.Item key="5" onClick={(e) => this.menuAction(5, d)}>
                        Report
                </Menu.Item>
                }
                {
                    (d.type == 'announcement' && d.created_by != this.state.user_id) &&
                    <Menu.Item key="6" onClick={(e) => this.menuAction(6, d)}>
                        Mark as { d.read_status ?  'unread':'read' }
                </Menu.Item>
                }
                {
                    (d.isGroupAdmin) &&
                    <Menu.Item key="7" onClick={(e) => this.menuAction(7, d)}>
                        Stick the post
                    </Menu.Item>
                }
            </Menu>
        );
    }
    loadComments = (post_id) => {
        let offset = 0;
        if (this.props.data && this.props.data.comments)
            offset = this.props.data.comments.length;
        else
            this.props.data.comments = [];

        getCommentsByPost({ post_id: post_id.toString(), user_id: this.state.user_id, limit: limit, offset: offset })
        .then(res => {
            if (res && res.data && res.data.data && res.data.data.length > 0) {
                let comments = res.data.data;// .reverse();
                if (this.props.data.comments && this.props.data.comments.length == 0) {
                    this.props.data.comments = comments;
                    if (this.props.page_type != 'details') {
                        this.setState({ commentFlag: true, chatTitle: this.props.data.group_name, openChatModal: true });
                    } else {
                        this.setState({ commentFlag: true, chatTitle: this.props.data.group_name, openChatModal: false });
                    }
                    this.gotoBottom('f-conversations');
                } else {
                    this.props.data.comments = this.props.data.comments.concat(comments);
                }
                let totalConversation = this.props.data.comments.length;
                if (totalConversation > 0 && this.props.data.conversation_count_new != '0') {
                    this.props.data.conversation_count_new = '0';
                    let conversationId = this.props.data.comments[totalConversation - 1].comment_id.toString();
                    updateLastReadMessage({ post_id: post_id.toString(), user_id: this.state.user_id, last_read_message_id: conversationId })
                    .then(res1 => {
                    });
                }
            }
        });
    }
    comment = (post_id) => {
        //let { conversation_count } = this.props.data;
        this.socket = openSocket(process.env.REACT_APP_SOCKET_URL);
        let e_post_id = encryption(post_id);
        // console.log(e_post_id);
        if (this.props.page_type != 'details' && this.props.data.type != 'announcement') {
            this.props.history.push('/post_details/' + e_post_id, { backPath: window.location.pathname, activeTab: this.props.current_tab })
        }
        if (this.props.data.type == 'announcement') {
            this.redirectStory();
            return;
        }
        let channel_name = 'post_channel_' + post_id.toString();
        this.socket.on(channel_name, timestamp => {
            if (timestamp && timestamp[0]) {
                if (timestamp[0].type == 'delete_comment') {
                    let delete_id_arr = timestamp[0].delete_id;
                    let total_elmnt = delete_id_arr.length;
                    let iteration = 0;
                    for (let i = this.props.data.comments.length - 1; i >= 0; i--) {
                        if (delete_id_arr.indexOf(this.props.data.comments[i].comment_id) >= 0) {
                            this.props.data.comments.splice(i, 1);
                            iteration++;
                            if (iteration >= total_elmnt) {
                                break;
                            }
                        }
                    }
                    this.setState({ commentFlag: true });
                } else {
                    this.gotoBottom('f-conversations');
                    this.props.data.comments.push(timestamp[0]);
                    this.setState({ commentFlag: true });
                }
            }
        });
        this.loadComments(post_id);
    }
    componentWillUnmount() {
        if (this.socket) {
            let channel_name = 'post_channel_' + this.props.data.post_id.toString();
            this.socket.off(channel_name);
        }
    }
    deleteConversation = (post_id, comment_id, index) => {
        let _this = this;
        confirm({
            title: 'Are you sure you want to delete this ?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                let comments_id = [comment_id];
                let post_params = {
                    "comment_id": comments_id,
                    "post_id": post_id.toString()
                };
                _this.props.data.comments.splice(index, 1);
                _this.setState({ commentFlag: true });

                deleteConversation(post_params).then(result => {
                });
            },
            onCancel() {

            },
        });
    }
    onChangeField = (e) => {
        console.log(e.target.value);
        this.setState({ comment: e.target.value })

    }
    onSelectFile = e => {
        this.setState({ imgFile: e.target.files[0] })
        // if (e.target.files && e.target.files.length > 0) {
        //     const reader = new FileReader();
        //     reader.addEventListener('load', () =>
        //         this.setState({ src: reader.result })
        //     );
        //     reader.readAsDataURL(e.target.files[0]);
        // }

    };


    onCloseCallback = () => {
        this.setState({ openDrawer: false, loader: false });
    }
    viewList = (e, name) => {
        if (name == 'Viewed By') {
            let { views_count, post_id } = this.props.data;
            if (parseInt(views_count) == 0) return;
            list_post_views({ post_id: post_id.toString(), user_id: this.state.user_id })
                .then(result => {
                    this.setState({ title: name, data: result.data.data, openDrawer: true, loader: true });
                });
        }
        if (name == 'Shared By') {
            let { shared_user_count, orgin_id, post_id } = this.props.data;
            let _id = (orgin_id) ? orgin_id : post_id;

            if (parseInt(shared_user_count) == 0) return;

            getCommonSharedUserList({ origin_post_id: _id.toString() })
                .then(result => {
                    this.setState({ title: name, data: result.data, openDrawer: true, loader: true });
                    // console.log(result.data);
                });
            // getSharedUserListByUserid({ post_id: this.props.data.post_id.toString(), group_id: this.props.data.to_group_id.toString() })
            // .then(result => {
            //     this.setState({ title: name, data: result.data.data, openDrawer: true, loader: true });
            //     console.log(result);
            // });
        }
        if (name == 'shared list') {
            getSharedUserList({ post_id: this.props.data.orgin_id.toString(), group_id: this.props.data.to_group_id.toString() })
                .then(result => {
                    this.setState({ title: name, data: result.data, openDrawer: true, loader: true });
                });
        }
    }
    save = (v, index) => {

        if (this.state.comment_data.trim() == '' && this.state.base_64_preview == '') {
            return;
        }
        if (this.state.disableComment == true) {
            return;
        }
        this.state.disableComment = true;
        this.setState({ base_64_preview: '', disableComment: true });

        var formData = new FormData();
        formData.append('post_id', this.props.data.post_id.toString());
        formData.append('group_id', this.props.data.to_group_id);
        formData.append('comment', this.state.comment_data);
        formData.append('commented_by', this.state.user_id);
        formData.append('file_name', this.state.files);

        updateComment(formData)
            .then(res => {
                this.gotoBottom('f-conversations');
                this.setState({ comment_data: '', files: '', disableComment: false });
            })
            .catch(err => {
                this.setState({ disableComment: false });

            });
    }
    onMessageChange = (e) => {
        this.setState({ comment_data: e.target.value });
    }
    uploadPhoto = (e) => {
        this.setState({ files: e.target.files[0] })
        this.getBase64(e.target.files[0], imageUrl => {
            this.setState({ base_64_preview: imageUrl });
            this.save(this.props.data);

        });
    }
    getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }

    redirectStory = () => {
        let { post_id, type:new_type } = this.props.data;
        if( new_type == 'announcement'){
            addViewCount({ user_id:this.state.user_id, post_id:post_id.toString() });
        }
        this.props.history.push('/individualStatus', { fromStory: { post_id: post_id, prev_url: `${window.location.pathname}` } });
    }

    spamReport = (post_id) => {
        this.setState({ reportFlag: false })
        if (this.state.description != '') {
            spam_report({
                user_id: this.state.user_id,
                type_id: post_id.toString(),
                spam_page_type: 'post',
                description: this.state.description
            }).then((res) => {
                message.success('Reported successfully.We will review the post and do the needful,thank you');
            })
        }
    }

    handleChange = name => e => {
        this.setState({ [name]: e.target.value })
    }
    keyPressed = (e) => {
        if (e.key == 'Enter') {
            this.save(this.props.data);
        }
    }
    deletePostFn = () => {
        let { user_id } = this.state
        deletePost({ id: this.props.data.post_id.toString(), user_id: `${user_id}` })
            .then(result => {
                message.success('DELETED SUCESSFULLY');
                this.props.callParentAfterDelete && this.props.callParentAfterDelete()
                this.setState({ hide: true });
            })
    }
    goDetails = (id) => {
        // this.props.history.push('post/' + encryption(id));
    }
    goUserProfile = (type, user_id = null) => {
        let userId
        userId = encryption(this.props.data.created_by);
        if (type == 2 && this.props.data.parent_post_created_user_id) {

            userId = encryption(this.props.data.parent_post_created_user_id);
        }
        else if (type === 3) {
            userId = encryption(user_id)
        }
        this.props.history.push(`/user-details/${userId}`, { prev: window.location.pathname })
    }
    goFamilyDetails = (id) => {
        console.log(id);
        if (id) {
            var familyId = encryption(id)
            this.props.history.push(`/familyview/${familyId}`)
        }

    }
    openLightBoxFn = (url) => {
        this.setState({ lightBoxImg: url, openLightBox: true });
    }
    gotoBottom(id) {
        var element = document.getElementById(id);
        if (!element) {
            return;
        }
        element.scrollTop = element.scrollHeight - element.clientHeight;
    }

    handleMenuClick = (e, post_id, type) => {
        console.log('click', e, post_id);
        switch (e.key) {
            case "1":
                this.share_post_modal(post_id, type)
                break;
            case "2":
                this.setState({
                    modalVisible: true, _post_id: post_id
                })
                break;
        }
    }

    menu = (post_id, type) => {
        return <Menu onClick={e => this.handleMenuClick(e, post_id, type)}>
            <Menu.Item key="1">
                Share in Familheey
          </Menu.Item>
            <Menu.Item key="2">
                Share in Social Media
            </Menu.Item>
        </Menu>
    }
    showSocialModal = () => {
        this.setState({
            modalVisible: !this.state.modalVisible
        })
    }
    checkDisplayName = (data) => {
        let { from } = this.props;
        let { post_type, other_group_names, other_user_names, group_name } = data
        let return_string;
        switch (post_type) {
            case 'public':
                return_string = "Public"
                break;
            case 'only_groups':
                return_string = other_group_names ? other_group_names : group_name
                break;
            case 'only_users':
                return_string = other_user_names
                break;
            case 'all_family':
                return_string = from == 'profile' ? 'All family' : group_name
                break;
            case 'private':
                return_string = 'Private'
                break;
        }
        return return_string
    }
    displayThanksComp = (data) => {
        let { publish_mention_users, publish_type, publish_id } = data;
        let folderId;
        if (publish_type === 'albums') folderId = encryption(publish_id)
        return (
            <div className="f-contribution-ty">
                <div className="f-contr-header">
                    {
                        publish_type != 'folder' && publish_mention_users && publish_mention_users.map((item, i) =>
                            <Tag key={i} onClick={() => this.goUserProfile(3, item.user_id)}>@{item.user_name}</Tag>
                        )
                    }
                </div>
                <div className="f-contr-footr">
                    <h5></h5>
                    {publish_type === 'request' && <Button onClick={() => { this.props.history.push({ pathname: '/request-details', state: { post_request_id: publish_id } }) }}>Open Request</Button>}
                    {publish_type === 'albums' && <Button onClick={() => { this.props.history.push({ pathname: `/album-details/${folderId}` }) }}>Open Album</Button>}
                </div>
            </div>
        )
    }
    determinePath = (data) => {
        let { publish_type } = data
        switch (publish_type) {
            case 'albums':
                return 'Documents/'
            case 'documents':
                return 'Documents/'
            default:
                return 'post/'
        }
    }
    checkPostType = (data) => {
        let { publish_type } = data
        switch (publish_type) {
            case 'albums':
                return false
            case 'documents':
                return false
            default:
                return true
        }
    }
    downloadButtonClicked = () => {
        // console.log(this.state.currentImageURlSelected)
        window.open(`${this.state.currentImageURlSelected}`, 'Download')
    }
    markAsreadUnread = (d)=>{
        let params = {
            post_id: `${d.post_id}`,
            user_id: this.state.user_id,
            read_status: d.read_status ? 'false':'true'
        }
        makeItUnread(params).then((res) => {
            this.openNotification();
            this.props.callParentAfterDelete && this.props.callParentAfterDelete();
            this.setState({ loader: false });
        })
    }
    render() {
        let { displayImage, currentImageURlSelected, images, photoIndex } = this.state;
        return (
            // For loading animation, add "loading={true}" to the card
            <div className="f-clearfix f-listland-card">
                {displayImage && (images && images.length > 1 ? <Lightbox
                    toolbarButtons={[
                        <Icon onClick={() => this.downloadButtonClicked()} type="download" ></Icon>
                    ]}
                    mainSrc={currentImageURlSelected}
                    nextSrc={`${process.env.REACT_APP_S3_BASE_URL + 'Documents/' + this.state.images[(this.state.photoIndex + 1) % this.state.images.length].filename}`}
                    prevSrc={`${process.env.REACT_APP_S3_BASE_URL + 'Documents/' + this.state.images[(this.state.photoIndex + this.state.images.length - 1) % this.state.images.length].filename}`}
                    onMovePrevRequest={() =>
                        this.setState({
                            currentImageURlSelected: `${process.env.REACT_APP_S3_BASE_URL + 'Documents/' + this.state.images[(this.state.photoIndex + this.state.images.length - 1) % this.state.images.length].filename}`,
                            photoIndex: (photoIndex + images.length - 1) % images.length,
                        })
                    }
                    onMoveNextRequest={() =>
                        this.setState({
                            currentImageURlSelected: `${process.env.REACT_APP_S3_BASE_URL + 'Documents/' + this.state.images[(this.state.photoIndex + 1) % this.state.images.length].filename}`,
                            photoIndex: (photoIndex + 1) % images.length,
                        })
                    }
                    onCloseRequest={() => { this.setState({ displayImage: false, currentImageURlSelected: '' }) }}
                /> :
                    <Lightbox
                        toolbarButtons={[
                            <Icon onClick={() => this.downloadButtonClicked(currentImageURlSelected)} type="download"></Icon>]}
                        mainSrc={currentImageURlSelected}
                        onCloseRequest={() => { this.setState({ displayImage: false, currentImageURlSelected: '' }) }}
                    />)
                }
                <SocialMediaShare modalVisible={this.state.modalVisible} post_id={this.state._post_id} showSocialModal={this.showSocialModal} />
                {/* CHAt */}
                {
                    this.props.data &&
                    <Drawer
                        title="CHAT"
                        placement="right"
                        closable={true}
                        onClose={(e) => this.onClose()}
                        visible={false}
                    >
                        <ul>
                            {
                                this.props.data.comments && this.props.data.comments.map((v, index) => {
                                    return (<li className={(this.props.data.commented_by == this.state.user_id) ? 'li-item align-right' : 'li-item align-left'} key={index}>{v.comment} <br />

                                        {this.props.data.file_name ? <img src={this.state.S3_BASE_URL + 'file_name/' + v.file_name} /> : ''}
                                        {this.props.data.full_name},{moment(this.props.data.createdAt).format('DD-MM-YYYY HH:mm')}
                                    </li>);
                                })

                            }
                        </ul>
                        <input type="file" onChange={this.onSelectFile} />
                        <TextArea rows={4} placeholder="Description" onChange={(e) => this.onChangeField(e)} />
                        {/* <Button type="primary" onClick={(e) => this.save(v)}>SAVE</Button> */}
                        <div>
                            <button onClick={(e) => this.save(this.props.data)}>SAVE</button>
                        </div>
                    </Drawer>}

                {/* END */}
                <Modal
                    className="f-modal f-modal-wizard f-share-tabbed"
                    title='Confirm'
                    visible={this.state.showDeleteModal}
                    // onOk={this.handleOk}
                    onCancel={this.onClose}
                    footer={[
                        //   <button>cancel</button>
                    ]}
                >
                    <h3>Are you sure to delete</h3>
                    {/* Footer */}
                    <div className="f-modal-wiz-footer">
                        <Button onClick={(e) => this.deletePostFn()} style={{ minWidth: 150 }} >Delete</Button>
                        <Button onClick={(e) => this.onClose()} style={{ minWidth: 150 }} >Cancel</Button>
                    </div>
                </Modal>
                {/* POST SHARE */}
                <Modal
                    className="f-modal f-modal-wizard f-share-tabbed"
                    title={this.state.share_title}
                    visible={this.state.share_drawer}
                    // onOk={this.handleOk}
                    onCancel={this.onClose}
                    footer={[
                        //   <button>cancel</button>
                    ]}
                >
                    <div className="f-modal-wiz-body" >
                        <Tabs defaultActiveKey="1" onChange={this.onTabChange}>
                            <TabPane tab="FAMILY" key="1">
                                <div className="f-member-listing">
                                    <div className="f-content-searchable">
                                        {/* <h4><strong>{this.state.selectedUsers.length}</strong> Users selected</h4> */}
                                    </div>
                                    <div className="f-member-listing-cards f-share-scroll">
                                        {
                                            this.state.familyList.map((d, i) => {
                                                return (<Card key={i} className="f-member-cardx">
                                                    <div className="f-start">
                                                        <div className="f-left">
                                                            <img onError={(e) => { e.target.onerror = null; e.target.src = "images/default_logo.png" }}
                                                                src={d.logo ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + d.logo : ('../../images/default_logo.png')} />
                                                        </div>
                                                        <div className="f-right">
                                                            <h4>{d.group_name}</h4>
                                                            <h6>By <strong>{d.created_by}</strong></h6>
                                                            <h6>Member since {d.member_since}</h6>
                                                            <h6 className="f-pin">{d.base_region}</h6>
                                                        </div>
                                                    </div>
                                                    <div className="f-stop">
                                                        <h5>
                                                            {/* <strong>5K</strong> Members */}
                                                        </h5>
                                                        <div className="f-right-combo">
                                                            {/* <Checkbox onChange={(e) => this.selectFamily(e, d)}>{d.group_name}</Checkbox> */}

                                                            <Checkbox onChange={(e) => this.selectFamily(e, d)}>
                                                                {this.state.selectedGrpsID.indexOf(d.id) == -1 ? 'Select' : 'Selected'}
                                                            </Checkbox>
                                                        </div>
                                                    </div>
                                                </Card>);
                                            })
                                        }
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane tab="USERS" key="2">
                                <div className="f-member-listing">
                                    <div className="f-content-searchable">
                                        {/* <h4><strong>{this.state.selectedUsers.length}</strong> Users selected</h4> */}
                                    </div>
                                    <div className="f-member-listing-cards f-share-scroll">
                                        {
                                            this.state.userList.map((user, i) => {
                                                return (
                                                    <Card key={i} className="f-member-cardx">
                                                        <div className="f-start">
                                                            <div className="f-left">
                                                                <img
                                                                    onError={(e) => { e.target.onerror = null; e.target.src = "images/default_propic.png" }}
                                                                    src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + user.propic} />
                                                            </div>
                                                            <div className="f-right">
                                                                <h4>{user.full_name}</h4>
                                                                <h4>{user.location}</h4>
                                                                <h6>Member since {user.member_since}</h6>
                                                                <div className="f-right-combo">
                                                                    <Checkbox
                                                                        onChange={(e) => this.selectUsers(e, user)} >
                                                                        {this.state.selectedUsrs.indexOf(user.user_id) == -1 ? 'Select' : 'Selected'}
                                                                    </Checkbox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Card>);
                                            })
                                        }
                                    </div>
                                </div>

                            </TabPane>
                        </Tabs>
                    </div>
                    {/* Footer */}
                    <div className="f-modal-wiz-footer">
                        <Button onClick={(e) => this.share()} style={{ minWidth: 150 }} >Share <Icon type="share-alt" /></Button>
                    </div>
                </Modal>

                <Modal
                    className="f-modal f-modal-wizard"
                    title="REPORT"
                    visible={this.state.reportFlag}
                    // onOk={this.handleOk}
                    onCancel={this.onClose}
                    footer={[
                        <button className={`ant-btn-lg`} onClick={() => this.spamReport(this.state.reportId)}>Send</button>
                    ]}
                >
                    <div>
                        <TextArea required rows={4} placeholder="Feedback" onChange={this.handleChange('description')}></TextArea>
                    </div>
                    <div className="f-modal-wiz-footer">
                        <Button onClick={() => this.spamReport(this.state.reportId)} style={{ minWidth: 150 }} >Send</Button>
                    </div>


                </Modal>

                {/* END  */}

                {/* <Card className="postsCard f-post-card" loading={true}></Card> */}

                {/* Card with Image */}
                {
                    !this.state.hide &&
                    this.props.type == '1' &&
                    <Card className="postsCard f-post-card">
                        <header className="f-head"  >
                            <div className="f-west" >
                                {/* Use If No User Image */}
                                { /* <Avatar icon="user" /> */}
                                <Avatar onClick={() => this.goUserProfile(1)} src={this.props.data.propic && this.props.data.propic != 'null' ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + this.props.data.propic : require('../../assets/images/default_propic.png')} />
                                <span>
                                    {/* Commenting as per mantisid =651 */}
                                    {/* <h5 onClick={() => this.goUserProfile(1)}>{(this.state.user_id != this.props.data.created_by) ? this.props.data.created_user_name : 'You'}</h5> */}
                                    <h5 onClick={() => this.goUserProfile(1)}>{this.props.data.created_user_name}</h5>

                                    <h6>Posted {`${this.props.data.post_type == 'only_users' ? 'to ' : 'in '}`} <strong onClick={() => this.goFamilyDetails(this.props.data.to_group_id)} >{`${this.checkDisplayName(this.props.data)}`}</strong></h6>
                                    <h6>{moment(this.props.data.createdAt).format('MMM DD ,YYYY hh:mm A')}</h6>
                                </span>
                            </div>
                            {this.props.data.publish_type !== 'request'&&(this.props.data.is_shareable) && <div className="f-east">
                                {/* <Button className="f-btn-group" /> */}
                                {this.checkPostType(this.props.data) && <Dropdown overlay={() => this.menu(this.props.data.post_id, this.props.data.type)}>
                                    <Button
                                        className="f-btn-share"
                                    // onClick={(e) => this.share_post_modal(this.props.data.post_id, this.props.data.type)}
                                    />
                                </Dropdown>}
                            </div>}
                        </header>
                        <section className="f-sect">
                            <div className="f-clearfix">
                                {
                                    this.props.data && this.props.data.post_attachment && this.props.data.post_attachment.length != 0 &&
                                    <div className="f-poster">
                                        <TrackVisibility>{({ isVisible }) =>

                                            (<Carousel >
                                                {this.props.data && this.props.data.post_attachment && this.props.data.post_attachment.map((v, i) => {
                                                    return (<div key={i} className="f-csl-in">
                                                        {v.type.indexOf('image') != -1 ? <img onClick={() => {
                                                            this.setState({
                                                                photoIndex: i,
                                                                images: this.props.data.post_attachment,
                                                                displayImage: true,
                                                                currentImageURlSelected: this.props.data.publish_type == 'folder' ? `${process.env.REACT_APP_S3_BASE_URL + 'Documents/' + v.filename}` : `${process.env.REACT_APP_S3_BASE_URL + this.determinePath(this.props.data) + v.filename}`
                                                            })
                                                        }} ref="image" src={this.props.data.publish_type == 'folder' ? process.env.REACT_APP_PRO_PIC_RESIZE + process.env.REACT_APP_S3_BASE_URL + 'Documents/' + v.filename : process.env.REACT_APP_PRO_PIC_RESIZE + process.env.REACT_APP_S3_BASE_URL + this.determinePath(this.props.data) + v.filename} /> : ''}
                                                        {v.type.indexOf('video') != -1 ? 
                                                        <ReactPlayer 
                                                        playing={false} 
                                                        className="f-csl-react-play" 
                                                        url={process.env.REACT_APP_S3_BASE_URL + `${this.props.data.publish_type === 'albums'?'Documents/':'post/'}` + v.filename} 
                                                        controls
                                                         /> : ''}
                                                        {(v.type.indexOf('application/pdf') != -1 || v.type.indexOf('pdf') != -1) ? <a href={process.env.REACT_APP_S3_BASE_URL + this.determinePath(this.props.data) + v.filename} >{v.original_name ? v.original_name : v.filename}</a> : ''}
                                                    </div>)
                                                })
                                                }
                                            </Carousel>)}
                                        </TrackVisibility>
                                        {/* {this.props.data && this.props.data.post_attachment && this.props.data.post_attachment[0] && this.props.data.post_attachment[0].filename &&
                                    <img ref="image" src={process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'post/' + this.props.data.post_attachment[0].filename} />
                                } */}
                                    </div>
                                }
                                {this.props.data && this.props.data.url_metadata && this.props.data.url_metadata.urlMetadataResult && URLDisplay(this.props.data)}
                            </div>
                            <div className="f-clearfix">
                                {this.props.data && this.props.data.snap_description &&
                                    <div className="f-post-description">
                                        <p><Linkify options={this.state.linkifyOptions}>{this.props.data.snap_description}</Linkify></p>
                                        {this.props.data && this.props.data.publish_mention_users && this.displayThanksComp(this.props.data)}
                                        {this.props.data && this.props.data.publish_type == 'albums' && this.displayThanksComp(this.props.data)}
                                    </div>
                                }
                            </div>
                        </section>
                        <footer className="f-foot">
                            <div className="f-west">
                                <ul>
                                    {this.props.data.conversation_enabled && <li><span className={`f-comment`} onClick={(e) => this.comment(this.props.data.post_id)} >{this.props.data.comments && this.props.data.comments.length ? this.props.data.comments.length : this.props.data.conversation_count ? this.props.data.conversation_count : 0} {(this.props.data.conversation_count > 0 && this.props.data.conversation_count_new && this.props.data.conversation_count_new != '0') && <i />}</span></li>}
                                    {this.props.data.created_by == this.state.user_id && <li><span className={`f-share   ${this.props.data.shared_user_count == 0 ? 'crsr-nt' : ''}`} onClick={(e) => this.viewList(this.props.data.shared_user_count, 'Shared By')}>{this.props.data.shared_user_count}</span></li>}
                                    {this.props.data.created_by == this.state.user_id && <li><span className={`f-viewed  ${this.props.data.views_count == 0 ? 'crsr-nt' : ''}`} onClick={(e) => this.viewList(this.props.data.views_count, 'Viewed By')}>{this.props.data.views_count}</span></li>}
                                </ul>
                            </div>
                            <div className="f-east">
                                {this.checkPostType(this.props.data) && <Dropdown overlay={() => this.getMenuTemplate(this.props.data)} placement="bottomLeft">
                                    <Button />
                                </Dropdown>}
                            </div>
                        </footer>
                    </Card>}

                {/* Shared Card */}
                {!this.state.hide && this.props.type == '2' &&
                    <Card className="postsCard f-post-card">
                        <header className="f-head">
                            <div className="f-west">
                                {/* Use If No User Image */}
                                {/* <Avatar icon="user" /> */}
                                <Avatar onClick={(e) => this.viewList(e, 'shared list')} src={require('../../assets/images/default_group.png')} />
                                <span>
                                    <h5>{this.props.data.shared_user_names} {(parseInt(this.props.data.common_share_count, 10) - 1) != 0 && `and ` + (parseInt(this.props.data.common_share_count, 10) - 1) + ' others '} <i>shared a post</i></h5>
                                    <h6>Shared in <strong onClick={() => this.goFamilyDetails(this.props.data.to_group_id)}>{this.props.data.group_name ? this.props.data.group_name : this.props.data.privacy_type == 'private' ? 'PRIVATE' : 'PUBLIC'}</strong></h6>
                                    <h6>{moment(this.props.data.createdAt).format('MMM DD ,YYYY hh:mm A')}</h6>
                                </span>
                            </div>
                            {this.props.data.is_shareable && <div className="f-east">
                                <Dropdown overlay={() => this.menu(this.props.data.post_id, this.props.data.type)}>
                                    <Button
                                        className="f-btn-share"
                                    // onClick={(e) => this.share_post_modal(this.props.data.post_id, this.props.data.type)}
                                    />
                                </Dropdown>
                                {/* <Button className="f-btn-share" onClick={(e) => this.share_post_modal(this.props.data.post_id, this.props.data.type)} /> */}
                            </div>}
                        </header>
                        <section className="f-sect" >
                            <div className="f-shared-post">
                                <header className="f-head">
                                    <div className="f-west">
                                        {/* Use If No User Image */}
                                        {/* <Avatar icon="user" /> */}
                                        <Avatar onClick={() => this.goUserProfile(2)} src={this.props.data.parent_post_created_user_propic && this.props.data.propic != 'null' ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + this.props.data.parent_post_created_user_propic : require('../../assets/images/default_propic.png')} />
                                        <span>
                                            {/* Commenting as per mantisid =651 */}
                                            {/* <h5 onClick={() => this.goUserProfile(2)} >{(this.state.user_id != this.props.data.parent_post_created_user_id) ? this.props.data.parent_post_created_user_name : 'You'}</h5> */}
                                            <h5 onClick={() => this.goUserProfile(2)} >{(this.state.user_id != this.props.data.parent_post_created_user_id) ? this.props.data.parent_post_created_user_name : 'You'}</h5>

                                            <h6>Posted in <strong>{this.props.data.parent_post_grp_name ? this.props.data.parent_post_grp_name : this.props.data.privacy_type == 'private' ? 'PRIVATE' : 'PUBLIC'}</strong></h6>
                                        </span>
                                    </div>
                                </header>
                                <section className="f-sect" >
                                    {
                                        this.props.data && this.props.data.post_attachment && this.props.data.post_attachment.length != 0 &&
                                        <div className="f-poster">
                                            <TrackVisibility>{({ isVisible }) => {
                                                return (<Carousel >
                                                    {this.props.data && this.props.data.post_attachment && this.props.data.post_attachment.map((v, i) => {
                                                        // console.log(v)
                                                        return (<div className="f-csl-in" key={i}>
                                                            {v.type.indexOf('image') != -1 && (
                                                                <div className="f-csl-in">
                                                                    <img onClick={() => {
                                                                        this.setState({
                                                                            photoIndex: i,
                                                                            images: this.props.data.post_attachment,
                                                                            displayImage: true,
                                                                            currentImageURlSelected: `${process.env.REACT_APP_S3_BASE_URL + 'post/' + v.filename}`
                                                                        })
                                                                    }}
                                                                        ref="image"
                                                                        src={process.env.REACT_APP_PRO_PIC_RESIZE + process.env.REACT_APP_S3_BASE_URL + 'post/' + v.filename}
                                                                    />
                                                                </div>)}
                                                            {v.type.indexOf('video') != -1 && <div className="f-csl-in">
                                                                <ReactPlayer
                                                                    playing={false}
                                                                    controls
                                                                    className="f-csl-react-play"
                                                                    url={process.env.REACT_APP_S3_BASE_URL + 'post/' + v.filename}
                                                                /></div>}
                                                            {v.type.indexOf('application') != -1 ? <a href={process.env.REACT_APP_S3_BASE_URL + 'post/' + v.filename} controls >{v.filename}</a> : ''}

                                                        </div>)
                                                    })
                                                    }
                                                </Carousel>)
                                            }}
                                            </TrackVisibility>
                                            {/* {this.props.data && this.props.data.post_attachment && this.props.data.post_attachment[0] && this.props.data.post_attachment[0].filename &&
                                            <img ref="image" src={process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'post/' + this.props.data.post_attachment[0].filename} />
                                        } */}
                                        </div>
                                    }

                                </section>
                            </div>
                            {this.props.data && this.props.data.snap_description &&
                                <div className="f-post-description">
                                    {this.props.data && this.props.data.url_metadata && this.props.data.url_metadata.urlMetadataResult && URLDisplay(this.props.data)}
                                    <p><Linkify options={this.state.linkifyOptions}>{this.props.data.snap_description}</Linkify></p>
                                </div>
                            }
                        </section>
                        <footer className="f-foot">
                            <div className="f-west">
                                <ul>
                                    {this.props.data.conversation_enabled && <li><span className={`f-comment`} onClick={(e) => this.comment(this.props.data.post_id)} >{this.props.data.conversation_count} {this.props.data.conversation_count_new != '0' && <i />}</span></li>}
                                    {this.props.data.created_by == this.state.user_id && <li><span className={`f-share   ${this.props.data.shared_user_count == 0 ? 'crsr-nt' : ''}`} onClick={(e) => this.viewList(e, 'Shared By')}>{this.props.data.shared_user_count}</span></li>}
                                    {this.props.data.created_by == this.state.user_id && <li><span className={`f-viewed  ${this.props.data.views_count == 0 ? 'crsr-nt' : ''}`} onClick={(e) => this.viewList(e, 'Viewed By')}>{this.props.data.views_count}</span></li>}
                                </ul>
                            </div>
                            <div className="f-east">
                                <Dropdown overlay={() => this.getMenuTemplate(this.props.data)} placement="bottomLeft">
                                    <Button />
                                </Dropdown>
                            </div>
                        </footer>
                    </Card>}

                {/* Card without Image */}
                {this.props.type == '3' && !this.state.hide &&
                    <Card className="postsCard f-post-card">
                        <header className="f-head" >
                            <div className="f-west" onClick={this.redirectStory}>
                                {/* Use If No User Image */}
                                {/* <Avatar icon="user" /> */}
                                <Avatar onClick={() => this.goUserProfile()} src={this.props.data.propic && this.props.data.propic != 'null' ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + this.props.data.propic : require('../../assets/images/default_propic.png')} />
                                <span>
                                    {/* Commenting as per mantisid =651 */}
                                    {/* <h5 onClick={() => this.goUserProfile()} >{(this.state.user_id != this.props.data.created_by) ? this.props.data.created_user_name : 'You'}</h5> */}
                                    <h5 onClick={() => this.goUserProfile()} >{this.props.data.created_user_name}</h5>

                                    <h6>Posted in <strong onClick={() => this.goFamilyDetails(this.props.data.to_group_id)}>{this.props.data.group_name ? this.props.data.group_name : this.props.data.privacy_type == 'private' ? 'PRIVATE' : 'PUBLIC'}</strong></h6>
                                    <h6>{moment(this.props.data.createdAt).format('MMM DD ,YYYY hh:mm A')}</h6>
                                </span>
                            </div>
                            {this.props.data.is_shareable && <div className="f-east">
                                <Dropdown overlay={() => this.menu(this.props.data.post_id, this.props.data.type)}>
                                    <Button
                                        className="f-btn-share"
                                    // onClick={(e) => this.share_post_modal(this.props.data.post_id, this.props.data.type)}
                                    />
                                </Dropdown>
                                {/* <Button className="f-btn-share" onClick={(e) => this.share_post_modal(this.props.data.post_id, this.props.data.type)} /> */}
                            </div>}
                        </header>
                        <section className="f-sect" onClick={this.redirectStory}>
                            <div className="f-post-description">
                                {this.props.data && this.props.data.url_metadata && this.props.data.url_metadata.urlMetadataResult && URLDisplay(this.props.data)}
                                <Linkify options={this.state.linkifyOptions}>
                                    <p>{this.props.data.snap_description}</p>
                                </Linkify>
                            </div>
                        </section>
                        <footer className="f-foot">
                            <div className="f-west">
                                <ul>
                                    {this.props.data.conversation_enabled && <li><span className={`f-comment`} onClick={(e) => this.comment(this.props.data.post_id)} >{this.props.data.conversation_count} {this.props.data.conversation_count_new != '0' && <i />}</span></li>}
                                    <li><span className={`f-share   ${this.props.data.shared_user_count == 0 ? 'crsr-nt' : ''}`} onClick={(e) => this.viewList(e, 'Shared By')}>{this.props.data.shared_user_count}</span></li>
                                    {this.props.data.created_by && this.props.data.created_by == this.state.user_id &&
                                        <li><span className={`f-viewed  ${this.props.data.views_count == 0 ? 'crsr-nt' : ''}`} onClick={(e) => this.viewList(e, 'Viewed By')}>{this.props.data.views_count}</span></li>
                                    }
                                </ul>
                            </div>
                            <div className="f-east">
                                {/* <span class="f-docs">2 Docs</span> */}
                                <Dropdown overlay={() => this.getMenuTemplate(this.props.data)} placement="bottomLeft">
                                    <Button />
                                </Dropdown>
                            </div>
                        </footer>
                    </Card>}
                {/* Communications */}
                {
                    (this.props.chatVisible===true|| this.props.chatVisible===undefined)&&<div
                        style={{
                            margin: '0px auto',
                            width: '720px',
                            minHeight: '200px',
                            marginTop: '20px'
                        }}
                        className="f-story-communications f-have-drop-menu">
                        <h4>Conversations</h4>
                        <div className="f-story-communication">
                            <ChatSection
                                one_Accepted={true}
                                is_accept={true}
                                data={this.props.data.comments}
                                userId={this.state.user_id}
                                comment={updateComment}
                                topicId={this.props.data.post_id}
                                trigger_={false}
                                deleteConversation={deleteConversation}
                                history={this.props.history}
                                created_user={""}
                                acceptOrRejectUserTopic={() => { }}
                                topicOwner={false}
                                chatloader={false}
                                post_id={this.props.data.post_id}
                                to_group_id={this.props.data.to_group_id}
                            />
                        </div>
                    </div>
                }
                {this.state.openDrawer && <RightSidebar title={this.state.title} data={this.state.data} visible={this.state.openDrawer} loader={this.state.loader} onCloseCallback={this.onCloseCallback} />}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let { userId } = state.user_details;
    return {
        userID: userId
    }
}
export default connect(mapStateToProps, null)(PostsCard);