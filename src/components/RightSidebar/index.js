import React from 'react';
import { Drawer, Avatar } from "antd";
import * as moment from 'moment'
import './style.scss'
import {encryption} from '../../shared/encryptDecrypt';
import { withRouter } from "react-router";
class RightSidebar extends React.Component {
    props;
    state;
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }
    componentWillMount() {
        console.log('componentDidUpdate');
        // if (this.props.visible != this.state.visible) {
        this.setState({ visible: this.props.visible });
        // }
    }
    onClose = () => {
        this.props.onCloseCallback();
        // this.props.visible = false
        this.setState({ visible: false });
    }

    navigateToUserDetails(value){
        let curntId = (value.user_id) || (value.id)
        var userId = encryption(curntId)
        this.props.history.push(`/user-details/${userId}`,{prev:window.location.pathname});
    }

    render() {
        return (
            <div className="d-flex" id="wrapper">
                <Drawer
                width="320"
                    title={this.props.title}
                    placement="right"
                    closable={true}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    className="f-drawer-custom"
                >   <ul>
                        {this.props.data && this.props.data.length > 0 &&
                            this.props.data.map((item, index) => {
                                return <li 
                                className={`crsr-pnt`} 
                                key={index} 
                                onClick={() => this.navigateToUserDetails(item)}
                                >
                                    <Avatar 
                                    src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic}
                                     >
                                         </Avatar>
                                          <i>
                                              {item.shared_user_name ? item.shared_user_name +(item.to_group_name?' to '+item.to_group_name:' to public') 
                                              : item.full_name} at {moment(item.created_date).format( "MMM DD,YYYY ")}</i></li>
                            })
                        }
                    </ul>
                </Drawer>
            </div>
        )
    }
}

export default withRouter(RightSidebar);