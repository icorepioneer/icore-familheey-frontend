import React from 'react';
import './style.scss'
import { Card, Button, Avatar, Input } from 'antd';
import { encryption } from '../../shared/encryptDecrypt';
import { getEventShareList } from '../../modules/events/eventListing/api-eventList';
import RightSidebar from '../RightSidebar';
import ShareOrInviteModel from '../../modules/events/components/ShareORInvite'
import { getUserGroupMembers, getAllFamily, ShareORInvite } from '../../modules/events/eventDetails/api-eventDetails'
import {connect} from 'react-redux'

var moment = require('moment');
const { Search } = Input
class EventCard extends React.Component {
    state;
    constructor(props) {
        super(props);
        let {userID} = props
        this.state = {
            user_id: userID,
            openDrawer: false,
            eventId: '',
            shared_list: [],
            loader: true,
            _sh_visible: false,
            currentItem: '',
            status_shareORinvite: '',
            event_id: '',
            _loading: false,
            searchParam: false
        }
    }

    navigateToEventDetails(value) {
        var id = value.event_id || value.id;
        var eventId = encryption(id)
        this.setState({ eventId: eventId, event_id: id })
        this.props.history.push(`/event-details/${eventId}`,{prev:`${window.location.pathname}`});
    }

    navigateToUserDetails(id) {
        var userId = encryption(id)
        this.props.history.push(`/user-details/${userId}`,{prev:window.location.pathname});
    }
    openDrawer = (event_id) => e => {
        this.setState({ openDrawer: true, loader: true })
        var params = {
            event_id: event_id.toString(),
            user_id: this.state.user_id
        }
        getEventShareList(params).then((respose) => {
            this.setState({ shared_list: respose.data.data, loader: false })
        }).catch((error) => {
        })
    }
    onClose = () => {
        this.setState({
            openDrawer: false,
        });
    };
    onCloseCallback = () => {
        this.setState({ openDrawer: false, loader: false });
    }
    _sh_showModal = (buttonType, currentItem) => {
        this.setState({
            _sh_visible: true,
            currentItem: currentItem,
            status_shareORinvite: buttonType
        })
    };
    sh_handleCancel = e => {
        this.setState({
            _sh_visible: false,
        })
        this.sh_setLoader();
    };
    sh_setLoader = () => {
        let { _loading } = this.state;
        this.setState({
            _loading: _loading ? !this.setState._loading : false
        })
    }
    dateFormatTodisplay = (eventDetails) => {
        let { from_date, to_date } = eventDetails;
        let date;
        switch (true) {
            case moment.unix(from_date).isSame(moment.unix(to_date)):
                date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')}`
                break;
            case moment.unix(from_date).get('year') == moment.unix(to_date).get('year'):
                date = `${moment.unix(from_date).format('ddd,MMM DD ')} to ${moment.unix(to_date).format('ddd,MMM DD,YYYY')}`
                break;
            case moment.unix(from_date).get('year') != moment.unix(to_date).get('year'):
                date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')} - ${moment.unix(to_date).format('ddd,MMM DD YYYY')}`
                break;
            default:
                date = `${moment.unix(from_date).format('ddd,MMM DD YYYY')} - ${moment.unix(to_date).format('ddd,MMM DD YYYY')}`
                break;
        }
        return date
    }
    eventHeader = (data, title) => {
        let { _sh_showModal } = this;
        return (
            <div className="f-clearfix">
                <div className="f-member-listing">
                    <div className="f-content-searchable f-alt-non-col" style={{ margin: '0 0 10px' }}>
                        {(data.length > 0 && title != 'familyEventDisplay') && <h4>{title}</h4>}
                        {/* {( !data[0].no_data || (data.length === 0)) && <Search placeholder="Search Events" loading={this.state._loading} onChange={(value) =>this.props.searchEvent(value)} />} */}

                    </div>
                </div>
                {/* {data === 0 || data.length === 0 && <Card className="eventCard f-event-card" loading={true}></Card>} */}
                {/* {( data[0].no_data || (data.length === 0)) && <div>No Events to Show</div>} */}
                {/* { (data.length === 0 ) && <div>No Events to Show</div>} */}
                {/* {data.length > 0 && !data[0].no_data && data.map((item, index) => { */}
                {data.length > 0 && data.map((item, index) => {
                    return <Card className="eventCard f-event-card" key={index} >
                        <header className="f-head">
                            <div className="f-west">
                                {/* Use If No User Image */}
                                {/* <Avatar icon="user" /> */}
                                <Avatar onClick={() => this.navigateToUserDetails(item.user_id)} src={item.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic : require('../../assets/images/male-avatar.jpg')} />
                                <span>
                                    <h5>{item.full_name ? (item.full_name) : (item.created_by_name ? item.created_by_name : '')}</h5>
                                    <h6>{moment.unix(item.created_at_).format('MMM DD YYYY hh:mm a')}</h6>
                                </span>
                            </div>
                            <div className="f-east">
                                {item.shared_events && (item.shared_events === true || item.shared_events === 'true') &&
                                    <Button className="f-btn-group" onClick={this.openDrawer(item.event_id)} />
                                }
                                {item.is_sharable && (item.is_sharable === true) &&
                                    <Button onClick={() => _sh_showModal("share", item)} className="f-btn-share" />
                                }
                            </div>
                        </header>
                        <section className="f-sect" onClick={() => this.navigateToEventDetails(item)}>
                            <div className="f-poster">

                                <img ref="image" src={item.event_image &&item.event_image!='undefined' ? process.env.REACT_APP_EVENT_CARD_CROP + process.env.REACT_APP_S3_BASE_URL + 'event_image/' + item.event_image : this.props.eventimageUri} />
                                <span className="f-overlay">
                                    {/* <Tag color="#ffdd72">{item.ticket_type}</Tag> */}
                                    
                                    {/* If Webinar */}
                                    {/* <Tag color="#ffdd72">Online</Tag> */}
                                    <ul>
                                        <li>{item.category}</li>
                                        <li>{item.event_type}</li>
                                    </ul>
                                </span>
                            </div>
                            <div className="f-agenda">
                                <div className="f-west">
                                    <span className="f-date">
                                        <i>{moment.unix(item.from_date).format('MMM')}</i>
                                        <strong>{moment.unix(item.from_date).format('DD')}</strong>
                                    </span>
                                </div>
                                <div className="f-east">
                                    <h3>{item.event_name}</h3>
                                    <h5 className="f-time">{this.dateFormatTodisplay(item)}</h5>
                                    {item.event_type !=='Online'?<h5 className="f-location">{item.location} <em></em></h5>
                                    :
                                    <h5 className="f-webinar">
                                    <img src={`${process.env.REACT_APP_S3_BASE_URL}${item.meeting_logo}`} />
                                        <em style={{color:"#1890ff"}}>{item.meeting_link}</em>
                                    </h5>}
                                </div>
                            </div>
                            <div className="f-meta">
                                <ul>
                                    <li>{item.going_count} Going</li>
                                    <li>{item.interested_count} Interested</li>
                                    {item.going_count && item.going_count > 0 &&
                                        <li>{item.first_person_going} {(item.going_count > 1 ? ('and ' + (item.going_count - 1) + ' others going') : 'is going')} </li>
                                    }
                                    {/* {item.going_count && item.going_count > 0 &&
                                <li>{item.going_count} going</li>
                                } */}
                                </ul>
                            </div>
                        </section>
                        {/* <footer className="f-foot"> */}
                        {/* <div className="f-action"> */}
                        {/* Disable other checkboxes if any option selected */}
                        {/* <Checkbox >Going</Checkbox> */}
                        {/* <Checkbox >Intrested</Checkbox> */}
                        {/* <Checkbox >Not Intrested</Checkbox> */}
                        {/* </div> */}
                        {/* <Button className="f-btn-calendar" /> */}
                        {/* </footer> */}
                    </Card>
                })}
            </div>
        )
    };

    render() {
        let { tag,from_family,handleButtonClick } = this.props;
        let { _sh_visible, status_shareORinvite, user_id, currentItem, _loading } = this.state;
        let { sh_handleCancel, sh_setLoader, eventHeader } = this;
        return (
            <div>
                <div>
                    {
                        this.props.type != 'myevent' &&
                        <div>
                            <div className="f-member-listing">
                                <div
                                    className="f-content-searchable f-alt-non-col"
                                    style={{ margin: '0 0 10px' }}
                                >
                                    <Search
                                        placeholder="Search Events"
                                        loading={this.state._loading}
                                        onChange={(value) => this.props.searchEvent(value)}
                                    />
                                    {from_family&&<Button onClick={()=>{handleButtonClick()}}>Create New Event</Button>}
                                </div>
                            </div>

                            {(this.props.exploreEventArray.length == 0 || Object.keys(this.props.exploreEventArray).length == 0) ?
                                <div>No Events to Show</div> : ''
                            }
                            <span style={{ marginTop: '100px' }}>{tag}</span>
                            {
                                this.props.type == 'familyEventDisplay' &&
                                // this.props.exploreEventArray && this.props.exploreEventArray.length > 0 &&
                                eventHeader(this.props.exploreEventArray, 'familyEventDisplay')

                            }
                            {
                                this.props.type == 'explore' &&
                                // this.props.exploreEventArray && this.props.exploreEventArray.length > 0 &&
                                eventHeader(this.props.exploreEventArray, 'Explore Events')
                            }
                            {
                                this.props.type == 'invitation' &&
                                // this.props.exploreEventArray && this.props.exploreEventArray.length > 0 &&
                                eventHeader(this.props.exploreEventArray, 'Invited Events')
                            }
                        </div>

                    }

                    {
                        this.props.type == 'myevent' &&

                        <div>
                            <div className="f-member-listing"><div className="f-content-searchable f-alt-non-col" style={{ margin: '0 0 10px' }}>   <Search placeholder="Search Events" loading={this.state._loading} onChange={(value) => this.props.searchEvent(value)} /> </div></div>
                            {
                                ((this.props.exploreEventArray.created_by_me && this.props.exploreEventArray.created_by_me.length == 0) && (this.props.exploreEventArray.rsvp_yes && this.props.exploreEventArray.rsvp_yes.length == 0) && (this.props.exploreEventArray.rsvp_maybe && this.props.exploreEventArray.rsvp_maybe.length == 0) && (this.props.pasteventObj && this.props.pasteventObj.data &&
                                    this.props.pasteventObj.data.length == 0)) ?
                                    <div>No Events to Show</div> : ''
                            }
                            <span style={{ marginTop: '100px' }}>{tag}</span>

                            {
                                this.props.exploreEventArray.created_by_me &&
                                this.props.exploreEventArray.created_by_me.length > 0 &&
                                eventHeader(this.props.exploreEventArray.created_by_me, 'Created By Me')
                            }
                            {
                                this.props.exploreEventArray.rsvp_yes &&
                                this.props.exploreEventArray.rsvp_yes.length > 0 &&
                                eventHeader(this.props.exploreEventArray.rsvp_yes, 'Going')

                            }
                            {
                                this.props.exploreEventArray.rsvp_maybe &&
                                this.props.exploreEventArray.rsvp_maybe.length > 0 &&
                                eventHeader(this.props.exploreEventArray.rsvp_maybe, 'May Be Going')

                            }
                            {
                                this.props.pasteventObj && this.props.pasteventObj.data &&
                                this.props.pasteventObj.data.length > 0 &&
                                eventHeader(this.props.pasteventObj.data, 'Past Events')

                            }
                        </div>
                    }
                    {this.state.shared_list && this.state.openDrawer && <RightSidebar title='Shared List' data={this.state.shared_list} visible={this.state.openDrawer} loader={this.state.loader} onCloseCallback={this.onCloseCallback} />}

                </div>

                <ShareOrInviteModel
                    sh_setLoader={sh_setLoader}
                    sh_visible={_sh_visible}
                    sh_handleCancel={sh_handleCancel}
                    getUserGroupMembers={getUserGroupMembers}
                    getAllFamily={getAllFamily}
                    user_id={user_id}
                    event_id={currentItem.event_id || currentItem.id}
                    ShareORInvite={ShareORInvite}
                    status={status_shareORinvite}
                    _loading={_loading}
                />
            </div>
        )
    }
}
const mapStateToProps=(state)=>{
    let {userId} = state.user_details;
    return {
        userID:userId
    }
}

export default connect(mapStateToProps,null)(EventCard);