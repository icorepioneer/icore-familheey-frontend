import React from 'react';
import './style.scss'
import { Button, Radio, Drawer, Checkbox, Modal, Input, Select, Steps, Icon, Upload, Switch, Menu } from 'antd';
import { listAllMembers } from '../../modules/memberListing/api-memberlist';
import { groupEvents } from '../../modules/events/eventListing/api-eventList';
import { updateFamily } from '../../modules/createFamily/api-family'
import { notifications } from '../../shared/toasterMessage'
import { connect } from 'react-redux'


const { SubMenu } = Menu;
const { Option } = Select;
const { Step } = Steps;
const { TextArea } = Input;
// const props = {
//     action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
//     listType: 'picture',
// }

// var moment = require('moment');


class FamilyDetailsSidebar extends React.Component {
    // state;
    // props;
    constructor(props) {
        super(props);
        this.props = props;
        let { userID } = props

        this.state = {
            fetchFamilyDetails: [],
            checkedMembers: [],
            groupEvents: [],
            modalVisible: false,
            familyDetails: this.props.familyPropValue.familyDetails,
            current: 0,
            showAdvSetting: false,
            diableButton: false,
            openLinkDrawer: false,
            familyLinkList: [],
            sidebar_data: {},
            user_id: userID

        }
        this.getgroupEvents()
    }

    getgroupEvents = () => {

        var params = {
            group_id: this.props.familyPropValue.group_id.toString(),
            user_id: this.state.user_id,
        }

        groupEvents(params).then(r_res => {
            let res = r_res.data;
            this.setState({ groupEvents: res.data.event_invitation.concat(res.data.event_share) })
        });
    }
    deleteConfirmation = () => {
        this.props.callbackFromParent('delete_family');
    }
    editFamily = () => {
        this.props.callbackFromParent('edit_family');
    }
    onRadioChange = (e) => {
        this.props.callbackFromParent(e.key);
    }
    addNewMember = () => e => {
        var params = {
            client_id: this.state.user_id,
            group_id: this.props.familyPropValue.group_id.toString()
        }
        listAllMembers(params).then((r_response) => {
            let response = r_response.data;
            if (response && response.rows && response.rows.length > 0) {
                this.setState({ visible: true, addMemberList: response.rows })
                console.log('response', response);
            }
        }).catch((error) => {
            console.log('errorr', error);
        })
    }
    checkedValue = (value) => e => {
        var checkedMembers = [];
        if (this.state.checkedMembers.indexOf(value) === -1) {
            checkedMembers.push(value);
            this.setState({ checkedMembers: checkedMembers })
        } else {
            checkedMembers = this.state.checkedMembers.filter(function (item) {
                return item !== value
            })
            this.setState({ checkedMembers: checkedMembers })
        }
        console.log('this.state.checkedMembers', this.state.checkedMembers);
    }
    onClose = () => {
        this.setState({
            visible: false, openLinkDrawer: false
        });
    };
    handleCancel = e => {
        this.setState({
            modalVisible: false,
        });
    };
    handleChange = (name) => e => {
        this.props.familyPropValue.familyDetails[name] = e.target.value;

    }
    handleSelect = (name) => e => {
        this.props.familyPropValue.familyDetails[name] = e
    }
    uploadPhoto = name => info => {
        if (info.file.status === 'uploading') {
            if (name === 'cover')
                this.setState({ loading: true });
            else
                this.setState({ logoLoad: true });
            return;
        }
        if (info.file.status === 'done') {
            if (name === 'cover') {
                this.getBase64(info.file.originFileObj, 'cover', imageUrl =>
                    this.setState({
                        imageUrl,
                        loading: false,
                    }),
                );
            } else {
                this.getBase64(info.file.originFileObj, 'logo', logoUrl =>
                    this.setState({
                        logoUrl,
                        logoLoad: false,
                    }),
                );
            }
        }
    }
    getBase64(img, type, callback) {
        console.log('imggg', img);
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
        if (type === 'cover') {
            this.setState({ coverpic: img })
        } else {
            this.setState({ propic: img })
        }
    }
    updateFamily = () => e => {
        this.setState({ diableButton: true })
        updateFamily(this.props.familyPropValue.familyDetails).then((response) => {
            if (response && response.data && response.data.data && response.data.data.length > 0) {
                notifications('success', 'Success', 'Update successfully')
                this.setState({ diableButton: false })
            }
        }).catch((error) => {
            notifications('error', 'Error', 'Something went wrong')
        })
    }

    render() {
        let { familyDetails } = this.state;
        return (
            <aside className="familyDetailsSidebar f-inside-sticky">
                {this.state.addMemberList && this.state.addMemberList.length > 0 &&
                    <Drawer
                        title="Basic Drawer"
                        placement="right"
                        closable={true}
                        onClose={this.onClose}
                        visible={this.state.visible}
                    >
                        {this.state.addMemberList.map((value, index) => {
                            return <Checkbox key={index} onChange={this.checkedValue(value.id)}>{value.full_name}</Checkbox>
                        })}
                    </Drawer>
                }

                {/* Sidebar Block */}
                <div className="f-sider-block f-sider-menu-incl">
                    {(this.props.familyPropValue.familyDetails.user_status === 'member' || this.props.familyPropValue.familyDetails.user_status === 'admin') &&
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={[this.props.initial]}
                            className="f-sider-menu"
                            onClick={(e) => this.onRadioChange(e)}
                        >
                            <Menu.Item key="10">
                                <img src={require('../../assets/images/icon/land9.svg')} />
                                <span>About Us</span>
                            </Menu.Item>
                            <Menu.Item key="1">
                                <img src={require('../../assets/images/icon/land1.svg')} />
                                <span>Feed</span>
                                {/* <i className="f-dot" /> */}
                            </Menu.Item>
                            <Menu.Item key="2">
                                <img src={require('../../assets/images/icon/land2.svg')} />
                                <span>Announcements</span>
                                <i>{this.props.count && this.props.count.announcement_count ? this.props.count.announcement_count : 0}</i>
                            </Menu.Item>
                            <Menu.Item key="16">
                                <img src={require('../../assets/images/icon/land2.svg')} />
                                <span>Requests</span>
                            </Menu.Item>
                            {/* <Menu.Item key="10">
                                <img src={require('../../assets/images/icon/chat.svg')} />
                                <span>Message Request</span>
                            </Menu.Item> */}

                            <SubMenu
                                key="sub1"
                                title={
                                    <span>
                                        <img src={require('../../assets/images/icon/land7.svg')} />
                                        <span>Members</span>
                                        {/* <i className="f-dot"></i> */}
                                    </span>
                                }
                            >
                                <Menu.Item key="3">
                                    <span>Members List</span>
                                    {/* <i className="f-dot"></i> */}
                                </Menu.Item>
                                <Menu.Item key="11">
                                    <span>Member Requests</span>
                                    {/* <i className="f-dot"></i> */}
                                </Menu.Item>
                                { this.props.familyPropValue.familyDetails.user_status === 'admin' && /*22 feb 2021 anand * */
                                <Menu.Item key="12">
                                    <span>Invite Members</span>
                                </Menu.Item>
                                }
                                <Menu.Item key="21">
                                    <span>Invited Members</span>
                                </Menu.Item>
                                {this.props.familyPropValue.familyDetails.user_status === 'admin' &&
                                    <Menu.Item key="13">
                                        <span>Blocked Members</span>
                                    </Menu.Item>
                                }
                                {this.props.familyPropValue.familyDetails.user_status === 'admin' &&
                                    <Menu.Item key="15">
                                        <span>Pending Requests</span>
                                    </Menu.Item>
                                }
                                {this.props.familyPropValue.familyDetails.user_status === 'admin' &&
                                    <Menu.Item key="20">
                                        <span>Payment history</span>
                                    </Menu.Item>
                                }
                            </SubMenu>
                            <Menu.Item key="4">
                                <img src={require('../../assets/images/icon/land4.svg')} />
                                <span>Events</span>
                                <i
                                // className="f-dot"
                                >{this.state.groupEvents ? this.state.groupEvents.length : 0}</i>
                                {/* <i className="f-dot"></i> */}
                            </Menu.Item>

                            <Menu.Item key="5">
                                <img src={require('../../assets/images/icon/land10.svg')} />
                                <span>Albums </span>
                                <i>{this.props.count && this.props.count.album_count ? this.props.count.album_count : 0}</i>
                            </Menu.Item>

                            <Menu.Item key="6">
                                <img src={require('../../assets/images/icon/land8.svg')} />
                                <span>Documents </span>
                                <i>{this.props.count && this.props.count.document_count ? this.props.count.document_count : 0}</i>
                            </Menu.Item>

                            {this.props.familyPropValue.familyDetails && this.props.familyPropValue.familyDetails.is_linkable &&
                                <SubMenu
                                    key="sub2"
                                    title={
                                        <span>
                                            <img src={require('../../assets/images/icon/land3.svg')} />
                                            <span>Linked Families</span>
                                        </span>
                                    }
                                >
                                    <Menu.Item key="7">
                                        <span>Families</span>
                                    </Menu.Item>
                                    <Menu.Item key="14">
                                        <span>Requests</span>
                                    </Menu.Item>
                                </SubMenu>
                            }

                            <Menu.Item key="8">
                                <img src={require('../../assets/images/icon/land5.svg')} />
                                <span>Calendar</span>
                            </Menu.Item>
                            <Menu.Item key="9">
                                <img src={require('../../assets/images/icon/land6.svg')} />
                                <span>My Feedback</span>
                            </Menu.Item>
                            {familyDetails.is_membership && familyDetails.user_status == 'admin' && <Menu.Item key="17">
                                <img src={require('../../assets/images/icon/mob_family.svg')} />
                                <span>Membership</span>
                            </Menu.Item>}
                            {/* {familyDetails.is_membership && familyDetails.user_status == 'admin' && <Menu.Item key="18">
                                <img src={require('../../assets/images/icon/mob_family.svg')} />
                                <span>Offline Membership</span>
                            </Menu.Item>} */}
                            <Menu.Item key="leave_family">
                                <img src={require('../../assets/images/icon/cls.png')} />
                                <span>Leave Family</span>
                            </Menu.Item>
                        </Menu>}
                    {(this.props.familyPropValue.familyDetails.user_status !== 'member' && this.props.familyPropValue.familyDetails.user_status !== 'admin') &&
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={['10']}
                            className="f-sider-menu"
                            onClick={(e) => this.onRadioChange(e)}
                        >
                            <Menu.Item key="10">
                                <img src={require('../../assets/images/icon/land9.svg')} />
                                <span>About Us</span>
                            </Menu.Item>
                            <Menu.Item key="1">
                                <img src={require('../../assets/images/icon/land1.svg')} />
                                <span>Feed</span>
                                {/* <i className="f-dot" /> */}
                            </Menu.Item>
                            <Menu.Item key="2">
                                <img src={require('../../assets/images/icon/land2.svg')} />
                                <span>Announcements</span>
                                {/* <i className="f-dot"></i> */}
                            </Menu.Item>
                            {/* <Menu.Item key="10">
                                <img src={require('../../assets/images/icon/chat.svg')} />
                                <span>Message</span>
                            </Menu.Item> */}
                        </Menu>}
                </div>
                {/* Sidebar Block */}
                {/* <div className="f-sider-block f-alt f-ord1">
                    <Radio.Group defaultValue={1} className="f-switch" onChange={this.onRadioChange}>
                        <Radio value={1} className="f-uid1">About Us</Radio>
                        <Radio value={2} className="f-uid2">Members</Radio>
                        <Radio value={3} className="f-uid3">Album</Radio>
                        <Radio value={4} className="f-uid4">Events</Radio>
                        <Radio value={5} className="f-uid5">Admins</Radio>
                        <Radio value={6} className="f-uid6">More</Radio>
                    </Radio.Group>
                </div> */}

                {/* Sidebar Block */}
                {
                    // this.props.user_status == 'admin' && <div className="f-sider-block f-ord2">
                    //     <div className="f-sider-head">
                    //         <ul className="f-action-btns">
                    //             <li><Button type="link" className="f-btn-edit" onClick={() => this.editFamily()}>Edit</Button></li>
                    //             <li><Button type="link" className="f-btn-settings">Settings</Button></li>
                    //             <li><Button type="link" className="f-btn-delete" onClick={() => this.deleteConfirmation()}>Delete Account</Button></li>
                    //         </ul>
                    //     </div>
                    // </div>
                }


                {/* Sidebar Block */}
                {/* <div className="f-sider-block f-ord3">
                    <div className="f-sider-head">
                        <h4>Upcomming Events</h4>
                        <Button className="f-alt" />
                    </div>
                    <div className="f-sider-body">
                        <ul>

                            {
                                this.state.groupEvents && this.state.groupEvents.length > 0 && this.state.groupEvents.map((item, index) => {

                                    return (
                                        <li key={index}>
                                            <Avatar src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + item.propic} />
                                            <span>
                                                <h5>{item.event_name}</h5>
                                                <h6>{moment(item.from_date * 1000).format('DD-MMM-YYYY')}</h6>
                                            </span>
                                        </li>
                                    );
                                })

                            }
                        </ul>
                    </div>
                    <div className="f-sider-foot">
                        <Button type="link">See more</Button>
                    </div>
                </div> */}

                {/* Sidebar Block */}
                {/* <div className="f-sider-block f-ord4">
                    <ul className="f-meta">
                        <li>Members <strong>{this.props.familyPropValue.count.familyMembers > 0 ? this.props.familyPropValue.count.familyMembers : 0}</strong></li>
                        <li>Upcomming Events <strong>{this.state.groupEvents.length}</strong></li>
                    </ul>
                </div> */}

                {/* Sidebar Block */}
                {(this.state.familyDetails.create_by === this.state.user_id) &&
                    <div className="f-sider-block f-ord5">
                        <ul className="f-action-btns">
                            <li><Button type="link" className="f-btn-edit" onClick={() => { this.setState({ modalVisible: true }) }}>Edit</Button></li>
                            <li><Button type="link" className="f-btn-settings">Settings</Button></li>
                            <li><Button type="link" className="f-btn-delete">Delete Familheey</Button></li>
                        </ul>
                    </div>
                }
                <Modal
                    className="f-modal f-modal-wizard"
                    title="Edit Family"
                    visible={this.state.modalVisible}
                    onCancel={this.handleCancel}
                >
                    <Steps className="f-steps" progressDot current={this.state.current}>
                        <Step />
                        <Step />
                        <Step />
                        <Step />
                    </Steps>
                    {this.state.current === 0 &&
                        <div className="f-modal-wiz-body">
                            <div className="f-modal-wiz-step">
                                <h4>Basic Details</h4>
                                <div className="f-form-control">
                                    <label>Family Name</label>
                                    <Input id='familyName' placeholder="Eg: Nigeria womens group" defaultValue={this.props.familyPropValue.familyDetails.group_name} onChange={this.handleChange('group_name')} />
                                </div>
                                <div className="f-form-control">
                                    <label>Family Type</label>
                                    <Select placeholder="Eg: Company" defaultValue={this.props.familyPropValue.familyDetails.group_type} onChange={this.handleSelect('group_type')}>
                                        <Option value="public">Public</Option>
                                        <Option value="private">Private</Option>
                                        <Option value="regular">regular</Option>
                                    </Select>
                                </div>
                                <div className="f-form-control">
                                    <label>Base region or location</label>
                                    <Input placeholder="Eg: Newyork" defaultValue={this.props.familyPropValue.familyDetails.base_region} onChange={this.handleChange('base_region')} />
                                </div>
                            </div></div>
                    }
                    {this.state.current === 1 &&
                        <div className="f-modal-wiz-step" >
                            <h4>Family Details</h4>
                            <div className="f-form-control">
                                <label>A little bit about your family</label>
                                <TextArea placeholder="250 characters max" defaultValue={this.props.familyPropValue.familyDetails.base_region} rows={6} onChange={this.handleChange('familyIntro')} />
                            </div>
                        </div>
                    }
                    {this.state.current === 2 &&
                        <div className="f-modal-wiz-step" >
                            <h4>Upload Photo</h4>
                            <div className="f-form-control">
                                <label>Please take a moment to upload your family Cover pic as well as Logo.</label>
                                <div className="f-split">
                                    <div>
                                        <Upload onChange={this.uploadPhoto('cover')}>
                                            <Button>
                                                <img src={process.env.REACT_APP_COVER_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'cover_pic/' + this.props.familyPropValue.familyDetails.cover_pic} /> Upload cover photo
                                                    </Button>
                                        </Upload>
                                    </div>
                                    <div>
                                        <Upload onChange={this.uploadPhoto('logo')}>
                                            <Button>
                                                <img src={process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'logo/' + this.props.familyPropValue.familyDetails.logo} /> Upload logo
                                                    </Button>
                                        </Upload>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }

                    {this.state.current === 3 &&
                        <div className="f-modal-wiz-step" >
                            <h4>Family Settings</h4>
                            <div className="f-form-control f-wiz-switch">
                                <ul>
                                    <li>
                                        <h6>Make this family private</h6>
                                        <Switch defaultChecked={this.props.familyPropValue.familyDetails.visibility} onChange={this.handleSelect('visibility')} />
                                    </li>
                                    <li>
                                        <h6>Make this group searchable</h6>
                                        <Switch defaultChecked={this.props.familyPropValue.familyDetails.searchable} onChange={this.handleSelect('searchable')} />
                                    </li>
                                    <li>
                                        <h6>Link with other families as well</h6>{this.props.familyPropValue.familyDetails.is_linkable}
                                        <Switch defaultChecked={this.props.familyPropValue.familyDetails.is_linkable} onClick={this.handleSelect('is_linkable')} /> {this.props.familyPropValue.familyDetails.is_linkable && <Button onClick={() => { this.setState({ openLinkDrawer: true }) }} type="link">Select</Button>}
                                    </li>
                                </ul>
                            </div>
                            <div className="f-form-control">
                                <Button type="link" onClick={() => { this.setState({ showAdvSetting: true }) }}>Advanced Settings</Button>
                            </div>
                            {/* Advanced Settings */}
                            {this.state.showAdvSetting &&
                                <div className="f-advanced">
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>How members can join?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.member_joining} onChange={this.handleChange('member_joining')}>
                                            <Radio value={1}>Invitation Only</Radio>
                                            <Radio value={2}>Anyone can join<br /><small>(with approval)</small></Radio>
                                            <Radio value={3}>Anyone can join</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Who can create posts, events, albums and documents?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.post_create} onChange={this.handleChange('post_create')}>
                                            <Radio value={6}>Members</Radio>
                                            <Radio value={7}>Admin only</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Post Visibility</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.post_visibilty} onChange={this.handleChange('post_visibilty')}>
                                            <Radio value={8}>Members only</Radio>
                                            <Radio value={9}>Any one</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Who can approve posts?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.post_approval} onChange={this.handleChange('post_approval')}>
                                            <Radio value={10}>Not needed</Radio>
                                            <Radio value={11}>Admin</Radio>
                                            <Radio value={12}>Members<br /><small>(with approval)</small></Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Who can link family?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.link_family} onChange={this.handleChange('link_family')}>
                                            <Radio value={13}>Admins</Radio>
                                            <Radio value={14}>Member</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Who can approve link request?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.link_approval} onChange={this.handleChange('link_approval')}>
                                            <Radio value={15}>Member</Radio>
                                            <Radio value={16}>Admins</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Who can create announcements ?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.announcement_create} onChange={this.handleChange('announcement_create')}>
                                            <Radio value={17}>Admin only</Radio>
                                            <Radio value={18}>Any member</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Who can view announcemnts ?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.announcement_visibility} onChange={this.handleChange('announcement_visibility')}>
                                            <Radio value={22}>Public</Radio>
                                            <Radio value={21}>Members</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="f-form-control f-wiz-checkbox">
                                        <label>Who can view requests?</label>
                                        <Radio.Group defaultValue={this.props.familyPropValue.familyDetails.request_visibility} onChange={this.handleChange('request_visibility')}>
                                            <Radio value={27}>Members</Radio>
                                            <Radio value={26}>Admin only</Radio>
                                        </Radio.Group>
                                    </div>
                                </div>
                            }
                        </div>
                    }
                    <div className="f-modal-wiz-footer">
                        {this.state.current < 3 && this.state.current === 0 &&
                            <Button onClick={() => { this.setState({ modalVisible: false }) }} >Cancel</Button>
                        }
                        {this.state.current > 0 &&
                            <Button onClick={() => { this.setState({ current: this.state.current - 1 }) }} > <Icon type="arrow-left" /> Back </Button>
                        }
                        {this.state.current < 3 &&
                            <Button onClick={() => { this.setState({ current: this.state.current + 1 }) }} >Continue <Icon type="arrow-right" /></Button>
                        }
                        {this.state.current === 3 &&
                            <Button style={{ minWidth: 150 }} disabled={this.state.diableButton} onClick={this.updateFamily()}>Update</Button>
                        }

                    </div>
                    {
                        <Drawer
                            title="Basic Drawer"
                            placement="right"
                            closable={true}
                            onClose={this.onClose}
                            visible={this.state.openLinkDrawer}
                        >
                            {this.state.familyLinkList && this.state.familyLinkList.length > 0 &&
                                this.state.familyLinkList.map((value, index) => {
                                    return <Checkbox key={index} onChange={this.checkedValue(value.id)}>{value.full_name}</Checkbox>
                                })}
                        </Drawer>
                    }
                </Modal>
            </aside>
        )
    }
}
const mapStateToProps = (state) => {
    let { userId } = state.user_details;
    return {
        userID: userId
    }
}
export default connect(mapStateToProps, null)(FamilyDetailsSidebar);