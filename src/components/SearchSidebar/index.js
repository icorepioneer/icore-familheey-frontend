import React, { useEffect, useState } from 'react';
import './style.scss'
import { Menu } from 'antd';
import { useSelector } from 'react-redux';
const SearchSidebar = ({ callbackFromParent, search_text, loader }) => {
    const storeval = useSelector(stateX => stateX.user_details);
    const [state, setState] = useState({
        defaultSelectedKey: '-1'
    })
    let { defaultSelectedKey } = state;

    /**
     * on discover search set the default value sidebar
     */
    // useEffect(() => {
    //     if (storeval.search_term == 'family') {
    //         setState(prev => ({ ...prev, defaultSelectedKey: '1' }))
    //     }
    // }, [storeval.search_term])

    /**
     * sidebar click event
     */
    const onMenuClick = (e) => {
        setState(prev => ({ ...prev, defaultSelectedKey: e.key }))
        callbackFromParent(e.key, search_text);
    }
    return (
        <aside className="SearchSidebar f-sider">
            {/* Sidebar Block */}
            <div className="f-sider-block f-sider-menu-incl">
                <Menu
                    mode="inline"
                    selectedKeys={[defaultSelectedKey]}
                    className="f-sider-menu"
                    onClick={(e) => onMenuClick(e)}
                >
                    <Menu.Item key="0">
                        <img src={require('../../assets/images/icon/left.png')} />
                        <span>Previous page</span>
                    </Menu.Item>
                    <Menu.Item key="1">
                        <img src={require('../../assets/images/icon/land3.svg')} />
                        <span>Family</span>
                    </Menu.Item>
                    <Menu.Item key="2">
                        <img src={require('../../assets/images/icon/land7.svg')} />
                        <span>People</span>
                    </Menu.Item>
                    <Menu.Item key="3">
                        <img src={require('../../assets/images/icon/land4.svg')} />
                        <span>Events</span>
                    </Menu.Item>
                    <Menu.Item key="4">
                        <img src={require('../../assets/images/icon/land1.svg')} />
                        <span>Post</span>
                    </Menu.Item>
                    {/* <Menu.Item key="5" className="f-hide-on-ipad">
                    <img src={require('../../assets/images/icon/land2.svg')} />
                    <span>Announcements</span>
                    </Menu.Item> */}
                </Menu>
            </div>
        </aside>
    )
}
export default SearchSidebar;