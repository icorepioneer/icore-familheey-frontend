import React from "react";
import axios from "axios";
import { encryption } from './shared/encryptDecrypt';
import { Button } from "antd";
class Callto extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appLink: null,
            browserLink: null,
            idParam: null,
            pageDetails: null,
            deviceType: null,
            isCheckAppInstalled: false
        }
    }
    componentDidMount() {
        let device = 'web';
        var typeName = '';
        let urlredirect = 'familheey://';
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if (/android/i.test(userAgent)) {
            device = "android";
        }
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            device = "ios";
        }
        let URL = window.location.href.split("page/");
        let innerUrlArray = URL[1].split("/");
        this.setState({ pageDetails: innerUrlArray[0] });
        this.setState({ deviceType: device });
        axios.get(`${process.env.REACT_APP_REDIRECTS3}/${URL[1]}`).then((response) => {
            if (response && response.data && response.data.data && response.data.data.length > 0) {
                // Deep link to your app goes here
                let id = response.data.data[0].id;
                this.setState({ idParam: id });
                if (device == 'android') {
                    // var typeName = '';
                    switch (innerUrlArray[0]) {
                        case 'events':
                            typeName = 'event';
                            break;
                        case 'posts':
                            typeName = 'post';
                            break;
                        case 'groups':
                            typeName = 'family';
                            break;
                        default:
                            // typeName = '';
                            break;
                    }
                    urlredirect = 'familheey://send?type=' + typeName + '&type_id=' + response.data.data[0].id;
                    this.setState({ appLink: urlredirect });
                } else if (device == 'ios') {
                    // var typeName = '';
                    switch (innerUrlArray[0]) {
                        case 'events':
                            typeName = 'event';
                            break;
                        case 'posts':
                            typeName = 'posts';
                            break;
                        case 'groups':
                            typeName = 'family';
                            break;
                        default:
                            // typeName = '';
                            break;
                    }
                    urlredirect = 'familheey://send?type=' + typeName + '&id=' + response.data.data[0].id;
                    this.setState({ appLink: urlredirect });
                } else {
                    switch (innerUrlArray[0]) {
                        case 'events':
                            var eventId = encryption(id);
                            this.props.history.push(`/event-details/` + eventId);
                            break;
                        case 'posts':
                            var postId = encryption(id);
                            this.props.history.push(`/post_details/` + postId);
                            break;
                        case 'groups':
                            var grouptId = encryption(id);
                            this.props.history.push(`/familyview/` + grouptId);
                            break;
                        default:
                            this.props.history.push(`/`);
                            break;
                    }
                }
                // this.checkWhatToRender();
            } else {
                if (device == 'android') {
                    // let urlredirect = 'familheey://';
                    this.setState({ appLink: urlredirect });
                } else if (device == 'ios') {
                    // let urlredirect = 'familheey://';
                    this.setState({ appLink: urlredirect });
                } else {
                    this.props.history.push(`/`);
                }
                // this.checkWhatToRender();
            }
        }).catch(error => {
            if (device == 'android') {
                // let urlredirect = 'familheey://';
                this.setState({ appLink: urlredirect });
            } else if (device == 'ios') {
                // let urlredirect = 'familheey://';
                this.setState({ appLink: urlredirect });
            } else {
                this.props.history.push(`/`);
            }
            // this.checkWhatToRender();
        });
    }

    goToApp = () => {
        window.location.href = this.state.appLink;
        this.setState({ isCheckAppInstalled: true });
        if (this.state.deviceType == 'android') {
            setTimeout(() => {
                //  window.location = "market://details?id=com.familheey.app"
                window.location = "https://play.google.com/store/apps/details?id=com.familheey.app&hl=en"
                // window.location = "https://familheey-android-apk.s3.amazonaws.com/app-release.apk"
                // window.location.href = this.state.appLink;
                this.setState({ isCheckAppInstalled: false });
            }, 3000);
        } else {
            setTimeout(() => {
                window.location = "https://apps.apple.com/us/app/familheey/id1485617876?ls=1"
                // window.location.href = this.state.appLink;
                this.setState({ isCheckAppInstalled: false });
            }, 3000);
        }
        setTimeout(() => {
            clearTimeout();
        }, 3300);
    }

    goToPage = () => {
        switch (this.state.pageDetails) {
            case 'events':
                var eventId = encryption(this.state.idParam);
                this.props.history.push(`/event-details/` + eventId);
                break;
            case 'posts':
                var postId = encryption(this.state.idParam);
                this.props.history.push(`/post_details/` + postId);
                break;
            case 'groups':
                var grouptId = encryption(this.state.idParam);
                this.props.history.push(`/familyview/` + grouptId);
                break;
            default:
                this.props.history.push(`/`);
                break;
        }
    }
    render() {
        return (
            <div style={{ textAlign: 'center', marginTop: '20%' }}>
                {!this.state.isCheckAppInstalled && <Button style={{ display: 'inline-block', margin: '5px' }} onClick={(e) => this.goToApp()}>Click To Open In App</Button>}
                {!this.state.isCheckAppInstalled && <Button style={{ display: 'inline-block', margin: '5px' }} onClick={(e) => this.goToPage()}>Click To Open In Browser</Button>}
                {this.state.isCheckAppInstalled && <p style={{ display: 'inline-block', margin: '5px' }}>Checking if your phone have Famhileey app installed. Please wait!!!</p>}
            </div>
        ); //render function should return something
    }

}
export default Callto;