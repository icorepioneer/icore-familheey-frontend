import axios from 'axios';
import getStateFromLocal from '../../src/shared/getUserDetailsFromLocal'
const fetchClient = () => {
    let app_details = {app_name:"familheey",app_version:"1.9.0.3",os:"Web"}
    let user_id = getStateFromLocal()&&getStateFromLocal()[0]
    const defaultOptions = {
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
            'Content-Type': 'multipart/form-data',
            'logined_user_id':`${user_id}`,
            'app_info': JSON.stringify(app_details)
        },
    };

    // Create instance
    let instance = axios.create(defaultOptions);

    // Set the AUTH token for any request
    instance.interceptors.request.use(function (config) {
        const token = getStateFromLocal()&&getStateFromLocal()[1];
        config.headers.Authorization = token ? `Bearer ${token}` : '';
        return config;
    });
    return instance;
}
export default fetchClient();
