import axios from 'axios';

const fetchClient = () => {
    //let app_details = {app_name:"familheey",app_version:"1.9.0.3",os:"Web"}
    const defaultOptions = {
        baseURL: process.env.REACT_APP_API_URL,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    };
    // Create instance
    let instance = axios.create(defaultOptions);
    return instance;
}
export default fetchClient();
