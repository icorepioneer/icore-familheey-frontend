import ReactDOM from 'react-dom';
import React from 'react'
import './assets/css/theme.scss';
import './assets/css/style.scss';
import * as serviceWorker from './serviceWorker';
import Routing from './router-module';
// import { createStore } from 'redux';
import { Provider } from 'react-redux';
import store from './redux/store'
ReactDOM.render(
    <Provider
        store={store}>
        <Routing />
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
