import React, { Component } from 'react'
//import Card, {CardActions, CardContent} from 'material-ui/Card'
// import Card from '@material-ui/core/Card';
// import CardActions from '@material-ui/core/CardActions';
// import CardContent from '@material-ui/core/CardContent';
//import Button from 'material-ui/Button'
//import TextField from 'material-ui/TextField'
import Buttons from '@material-ui/core/Button';
// import TextField from "@material-ui/core/TextField";
//import Typography from 'material-ui/Typography'
import Typography from '@material-ui/core/Typography';
//import Icon from 'material-ui/Icon'
// import Icon from '@material-ui/core/Icon';
import PropTypes from 'prop-types'
//import {withStyles} from 'material-ui/styles'
import { withStyles } from "@material-ui/core/styles";
import { create, registerSocial, confirmOtp } from './api-auth.js'
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog'
import { Link } from 'react-router-dom';
// import config from '../config/config'

//import FacebookLogin from 'react-facebook-login';
// import { GoogleLogin } from 'react-google-login';
// import CustomLinkedIN from '../modules/user/linkedinAuth.js.js';
// import { FacebookProvider, LoginButton } from 'react-facebook';
import countries from '../common/countrylist.json';
import { Select } from "antd";
import { Redirect } from 'react-router-dom';
import { notifications } from '../shared/toasterMessage';

const { Option } = Select;

const styles = theme => ({
  card: {
    maxWidth: 600,
    margin: 'auto',
    textAlign: 'center',
    marginTop: theme.spacing.unit * 5,
    paddingBottom: theme.spacing.unit * 2
  },
  error: {
    verticalAlign: 'middle'
  },
  title: {
    marginTop: theme.spacing.unit * 2,
    color: theme.palette.openTitle
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300
  },
  submit: {
    margin: 'auto',
    marginBottom: theme.spacing.unit * 2
  }
})

class Signup extends Component {
  constructor(props) {
    super(props);
    if (localStorage.getItem('user_id')) {
      this.props.history.push(`/events`)
    }
  }
  state = {
    full_name: '',
    password: '',
    email: '',
    phone: '',
    open: false,
    error: '',
    gender: '',
    country: '',
    otpcheck: false,
    otpvalue: '',
    countryList: countries.countries,
    selectValue: '',
    redirectToReferrer: false,
  }

  handleChange = name => event => {
    if (name === 'phone') {
      var re = /^[0-9\b]+$/;
      if (event.target.value === '' || re.test(event.target.value)) {
        this.setState({ [name]: event.target.value, error: '' })
      } else {
        var message = 'Error'
        var description = 'enter a valid phone number'
        notifications('error', message, description)
        // this.setState({ error: 'enter a valid phone number' })
      }
    } else {
      this.setState({ [name]: event.target.value })
    }
  }

  clickSubmit = () => {

    var errcount = 0;
    var errtxt = '';


    const user = {
      full_name: this.state.full_name || undefined,
      email: this.state.email || undefined,
      password: this.state.password || undefined,
      phone: this.state.phone || undefined,
      gender: this.state.gender || undefined,
      country: this.state.selectValue || undefined
    }

    if (!user.full_name) {
      errtxt += "Name is required "
      errcount++;
    } else if (!user.gender) {
      errtxt += "Gender is required "
      errcount++;
    } else if (!user.email) {
      errtxt += "Email is required "
      errcount++;
    } else if (!user.phone) {
      errtxt += "Phone is required "
      errcount++;
    } else if (!user.country) {
      errtxt += "Country is required "
      errcount++;
    } else if (!user.password) {
      errtxt += "Password is required "
      errcount++;
    } else if (this.state.phone.length !== 10) {
      errtxt += "invalid phone number "
      errcount++;
    }


    if (errcount > 0) {
      // this.setState({ error: errtxt })
      var message = 'Error'
      var description = errtxt
      notifications('error', message, description)
      return;
    }
    create(user).then((data) => {
      if (data.error) {
        this.setState({ error: data.error })
      } else {
        this.setState({ error: '', otpcheck: true })
      }
    })
  }
  submitOtp = () => {
    var otpObj = {
      otp: this.state.otpvalue || undefined,
      phone: this.state.selectValue + this.state.phone
    }
    confirmOtp(otpObj).then((data) => {
      if (data.data.id) {
        localStorage.setItem('user_id', data.data.id)
        localStorage.setItem('token', data.data.accessToken)

      }
      this.setState({ otpcheck: false })
      window.location.reload()

      // if (data && data.error) {
      //   this.setState({ error: data.error })
      // } else {
      //   this.setState({ otpcheck: false,redirectToReferrer:true })
      // }
    })
  }
  handleChild(status) {
    this.setState(status)
  }
  onChange = (value) => {
    var str = value.split("/");
    this.setState({ selectValue: str[1] })
  }


  render() {
    if (this.state.redirectToReferrer) {
      return (<Redirect to={{ pathname: '/events' }} />)
    }
    const { classes } = this.props
    return (<section className="fg-auth-page fh-auth-signup">
      <div className="fh-auth-left">
        <div>
          <h2>I am <strong>rich</strong> as<br />I have <strong className="fh-alt">reach</strong></h2>
          <span className="fh-app-buttons">
            <a href="https://apps.apple.com/us/app/familheey/id1485617876" target="_blank"><img src="images/button-appstore.png" alt="Download on AppStore" /></a>
            <a
              href="https://play.google.com/store/apps/details?id=com.familheey.app&hl=en"
              // href="https://familheey-android-apk.s3.amazonaws.com/app-release.apk"
              target="_blank"><img src="images/button-googleplay.png" alt="Get it on PlayStore" /></a>
          </span>
        </div>
      </div>
      {!this.state.otpcheck &&
        <div className="fh-auth-right">
          <div className="fh-auth-flex">
            <div className="fh-logo">
              <img src="images/logo.png" alt="Familheey" style={{ 'width': '53%' }} />
            </div>
            <div className="fg-form-items">
              <div className="fh-form-control fl-inp-flex">
                <input required id="name" placeholder="Name" data-lpignore="true" autoComplete="off" className="fh-icons fh-icon-user" value={this.state.full_name} onChange={this.handleChange('full_name')} margin="normal" />
              </div>
              <div className="fh-form-control fl-inp-flex">
                <input required id="gender" type="gender" data-lpignore="true" autoComplete="off" placeholder="Gender" className="fh-icons fh-icon-pass" value={this.state.gender} onChange={this.handleChange('gender')} margin="normal" />
              </div>
              {/* <div className="fh-form-control">
                <input required id="country" type="country" data-lpignore="true" autoComplete="off" placeholder="Country" className="fh-icons fh-icon-pass" value={this.state.country} onChange={this.handleChange('country')} margin="normal" />
              </div> */}
              <div className="fh-form-control fl-inp-flex">
                <input required id="email" type="email" data-lpignore="true" autoComplete="off" placeholder="Email" className="fh-icons fh-icon-mail" value={this.state.email} onChange={this.handleChange('email')} margin="normal" />
              </div>

              <div className="fh-form-control fl-inp-flex">
                <div className="code-selection f-fix">
                  <Select
                    value={this.state.selectValue}
                    showSearch
                    style={{ width: 80 }}
                    placeholder="Country Code"
                    onChange={this.onChange}
                    getPopupContainer={trigger => trigger.parentNode}
                  >
                    {this.state.countryList.map((value, index) => {
                      return <Option key={index} value={value.name + '/' + value.code}>{value.name} ({value.code})</Option>
                    })}
                  </Select>
                </div>
                <input required id="phone" type="text" maxLength="10" placeholder="Phone" data-lpignore="true" autoComplete="off" className="fh-icons fh-icon-phone" value={this.state.phone} onChange={this.handleChange('phone')} margin="normal" />
              </div>
              <div className="fh-form-control fl-inp-flex">
                <input required id="password" type="password" data-lpignore="true" autoComplete="off" placeholder="Password" className="fh-icons fh-icon-pass" value={this.state.password} onChange={this.handleChange('password')} margin="normal" />
              </div>
              {this.state.error && (<Typography component="p" color="error">{this.state.error}</Typography>)}
            </div>

            <div className="fh-submit">
              <Buttons variant="contained" onClick={this.clickSubmit} className="fh-submit-btn">Sign Up</Buttons>
            </div>
            <div className="fh-sm-buttons fh-alt">
              <div className="fh-sm-signup">Already have an account? <Link to="/signin"><Buttons >Sign In</Buttons></Link></div>
            </div>
          </div>
        </div>}
      {this.state.otpcheck &&
        <div className="fh-auth-right">
          <div className="fh-form-control fl-inp-flex">
            <input id="otp" type="text" placeholder="OTP" className={classes.textField} value={this.state.otpvalue} onChange={this.handleChange('otpvalue')} margin="normal" />
          </div>
          <div className="fh-submit">
            <Buttons variant="contained" onClick={this.submitOtp} className="fh-submit-btn">submit</Buttons>
          </div>
        </div>
      }
      <Dialog open={this.state.open} disableBackdropClick={true}>
        <DialogTitle>New Account</DialogTitle>
        <DialogContent>
          <DialogContentText>
            New account successfully created.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Link to="/signin">
            <Buttons color="primary" autoFocus="autoFocus" variant="contained">
              Sign In
            </Buttons>
          </Link>
        </DialogActions>
      </Dialog>
    </section>)
  }

  /**
     * after sucessful login from gamil save data to db
     */
  responseGoogle = (res) => {
    console.log(res.profileObj)
    if (res.profileObj && res.profileObj.email) {
      registerSocial(res.profileObj)
        .then((data) => {
          console.log(" *************** -------------")
          console.log(data)
          if (data.status !== 400) {
            this.setState({ error: '', open: true })
          } else {
            this.setState({ error: data.message })
          }
        })
        .catch((err) => {
          this.setState({ error: err })
        })
    }
  }

  /**
   * retun from fb error/success
   */
  responseFB = (res) => {
    console.log(res)
    res['profile'].fbId = res['profile'].id;
    res['profile'].photo = res['profile'].picture ? res['profile'].picture['data'].url : '';
    res['profile'].type = "fb";
    if (res && res['profile']) {
      registerSocial(res['profile'])
        .then((data) => {
          if (data.status !== 400) {
            this.setState({ error: '', open: true })
          } else {
            this.setState({ error: data.message })
          }
        })
        .catch((err) => {
          this.setState({ error: err })
        })
    }
  }

  handleSuccess = (data) => {
    console.log(data)
  }

  handleFailure = (err) => {
    this.setState({ error: err.message })
  }


}

Signup.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Signup)
