import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import auth from "./../auth/auth-helper";
import { Redirect } from "react-router-dom";
import { Modal } from 'antd';
import { encryption } from '../shared/encryptDecrypt'
import {
  generateOtp,
  //checkOtp,
  signin,
  forgotPasswordApi,
  reset_password,
  userExist,
} from "./api-auth.js";
import "antd/dist/antd.css";
import { Select, Input, Spin } from "antd";
import countries from "../common/countrylist.json";
import { notifications } from "../shared/toasterMessage";
const { confirm } = Modal;

const { Option } = Select;
const styles = (theme) => ({
  card: {
    maxWidth: 600,
    margin: "auto",
    textAlign: "center",
    marginTop: theme.spacing.unit * 5,
    paddingBottom: theme.spacing.unit * 2,
  },
  error: {
    verticalAlign: "middle",
  },
  title: {
    marginTop: theme.spacing.unit * 2,
    color: theme.palette.openTitle,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300,
  },
  submit: {
    margin: "auto",
    marginBottom: theme.spacing.unit * 2,
  },
});

const Signin = ({ history, location }) => {

  const [state, setState] = useState({
    email: "",
    password: "",
    error: "",
    redirectToReferrer: false,
    otpstatus: false,
    password_status: false,
    onBoard: false,
    user_id: "",
    message: "",
    full_name: "",
    phone: "",
    user_info: {},
    auto_password: 0,
    countryList: countries.countries,
    selectValue: "+1",
    otpEnable: false,
    img_preview: "",
    loader: false,
    one: "",
    two: "",
    three: "",
    four: "",
    five: "",
    six: "",
    // redirect_uri: "",
    gender: undefined,
    origin: undefined,
    cover_pic: undefined,
    pro_pic: undefined,
    accessToken: "",
  });
  let dispatch = useDispatch();

  let {
    email,
    password,
    error,
    redirectToReferrer,
    otpstatus,
    password_status,
    onBoard,
    user_id,
    //message,
    full_name,
    phone,
    user_info,
    auto_password,
    countryList,
    selectValue,
    otpEnable,
    img_preview,
    loader,
    //one, 
    //two,
    //three,
    //four,
    //five,
    //six,
    // redirect_uri,
    gender,
    // cover_pic,
    // pro_pic,
  } = state;

  const onChange = (value) => {
    let str = value.split("/");
    setState((prev) => ({ ...prev, selectValue: str[1] }));
  };

  const clickSubmit = (e) => {
    if (password_status == true) {
      if (auto_password == 0) {
        if (password == '') {
          notifications(
            "error",
            "Error",
            "Password is mandatory"
          );
          return;
        }
        setState((prev) => ({ ...prev, loader: true }));
        signin({
          phone: selectValue + phone,
          password: password,
        })
          .then((result) => {
            let {
              gender: gender_data,
              origin: origin_data,
              location: location_data,
              full_name: full_name_data,
              email: email_data,
              id: id_data,
              accessToken: accessToken_data,
              refreshToken: refreshToken_data,
              phone: phone_data
            } = result.data;
            let result_data = result.data
            auth.authenticate(
              { accessToken: accessToken_data, refreshToken: refreshToken_data },
              () => {
                dispatch({ type: "USER_DETAILS_UPDATE", payload: { data: result_data } });
              }
            );
            setState((prev) => ({
              ...prev,
              gender: gender_data,
              origin: origin_data,
              location: location_data,
              full_name: full_name_data,
              email: email_data,
              loader: false,
            }));
            let to_encrypt = `${id_data}#@#${accessToken_data}#@#${refreshToken_data}#@#${phone_data}`
            localStorage.setItem('hash_key', encryption(to_encrypt))
            dispatch({
              type: "USER_ADD",
              payload: {
                phone: phone_data,
                user_id: id_data,
                accessToken: accessToken_data,
                token: refreshToken_data,
              },
            });
          })
          .catch((err) => {
            let { data } = err.response;
            let { code } = data.message;
            setState((prev) => ({ ...prev, loader: false }));
            if (code && code == 'NotAuthorizedException') {
              confirm({
                title: `Sorry, you account have been suspended. Please email us on contact@familheey.com to reactivate your account.`,
                okText: 'Compose mail',
                cancelText: 'No',
                onOk() {
                  emailToAdmin();
                },
                onCancel() {
                },
              });
            } else {
              notifications("error", "Error", "Invalid username or password");
            }
          });
      } else {
        var pe = /^\S{8,}$/;
        if (password && pe.exec(password) == null) {
          notifications(
            "error",
            "Error",
            "Minimum password length is 8 and space not allowed"
          );
          return;
        }
        reset_password({
          phone: selectValue + phone,
          old_pwd: "",
          new_pwd: password,
        })
          .then((result) => {
            setState((prev) => ({ ...prev, loader: false }));
            if (result.data && result.data.data) {
              let { id, accessToken: accessToken_data } = result.data.data;
              dispatch({
                type: "ADD",
                payload: {
                  user_id: id,
                  accessToken: accessToken_data,
                  token: accessToken_data,
                },
              });
              history.push("/post");
            }
          })
          .catch((err) => {
            notifications("error", "Error", err.message);
            setState((prev) => ({ ...prev, loader: false }));
          });
      }
      return;
    }
    let errcount = 0;
    let errtxt = "";
    let description_ = "";
    if (phone.length <= 4) {
      errtxt += "Invalid Mobile number ";
      errcount++;
    } else if (phone.length === 0) {
      description_ += `Mobile number is required `;
      errcount++;
    }
    if (errcount > 0) {
      description_ += errtxt;
      notifications("error", "Error", description_);
      return;
    }
    let userExistInput = {
      phone: selectValue + phone,
    };
    if (errcount > 0) {
      description_ += errtxt;
      notifications("error", "Error", description_);
      return;
    }
    setState((prev) => ({ ...prev, loader: true }));
    userExist(userExistInput).then((resp) => {
      let { data, isUserExist } = resp.data;
      if (isUserExist) {
        let { auto_password: auto_password_data } = data[0];
        auto_password_data && (auto_password_data == 1 || auto_password_data == true) && forgotPassword();
      }
      if (data && data.length > 0) setState((prev) => ({ ...prev, user_id: data[0].id }));

      if (resp && resp.data && resp.data.data[0] && resp.data.isUserExist) {
        setState((prev) => ({
          ...prev,
          error: "",
          password_status: true,
          loader: false,
          auto_password: resp.data.data[0].auto_password,
        }));
      } else {
        setState((prev) => ({
          ...prev,
          error: "",
          otpstatus: true,
          loader: false,
        }));
      }
    });
  };


  const emailToAdmin = () => {
    window.location = `mailto:contact@familheey.com?subject='Reactivate Account - ${user_id}'`
  }

  const clickOtp = () => {
    setState((prev) => ({ ...prev, otpstatus: true }));
    const user = {
      phone: selectValue + phone || undefined,
    };
    if (!phone || phone.length !== 10) {
      let description = `Please enter a valid phone number`;
      notifications("error", "Error", description);
      return;
    }
    generateOtp(user).then((data) => {
      let resp_data = data.data;
      if (resp_data && resp_data.message.data === "") {
        let description = `Please enter a valid phone number`;
        notifications("warning", "Warning", description);
      } else {
        console.log(resp_data);
        setState((prev) => ({ ...prev, error: "" }));
      }
    });
  };

  const forgotPassword = () => {
    forgotPasswordApi({ phone: selectValue + phone }).then((data) => {
      setState((prev) => ({
        ...prev,
        error: "",
        otpstatus: true,
        loader: false,
      }));
      history.push(`/forgot`, {
        selectValue: selectValue,
        phone: phone,
      });
    }).catch((err) => {
      let { data } = err.response;
      let { code } = data.message;
      setState((prev) => ({ ...prev, loader: false }));
      if (code && code == 'NotAuthorizedException') {

        confirm({
          title: `Sorry, you account have been suspended. Please email us on contact@familheey.com to reactivate your account.`,
          okText: 'Compose mail',
          cancelText: 'No',
          onOk() {
            emailToAdmin();
          },
          onCancel() {
          },
        });
      } else {
        notifications("error", "Error", "Invalid username or password");
      }
    });
  };

  const handleChangeSelect = (name, event) => {
    setState((prev) => ({ ...prev, [name]: event }));
  };
  const handleEnterPress = (event, name) => {
    if ((name == "phone" || name == "password") && event.keyCode === 13) {
      clickSubmit(event);
    }
  };
  const handleChange = (name, event) => {
    event.persist();
    if (name === "phone") {
      var re = /^[0-9\b]+$/;
      if (event.target.value === "" || re.test(event.target.value)) {
        setState((prev) => ({
          ...prev,
          [name]: event.target.value,
          error: "",
        }));
      }
    } else {
      setState((prev) => ({ ...prev, [name]: event.target.value }));
    }
  };

  const cancelOtp = (e) => {
    setState((prev) => ({ ...prev, otpstatus: false, otpEnable: false }));
  };
  const uploadPhoto = (e) => {
    setState((prev) => ({ ...prev, pro_pic: e.target.files[0] }));
    let type = e.target.files[0].type;
    if (type.indexOf("image") == -1) {
      notifications("error", "Error", "Please upload image");
      return;
    }
    getBase64(e.target.files[0], (imageUrl) => {
      setState((prev) => ({ ...prev, img_preview: imageUrl }));
    });
  };
  const getBase64 = (img, callback) => {
    if (img.type.indexOf("image") != -1) {
      const reader = new FileReader();
      reader.addEventListener("load", () => callback(reader.result));
      reader.readAsDataURL(img);
      setState((prev) => ({ ...prev, image: img }));
    } else {
      callback("");
    }
  };

  // const { classes } = this.props
  useEffect(() => {
    const { from } = location.state || {
      from: {
        pathname: "/events",
      },
    };
    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
  }, [])

  return (
    <section className="fg-auth-page fh-auth-signin">
      <div className="fh-auth-left">
        <div>
          <h2>
            I am <strong>rich</strong> as <br />I have <strong className="fh-alt">reach</strong>
          </h2>
          {!otpstatus && (
            <span className="fh-app-buttons">
              <a
                href="https://apps.apple.com/us/app/familheey/id1485617876"
                target="_blank"
              >
                <img
                  src="images/button-appstore.png"
                  alt="Download on AppStore"
                />
              </a>
              <a
                href="https://play.google.com/store/apps/details?id=com.familheey.app&hl=en"
                // href="https://familheey-android-apk.s3.amazonaws.com/app-release.apk"
                target="_blank"
              >
                <img
                  src="images/button-googleplay.png"
                  alt="Get it on PlayStore"
                />
              </a>
            </span>
          )}
        </div>
      </div>

      <div className="fh-auth-right">
        <div className="fh-auth-flex">
          <div className="fh-logo">
            <img
              src="images/logo.png"
              alt="Familheey"
              style={{ width: "53%" }}
            />
          </div>
          {loader && (
            <div className="spin-loader">
              <Spin> </Spin>
            </div>
          )}

          {!otpstatus && !password_status && !onBoard && (
            <div className="fg-form-items fh-w-label">
              <label className="f-alt">Please enter your mobile number</label>
              <div className="fh-form-control fl-inp-flex">
                <div className="code-selection f-fix">
                  <Select
                    value={selectValue}
                    showSearch
                    placeholder="Country Code"
                    onChange={onChange}
                    style={{ width: "80px" }}
                    getPopupContainer={(trigger) => trigger.parentNode}
                  >
                    {countryList.map((value, index) => {
                      return (
                        <Option
                          key={index}
                          value={value.name + "/" + value.code}
                        >
                          {value.name} ({value.code})
                        </Option>
                      );
                    })}
                  </Select>
                </div>
                <input
                  id="phone"
                  type="text"
                  // maxLength="10"
                  placeholder="Phone"
                  value={phone}
                  onKeyUp={(e) => handleEnterPress(e, "phone")}
                  onChange={(e) => handleChange("phone", e)}
                  margin="normal"
                  className="fh-icons fh-icon-user"
                ></input>
              </div>
              {error && (
                <Typography component="p" color="error">
                  {error}
                </Typography>
              )}
            </div>
          )}

          {password_status && !onBoard && auto_password == 0 && (
            <div className="fg-form-items fg-inp-pass fh-w-label">
              <label style={{ padding: "0" }}>
                {auto_password == 0 ? "Enter " : "Set"} your password{" "}
              </label>
              {!otpEnable && (
                <Input.Password
                  autoFocus={true}
                  id="password"
                  placeholder="Password"
                  value={password}
                  onKeyUp={(e) => handleEnterPress(e, "password")}
                  onChange={(e) => handleChange("password", e)}
                  margin="normal"
                />
              )}
              {auto_password == 0 && (
                <div className="fg-otp-re">
                  <p>
                    <Button onClick={() => forgotPassword()}>
                      Forgot password?
                    </Button>
                  </p>
                </div>
              )}
            </div>
          )}
          {otpstatus && (
            <span className="fh-app-buttons text-center">
              <p>You don't have an account with us! </p>
              <p>Download our app and set up your account to login.</p>
              <a
                href="https://apps.apple.com/us/app/familheey/id1485617876"
                target="_blank"
                style={{ display: "inline-block", margin: "10px" }}
              >
                <img
                  src="images/button-appstore.png"
                  alt="Download on AppStore"
                />
              </a>
              <a
                href="https://play.google.com/store/apps/details?id=com.familheey.app&hl=en"
                // href="https://familheey-android-apk.s3.amazonaws.com/app-release.apk"
                target="_blank"
                style={{ display: "inline-block", margin: "10px" }}
              >
                <img
                  src="images/button-googleplay.png"
                  alt="Get it on PlayStore"
                />
              </a>
            </span>
          )}

          <div className="fh-submit">
            {!otpstatus && !otpEnable && !onBoard && (
              <Button
                variant="contained"
                onClick={(e) => clickSubmit(e)}
                className="fh-submit-btn"
              >
                Continue
              </Button>
            )}
          </div>
          {otpEnable && !otpstatus && (
            <div className="fh-otp-btn">
              <Button onClick={cancelOtp}>Cancel</Button>
              <Button onClick={clickOtp}>Send OTP</Button>
            </div>
          )}
          {onBoard && (
            <div className="fh-auth-flex">
              <div className="fg-form-items">
                <div className="fh-form-control">
                  <label htmlFor="profile-pix" className="fg-profile-pix">
                    {!img_preview && (
                      <img
                        accept="image/*"
                        src={
                          user_info.propic
                            ? process.env.REACT_APP_PRO_PIC_CROP +
                            process.env.REACT_APP_S3_BASE_URL +
                            "propic/" +
                            user_info.propic
                            : "images/profile-pic.png"
                        }
                      />
                    )}
                    {img_preview && <img src={img_preview} />}
                    <br />
                    Profile Picture
                  </label>
                  <input
                    required
                    id="profile-pix"
                    onChange={(event) => uploadPhoto(event)}
                    type="file"
                    data-lpignore="true"
                    autoComplete="off"
                    placeholder="PROFILE PIC"
                    className="hide"
                  />
                </div>

                <div className="fh-form-control fl-inp-flex">
                  <input
                    required
                    id="name"
                    placeholder="Name"
                    data-lpignore="true"
                    autoComplete="off"
                    className="fh-icons fh-icon-user"
                    value={full_name}
                    onChange={(e) => handleChange("full_name", e)}
                    margin="normal"
                  />
                </div>
                <div className="fh-form-control fl-inp-flex">
                  {/* <label>Gender</label> */}
                  <Select
                    style={{ width: "100%" }}
                    defaultValue={gender ? gender : ""}
                    value={gender}
                    onChange={(e) => handleChangeSelect("gender", e)}
                  >
                    <Option value="male">Male</Option>
                    <Option value="female">Female</Option>
                    <Option value="other">Other</Option>
                  </Select>
                </div>
                <div className="fh-form-control fl-inp-flex">
                  <input
                    required
                    id="email3"
                    type="text"
                    data-lpignore="true"
                    autoComplete="off"
                    placeholder="Email"
                    className="fh-icons fh-icon-mail"
                    value={email}
                    onChange={(e) => handleChange("email", e)}
                    margin="normal"
                  />
                </div>
                <div className="fh-form-control fl-inp-flex">
                  <input
                    required
                    id="email1"
                    type="text"
                    data-lpignore="true"
                    autoComplete="off"
                    placeholder="Origin"
                    className="fh-icons fh-icon-mail"
                    value={origin}
                    onChange={(e) => handleChange("origin", e)}
                    margin="normal"
                  />
                </div>
                <div className="fh-form-control fl-inp-flex">
                  <input
                    required
                    id="email2"
                    type="text"
                    data-lpignore="true"
                    autoComplete="off"
                    placeholder="Location"
                    className="fh-icons fh-icon-mail"
                    value={location}
                    onChange={(e) => handleChange("location", e)}
                    margin="normal"
                  />
                </div>
              </div>

              <div className="fh-submit">
                <Button
                  variant="contained"
                  onClick={this.saveProfile}
                  className="fh-submit-btn"
                >
                  UPDATE
                </Button>
              </div>
            </div>
          )}
          {!otpstatus && (
            <div className="fh-sm-buttons">
              <div className="fm-terms">
                <p>
                  You must verify your phone number to create a Familheey
                  account.
                </p>
                <p>
                  By clicking ‘Continue’ you agree to Familheey's{" "}
                  <a href="/termsofservice">Terms and Conditions</a> and{" "}
                  <a href="/policy">Privacy Policy</a>.
                </p>
              </div>
              {/* <div className="fh-sm-signup">Don't have an account? <Link to="/signup">Sign Up</Link></div> */}
            </div>
          )}
        </div>
      </div>
    </section>
  );
};
export default withStyles(styles)(Signin);


