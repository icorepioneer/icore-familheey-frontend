import axios from '../services/apiCall';
import axiosBeforeLogin from '../services/beforeLogin'
const signin = (user) => {
  return axiosBeforeLogin.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/login`, user);
}

const signout = () => {
  return axios.post(`${process.env.REACT_APP_API_URL}/auth/signout/`, {});
}

const generateOtp = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/loginWithOtp`, user);
}

const checkOtp = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/confirmotp`, user);
}

const socialLogin = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/auth/socialLogin`, user);
}
const confirmOtp = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/confirmotp`, data);

}
const registerSocial = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/users/registerSocial`, user);
}
const create = (user) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/register`, user);
}
const reset_password = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/reset_password`, data);
}
const change_password = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/change_password`, data);
}
const forgotPasswordApi = (data) => {
  return axios.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/forgot_password`, data);
}
const userExist = (data) =>{
  return axiosBeforeLogin.post(`${process.env.REACT_APP_API_URL}/api/v1/familheey/isUserExist`,data)
}

export {
  signin,
  signout,
  generateOtp,
  checkOtp,
  socialLogin,
  confirmOtp,
  registerSocial,
  create,
  reset_password,
  change_password,
  forgotPasswordApi,
  userExist
}
