import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types'
import { withStyles } from "@material-ui/core/styles";
import auth from './../auth/auth-helper'
import { Redirect } from 'react-router-dom'
import { generateOtp, checkOtp, change_password, socialLogin, forgotPasswordApi } from './api-auth.js'
import { updateUserDetails } from '../modules/users/userProfile/api-profile';
import "antd/dist/antd.css";
import { Select, message, Input, Spin } from "antd";
import countries from '../common/countrylist.json';
import { notifications } from '../shared/toasterMessage';

const { Option } = Select;
const styles = theme => ({
    card: {
        maxWidth: 600,
        margin: 'auto',
        textAlign: 'center',
        marginTop: theme.spacing.unit * 5,
        paddingBottom: theme.spacing.unit * 2
    },
    error: {
        verticalAlign: 'middle'
    },
    title: {
        marginTop: theme.spacing.unit * 2,
        color: theme.palette.openTitle
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300
    },
    submit: {
        margin: 'auto',
        marginBottom: theme.spacing.unit * 2
    }
})

class forgotPasswordClass extends Component {

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        if (localStorage.getItem('user_id')) {
            this.props.history.push(`/post`)
        }
    }

    state = {
        email: '',
        password: '',
        error: '',
        redirectToReferrer: false,
        otpstatus: false,
        password_status: false,
        onBoard: false,
        otp: '',
        user_id: '',
        message: '',
        full_name: '',
        phone: '',
        country: '',
        user_info: {},
        auto_password: 0,
        countryList: countries.countries,
        selectValue: '+91',
        otpEnable: false,
        img_preview: '',
        loader: false,
        one: '',
        two: '',
        three: '',
        four: '',
        five: '',
        six: ''
    }

    onChange = (value) => {
        var str = value.split("/");
        this.setState({ selectValue: str[1] })
    }
    componentDidMount() {
        this.setState({
            selectValue: this.props.location.state.selectValue,
            phone: this.props.location.state.phone
        });
    }
    clickSubmit = () => event => {
        var pe=/^\S{8,}$/;        
        let otp = this.state.one + '' + this.state.two + '' + this.state.three + '' + this.state.four + '' + this.state.five + '' + this.state.six;
        let errcount = 0;
        if(this.state.password=='') errcount++;
        if(otp=='') errcount++;
        if(errcount>0){
            notifications('error', "* required fields are empty", '')
            return
        } else if (this.state.password && pe.exec(this.state.password)==null) {
            notifications('error', 'Error', 'Minimum password length is 8 and space not allowed');
            return;
        }
        this.setState({ loader: true });
        change_password({
            phone: this.state.selectValue + this.state.phone,
            new_pwd: this.state.password,
            confirm_pwd: this.state.password,
            otp: otp
        })
            .then(result => {
                console.log(result);
                this.props.history.push('/signin')
            })
            .catch(err => {
                this.setState({ loader: false });
                notifications('error', 'Error', 'Invalid OTP')
            })


    }

    clickOtp = () => {
        this.setState({ otpstatus: true });
        const user = {
            phone: (this.state.selectValue + this.state.phone) || undefined,
            // country: this.state.selectValue || undefined
        }

        if (!this.state.phone || this.state.phone.length !== 10) {
            // this.setState({ error: 'Please enter a valid phone number' })
            var _message = 'Error'
            var _description = `Please enter a valid phone number`
            notifications('error', _message, _description)
            return;
        }

        generateOtp(user)
            .then((data) => {
                let resp_data = data.data;
                if (resp_data && resp_data.message.data === '') {
                    // this.setState({ error: 'please sign up for continue' })
                    // var _message = 'Warning'
                    // var _description = `Please enter a valid phone number`
                    notifications('warning', 'Warning', `Please enter a valid phone number`)
                } else {
                    console.log(resp_data);
                    this.setState({ error: '' });
                    // if (this.state.phone && resp_data.data.id)
                }
            })
    }
    saveProfile = () => {


        if (!this.state.gender || !this.state.origin || !this.state.full_name || !this.state.email || !this.state.location) {
            message.error('field missing');
            return;
        }

        const formData = new FormData();
        formData.append('id', this.state.user_info.id);
        if (this.state.cover_pic) {
            formData.append('cover_pic', this.state.cover_pic);
        }
        if (this.state.pro_pic) {
            formData.append('propic', this.state.pro_pic);
        }

        // formData.append('about', this.state.userDetails.about);
        formData.append('full_name', this.state.full_name);
        formData.append('gender', this.state.gender);
        formData.append('location', this.state.location);
        formData.append('email', this.state.email);
        formData.append('origin', this.state.origin);
        localStorage.setItem('user_id', this.state.user_info.id);
        localStorage.setItem('token', this.state.user_info.accessToken);
        updateUserDetails(formData).then((response) => {
            window.location.reload();
        }).catch((error) => {
            console.log('error', error)
        })
    }
    OtpSubmit = () => {
        let otp = this.state.one + '' + this.state.two + '' + this.state.three + '' + this.state.four + '' + this.state.five + '' + this.state.six;
        const user = {
            phone: this.state.selectValue + this.state.phone,
            otp: otp
        }

        checkOtp(user).then((r_data) => {
            let data = r_data.data;
            console.log(r_data);
            if (data.error) {
                this.setState({ error: data.error })
            } else {
                console.log(data);
                auth.authenticate(data, () => {
                    console.log(data.id)
                    this.setState({ password_status: true, otpstatus: false, auto_password: data.auto_password, user_id: data.id });
                    localStorage.setItem('user_id', data.id)
                    localStorage.setItem('token', data.accessToken)
                    // window.location.reload()
                })
            }
        })
            .catch(err => {
                document.getElementById('one').value = '';
                document.getElementById('two').value = '';
                document.getElementById('three').value = '';
                document.getElementById('four').value = '';
                document.getElementById('five').value = '';
                document.getElementById('six').value = '';
                this.setState({ otp: '', one: '', two: '', three: '', four: '', five: '', six: '' });
                message.error('Invalid otp code, Please try again');
            })
    }
    forgotPassword = () => {
        let { selectValue, phone } = this.state;
        forgotPasswordApi({ phone: selectValue + phone })
            .then((data) => {
                this.setState({ error: '', otpstatus: true, loader: false })
            })
    }
    backToPhoneNum = () => {
        this.setState({ otpstatus: false, otp: '', one: '', two: '', three: '', four: '', five: '', six: '' });
        this.props.history.push('/signin')
    }
    handleChangeSelect = name => event => {
        this.setState({ [name]: event });
    }
    handleChange = name => event => {
        console.log('jini-->',name,event.target.value);
        
        // console.log(name);
        // console.log(event);
        // console.log( event.target.value);
        
            // var pe=/^\S{8,}$/;
       
        if (name === 'phone') {
            var re = /^[0-9\b]+$/;
            if (event.target.value === '' || re.test(event.target.value)) {
                this.setState({ [name]: event.target.value, error: '' })
            }
            // else {
            //   // this.setState({ error: 'enter a valid phone number' })
            //   var message = 'Error'
            //   var description = `Please enter a valid phone number`
            //   notifications('error', message, description)
            // }
        } else {
            this.setState({ [name]: event.target.value })
        }
    }
    signinWithOtp = e => {
        this.setState({ otpEnable: true, phone: '' });
    }

    addTodoItem = function (todoItem) {
        this.state.push(todoItem);
        this.setState(todoItem);
    }
    changeFocus = (next, previous, current) => event => {
        var id;
        if (event.target.value.length > 1) {
            this.setState({ [next]: event.target.value.split('')[1] });
            id = document.getElementById(next);
            id.focus();
        } else {
            this.setState({ [current]: event.target.value.split('')[0] });
        }
        if ((event.target.value.length === 0 || event.target.value === undefined) && current !== 'one') {
            this.setState({ [current]: '' });
            id = document.getElementById(previous);
            id.focus();
        } else if ((event.target.value.length === 0 || event.target.value === undefined) && current === 'one') {
            this.setState({ [current]: '' });
            id = document.getElementById(current);
            id.focus();
        }
        console.log(this.state.otp);
        console.log(this.state);
        // if (next !== '' && event.target.value !== '') {
        //   id = document.getElementById(next);
        //   id.focus();
        // }
        // if (previous !== '' && !event.target.value) {
        //   id = document.getElementById(previous);
        //   id.focus()
        // }
        // if (!event.target.value) {
        //   var str = this.state.otp
        //   var substr = str.slice(0, str.length - 1)
        //   this.setState({ otp: substr })
        // } else {
        //   var otpval = `${this.state.otp}${event.target.value}`
        //   this.setState({ otp: otpval })
        // }
    }
    cancelOtp = e => {
        this.setState({ otpstatus: false, otpEnable: false })
    }
    uploadPhoto = e => {
        // console.log(e.target.files);
        this.setState({ pro_pic: e.target.files[0] })

        // this.setState({ imageUploading: true });
        let type = e.target.files[0].type;
        if (type.indexOf('image') == -1) {
            notifications('error', 'Error', 'Please upload image')
            return;
        }
        this.getBase64(e.target.files[0], imageUrl => {
            this.setState({ img_preview: imageUrl })
        });
    }
    getBase64 = (img, callback) => {
        if (img.type.indexOf('image') != -1) {
            const reader = new FileReader();
            reader.addEventListener('load', () => callback(reader.result));
            reader.readAsDataURL(img);
            this.setState({ image: img });
        } else {
            callback('');
        }

    }

    render() {
        // const { classes } = this.props
        const { from } = this.props.location.state || {
            from: {
                pathname: '/events'
            }
        }
        const { redirectToReferrer } = this.state
        if (redirectToReferrer) {
            return (<Redirect to={from} />)
        }



        return (
            <section className="fg-auth-page fh-auth-signin">
                <div className="fh-auth-left">
                    <div>
                        <h2>I am <strong>rich</strong> as<br />I have <strong className="fh-alt">reach</strong></h2>
                        <span className="fh-app-buttons">
                        <a href="https://apps.apple.com/us/app/familheey/id1485617876" target="_blank"><img src="images/button-appstore.png" alt="Download on AppStore" /></a>
                        <a href="https://play.google.com/store/apps/details?id=com.familheey.app&hl=en" target="_blank"><img src="images/button-googleplay.png" alt="Get it on PlayStore" /></a>
                        {/* <a href="https://familheey-android-apk.s3.amazonaws.com/app-release.apk" target="_blank"><img src="images/button-googleplay.png" alt="Get it on PlayStore" /></a> */}

                        </span>
                    </div>
                </div>
                <div className="fh-auth-right">
                    <div className="fh-auth-flex">
                        <div className="fh-logo">
                            <img src="images/logo.png" alt="Familheey" style={{ 'width': '53%' }} />
                        </div>
                        {
                            this.state.loader && (<div className="spin-loader">
                                <Spin> </Spin>
                            </div>)
                        }



                        <div className="fg-form-items fg-get-otp fh-w-label">
                            <Button className="fg-otp-back" onClick={this.backToPhoneNum} />
                            <label className="fg-alt">Enter the OTP*</label>
                            <label>Sent to {this.state.selectValue} {this.state.phone} </label>
                            <div className="fg-otp-fields">
                                {/* <button onClick={this.changeFocus('','')}>fsdaf</button> */}
                                <input id='one' className='otp-input' value={this.state.one} placeholder="-" autoFocus={true} maxLength='2' onChange={this.changeFocus('two', '', 'one')}  ></input>
                                <input id='two' className='otp-input' value={this.state.two} placeholder="-" maxLength='2' onChange={this.changeFocus('three', 'one', 'two')}></input>
                                <input id='three' className='otp-input' value={this.state.three} placeholder="-" maxLength='2' onChange={this.changeFocus('four', 'two', 'three')}></input>
                                <input id='four' className='otp-input' value={this.state.four} placeholder="-" maxLength='2' onChange={this.changeFocus('five', 'three', 'four')}></input>
                                <input id='five' className='otp-input' value={this.state.five} placeholder="-" maxLength='2' onChange={this.changeFocus('six', 'four', 'five')}></input>
                                <input id='six' className='otp-input' value={this.state.six} placeholder="-" maxLength='1' onChange={this.changeFocus('', 'five', 'six')}></input>
                            </div>
                            <div className="fg-otp-re">
                                {/* <p>Didn‘t you receive yet? <Button onClick={this.clickOtp}>Resend</Button></p> */}
                            </div>

                        </div>
                        <div className="fg-form-items fg-inp-pass fh-w-label">
                            <label>{(this.state.auto_password == 0) ? 'Enter ' : 'Set'} your new password* </label>
                            {!this.state.otpEnable &&
                                <Input.Password 
                                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  required
                                minLength={8} id="password" placeholder="Password" value={this.state.password} onChange={this.handleChange('password')} margin="normal" />

                            }

                        </div>
                        <div className="fh-submit">
                            <Button variant="contained" onClick={this.clickSubmit()} className="fh-submit-btn">Change</Button>
                        </div>
                        <div >
                            <p> <Button onClick={() => this.forgotPassword()}>Resend OTP</Button></p>
                        </div>
                        {
                            this.state.otpEnable && !this.state.otpstatus &&
                            <div className="fh-otp-btn">
                                <Button onClick={this.cancelOtp}>Cancel</Button><Button onClick={this.clickOtp}>Send OTP</Button>
                            </div>
                        }
                        {
                            this.state.onBoard && <div className="fh-auth-flex">
                                <div className="fg-form-items">
                                    <div className="fh-form-control">
                                        <label htmlFor="profile-pix" className="fg-profile-pix">
                                            {!this.state.img_preview && <img accept="image/*" src={this.state.user_info.propic ? process.env.REACT_APP_PRO_PIC_CROP + process.env.REACT_APP_S3_BASE_URL + 'propic/' + this.state.user_info.propic : "images/profile-pic.png"} />}
                                            {this.state.img_preview && <img src={this.state.img_preview} />}
                                            <br />Profile Picture
                      </label>
                                        <input required id="profile-pix" onChange={(event) => this.uploadPhoto(event)} type="file" data-lpignore="true" autoComplete="off" placeholder="PROFILE PIC" className="hide" />
                                    </div>

                                    <div className="fh-form-control fl-inp-flex">
                                        <input required id="name" placeholder="Name" data-lpignore="true" autoComplete="off" className="fh-icons fh-icon-user" value={this.state.full_name} onChange={this.handleChange('full_name')} margin="normal" />
                                    </div>
                                    <div className="fh-form-control fl-inp-flex">
                                        <Select style={{ width: '100%' }} defaultValue={this.state.gender ? this.state.gender : ''} value={this.state.gender} onChange={this.handleChangeSelect('gender')}>
                                            <Option value="male">Male</Option>
                                            <Option value="female">Female</Option>
                                            <Option value="other">Other</Option>
                                        </Select>
                                    </div>

                                    <div className="fh-form-control fl-inp-flex">
                                        <input required id="email3" type="text" data-lpignore="true" autoComplete="off" placeholder="Email" className="fh-icons fh-icon-mail" value={this.state.email} onChange={this.handleChange('email')} margin="normal" />
                                    </div>
                                    <div className="fh-form-control fl-inp-flex">
                                        <input required id="email1" type="text" data-lpignore="true" autoComplete="off" placeholder="Orgin" className="fh-icons fh-icon-mail" value={this.state.origin} onChange={this.handleChange('origin')} margin="normal" />
                                    </div>
                                    <div className="fh-form-control fl-inp-flex">
                                        <input required id="email2" type="text" data-lpignore="true" autoComplete="off" placeholder="Location" className="fh-icons fh-icon-mail" value={this.state.location} onChange={this.handleChange('location')} margin="normal" />
                                    </div>

                                </div>

                                <div className="fh-submit">
                                    <Button variant="contained" onClick={this.saveProfile} className="fh-submit-btn">UPDATE</Button>
                                </div>
                            </div>

                        }
                        <div className="fh-sm-buttons">
                            {/* <label>Sign In with</label> */}
                            {/* <div className="fh-sm-button-flex"> */}
                            {/* <FacebookProvider appId={process.env.REACT_APP_FB_ID}>
                  <LoginButton scope="email" onCompleted={this.responseFB} onError={this.handleFailure}><img className="fh-sm-btn" alt='' src="facebook.png" /></LoginButton>
                </FacebookProvider>

                <GoogleLogin clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID} render={renderProps => (<button onClick={renderProps.onClick} disabled={renderProps.disabled}><img src="google.png" alt='' className="fh-sm-btn" /></button>)} buttonText="Sign in" onSuccess={this.responseGoogle} onFailure={this.handleFailure} /> */}

                            {/* <CustomLinkedIN addTodoItem={this.addTodoItem} from='signIn' render={renderProps => (<button onClick={renderProps.onClick} disabled={renderProps.disabled}><img src="linkedin.png" className="fh-sm-btn" /></button>)} buttonText="Sign in"></CustomLinkedIN> */}
                            {/* </div> */}
                            <div className="fm-terms">
                                <p>You must verify your phone number to create a Familheey account.</p>
                                <p>By clicking ‘Continue’ you agree to Familheey's <a href="/termsofservice">Terms and Conditions</a> and <a href="/policy">Privacy Policy </a>.</p>
                            </div>
                            {/* <div className="fh-sm-signup">Don't have an account? <Link to="/signup">Sign Up</Link></div> */}
                        </div>
                    </div>

                </div>
            </section>

        )
    }

    /**
     * after sucessful login from gamil save data to db
     */
    responseGoogle = (res) => {

        res.profileObj.type = "google";
        if (res.profileObj && res.profileObj.email) {
            socialLogin(res.profileObj)
                .then((data) => {
                    if (data.status === 200) {
                        auth.authenticate(data, () => {
                            // this.setState({ redirectToReferrer: true })
                            window.location.reload()
                        })
                    } else {
                        this.setState({ error: data.message })
                    }
                })
                .catch((err) => {
                    this.setState({ error: err })
                })
        }
    }

    /**
     * retun from fb error/success
     */
    responseFB = (res) => {
        res['profile'].fbId = res['profile'].id;
        res['profile'].photo = res['profile'].picture ? res['profile'].picture['data'].url : '';
        res['profile'].type = "fb";
        if (res && res['profile']) {
            socialLogin(res['profile'])
                .then((data) => {
                    if (data.status === 200) {
                        auth.authenticate(data, () => {
                            // this.setState({ redirectToReferrer: true })
                            window.location.reload()
                        })
                    } else {
                        this.setState({ error: data.message })
                    }
                })
                .catch((err) => {
                    this.setState({ error: err })
                })
        }
    }

    handleFailure = (err) => {
        this.setState({ error: err.message })
    }
}

forgotPasswordClass.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(forgotPasswordClass)